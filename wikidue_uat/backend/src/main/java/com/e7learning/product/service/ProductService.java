package com.e7learning.product.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.common.Constant;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.CommonService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.service.MailService;
import com.e7learning.common.utils.CommonUtils;
import com.e7learning.product.form.ProductForm;
import com.e7learning.repository.CourseRepository;
import com.e7learning.repository.KnowledgeMapRepository;
import com.e7learning.repository.ProductAdRepository;
import com.e7learning.repository.ProductRepository;
import com.e7learning.repository.ProductTagRepository;
import com.e7learning.repository.PropsRepository;
import com.e7learning.repository.SubOrderDetailRepository;
import com.e7learning.repository.TagRepository;
import com.e7learning.repository.VendorRepository;
import com.e7learning.repository.model.Camp;
import com.e7learning.repository.model.Course;
import com.e7learning.repository.model.KnowledgeMap;
import com.e7learning.repository.model.Product;
import com.e7learning.repository.model.ProductAd;
import com.e7learning.repository.model.ProductTag;
import com.e7learning.repository.model.Props;
import com.e7learning.repository.model.SubOrderDetail;
import com.e7learning.repository.model.Tag;
import com.e7learning.repository.model.Vendor;

/**
 * @author J
 *
 */
@Service
public class ProductService extends BaseService {

	private static final Logger log = Logger.getLogger(ProductService.class);

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ConfigService configService;

	@Autowired
	private MessageSource resources;

	@Autowired
	private PropsRepository propsRepository;

	@Autowired
	private CourseRepository courseRepository;

	@Autowired
	private TagRepository tagRepository;

	@Autowired
	private ProductTagRepository productTagRepository;

	@Autowired
	private KnowledgeMapRepository knowledgeMapRepository;

	@Autowired
	private SubOrderDetailRepository subOrderDetailRepository;

	@Autowired
	private ProductAdRepository productAdRepository;

	@Autowired
	private VendorRepository vendorRepository;

	@Autowired
	private MailService mailService;

	@Autowired
	private CommonService commonService;

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void addProduct(ProductForm form) throws ServiceException {
		try {
			Product product = new Product();
			BeanUtils.copyProperties(product, form);
			if (StringUtils.equalsIgnoreCase("PROPS", form.getCategoryType())) {
				// product.setQuantity(Integer.valueOf(form.getQuantity()));
				Props props = propsRepository.findOne(Integer.valueOf(form.getPkProps()));
				if (props != null) {
					product.setProps(props);
					product.setVendor(props.getVendor());
				}
			}
			if (StringUtils.equalsIgnoreCase("COURSE", form.getCategoryType())) {
				Course course = courseRepository.findOne(Integer.valueOf(form.getPkCourse()));
				if (course != null) {
					product.setCourse(course);
					product.setVendor(course.getVendor());
				}
			}
			if(form.getPublishEDate() != null) {
				product.setPublishEDate(CommonUtils.getDateEndWithTimes(form.getPublishEDate()));
			}
			product.setPrice(Integer.valueOf(form.getPrice()));
			product.setDiscountPrice(Integer.valueOf(form.getDiscountPrice()));
			if(StringUtils.isNotBlank(form.getWeights())) {
				product.setWeights(Integer.valueOf(form.getWeights()));
			}else {
				product.setWeights(null);
			}
			product.setUpdateDt(new Date());
			product.setCreateDt(new Date());
			product.setCreateId(getCurrentUserId());
			product.setLastModifiedId(getCurrentUserId());
			product = productRepository.save(product);
			// add Product_Tag
			addProductTag(product);
		} catch (Exception e) {
			log.error("addProduct", e);
			throw new ServiceException(resources.getMessage("op.add.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.add.success", null, null));
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void saveProduct(ProductForm form) throws ServiceException {
		try {
			Product product = productRepository.findOne(form.getPkProduct());
			if (product != null) {
				BeanUtils.copyProperties(product, form);
				if(form.getPublishEDate() != null) {
					product.setPublishEDate(CommonUtils.getDateEndWithTimes(form.getPublishEDate()));
				}
				product.setUpdateDt(new Date());
				// if(StringUtils.equalsIgnoreCase("PROPS",
				// form.getCategoryType())) {
				// product.setQuantity(Integer.valueOf(form.getQuantity()));
				// }
				product.setPrice(Integer.valueOf(form.getPrice()));
				product.setDiscountPrice(Integer.valueOf(form.getDiscountPrice()));
				if(StringUtils.isNotBlank(form.getWeights())) {
					product.setWeights(Integer.valueOf(form.getWeights()));
				}else {
					product.setWeights(null);
				}
				product.setUpdateDt(new Date());
				product.setLastModifiedId(getCurrentUserId());

				// update Product_Tag
				updateProductTag(product.getPkProduct());
			}
		} catch (Exception e) {
			log.error("editProduct", e);
			throw new ServiceException(resources.getMessage("op.modify.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.modify.success", null, null));
	}

	public Page<Product> queryProduct(ProductForm form, Integer page) throws ServiceException {
		try {
			Sort sort = new Sort(Direction.ASC, "publishSDate");
			Pageable pageable = new PageRequest(page - 1, configService.getPageSize(), sort);
			Page<Product> result = productRepository.findAll(buildOperSpecification(form), pageable);
			return result;
		} catch (Exception e) {
			log.error("queryProduct", e);
			throw new ServiceException(resources.getMessage("op.query.error", null, null));
		}
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void updateProductTag(Product product) throws ServiceException {
		try {
			if (product != null) {
				this.updateProductTag(product.getPkProduct());
			}
		} catch (Exception e) {
			log.error("updateProductTag", e);
			throw new ServiceException(resources.getMessage("product.update.tag.err", null, null));
		}
		throw new MessageException(resources.getMessage("product.update.tag.success", null, null));
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void remove(String id) throws ServiceException {
		String result = this.validateBeforeRemove(id);
		if (StringUtils.isNotEmpty(result)) {
			throw new ServiceException(result);
		}
		try {
			Product product = productRepository.findOne(id);
			if (product != null) {
				// remve Prodcut_Tag
				removeProductTag(product.getPkProduct());
				productRepository.delete(product);
			}
		} catch (Exception e) {
			log.error("removeProduct", e);
			throw new ServiceException(resources.getMessage("op.del.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.del.success", null, null));
	}

	private String validateBeforeRemove(String pkProduct) {
		String result = "";
		List<SubOrderDetail> list = subOrderDetailRepository.findProductByPkProduct(pkProduct);
		if (list != null && !list.isEmpty()) {
			result = resources.getMessage("product.validate.po", null, null);
		} else {
			List<ProductAd> productAdList = productAdRepository
					.findAll((Root<ProductAd> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {

						List<Predicate> predicates = new ArrayList<>();

						predicates.add(cb.equal(root.get("product").get("pkProduct"), pkProduct));

						if (!predicates.isEmpty()) {
							return cb.and(predicates.toArray(new Predicate[predicates.size()]));
						}
						return cb.conjunction();
					});
			if (productAdList != null && !productAdList.isEmpty()) {
				result = resources.getMessage("productAd.exists", null, null);
			}
		}
		return result;
	}

	public Product findByPkProduct(String id) {
		Product product = null;
		if (StringUtils.isNotEmpty(id)) {
			product = productRepository.findOne(id);
		}
		return product;
	}

	// 驗證是否已存在上架商品
	public boolean isOnMarket(ProductForm form) {
		boolean result = false;
		List<Product> list = null;
		try {
			if (StringUtils.isBlank(form.getPkProduct())) {
				// 新增
				if (StringUtils.equalsIgnoreCase("PROPS", form.getCategoryType())) {
					list = productRepository.findByPkProps(Integer.valueOf(form.getPkProps()), form.getPublishSDate(),
							form.getPublishEDate());
					if (list != null && list.size() > 0) {
						Product p = list.get(0);
						// System.err.println(BeanUtils.describe(p));
					}
				} else if (StringUtils.equalsIgnoreCase("COURSE", form.getCategoryType())) {
					list = productRepository.findByPkCourse(Integer.valueOf(form.getPkCourse()), form.getPublishSDate(),
							form.getPublishEDate());
					// 上架日期要大於[最後上架商品]的下架日期
					if (list != null && list.size() > 0) {
						Product p = list.get(0);
						// System.err.println(BeanUtils.describe(p));
					}
				}
			} else {
				// 修改
				if (StringUtils.isNotBlank(form.getPkProps())) {
					list = productRepository.findByPkPropsAndPkProduct(form.getPkProduct(),
							Integer.valueOf(form.getPkProps()), form.getPublishSDate(), form.getPublishEDate());
					if (list != null && list.size() > 0) {
						Product p = list.get(0);
						// System.err.println(BeanUtils.describe(p));
					}
				} else if (StringUtils.isNotBlank(form.getPkCourse())) {
					list = productRepository.findByPkCourseAndPkProduct(form.getPkProduct(),
							Integer.valueOf(form.getPkCourse()), form.getPublishSDate(), form.getPublishEDate());
					if (list != null && list.size() > 0) {
						Product p = list.get(0);
						// System.err.println(BeanUtils.describe(p));
					}
				}
			}
		} catch (Exception e) {

		}
		if (list != null && !list.isEmpty()) {
			result = true;
		}
		return result;
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void inventoryNotice() {
		try {
			List<Product> lowInventoryProductList = productRepository
					.findAll((Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
						query.distinct(true);
						List<Predicate> predicates = new ArrayList<>();

						predicates.add(cb.equal(root.get("active"), Constant.TRUE));
						predicates.add(cb.isNotNull(root.get("props")));
						predicates.add(cb.greaterThanOrEqualTo(root.get("publishEDate"), new Date()));
						predicates.add(cb.lessThanOrEqualTo(root.get("publishSDate"), new Date()));
						predicates.add(cb.lessThanOrEqualTo(root.get("props").get("quantity"), configService.getLowInventory()));

						if (!predicates.isEmpty()) {
							return cb.and(predicates.toArray(new Predicate[predicates.size()]));
						}

						return cb.conjunction();
					});
			if (lowInventoryProductList != null && !lowInventoryProductList.isEmpty()) {
				List<String> adminEmailList = commonService.getAdminEmailList();
				Date now = new Date();
				Date sendDate = now;
				Date noticeDate = now;
				Map<String, List<Product>> vendorMap = new HashMap<>();
				for (Product product : lowInventoryProductList) {
					Vendor vendor = product.getVendor();
					if (vendorMap.get(vendor.getUid()) == null) {
						vendorMap.put(vendor.getUid(), new ArrayList<>());
					}
					vendorMap.get(vendor.getUid()).add(product);
				}
				for (Entry<String, List<Product>> entry : vendorMap.entrySet()) {
					String vendorUid = entry.getKey();
					Vendor vendor = vendorRepository.findOne(vendorUid);
					List<Product> productList = entry.getValue();
					String vendorName = StringUtils.isEmpty(vendor.getVendorName()) ? vendor.getUid()
							: vendor.getVendorName();
					List<Map<String, String>> details = new ArrayList<>();
					for (Product product : productList) {
						Map<String ,String> detail = new HashMap<>();
						Props props = product.getProps();
						detail.put("materialNum", props.getMaterialNum());
						detail.put("name", product.getProductName());
						detail.put("price", String.valueOf(product.getDiscountPrice()));
						detail.put("quantity", String.valueOf(product.getProps().getQuantity()));
						details.add(detail);
					}
					mailService.sendInventoryNotificationEmail(
							new String[] {
									StringUtils.isEmpty(vendor.getEmail()) ? vendor.getUid() : vendor.getEmail() },
							adminEmailList.toArray(new String[adminEmailList.size()]), sendDate, vendorName, noticeDate,
							details);
				}
			}
		} catch (Exception e) {
			log.error("inventoryNotice", e);
		}
	}

	private void addProductTag(Product product) {
		if (product != null) {
			List<Tag> tagList = null;
			if (product.getProps() != null) {
				tagList = tagRepository.findByPkProps(product.getProps().getPkProps());
			} else if (product.getCourse() != null) {
				tagList = tagRepository.findByPkCourse(product.getCourse().getPkCourse());
			}
			if (tagList != null && tagList.size() > 0) {
				List<ProductTag> productTagList = transferToProductTagList(tagList, product);
				if (productTagList != null) {
					productTagRepository.save(productTagList);
				}
			}
		}
	}

	private void removeProductTag(String pkProduct) {
		if (StringUtils.isNotBlank(pkProduct)) {
			productTagRepository.deleteByPkProduct(pkProduct);
		}
	}

	private void updateProductTag(String pkProduct) {
		if (StringUtils.isNotBlank(pkProduct)) {
			Product product = findByPkProduct(pkProduct);
			if (product != null) {
				removeProductTag(pkProduct);
				addProductTag(product);
			}
		}
	}

	private List<ProductTag> transferToProductTagList(List<Tag> tagList, Product product) {
		List<ProductTag> productTagList = null;
		if (tagList != null && tagList.size() > 0 && product != null) {
			Set<String> tagSet = new TreeSet<String>();
			Set<Integer> categorySet = new TreeSet<Integer>();
			productTagList = new ArrayList<ProductTag>();
			for (Tag tag : tagList) {
				flatten(tagSet, categorySet, tag);
			}

			for (String code : tagSet) {
				ProductTag pt = new ProductTag();
				pt.setProduct(product);
				setProductTagInfo(pt);
				pt.setTag(code);
				this.setTagName(pt);
				productTagList.add(pt);
				for (Integer pkCategory : categorySet) {
					pt = new ProductTag();
					pt.setProduct(product);
					setProductTagInfo(pt);
					pt.setTag(code + "-" + pkCategory);
					productTagList.add(pt);
				}
			}
			for (Integer pkCategory : categorySet) {
				ProductTag pt = new ProductTag();
				pt.setProduct(product);
				setProductTagInfo(pt);
				pt.setTag(pkCategory.toString());
				productTagList.add(pt);
			}
			// for(ProductTag pt:productTagList) {
			// System.err.println(pt.getTag()+":"+pt.getTagLevel());
			// }
		}
		return productTagList;
	}

	private void flatten(Set<String> tagSet, Set<Integer> categorySet, Tag tag) {
		if (tag != null) {
			if (StringUtils.isNotBlank(tag.getTag())) {
				String code = tag.getTag();
				String[] codeArray = code.split("-");
				String s = codeArray[0];
				tagSet.add(s);
				for (int i = 1; i < codeArray.length; i++) {
					s = s + "-" + codeArray[i];
					if (i != 2) {
						tagSet.add(s);
					}
				}
			} else if (tag.getCategory() != null) {
				categorySet.add(tag.getCategory().getPkCategory());
			}
		}
	}

	private void setProductTagInfo(ProductTag tag) {
		if (tag != null) {
			tag.setActive("1");
			tag.setUpdateDt(new Date());
			tag.setCreateDt(new Date());
			tag.setCreateId(getCurrentUserId());
			tag.setLastModifiedId(getCurrentUserId());
		}
	}

	// 取得知識地圖名稱
	private void setTagName(ProductTag productTag) {
		if (productTag != null && StringUtils.isNotBlank(productTag.getTag())) {
			KnowledgeMap km = knowledgeMapRepository.findOne(productTag.getTag());
			if (km != null) {
				productTag.setTagName(km.getTagName());
				productTag.setTagLevel(km.getTagLevel());
			}
		}
	}

	private Specification<Product> buildOperSpecification(ProductForm form) {
		return new Specification<Product>() {
			@Override
			public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

				List<Predicate> predicates = new ArrayList<>();
				if (isAdmin()) {
					if (!StringUtils.isEmpty(form.getVendorName())) {
						Join<Product, Vendor> vendorJoin = root
								.join(root.getModel().getSingularAttribute("vendor", Vendor.class), JoinType.LEFT);
						predicates.add(cb.like(vendorJoin.get("vendorName").as(String.class),
								"%" + form.getVendorName() + "%"));
					}
				} else {
					// 非管理者只能查屬於自己的訂單
					Join<Product, Vendor> vendorJoin = root
							.join(root.getModel().getSingularAttribute("vendor", Vendor.class), JoinType.LEFT);
					predicates.add(cb.equal(vendorJoin.get("uid").as(String.class), getCurrentUserId()));
				}
				if (!StringUtils.isEmpty(form.getProductName())) {
					Path<String> np = root.get("productName");
					predicates.add(cb.like(np, "%" + form.getProductName() + "%"));
				}
				if (form.getPublishSDate() != null) {
					Path<Date> np = root.get("publishSDate");
					Date sDate = form.getPublishSDate();
					sDate = CommonUtils.getDateWithoutTimes(sDate);
					predicates.add(cb.greaterThanOrEqualTo(np, sDate));
				}
				if (form.getPublishEDate() != null) {
					Path<Date> np = root.get("publishEDate");
					Date eDate = form.getPublishEDate();
					eDate = CommonUtils.getDateEndWithTimes(eDate);
					predicates.add(cb.lessThanOrEqualTo(np, eDate));
				}
				if (!StringUtils.isEmpty(form.getCategoryType())) {
					String categoryType = form.getCategoryType();
					if (StringUtils.equalsIgnoreCase("CAMP", categoryType)) {
						Join<Product, Camp> campJoin = root
								.join(root.getModel().getSingularAttribute("camp", Camp.class), JoinType.LEFT);
						predicates.add(cb.isNotNull(campJoin.get("pkCamp").as(Integer.class)));
					}
					if (StringUtils.equalsIgnoreCase("COURSE", categoryType)) {
						Join<Product, Course> courseJoin = root
								.join(root.getModel().getSingularAttribute("course", Course.class), JoinType.LEFT);
						predicates.add(cb.isNotNull(courseJoin.get("pkCourse").as(Integer.class)));
					}
					if (StringUtils.equalsIgnoreCase("PROPS", categoryType)) {
						Join<Product, Props> courseJoin = root
								.join(root.getModel().getSingularAttribute("props", Props.class), JoinType.LEFT);
						predicates.add(cb.isNotNull(courseJoin.get("pkProps").as(Integer.class)));

					}
				}
				if (form.isOnMarket()) {
					Date today = new Date();
					today = CommonUtils.getDateWithoutTimes(today);
					Path<Date> np = root.get("publishSDate");
					predicates.add(cb.lessThanOrEqualTo(np, today));
					np = root.get("publishEDate");
					predicates.add(cb.greaterThanOrEqualTo(np, today));
				}
				if (!predicates.isEmpty()) {
					return cb.and(predicates.toArray(new Predicate[predicates.size()]));
				}
				return cb.conjunction();
			}
		};
	}
}
