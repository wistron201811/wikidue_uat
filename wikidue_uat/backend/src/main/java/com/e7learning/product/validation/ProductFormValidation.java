package com.e7learning.product.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.e7learning.common.ValidatorUtil;
import com.e7learning.common.utils.CommonUtils;
import com.e7learning.product.form.ProductForm;
import com.e7learning.product.service.ProductService;

@Component
public class ProductFormValidation implements Validator {
	
	@Autowired
	private ProductService productService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(ProductForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ProductForm form = (ProductForm) target;
		if (StringUtils.equalsIgnoreCase("save", form.getMethod())
				|| StringUtils.equalsIgnoreCase("modify", form.getMethod())) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "productName", "validate.empty.message");
			if(StringUtils.equalsIgnoreCase(form.getCategoryType(), "PROPS")) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "quantity", "validate.empty.message");
			}
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "publishSDate", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "publishEDate", "validate.empty.message");
			//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price", "validate.empty.message");
			//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "discountPrice", "validate.empty.message");
			if(productService.isAdmin()) {
				//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "weights", "validate.empty.message");
			}
		}
		if(StringUtils.isNotBlank(form.getQuantity()) ){
			if(!ValidatorUtil.isNumber(form.getQuantity())) {
				errors.rejectValue("quantity", "validate.number.format");
			}else {
				if(Integer.valueOf(form.getQuantity()) < 0) {
					errors.rejectValue("quantity", "validate.quantity.negative");
				}
			}
		}
		if(StringUtils.isNotBlank(form.getPrice()) ){
			if(!ValidatorUtil.isNumber(form.getPrice())) {
				errors.rejectValue("price", "validate.number.format");
			}else {
				if(Integer.valueOf(form.getPrice()) < 0) {
					errors.rejectValue("price", "validate.price.negative");
				}else if(Integer.valueOf(form.getPrice()) == 0) {
					if(StringUtils.equalsIgnoreCase(form.getCategoryType(), "PROPS")) {
						errors.rejectValue("price", "validate.price.zero");
					}
				}
			}
		}
		if(StringUtils.isNotBlank(form.getDiscountPrice()) ){
			if(!ValidatorUtil.isNumber(form.getDiscountPrice())) {
				errors.rejectValue("discountPrice", "validate.number.format");
			}else {
				if(Integer.valueOf(form.getDiscountPrice()) < 0) {
					errors.rejectValue("discountPrice", "validate.price.negative");
				}else if(Integer.valueOf(form.getDiscountPrice()) == 0) {
					if(StringUtils.equalsIgnoreCase(form.getCategoryType(), "PROPS")) {
						errors.rejectValue("discountPrice", "validate.price.zero");
					}
				}
			}
		}
		//  驗證起迄日
		if(form.getPublishEDate() != null && form.getPublishSDate() !=null) {
			// 新增商品時間驗證
			if(StringUtils.isBlank(form.getPkProduct())) {
				if(form.getPublishSDate().compareTo(CommonUtils.getTodayWithoutTimes()) < 0) {
					errors.rejectValue("publishSDate", "product.validate.start.date");
				}
			}else {
				// 修改商品(上架日期小於今日，不可修改)
				if(form.isSdateReadonly()) {
					// 驗證下架日期
					if(form.getPublishEDate().compareTo(CommonUtils.getTodayWithoutTimes()) < 0) {
						errors.rejectValue("publishEDate", "product.validate.end.date");
					}
				}
			}
			if(form.getPublishEDate().compareTo(form.getPublishSDate()) < 0) {
				errors.rejectValue("publishEDate", "product.publish.date.error");
			}else {
				// 驗證是否已存在上架商品 
				if(productService.isOnMarket(form)) {
					errors.rejectValue("publishEDate", "product.validate.market");
				}
			}
		}
		// 驗證原價、售價
		if(StringUtils.isNotBlank(form.getDiscountPrice())  && StringUtils.isNotBlank(form.getPrice()) 
				&& ValidatorUtil.isNumber(form.getPrice()) && ValidatorUtil.isNumber(form.getDiscountPrice())){
			if(Integer.parseInt(form.getDiscountPrice()) > Integer.parseInt(form.getPrice())) {
				errors.rejectValue("discountPrice", "product.price.error");
			}
		}
		// 權重
		if (StringUtils.isNotEmpty(form.getWeights())) {
			if (!ValidatorUtil.isNumber(form.getWeights())) {
				errors.rejectValue("weights", "validate.number.format");
			}else {
				int courseSplitRatio = Integer.parseInt(form.getWeights());
				if(courseSplitRatio < 0 || courseSplitRatio > 9999) {
					errors.rejectValue("weights", "product.validate.weights");
				}
			}
		}
		if (errors.hasErrors()) {
			return;
		}
	}
	
}