package com.e7learning.product.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.e7learning.common.controller.BaseController;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.utils.CommonUtils;
import com.e7learning.product.form.ProductForm;
import com.e7learning.product.service.ProductService;
import com.e7learning.product.validation.ProductFormValidation;
import com.e7learning.repository.model.Product;

/**
 * @author J
 *
 */
@Controller
@RequestMapping(value = "/product")
@SessionAttributes(ProductController.FORM_BEAN_NAME)
public class ProductController extends BaseController {

	private static final Logger log = Logger.getLogger(ProductController.class);

	public static final String FORM_BEAN_NAME = "productForm";
	
	private static final String QUERY_PAGE = "queryProductPage";

	private static final String ADD_PAGE = "addProductPage";
	
	private static final String EDIT_PAGE = "editProductPage";

	@Autowired
	private ProductFormValidation productFormValidation;
	
	@Autowired
	private ProductService productService ;
	
	@Autowired
	private ConfigService configService;
	
	@InitBinder(ProductController.FORM_BEAN_NAME)
	protected void initBinder(WebDataBinder binder, HttpSession session) {
		binder.setValidator(productFormValidation);
	}
	
	@RequestMapping(value = "/query", method = { RequestMethod.GET })
	public String goQuery(Model model) {
		ProductForm form = new ProductForm();
		try {
			form.setResult(productService.queryProduct(form, 1));
		} catch (ServiceException e) {
			
		}
		model.addAttribute(FORM_BEAN_NAME, form);
		return QUERY_PAGE;
	}

	@RequestMapping(value = "/query", params = "method=query")
	public String query(HttpServletRequest request, ProductForm form, Model model) {
		String pageName = new ParamEncoder("list").encodeParameterName(TableTagParameters.PARAMETER_PAGE);
		int nextPage = getPage(request.getParameter(pageName));
		try {
			form.setResult(productService.queryProduct(form, nextPage));
		} catch (ServiceException e) {
			
		}
		return QUERY_PAGE;
	}
	
	@RequestMapping(value = "/add", method = { RequestMethod.GET })
	public String goAdd(Model model) {
		ProductForm form = new ProductForm();
		// 預設排序權重
		form.setWeights(configService.getWeights());
		model.addAttribute(FORM_BEAN_NAME, form);
		return ADD_PAGE;
	}

	@RequestMapping(params = "method=save", value = "/add", method = { RequestMethod.POST })
	public String add(@Validated ProductForm form, BindingResult result) {
		if (result.hasErrors()) {
			return ADD_PAGE;
		}
		try {
			productService.addProduct(form);
			return "redirect:/product/query?method=query";
		} catch (ServiceException e) {
			
		}
		return ADD_PAGE;
	}
	
	@RequestMapping(params = "method=remove", value = "/remove", method = { RequestMethod.POST })
	public String remove(ProductForm form, Model model) {
		if(StringUtils.isNotBlank(form.getPkProduct() )) {
			Product product = productService.findByPkProduct(form.getPkProduct());
			if(product != null) {
				try {
					productService.remove(product.getPkProduct());
					return "redirect:/product/query?method=query";
				} catch (ServiceException e) {
					// 避免display tag 查詢request發生405錯誤
					form.setResult(null);
					model.addAttribute(FORM_BEAN_NAME, form);
				}
			}
		}
		return QUERY_PAGE;
	}
	
	@RequestMapping(value = "/modify", method = { RequestMethod.POST })
	public String goModify(Model model, ProductForm form) {
		if(StringUtils.isNotBlank(form.getPkProduct() )) {
			Product product = productService.findByPkProduct(form.getPkProduct());
			try {
				BeanUtils.copyProperties(form, product);
				// 顯示props數量
				if(product.getProps() != null) {
					form.setQuantity(product.getProps().getQuantity()+"");
					form.setCategoryType("PROPS");
				}else if(product.getCourse() != null) {
					form.setCategoryType("COURSE");
				}
				setProductFormStatus(form, product);
			} catch (Exception e) {
				e.printStackTrace();
			}
			model.addAttribute(FORM_BEAN_NAME, form);
		}
		return EDIT_PAGE;
	}
	
	@RequestMapping(params = "method=modify", value = "/modify", method = { RequestMethod.POST })
	public String modify(@Validated ProductForm form,BindingResult result) {
		if (result.hasErrors()) {
			return EDIT_PAGE;
		}
		try {
			productService.saveProduct(form);
			return "redirect:/product/query?method=query";
		} catch (ServiceException e) {
			
		}
		return EDIT_PAGE;
	}
	
	@RequestMapping(params = "method=update",value = "/updateTag", method = { RequestMethod.POST })
	public String updateTag(Model model, ProductForm form) {
		if(StringUtils.isNotBlank(form.getPkProduct() )) {
			Product product = productService.findByPkProduct(form.getPkProduct());
			try {
				BeanUtils.copyProperties(form, product);
				setProductFormStatus(form, product);
				productService.updateProductTag(product);
			} catch (Exception e) {
				e.printStackTrace();
			}
			model.addAttribute(FORM_BEAN_NAME, form);
		}
		return EDIT_PAGE;
	}
	
	private void setProductFormStatus(ProductForm form,Product product) {
		Date today = CommonUtils.getTodayWithoutTimes();
		if(product.getPublishSDate().compareTo(today) < 0) {
			form.setSdateReadonly(true);
		}else {
			form.setSdateReadonly(false);
		}
		if(product.getPublishEDate().compareTo(today) < 0 ) {
			form.setPageReadonly(true);
		}else {
			form.setPageReadonly(false);
		}
		if(product.getProps() != null) {
			form.setPkProps(product.getProps().getPkProps().toString());
		}else if(product.getCourse() != null) {
			form.setPkCourse(product.getCourse().getPkCourse().toString());
		}else if(product.getCamp() != null) {
			form.setPkCamp(product.getCamp().getPkCamp().toString());
		}
	}
	
}
