
package com.e7learning.product.form;

import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;

import com.e7learning.common.form.BaseForm;
import com.e7learning.repository.model.Product;

/**
 * @author J
 * 
 * 商品管理
 */
public class ProductForm extends BaseForm<Product>{

	/**
	 * 商品PK
	 */
	private String pkProduct;
	/**
	 * 教具PK
	 */
	private String pkProps;
	/**
	 * 課程PK
	 */
	private String pkCourse;
	/**
	 * 營隊PK
	 */
	private String pkCamp;
	/**
	 * 商品名稱
	 */
	private String productName;
	/**
	 * 商品數量
	 */
	private String quantity;
	/**
	 * 上架日期
	 */
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date publishSDate;
	/**
	 * 下架日期
	 */
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date publishEDate;
	/**
	 * 原價
	 */
	private String price;
	/**
	 * 售價
	 */
	private String discountPrice;
	
	/**
	 * 啟用
	 */
	private String  active;
	
	/**
	 * 分類類型
	 */
	private String categoryType;
	
	/**
	 * 廠商名稱
	 */
	private String vendorName;
	/**
	 * 廠商代號
	 */
	private String pkVendor;
	
	/**
	 * 修改頁面是否唯讀
	 */
	private boolean isPageReadonly;
	
	private boolean isSdateReadonly;
	
	/**
	 * 正在上架
	 */
	private boolean onMarket;
	
	/**
	 * 排序權重
	 */
	private String weights;
	
	
	public ProductForm() {
	}


	public String getPkProduct() {
		return pkProduct;
	}


	public void setPkProduct(String pkProduct) {
		this.pkProduct = pkProduct;
	}



	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		this.productName = productName;
	}


	public String getQuantity() {
		return quantity;
	}


	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}


	public Date getPublishSDate() {
		return publishSDate;
	}


	public void setPublishSDate(Date publishSDate) {
		this.publishSDate = publishSDate;
	}


	public Date getPublishEDate() {
		return publishEDate;
	}


	public void setPublishEDate(Date publishEDate) {
		this.publishEDate = publishEDate;
	}


	public String getDiscountPrice() {
		return discountPrice;
	}


	public void setDiscountPrice(String discountPrice) {
		this.discountPrice = discountPrice;
	}


	public String getPrice() {
		return price;
	}


	public void setPrice(String price) {
		this.price = price;
	}


	public String getActive() {
		return active;
	}


	public void setActive(String active) {
		this.active = active;
	}
	
	public String getCategoryType() {
		return categoryType;
	}


	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}
	


	public String getPkProps() {
		return pkProps;
	}


	public void setPkProps(String pkProps) {
		this.pkProps = pkProps;
	}


	public String getPkCourse() {
		return pkCourse;
	}


	public void setPkCourse(String pkCourse) {
		this.pkCourse = pkCourse;
	}


	public String getPkCamp() {
		return pkCamp;
	}


	public void setPkCamp(String pkCamp) {
		this.pkCamp = pkCamp;
	}


	public String getVendorName() {
		return vendorName;
	}


	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}


	public String getPkVendor() {
		return pkVendor;
	}

	public void setPkVendor(String pkVendor) {
		this.pkVendor = pkVendor;
	}


	public boolean isPageReadonly() {
		return isPageReadonly;
	}


	public void setPageReadonly(boolean isPageReadonly) {
		this.isPageReadonly = isPageReadonly;
	}


	public boolean isSdateReadonly() {
		return isSdateReadonly;
	}

	public void setSdateReadonly(boolean isSdateReadonly) {
		this.isSdateReadonly = isSdateReadonly;
	}


	public boolean isOnMarket() {
		return onMarket;
	}


	public void setOnMarket(boolean onMarket) {
		this.onMarket = onMarket;
	}


	public String getWeights() {
		return weights;
	}


	public void setWeights(String weights) {
		this.weights = weights;
	}
	
}