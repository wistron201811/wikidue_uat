package com.e7learning.videoAd.form;

import org.springframework.web.multipart.MultipartFile;

import com.e7learning.common.form.BaseForm;
import com.e7learning.repository.model.VideoAd;

public class VideoAdForm extends BaseForm<VideoAd>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 影片廣告ID
	 */
	private Integer videoAdId;
	/**
	 * 預覽圖片
	 */
	private MultipartFile image;
	
	/**
	 * 影片連結  https://www.youtube.com/watch?v=xxxxxxx
	 * xxxxxxx
	 */
	private String videoUrl;
	
	/**
	 * 影片廣告標題
	 */
	private String title;
	
	/**
	 * 影片廣告介紹
	 */
	private String brief;

	/**
	 * 影片網址
	 */
	private String contentUrl;

	/**
	 * 啟用
	 */
	private String active;

	public Integer getVideoAdId() {
		return videoAdId;
	}

	public void setVideoAdId(Integer videoAdId) {
		this.videoAdId = videoAdId;
	}

	public MultipartFile getImage() {
		return image;
	}

	public void setImage(MultipartFile image) {
		this.image = image;
	}

	public String getVideoUrl() {
		return videoUrl;
	}

	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBrief() {
		return brief;
	}

	public void setBrief(String brief) {
		this.brief = brief;
	}

	public String getContentUrl() {
		return contentUrl;
	}

	public void setContentUrl(String contentUrl) {
		this.contentUrl = contentUrl;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}
	
}
