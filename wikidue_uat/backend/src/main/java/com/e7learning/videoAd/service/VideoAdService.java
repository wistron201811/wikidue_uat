package com.e7learning.videoAd.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.service.FileStorageService;
import com.e7learning.course.service.CourseService;
import com.e7learning.repository.VideoAdRepository;
import com.e7learning.repository.model.VideoAd;
import com.e7learning.videoAd.form.VideoAdForm;

@Service
public class VideoAdService extends BaseService {

	private static final Logger LOG = Logger.getLogger(CourseService.class);
	@Autowired
	private MessageSource msg;

	@Autowired
	private FileStorageService fileStorageService;

	@Autowired
	private ConfigService configService;

	@Autowired
	private VideoAdRepository videoAdRepository;

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void addVideoAd(VideoAdForm form) throws ServiceException {
		try {
			Date now = new Date();
			VideoAd videoAd = new VideoAd();
			videoAd.setActive(form.getActive());
			videoAd.setBrief(form.getBrief());
			videoAd.setContentUrl(form.getContentUrl());
			videoAd.setCreateDt(now);
			videoAd.setCreateId(getCurrentUserId());
			if (form.getImage() != null && !form.getImage().isEmpty()) {
				videoAd.setImageId(fileStorageService.saveFile(form.getImage()).getPkFile());
			}
			videoAd.setTitle(form.getTitle());
			videoAd.setUpdateDt(now);
			videoAd.setVideoUrl(form.getVideoUrl());

			videoAdRepository.save(videoAd);
		} catch (Exception e) {
			LOG.error("addVideoAd", e);
			throw new ServiceException(msg.getMessage("videoAd.create.error", null, null));
		}

		throw new MessageException(msg.getMessage("videoAd.create.success", null, null));
	}

	public Page<VideoAd> query(VideoAdForm form, int pageNo) throws ServiceException {
		try {
			Pageable pageable = new PageRequest(pageNo - 1, configService.getPageSize());
			return videoAdRepository.findAll(buildOperSpecification(form), pageable);
		} catch (Exception e) {
			LOG.error("exception", e);
			throw new ServiceException(msg.getMessage("videoAd.query.error", null, null));
		}
	}

	public VideoAdForm getEditForm(Integer videoAdId) throws ServiceException {
		try {
			VideoAd videoAd = videoAdRepository.findOne(videoAdId);
			if (videoAd == null) {
				throw new ServiceException(msg.getMessage("videoAd.query.error", null, null));
			}
			VideoAdForm form = new VideoAdForm();
			form.setActive(videoAd.getActive());
			form.setBrief(videoAd.getBrief());
			form.setContentUrl(videoAd.getContentUrl());
			form.setImage(fileStorageService.getFileStorage(videoAd.getImageId()).orElse(null));
			form.setTitle(videoAd.getTitle());
			form.setVideoUrl(videoAd.getVideoUrl());
			form.setVideoAdId(videoAdId);
			return form;
		} catch (ServiceException e) {
			LOG.error("exception", e);
			throw e;
		} catch (Exception e) {
			LOG.error("exception", e);
			throw new ServiceException(msg.getMessage("videoAd.query.error", null, null));
		}
	}

	public void modifyVideoAd(VideoAdForm form) throws ServiceException {
		try {
			VideoAd videoAd = videoAdRepository.findOne(form.getVideoAdId());
			if (videoAd == null) {
				throw new ServiceException(msg.getMessage("videoAd.modify.error", null, null));
			}

			Date now = new Date();
			videoAd.setActive(form.getActive());
			videoAd.setBrief(form.getBrief());
			videoAd.setContentUrl(form.getContentUrl());
			if (form.getImage() != null && !form.getImage().isEmpty()) {
				fileStorageService.delFile(videoAd.getImageId());
				videoAd.setImageId(fileStorageService.saveFile(form.getImage()).getPkFile());
			}
			videoAd.setTitle(form.getTitle());
			videoAd.setVideoUrl(form.getVideoUrl());
			videoAd.setUpdateDt(now);
			videoAdRepository.save(videoAd);

		} catch (ServiceException e) {
			LOG.error("exception", e);
			throw e;
		} catch (Exception e) {
			LOG.error("exception", e);
			throw new ServiceException(msg.getMessage("videoAd.modify.error", null, null));
		}
		throw new MessageException(msg.getMessage("videoAd.modify.success", null, null));
	}

	public void removeVideoAd(Integer videoAdId) throws ServiceException {
		try {
			if (videoAdId == null) {
				throw new ServiceException(msg.getMessage("videoAd.remove.error", null, null));
			}
			VideoAd videoAd = videoAdRepository.findOne(videoAdId);
			if (videoAd != null) {
				fileStorageService.delFile(videoAd.getImageId());
			}
			videoAdRepository.delete(videoAdId);

		} catch (ServiceException e) {
			LOG.error("exception", e);
			throw e;
		} catch (Exception e) {
			LOG.error("exception", e);
			throw new ServiceException(msg.getMessage("videoAd.remove.error", null, null));
		}
		throw new MessageException(msg.getMessage("videoAd.remove.success", null, null));
	}

	private Specification<VideoAd> buildOperSpecification(VideoAdForm form) {
		return (Root<VideoAd> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
			List<Predicate> predicates = new ArrayList<>();
			if (!StringUtils.isEmpty(form.getTitle())) {
				Path<String> np = root.get("title");
				predicates.add(cb.like(np, "%" + form.getTitle() + "%"));
			}
			if (!predicates.isEmpty()) {
				return cb.and(predicates.toArray(new Predicate[predicates.size()]));
			}
			return cb.conjunction();

		};
	}
}
