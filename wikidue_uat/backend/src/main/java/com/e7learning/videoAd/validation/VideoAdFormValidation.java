package com.e7learning.videoAd.validation;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.e7learning.videoAd.form.VideoAdForm;

@Component
public class VideoAdFormValidation implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(VideoAdForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {

		VideoAdForm form = (VideoAdForm) target;
		if (StringUtils.equalsIgnoreCase("add", form.getMethod())
				|| StringUtils.equalsIgnoreCase("modify", form.getMethod())) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "brief", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "contentUrl", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "videoUrl", "validate.empty.message");


			if (StringUtils.equalsIgnoreCase("add", form.getMethod()) && form.getImage().isEmpty()) {
				errors.rejectValue("image", "validate.empty.message");
			}

			if (errors.hasErrors()) {
				return;
			}

			// 檢查格式
			UrlValidator urlValidator = new UrlValidator();
			if (!urlValidator.isValid(form.getContentUrl())) {
				errors.rejectValue("contentUrl", "validate.url.format");
			}


		}

		if (errors.hasErrors()) {
			return;
		}
	}
}