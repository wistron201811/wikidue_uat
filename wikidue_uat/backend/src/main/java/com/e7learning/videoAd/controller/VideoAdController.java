package com.e7learning.videoAd.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.e7learning.common.controller.BaseController;
import com.e7learning.videoAd.form.VideoAdForm;
import com.e7learning.videoAd.service.VideoAdService;
import com.e7learning.videoAd.validation.VideoAdFormValidation;

@Controller
@RequestMapping(value = "/videoAd")
@SessionAttributes(VideoAdController.FORM)
public class VideoAdController extends BaseController {

	private static final Logger LOG = Logger.getLogger(VideoAdController.class);

	private static final String ADD_VIDEOAD_PAGE = "addVideoAdPage";

	private static final String QUERY_VIDEOAD_PAGE = "queryVideoAdPage";

	private static final String MODIFY_VIDEOAD_PAGE = "modifyVideoAdPage";

	public static final String FORM = "videoAdForm";
	
	@Autowired
	private VideoAdFormValidation videoAdFormValidation;
	
	@Autowired
	private VideoAdService videoAdService;


	@InitBinder(FORM)
	protected void initBinder(WebDataBinder binder, HttpSession session) {
		binder.setValidator(videoAdFormValidation);
	}

	@RequestMapping(value = "/add", method = { RequestMethod.GET })
	public String goAdd(Model model) {
		model.addAttribute(FORM, new VideoAdForm());
		return ADD_VIDEOAD_PAGE;
	}

	@RequestMapping(params = "method=add", value = "/add", method = { RequestMethod.POST })
	public String add(Model model, @Validated @ModelAttribute(FORM) VideoAdForm form, BindingResult bindingResult) {
		try {
			if (!bindingResult.hasErrors()) {
				videoAdService.addVideoAd(form);
				return "redirect:/videoAd/query";
			}
		} catch (Exception e) {
		}

		return ADD_VIDEOAD_PAGE;
	}

	@RequestMapping(value = "/goQuery", method = { RequestMethod.GET })
	public String goQuery(Model model) {
		model.addAttribute(FORM, new VideoAdForm());
		return QUERY_VIDEOAD_PAGE;
	}

	@RequestMapping(value = "/query")
	public String query(Model model, VideoAdForm form, HttpServletRequest request) {
		try {
			String pageName = new ParamEncoder("videoAd").encodeParameterName(TableTagParameters.PARAMETER_PAGE);
			int nextPage = getPage(request.getParameter(pageName));
			form.setResult(videoAdService.query(form, nextPage));
		} catch (Exception e) {
		}
		return QUERY_VIDEOAD_PAGE;
	}

	@RequestMapping(value = "/modify", method = { RequestMethod.POST })
	public String goModify(Model model, VideoAdForm form) {
		try {
			model.addAttribute(FORM, videoAdService.getEditForm(form.getVideoAdId()));
		} catch (Exception e) {
			return "redirect:/videoAd/query";
		}

		return MODIFY_VIDEOAD_PAGE;
	}

	@RequestMapping(params = "method=modify", value = "/modify", method = { RequestMethod.POST })
	public String modify(@Validated VideoAdForm form, BindingResult result) {

		try {
			if (!result.hasErrors()) {
				videoAdService.modifyVideoAd(form);
				return "redirect:/videoAd/query";
			}
		} catch (Exception e) {

		}
		return MODIFY_VIDEOAD_PAGE;
	}

	@RequestMapping(params = "method=remove", value = "/query", method = { RequestMethod.POST })
	public String remove(VideoAdForm form) {
		try {
			videoAdService.removeVideoAd(form.getVideoAdId());

		} catch (Exception e) {

		}
		return "redirect:/videoAd/query";
	}

}
