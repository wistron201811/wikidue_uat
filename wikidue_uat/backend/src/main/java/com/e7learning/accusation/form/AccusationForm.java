package com.e7learning.accusation.form;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.e7learning.common.form.BaseForm;
import com.e7learning.repository.model.Accusation;

/**
 * @author J
 * 
 * 分享園地管理
 *
 */
public class AccusationForm extends BaseForm<Accusation>{
	/**
	 * 檢舉PK
	 */
	private int pkAccusation;
	/**
	 * 狀態
	 */
	private String approveStatus;
	/** 
	* 查詢狀態
	 */
	private String queryStatus;
	/**
	 * 檢舉時間
	 */
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date createDt;
	
	/**
	 * 檢舉時間
	 */
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date createDtEnd;
	
	public int getPkAccusation() {
		return pkAccusation;
	}
	public void setPkAccusation(int pkAccusation) {
		this.pkAccusation = pkAccusation;
	}
	public Date getCreateDt() {
		return createDt;
	}
	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}
	public String getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	public String getQueryStatus() {
		return queryStatus;
	}
	public void setQueryStatus(String queryStatus) {
		this.queryStatus = queryStatus;
	}
	public Date getCreateDtEnd() {
		return createDtEnd;
	}
	public void setCreateDtEnd(Date createDtEnd) {
		this.createDtEnd = createDtEnd;
	}
}
