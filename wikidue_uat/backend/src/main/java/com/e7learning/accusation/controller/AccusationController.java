package com.e7learning.accusation.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.e7learning.accusation.form.AccusationForm;
import com.e7learning.accusation.service.AccusationService;
import com.e7learning.common.controller.BaseController;
import com.e7learning.common.exception.ServiceException;

/**
 * @author J
 *
 */
@Controller
@RequestMapping(value = "/accusation")
@SessionAttributes(AccusationController.FORM_BEAN_NAME)
public class AccusationController extends BaseController {

	private static final Logger log = Logger.getLogger(AccusationController.class);

	public static final String FORM_BEAN_NAME = "accusationForm";
	
	private static final String QUERY_PAGE = "queryAccusationPage";

	@Autowired
	private AccusationService accusationService ;
	
	@RequestMapping(value = "/query", method = { RequestMethod.GET })
	public String goQuery(HttpServletRequest request,Model model) {
		AccusationForm form = new AccusationForm();
		try {
			form.setResult(accusationService.queryAccusation(form, 1));
		} catch (ServiceException e) {
			
		}
		model.addAttribute(FORM_BEAN_NAME, form);
		return QUERY_PAGE;
	}

	@RequestMapping(value = "/query", params = "method=query")
	public String query(HttpServletRequest request, AccusationForm form, Model model) {
		String pageName = new ParamEncoder("list").encodeParameterName(TableTagParameters.PARAMETER_PAGE);
		int nextPage = getPage(request.getParameter(pageName));
		try {
			form.setResult(accusationService.queryAccusation(form, nextPage));
		} catch (ServiceException e) {
			
		}
		return QUERY_PAGE;
	}
	
	@RequestMapping(params = "method=modify", value = "/modify", method = { RequestMethod.POST })
	public String modify(AccusationForm form) {
		try {
			accusationService.saveAccusation(form);
			return "redirect:/accusation/query?method=query";
		} catch (ServiceException e) {
			
		}
		return QUERY_PAGE;
	}
	
}
