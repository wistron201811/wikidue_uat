package com.e7learning.accusation.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.accusation.form.AccusationForm;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.utils.CommonUtils;
import com.e7learning.repository.AccusationRepository;
import com.e7learning.repository.model.Accusation;
import com.e7learning.repository.model.ShareCenter;

/**
 * @author J
 *
 */
@Service
public class AccusationService extends BaseService {

	private static final Logger log = Logger.getLogger(AccusationService.class);

	@Autowired
	private AccusationRepository accusationRepository;
	
	@Autowired
	private ConfigService configService;

	@Autowired
	private MessageSource resources;

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void saveAccusation(AccusationForm form) throws ServiceException {
		try {
			Accusation accusation = accusationRepository.findOne(form.getPkAccusation());
			if (accusation != null) {
				accusation.setApproveStatus(form.getApproveStatus());
				accusation.setUpdateDt(new Date());
				accusation.setLastModifiedId(getCurrentUserId());
				accusation.getShareCenter().setApproveStatus(form.getApproveStatus());
				accusation.getShareCenter().setUpdateDt(new Date());
				accusation.getShareCenter().setLastModifiedId(getCurrentUserId());
			}
		} catch (Exception e) {
			log.error("saveAccusation", e);
			throw new ServiceException(resources.getMessage("op.modify.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.modify.success", null, null));
	}

	public Page<Accusation> queryAccusation(AccusationForm form, Integer page) throws ServiceException {
		try {
			Sort sort = new Sort(Direction.ASC, "createDt");
			Pageable pageable = new PageRequest(page - 1, configService.getPageSize(), sort);
			Page<Accusation> result = accusationRepository.findAll(buildOperSpecification(form), pageable);
			return result;
		} catch (Exception e) {
			log.error("queryAccusation", e);
			throw new ServiceException(resources.getMessage("op.query.error", null, null));
		}
	}

	private Specification<Accusation> buildOperSpecification(AccusationForm form) {
		return new Specification<Accusation>() {
			@Override
			public Predicate toPredicate(Root<Accusation> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

				List<Predicate> predicates = new ArrayList<>();
				
				Join<Accusation,ShareCenter> shareCenterJoin = root.join(root.getModel().getSingularAttribute("shareCenter",ShareCenter.class),JoinType.LEFT);
                predicates.add(cb.equal(shareCenterJoin.get("active").as(String.class), "1"));
                
				if (StringUtils.isNotEmpty(form.getQueryStatus())) {
					Path<String> np = root.get("approveStatus");
					String status=form.getQueryStatus();
					if(StringUtils.isBlank(status)) {
						predicates.add(cb.isNull(np));
					}else {
						predicates.add(cb.equal(np, status));
					}
				}
				if (form.getCreateDt() != null) {
					Path<Date> np = root.get("createDt");
					Date sDate=form.getCreateDt();
					sDate = CommonUtils.getDateWithoutTimes(sDate);
					predicates.add(cb.greaterThanOrEqualTo(np,sDate));
				}
				if (form.getCreateDtEnd() != null) {
					Path<Date> np = root.get("createDt");
					Date eDate=form.getCreateDtEnd();
					eDate = CommonUtils.getDateEndWithTimes(eDate);
					predicates.add(cb.lessThanOrEqualTo(np,eDate));
				}
				if (!predicates.isEmpty()) {
					return cb.and(predicates.toArray(new Predicate[predicates.size()]));
				}
				return cb.conjunction();
			}
		};
	}
}
