package com.e7learning.common.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import freemarker.template.Configuration;

@Service
public class TemplateService {

	private static final Logger log = Logger.getLogger(TemplateService.class);

	@Autowired
	private Configuration freemarkerConfiguration;

	public String getVideoUploadSuccess(String courseName, String storeUrl, String imageUrl, String sendDate,
			List<Map<String, String>> details) {

		Map<String, Object> map = new HashMap<>();
		map.put("courseName", courseName);
		map.put("storeUrl", storeUrl);
		map.put("imageUrl", imageUrl);
		map.put("sendDate", sendDate);
		map.put("details", details);

		try {
			StringBuilder content = new StringBuilder();

			content.append(FreeMarkerTemplateUtils.processTemplateIntoString(
					freemarkerConfiguration.getTemplate("videoUploadSuccess.html", "UTF-8"), map));
			return content.toString();
		} catch (Exception e) {
			log.error("Exception occured while processing fmtemplate:" + e.getMessage());
			return StringUtils.EMPTY;
		}
	}

	public String getVideoUploadFailed(String courseName, String storeUrl, String imageUrl, String sendDate,
			List<Map<String, String>> details) {

		Map<String, Object> map = new HashMap<>();
		map.put("courseName", courseName);
		map.put("storeUrl", storeUrl);
		map.put("imageUrl", imageUrl);
		map.put("sendDate", sendDate);
		map.put("details", details);

		try {
			StringBuilder content = new StringBuilder();

			content.append(FreeMarkerTemplateUtils.processTemplateIntoString(
					freemarkerConfiguration.getTemplate("videoUploadFailed.html", "UTF-8"), map));
			return content.toString();
		} catch (Exception e) {
			log.error("Exception occured while processing fmtemplate:" + e.getMessage());
			return StringUtils.EMPTY;
		}
	}

	/**
	 * 退貨成功通知信-買家
	 * 
	 * @param buyerName
	 * @param refundDate
	 * @param details
	 * @param orderNo
	 * @param totalAmt
	 * @return
	 */
	public String getRefundBuyerNotification(String storeUrl, String sendDate, String buyerName, String myOrderUrl,
			String orderNo, List<Map<String, String>> details, String imageUrl, String serviceUrl) {
		Map<String, Object> map = new HashMap<>();
		map.put("storeUrl", storeUrl);
		map.put("sendDate", sendDate);
		map.put("buyerName", buyerName);
		map.put("myOrderUrl", myOrderUrl);
		map.put("orderNo", orderNo);
		map.put("details", details);
		map.put("imageUrl", imageUrl);
		map.put("serviceUrl", serviceUrl);
		try {
			StringBuilder content = new StringBuilder();

			content.append(FreeMarkerTemplateUtils
					.processTemplateIntoString(freemarkerConfiguration.getTemplate("mail_61.html", "UTF-8"), map));
			return content.toString();
		} catch (Exception e) {
			log.error("Exception occured while processing fmtemplate:" + e.getMessage());
			return StringUtils.EMPTY;
		}
	}

	/**
	 * 退貨成功通知信-廠商
	 * 
	 * @param vendorName
	 * @param buyerName
	 * @param details
	 * @param refundDate
	 * @param buyerPhone
	 * @param buyerAddress
	 * @return
	 */
	public String getRefundVendorNotification(String storeUrl, String adminName, String orderNo, String refundDate,
			String backendUrl, List<Map<String, String>> details, String imageUrl, String sendDate,
			String backendManualUrl, String serviceUrl) {
		try {
			StringBuilder content = new StringBuilder();
			Map<String, Object> map = new HashMap<>();
			map.put("storeUrl", storeUrl);
			map.put("adminName", adminName);
			map.put("orderNo", orderNo);
			map.put("refundDate", refundDate);
			map.put("backendUrl", backendUrl);
			map.put("details", details);
			map.put("imageUrl", imageUrl);
			map.put("sendDate", sendDate);
			map.put("backendManualUrl", backendManualUrl);
			map.put("serviceUrl", serviceUrl);

			content.append(FreeMarkerTemplateUtils
					.processTemplateIntoString(freemarkerConfiguration.getTemplate("mail_62.html", "UTF-8"), map));
			return content.toString();
		} catch (Exception e) {
			log.error("Exception occured while processing fmtemplate:" + e.getMessage());
			return StringUtils.EMPTY;
		}
	}

	/**
	 * 庫存不足通知信
	 * 
	 * @return
	 */
	public String getInventoryNotification(String storeUrl, String sendDate, String vendorName, String noticeDate,
			String backendUrl, List<Map<String, String>> details, String imageUrl, String backendManualUrl,
			String serviceUrl, Integer quantityLimit) {
		try {
			StringBuilder content = new StringBuilder();
			Map<String, Object> map = new HashMap<>();
			map.put("storeUrl", storeUrl);
			map.put("sendDate", sendDate);
			map.put("vendorName", vendorName);
			map.put("noticeDate", noticeDate);
			map.put("backendUrl", backendUrl);
			map.put("details", details);
			map.put("imageUrl", imageUrl);
			map.put("backendManualUrl", backendManualUrl);
			map.put("serviceUrl", serviceUrl);
			map.put("quantityLimit", quantityLimit);

			content.append(FreeMarkerTemplateUtils
					.processTemplateIntoString(freemarkerConfiguration.getTemplate("mail_31.html", "UTF-8"), map));
			return content.toString();
		} catch (Exception e) {
			log.error("Exception occured while processing fmtemplate:" + e.getMessage());
			return StringUtils.EMPTY;
		}
	}

	/**
	 * 收到退貨通知信
	 * 
	 * @return
	 */
	public String getReceiveRefundNotification(String storeUrl, String sendDate, String adminName, String orderNo,
			String refundDate, String backendUrl, List<Map<String, String>> details, String total, String recipient,
			String telPhone, String mobile, String address, String imageUrl, String backendManualUrl, String serviceUrl,
			boolean mail) {
		try {
			StringBuilder content = new StringBuilder();
			Map<String, Object> map = new HashMap<>();
			map.put("storeUrl", storeUrl);
			map.put("sendDate", sendDate);
			map.put("adminName", adminName);
			map.put("orderNo", orderNo);
			map.put("refundDate", refundDate);
			map.put("backendUrl", backendUrl);
			map.put("details", details);
			map.put("total", total);
			map.put("recipient", recipient);
			map.put("telPhone", telPhone);
			map.put("mobile", mobile);
			map.put("address", address);
			map.put("imageUrl", imageUrl);
			map.put("backendManualUrl", backendManualUrl);
			map.put("serviceUrl", serviceUrl);
			map.put("mail", mail);

			content.append(FreeMarkerTemplateUtils
					.processTemplateIntoString(freemarkerConfiguration.getTemplate("mail_52.html", "UTF-8"), map));
			return content.toString();
		} catch (Exception e) {
			log.error("Exception occured while processing fmtemplate:" + e.getMessage());
			return StringUtils.EMPTY;
		}
	}

	/**
	 * 拒絕退貨通知信
	 * 
	 * @return
	 */
	public String getRefuseRefundNotification(String storeUrl, String imageUrl, String sendDate, String adminName,
			String orderNo, String backendUrl, List<Map<String, String>> details, String total, String backendManualUrl,
			String serviceUrl) {
		try {
			StringBuilder content = new StringBuilder();
			Map<String, Object> map = new HashMap<>();
			map.put("storeUrl", storeUrl);
			map.put("imageUrl", imageUrl);
			map.put("sendDate", sendDate);
			map.put("adminName", adminName);
			map.put("orderNo", orderNo);
			map.put("backendUrl", backendUrl);
			map.put("details", details);
			map.put("total", total);
			map.put("backendManualUrl", backendManualUrl);
			map.put("serviceUrl", serviceUrl);

			content.append(FreeMarkerTemplateUtils
					.processTemplateIntoString(freemarkerConfiguration.getTemplate("mail_53.html", "UTF-8"), map));
			return content.toString();
		} catch (Exception e) {
			log.error("Exception occured while processing fmtemplate:" + e.getMessage());
			return StringUtils.EMPTY;
		}
	}

	/**
	 * 出貨通知信-買家
	 * 
	 * @return
	 */
	public String getSendNotificationBuyer(String storeUrl, String sendDate, String buyerName, String shipDate,
			String myOrderUrl, String orderNo, List<Map<String, String>> details, String imageUrl) {
		try {
			StringBuilder content = new StringBuilder();
			Map<String, Object> map = new HashMap<>();
			map.put("storeUrl", storeUrl);
			map.put("sendDate", sendDate);
			map.put("buyerName", buyerName);
			map.put("shipDate", shipDate);
			map.put("myOrderUrl", myOrderUrl);
			map.put("orderNo", orderNo);
			map.put("details", details);
			map.put("imageUrl", imageUrl);

			content.append(FreeMarkerTemplateUtils
					.processTemplateIntoString(freemarkerConfiguration.getTemplate("mail_21.html", "UTF-8"), map));
			return content.toString();
		} catch (Exception e) {
			log.error("Exception occured while processing fmtemplate:" + e.getMessage());
			return StringUtils.EMPTY;
		}
	}

	/**
	 * 出貨通知信-廠商
	 * 
	 * @return
	 */
	public String getSendNotificationAdmin(String storeUrl, String sendDate, String adminName, String orderNo,
			String shipDate, String backendUrl, List<Map<String, String>> details, String imageUrl) {
		try {
			StringBuilder content = new StringBuilder();
			Map<String, Object> map = new HashMap<>();
			map.put("storeUrl", storeUrl);
			map.put("sendDate", sendDate);
			map.put("adminName", adminName);
			map.put("orderNo", orderNo);
			map.put("shipDate", shipDate);
			map.put("backendUrl", backendUrl);
			map.put("details", details);
			map.put("imageUrl", imageUrl);

			content.append(FreeMarkerTemplateUtils
					.processTemplateIntoString(freemarkerConfiguration.getTemplate("mail_22.html", "UTF-8"), map));
			return content.toString();
		} catch (Exception e) {
			log.error("Exception occured while processing fmtemplate:" + e.getMessage());
			return StringUtils.EMPTY;
		}
	}

	/**
	 * 退貨處理中通知信
	 * 
	 * @return
	 */
	public String getRefundProcessNotification(String storeUrl, String vendorName, String orderNo, String refundDate,
			String backendUrl, List<Map<String, String>> details, String total, String recipient, String telPhone,
			String mobile, String address, String imageUrl, String sendDate, String backendManualUrl, String serviceUrl,
			boolean mail) {
		try {
			StringBuilder content = new StringBuilder();
			Map<String, Object> map = new HashMap<>();
			map.put("storeUrl", storeUrl);
			map.put("vendorName", vendorName);
			map.put("orderNo", orderNo);
			map.put("refundDate", refundDate);
			map.put("backendUrl", backendUrl);
			map.put("details", details);
			map.put("total", total);
			map.put("recipient", recipient);
			map.put("telPhone", telPhone);
			map.put("mobile", mobile);
			map.put("address", address);
			map.put("imageUrl", imageUrl);
			map.put("sendDate", sendDate);
			map.put("backendManualUrl", backendManualUrl);
			map.put("serviceUrl", serviceUrl);
			map.put("mail", mail);

			content.append(FreeMarkerTemplateUtils
					.processTemplateIntoString(freemarkerConfiguration.getTemplate("mail_51.html", "UTF-8"), map));
			return content.toString();
		} catch (Exception e) {
			log.error("Exception occured while processing fmtemplate:" + e.getMessage());
			return StringUtils.EMPTY;
		}
	}

	/**
	 * 48h未出貨通知信
	 * 
	 * @return
	 */
	public String getUnsendNotification(String storeUrl, String vendorName, String buyerName, String orderNo,
			String orderDate, String backendUrl, List<Map<String, String>> details, String total, String recipient,
			String telPhone, String mobile, String address, String imageUrl, String sendDate, String backendManualUrl,
			String serviceUrl) {
		try {
			StringBuilder content = new StringBuilder();
			Map<String, Object> map = new HashMap<>();
			map.put("storeUrl", storeUrl);
			map.put("vendorName", vendorName);
			map.put("buyerName", buyerName);
			map.put("orderNo", orderNo);
			map.put("orderDate", orderDate);
			map.put("backendUrl", backendUrl);
			map.put("details", details);
			map.put("total", total);
			map.put("recipient", recipient);
			map.put("telPhone", telPhone);
			map.put("mobile", mobile);
			map.put("address", address);
			map.put("imageUrl", imageUrl);
			map.put("sendDate", sendDate);
			map.put("backendManualUrl", backendManualUrl);
			map.put("serviceUrl", serviceUrl);

			content.append(FreeMarkerTemplateUtils
					.processTemplateIntoString(freemarkerConfiguration.getTemplate("mail_11.html", "UTF-8"), map));
			return content.toString();
		} catch (Exception e) {
			log.error("Exception occured while processing fmtemplate:" + e.getMessage());
			return StringUtils.EMPTY;
		}
	}
}
