package com.e7learning.common.taglib;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class HtmlPreviewTag extends SimpleTagSupport {

	/***/
	private static final Logger LOG = Logger.getLogger(HtmlPreviewTag.class);

	private String htmlBody;

	@Override
	public void doTag() {
		try {
			PageContext pageContext = (PageContext) getJspContext();
			ServletContext servletContext = pageContext.getServletContext();

			StringBuffer html = new StringBuffer();

			if (StringUtils.isNotBlank(htmlBody)) {
				Document data = Jsoup.parse(htmlBody);
				Elements images = data.select("img");
				for (Element img : images) {
					String imgSrc = img.attr("src");
					if (StringUtils.isNotBlank(imgSrc) && imgSrc.length() == 32) {
						img.attr("src", servletContext.getContextPath() + "/common/query/image/" + imgSrc);
					}
				}
				html.append(data.toString());
			}

			JspWriter out = getJspContext().getOut();
			out.print(html.toString());
		} catch (Exception e) {
			LOG.error("Exception:", e);
		}
	}

	public void setHtmlBody(String htmlBody) {
		this.htmlBody = htmlBody;
	}

}
