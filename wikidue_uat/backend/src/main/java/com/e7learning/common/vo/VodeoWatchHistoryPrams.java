package com.e7learning.common.vo;

import java.util.Date;

public class VodeoWatchHistoryPrams {
	
	private String accountName;
	
	private String uid;
	
	private String hbID;
	
	private Date begin;

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public Date getBegin() {
		return begin;
	}

	public void setBegin(Date begin) {
		this.begin = begin;
	}

	public String getHbID() {
		return hbID;
	}

	public void setHbID(String hbID) {
		this.hbID = hbID;
	}
	
	
	

}
