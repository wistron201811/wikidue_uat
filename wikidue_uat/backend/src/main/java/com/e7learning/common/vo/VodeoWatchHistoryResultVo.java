package com.e7learning.common.vo;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

public class VodeoWatchHistoryResultVo {

	private String result;
	
	private String msg;
	
	private List<VodeoWatchHistoryVo> hbIDList;
	
	private int status;
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date lastParseTime;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public List<VodeoWatchHistoryVo> getHbIDList() {
		return hbIDList;
	}

	public void setHbIDList(List<VodeoWatchHistoryVo> hbIDList) {
		this.hbIDList = hbIDList;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	public Date getLastParseTime() {
		return lastParseTime;
	}
	
	public void setLastParseTime(Date lastParseTime) {
		this.lastParseTime = lastParseTime;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	
}
