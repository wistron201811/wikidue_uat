package com.e7learning.common.schedule;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.KnowledgeMapService;
import com.e7learning.common.service.VideoPlatformService;
import com.e7learning.po.service.MainOrderService;
import com.e7learning.product.service.ProductService;

@Service
public class ScheduleService {

	@Autowired
	private KnowledgeMapService kmService;

	@Autowired
	private MainOrderService mainOrderService;

	@Autowired
	private ProductService productService;
	
	@Autowired
	private VideoPlatformService videoPlatformService;

	@Autowired
	private static final Logger log = Logger.getLogger(ScheduleService.class);

	// 處理訂單逾期
	@Scheduled(cron = "${cancel.po.schedule}")
	public void cancelPO() {
		log.info("訂單排程啟動");
		mainOrderService.cancelOverDuePo();
	}

	// 處理訂單逾期更新知識地圖
	@Scheduled(cron = "${update.km.schedule}")
	public void updateKM() {
		try {
			log.info("更新知識地圖…");
			kmService.getKnowledgeMap("rebuildDB");
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

	// 寄送48小時未出貨通知
	@Scheduled(cron = "${unsend.notice.schedule}")
	public void unsendNotice() {
		try {
			log.info("未出貨商品檢查...");
			mainOrderService.unsendNotice();
		} catch (Exception e) {
			log.error("unsendNotice", e);
		}
	}

	// 寄送商品存貨低庫存通知
	@Scheduled(cron = "${inventory.notice.schedule}")
	public void inventoryNotice() {
		try {
			log.info("商品存貨低庫存檢查...");
			productService.inventoryNotice();
		} catch (Exception e) {
			log.error("inventoryNotice", e);
		}
	}

	
	/**
	 * 更新觀看流量
	 */
	@Scheduled(cron = "${update.watch.data.schedule}")
	public void updateWatchData() {
		try {
			log.info("更新觀看流量...");
			videoPlatformService.updateVideoWatchHistory();
		} catch (Exception e) {
			log.error("updateWatchData", e);
		}
	}

}
