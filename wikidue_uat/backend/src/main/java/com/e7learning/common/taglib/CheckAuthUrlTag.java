package com.e7learning.common.taglib;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.expression.BeanResolver;
import org.springframework.expression.ConstructorResolver;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.MethodResolver;
import org.springframework.expression.OperatorOverloader;
import org.springframework.expression.PropertyAccessor;
import org.springframework.expression.TypeComparator;
import org.springframework.expression.TypeConverter;
import org.springframework.expression.TypeLocator;
import org.springframework.expression.TypedValue;
import org.springframework.security.access.expression.SecurityExpressionHandler;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.taglibs.TagLibConfig;
import org.springframework.security.taglibs.authz.AbstractAuthorizeTag;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.access.WebInvocationPrivilegeEvaluator;
import org.springframework.security.web.context.support.SecurityWebApplicationContextUtils;

public class CheckAuthUrlTag extends AbstractAuthorizeTag implements Tag {

	private Tag parent;

	protected PageContext pageContext;

	protected String id;

	private String var;

	private String urls;

	private boolean authorized;

	/**
	 * Invokes the base class {@link AbstractAuthorizeTag#authorize()} method to
	 * decide if the body of the tag should be skipped or not.
	 *
	 * @return {@link Tag#SKIP_BODY} or {@link Tag#EVAL_BODY_INCLUDE}
	 */
	public int doStartTag() throws JspException {
		try {
			authorized = StringUtils.isNotBlank(getUrls()) ? authorizeUsingUrlsCheck(urls.split(","))
					: super.authorize();
			if (!authorized && TagLibConfig.isUiSecurityDisabled()) {
				pageContext.getOut().write(TagLibConfig.getSecuredUiPrefix());
			}

			if (var != null) {
				pageContext.setAttribute(var, authorized, PageContext.PAGE_SCOPE);
			}

			return TagLibConfig.evalOrSkip(authorized);

		} catch (IOException e) {
			throw new JspException(e);
		}
	}

	@Override
	protected EvaluationContext createExpressionEvaluationContext(SecurityExpressionHandler<FilterInvocation> handler) {
		return new PageContextVariableLookupEvaluationContext(super.createExpressionEvaluationContext(handler));
	}

	/**
	 * Default processing of the end tag returning EVAL_PAGE.
	 *
	 * @return EVAL_PAGE
	 * @see Tag#doEndTag()
	 */
	public int doEndTag() throws JspException {
		try {
			if (!authorized && TagLibConfig.isUiSecurityDisabled()) {
				pageContext.getOut().write(TagLibConfig.getSecuredUiSuffix());
			}
		} catch (IOException e) {
			throw new JspException(e);
		}

		return EVAL_PAGE;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Tag getParent() {
		return parent;
	}

	public void setParent(Tag parent) {
		this.parent = parent;
	}

	public String getVar() {
		return var;
	}

	public void setVar(String var) {
		this.var = var;
	}

	public void release() {
		parent = null;
		id = null;
	}

	public void setPageContext(PageContext pageContext) {
		this.pageContext = pageContext;
	}

	@Override
	protected ServletRequest getRequest() {
		return pageContext.getRequest();
	}

	@Override
	protected ServletResponse getResponse() {
		return pageContext.getResponse();
	}

	@Override
	protected ServletContext getServletContext() {
		return pageContext.getServletContext();
	}

	private final class PageContextVariableLookupEvaluationContext implements EvaluationContext {

		private EvaluationContext delegate;

		private PageContextVariableLookupEvaluationContext(EvaluationContext delegate) {
			this.delegate = delegate;
		}

		public TypedValue getRootObject() {
			return delegate.getRootObject();
		}

		public List<ConstructorResolver> getConstructorResolvers() {
			return delegate.getConstructorResolvers();
		}

		public List<MethodResolver> getMethodResolvers() {
			return delegate.getMethodResolvers();
		}

		public List<PropertyAccessor> getPropertyAccessors() {
			return delegate.getPropertyAccessors();
		}

		public TypeLocator getTypeLocator() {
			return delegate.getTypeLocator();
		}

		public TypeConverter getTypeConverter() {
			return delegate.getTypeConverter();
		}

		public TypeComparator getTypeComparator() {
			return delegate.getTypeComparator();
		}

		public OperatorOverloader getOperatorOverloader() {
			return delegate.getOperatorOverloader();
		}

		public BeanResolver getBeanResolver() {
			return delegate.getBeanResolver();
		}

		public void setVariable(String name, Object value) {
			delegate.setVariable(name, value);
		}

		public Object lookupVariable(String name) {
			Object result = delegate.lookupVariable(name);

			if (result == null) {
				result = pageContext.findAttribute(name);
			}
			return result;
		}
	}

	public String getUrls() {
		return urls;
	}

	public void setUrls(String urls) {
		this.urls = urls;
	}

	private WebInvocationPrivilegeEvaluator getPrivilegeEvaluator() throws IOException {
		WebInvocationPrivilegeEvaluator privEvaluatorFromRequest = (WebInvocationPrivilegeEvaluator) getRequest()
				.getAttribute(WebAttributes.WEB_INVOCATION_PRIVILEGE_EVALUATOR_ATTRIBUTE);
		if (privEvaluatorFromRequest != null) {
			return privEvaluatorFromRequest;
		}

		ApplicationContext ctx = SecurityWebApplicationContextUtils
				.findRequiredWebApplicationContext(getServletContext());
		Map<String, WebInvocationPrivilegeEvaluator> wipes = ctx.getBeansOfType(WebInvocationPrivilegeEvaluator.class);

		if (wipes.size() == 0) {
			throw new IOException(
					"No visible WebInvocationPrivilegeEvaluator instance could be found in the application "
							+ "context. There must be at least one in order to support the use of URL access checks in 'authorize' tags.");
		}

		return (WebInvocationPrivilegeEvaluator) wipes.values().toArray()[0];
	}

	public boolean authorizeUsingUrlsCheck(String[] urls) throws IOException {
		String contextPath = ((HttpServletRequest) getRequest()).getContextPath();
		Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();

		boolean allowed = false;
		for (String url : urls) {
			if (getPrivilegeEvaluator().isAllowed(contextPath, url, getMethod(), currentUser)) {
				allowed = true;
				break;
			}
		}
		return allowed;
	}

}