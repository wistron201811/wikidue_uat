package com.e7learning.common.taglib;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.e7learning.common.Constant;
import com.e7learning.common.enums.AdCategoryTypeEnum;
import com.e7learning.common.enums.CategoryTypeEnum;
import com.e7learning.common.enums.OrderStatusEnum;
import com.e7learning.common.enums.VendorTypeEnum;

public class CodeTag extends SimpleTagSupport {

	/***/
	private static final Logger LOG = Logger.getLogger(HtmlPreviewTag.class);

	private Integer type;

	private String code;

	@Override
	public void doTag() {
		try {
			String name = "";
			switch (type) {
			// active
			case 1:
				if (StringUtils.equalsIgnoreCase(Constant.TRUE, code))
					name = "是";
				else if (StringUtils.equalsIgnoreCase(Constant.FLASE, code))
					name = "否";
				break;
			// category
			case 2:
				List<CategoryTypeEnum> categoryTypeList = new ArrayList<CategoryTypeEnum>(
						Arrays.asList(CategoryTypeEnum.values()));
				for (CategoryTypeEnum category : categoryTypeList) {
					if (StringUtils.equalsIgnoreCase(category.name(), code)) {
						name = category.getDisplayName();
						break;
					}
				}
				break;
			// vendor
			case 3:
				List<VendorTypeEnum> vendorTypeList = new ArrayList<VendorTypeEnum>(
						Arrays.asList(VendorTypeEnum.values()));
				for (VendorTypeEnum vendor : vendorTypeList) {
					if (StringUtils.equalsIgnoreCase(vendor.name(), code)) {
						name = vendor.getDisplayName();
						break;
					}
				}
				break;
			// ad category
			case 4:
				List<AdCategoryTypeEnum> adCategoryTypeList = Arrays.asList(AdCategoryTypeEnum.values());
				for (AdCategoryTypeEnum category : adCategoryTypeList) {
					if (StringUtils.equalsIgnoreCase(category.name(), code)) {
						name = category.getDisplayName();
						break;
					}
				}
				break;
			// 訂單狀態
			case 5:
				List<OrderStatusEnum> orderStatusList = Arrays.asList(OrderStatusEnum.values());
				for (OrderStatusEnum status : orderStatusList) {
					if (StringUtils.equalsIgnoreCase(status.name(), code)) {
						name = status.getDisplayName();
						break;
					}
				}
				break;
			// active
			case 6:
				if (StringUtils.equalsIgnoreCase(Constant.TRUE, code))
					name = "可上架";
				else if (StringUtils.equalsIgnoreCase(Constant.FLASE, code))
					name = "不可上架";
				break;
			default:
				break;
			}

			JspWriter out = getJspContext().getOut();
			out.print(name);
		} catch (Exception e) {
			LOG.error("Exception:", e);
		}
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
