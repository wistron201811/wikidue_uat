package com.e7learning.common.handler;

import org.apache.log4j.Logger;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Order(100)
public class GlobalExceptionHandler {
	private static final Logger log = Logger.getLogger(GlobalExceptionHandler.class);

	@ExceptionHandler
	public String handleException(Exception e) {
		log.error("GlobalExceptionHandler:", e);
		return "errorPage";
	}
}