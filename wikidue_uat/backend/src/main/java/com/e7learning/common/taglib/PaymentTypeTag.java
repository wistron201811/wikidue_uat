package com.e7learning.common.taglib;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.e7learning.common.enums.PayWayEnum;

public class PaymentTypeTag extends SimpleTagSupport {

	/***/
	private static final Logger LOG = Logger.getLogger(PaymentTypeTag.class);

	private String type;

	@Override
	public void doTag() {
		try {
			PayWayEnum payWay = null;
			if (!StringUtils.isEmpty(type)) {
				payWay = PayWayEnum.getByCode(type);
			}
			JspWriter out = getJspContext().getOut();
			out.print(payWay == null ? StringUtils.EMPTY : payWay.getName());
		} catch (Exception e) {
			LOG.error("Exception:", e);
		}
	}

	public void setType(String type) {
		this.type = type;
	}

}
