package com.e7learning.common.aop;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.time.StopWatch;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.annotation.Order;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.e7learning.common.Constant;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.vo.UserAuthVo;

@Aspect
@Order(1)
@Component
public class MessageAop {
	/**
	 * log
	 */
	private static final Logger LOG = Logger.getLogger(MessageAop.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private ConfigService configService;

	@Around("execution(* com.e7learning.*.controller..*.*(..))")
	public Object accessAuth(ProceedingJoinPoint joinPoint) throws Throwable {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getRequest();
		AuthThreadLocal.setRequest(request);
		request.setAttribute(Constant.LOGOUT_REDIRECT_URI, configService.getHost()
				+ configService.getSsoLogoutRedirectUri() + "?d=" + Calendar.getInstance().getTimeInMillis());
		request.setAttribute(Constant.SSO_LOGOUT_PAGE, configService.getSsoLogoutPage());

		if (SecurityContextHolder.getContext().getAuthentication() != null
				&& !StringUtils.isEmpty(SecurityContextHolder.getContext().getAuthentication().getName())
				&& !"anonymousUser".equals(SecurityContextHolder.getContext().getAuthentication().getName())) {
			UserAuthVo loginUser = (UserAuthVo) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			AuthThreadLocal.setLoginUser(loginUser);
			request.getSession().setAttribute(Constant.USER_VO_KEY, loginUser);
		}

		return joinPoint.proceed();
		// return logging(joinPoint);
	}

	@Pointcut("execution(* com.e7learning.*.service..*.*(..))")
	public void service() {
	}

	@Around("service()")
	public Object logging(ProceedingJoinPoint joinPoint) throws Throwable {
		String clazz = joinPoint.getTarget().getClass().getSimpleName();
		String method = joinPoint.getSignature().getName();
		StopWatch watch = new StopWatch();
		watch.start();
		Object result = null;
		try {
			result = joinPoint.proceed();
		} catch (ServiceException e) {
			AuthThreadLocal.getRequest().setAttribute(Constant.ERROR_MESSAGE_KEY, e.getMessage());
			AuthThreadLocal.getRequest().getSession().setAttribute(Constant.ERROR_MESSAGE_KEY, e.getMessage());
			LOG.info("set err msg");
			LOG.error(clazz + "." + method, e);
			throw e;
		} catch (MessageException e) {
			AuthThreadLocal.getRequest().setAttribute(Constant.MESSAGE_KEY, e.getMessage());
			AuthThreadLocal.getRequest().getSession().setAttribute(Constant.MESSAGE_KEY, e.getMessage());
		} catch (Throwable e) {
			LOG.error(clazz + "." + method, e);
			throw new ServiceException(resources.getMessage("system.error", null, null));
		}

		watch.stop();
		String log = "token: " + AuthThreadLocal.getToken() + " >>>> " + clazz + " Run " + method + " complete in "
				+ watch.getTime() + " ms";
		LOG.info(log);
		return result;
	}
}
