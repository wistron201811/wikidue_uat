package com.e7learning.common.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import com.e7learning.common.utils.RestTemplateUtils;
import com.e7learning.common.vo.VodeoWatchHistoryPrams;
import com.e7learning.common.vo.VodeoWatchHistoryResultVo;
import com.e7learning.repository.CourseVideoRepository;
import com.e7learning.repository.LearningProgressRepository;
import com.e7learning.repository.RoleRepository;
import com.e7learning.repository.UserCourseRepository;
import com.e7learning.repository.VideoWatchHistoryRepository;
import com.e7learning.repository.model.CourseVideo;
import com.e7learning.repository.model.LearningProgress;
import com.e7learning.repository.model.Role;
import com.e7learning.repository.model.VideoWatchHistory;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Service
public class VideoPlatformService {

	private static final Logger LOG = Logger.getLogger(VideoPlatformService.class);

	@Autowired
	private ConfigService configService;

//	@Autowired
//	private RestTemplate restTemplate;

	@Autowired
	private LearningProgressRepository learningProgressRepository;

	@Autowired
	private CourseVideoRepository courseVideoRepository;

	@Autowired
	private VideoWatchHistoryRepository videoWatchHistoryRepository;

	@Autowired
	private UserCourseRepository userCourseRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Transactional
	public void updateVideoWatchHistory() {

		List<Role> roles = roleRepository.findAll((Root<Role> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
			return cb.equal(root.get("roleId").get("roleName"), "USER");
		});
		DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyyMMddHHmmss");

		roles.stream().forEach(v -> {
			Map<String, String> urlVariables = new HashMap<>();
			VodeoWatchHistoryPrams params = new VodeoWatchHistoryPrams();
			if (v.getVideoWatchUpateDt() != null) {
				params.setBegin(v.getVideoWatchUpateDt());
			}
			params.setAccountName(configService.getVideoAccountNmae());
			params.setUid(v.getRoleId().getEmail());
			urlVariables.put("json", new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create().toJson(params));
			try {
				ResponseEntity<VodeoWatchHistoryResultVo> resutl = RestTemplateUtils.getSSlRestTemplate().getForEntity(
						configService.getUpdateVodeoWatchHistoryApi() + "?json={json}", VodeoWatchHistoryResultVo.class,
						urlVariables);
				LOG.info("updateVideoWatchHistory >> params:" + urlVariables + ",result:"
						+ new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create().toJson(resutl));
				if (resutl.getStatusCode().value() == HttpStatus.SC_OK
						&& "success".equals(resutl.getBody().getResult())) {
					Date updateDt = resutl.getBody().getLastParseTime();
					boolean hasUpdate = v.getVideoWatchUpateDt() != null && !StringUtils
							.equals(dtf.print(updateDt.getTime()), dtf.print(v.getVideoWatchUpateDt().getTime()));
					v.setVideoWatchUpateDt(updateDt);
					if (hasUpdate)
						roleRepository.save(v);
					if (resutl.getBody() != null && !CollectionUtils.isEmpty(resutl.getBody().getHbIDList())
							&& hasUpdate) {
						userCourseRepository.updateUpdateDtByUserId(updateDt, v.getRoleId().getEmail());
						resutl.getBody().getHbIDList().forEach(tmp -> {
							// 把時間累積上去
							courseVideoRepository
									.findAll((Root<CourseVideo> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
										return cb.equal(root.get("video").get("thirdId"), tmp.getHbID());
									}).stream().forEach(courseVideo -> {
										learningProgressRepository.findAll((Root<LearningProgress> root,
												CriteriaQuery<?> query, CriteriaBuilder cb) -> {
											return cb.equal(root.get("fkCourseVideo"), courseVideo.getPkCourseVideo());
										}).stream().forEach(learningProgress -> {
											learningProgress.setLastWatchDt(updateDt);
											learningProgress.setUpdateDt(updateDt);
											learningProgress.setLearningDuration(
													learningProgress.getLearningDuration() + tmp.getTotalDuration());
											learningProgressRepository.save(learningProgress);
										});
									});

							// 紀錄log
							VideoWatchHistory videoWatchHistory = new VideoWatchHistory();
							videoWatchHistory.setAccountName(configService.getVideoAccountNmae());
							videoWatchHistory.setBegin(params.getBegin());
							videoWatchHistory.setCreateDt(new Date());
							videoWatchHistory.setUid(params.getUid());
							videoWatchHistory.setHbId(tmp.getHbID());
							videoWatchHistory.setStreaming(tmp.getMb());
							videoWatchHistory.setViews(tmp.getTotalDuration());
							videoWatchHistoryRepository.save(videoWatchHistory);
						});
					}

				} else {
					LOG.error("updateVideoWatchHistory >> params:" + urlVariables + ",result:"
							+ new Gson().toJson(resutl));
				}
			} catch (Exception e) {
				LOG.error("updateVideoWatchHistory >> params:" + urlVariables, e);
			}

		});

	}

}
