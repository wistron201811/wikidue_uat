package com.e7learning.common.service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.api.domain.param.TagQueryParam;
import com.e7learning.api.domain.result.KnowledgeMapResult;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.RestException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.utils.RestTemplateUtils;
import com.e7learning.common.vo.KnowledgeMapVo;
import com.e7learning.repository.KnowledgeMapRepository;
import com.e7learning.repository.model.KnowledgeMap;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Service
public class KnowledgeMapService extends BaseService {

	private static final Logger LOG = Logger.getLogger(KnowledgeMapService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private ConfigService configService;

	@Autowired
	private KnowledgeMapRepository kmRepository;

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void getKnowledgeMap(String operation) throws ServiceException {
		try {
			if (StringUtils.equalsIgnoreCase("rebuildDB", operation)) {
				kmRepository.truncateDB();
			}
			this.insertLevel0();
			ResponseEntity<String> response = RestTemplateUtils.getSSlRestTemplate().getForEntity(configService.getKmApi2Url(), String.class);
			Type collectionType = new TypeToken<ArrayList<String>>() {
			}.getType();
			List<String> resultList = new Gson().fromJson(response.getBody(), collectionType);
			for (String code : resultList) {
				response = RestTemplateUtils.getSSlRestTemplate().getForEntity(configService.getKmApi3Url() + "?referCode=" + code, String.class);
				collectionType = new TypeToken<ArrayList<KnowledgeMapVo>>() {
				}.getType();
				List<KnowledgeMapVo> list = new Gson().fromJson(response.getBody(), collectionType);
				List<KnowledgeMap> saveList = new ArrayList<KnowledgeMap>();
				for (KnowledgeMapVo vo : list) {
					if (vo.getLevel().equals("1") || vo.getLevel().equals("3")) {
						KnowledgeMap km = this.getKm(vo, code.substring(5));
						if (km != null) {
							saveList.add(km);
						}
					}
				}
				kmRepository.save(saveList);
				kmRepository.flush();
			}
		} catch (Exception e) {
			LOG.error("getKnowledgeMap error", e);
			throw new ServiceException(resources.getMessage("km.get.data.error", null, null));
		}
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	private KnowledgeMap getKm(KnowledgeMapVo kmVo, String level0) {
		KnowledgeMap km = new KnowledgeMap();
		km.setPkKnowledgeMap(level0 + "-" + kmVo.getSubjectCode());
		km.setTagName(kmVo.getSubjectName());
		km.setTagLevel(kmVo.getLevel());
		km.setCreateDt(new Date());
		km.setUpdateDt(new Date());
		km.setCreateId(getCurrentUserId());
		km.setLastModifiedId(getCurrentUserId());
		return km;
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	private void insertLevel0() {
		KnowledgeMap km = null;
		Map<String, String> map = new LinkedHashMap<String, String>();
		map.put("0001", "一年級");
		map.put("0002", "二年級");
		map.put("0003", "三年級");
		map.put("0004", "四年級");
		map.put("0005", "五年級");
		map.put("0006", "六年級");
		map.put("0007", "七年級");
		map.put("0008", "八年級");
		map.put("0009", "九年級");
		map.put("0010", "高一");
		map.put("0011", "高二");
		map.put("0012", "高三");
		Set<String> keys = map.keySet();
		for (String key : keys) {
			String value = map.get(key);
			km = new KnowledgeMap();
			km.setPkKnowledgeMap(key);
			km.setTagName(value);
			km.setTagLevel("0");
			km.setCreateDt(new Date());
			km.setUpdateDt(new Date());
			km.setCreateId(getCurrentUserId());
			km.setLastModifiedId(getCurrentUserId());
			kmRepository.save(km);
		}
	}

	public List<KnowledgeMapResult> queryTag(TagQueryParam param) throws RestException {
		List<KnowledgeMapResult> result = new ArrayList<>();

		try {
			LOG.info(new Gson().toJson(param));
			List<KnowledgeMap> tags = kmRepository.findAll(buildOperSpecification(param));
			tags.stream().map(KnowledgeMapService::toKnowledgeMapResult).forEach(result::add);
		} catch (Exception e) {
			LOG.error("queryTag", e);
			throw new RestException(resources.getMessage("tag.query.err", null, null));
		}

		return result;
	}

	private static KnowledgeMapResult toKnowledgeMapResult(KnowledgeMap map) {
		KnowledgeMapResult result = new KnowledgeMapResult();
		result.setLevel(map.getTagLevel());
		result.setName(map.getTagName());
		result.setTag(map.getPkKnowledgeMap());
		return result;
	}

	private Specification<KnowledgeMap> buildOperSpecification(TagQueryParam param) {
		return (Root<KnowledgeMap> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {

			List<Predicate> predicates = new ArrayList<>();

			if (StringUtils.isNotBlank(param.getLevel())) {
				Path<String> np = root.get("tagLevel");
				predicates.add(cb.equal(np, param.getLevel()));
			}
			if (StringUtils.isNotBlank(param.getTag())) {
				Path<String> np = root.get("pkKnowledgeMap");
				predicates.add(cb.like(np, param.getTag() + "%"));
			}
			if (!predicates.isEmpty()) {
				return cb.and(predicates.toArray(new Predicate[predicates.size()]));
			}
			return cb.conjunction();
		};
	}

	public boolean isEmpty() {
		return kmRepository.count() == 0;
	}
}
