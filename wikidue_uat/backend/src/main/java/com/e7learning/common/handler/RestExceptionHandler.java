package com.e7learning.common.handler;

import org.apache.log4j.Logger;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.e7learning.api.domain.RestResult;
import com.e7learning.common.exception.FileNotFoundException;
import com.e7learning.common.exception.RestException;

@RestControllerAdvice(basePackages = "com.e7learning.api.controller")
@Order(99)
public class RestExceptionHandler {

	private static final Logger log = Logger.getLogger(RestExceptionHandler.class);

	@ExceptionHandler(RestException.class)
	public RestResult<Error> handleRestException(RestException e) {
		log.error("handleException:", e);
		return new RestResult<>(false, e.getMessage());
	}

	@ExceptionHandler(FileNotFoundException.class)
	public void handleFileNotFoundException(FileNotFoundException e) {
		log.error("handleFileNotFoundException:", e);
		throw e;
	}

	@ExceptionHandler
	public RestResult<Error> handleException(Exception e) {
		try {
			if (e.getCause() instanceof RestException) {
				return new RestResult<>(false, e.getMessage());
			}
		} catch (Exception e1) {
			log.error("handleException:", e1);
		}
		log.error("handleException:", e);
		return new RestResult<>(false);
	}
}
