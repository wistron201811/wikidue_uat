package com.e7learning.common.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 讀取config檔
 * 
 * @author ChrisTsai
 *
 */
@Component
public class ConfigService {

	@Value("${page.size}")
	private Integer pageSize;

	@Value("${sso.login.page}")
	private String ssoLoginPage;

	@Value("${sso.token.page}")
	private String ssoTokenPage;

	@Value("${sso.clientId}")
	private String ssoClientId;

	@Value("${sso.login.responseType}")
	private String ssoLoginResponseType;

	@Value("${sso.login.redirectUri}")
	private String ssoLoginRedirectUri;

	@Value("${sso.clientSecret}")
	private String clientSecret;

	@Value("${sso.token.grantType}")
	private String ssoTokenGrantType;

	@Value("${host}")
	private String host;

	@Value("${sso.userInfo.api}")
	private String ssoUserInfoApi;

	@Value("${sso.logout.page}")
	private String ssoLogoutPage;

	@Value("${sso.logout.redirect.uri}")
	private String ssoLogoutRedirectUri;

	@Value("${km.api1.url}")
	private String kmApi1Url;

	@Value("${km.api2.url}")
	private String kmApi2Url;

	@Value("${km.api3.url}")
	private String kmApi3Url;

	@Value("${share.center.url}")
	private String shareCenterUrl;

	@Value("${application.version}")
	private String applicationVersion;

	@Value("${low.inventory}")
	private Integer lowInventory;

	@Value("${external.link.map.management}")
	private String mapManagementUrl;

	@Value("${external.link.evaluation.upload}")
	private String evaluationUploadUrl;

	@Value("${external.link.evaluation.management}")
	private String evaluationManagementUrl;

	@Value("${external.link.account.management}")
	private String accountManagementUrl;

	@Value("${update.vodeo.watch.history.api}")
	private String updateVodeoWatchHistoryApi;

	@Value("${video.account.name}")
	private String videoAccountNmae;

	@Value("${accounting.process.fee.rate}")
	private String processingFeeRate;

	@Value("${accounting.management.fee}")
	private String managementFee;

	@Value("${accounting.ad.fee}")
	private String adFee;

	@Value("${skip.invoice}")
	private Boolean skipInvoice;

	@Value("${store.url}")
	private String storeUrl;

	@Value("${myorder.url}")
	private String myOrderUrl;

	@Value("${image.url}")
	private String imageUrl;

	@Value("${admin.name}")
	private String adminName;

	@Value("${email.sender}")
	private String emailSender;
	
	@Value("${service.url}")
	private String serviceUrl;
		
	@Value("${backend.manual.url}")
	private String backendManualUrl;
	
	@Value("${product.default.weights}")
	private String weights;
	
	@Value("${course.file.size.limit}")
	private Integer courseFileSizeLimit;

	@Value("${course.file.size.limit.total}")
	private Integer courseFileSizeLimitTotal;
	
	public Integer getPageSize() {
		return pageSize;
	}

	public String getSsoLoginPage() {
		return ssoLoginPage;
	}

	public String getSsoClientId() {
		return ssoClientId;
	}

	public String getSsoLoginResponseType() {
		return ssoLoginResponseType;
	}

	public String getSsoLoginRedirectUri() {
		return ssoLoginRedirectUri;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public String getSsoTokenGrantType() {
		return ssoTokenGrantType;
	}

	public String getHost() {
		return host;
		//return "http://localhost:8080/backend";
	}

	public String getSsoTokenPage() {
		return ssoTokenPage;
	}

	public String getSsoUserInfoApi() {
		return ssoUserInfoApi;
	}

	public String getSsoLogoutPage() {
		return ssoLogoutPage;
	}

	public String getSsoLogoutRedirectUri() {
		return ssoLogoutRedirectUri;
	}

	public String getKmApi1Url() {
		return kmApi1Url;
	}

	public String getKmApi2Url() {
		return kmApi2Url;
	}

	public String getKmApi3Url() {
		return kmApi3Url;
	}

	public String getShareCenterUrl() {
		return shareCenterUrl;
	}

	public String getApplicationVersion() {
		return applicationVersion;
	}

	public String getUpdateVodeoWatchHistoryApi() {
		return updateVodeoWatchHistoryApi;
	}

	public String getVideoAccountNmae() {
		return videoAccountNmae;
	}

	public Integer getLowInventory() {
		return lowInventory;
	}

	public String getMapManagementUrl() {
		return mapManagementUrl;
	}

	public String getEvaluationUploadUrl() {
		return evaluationUploadUrl;
	}

	public String getEvaluationManagementUrl() {
		return evaluationManagementUrl;
	}

	public String getAccountManagementUrl() {
		return accountManagementUrl;
	}

	public String getProcessingFeeRate() {
		return processingFeeRate;
	}

	public String getManagementFee() {
		return managementFee;
	}

	public String getAdFee() {
		return adFee;
	}

	public Boolean getSkipInvoice() {
		return skipInvoice;
	}

	public String getStoreUrl() {
		return storeUrl;
	}

	public String getMyOrderUrl() {
		return storeUrl + myOrderUrl;
	}

	public String getImageUrl() {
		return storeUrl + imageUrl;
	}

	public String getAdminName() {
		return adminName;
	}

	public String getEmailSender() {
		return emailSender;
	}
	
	public String getServiceUrl() {
		return serviceUrl;
	}

	public String getBackendManualUrl() {
		return backendManualUrl;
	}
	
	public String getWeights() {
		return weights;
	}

	public Integer getCourseFileSizeLimit() {
		return courseFileSizeLimit;
	}

	public Integer getCourseFileSizeLimitTotal() {
		return courseFileSizeLimitTotal;
	}
	
}
