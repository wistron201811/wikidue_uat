package com.e7learning.common.controller;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

public class BaseController {

	private static final Logger log = Logger.getLogger(BaseController.class);
	
	public int getPage(String page) {
		if (StringUtils.isEmpty(page)) {
			return 1;
		} else {
			try {
				page = page.replace(",", "");
				return Integer.parseInt(page);
			} catch (Exception e) {
				log.error("Exception:", e);
				return 1;
			}
		}
	}
}
