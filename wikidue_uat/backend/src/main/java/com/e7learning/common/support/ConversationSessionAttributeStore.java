package com.e7learning.common.support;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;
import org.springframework.web.bind.support.DefaultSessionAttributeStore;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.WebRequest;

public class ConversationSessionAttributeStore extends DefaultSessionAttributeStore {

	@Override
	public void storeAttribute(WebRequest request, String attributeName, Object attributeValue) {
		Assert.notNull(request, "WebRequest must not be null");
		Assert.notNull(attributeName, "Attribute name must not be null");
		Assert.notNull(attributeValue, "Attribute value must not be null");
		String storeAttributeName = getAttributeNameInSession(request, attributeName, true);
		System.out.println("storeAttributeName:"+storeAttributeName);
		request.setAttribute(storeAttributeName, attributeValue, WebRequest.SCOPE_SESSION);
	}

	@Override
	public Object retrieveAttribute(WebRequest request, String attributeName) {
		Assert.notNull(request, "WebRequest must not be null");
		Assert.notNull(attributeName, "Attribute name must not be null");
		String storeAttributeName = getAttributeNameInSession(request, attributeName, false);
		System.out.println("retrieveAttribute:"+storeAttributeName);
		return request.getAttribute(storeAttributeName, WebRequest.SCOPE_SESSION);
	}

	@Override
	public void cleanupAttribute(WebRequest request, String attributeName) {
		Assert.notNull(request, "WebRequest must not be null");
		Assert.notNull(attributeName, "Attribute name must not be null");
		String storeAttributeName = getAttributeNameInSession(request, attributeName, false);
		System.out.println("storeAttributeName:"+storeAttributeName);
		request.removeAttribute(storeAttributeName, WebRequest.SCOPE_SESSION);
	}

	private String getAttributeNameInSession(WebRequest request, String attributeName, boolean isNew) {
		String a = super.getAttributeNameInSession(request, attributeName);
		if (isNew) {
			String c = String.valueOf(new Date().getTime());
			request.setAttribute("conversationKey", c, RequestAttributes.SCOPE_REQUEST);
			a += "_" + c;
		} else {
			String c = request.getParameter("conversationKey");
			if (StringUtils.isNotBlank(c)) {
				a += "_" + c;
			}
		}
		System.out.println("getAttributeNameInSession:" + a);
		return a;
	}

}