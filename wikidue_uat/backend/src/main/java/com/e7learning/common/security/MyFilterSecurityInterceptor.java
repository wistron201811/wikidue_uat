package com.e7learning.common.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpRequest;
import org.springframework.security.access.SecurityMetadataSource;
import org.springframework.security.access.intercept.AbstractSecurityInterceptor;
import org.springframework.security.access.intercept.InterceptorStatusToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;

import com.e7learning.common.Constant;
import com.e7learning.common.aop.AuthThreadLocal;
import com.e7learning.common.vo.UserAuthVo;

public class MyFilterSecurityInterceptor extends AbstractSecurityInterceptor implements Filter {

	private static final Logger log = Logger.getLogger(MyFilterSecurityInterceptor.class);

	private FilterInvocationSecurityMetadataSource securityMetadataSource;

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
//		if (!StringUtils.isEmpty(SecurityContextHolder.getContext().getAuthentication().getName())
//				&& !"anonymousUser".equals(SecurityContextHolder.getContext().getAuthentication().getName())) {
//			UserAuthVo loginUser = (UserAuthVo)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//			HttpServletRequest servletRequest = (HttpServletRequest)request;
//			servletRequest.getSession().setAttribute(Constant.USER_VO_KEY, loginUser);
//		}
		
		
		FilterInvocation fi = new FilterInvocation(request, response, chain);
		invoke(fi);

	}

	public FilterInvocationSecurityMetadataSource getSecurityMetadataSource() {
		return this.securityMetadataSource;
	}

	public Class<? extends Object> getSecureObjectClass() {
		return FilterInvocation.class;
	}

	public void invoke(FilterInvocation fi) throws IOException, ServletException {

		InterceptorStatusToken token = super.beforeInvocation(fi);

		try {
			fi.getChain().doFilter(fi.getRequest(), fi.getResponse());
		} finally {
			super.afterInvocation(token, null);
		}

	}

	@Override
	public SecurityMetadataSource obtainSecurityMetadataSource() {
		return this.securityMetadataSource;
	}

	public void setSecurityMetadataSource(FilterInvocationSecurityMetadataSource securityMetadataSource) {
		this.securityMetadataSource = securityMetadataSource;
	}

	public void destroy() {

	}

	public void init(FilterConfig filterconfig) throws ServletException {

	}

}