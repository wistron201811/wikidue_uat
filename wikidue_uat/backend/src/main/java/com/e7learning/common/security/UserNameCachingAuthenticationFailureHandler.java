package com.e7learning.common.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.e7learning.common.Constant;

public class UserNameCachingAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

	public static final String LAST_USERNAME_KEY = "LAST_USERNAME";

	@Autowired
	private MessageSource msg;
	
	@Autowired
	private UsernamePasswordAuthenticationFilter usernamePasswordAuthenticationFilter;

	@Override
	public void onAuthenticationFailure(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {

//		super.onAuthenticationFailure(request, response, exception);
		
		String usernameParameter = usernamePasswordAuthenticationFilter.getUsernameParameter();
		String lastUserName = request.getParameter(usernameParameter);

//		HttpSession session = request.getSession(false);
//		if (session != null || isAllowSessionCreation()) {
			if(exception instanceof DisabledException){
				request.setAttribute(Constant.ERROR_MESSAGE_KEY, msg.getMessage("validate.account.disable", null, null));
	            request.setAttribute("account", lastUserName);
				request.getRequestDispatcher("/sign-up/login-failed").forward(request, response);
			}else{
				request.setAttribute(Constant.ERROR_MESSAGE_KEY, msg.getMessage("login.error", null, null));
			}
			request.setAttribute(LAST_USERNAME_KEY, lastUserName);
//		}
            request.getRequestDispatcher("/login").forward(request, response);
	}
}
