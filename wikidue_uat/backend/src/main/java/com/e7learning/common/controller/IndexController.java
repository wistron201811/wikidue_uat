package com.e7learning.common.controller;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.e7learning.common.Constant;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.service.SsoService;
import com.e7learning.common.vo.SsoToken;
import com.e7learning.common.vo.SsoUserInfoResponse;
import com.e7learning.common.vo.UserAuthVo;

@Controller
public class IndexController {

	private static Logger log = Logger.getLogger(IndexController.class);

	private static final String PAGE = "indexPage";

	private static final String LOGIN_PAGE = "login";
	
	private static final String PAGE_NOT_FOUND = "notFoundPage";

	private static final String ERROR_PAGE = "errorPage";

	@Autowired
	private ConfigService configService;

	@Autowired
	private SsoService ssoService;

	@RequestMapping(value = { "/", "/index" }, method = { RequestMethod.GET })
	public String goIndex(HttpServletRequest request, HttpSession session, Model model) {
		return PAGE;
	}

	@RequestMapping(value = { "/login" }, method = { RequestMethod.GET })
	public String goLogin(HttpServletRequest request, HttpSession session, Model model) {
		model.addAttribute("ssoLoginPage", configService.getSsoLoginPage());
		model.addAttribute("clientId", configService.getSsoClientId());
		model.addAttribute("responseType", configService.getSsoLoginResponseType());
		model.addAttribute("redirectUri", configService.getHost() + configService.getSsoLoginRedirectUri());

		return LOGIN_PAGE;
	}

	@RequestMapping(value = { "/login/auth" }, method = { RequestMethod.GET })
	public String auth(String code, String state, String series, HttpSession session) {
		log.info("code:" + code);
		log.info("state:" + state);
		log.info("series:" + series);

		try {

			SsoToken ssoToken = ssoService.getToken(code, state, series,
					configService.getHost() + configService.getSsoLoginRedirectUri());
			// 取得使用者資訊
			if (StringUtils.isNoneBlank(ssoToken.getAccessToken())) {
				SsoUserInfoResponse ssoUserInfoResponse = ssoService.getUserInfo(ssoToken.getAccessToken());
				ssoService.authUser(ssoUserInfoResponse.getBody().getUserInfo(), ssoToken.getRefreshToken(), series);
				return "redirect:/index?d=" + Calendar.getInstance().getTimeInMillis();
			}
		} catch (Exception e) {
			session.setAttribute("host", configService.getHost());
			session.setAttribute("series", series);
		}
		return "redirect:/login?d=" + Calendar.getInstance().getTimeInMillis();
	}

	@RequestMapping(value = { "/logout" })
	public String logout(HttpSession session) {
		log.info("/logout removeAttribute");
		session.removeAttribute("series");
		return "redirect:/logout/success?d=" + Calendar.getInstance().getTimeInMillis();
	}
	

	@RequestMapping(value = "/404")
	public String go404() {
		return PAGE_NOT_FOUND;
	}

	@RequestMapping(value = "/error")
	public String goErrpage() {
		return ERROR_PAGE;
	}

}
