package com.e7learning.common.form;

import java.io.Serializable;

import org.springframework.data.domain.Page;

/**
 * @author ChrisTsai
 *
 */
public class BaseForm<T> implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	private String method;

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	private Page<T> result;

	public Page<T> getResult() {
		return result;
	}

	public void setResult(Page<T> result) {
		this.result = result;
	}
}
