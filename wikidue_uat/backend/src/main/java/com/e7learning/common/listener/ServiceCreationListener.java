package com.e7learning.common.listener;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.KnowledgeMapService;

@Component
public class ServiceCreationListener implements ApplicationListener<ContextRefreshedEvent> {

	private static final Logger log = Logger.getLogger(ServiceCreationListener.class);

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		ApplicationContext applicationContext = event.getApplicationContext();
		try {
			System.out.println("initKnowledgeMap");
			initKnowledgeMap(applicationContext);
		} catch (Exception e) {
			log.error("initKnowledgeMap:", e);
		}
	}

	synchronized void initKnowledgeMap(ApplicationContext applicationContext) {
		KnowledgeMapService knowledgeMapService = (KnowledgeMapService) applicationContext
				.getBean("knowledgeMapService");
		if (knowledgeMapService != null && knowledgeMapService.isEmpty()) {
			try {
				knowledgeMapService.getKnowledgeMap("rebuildDB");
			} catch (ServiceException e) {
				log.error("rebuildDB", e);
			}
		}
	}
}
