package com.e7learning.common.vo;

public class VodeoWatchHistoryVo {

	private int totalDuration;
	
	private double mb;
	
	private String hbID;

	public int getTotalDuration() {
		return totalDuration;
	}

	public void setTotalDuration(int totalDuration) {
		this.totalDuration = totalDuration;
	}

	public double getMb() {
		return mb;
	}

	public void setMb(double mb) {
		this.mb = mb;
	}

	public String getHbID() {
		return hbID;
	}

	public void setHbID(String hbID) {
		this.hbID = hbID;
	}
	
	
}
