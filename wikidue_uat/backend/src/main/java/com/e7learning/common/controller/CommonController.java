package com.e7learning.common.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.e7learning.common.service.CommonService;
import com.e7learning.common.service.KnowledgeMapService;
import com.e7learning.common.service.VideoPlatformService;

@Controller
@RequestMapping("/common")
public class CommonController {

	private static final Logger log = Logger.getLogger(CommonController.class);

	@Autowired
	private CommonService commonService;

	@Autowired
	private KnowledgeMapService kmService;

	@Autowired
	private VideoPlatformService videoService;
	
	@Autowired
	private Properties appProperties;

	@RequestMapping(value = "/query/image/{fileId}")
	public void queryImage(@PathVariable("fileId") String fileId, HttpServletResponse response) throws IOException {
		commonService.downloadImageFile(fileId, response);
		response.flushBuffer();
	}

	@RequestMapping(value = "/km/{operation}")
	public @ResponseBody String getKnowledgeMap(@PathVariable("operation") String operation) {
		String result = "";
		try {
			// operation => "rebuildDB" : 刪除後重建
			// => "initDB" ： 初使化
			if ((StringUtils.equalsIgnoreCase("initDB", operation)
					|| StringUtils.equalsIgnoreCase("rebuildDB", operation)) && kmService.isAdmin()) {
				log.warn(operation + " for KnowledgeMap by Admion on" + new Date());
				kmService.getKnowledgeMap(operation);
				result = "ok";
			}

		} catch (Exception e) {
			result = "fail";
		}
		return result;
	}

	// @RequestMapping(value = "/file/init")
	public @ResponseBody String initFile(HttpServletResponse response) throws IOException {
		try {
			commonService.initFile();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "fail";
		}
		return "ok";
	}

	@RequestMapping(value = "/video/watch")
	public @ResponseBody String updateVideWatch(HttpServletResponse response) throws IOException {
		try {
			videoService.updateVideoWatchHistory();
		} catch (Exception e) {
			log.error("updateVideWatch", e);
			return "fail";
		}
		return "ok";
	}

	@RequestMapping(value = "/sys/sql")
	public String testSql(HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("allTable", commonService.getAllTableString());
		return "test";
	}
	
	@RequestMapping(value = "/sys/properties")
	public String showProperties(HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("props", appProperties);
		return "properties";
	}

	@RequestMapping(value = "/sys/sql", method = RequestMethod.POST)
	public String exeSql(Model model, String method, String sql) {
		try {
			model.addAttribute("allTable", commonService.getAllTableString());
			model.addAttribute("sql", sql);
			model.addAttribute("method", method);
			if ("query".equals(method)) {
				List<Map<String, Object>> result = commonService.querySql(sql);
//				System.out.println(new Gson().toJson(result));
				model.addAttribute("result", result);
			} else {
				model.addAttribute("result", commonService.exeSql(sql));
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "test";
	}
}
