package com.e7learning.common.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.e7learning.common.utils.MailLogService;

import freemarker.template.Configuration;

@Service
public class MailService {

	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	private Configuration freemarkerConfiguration;
	@Autowired
	private ConfigService configService;
	@Autowired
	private TemplateService templateService;

	private static final Logger log = Logger.getLogger(MailService.class);

	private static final String DATE_FORMAT = "yyyy/MM/dd";

	public void sendVideoUploadSuccessEmail(String[] email, String[] copy, String courseName, Date sendDate,
			List<Map<String, String>> details) {
		try {
			if (email == null) {
				email = new String[] {};
			}
			if (copy == null) {
				copy = new String[] {};
			}
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			MimeMessagePreparator preparator = getMessagePreparator4VideoUploadSuccess(email, copy, courseName,
					configService.getStoreUrl(), configService.getImageUrl(), sdf.format(sendDate), details);
			mailSender.send(preparator);
		} catch (Exception e) {
			MailLogService.writeErrorLog("Wikidue Store 教育商城 - 影片轉檔成功通知信", email, copy, e);
		}
	}

	public void sendVideoUploadFailedEmail(String[] email, String[] copy, String courseName, Date sendDate,
			List<Map<String, String>> details) {
		try {
			if (email == null) {
				email = new String[] {};
			}
			if (copy == null) {
				copy = new String[] {};
			}
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			MimeMessagePreparator preparator = getMessagePreparator4VideoUploadFailed(email, copy, courseName,
					configService.getStoreUrl(), configService.getImageUrl(), sdf.format(sendDate), details);
			mailSender.send(preparator);
		} catch (Exception e) {
			MailLogService.writeErrorLog("Wikidue Store 教育商城 - 影片轉檔失敗通知信", email, copy, e);
		}
	}

	public void sendRefundBuyerNotificationEmail(String[] email, String[] copy, Date sendDate, String buyerName,
			String orderNo, List<Map<String, String>> details) {

		// 退貨成功寄給買家通知信
		if (email == null) {
			email = new String[] {};
		}
		if (copy == null) {
			copy = new String[] {};
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			MimeMessagePreparator preparator = getMessagePreparator4RefundBuyerNotification(email, copy,
					configService.getStoreUrl(), sdf.format(sendDate), buyerName, configService.getMyOrderUrl(),
					orderNo, details, configService.getImageUrl(), configService.getServiceUrl());
			mailSender.send(preparator);
		} catch (Exception e) {
			MailLogService.writeErrorLog(String.format("Wikidue Store 教育商城 - 訂單編號 %s 退款通知信", orderNo), email, copy, e);
		}
	}

	public void sendRefundVendorNotificationEmail(String[] email, String[] copy, String adminName, String orderNo,
			Date refundDate, List<Map<String, String>> details, Date sendDate) {

		// 退貨成功寄給廠商通知信
		if (email == null) {
			email = new String[] {};
		}
		if (copy == null) {
			copy = new String[] {};
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			MimeMessagePreparator preparator = getMessagePreparator4RefundVendorNotification(email, copy,
					configService.getStoreUrl(), adminName, orderNo, sdf.format(refundDate), configService.getHost(),
					details, configService.getImageUrl(), sdf.format(sendDate), configService.getBackendManualUrl(),
					configService.getServiceUrl());
			mailSender.send(preparator);
		} catch (Exception e) {
			MailLogService.writeErrorLog(String.format("Wikidue Store 教育商城 - 訂單編號 %s 退款通知信", orderNo), email, copy, e);
		}
	}

	public void sendInventoryNotificationEmail(String[] email, String[] copy, Date sendDate, String vendorName,
			Date noticeDate, List<Map<String, String>> details) {
		// 廠商庫存不足通知信
		if (email == null) {
			email = new String[] {};
		}
		if (copy == null) {
			copy = new String[] {};
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			MimeMessagePreparator preparator = getMessagePreparator4InventoryNotification(email, copy,
					configService.getStoreUrl(), sdf.format(sendDate), vendorName, sdf.format(noticeDate),
					configService.getHost(), details, configService.getImageUrl(), configService.getBackendManualUrl(),
					configService.getServiceUrl(), configService.getLowInventory());
			mailSender.send(preparator);
		} catch (Exception e) {
			MailLogService.writeErrorLog("Wikidue Store 教育商城 - 商品庫存過低提醒通知信", email, copy, e);
		}
	}

	public void sendReceiveRefundNotificationEmail(String[] email, String[] copy, Date sendDate, String adminName,
			String orderNo, Date refundDate, List<Map<String, String>> details, String total, String recipient,
			String telPhone, String mobile, String address, boolean mail) {
		// 管理員收到退貨通知信
		if (email == null) {
			email = new String[] {};
		}
		if (copy == null) {
			copy = new String[] {};
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			MimeMessagePreparator preparator = getMessagePreparator4ReceiveRefundNotification(email, copy,
					configService.getStoreUrl(), sdf.format(sendDate), adminName, orderNo, sdf.format(refundDate),
					configService.getHost(), details, total, recipient, telPhone, mobile, address,
					configService.getImageUrl(), configService.getBackendManualUrl(), configService.getServiceUrl(),
					mail);
			mailSender.send(preparator);
		} catch (Exception e) {
			MailLogService.writeErrorLog(String.format("Wikidue Store 教育商城 - 訂單編號 %s 退貨成功通知信", orderNo), email, copy,
					e);
		}
	}

	public void sendRefuseRefundNotificationEmail(String[] email, String[] copy, Date sendDate, String adminName,
			String orderNo, List<Map<String, String>> details, String total) {
		// 管理員拒絕退貨通知信

		if (email == null) {
			email = new String[] {};
		}
		if (copy == null) {
			copy = new String[] {};
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			MimeMessagePreparator preparator = getMessagePreparator4RefuseRefundNotification(email, copy,
					configService.getStoreUrl(), configService.getImageUrl(), sdf.format(sendDate), adminName, orderNo,
					configService.getHost(), details, total, configService.getBackendManualUrl(),
					configService.getServiceUrl());
			mailSender.send(preparator);
		} catch (Exception e) {
			MailLogService.writeErrorLog(String.format("Wikidue Store 教育商城 - 訂單編號 %s 退貨不成功通知信", orderNo), email, copy,
					e);
		}
	}

	public void sendNotificationBuyerEmail(String[] email, String[] copy, Date sendDate, String buyerName,
			Date shipDate, String orderNo, List<Map<String, String>> details) {
		// to買家:出貨通知信
		if (email == null) {
			email = new String[] {};
		}
		if (copy == null) {
			copy = new String[] {};
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			MimeMessagePreparator preparator = getMessagePreparator4SendNotificationBuyer(email, copy,
					configService.getStoreUrl(), sdf.format(sendDate), buyerName, sdf.format(shipDate),
					configService.getMyOrderUrl(), orderNo, details, configService.getImageUrl());
			mailSender.send(preparator);
		} catch (Exception e) {
			MailLogService.writeErrorLog(String.format("Wikidue Store 教育商城 - 訂單編號 %s 出貨通知信", orderNo), email, copy, e);
		}
	}

	public void sendNotificationAdminEmail(String[] email, String[] copy, Date sendDate, String adminName,
			String orderNo, Date shipDate, List<Map<String, String>> details) {
		// to廠商:出貨通知信
		if (email == null) {
			email = new String[] {};
		}
		if (copy == null) {
			copy = new String[] {};
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			MimeMessagePreparator preparator = getMessagePreparator4SendNotificationAdmin(email, copy,
					configService.getStoreUrl(), sdf.format(sendDate), adminName, orderNo, sdf.format(shipDate),
					configService.getHost(), details, configService.getImageUrl());
			mailSender.send(preparator);
		} catch (Exception e) {
			MailLogService.writeErrorLog(String.format("Wikidue Store 教育商城 - 訂單編號 %s 出貨通知信", orderNo), email, copy, e);
		}
	}

	public void sendUnsendNotificationEmail(String[] email, String[] copy, String vendorName, String buyerName,
			String orderNo, String orderDate, List<Map<String, String>> details, String total, String recipient,
			String telPhone, String mobile, String address, String sendDate) {
		// 未出貨通知
		if (email == null) {
			email = new String[] {};
		}
		if (copy == null) {
			copy = new String[] {};
		}
		try {
			MimeMessagePreparator preparator = getMessagePreparator4UnsendNotification(email, copy,
					configService.getStoreUrl(), vendorName, buyerName, orderNo, orderDate, configService.getHost(),
					details, total, recipient, telPhone, mobile, address, configService.getImageUrl(), sendDate,
					configService.getBackendManualUrl(), configService.getServiceUrl());
			mailSender.send(preparator);
		} catch (Exception e) {
			MailLogService.writeErrorLog(String.format("Wikidue Store 教育商城 - 訂單編號 %s 已超過48小時尚未出貨提醒通知信", orderNo), email,
					copy, e);
		}
	}

	public void sendRefundProcessNotificationEmail(String[] email, String[] copy, String vendorName, String orderNo,
			Date refundDate, List<Map<String, String>> details, String total, String recipient, String telPhone,
			String mobile, String address, Date sendDate, boolean mail) {
		// 退貨處理中
		if (email == null) {
			email = new String[] {};
		}
		if (copy == null) {
			copy = new String[] {};
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			MimeMessagePreparator preparator = getMessagePreparator4RefundProcessNotification(email, copy,
					configService.getStoreUrl(), vendorName, orderNo, sdf.format(refundDate), configService.getHost(),
					details, total, recipient, telPhone, mobile, address, configService.getImageUrl(),
					sdf.format(sendDate), configService.getBackendManualUrl(), configService.getServiceUrl(), mail);
			mailSender.send(preparator);
		} catch (Exception e) {
			MailLogService.writeErrorLog(String.format("Wikidue Store 教育商城 - 訂單編號 %s 申請退貨通知信", orderNo), email, copy,
					e);
		}
	}

	private MimeMessagePreparator getMessagePreparator4RefundBuyerNotification(String[] email, String[] copy,
			String storeUrl, String sendDate, String buyerName, String myOrderUrl, String orderNo,
			List<Map<String, String>> details, String imageUrl, String serviceUrl) {
		// 退貨成功寄給買家通知信

		return new MimeMessagePreparator() {

			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");

				helper.setSubject(String.format("Wikidue Store 教育商城 - 訂單編號 %s 退款通知信", orderNo));
				helper.setTo(email);
				helper.setCc(copy);
				helper.setFrom(configService.getEmailSender());

				helper.setText(templateService.getRefundBuyerNotification(storeUrl, sendDate, buyerName, myOrderUrl,
						orderNo, details, imageUrl, serviceUrl), true);
			}
		};
	}

	private MimeMessagePreparator getMessagePreparator4RefundVendorNotification(String[] email, String[] copy,
			String storeUrl, String adminName, String orderNo, String refundDate, String backendUrl,
			List<Map<String, String>> details, String imageUrl, String sendDate, String backendManualUrl,
			String serviceUrl) {
		// 退貨成功寄給廠商通知信

		return new MimeMessagePreparator() {

			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");

				helper.setSubject(String.format("Wikidue Store 教育商城 - 訂單編號 %s 退款通知信", orderNo));
				helper.setTo(email);
				helper.setCc(copy);
				helper.setFrom(configService.getEmailSender());

				helper.setText(templateService.getRefundVendorNotification(storeUrl, adminName, orderNo, refundDate,
						backendUrl, details, imageUrl, sendDate, backendManualUrl, serviceUrl), true);
			}
		};
	}

	private MimeMessagePreparator getMessagePreparator4InventoryNotification(String[] email, String[] copy,
			String storeUrl, String sendDate, String vendorName, String noticeDate, String backendUrl,
			List<Map<String, String>> details, String imageUrl, String backendManualUrl, String serviceUrl,
			Integer quantityLimit) {
		// 寄給廠商庫存不足通知信

		return new MimeMessagePreparator() {

			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");

				helper.setSubject("Wikidue Store 教育商城 - 商品庫存過低提醒通知信");
				helper.setTo(email);
				helper.setCc(copy);
				helper.setFrom(configService.getEmailSender());

				helper.setText(templateService.getInventoryNotification(storeUrl, sendDate, vendorName, noticeDate,
						backendUrl, details, imageUrl, backendManualUrl, serviceUrl, quantityLimit), true);
			}
		};
	}

	private MimeMessagePreparator getMessagePreparator4ReceiveRefundNotification(String[] email, String[] copy,
			String storeUrl, String sendDate, String adminName, String orderNo, String refundDate, String backendUrl,
			List<Map<String, String>> details, String total, String recipient, String telPhone, String mobile,
			String address, String imageUrl, String backendManualUrl, String serviceUrl, boolean mail) {
		// 寄給管理員收到退貨通知信

		return new MimeMessagePreparator() {

			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");

				helper.setSubject(String.format("Wikidue Store 教育商城 - 訂單編號 %s 退貨成功通知信", orderNo));
				helper.setTo(email);
				helper.setCc(copy);
				helper.setFrom(configService.getEmailSender());

				helper.setText(templateService.getReceiveRefundNotification(storeUrl, sendDate, adminName, orderNo,
						refundDate, backendUrl, details, total, recipient, telPhone, mobile, address, imageUrl,
						backendManualUrl, serviceUrl, mail), true);
			}
		};
	}

	private MimeMessagePreparator getMessagePreparator4RefuseRefundNotification(String[] email, String[] copy,
			String storeUrl, String imageUrl, String sendDate, String adminName, String orderNo, String backendUrl,
			List<Map<String, String>> details, String total, String backendManualUrl, String serviceUrl) {
		// 寄給管理員拒絕退貨通知信

		return new MimeMessagePreparator() {

			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");

				helper.setSubject(String.format("Wikidue Store 教育商城 - 訂單編號 %s 退貨不成功通知信", orderNo));
				helper.setTo(email);
				helper.setCc(copy);
				helper.setFrom(configService.getEmailSender());

				helper.setText(templateService.getRefuseRefundNotification(storeUrl, imageUrl, sendDate, adminName,
						orderNo, backendUrl, details, total, backendManualUrl, serviceUrl), true);
			}
		};
	}

	private MimeMessagePreparator getMessagePreparator4SendNotificationBuyer(String[] email, String[] copy,
			String storeUrl, String sendDate, String buyerName, String shipDate, String myOrderUrl, String orderNo,
			List<Map<String, String>> details, String imageUrl) {
		// 寄給買家出貨通知信

		return new MimeMessagePreparator() {

			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");

				helper.setSubject(String.format("Wikidue Store 教育商城 - 訂單編號 %s 出貨通知信", orderNo));
				helper.setTo(email);
				helper.setCc(copy);
				helper.setFrom(configService.getEmailSender());

				helper.setText(templateService.getSendNotificationBuyer(storeUrl, sendDate, buyerName, shipDate,
						myOrderUrl, orderNo, details, imageUrl), true);
			}
		};
	}

	private MimeMessagePreparator getMessagePreparator4SendNotificationAdmin(String[] email, String[] copy,
			String storeUrl, String sendDate, String adminName, String orderNo, String shipDate, String backendUrl,
			List<Map<String, String>> details, String imageUrl) {
		// 寄給廠商出貨通知信

		return new MimeMessagePreparator() {

			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");

				helper.setSubject(String.format("Wikidue Store 教育商城 - 訂單編號 %s 出貨通知信", orderNo));
				helper.setTo(email);
				helper.setCc(copy);
				helper.setFrom(configService.getEmailSender());

				helper.setText(templateService.getSendNotificationAdmin(storeUrl, sendDate, adminName, orderNo,
						shipDate, backendUrl, details, imageUrl), true);
			}
		};
	}

	private MimeMessagePreparator getMessagePreparator4UnsendNotification(String[] email, String[] copy,
			String storeUrl, String vendorName, String buyerName, String orderNo, String orderDate, String backendUrl,
			List<Map<String, String>> details, String total, String recipient, String telPhone, String mobile,
			String address, String imageUrl, String sendDate, String backendManualUrl, String serviceUrl) {
		// 寄給廠商48h未出貨通知信

		return new MimeMessagePreparator() {

			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");

				helper.setSubject(String.format("Wikidue Store 教育商城 - 訂單編號 %s 已超過48小時尚未出貨提醒通知信", orderNo));
				helper.setTo(email);
				helper.setCc(copy);
				helper.setFrom(configService.getEmailSender());

				helper.setText(templateService.getUnsendNotification(storeUrl, vendorName, buyerName, orderNo,
						orderDate, backendUrl, details, total, recipient, telPhone, mobile, address, imageUrl, sendDate,
						backendManualUrl, serviceUrl), true);
			}
		};
	}

	private MimeMessagePreparator getMessagePreparator4RefundProcessNotification(String[] email, String[] copy,
			String storeUrl, String vendorName, String orderNo, String refundDate, String backendUrl,
			List<Map<String, String>> details, String total, String recipient, String telPhone, String mobile,
			String address, String imageUrl, String sendDate, String backendManualUrl, String serviceUrl,
			boolean mail) {
		// 寄給廠商48h未出貨通知信

		return new MimeMessagePreparator() {

			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");

				helper.setSubject(String.format("Wikidue Store 教育商城 - 訂單編號 %s 申請退貨通知信", orderNo));
				helper.setTo(email);
				helper.setCc(copy);
				helper.setFrom(configService.getEmailSender());

				helper.setText(templateService.getRefundProcessNotification(storeUrl, vendorName, orderNo, refundDate,
						backendUrl, details, total, recipient, telPhone, mobile, address, imageUrl, sendDate,
						backendManualUrl, serviceUrl, mail), true);
			}
		};
	}

	private MimeMessagePreparator getMessagePreparator4VideoUploadSuccess(String[] email, String[] copy,
			String courseName, String storeUrl, String imageUrl, String sendDate, List<Map<String, String>> details) {
		// 影片上傳成功通知信

		return new MimeMessagePreparator() {

			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");

				helper.setSubject("Wikidue Store 教育商城 - 影片轉檔成功通知信");
				helper.setTo(email);
				helper.setCc(copy);
				helper.setFrom(configService.getEmailSender());

				helper.setText(templateService.getVideoUploadSuccess(courseName, storeUrl, imageUrl, sendDate, details),
						true);
			}
		};
	}

	private MimeMessagePreparator getMessagePreparator4VideoUploadFailed(String[] email, String[] copy,
			String courseName, String storeUrl, String imageUrl, String sendDate, List<Map<String, String>> details) {
		// 影片上傳失敗通知信

		return new MimeMessagePreparator() {

			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");

				helper.setSubject("Wikidue Store 教育商城 - 影片轉檔失敗通知信");
				helper.setTo(email);
				helper.setCc(copy);
				helper.setFrom(configService.getEmailSender());

				helper.setText(templateService.getVideoUploadFailed(courseName, storeUrl, imageUrl, sendDate, details),
						true);
			}
		};
	}

	public String geFreeMarkerTemplateContent(Map<String, Object> map, String template) {
		StringBuffer content = new StringBuffer();
		try {
			content.append(FreeMarkerTemplateUtils
					.processTemplateIntoString(freemarkerConfiguration.getTemplate(template, "UTF-8"), map));
			return content.toString();
		} catch (Exception e) {
			log.error("Exception occured while processing fmtemplate:" + e.getMessage());
		}
		return "";
	}
}
