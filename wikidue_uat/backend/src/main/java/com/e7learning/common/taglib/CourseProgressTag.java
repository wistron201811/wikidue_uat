package com.e7learning.common.taglib;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.e7learning.course.service.CourseService;
import com.e7learning.repository.model.Course;

public class CourseProgressTag extends SimpleTagSupport {

	/***/
	private static final Logger LOG = Logger.getLogger(HtmlPreviewTag.class);

	private Integer courseId;

	private String uid;

	@Override
	public void doTag() {
		try {
			PageContext pageContext = (PageContext) getJspContext();
			WebApplicationContext context = WebApplicationContextUtils
					.getRequiredWebApplicationContext(pageContext.getServletContext());
			CourseService courseService = context.getBean(CourseService.class);
			Course course = courseService.getCourseAndProgess(courseId, uid);

			JspWriter out = getJspContext().getOut();
			if (course != null) {
				out.print(course.getProgress()>=20?"<p class='text-danger'>"+course.getProgress()+"%</p>":(course.getProgress()+"%"));
			} else {
				out.print(StringUtils.EMPTY);
			}

		} catch (Exception e) {
			LOG.error("Exception:", e);
		}
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

}
