package com.e7learning.api.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Rest Service回應結果
 *
 */
public class RestResult<T> implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 回傳結果代號
	 */
	private boolean success;
	/**
	 * 回傳結果訊息
	 */
	private String message;
	/**
	 * 回傳結果物件
	 */
	private T result;
	
	public RestResult() {
	}

	public RestResult(boolean success, String message) {
		this.success = success;
		this.message = message;
	}
	
	public RestResult(boolean success, String message, T result) {
		this.success = success;
		this.message = message;
		this.result = result;
	}
	
	public RestResult(boolean success, T result) {
		this.success = success;
		this.result = result;
	}
	
	public RestResult(boolean success) {
		this.success = success;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}
	
	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
