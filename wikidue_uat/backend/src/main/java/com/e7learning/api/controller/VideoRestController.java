package com.e7learning.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.e7learning.api.domain.RestResult;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.video.service.VideoService;
import com.e7learning.video.vo.VideoVo;
import com.google.gson.Gson;

@RestController
@RequestMapping(value = "/api")
public class VideoRestController {

	@Autowired
	private VideoService videoService;

	@RequestMapping(value = "/video/update")
	public RestResult<Void> updateVideo(String json) {
		try {
			System.out.println(json);
			videoService.updateVideo(new Gson().fromJson(json, VideoVo.class));
			return new RestResult<>(true);
		} catch (ServiceException e) {
			return new RestResult<>(false, e.getMessage());
		}
	}
}
