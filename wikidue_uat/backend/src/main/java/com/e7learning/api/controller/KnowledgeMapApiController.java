package com.e7learning.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.e7learning.api.domain.RestResult;
import com.e7learning.api.domain.param.CategoryQueryParam;
import com.e7learning.api.domain.param.TagQueryParam;
import com.e7learning.api.domain.result.CategoryResult;
import com.e7learning.api.domain.result.KnowledgeMapResult;
import com.e7learning.category.service.CategoryService;
import com.e7learning.common.exception.RestException;
import com.e7learning.common.service.KnowledgeMapService;

@RestController
@RequestMapping(value = "/api")
public class KnowledgeMapApiController {

	@Autowired
	private KnowledgeMapService knowledgeMapService;
	
	@Autowired
	private CategoryService categoryService;
	
	@RequestMapping(value = "/tag/query", method = { RequestMethod.POST })
	public RestResult<List<KnowledgeMapResult>> queryShareTag(@RequestBody TagQueryParam param) throws RestException {
		return new RestResult<>(true, knowledgeMapService.queryTag(param));
	}
	@RequestMapping(value = "/category/query", method = { RequestMethod.POST })
	public RestResult<List<CategoryResult>> queryTag(@RequestBody CategoryQueryParam param) throws RestException {
		return new RestResult<>(true, categoryService.queryCategory(param));
	}
}
