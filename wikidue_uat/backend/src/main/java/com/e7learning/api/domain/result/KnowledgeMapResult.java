package com.e7learning.api.domain.result;

import java.io.Serializable;

public class KnowledgeMapResult implements Serializable {

	private String tag;
	private String name;
	private String level;

	public KnowledgeMapResult() {
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + tag.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof KnowledgeMapResult) {
			return this.tag.equals(((KnowledgeMapResult) o).getTag());
		}
		return false;
	}
}
