package com.e7learning.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.e7learning.api.domain.RestResult;
import com.e7learning.api.domain.param.AdQueryParam;
import com.e7learning.api.domain.result.AdQueryResult;
import com.e7learning.api.service.AdApiService;

@RestController
@RequestMapping(value = "/api")
public class AdRestController {
	@Autowired
	private AdApiService adService;

	@RequestMapping(value = "/ad/query")
	public RestResult<List<AdQueryResult>> queryAd(@RequestBody AdQueryParam param) {
		return new RestResult<>(true, adService.queryAd(param));
	}
}
