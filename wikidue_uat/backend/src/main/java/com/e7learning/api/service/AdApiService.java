package com.e7learning.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.e7learning.api.domain.param.AdQueryParam;
import com.e7learning.api.domain.result.AdQueryResult;

@Service
public class AdApiService {

	public List<AdQueryResult> queryAd(AdQueryParam param){
		List<AdQueryResult> result = new ArrayList<>();
		AdQueryResult ad = new AdQueryResult();
		ad.setAdsName("廣告");
		ad.setDesc("描述");
		ad.setExternalLink("http://www.google.com");
		ad.setImage1("https://www.google.com.tw/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png");
		result.add(ad);
		return result;
	}
}
