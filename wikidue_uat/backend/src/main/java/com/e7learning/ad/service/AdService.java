package com.e7learning.ad.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.ad.form.AdForm;
import com.e7learning.common.Constant;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.service.FileStorageService;
import com.e7learning.repository.AdCategoryRepository;
import com.e7learning.repository.AdRoleGradeRepository;
import com.e7learning.repository.AdvertisementRepostiory;
import com.e7learning.repository.model.AdCategory;
import com.e7learning.repository.model.AdRoleGrade;
import com.e7learning.repository.model.Advertisement;
import com.e7learning.repository.model.FileStorage;

/**
 * @author ChrisTsai
 *
 */
@Service
public class AdService extends BaseService {

	private static final Logger log = Logger.getLogger(AdService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private ConfigService configService;

	@Autowired
	private AdvertisementRepostiory adRepostiory;

	@Autowired
	private AdRoleGradeRepository adRoleGradeRepository;

	@Autowired
	private AdCategoryRepository adCategoryRepository;

	@Autowired
	private FileStorageService fileStorageService;
	
	public Integer countBannerCode(String bannerCode) {
		return adRepostiory.countByBannerCode(bannerCode);
	}

	public Integer countBannerCode(Integer pkAdvertisement, String bannerCode) {
		return adRepostiory.countByPkAdvertisementNotAndBannerCode(pkAdvertisement, bannerCode);
	}

	public Page<Advertisement> queryAdvertisement(AdForm form, Integer page) throws ServiceException {
		try {
			Sort sort = new Sort(Direction.ASC, "publishEDate");
			Pageable pageable = new PageRequest(page - 1, configService.getPageSize(), sort);
			Page<Advertisement> result = adRepostiory.findAll(buildOperSpecification(form), pageable);
			return result;
		} catch (Exception e) {
			log.error("queryAdvertisement", e);
			throw new ServiceException(resources.getMessage("query.ad.error", null, null));
		}

	}

	@Transactional(readOnly = true)
	public AdForm findAdvertisement(Integer pkAdvertisement) throws ServiceException {
		try {
			if (adRepostiory.exists(pkAdvertisement)) {
				Advertisement tmp = null;
				if (isAdmin()) {
					tmp = adRepostiory.findOne(pkAdvertisement);
				} else {
					tmp = adRepostiory.findByCreateIdAndPkAdvertisement(getCurrentUserId(), pkAdvertisement);
				}
				if (tmp == null)
					throw new ServiceException(resources.getMessage("ad.not.exist.error", null, null));
				tmp.getRoleGrades().size();
				return toAdForm(tmp);
			} else {
				throw new ServiceException(resources.getMessage("ad.not.exist.error", null, null));
			}
		} catch (ServiceException e) {
			log.error("queryAdvertisement", e);
			throw e;
		} catch (Exception e) {
			log.error("queryAdvertisement", e);
			throw new ServiceException(resources.getMessage("query.ad.error", null, null));
		}

	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void updateAdvertisement(AdForm form) throws ServiceException {
		try {
			if (adRepostiory.exists(form.getPkAdvertisement())) {
				final Advertisement tmp = adRepostiory.findOne(form.getPkAdvertisement());
				if (!isAdmin() && !StringUtils.equals(tmp.getCreateId(), getCurrentUserId())) {
					throw new ServiceException(resources.getMessage("modify.ad.error", null, null));
				}
				tmp.setAdsName(form.getAdsName());
				tmp.setCategory(adCategoryRepository.findOne(form.getAdCategoryId()));
				tmp.setExternalLink(form.getExternalLink());
				if (form.getImageOneId() != null && !form.getImageOneId().isEmpty()) {
					fileStorageService.delFile(tmp.getImageOneId());
					tmp.setImageOneId(fileStorageService.saveFile(form.getImageOneId()).getPkFile());
				}
				if (form.getImageTwoId() != null && !form.getImageTwoId().isEmpty()) {
					fileStorageService.delFile(tmp.getImageTwoId());
					tmp.setImageTwoId(fileStorageService.saveFile(form.getImageTwoId()).getPkFile());
				}
				for (AdRoleGrade adRoleGrade : tmp.getRoleGrades()) {
					adRoleGradeRepository.delete(adRoleGrade);
				}

				List<AdRoleGrade> roleGrades = new ArrayList<>();
				Arrays.stream(form.getRoleGrades()).parallel().forEach(v -> {
					AdRoleGrade adroleGrade = new AdRoleGrade();
					adroleGrade.setRoleGradeCode(v);
					adroleGrade.setAd(tmp);
					adroleGrade.setActive(Constant.TRUE);
					adroleGrade.setCreateDt(new Date());
					adroleGrade.setCreateId(getCurrentUserId());
					roleGrades.add(adroleGrade);
					adRoleGradeRepository.save(adroleGrade);
				});
				tmp.setNotice(form.getNotice());
				tmp.setPublishEDate(form.getPublishEDate());
				tmp.setPublishSDate(form.getPublishSDate());
				tmp.setActive(form.getActive());
				tmp.setBannerCode(form.getBannerCode());
				tmp.setSupplier(form.getSupplier());
				tmp.setSupplierCode(form.getSupplierCode());
			} else {
				throw new ServiceException(resources.getMessage("ad.not.exist.error", null, null));
			}
		} catch (ServiceException e) {
			log.error("updateAdvertisement", e);
			throw e;
		} catch (Exception e) {
			log.error("updateAdvertisement", e);
			throw new ServiceException(resources.getMessage("modify.ad.error", null, null));
		}
		throw new MessageException(resources.getMessage("modify.ad.success", null, null));
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void createAdvertisement(AdForm form) throws ServiceException {
		try {

			Advertisement advertisement = new Advertisement();
			advertisement.setActive(Constant.FLASE);
			advertisement.setCreateDt(new Date());
			advertisement.setCreateId(getCurrentUserId());

			advertisement.setAdsName(form.getAdsName());
			advertisement.setExternalLink(form.getExternalLink());

			advertisement.setImageOneId(
					fileStorageService.careateFile(form.getImageOneId()).orElse(new FileStorage()).getPkFile());
			advertisement.setImageTwoId(
					fileStorageService.careateFile(form.getImageTwoId()).orElse(new FileStorage()).getPkFile());
			advertisement.setNotice(form.getNotice());
			advertisement.setPublishSDate(form.getPublishSDate());
			advertisement.setPublishEDate(form.getPublishEDate());

			AdCategory category = new AdCategory();
			category.setPkAdCategory(form.getAdCategoryId());
			advertisement.setCategory(category);

			advertisement.setActive(form.getActive());
			advertisement.setBannerCode(form.getBannerCode());
			advertisement.setSupplier(form.getSupplier());
			advertisement.setSupplierCode(form.getSupplierCode());

			advertisement = adRepostiory.save(advertisement);

			// List<AdRoleGrade> roleGrades = new ArrayList<>();
			for (String tmp : form.getRoleGrades()) {
				AdRoleGrade adroleGrade = new AdRoleGrade();
				adroleGrade.setRoleGradeCode(tmp);
				adroleGrade.setAd(advertisement);
				adroleGrade.setActive(Constant.TRUE);
				adroleGrade.setCreateDt(new Date());
				adroleGrade.setCreateId(getCurrentUserId());
				// roleGrades.add(adroleGrade);
				adRoleGradeRepository.save(adroleGrade);
			}

		} catch (Exception e) {
			log.error("createAdvertisement", e);
			throw new ServiceException(resources.getMessage("create.ad.error", null, null));
		}

		throw new MessageException(resources.getMessage("create.ad.success", null, null));
	}

	private Specification<Advertisement> buildOperSpecification(AdForm form) {
		return new Specification<Advertisement>() {
			@Override
			public Predicate toPredicate(Root<Advertisement> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

				List<Predicate> predicates = new ArrayList<>();

				if (!StringUtils.isEmpty(form.getAdsName())) {
					Path<String> np = root.get("adsName");
					predicates.add(cb.like(np, "%" + form.getAdsName() + "%"));
				}
				if (!predicates.isEmpty()) {
					return cb.and(predicates.toArray(new Predicate[predicates.size()]));
				}
				return cb.conjunction();
			}
		};
	}

	private AdForm toAdForm(Advertisement advertisement) {
		AdForm adForm = null;
		if (advertisement != null) {
			adForm = new AdForm();
			adForm.setAdCategoryId(advertisement.getCategory().getPkAdCategory());
			adForm.setAdsName(advertisement.getAdsName());
			adForm.setCreateDt(advertisement.getCreateDt());
			adForm.setCreateId(advertisement.getCreateId());
			adForm.setExternalLink(advertisement.getExternalLink());
			adForm.setImageOneId(fileStorageService.getFileStorage(advertisement.getImageOneId()).orElse(null));
			adForm.setImageTwoId(fileStorageService.getFileStorage(advertisement.getImageTwoId()).orElse(null));
			adForm.setNotice(advertisement.getNotice());
			adForm.setPkAdvertisement(advertisement.getPkAdvertisement());
			adForm.setPublishEDate(advertisement.getPublishEDate());
			adForm.setPublishSDate(advertisement.getPublishSDate());
			List<AdRoleGrade> roles = advertisement.getRoleGrades();
			if (roles != null) {
				adForm.setRoleGrades(roles.stream().map(AdRoleGrade::getRoleGradeCode).toArray(String[]::new));
			}
			adForm.setUpdateDt(advertisement.getUpdateDt());
			adForm.setActive(advertisement.getActive());
			adForm.setBannerCode(advertisement.getBannerCode());
			adForm.setSupplier(advertisement.getSupplier());
			adForm.setSupplierCode(advertisement.getSupplierCode());
		}

		return adForm;
	}
}
