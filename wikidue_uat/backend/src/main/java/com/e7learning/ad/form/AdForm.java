package com.e7learning.ad.form;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import com.e7learning.common.enums.AdCategoryTypeEnum;
import com.e7learning.common.enums.RoleGradeCodeEnum;
import com.e7learning.common.form.BaseForm;
import com.e7learning.repository.model.AdCategory;
import com.e7learning.repository.model.Advertisement;

/**
 * The Class AdForm.
 *
 * @author ChrisTsai
 */
public class AdForm extends BaseForm<Advertisement>{

	/** The pk advertisement. */
	private Integer pkAdvertisement;

	/** The ads name. */
	private String adsName;

	/** The create dt. */
	private Date createDt;

	/** The create id. */
	private String createId;

	/** The external link. */
	private String externalLink;

	/** The image one id. */
	private MultipartFile imageOneId;

	/** The image two id. */
	private MultipartFile imageTwoId;
	
	/** The image one id url. */
	private String imageOneIdUrl;

	/** The image two id url. */
	private String imageTwoIdUrl;

	/** The notice. */
	private String notice;

	/** The publish E date. */
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date publishEDate;

	/** The publish S date. */
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date publishSDate;

	/** The update dt. */
	private Date updateDt;
	
	/** 廣告分類. */
	private Integer adCategoryId;
	
	/** 分類類型選單 */
	private Map<String, List<AdCategory>> categoryList;
	
	/** 分類類型選單 */
	private List<AdCategoryTypeEnum> categoryTypeList = Arrays.asList(AdCategoryTypeEnum.values());
	
	/** 分類類型選單 */
	private List<RoleGradeCodeEnum> roleGradeList = Arrays.asList(RoleGradeCodeEnum.values());
	
	private String roleGrades[];
	
	/** 啟用 */
	private String  active;
	
	/** 廣告編號 */
	private String bannerCode;
	
	/** 供應廠商 */
	private String supplier;
	
	/** 廠商編號 */
	private String supplierCode;
	
	/**
	 * Gets the pk advertisement.
	 *
	 * @return the pk advertisement
	 */
	public Integer getPkAdvertisement() {
		return pkAdvertisement;
	}

	/**
	 * Sets the pk advertisement.
	 *
	 * @param pkAdvertisement the new pk advertisement
	 */
	public void setPkAdvertisement(Integer pkAdvertisement) {
		this.pkAdvertisement = pkAdvertisement;
	}

	/**
	 * Gets the ads name.
	 *
	 * @return the ads name
	 */
	public String getAdsName() {
		return adsName;
	}

	/**
	 * Sets the ads name.
	 *
	 * @param adsName the new ads name
	 */
	public void setAdsName(String adsName) {
		this.adsName = adsName;
	}

	/**
	 * Gets the creates the dt.
	 *
	 * @return the creates the dt
	 */
	public Date getCreateDt() {
		return createDt;
	}

	/**
	 * Sets the creates the dt.
	 *
	 * @param createDt the new creates the dt
	 */
	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	/**
	 * Gets the creates the id.
	 *
	 * @return the creates the id
	 */
	public String getCreateId() {
		return createId;
	}

	/**
	 * Sets the creates the id.
	 *
	 * @param createId the new creates the id
	 */
	public void setCreateId(String createId) {
		this.createId = createId;
	}

	/**
	 * Gets the external link.
	 *
	 * @return the external link
	 */
	public String getExternalLink() {
		return externalLink;
	}

	/**
	 * Sets the external link.
	 *
	 * @param externalLink the new external link
	 */
	public void setExternalLink(String externalLink) {
		this.externalLink = externalLink;
	}

	/**
	 * Gets the notice.
	 *
	 * @return the notice
	 */
	public String getNotice() {
		return notice;
	}

	/**
	 * Sets the notice.
	 *
	 * @param notice the new notice
	 */
	public void setNotice(String notice) {
		this.notice = notice;
	}

	/**
	 * Gets the publish E date.
	 *
	 * @return the publish E date
	 */
	public Date getPublishEDate() {
		return publishEDate;
	}

	/**
	 * Sets the publish E date.
	 *
	 * @param publishEDate the new publish E date
	 */
	public void setPublishEDate(Date publishEDate) {
		this.publishEDate = publishEDate;
	}

	/**
	 * Gets the publish S date.
	 *
	 * @return the publish S date
	 */
	public Date getPublishSDate() {
		return publishSDate;
	}

	/**
	 * Sets the publish S date.
	 *
	 * @param publishSDate the new publish S date
	 */
	public void setPublishSDate(Date publishSDate) {
		this.publishSDate = publishSDate;
	}

	/**
	 * Gets the update dt.
	 *
	 * @return the update dt
	 */
	public Date getUpdateDt() {
		return updateDt;
	}

	/**
	 * Sets the update dt.
	 *
	 * @param updateDt the new update dt
	 */
	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	/**
	 * Gets the image one id url.
	 *
	 * @return the image one id url
	 */
	public String getImageOneIdUrl() {
		return imageOneIdUrl;
	}

	/**
	 * Sets the image one id url.
	 *
	 * @param imageOneIdUrl the new image one id url
	 */
	public void setImageOneIdUrl(String imageOneIdUrl) {
		this.imageOneIdUrl = imageOneIdUrl;
	}

	/**
	 * Gets the image two id url.
	 *
	 * @return the image two id url
	 */
	public String getImageTwoIdUrl() {
		return imageTwoIdUrl;
	}

	/**
	 * Sets the image two id url.
	 *
	 * @param imageTwoIdUrl the new image two id url
	 */
	public void setImageTwoIdUrl(String imageTwoIdUrl) {
		this.imageTwoIdUrl = imageTwoIdUrl;
	}

	/**
	 * Sets the image one id.
	 *
	 * @param imageOneId the new image one id
	 */
	public void setImageOneId(MultipartFile imageOneId) {
		this.imageOneId = imageOneId;
	}

	/**
	 * Sets the image two id.
	 *
	 * @param imageTwoId the new image two id
	 */
	public void setImageTwoId(MultipartFile imageTwoId) {
		this.imageTwoId = imageTwoId;
	}

	/**
	 * Gets the image one id.
	 *
	 * @return the image one id
	 */
	public MultipartFile getImageOneId() {
		return imageOneId;
	}

	/**
	 * Gets the image two id.
	 *
	 * @return the image two id
	 */
	public MultipartFile getImageTwoId() {
		return imageTwoId;
	}

	public Map<String, List<AdCategory>> getCategoryList() {
		return categoryList;
	}

	public AdForm setCategoryList(Map<String, List<AdCategory>> categoryList) {
		this.categoryList = categoryList;
		return this;
	}

	public List<AdCategoryTypeEnum> getCategoryTypeList() {
		return categoryTypeList;
	}

	public Integer getAdCategoryId() {
		return adCategoryId;
	}

	public void setAdCategoryId(Integer adCategoryId) {
		this.adCategoryId = adCategoryId;
	}

	public List<RoleGradeCodeEnum> getRoleGradeList() {
		return roleGradeList;
	}

	public String[] getRoleGrades() {
		return roleGrades;
	}

	public void setRoleGrades(String[] roleGrades) {
		this.roleGrades = roleGrades;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getBannerCode() {
		return bannerCode;
	}

	public void setBannerCode(String bannerCode) {
		this.bannerCode = bannerCode;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	
	
	
}
