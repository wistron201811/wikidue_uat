package com.e7learning.ad.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections4.map.LinkedMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.ad.form.AdCategoryForm;
import com.e7learning.common.Constant;
import com.e7learning.common.enums.AdCategoryTypeEnum;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.repository.AdCategoryRepository;
import com.e7learning.repository.model.AdCategory;

/**
 * @author J
 *
 */
@Service
public class AdCategoryService extends BaseService {

	private static final Logger log = Logger.getLogger(AdCategoryService.class);

	@Autowired
	private AdCategoryRepository categoryRepository;

	@Autowired
	private ConfigService configService;

	@Autowired
	private MessageSource resources;

	public Integer countCategoryCode(String categoryCode) {
		return categoryRepository.countByCategoryCode(categoryCode);
	}

	public Integer countCategoryCode(Integer pkAdCategory, String categoryCode) {
		return categoryRepository.countByPkAdCategoryNotAndCategoryCode(pkAdCategory, categoryCode);
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void addCategory(AdCategoryForm form) throws ServiceException {
		try {
			AdCategory category = new AdCategory();
			BeanUtils.copyProperties(category, form);
			category.setUpdateDt(new Date());
			category.setCreateDt(new Date());
			category.setCreateId(getCurrentUserId());
			categoryRepository.save(category);
		} catch (Exception e) {
			log.error("addCategory", e);
			throw new ServiceException(resources.getMessage("op.add.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.add.success", null, null));
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void saveCategory(AdCategoryForm form) throws ServiceException {
		try {
			AdCategory category = findByPkAdCategory(form.getPkAdCategory());
			if (category != null) {
				BeanUtils.copyProperties(category, form);
				category.setUpdateDt(new Date());
				categoryRepository.save(category);
			}
		} catch (Exception e) {
			log.error("editCategory", e);
			throw new ServiceException(resources.getMessage("op.modify.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.modify.success", null, null));
	}

	public Map<String, List<AdCategory>> queryActiveCategory() throws ServiceException {
		try {
			Map<String, List<AdCategory>> result = new LinkedMap<>();
			result.put(AdCategoryTypeEnum.E7.toString(),
					categoryRepository.findByCategoryTypeAndActive(AdCategoryTypeEnum.E7.toString(), Constant.TRUE));
			result.put(AdCategoryTypeEnum.EVALUATE.toString(), categoryRepository
					.findByCategoryTypeAndActive(AdCategoryTypeEnum.EVALUATE.toString(), Constant.TRUE));
			return result;
		} catch (Exception e) {
			log.error("queryCategory", e);
			throw new ServiceException(resources.getMessage("op.query.error", null, null));
		}
	}

	public Page<AdCategory> queryCategory(AdCategoryForm form, Integer page) throws ServiceException {
		try {
			Sort sort = new Sort(Direction.ASC, "categoryName");
			Pageable pageable = new PageRequest(page - 1, configService.getPageSize(), sort);
			Page<AdCategory> result = categoryRepository.findAll(buildOperSpecification(form), pageable);
			return result;
		} catch (Exception e) {
			log.error("queryCategory", e);
			throw new ServiceException(resources.getMessage("op.query.error", null, null));
		}
	}

	public AdCategory findByPkAdCategory(int pkAdCategory) {
		return categoryRepository.findOne(pkAdCategory);
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void remove(int id) throws ServiceException {
		try {
			if (categoryRepository.exists(id))
				categoryRepository.delete(id);
		} catch (Exception e) {
			log.error("removeCategory", e);
			throw new ServiceException(resources.getMessage("op.del.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.del.success", null, null));
	}

	private Specification<AdCategory> buildOperSpecification(AdCategoryForm form) {
		return new Specification<AdCategory>() {
			@Override
			public Predicate toPredicate(Root<AdCategory> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

				List<Predicate> predicates = new ArrayList<>();

				if (StringUtils.isNotEmpty(form.getCategoryName())) {
					Path<String> np = root.get("categoryName");
					predicates.add(cb.like(np, "%" + form.getCategoryName() + "%"));
				}
				if (StringUtils.isNotEmpty(form.getCategoryCode())) {
					Path<String> np = root.get("categoryCode");
					predicates.add(cb.like(np, "%" + form.getCategoryCode() + "%"));
				}
				if (StringUtils.isNotEmpty(form.getCategoryType())) {
					Path<String> np = root.get("categoryType");
					predicates.add(cb.equal(np, form.getCategoryType()));
				}
				if (!predicates.isEmpty()) {
					return cb.and(predicates.toArray(new Predicate[predicates.size()]));
				}
				return cb.conjunction();
			}
		};
	}
}
