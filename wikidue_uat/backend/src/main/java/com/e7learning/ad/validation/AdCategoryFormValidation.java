package com.e7learning.ad.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.e7learning.ad.form.AdCategoryForm;
import com.e7learning.ad.service.AdCategoryService;

@Component
public class AdCategoryFormValidation implements Validator {

	@Autowired
	private AdCategoryService adCategoryService;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(AdCategoryForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		AdCategoryForm form = (AdCategoryForm) target;
		if (StringUtils.equalsIgnoreCase("save", form.getMethod())
				|| StringUtils.equalsIgnoreCase("modify", form.getMethod())) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "categoryName", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "categoryCode", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "categoryType", "validate.empty.message");
		}
		if (errors.hasErrors()) {
			return;
		}

		if (StringUtils.equalsIgnoreCase("save", form.getMethod())
				&& adCategoryService.countCategoryCode(form.getCategoryCode()) > 0) {
			errors.rejectValue("categoryCode", "validate.duplicate.message", new String[] { "分類編號" }, "");
		}

		if (StringUtils.equalsIgnoreCase("modify", form.getMethod())
				&& adCategoryService.countCategoryCode(form.getPkAdCategory(), form.getCategoryCode()) > 0) {
			errors.rejectValue("categoryCode", "validate.duplicate.message", new String[] { "分類編號" }, "");
		}

	}
}