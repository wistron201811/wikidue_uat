package com.e7learning.ad.validation;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.e7learning.ad.form.AdForm;
import com.e7learning.ad.service.AdService;

@Component
public class AdFormValidation implements Validator {

	@Autowired
	private AdService adService;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(AdForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		AdForm form = (AdForm) target;
		if (StringUtils.equalsIgnoreCase("save", form.getMethod())
				|| StringUtils.equalsIgnoreCase("modify", form.getMethod())) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "adsName", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "adCategoryId", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "roleGrades", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "imageOneId", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "publishEDate", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "publishSDate", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "active", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bannerCode", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "supplier", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "supplierCode", "validate.empty.message");
		}
		if (errors.hasErrors()) {
			return;
		}

		// 檢查格式
		UrlValidator urlValidator = new UrlValidator();
		if (StringUtils.isNotBlank(form.getExternalLink()) && !urlValidator.isValid(form.getExternalLink())) {
			errors.rejectValue("externalLink", "validate.url.format");
		}

		if (StringUtils.equalsIgnoreCase("save", form.getMethod()) && form.getImageOneId().isEmpty()) {
			errors.rejectValue("imageOneId", "validate.empty.message");
		}

		if (StringUtils.equalsIgnoreCase("save", form.getMethod())
				&& adService.countBannerCode(form.getBannerCode()) > 0) {
			errors.rejectValue("bannerCode", "validate.duplicate.message", new String[] { "廣告編號" }, "");
		}

		if (StringUtils.equalsIgnoreCase("modify", form.getMethod())
				&& adService.countBannerCode(form.getPkAdvertisement(), form.getBannerCode()) > 0) {
			errors.rejectValue("bannerCode", "validate.duplicate.message", new String[] { "廣告編號" }, "");
		}

	}

}
