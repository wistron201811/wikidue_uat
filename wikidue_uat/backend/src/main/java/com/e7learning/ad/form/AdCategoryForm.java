package com.e7learning.ad.form;

import java.util.Arrays;
import java.util.List;

import com.e7learning.common.enums.AdCategoryTypeEnum;
import com.e7learning.common.form.BaseForm;
import com.e7learning.repository.model.AdCategory;

/**
 * @author J
 * 
 * 分類管理
 *
 */
public class AdCategoryForm extends BaseForm<AdCategory>{
	/**
	 * 分類PK
	 */
	private int pkAdCategory;
	/**
	 * 分類編號
	 */
	private String categoryCode;
	/**
	 * 分類名稱
	 */
	private String categoryName;
	/**
	 * 分類類型
	 */
	private String categoryType;
	/**
	 * 啟用
	 */
	private String  active;
	/**
	 * 操作方法
	 */
	private String method;
	/**
	 * 分類類型選單
	 */
	private List<AdCategoryTypeEnum> categoryTypeList = Arrays.asList(AdCategoryTypeEnum.values());;
	
	public AdCategoryForm() {
		
	}
	public int getPkAdCategory() {
		return pkAdCategory;
	}
	public void setPkAdCategory(int pkAdCategory) {
		this.pkAdCategory = pkAdCategory;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public List<AdCategoryTypeEnum> getCategoryTypeList() {
		return categoryTypeList;
	}
}
