package com.e7learning.ad.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.e7learning.ad.form.AdForm;
import com.e7learning.ad.service.AdCategoryService;
import com.e7learning.ad.service.AdService;
import com.e7learning.ad.validation.AdFormValidation;
import com.e7learning.common.controller.BaseController;
import com.e7learning.common.exception.ServiceException;

/**
 * @author ChrisTsai
 *
 */
@Controller
@RequestMapping(value = "/ad")
@SessionAttributes(AdController.FORM_BEAN_NAME)
public class AdController extends BaseController {

	private static final Logger log = Logger.getLogger(AdController.class);

	public static final String FORM_BEAN_NAME = "adForm";

	private static final String QUERY_PAGE = "queryAdPage";

	private static final String EDIT_PAGE = "editAdPage";

	private static final String ADD_PAGE = "addAdPage";

	@Autowired
	private AdFormValidation adFormValidation;

	@Autowired
	private AdService adService;

	@Autowired
	private AdCategoryService adCategoryService;

	@InitBinder(AdController.FORM_BEAN_NAME)
	protected void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
		binder.setValidator(adFormValidation);
	}

	@RequestMapping(value = "/query", method = { RequestMethod.GET })
	public String goQuery(Model model) {
		AdForm form = new AdForm();
		try {
			form.setResult(adService.queryAdvertisement(form, 1));
		} catch (ServiceException e) {

		}
		model.addAttribute(FORM_BEAN_NAME, form);
		return QUERY_PAGE;
	}

	@RequestMapping(value = "/query", params = "method=query")
	public String query(HttpServletRequest request, AdForm form) {

		String pageName = new ParamEncoder("result").encodeParameterName(TableTagParameters.PARAMETER_PAGE);
		int nextPage = getPage(request.getParameter(pageName));
		log.info("nextPage:" + nextPage);
		try {
			form.setResult(adService.queryAdvertisement(form, nextPage));
		} catch (ServiceException e) {

		}
		return QUERY_PAGE;
	}

	@RequestMapping(value = "/add", method = { RequestMethod.GET })
	public String goAdd(Model model) {
		try {
			model.addAttribute(FORM_BEAN_NAME, new AdForm().setCategoryList(adCategoryService.queryActiveCategory()));
		} catch (ServiceException e) {
			model.addAttribute(new AdForm());
		}
		return ADD_PAGE;
	}

	@RequestMapping(value = "/add", params = "method=save", method = { RequestMethod.POST })
	public String add(@Validated AdForm form, BindingResult result) {
		if (result.hasErrors()) {
			return ADD_PAGE;
		}
		try {
			adService.createAdvertisement(form);
			return "redirect:/ad/query?method=query";
		} catch (ServiceException e) {

		}
		return ADD_PAGE;

	}

	@RequestMapping(value = "/modify", method = { RequestMethod.POST })
	public String goModify(Model model, AdForm form) {
		try {
			form = adService.findAdvertisement(form.getPkAdvertisement())
					.setCategoryList(adCategoryService.queryActiveCategory());
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute(FORM_BEAN_NAME, form);
		return EDIT_PAGE;
	}

	@RequestMapping(params = "method=modify", value = "/modify", method = { RequestMethod.POST })
	public String modify(@Validated AdForm form, BindingResult result) {
		if (result.hasErrors()) {
			return EDIT_PAGE;
		}
		try {
			adService.updateAdvertisement(form);
			return "redirect:/ad/query?method=query";
		} catch (ServiceException e) {

		}
		return EDIT_PAGE;
	}

}
