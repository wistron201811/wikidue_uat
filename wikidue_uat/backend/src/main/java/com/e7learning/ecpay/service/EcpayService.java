package com.e7learning.ecpay.service;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.utils.ParamUtils;
import com.e7learning.invoice.integration.InvoiceAllInOne;
import com.e7learning.invoice.integration.domain.AllowanceObj;
import com.e7learning.invoice.integration.domain.IssueInvalidObj;
import com.e7learning.invoice.integration.domain.IssueObj;
import com.e7learning.payment.integration.PaymentAllInOne;
import com.e7learning.payment.integration.domain.DoActionObj;
import com.google.gson.Gson;

@Service
public class EcpayService {

	private static final Logger LOG = Logger.getLogger(EcpayService.class);

	@Autowired
	private PaymentAllInOne paymentAllInOne;

	@Autowired
	private InvoiceAllInOne invoiceAllInOne;

	/**
	 * 電子發票作廢
	 */
	public void invalidInvoice(String invoiceNo, String reason) throws ServiceException {
		try {
			IssueInvalidObj obj = new IssueInvalidObj();
			obj.setInvoiceNumber(invoiceNo);
			obj.setReason(reason);
			LOG.error("invalidInvoice param:" + new Gson().toJson(obj));
			String result = invoiceAllInOne.issueInvalid(obj);
			LOG.error("invalidInvoice:" + result);
			Map<String, String> resultMap = ParamUtils.solveParams(result);
			LOG.error("invalidInvoice result:" + new Gson().toJson(resultMap));
			if (resultMap == null || !StringUtils.equals(resultMap.get("RtnCode"), "1")) {
				throw new ServiceException("作廢發票失敗");
			}
		} catch (ServiceException e) {
			throw e;
		} catch (Exception e) {
			LOG.error("invalidInvoice", e);
		}
	}

	/**
	 * 信用卡放棄交易
	 * 
	 * @throws ServiceException
	 */
	public boolean giveUpTrade(String merchantTradeNo, String tradeNo, Integer totalAmount) throws ServiceException {
		try {
			DoActionObj doActionObj = new DoActionObj();
			doActionObj.setAction("N");
			doActionObj.setMerchantTradeNo(merchantTradeNo);
			doActionObj.setTotalAmount(String.valueOf(totalAmount));
			doActionObj.setTradeNo(tradeNo);
			LOG.error("giveUpTrade param:" + new Gson().toJson(doActionObj));
			String result = paymentAllInOne.doAction(doActionObj);
			Map<String, String> resultMap = ParamUtils.solveParams(result);
			LOG.error("giveUpTrade result:" + new Gson().toJson(resultMap));
			if (resultMap == null || !StringUtils.equals(resultMap.get("RtnCode"), "1")) {
				throw new ServiceException("放棄信用卡交易失敗");
			}
			return true;
		} catch (ServiceException e) {
			throw e;
		} catch (Exception e) {
			LOG.error("giveUpTrade", e);
		}
		return false;
	}

	/**
	 * 信用卡交易關帳
	 */
	public void closeTrade(String merchantTradeNo, String tradeNo, Integer totalAmount) throws ServiceException {
		try {
			DoActionObj doActionObj = new DoActionObj();
			doActionObj.setAction("C");
			doActionObj.setMerchantTradeNo(merchantTradeNo);
			doActionObj.setTotalAmount(String.valueOf(totalAmount));
			doActionObj.setTradeNo(tradeNo);
			LOG.error("closeTrade param:" + new Gson().toJson(doActionObj));
			String result = paymentAllInOne.doAction(doActionObj);
			Map<String, String> resultMap = ParamUtils.solveParams(result);
			LOG.error("closeTrade result:" + new Gson().toJson(resultMap));
		} catch (Exception e) {
			LOG.error("closeTrade", e);
		}
	}

	/**
	 * 信用卡交易退刷(需先關帳)
	 */
	public boolean returnTrade(String merchantTradeNo, String tradeNo, Integer totalAmount) throws ServiceException {
		try {
			DoActionObj doActionObj = new DoActionObj();
			doActionObj.setAction("R");
			doActionObj.setMerchantTradeNo(merchantTradeNo);
			doActionObj.setTotalAmount(String.valueOf(totalAmount));
			doActionObj.setTradeNo(tradeNo);
			LOG.error("returnTrade param:" + new Gson().toJson(doActionObj));
			String result = paymentAllInOne.doAction(doActionObj);
			Map<String, String> resultMap = ParamUtils.solveParams(result);
			LOG.error("returnTrade result:" + new Gson().toJson(resultMap));
			if (resultMap == null || !StringUtils.equals(resultMap.get("RtnCode"), "1")) {
				throw new ServiceException("退刷失敗");
			}
			return true;
		} catch (ServiceException e) {
			throw e;
		} catch (Exception e) {
			LOG.error("returnTrade", e);
		}
		return false;
	}

	/**
	 * 開立發票
	 */
	public Map<String, String> issueInvoice(IssueObj issueObj) {
		try {
			LOG.error("issue invoice param:" + new Gson().toJson(issueObj));
			String issueResult = invoiceAllInOne.issue(issueObj);
			Map<String, String> resultMap = ParamUtils.solveParams(issueResult);
			LOG.error("issue invoice result:" + new Gson().toJson(resultMap));
			return resultMap;
		} catch (Exception e) {
			LOG.error("issueInvoice", e);
		}
		return null;
	}

	/**
	 * 折讓發票
	 */
	public Map<String, String> allowanceInvoice(AllowanceObj allowanceObj) {
		try {
			LOG.error("allowance invoice param:" + new Gson().toJson(allowanceObj));
			String issueResult = invoiceAllInOne.allowance(allowanceObj);
			Map<String, String> resultMap = ParamUtils.solveParams(issueResult);
			LOG.error("allowance invoice result:" + new Gson().toJson(resultMap));
			return resultMap;
		} catch (Exception e) {
			LOG.error("allowanceInvoice", e);
		}
		return null;
	}
}
