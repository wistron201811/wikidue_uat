package com.e7learning.po.vo;

import java.io.Serializable;

public class ReturnOrderVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean refund;
	
	private Integer orderDetailNo;
	
//	private Integer returnQuantity;

	public Integer getOrderDetailNo() {
		return orderDetailNo;
	}

	public void setOrderDetailNo(Integer orderDetailNo) {
		this.orderDetailNo = orderDetailNo;
	}

//	public Integer getReturnQuantity() {
//		return returnQuantity;
//	}
//
//	public void setReturnQuantity(Integer returnQuantity) {
//		this.returnQuantity = returnQuantity;
//	}

	public boolean isRefund() {
		return refund;
	}

	public void setRefund(boolean refund) {
		this.refund = refund;
	}

}
