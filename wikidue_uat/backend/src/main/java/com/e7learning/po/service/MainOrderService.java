package com.e7learning.po.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.common.Constant;
import com.e7learning.common.enums.OrderStatusEnum;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.CommonService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.service.MailService;
import com.e7learning.common.service.TemplateService;
import com.e7learning.common.utils.CommonUtils;
import com.e7learning.ecpay.service.EcpayService;
import com.e7learning.invoice.integration.domain.AllowanceObj;
import com.e7learning.invoice.integration.domain.IssueObj;
import com.e7learning.po.form.MainOrderForm;
import com.e7learning.repository.E7MessageRepository;
import com.e7learning.repository.MainOrderRepository;
import com.e7learning.repository.ReturnRecordDetailRepository;
import com.e7learning.repository.ReturnRecordRepository;
import com.e7learning.repository.SubOrderDetailHisRepository;
import com.e7learning.repository.SubOrderDetailRepository;
import com.e7learning.repository.SubOrderRepository;
import com.e7learning.repository.UserCourseRepository;
import com.e7learning.repository.UserInfoRepository;
import com.e7learning.repository.model.Course;
import com.e7learning.repository.model.E7Message;
import com.e7learning.repository.model.MainOrder;
import com.e7learning.repository.model.MsgCategory;
import com.e7learning.repository.model.Payment;
import com.e7learning.repository.model.ReturnRecord;
import com.e7learning.repository.model.ReturnRecordDetail;
import com.e7learning.repository.model.SubOrder;
import com.e7learning.repository.model.SubOrderDetail;
import com.e7learning.repository.model.SubOrderDetailHis;
import com.e7learning.repository.model.UserCourse;
import com.e7learning.repository.model.UserInfo;
import com.e7learning.repository.model.Vendor;

/**
 * @author J
 *
 */
@Service
public class MainOrderService extends BaseService {

	private static final Logger log = Logger.getLogger(MainOrderService.class);

	@Autowired
	private ConfigService configService;

	@Autowired
	private MessageSource resources;

	@Autowired
	private ReturnRecordRepository returnRecordRepository;

	@Autowired
	private ReturnRecordDetailRepository returnRecordDetailRepository;

	@Autowired
	private MainOrderRepository mainOrderRepository;

	@Autowired
	private SubOrderRepository subOrderRepository;

	@Autowired
	private SubOrderDetailRepository subOrderDetailRepository;

	@Autowired
	private UserCourseRepository userCourseRepository;

	@Autowired
	private UserInfoRepository userInfoRepository;

	@Autowired
	private EcpayService ecpayService;

	@Autowired
	private MailService mailService;

	@Autowired
	private TemplateService templateService;

	@Autowired
	private CommonService commonService;

	@Autowired
	private E7MessageRepository e7MessageRepository;

	@Autowired
	private SubOrderDetailHisRepository subOrderDetailHisRepository;

	private static final String DATE_FORMAT = "yyyy/MM/dd";

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void deliever(MainOrderForm form) throws ServiceException {
		boolean result = false;
		Vendor vendor = null;
		MainOrder mainOrder = null;
		List<SubOrderDetail> productList = new ArrayList<>();
		try {
			SubOrder subOrder = subOrderRepository.findOne((Integer.valueOf(form.getPkSubOrder())));
			if (subOrder != null) {
				// 登入廠商與訂單廠商不同時，不得操作
				this.checkVendorAuth(subOrder);
				Date now = new Date();
				subOrder.setUpdateDt(now);
				subOrder.setLogistics(form.getLogistics());
				subOrder.setLogisticsNum(form.getLogisticsNum());
				subOrder.setOrderStatus(OrderStatusEnum.D.name());
				subOrder.setDeliveryDt(now);
				// 寫入出貨狀態
				List<SubOrderDetail> list = subOrder.getSubOrderDetailList();
				if (list != null) {
					for (SubOrderDetail detail : list) {
						// 教具才可出貨
						if (detail.getProduct().getProps() != null
								&& StringUtils.equalsIgnoreCase(OrderStatusEnum.P.name(), detail.getStatus())) {
							detail.setStatus(OrderStatusEnum.D.name());
							detail.setLastModifiedId(getCurrentUserId());
							detail.setUpdateDt(now);
							// 寫入出貨日期
							detail.setDeliveryDt(now);
							productList.add(detail);
						}
					}
				}
				subOrder.setLastModifiedId(getCurrentUserId());
				vendor = subOrder.getVendor();
				mainOrder = subOrder.getMainOrder();
				result = true;
			}
		} catch (ServiceException e) {
			log.error("deliever", e);
			throw e;

		} catch (Exception e) {
			log.error("deliever", e);
			throw new ServiceException(resources.getMessage("op.modify.error", null, null));
		} finally {
			if (result) {
				try {
					Date now = new Date();
					Date sendDate = now;
					Date shipDate = now;
					String orderNo = mainOrder.getPkMainOrder();
					List<String> adminEmailList = commonService.getAdminEmailList();
					List<Map<String, String>> details = new ArrayList<>();
					String buyerName = mainOrder.getPurchaser();
					for (SubOrderDetail detail : productList) {
						Map<String, String> map = new HashMap<>();
						map.put("name", detail.getProductName());
						map.put("quantity", String.valueOf(detail.getQuantity()));
						details.add(map);
					}
					mailService.sendNotificationAdminEmail(adminEmailList.toArray(new String[adminEmailList.size()]),
							new String[] {
									StringUtils.isEmpty(vendor.getEmail()) ? vendor.getUid() : vendor.getEmail() },
							sendDate, configService.getAdminName(), orderNo, shipDate, details);
					if (mainOrder != null) {
						SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
						mailService.sendNotificationBuyerEmail(new String[] { mainOrder.getReceiptEmail() },
								new String[] {}, sendDate, buyerName, shipDate, orderNo, details);
						sendNotification(
								String.format("Wikidue Store 教育商城 - 訂單編號 %s 出貨通知信", mainOrder.getPkMainOrder()),
								templateService.getSendNotificationBuyer(configService.getStoreUrl(),
										sdf.format(sendDate), buyerName, sdf.format(shipDate),
										configService.getMyOrderUrl(), orderNo, details, configService.getImageUrl()),
								mainOrder.getCreateId(), 1);
					}
				} catch (Exception e) {
					log.error("deliver email notification", e);
				}
			}
		}
		throw new MessageException(resources.getMessage("op.modify.success", null, null));
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void checkVendorAuth(SubOrder subOrder) throws ServiceException {
		if (subOrder != null) {
			// 登入廠商與訂單廠商不同時，不得操作
			if (!subOrder.getVendor().getUid().equalsIgnoreCase(this.getCurrentUserId())) {
				throw new ServiceException(resources.getMessage("access.denied", null, null));
			}
		}
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public MainOrder getMainOrder(String pkMainOrder) {
		MainOrder mainOrder = mainOrderRepository.findOne(pkMainOrder);
		if (mainOrder != null) {
			List<SubOrder> list = mainOrder.getSubOrderList();
			if (list != null) {
				list.size();
				for (SubOrder so : list) {
					List<SubOrderDetail> subList = so.getSubOrderDetailList();
					if (subList != null) {
						subList.size();
					}
				}
			}
		}
		return mainOrder;
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public SubOrder getSubOrder(Integer pkSubOrder) {
		SubOrder subOrder = subOrderRepository.findOne(pkSubOrder);
		if (subOrder != null) {
			List<SubOrderDetail> subList = subOrder.getSubOrderDetailList();
			if (subList != null) {
				subList.size();
			}
		}
		return subOrder;
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public List<SubOrderDetail> getAllSubOrderDetail(String pkMainOrder) {
		List<SubOrderDetail> resultList = null;
		MainOrder mainOrder = mainOrderRepository.findOne(pkMainOrder);
		if (mainOrder != null) {
			resultList = new ArrayList<SubOrderDetail>();
			List<SubOrder> list = mainOrder.getSubOrderList();
			if (list != null && list.size() > 0) {
				for (SubOrder so : list) {
					List<SubOrderDetail> subOrderDetail = so.getSubOrderDetailList();
					if (subOrderDetail != null && subOrderDetail.size() > 0) {
						resultList.addAll(subOrderDetail);
					}
				}
			}
		}
		return resultList;
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public Page<MainOrder> queryOrderAdmin(MainOrderForm form, Integer page) throws ServiceException {
		try {
			Sort sort = new Sort(Direction.DESC, "orderDt");
			Pageable pageable = new PageRequest(page - 1, configService.getPageSize(), sort);
			Page<MainOrder> result = mainOrderRepository.findAll(buildOperSpecification(form), pageable);
			return result;
		} catch (Exception e) {
			log.error("queryOrderAdmin", e);
			throw new ServiceException(resources.getMessage("op.query.error", null, null));
		}
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public Integer queryOrderTotalAdmin(MainOrderForm form) throws ServiceException {
		Integer total = new Integer(0);
		try {
			List<MainOrder> result = mainOrderRepository.findAll(buildOperSpecification(form));
			for (MainOrder mainOrder : result) {
				total += mainOrder.getOrderAmount();
			}
			return total;
		} catch (Exception e) {
			log.error("queryOrderAdmin", e);
			throw new ServiceException(resources.getMessage("op.query.error", null, null));
		}
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public Page<SubOrder> queryOrder(MainOrderForm form, Integer page) throws ServiceException {
		try {
			Sort sort = new Sort(Direction.DESC, "mainOrder.orderDt");
			Pageable pageable = new PageRequest(page - 1, configService.getPageSize(), sort);
			Page<SubOrder> result = subOrderRepository.findAll(buildOperSpecification1(form), pageable);
			return result;
		} catch (Exception e) {
			log.error("queryOrder", e);
			throw new ServiceException(resources.getMessage("op.query.error", null, null));
		}
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public Integer queryOrderTotal(MainOrderForm form) throws ServiceException {
		Integer total = new Integer(0);
		try {
			List<SubOrder> result = subOrderRepository.findAll(buildOperSpecification1(form));
			for (SubOrder subOrder : result) {
				total += subOrder.getOrderAmount();
			}
			return total;
		} catch (Exception e) {
			log.error("queryOrder", e);
			throw new ServiceException(resources.getMessage("op.query.error", null, null));
		}
	}

	/**
	 * 管理者查詢用
	 * 
	 * @param form
	 * @return
	 */
	private Specification<MainOrder> buildOperSpecification(MainOrderForm form) {
		return new Specification<MainOrder>() {
			@Override
			public Predicate toPredicate(Root<MainOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

				List<Predicate> predicates = new ArrayList<>();

				if (!StringUtils.isEmpty(form.getPkMainOrder())) {
					Path<String> np = root.get("pkMainOrder");
					predicates.add(cb.like(np, "%" + form.getPkMainOrder() + "%"));
				}
				if (form.getOrderDt() != null) {
					Path<Date> np = root.get("orderDt");
					Date sDate = form.getOrderDt();
					sDate = CommonUtils.getDateWithoutTimes(sDate);
					predicates.add(cb.greaterThanOrEqualTo(np, sDate));
				}
				if (form.getOrderDtEnd() != null) {
					Path<Date> np = root.get("orderDt");
					Date eDate = form.getOrderDtEnd();
					eDate = CommonUtils.getDateEndWithTimes(eDate);
					predicates.add(cb.lessThanOrEqualTo(np, eDate));
				}
				if (!StringUtils.isEmpty(form.getOrderStatus())) {
					Path<String> np = root.get("orderStatus");
					predicates.add(cb.equal(np, form.getOrderStatus()));
				}
				if (!predicates.isEmpty()) {
					return cb.and(predicates.toArray(new Predicate[predicates.size()]));
				}
				return cb.conjunction();
			}
		};
	}

	/**
	 * 廠商查詢用
	 * 
	 * @param form
	 * @return
	 */
	private Specification<SubOrder> buildOperSpecification1(MainOrderForm form) {
		return new Specification<SubOrder>() {
			@Override
			public Predicate toPredicate(Root<SubOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

				List<Predicate> predicates = new ArrayList<>();

				// 非管理者只能查屬於自己的訂單
				Join<SubOrder, Vendor> vendorJoin = root
						.join(root.getModel().getSingularAttribute("vendor", Vendor.class), JoinType.LEFT);
				predicates.add(cb.equal(vendorJoin.get("uid").as(String.class), getCurrentUserId()));

				Join<SubOrder, MainOrder> mainOrderJoin = root
						.join(root.getModel().getSingularAttribute("mainOrder", MainOrder.class), JoinType.LEFT);

				if (!StringUtils.isEmpty(form.getPkMainOrder())) {
					predicates.add(cb.like(mainOrderJoin.get("pkMainOrder").as(String.class),
							"%" + form.getPkMainOrder() + "%"));

				}
				if (form.getOrderDt() != null) {
					Date sDate = form.getOrderDt();
					sDate = CommonUtils.getDateWithoutTimes(sDate);
					predicates.add(cb.greaterThanOrEqualTo(mainOrderJoin.get("orderDt").as(Date.class), sDate));
				}
				if (form.getOrderDtEnd() != null) {
					Date eDate = form.getOrderDtEnd();
					eDate = CommonUtils.getDateEndWithTimes(eDate);
					predicates.add(cb.lessThanOrEqualTo(mainOrderJoin.get("orderDt").as(Date.class), eDate));
				}
				if (!StringUtils.isEmpty(form.getOrderStatus())) {
					predicates.add(cb.equal(mainOrderJoin.get("orderStatus").as(String.class), form.getOrderStatus()));
				}
				if (!predicates.isEmpty()) {
					return cb.and(predicates.toArray(new Predicate[predicates.size()]));
				}
				return cb.conjunction();
			}
		};
	}

	/**
	 * 取消訂單
	 * 
	 * @param form
	 * @throws ServiceException
	 */
	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void cancelPo(MainOrderForm form) throws ServiceException {
		boolean sendEmail = false;
		if (StringUtils.isBlank(form.getPkMainOrder()) || StringUtils.isBlank(form.getReturnReason())) {
			throw new ServiceException(resources.getMessage("order.not.found", null, null));
		}
		Date now = new Date();
		Integer totalAmt = 0;
		MainOrder mainOrder = null;
		try {
			mainOrder = mainOrderRepository.findOne(form.getPkMainOrder());
			if (mainOrder != null) {
				mainOrder.setOrderStatus(OrderStatusEnum.C.name());
				mainOrder.setLastModifiedId(getCurrentUserId());
				mainOrder.setUpdateDt(now);
				String purchaserId = mainOrder.getPurchaserId();
				mainOrder.getSubOrderList().size();
				Payment payment = mainOrder.getPayment();
				List<SubOrderDetail> refundList = new ArrayList<>();
				List<SubOrder> soList = mainOrder.getSubOrderList();
				for (SubOrder so : soList) {
					if (so.getSubOrderDetailList() != null && !so.getSubOrderDetailList().isEmpty()) {
						for (SubOrderDetail od : so.getSubOrderDetailList()) {
							// 只有已付款才能取消訂單
							if (!StringUtils.equalsIgnoreCase(OrderStatusEnum.P.name(), od.getStatus())) {
								throw new ServiceException(
										resources.getMessage("order.detail.status.error", null, null));
							}
							totalAmt += od.getTotalAmount();
							refundList.add(od);
							od.setReturnReason(form.getReturnReason());
							od.setStatus(OrderStatusEnum.R.name());
							od.setRefundDt(now);
							od.setUpdateDt(now);
							od.setLastModifiedId(getCurrentUserId());
							if (od.getProduct().getCourse() != null) {
								Course course = od.getProduct().getCourse();
								this.deleteCourse(course, purchaserId);
							} else if (od.getProduct().getProps() != null) {
								// 加回庫存
								int quantity = od.getProduct().getProps().getQuantity();
								od.getProduct().getProps().setQuantity(quantity + od.getQuantity());
							}
						}
						so.setOrderStatus(OrderStatusEnum.R.name());
						so.setUpdateDt(now);
						so.setLastModifiedId(getCurrentUserId());
					}
				}

				if (!refundList.isEmpty() && payment != null) {
					// 綠界退款
					this.callECpay(mainOrder, payment, totalAmt, form.getReturnReason());
					// 退貨記錄
					this.recordReturn(form.getPkMainOrder(), payment, totalAmt, form.getReturnReason(), refundList);
					sendEmail = true;
				}
			}
		} catch (ServiceException e) {
			log.error("cancelPo", e);
			throw e;

		} catch (Exception e) {
			log.error("cancelPo", e);
			throw new ServiceException(resources.getMessage("op.modify.error", null, null));

		} finally {
			if (sendEmail) {
				this.sendEmail(form.getPkMainOrder(), totalAmt, null);
			}
		}
		throw new MessageException(resources.getMessage("op.modify.success", null, null));
	}

	/**
	 * 退貨
	 * 
	 * @param form
	 * @throws ServiceException
	 */
	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void returnProduct(MainOrderForm form) throws ServiceException {
		Date now = new Date();
		boolean sendEmail = false;
		boolean processRefund = false;
		Integer totalAmt = 0;
		MainOrder mainOrder = null;
		List<SubOrderDetail> refundList = new ArrayList<>();
		List<SubOrderDetail> refundProcessList = new ArrayList<>();
		try {
			mainOrder = mainOrderRepository.findOne(form.getPkMainOrder());
			if (mainOrder == null) {
				throw new ServiceException(resources.getMessage("order.not.found", null, null));
			}
			String purchaserId = mainOrder.getPurchaserId();
			List<SubOrderDetail> detailList = this.getAllSubOrderDetail(form.getPkMainOrder());
			if (detailList != null && !detailList.isEmpty()) {
				List<Integer> subOrderDetailPkList = form.getReturnProductList();
				// 檢查退貨清單是否可以直接退款
				boolean directRefund = this.checkRefundStatus(form, detailList);
				for (SubOrderDetail detail : detailList) {
					if (detail != null && subOrderDetailPkList.contains(detail.getPkSubOrderDetail())) {
						detail.setUpdateDt(now);
						if (detail.getProduct().getProps() != null) {
							// 教具
							if (!directRefund) {
								if (StringUtils.equalsIgnoreCase(OrderStatusEnum.P.name(), detail.getStatus())) {
									detail.setStatus(OrderStatusEnum.G.name());
								} else if (StringUtils.equalsIgnoreCase(OrderStatusEnum.D.name(), detail.getStatus())) {
									detail.setStatus(OrderStatusEnum.H.name());
									refundProcessList.add(detail);
								}
							} else {
								// 可直接退款
								detail.setStatus(OrderStatusEnum.R.name());
								// 寫入退款時間
								detail.setRefundDt(now);
								totalAmt += detail.getTotalAmount();
								refundList.add(detail);
								// 加回庫存
								int quantity = detail.getProduct().getProps().getQuantity();
								detail.getProduct().getProps().setQuantity(quantity + detail.getQuantity());
							}
						} else if (detail.getProduct().getCourse() != null) {
							// 課程
							if (!directRefund) {
								// 退貨清單含教具必須等所有退貨商品變G，才可退款
								detail.setStatus(OrderStatusEnum.G.name());
							} else {
								// 退貨清單僅課程直接退款
								detail.setStatus(OrderStatusEnum.R.name());
								// 寫入退款時間
								detail.setRefundDt(now);
								totalAmt += detail.getTotalAmount();
								refundList.add(detail);
								// 刪除課程
								Course course = detail.getProduct().getCourse();
								this.deleteCourse(course, purchaserId);
							}
						}
						detail.setReturnReason(form.getReturnReason());
						detail.setUpdateDt(now);
						detail.setLastModifiedId(getCurrentUserId());
					}
				}
				processRefund = true;
			}

			Payment payment = mainOrder.getPayment();
			if (!refundList.isEmpty() && payment != null) {
				List<SubOrderDetail> remainList = new ArrayList<>();
				for (SubOrderDetail detail : detailList) {
					if (isRemain(detail)) {
						remainList.add(detail);
					}
				}
				// 緣界退款
				this.callECpay(mainOrder, payment, totalAmt, form.getReturnReason(), remainList);
				// 退貨記錄
				this.recordReturn(form.getPkMainOrder(), payment, totalAmt, form.getReturnReason(), refundList);
				sendEmail = true;
			}
		} catch (ServiceException e) {
			log.error("returnProduct", e);
			throw e;

		} catch (Exception e) {
			log.error("returnProduct", e);
			throw new ServiceException(resources.getMessage("op.modify.error", null, null));

		} finally {
			if (sendEmail) {
				this.sendEmail(form.getPkMainOrder(), totalAmt, refundList);
			}
			if (processRefund) {
				String orderNo = mainOrder.getPkMainOrder();
				Date sendDate = now;
				Date refundDate = now;
				String recipient = StringUtils.isEmpty(mainOrder.getRecipient()) ? StringUtils.EMPTY
						: mainOrder.getRecipient();
				String telPhone = StringUtils.EMPTY;
				String mobile = StringUtils.isEmpty(mainOrder.getTelphone()) ? StringUtils.EMPTY
						: mainOrder.getTelphone();
				String address = StringUtils.isEmpty(mainOrder.getAddress()) ? StringUtils.EMPTY
						: mainOrder.getAddress();
				SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
				Map<Vendor, List<SubOrderDetail>> map = new HashMap<>();
				for (SubOrderDetail detail : refundProcessList) {
					Vendor vendor = detail.getSubOrder().getVendor();
					if (map.get(vendor) == null) {
						map.put(vendor, new ArrayList<>());
					}
					map.get(vendor).add(detail);
				}
				if (!map.isEmpty()) {
					List<String> adminEmailList = commonService.getAdminEmailList();
					for (Entry<Vendor, List<SubOrderDetail>> entry : map.entrySet()) {
						Vendor vendor = entry.getKey();
						String vendorName = vendor.getVendorName();
						List<Map<String, String>> details = new ArrayList<>();
						List<SubOrderDetail> list = entry.getValue();
						int total = 0;
						boolean mail = false;
						for (SubOrderDetail sod : list) {
							Map<String, String> detail = new HashMap<>();
							detail.put("name", sod.getProductName());
							detail.put("price", String.valueOf(sod.getUnitPrice()));
							detail.put("quantity", String.valueOf(sod.getQuantity()));
							detail.put("total", String.valueOf(sod.getTotalAmount()));
							if (sod.getProduct().getProps() != null) {
								mail = true;
							}
							total += sod.getTotalAmount();
							details.add(detail);
						}
						try {
							mailService.sendRefundProcessNotificationEmail(
									new String[] { StringUtils.isEmpty(vendor.getEmail()) ? vendor.getUid()
											: vendor.getEmail() },
									adminEmailList.toArray(new String[adminEmailList.size()]), vendorName, orderNo,
									refundDate, details, String.valueOf(total), recipient, telPhone, mobile, address,
									sendDate, mail);
							sendNotification(String.format("Wikidue Store 教育商城 - 訂單編號 %s 申請退貨通知信", orderNo),
									templateService.getRefundProcessNotification(configService.getStoreUrl(),
											vendorName, orderNo, sdf.format(refundDate), configService.getHost(),
											details, String.valueOf(total), recipient, telPhone, mobile, address,
											configService.getImageUrl(), sdf.format(sendDate),
											configService.getBackendManualUrl(), configService.getServiceUrl(), mail),
									mainOrder.getCreateId(), 1);
						} catch (Exception e) {
							log.error("return product email notification", e);
						}
					}
				}
			}
		}
		throw new MessageException(resources.getMessage("op.modify.success", null, null));
	}

	/**
	 * 確認退貨清單是否可以直接退款
	 * 
	 * @param form
	 * @param detailList
	 * @return
	 */
	private boolean checkRefundStatus(MainOrderForm form, List<SubOrderDetail> detailList) {
		if (detailList != null && detailList.size() > 0) {
			List<Integer> subOrderDetailPkList = form.getReturnProductList();
			for (SubOrderDetail detail : detailList) {
				if (detail != null && subOrderDetailPkList.contains(detail.getPkSubOrderDetail())) {
					if (!StringUtils.equalsIgnoreCase(OrderStatusEnum.P.name(), detail.getStatus())) {
						return false;
					}
				}
			}
		}
		return true;
	}

	/**
	 * 確認或拒絕退貨
	 * 
	 * @param form
	 * @throws ServiceException
	 */
	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void returnHandling(MainOrderForm form) throws ServiceException {
		boolean result = false;
		SubOrderDetail subOrderDetail = null;
		SubOrder subOrder = null;
		Vendor vendor = null;
		MainOrder mainOrder = null;
		try {
			if (StringUtils.isBlank(form.getPkSubOrderDetail())) {
				throw new ServiceException(resources.getMessage("order.not.found", null, null));
			}
			if (!(StringUtils.equals(form.getSubOrderDetailStatus(), OrderStatusEnum.G.name())
					|| StringUtils.equals(form.getSubOrderDetailStatus(), OrderStatusEnum.J.name()))) {
				throw new ServiceException(resources.getMessage("order.detail.status.error", null, null));
			}
			subOrderDetail = subOrderDetailRepository.findOne((Integer.valueOf(form.getPkSubOrderDetail())));
			if (subOrderDetail != null) {
				if (!StringUtils.equals(subOrderDetail.getStatus(), OrderStatusEnum.H.name())) {
					throw new ServiceException(resources.getMessage("order.detail.status.error", null, null));
				}
				subOrder = subOrderDetail.getSubOrder();
				mainOrder = subOrder.getMainOrder();
				vendor = subOrder.getVendor();
				// 登入廠商與訂單廠商不同時，不得操作
				this.checkVendorAuth(subOrder);
				Date now = new Date();
				subOrderDetail.setStatus(form.getSubOrderDetailStatus());
				// 拒絕退件時，寫入時間(for report)
				if (StringUtils.equals(form.getSubOrderDetailStatus(), OrderStatusEnum.J.name())) {
					subOrderDetail.setRefundDt(now);
				}

				subOrderDetail.setLastModifiedId(getCurrentUserId());
				subOrderDetail.setUpdateDt(now);

				result = true;
			}
		} catch (ServiceException e) {
			log.error("returnHandling", e);
			throw e;

		} catch (Exception e) {
			log.error("returnHandling", e);
			throw new ServiceException(resources.getMessage("op.modify.error", null, null));
		} finally {
			if (result && vendor != null) {
				try {
					Date now = new Date();
					Date sendDate = now;
					Date refundDate = now;
					String adminName = configService.getAdminName();
					String orderNo = mainOrder.getPkMainOrder();
					String recipient = StringUtils.isEmpty(mainOrder.getRecipient()) ? StringUtils.EMPTY
							: mainOrder.getRecipient();
					String telPhone = StringUtils.EMPTY;
					String mobile = StringUtils.isEmpty(mainOrder.getTelphone()) ? StringUtils.EMPTY
							: mainOrder.getTelphone();
					String address = StringUtils.isEmpty(mainOrder.getAddress()) ? StringUtils.EMPTY
							: mainOrder.getAddress();
					boolean mail = false;
					List<String> adminEmailList = commonService.getAdminEmailList();
					List<Map<String, String>> details = new ArrayList<>();
					Map<String, String> detail = new HashMap<>();
					detail.put("name", subOrderDetail.getProductName());
					detail.put("price", String.valueOf(subOrderDetail.getUnitPrice()));
					detail.put("quantity", String.valueOf(subOrderDetail.getQuantity()));
					detail.put("total", String.valueOf(subOrderDetail.getTotalAmount()));
					details.add(detail);
					if (subOrderDetail.getProduct().getProps() != null) {
						mail = true;
					}
					SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
					String total = String.valueOf(subOrderDetail.getTotalAmount());
					if (StringUtils.equals(form.getSubOrderDetailStatus(), OrderStatusEnum.G.name())) {
						// 已收到退貨
						mailService.sendReceiveRefundNotificationEmail(
								adminEmailList.toArray(new String[adminEmailList.size()]),
								new String[] {
										StringUtils.isEmpty(vendor.getEmail()) ? vendor.getUid() : vendor.getEmail() },
								sendDate, adminName, orderNo, refundDate, details, total, recipient, telPhone, mobile,
								address, mail);
						sendNotification(String.format("Wikidue Store 教育商城 - 訂單編號 %s 退貨成功通知信", orderNo),
								templateService.getReceiveRefundNotification(configService.getStoreUrl(),
										sdf.format(sendDate), adminName, orderNo, sdf.format(refundDate),
										configService.getHost(), details,
										String.valueOf(subOrderDetail.getTotalAmount()), recipient, telPhone, mobile,
										address, configService.getImageUrl(), configService.getBackendManualUrl(),
										configService.getServiceUrl(), mail),
								mainOrder.getCreateId(), 1);
					} else if (StringUtils.equals(form.getSubOrderDetailStatus(), OrderStatusEnum.J.name())) {
						// 拒絕退件
						mailService.sendRefuseRefundNotificationEmail(new String[adminEmailList.size()],
								new String[] {
										StringUtils.isEmpty(vendor.getEmail()) ? vendor.getUid() : vendor.getEmail() },
								sendDate, adminName, orderNo, details, total);
						sendNotification(String.format("Wikidue Store 教育商城 - 訂單編號 %s 退貨不成功通知信", orderNo),
								templateService.getRefuseRefundNotification(configService.getStoreUrl(),
										configService.getImageUrl(), sdf.format(sendDate), adminName, orderNo,
										configService.getHost(), details, total, configService.getBackendManualUrl(),
										configService.getServiceUrl()),
								mainOrder.getCreateId(), 1);

					}
				} catch (Exception e) {
					log.error("return handling email notification", e);
				}
			}
		}
		throw new MessageException(resources.getMessage("op.modify.success", null, null));
	}

	/**
	 * 退款
	 * 
	 * @param form
	 * @throws ServiceException
	 */
	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void refundProduct(MainOrderForm form) throws ServiceException {
		Date now = new Date();
		boolean sendEmail = false;
		Integer totalAmt = 0;
		MainOrder mainOrder = null;
		List<SubOrderDetail> refundList = new ArrayList<>();
		String reason = "";
		try {
			mainOrder = mainOrderRepository.findOne(form.getPkMainOrder());
			if (mainOrder == null) {
				throw new ServiceException(resources.getMessage("order.not.found", null, null));
			}
			String purchaserId = mainOrder.getPurchaserId();
			List<SubOrderDetail> detailList = this.getAllSubOrderDetail(form.getPkMainOrder());
			if (detailList != null && !detailList.isEmpty()) {
				for (SubOrderDetail detail : detailList) {
					if (StringUtils.isEmpty(reason)) {
						reason = detail.getReturnReason();
					}
					if (StringUtils.equalsIgnoreCase(OrderStatusEnum.G.name(), detail.getStatus())) {
						if (detail.getProduct().getCourse() != null) {
							// 刪除課程
							Course course = detail.getProduct().getCourse();
							this.deleteCourse(course, purchaserId);
						} else if (detail.getProduct().getProps() != null) {
							// 加回庫存
							int quantity = detail.getProduct().getProps().getQuantity();
							detail.getProduct().getProps().setQuantity(quantity + detail.getQuantity());
						}
						refundList.add(detail);
						totalAmt += detail.getTotalAmount();
						detail.setUpdateDt(now);
						detail.setStatus(OrderStatusEnum.R.name());
						// 退款時間
						detail.setRefundDt(now);
						detail.setUpdateDt(now);
						detail.setLastModifiedId(getCurrentUserId());
					}
				}
			}
			if (StringUtils.isEmpty(reason)) {
				reason = "Wikidue Return.";
			}
			Payment payment = mainOrder.getPayment();
			if (!refundList.isEmpty() && payment != null) {
				List<SubOrderDetail> remainList = new ArrayList<>();
				for (SubOrderDetail detail : detailList) {
					if (isRemain(detail)) {
						remainList.add(detail);
					}
				}
				// 緣界退款
				this.callECpay(mainOrder, payment, totalAmt, reason, remainList);
				// 退貨記錄
				this.recordReturn(form.getPkMainOrder(), payment, totalAmt, reason, refundList);
				sendEmail = true;
			}
		} catch (ServiceException e) {
			log.error("refundProduct", e);
			throw e;

		} catch (Exception e) {
			log.error("refundProduct", e);
			throw new ServiceException(resources.getMessage("op.modify.error", null, null));

		} finally {
			if (sendEmail) {
				this.sendEmail(form.getPkMainOrder(), totalAmt, refundList);
			}
		}
		throw new MessageException(resources.getMessage("op.modify.success", null, null));
	}

	/**
	 * 折讓發票
	 * 
	 * @param payment
	 * @param remainList
	 */
	private Map<String, String> allowanceInvoice(MainOrder mainOrder, Payment payment,
			List<SubOrderDetail> remainList) {
		if (mainOrder == null || payment == null || remainList == null || remainList.isEmpty()) {
			log.error("allowanceInvoice param object is null");
			return null;
		}
		StringBuilder itemAmount = new StringBuilder();
		StringBuilder itemCount = new StringBuilder();
		StringBuilder itemName = new StringBuilder();
		StringBuilder itemPrice = new StringBuilder();
		StringBuilder itemWord = new StringBuilder();
		int remainAmount = 0;
		for (int i = 0; i < remainList.size(); i++) {
			SubOrderDetail od = remainList.get(i);
			if (i > 0) {
				itemAmount.append("|");
				itemCount.append("|");
				itemName.append("|");
				itemPrice.append("|");
				itemWord.append("|");
			}
			itemName.append(od.getProductName());
			itemCount.append(od.getQuantity());
			if (od.getProduct().getCourse() != null) {
				itemWord.append("堂");
			} else {
				itemWord.append("個");
			}
			itemPrice.append(od.getUnitPrice());
			itemAmount.append((od.getQuantity() * od.getUnitPrice()));
			remainAmount += od.getTotalAmount();
		}
		int allowanceAmount = payment.getTradeAmt() - remainAmount;
		AllowanceObj allowanceObj = new AllowanceObj();
		allowanceObj.setInvoiceNo(payment.getInvoiceNo());
		allowanceObj.setAllowanceNotify("A");
		allowanceObj.setAllowanceAmount(String.valueOf(allowanceAmount));
		allowanceObj.setItemAmount(itemAmount.toString());
		allowanceObj.setItemCount(itemCount.toString());
		allowanceObj.setItemName(itemName.toString());
		allowanceObj.setItemPrice(itemPrice.toString());
		allowanceObj.setItemWord(itemWord.toString());
		allowanceObj.setNotifyMail(mainOrder.getReceiptEmail());
		allowanceObj.setNotifyPhone(mainOrder.getReceiptTelphone());
		allowanceObj.setCustomerName(
				StringUtils.equals(payment.getInvoiceType(), "6") && !StringUtils.isEmpty(payment.getCompanyName())
						? payment.getCompanyName() : mainOrder.getPurchaser());
		return ecpayService.allowanceInvoice(allowanceObj);
	}

	/**
	 * 開立新發票
	 * 
	 * @param payment
	 * @param remainList
	 */
	private Map<String, String> issueInvoice(MainOrder mainOrder, Payment payment, List<SubOrderDetail> remainList) {
		if (mainOrder == null || payment == null || remainList == null || remainList.isEmpty()) {
			log.error("issueInvoice param object is null");
			return null;
		}
		StringBuilder itemAmount = new StringBuilder();
		StringBuilder itemCount = new StringBuilder();
		StringBuilder itemName = new StringBuilder();
		StringBuilder itemPrice = new StringBuilder();
		StringBuilder itemWord = new StringBuilder();
		for (int i = 0; i < remainList.size(); i++) {
			SubOrderDetail od = remainList.get(i);
			if (i > 0) {
				itemAmount.append("|");
				itemCount.append("|");
				itemName.append("|");
				itemPrice.append("|");
				itemWord.append("|");
			}
			itemName.append(od.getProductName());
			itemCount.append(od.getQuantity());
			if (od.getProduct().getCourse() != null) {
				itemWord.append("堂");
			} else {
				itemWord.append("個");
			}
			itemPrice.append(od.getUnitPrice());
			itemAmount.append((od.getQuantity() * od.getUnitPrice()));
		}
		IssueObj issueObj = new IssueObj();
		issueObj.setDonation("0");
		issueObj.setPrint("0");
		switch (payment.getInvoiceType()) {
		case "1":
			issueObj.setCarruerType("1");
			break;
		case "2":
			issueObj.setCarruerType("2");
			issueObj.setCarruerNum(payment.getMoicaNo());
			break;
		case "3":
			issueObj.setCarruerType("3");
			issueObj.setCarruerNum(payment.getMobileCode());
			break;
		case "4":
			issueObj.setPrint("1");
			break;
		case "5":
			issueObj.setDonation("1");
			issueObj.setLoveCode(payment.getLoveCode());
			break;
		case "6":
			issueObj.setCustomerIdentifier(payment.getCompanyNo());
			issueObj.setCustomerName(payment.getCompanyName());
			issueObj.setPrint("1");
			break;
		default:
			return null;
		}
		if (StringUtils.isEmpty(issueObj.getCustomerName())) {
			issueObj.setCustomerName(mainOrder.getPurchaser());
		}
		issueObj.setCustomerAddr(mainOrder.getReceiptAddress());
		issueObj.setCustomerEmail(mainOrder.getReceiptEmail());
		issueObj.setInvType("07");
		issueObj.setItemAmount(itemAmount.toString());
		issueObj.setItemCount(itemCount.toString());
		issueObj.setItemName(itemName.toString());
		issueObj.setItemPrice(itemPrice.toString());
		issueObj.setItemWord(itemWord.toString());
		String relateNumber = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 30);
		issueObj.setRelateNumber(relateNumber);
		issueObj.setSalesAmount(String.valueOf(payment.getTradeAmt()));
		issueObj.setTaxType("1");
		return ecpayService.issueInvoice(issueObj);
	}

	private boolean callECpay(MainOrder mainOrder, Payment payment, Integer totalAmt, String returnReason)
			throws ServiceException {

		return callECpay(mainOrder, payment, totalAmt, returnReason, null);
	}

	/**
	 * 綠界退款
	 * 
	 * @param payment
	 * @param totalAmt
	 * @param returnReason
	 * @return
	 * @throws ServiceException
	 */
	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	private boolean callECpay(MainOrder mainOrder, Payment payment, Integer totalAmt, String returnReason,
			List<SubOrderDetail> remainList) throws ServiceException {
		boolean returnTradeResult = false;
		Calendar now = Calendar.getInstance();
		now.setTime(new Date());
		Calendar paymentDate = Calendar.getInstance();
		paymentDate.setTime(payment.getPaymentDt());
		boolean skipInvoice = (StringUtils.isEmpty(payment.getInvoiceNo()) || configService.getSkipInvoice());
		/*
		 * 0:次月 1:當月 2:當日
		 */
		int status = 0;
		if ((paymentDate.get(Calendar.YEAR) == now.get(Calendar.YEAR))
				&& (paymentDate.get(Calendar.MONTH) == now.get(Calendar.MONTH))) {
			status = 1;
			if ((paymentDate.get(Calendar.DATE) == now.get(Calendar.DATE))
					&& (paymentDate.get(Calendar.HOUR_OF_DAY) < 20)) {
				status = 2;
			}
		}

		switch (status) {
		case 0:
			if (!skipInvoice) {
				allowanceInvoice(mainOrder, payment, remainList);
			}
			ecpayService.closeTrade(payment.getPkPayment(), payment.getTradeNo(), payment.getTradeAmt());
			returnTradeResult = ecpayService.returnTrade(payment.getPkPayment(), payment.getTradeNo(), totalAmt);
			break;
		case 1:
			if (!skipInvoice) {
				if (remainList == null || remainList.isEmpty()) {
					ecpayService.invalidInvoice(payment.getInvoiceNo(), StringUtils.substring(returnReason, 0, 20));
				} else {
					allowanceInvoice(mainOrder, payment, remainList);
				}
			}
			ecpayService.closeTrade(payment.getPkPayment(), payment.getTradeNo(), payment.getTradeAmt());
			returnTradeResult = ecpayService.returnTrade(payment.getPkPayment(), payment.getTradeNo(), totalAmt);
			break;
		case 2:
			if (!skipInvoice) {
				if (remainList == null || remainList.isEmpty()) {
					ecpayService.invalidInvoice(payment.getInvoiceNo(), StringUtils.substring(returnReason, 0, 20));
				} else {
					allowanceInvoice(mainOrder, payment, remainList);
				}
			}
			if (remainList == null || remainList.isEmpty()) {
				returnTradeResult = ecpayService.giveUpTrade(payment.getPkPayment(), payment.getTradeNo(),
						payment.getTradeAmt());
			} else {
				ecpayService.closeTrade(payment.getPkPayment(), payment.getTradeNo(), payment.getTradeAmt());
				returnTradeResult = ecpayService.returnTrade(payment.getPkPayment(), payment.getTradeNo(), totalAmt);
			}
			break;
		}
		return returnTradeResult;
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	private void recordReturn(String pkMainOrder, Payment payment, Integer totalAmt, String returnReason,
			List<SubOrderDetail> refundList) {
		Date now = new Date();
		ReturnRecord returnRecord = new ReturnRecord();
		returnRecord.setActive("1");
		returnRecord.setCreateDt(now);
		returnRecord.setCreateId(getCurrentUserId());
		returnRecord.setInvoiceNo(payment.getInvoiceNo());
		returnRecord.setMainOrderNo(pkMainOrder);
		returnRecord.setRemainAllowanceAmt(0);
		returnRecord.setReturnAmt(totalAmt);
		returnRecord.setReturnReason(returnReason);
		returnRecord.setType("A");
		returnRecord = returnRecordRepository.save(returnRecord);
		for (SubOrderDetail od : refundList) {
			ReturnRecordDetail returnRecordDetail = new ReturnRecordDetail();
			returnRecordDetail.setActive("1");
			returnRecordDetail.setCreateDt(now);
			returnRecordDetail.setCreateId(getCurrentUserId());
			returnRecordDetail.setOrderDetail(od);
			returnRecordDetail.setReturnQuantity(od.getQuantity());
			returnRecordDetail.setReturnRecord(returnRecord);
			returnRecordDetailRepository.save(returnRecordDetail);
		}
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	private void sendEmail(String pkMainOrder, Integer totalAmt, List<SubOrderDetail> refundList) {
		try {
			MainOrder mainOrder = mainOrderRepository.findOne(pkMainOrder);
			mainOrder.getSubOrderList().size();
			String buyerName = mainOrder.getPurchaser();
			String buyerPhone = mainOrder.getTelphone();
			String buyerAddress = mainOrder.getAddress();
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);

			String email = "";
			if (userInfoRepository.exists(mainOrder.getPurchaserId())) {
				UserInfo userInfo = userInfoRepository.findOne(mainOrder.getPurchaserId());
				email = userInfo.getEmail();
			} else {
				email = mainOrder.getReceiptEmail();
			}
			String orderNo = mainOrder.getPkMainOrder();
			String adminName = configService.getAdminName();
			Date now = new Date();
			Date sendDate = now;
			Date refundDate = now;
			if (refundList != null) {
				Map<Integer, List<SubOrderDetail>> soMap = new HashMap<>();
				List<Map<String, String>> buyerEmailDetails = new ArrayList<>();
				for (SubOrderDetail sod : refundList) {
					Map<String, String> map = new HashMap<>();
					map.put("name", sod.getProductName());
					map.put("price", String.valueOf(sod.getUnitPrice()));
					map.put("quantity", String.valueOf(sod.getQuantity()));
					map.put("total", String.valueOf(sod.getTotalAmount()));
					buyerEmailDetails.add(map);

					if (soMap.get(sod.getSubOrder().getPkSubOrder()) == null) {
						soMap.put(sod.getSubOrder().getPkSubOrder(), new ArrayList<>());
					}
					soMap.get(sod.getSubOrder().getPkSubOrder()).add(sod);
				}
				mailService.sendRefundBuyerNotificationEmail(new String[] { email }, null, sendDate, buyerName, orderNo,
						buyerEmailDetails);
				sendNotification(String.format("Wikidue Store 教育商城 - 訂單編號 %s 退款通知信", orderNo),
						templateService.getRefundBuyerNotification(configService.getStoreUrl(), sdf.format(sendDate),
								buyerName, configService.getMyOrderUrl(), orderNo, buyerEmailDetails,
								configService.getImageUrl(), configService.getServiceUrl()),
						mainOrder.getCreateId(), 1);

				List<String> adminEmailList = commonService.getAdminEmailList();
				for (Entry<Integer, List<SubOrderDetail>> e : soMap.entrySet()) {
					SubOrder so = subOrderRepository.findOne(e.getKey());

					List<Map<String, String>> vendorEmailDetails = new ArrayList<>();
					for (SubOrderDetail sod : e.getValue()) {
						Map<String, String> map = new HashMap<>();
						map.put("name", sod.getProductName());
						map.put("price", String.valueOf(sod.getUnitPrice()));
						map.put("quantity", String.valueOf(sod.getQuantity()));
						map.put("total", String.valueOf(sod.getTotalAmount()));
						vendorEmailDetails.add(map);
					}

					Vendor vendor = so.getVendor();
					if (vendor != null) {
						String vendorEmail = StringUtils.isEmpty(vendor.getEmail()) ? vendor.getUid()
								: vendor.getEmail();
						mailService.sendRefundVendorNotificationEmail(
								adminEmailList.toArray(new String[adminEmailList.size()]), new String[] { vendorEmail },
								adminName, orderNo, refundDate, vendorEmailDetails, sendDate);
					}
				}

			} else {
				List<Map<String, String>> buyerEmailDetails = new ArrayList<>();
				List<String> adminEmailList = commonService.getAdminEmailList();
				for (SubOrder so : mainOrder.getSubOrderList()) {
					so.getSubOrderDetailList().size();
					List<Map<String, String>> vendorEmailDetails = new ArrayList<>();
					for (SubOrderDetail sod : so.getSubOrderDetailList()) {
						Map<String, String> map = new HashMap<>();
						map.put("name", sod.getProductName());
						map.put("price", String.valueOf(sod.getUnitPrice()));
						map.put("quantity", String.valueOf(sod.getQuantity()));
						map.put("total", String.valueOf(sod.getTotalAmount()));
						vendorEmailDetails.add(map);
						buyerEmailDetails.add(map);
					}

					Vendor vendor = so.getVendor();
					if (vendor != null) {
						String vendorEmail = StringUtils.isEmpty(vendor.getEmail()) ? vendor.getUid()
								: vendor.getEmail();
						mailService.sendRefundVendorNotificationEmail(
								adminEmailList.toArray(new String[adminEmailList.size()]), new String[] { vendorEmail },
								adminName, orderNo, refundDate, vendorEmailDetails, sendDate);
					}
				}
				mailService.sendRefundBuyerNotificationEmail(new String[] { email }, null, sendDate, buyerName, orderNo,
						buyerEmailDetails);
			}
		} catch (

		Exception e) {
			log.error("sendEmail", e);
		}
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	private void deleteCourse(Course course, String purchaserId) {
		List<UserCourse> userCourseList = userCourseRepository
				.findAll((Root<UserCourse> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
					List<Predicate> predicates = new ArrayList<>();
					query.distinct(true);
					// 類別
					predicates.add(cb.equal(root.get("course").get("pkCourse"), course.getPkCourse()));
					predicates.add(cb.equal(root.get("userId"), purchaserId));

					if (!predicates.isEmpty()) {
						return cb.and(predicates.toArray(new Predicate[predicates.size()]));
					}

					return cb.conjunction();
				});
		if (userCourseList != null && !userCourseList.isEmpty()) {
			userCourseRepository.delete(userCourseList);
		}
	}

	/**
	 * 逾期訂單
	 * 
	 * @param days
	 */
	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void cancelOverDuePo() {
		Date today = CommonUtils.getTodayWithoutTimes();
		List<MainOrder> list = mainOrderRepository.findOverDueMainOrder(today);
		Date now = new Date();
		for (MainOrder mo : list) {
			mo.setOrderStatus(OrderStatusEnum.S.name());
			mo.setUpdateDt(now);
			mo.setLastModifiedId("System");
			List<SubOrder> subList = mo.getSubOrderList();
			for (SubOrder so : subList) {
				so.setOrderStatus(OrderStatusEnum.S.name());
				so.setUpdateDt(now);
				so.setLastModifiedId("System");
				for (SubOrderDetail od : so.getSubOrderDetailList()) {
					od.setStatus(OrderStatusEnum.S.name());
					od.setUpdateDt(now);
					od.setLastModifiedId("System");
					// 加回庫存
					if (od.getProduct().getProps() != null) {
						int quantity = od.getProduct().getProps().getQuantity();
						od.getProduct().getProps().setQuantity(quantity + od.getQuantity());
					}
				}
			}
		}
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void saveMainRemark(MainOrderForm form) throws ServiceException {
		try {
			if (StringUtils.isBlank(form.getPkMainOrder())) {
				throw new ServiceException(resources.getMessage("order.not.found", null, null));
			}
			MainOrder mainOrder = mainOrderRepository.findOne(form.getPkMainOrder());
			if (mainOrder != null) {
				Date now = new Date();
				mainOrder.setUpdateDt(now);
				mainOrder.setAdminRemark(form.getAdminRemark());
				mainOrder.setUserRemark(form.getUserRemark());
				mainOrder.setLastModifiedId(getCurrentUserId());
			} else {
				throw new ServiceException(resources.getMessage("order.not.found", null, null));
			}
		} catch (ServiceException e) {
			log.error("saveMainRemark", e);
			throw e;

		} catch (Exception e) {
			log.error("saveMainRemark", e);
			throw new ServiceException(resources.getMessage("op.modify.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.modify.success", null, null));
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void saveDetailRemark(MainOrderForm form) throws ServiceException {
		try {
			if (StringUtils.isBlank(form.getPkSubOrderDetail())) {
				throw new ServiceException(resources.getMessage("order.not.found", null, null));
			}
			SubOrderDetail subOrderDetail = subOrderDetailRepository
					.findOne((Integer.valueOf(form.getPkSubOrderDetail())));

			if (subOrderDetail != null) {
				subOrderDetail.setUserRemark(form.getDetailUserRemark());
				subOrderDetail.setAdminRemark(form.getDetailAdminRemark());
				subOrderDetail.setLastModifiedId(getCurrentUserId());
			}
		} catch (ServiceException e) {
			log.error("saveDetailRemark", e);
			throw e;

		} catch (Exception e) {
			log.error("saveDetailRemark", e);
			throw new ServiceException(resources.getMessage("op.modify.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.modify.success", null, null));
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void saveVendorRemark(MainOrderForm form) throws ServiceException {
		try {
			if (StringUtils.isBlank(form.getPkSubOrderDetail())) {
				throw new ServiceException(resources.getMessage("order.not.found", null, null));
			}
			SubOrderDetail subOrderDetail = subOrderDetailRepository
					.findOne((Integer.valueOf(form.getPkSubOrderDetail())));

			if (subOrderDetail != null) {
				subOrderDetail.setVendorRemark(form.getDetailVendorRemark());
				subOrderDetail.setLastModifiedId(getCurrentUserId());
			}
		} catch (ServiceException e) {
			log.error("saveVendorRemark", e);
			throw e;

		} catch (Exception e) {
			log.error("saveVendorRemark", e);
			throw new ServiceException(resources.getMessage("op.modify.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.modify.success", null, null));
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public String checkBtnStatus(MainOrder mainOrder) {
		String btnStatus = "";
		int productNum = 0;
		// 退貨筆數
		int courseP = 0;
		int propsP = 0;

		if (mainOrder != null) {
			List<SubOrder> list = mainOrder.getSubOrderList();
			if (list != null && list.size() > 0) {
				for (SubOrder so : list) {
					productNum += so.getSubOrderDetailList().size();
					boolean hasReturnRecord = false;
					for (SubOrderDetail detail : so.getSubOrderDetailList()) {
						if (detail.getProduct().getProps() != null) {
							if (StringUtils.equalsIgnoreCase(OrderStatusEnum.P.name(), detail.getStatus())) {
								propsP++;
							}
							if (StringUtils.equalsIgnoreCase(OrderStatusEnum.H.name(), detail.getStatus())) {
								hasReturnRecord = true;
							}
							if (StringUtils.equalsIgnoreCase(OrderStatusEnum.G.name(), detail.getStatus())) {
								hasReturnRecord = true;
							}
							if (StringUtils.equalsIgnoreCase(OrderStatusEnum.J.name(), detail.getStatus())) {
								hasReturnRecord = true;
							}
							if (StringUtils.equalsIgnoreCase(OrderStatusEnum.R.name(), detail.getStatus())) {
								hasReturnRecord = true;
							}
						} else if (detail.getProduct().getCourse() != null) {
							if (StringUtils.equalsIgnoreCase(OrderStatusEnum.P.name(), detail.getStatus())) {
								courseP++;
							}
							if (StringUtils.equalsIgnoreCase(OrderStatusEnum.R.name(), detail.getStatus())) {
								hasReturnRecord = true;
							}
						}
					}
					so.setHasReturnRecord(hasReturnRecord);
				}
				// 已付款，未出貨
				if (productNum == (courseP + propsP)) {
					// 訂單取消Cancel
					btnStatus = "C";
				}

			}
		}
		return btnStatus;
	}

	/**
	 * 退貨按鈕
	 * 
	 * @param detailList
	 * @return
	 * @throws ServiceException
	 */
	public boolean getReturnBtn(List<SubOrderDetail> detailList) throws ServiceException {
		boolean showBtn = false;
		boolean hasPorD = false;
		boolean hasHorG = false;
		if (detailList != null) {
			for (SubOrderDetail detail : detailList) {
				if (StringUtils.equals(OrderStatusEnum.P.name(), detail.getStatus())
						|| StringUtils.equals(OrderStatusEnum.D.name(), detail.getStatus())) {
					hasPorD = true;
				}
				// 尚未處理完前一批退款，不得再退
				if (StringUtils.equals(OrderStatusEnum.H.name(), detail.getStatus())
						|| StringUtils.equals(OrderStatusEnum.G.name(), detail.getStatus())) {
					hasHorG = true;
				}
			}
		}
		// 有P、D，且不能有 H & G
		if (hasPorD && !hasHorG) {
			showBtn = true;
		}
		return showBtn;
	}

	/**
	 * 退款按鈕
	 * 
	 * @param detailList
	 * @return
	 * @throws ServiceException
	 */
	public boolean getRefundBtn(List<SubOrderDetail> detailList) throws ServiceException {
		boolean showBtn = false;
		boolean hasH = false;
		boolean hasG = false;
		if (detailList != null) {
			for (SubOrderDetail detail : detailList) {
				if (StringUtils.equals(OrderStatusEnum.H.name(), detail.getStatus())) {
					hasH = true;
				}
				if (StringUtils.equals(OrderStatusEnum.G.name(), detail.getStatus())) {
					hasG = true;
				}
			}
		}
		// 有G且不能有任何H
		if (hasG && !hasH) {
			showBtn = true;
		}
		return showBtn;
	}

	/**
	 * 送貨按鈕
	 * 
	 * @param SubOrder
	 * @return
	 */
	public boolean getDeliveryBtn(SubOrder subOrder) {
		boolean showDeliverBtn = false;
		if (subOrder != null) {
			for (SubOrderDetail od : subOrder.getSubOrderDetailList()) {
				if (od.getProduct().getProps() != null) {
					if (!(StringUtils.equalsIgnoreCase(OrderStatusEnum.P.name(), od.getStatus())
							|| StringUtils.equalsIgnoreCase(OrderStatusEnum.R.name(), od.getStatus()))) {
						return false;
					}
					if (StringUtils.equalsIgnoreCase(OrderStatusEnum.P.name(), od.getStatus())) {
						showDeliverBtn = true;
					}
				}
			}
		}
		return showDeliverBtn;
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void unsendNotice() {
		try {
			Date now = new Date();
			Calendar twoDaysAgo = Calendar.getInstance();
			twoDaysAgo.set(Calendar.DATE, twoDaysAgo.get(Calendar.DATE) - 2);
			List<SubOrder> unsendOrderList = subOrderRepository
					.findAll((Root<SubOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
						List<Predicate> predicates = new ArrayList<>();

						predicates.add(cb.equal(root.get("orderStatus"), OrderStatusEnum.P.toString()));
						predicates.add(cb.isNull(root.get("deliveryDt")));

						if (!predicates.isEmpty()) {
							return cb.and(predicates.toArray(new Predicate[predicates.size()]));
						}

						return cb.conjunction();
					});
			if (unsendOrderList != null && !unsendOrderList.isEmpty()) {
				List<String> adminEmailList = commonService.getAdminEmailList();
				SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
				for (SubOrder so : unsendOrderList) {
					Payment payment = so.getMainOrder().getPayment();
					if (payment != null && payment.getPaymentDt() != null
							&& payment.getPaymentDt().before(twoDaysAgo.getTime())) {
						boolean send = false;
						List<Map<String, String>> details = new ArrayList<>();
						so.getSubOrderDetailList().size();
						for (SubOrderDetail sod : so.getSubOrderDetailList()) {
							if (sod.getProduct().getProps() != null
									&& StringUtils.equals(sod.getStatus(), OrderStatusEnum.P.name())) {
								send = true;
								break;
							}
						}
						if (send) {
							Vendor vendor = so.getVendor();
							MainOrder order = so.getMainOrder();
							String vendorName = vendor.getVendorName();
							String buyerName = StringUtils.isEmpty(order.getPurchaser()) ? StringUtils.EMPTY
									: order.getPurchaser();
							String recipient = StringUtils.isEmpty(order.getRecipient()) ? StringUtils.EMPTY
									: order.getRecipient();
							String telPhone = StringUtils.EMPTY;
							String mobile = StringUtils.isEmpty(order.getMobile()) ? StringUtils.EMPTY
									: order.getMobile();
							String address = StringUtils.isEmpty(order.getAddress()) ? StringUtils.EMPTY
									: order.getAddress();
							String orderNo = order.getPkMainOrder();
							Date orderDate = order.getOrderDt();
							for (SubOrderDetail sod : so.getSubOrderDetailList()) {
								Map<String, String> detail = new HashMap<>();
								detail.put("name", sod.getProductName());
								detail.put("price", String.valueOf(sod.getUnitPrice()));
								detail.put("quantity", String.valueOf(sod.getQuantity()));
								detail.put("total", String.valueOf(sod.getTotalAmount()));
								OrderStatusEnum status = OrderStatusEnum.getByName(sod.getStatus());
								detail.put("status", status != null ? status.getDisplayName() : StringUtils.EMPTY);
								details.add(detail);

							}
							mailService.sendUnsendNotificationEmail(
									new String[] { StringUtils.isEmpty(vendor.getEmail()) ? vendor.getUid()
											: vendor.getEmail() },
									adminEmailList.toArray(new String[adminEmailList.size()]), vendorName, buyerName,
									orderNo, sdf.format(orderDate), details, String.valueOf(so.getOrderAmount()),
									recipient, telPhone, mobile, address, sdf.format(now));
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("unsendNotice", e);
		}
	}

	private void sendNotification(String title, String message, String createId, Integer msgCategoryPk) {

		E7Message e7Message = new E7Message();
		e7Message.setActive(Constant.TRUE);
		e7Message.setContent(message);
		e7Message.setCreateDt(new Date());
		e7Message.setCreateId(Constant.STYSTEM);
		e7Message.setOwnerID(createId);
		e7Message.setIsRead(Constant.FLASE);
		e7Message.setTitle(title);
		MsgCategory msgCategory = new MsgCategory();
		msgCategory.setPkMsgCategory(1);
		e7Message.setMsgCategory(msgCategory);
		e7MessageRepository.save(e7Message);
	}

	private boolean isRemain(SubOrderDetail detail) {
		return detail != null && detail.getStatus() != null
				&& Arrays.asList(OrderStatusEnum.P.toString(), OrderStatusEnum.D.toString(),
						OrderStatusEnum.H.toString(), OrderStatusEnum.G.toString(), OrderStatusEnum.J.toString())
						.contains(detail.getStatus());

	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public List<SubOrderDetailHis> querySubOrderDetailHis(String pkMainOrder) {
		List<SubOrderDetailHis> list = null;
		if (StringUtils.isNotEmpty(pkMainOrder)) {
			list = subOrderDetailHisRepository.findByPkMainOrder(pkMainOrder);
			if (list != null) {
				list.size();
			}
		}
		return list;
	}

}
