package com.e7learning.po.form;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;

import com.e7learning.common.form.BaseForm;
import com.e7learning.repository.model.MainOrder;
import com.e7learning.repository.model.SubOrder;

/**
 * @author J
 * 
 * 訂單管理
 *
 */
public class MainOrderForm extends BaseForm<MainOrder>{
	/**
	 * 主訂單編號
	 */
	private String pkMainOrder;
	/**
	 * 訂單編號
	 */
	private String pkSubOrder;
	/**
	 * 訂單商品編號
	 */
	private String pkSubOrderDetail;
	/**
	 * 廠商帳號
	 */
	private String uid;
	/**
	 * 廠商名稱
	 */
	private String vendorName;
	/**
	 * 訂單日期(起)
	 */
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date orderDt;
	
	/**
	 * 訂單日期(迄)
	 */
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date orderDtEnd;
	
	/**
	 * 訂單狀態
	 */
	private String orderStatus;
	/**
	 * 購買人
	 */
	private String purchaserId;
	/**
	 * 物流業者
	 */
	private String logistics;
	/**
	 * 物流編號
	 */
	private String logisticsNum;
	/**
	 * 退貨原因
	 */
	private String returnReason;
	/**
	 * 退貨金額
	 */
	private String returnAmt;
	
	/**
	 * 退貨按鈕狀態
	 */
	private String returnBtnStatus;
	
	private Page<SubOrder> subOrderResult;
	
	/**
	 * 退貨商品清單
	 */
	private List<Integer> returnProductList;

	
	private String subOrderDetailStatus;
	
	
	private String adminRemark;
	
	private String userRemark;
	
	private String detailAdminRemark;
	
	private String detailUserRemark;
	
	private String detailVendorRemark;
	
	private Integer totalAmount;
	
	public String getPkMainOrder() {
		return pkMainOrder;
	}

	public void setPkMainOrder(String pkMainOrder) {
		this.pkMainOrder = pkMainOrder;
	}

	public String getPkSubOrder() {
		return pkSubOrder;
	}

	public void setPkSubOrder(String pkSubOrder) {
		this.pkSubOrder = pkSubOrder;
	}

	public String getPkSubOrderDetail() {
		return pkSubOrderDetail;
	}

	public void setPkSubOrderDetail(String pkSubOrderDetail) {
		this.pkSubOrderDetail = pkSubOrderDetail;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public Date getOrderDt() {
		return orderDt;
	}

	public void setOrderDt(Date orderDt) {
		this.orderDt = orderDt;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getPurchaserId() {
		return purchaserId;
	}

	public void setPurchaserId(String purchaserId) {
		this.purchaserId = purchaserId;
	}

	public String getLogistics() {
		return logistics;
	}

	public void setLogistics(String logistics) {
		this.logistics = logistics;
	}

	public String getLogisticsNum() {
		return logisticsNum;
	}

	public void setLogisticsNum(String logisticsNum) {
		this.logisticsNum = logisticsNum;
	}

	public String getReturnReason() {
		return returnReason;
	}

	public void setReturnReason(String returnReason) {
		this.returnReason = returnReason;
	}

	public String getReturnAmt() {
		return returnAmt;
	}

	public void setReturnAmt(String returnAmt) {
		this.returnAmt = returnAmt;
	}

	public String getReturnBtnStatus() {
		return returnBtnStatus;
	}

	public void setReturnBtnStatus(String returnBtnStatus) {
		this.returnBtnStatus = returnBtnStatus;
	}

	public Page<SubOrder> getSubOrderResult() {
		return subOrderResult;
	}

	public void setSubOrderResult(Page<SubOrder> subOrderResult) {
		this.subOrderResult = subOrderResult;
	}

	public List<Integer> getReturnProductList() {
		return returnProductList;
	}

	public void setReturnProductList(List<Integer> returnProductList) {
		this.returnProductList = returnProductList;
	}

	public String getSubOrderDetailStatus() {
		return subOrderDetailStatus;
	}

	public void setSubOrderDetailStatus(String subOrderDetailStatus) {
		this.subOrderDetailStatus = subOrderDetailStatus;
	}

	public Date getOrderDtEnd() {
		return orderDtEnd;
	}

	public void setOrderDtEnd(Date orderDtEnd) {
		this.orderDtEnd = orderDtEnd;
	}

	public String getAdminRemark() {
		return adminRemark;
	}

	public void setAdminRemark(String adminRemark) {
		this.adminRemark = adminRemark;
	}

	public String getUserRemark() {
		return userRemark;
	}

	public void setUserRemark(String userRemark) {
		this.userRemark = userRemark;
	}

	public String getDetailAdminRemark() {
		return detailAdminRemark;
	}

	public void setDetailAdminRemark(String detailAdminRemark) {
		this.detailAdminRemark = detailAdminRemark;
	}

	public String getDetailUserRemark() {
		return detailUserRemark;
	}

	public void setDetailUserRemark(String detailUserRemark) {
		this.detailUserRemark = detailUserRemark;
	}

	public String getDetailVendorRemark() {
		return detailVendorRemark;
	}

	public void setDetailVendorRemark(String detailVendorRemark) {
		this.detailVendorRemark = detailVendorRemark;
	}

	public Integer getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}
	
}
