package com.e7learning.po.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.e7learning.po.form.MainOrderForm;

@Component
public class MainOrderFormValidation implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(MainOrderForm.class);
	}
	@Override
	public void validate(Object target, Errors errors) {
		MainOrderForm form = (MainOrderForm) target;
//		if(StringUtils.isNotBlank(form.getMainOrderNo()) ){
//			if(!ValidatorUtil.isNumber(form.getMainOrderNo())) {
//				errors.rejectValue("mainOrderNo", "validate.number.format");
//			}
//		}
//		if(StringUtils.isNotBlank(form.getPkOrder()) ){
//			if(!ValidatorUtil.isNumber(form.getPkOrder())) {
//				errors.rejectValue("pkOrder", "validate.number.format");
//			}
//		}
		
		if (errors.hasErrors()) {
			return;
		}
	}
}