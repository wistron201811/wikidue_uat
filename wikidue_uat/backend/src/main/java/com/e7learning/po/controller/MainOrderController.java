package com.e7learning.po.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.e7learning.common.controller.BaseController;
import com.e7learning.common.enums.OrderStatusEnum;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.po.form.MainOrderForm;
import com.e7learning.po.service.MainOrderService;
import com.e7learning.po.validation.MainOrderFormValidation;
import com.e7learning.repository.model.MainOrder;
import com.e7learning.repository.model.SubOrder;
import com.e7learning.repository.model.SubOrderDetail;
import com.e7learning.repository.model.SubOrderDetailHis;

/**
 * @author J
 *
 */
@Controller
@RequestMapping(value = "/order")
@SessionAttributes(MainOrderController.FORM_BEAN_NAME)
public class MainOrderController extends BaseController {

	private static final Logger log = Logger.getLogger(MainOrderController.class);

	public static final String FORM_BEAN_NAME = "mainOrderForm";

	private static final String QUERY_PAGE_ADMIN = "queryMainOrderPageAdmin";

	private static final String QUERY_DETAIL_PAGE_ADMIN = "queryDetailPageAdmin";

	private static final String QUERY_PAGE = "queryMainOrderPage";

	private static final String QUERY_DETAIL_PAGE = "queryDetailPage";

	private static final String REFUND_PAGE_ADMIN = "refundAdmin";
	
	private static final String STATUS_HIS_PAGE = "orderStatuHis";

	@Autowired
	private MainOrderFormValidation mainFormValidation;

	@Autowired
	private MainOrderService mainOrderService;

	@InitBinder(MainOrderController.FORM_BEAN_NAME)
	protected void initBinder(WebDataBinder binder, HttpSession session) {
		binder.setValidator(mainFormValidation);
	}

	@RequestMapping(value = "/query", method = { RequestMethod.GET })
	public String goQuery(Model model) {
		MainOrderForm form = new MainOrderForm();
		if (mainOrderService.isAdmin()) {
			try {
				form.setResult(mainOrderService.queryOrderAdmin(form, 1));
				form.setTotalAmount(mainOrderService.queryOrderTotalAdmin(form));
			} catch (ServiceException e) {
			}
			model.addAttribute(FORM_BEAN_NAME, form);
			return QUERY_PAGE_ADMIN;
		} else {
			try {
				form.setSubOrderResult(mainOrderService.queryOrder(form, 1));
				form.setTotalAmount(mainOrderService.queryOrderTotal(form));
			} catch (ServiceException e) {
			}
			model.addAttribute(FORM_BEAN_NAME, form);
			return QUERY_PAGE;
		}
	}

	@RequestMapping(value = "/query", params = "method=query")
	public String query(@Validated MainOrderForm form, BindingResult result, HttpServletRequest request, Model model) {
		if (result.hasErrors()) {
			if (mainOrderService.isAdmin()) {
				return QUERY_PAGE_ADMIN;
			} else {
				return QUERY_PAGE;
			}
		}
		String pageName = new ParamEncoder("list").encodeParameterName(TableTagParameters.PARAMETER_PAGE);
		int nextPage = getPage(request.getParameter(pageName));
		if (mainOrderService.isAdmin()) {
			try {
				form.setResult(mainOrderService.queryOrderAdmin(form, nextPage));
				form.setTotalAmount(mainOrderService.queryOrderTotalAdmin(form));
			} catch (ServiceException e) {
			}
			model.addAttribute(FORM_BEAN_NAME, form);
			return QUERY_PAGE_ADMIN;
		} else {
			try {
				form.setSubOrderResult(mainOrderService.queryOrder(form, nextPage));
				form.setTotalAmount(mainOrderService.queryOrderTotal(form));
			} catch (ServiceException e) {
			}
			model.addAttribute(FORM_BEAN_NAME, form);
			return QUERY_PAGE;
		}
	}

	@RequestMapping(value = "/queryDetail/{pkOrder}/{time}", method = { RequestMethod.GET })
	public String queryDetail(@PathVariable String pkOrder, Model model, MainOrderForm form,
			HttpServletRequest request) {
		if (mainOrderService.isAdmin()) {
			// admin身份，傳入的是mainOrderNo
			if (StringUtils.isNotBlank(pkOrder)) {
				form.setPkMainOrder(pkOrder);
				MainOrder mainOrder = mainOrderService.getMainOrder(pkOrder);
				if(mainOrder!=null) {
					form.setAdminRemark(mainOrder.getAdminRemark());
					form.setUserRemark(mainOrder.getUserRemark());
					model.addAttribute("mainOrder", mainOrder);
					model.addAttribute("returnBtnStatus", mainOrderService.checkBtnStatus(mainOrder));
				}
				model.addAttribute(FORM_BEAN_NAME, form);
			}
			return QUERY_DETAIL_PAGE_ADMIN;
		} else {
			if (StringUtils.isNotBlank(pkOrder)) {
				form.setPkSubOrder(pkOrder);
				SubOrder subOrder = mainOrderService.getSubOrder(Integer.parseInt(pkOrder));
				try {
					mainOrderService.checkVendorAuth(subOrder);
				} catch (ServiceException e) {
					return QUERY_DETAIL_PAGE;
				}

				model.addAttribute("subOrder", subOrder);
				model.addAttribute("showDeliverBtn", mainOrderService.getDeliveryBtn(subOrder));
				model.addAttribute(FORM_BEAN_NAME, form);
			}
			return QUERY_DETAIL_PAGE;
		}
	}

	@RequestMapping(params = "method=modify", value = "/deliever", method = { RequestMethod.POST })
	public String deliever(MainOrderForm form) {
		try {
			try {
				SubOrder subOrder = mainOrderService.getSubOrder(Integer.valueOf(form.getPkSubOrder()));
				mainOrderService.checkVendorAuth(subOrder);
			} catch (ServiceException e) {
				return QUERY_DETAIL_PAGE;
			}
			mainOrderService.deliever(form);
			
		} catch (ServiceException e) {
		}
		return "redirect:/order/queryDetail/" + form.getPkSubOrder() + "/" + System.currentTimeMillis() + "/?method=query";
	}

	
	/**
	 * 取消訂單
	 * @param model
	 * @param form
	 * @return
	 */
	@RequestMapping(params = "method=cancelPo", value = "/return", method = { RequestMethod.POST })
	public String cancelPo(Model model, MainOrderForm form) {
		String pkMainOrder = form.getPkMainOrder();
		long time = System.currentTimeMillis();
		try {
			mainOrderService.cancelPo(form);
		} catch (Exception e) {
			log.error("e", e);
		}
		return "redirect:/order/queryDetail/" + pkMainOrder + "/" + time + "/?method=query";
	}

	/**
	 * @param model
	 * @param form
	 * @return
	 */
	@RequestMapping(params = "method=goReturn", value = "/return", method = { RequestMethod.POST })
	public String goReturn(Model model, MainOrderForm form) {
		try {
			List<SubOrderDetail> detailList = mainOrderService.getAllSubOrderDetail(form.getPkMainOrder());
			model.addAttribute("detailList", detailList);
			model.addAttribute("showReturnBtn", mainOrderService.getReturnBtn(detailList));
			model.addAttribute("showRefundBtn", mainOrderService.getRefundBtn(detailList));			
		} catch (Exception e) {
			log.error("e", e);
		}
		return REFUND_PAGE_ADMIN;
	}

	@RequestMapping(params = "method=returnProduct", value = "/return", method = { RequestMethod.POST })
	public String returnProduct(Model model, MainOrderForm form) {
		try {
			mainOrderService.returnProduct(form);
		} catch (Exception e) {
			log.error("e", e);
		}
		String pkMainOrder = form.getPkMainOrder();
		long time = System.currentTimeMillis();
		return "redirect:/order/queryDetail/" + pkMainOrder + "/" + time + "/?method=query";
	}

	/**
	 * 退貨處理
	 * 
	 * @param model
	 * @param form
	 * @return
	 */
	@RequestMapping(params = "method=returnHandling", value = "/return", method = { RequestMethod.POST })
	public String returnHandling(Model model, MainOrderForm form) {
		try {
			mainOrderService.returnHandling(form);
			String pkSubOrder = form.getPkSubOrder();
			long time = System.currentTimeMillis();
			return "redirect:/order/queryDetail/" + pkSubOrder + "/" + time + "/?method=query";
		} catch (ServiceException e) {
		}
		return QUERY_DETAIL_PAGE;
	}

	/**
	 * 退款
	 * 
	 * @param model
	 * @param form
	 * @return
	 */
	@RequestMapping(params = "method=refundProduct", value = "/return", method = { RequestMethod.POST })
	public String refundProduct(Model model, MainOrderForm form) {
		try {
			mainOrderService.refundProduct(form);
			String pkMainOrder = form.getPkMainOrder();
			long time = System.currentTimeMillis();
			return "redirect:/order/queryDetail/" + pkMainOrder + "/" + time + "/?method=query";
		} catch (ServiceException e) {
		}
		return QUERY_DETAIL_PAGE_ADMIN;
	}

	/**
	 * 訂單備註
	 * 
	 * @param model
	 * @param form
	 * @return
	 */
	@RequestMapping(params = "method=saveMainRemark", value = "/remark", method = { RequestMethod.POST })
	public String saveMainRemark(Model model, MainOrderForm form) {
		try {
			mainOrderService.saveMainRemark(form);
			String pkMainOrder = form.getPkMainOrder();
			long time = System.currentTimeMillis();
			return "redirect:/order/queryDetail/" + pkMainOrder + "/" + time + "/?method=query";
		} catch (ServiceException e) {
		}
		return QUERY_DETAIL_PAGE_ADMIN;
	}
	
	/**
	 * 明細備註
	 * 
	 * @param model
	 * @param form
	 * @return
	 */
	@RequestMapping(params = "method=saveDeatilRemark", value = "/remark", method = { RequestMethod.POST })
	public String saveDeatilRemark(Model model, MainOrderForm form) {
		try {
			mainOrderService.saveDetailRemark(form);
			String pkMainOrder = form.getPkMainOrder();
			long time = System.currentTimeMillis();
			return "redirect:/order/queryDetail/" + pkMainOrder + "/" + time + "/?method=query";
		} catch (ServiceException e) {
		}
		return QUERY_DETAIL_PAGE_ADMIN;
	}
	
	/**
	 * 廠商備註
	 * 
	 * @param model
	 * @param form
	 * @return
	 */
	@RequestMapping(params = "method=saveVendorRemark", value = "/remark", method = { RequestMethod.POST })
	public String saveVendorRemark(Model model, MainOrderForm form) {
		try {
			mainOrderService.saveVendorRemark(form);
			String pkSubOrder = form.getPkSubOrder();
			long time = System.currentTimeMillis();
			return "redirect:/order/queryDetail/" + pkSubOrder + "/" + time + "/?method=query";
		} catch (ServiceException e) {
		}
		return QUERY_DETAIL_PAGE;
	}
	
	/**
	 * 訂單歷程
	 * 
	 * @param model
	 * @param form
	 * @return
	 */
	@RequestMapping(value = "/queryStatusHis/{pkMainOrder}/{time}")
	public String queryStatusHst(@PathVariable String pkMainOrder, Model model, MainOrderForm form) {
		form.setPkMainOrder(pkMainOrder);
		List<SubOrderDetailHis> list = mainOrderService.querySubOrderDetailHis(pkMainOrder);
		if(list!=null) {
			model.addAttribute("pkMainOrder", pkMainOrder);
			model.addAttribute("statusHstList", list);
		}
		model.addAttribute(FORM_BEAN_NAME, form);
		return STATUS_HIS_PAGE;
	}
}
