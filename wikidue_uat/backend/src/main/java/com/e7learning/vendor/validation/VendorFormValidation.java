package com.e7learning.vendor.validation;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.e7learning.common.Constant;
import com.e7learning.common.ValidatorUtil;
import com.e7learning.common.utils.E7ValidationUtils;
import com.e7learning.repository.VendorRepository;
import com.e7learning.repository.model.Vendor;
import com.e7learning.vendor.form.VendorForm;

@Component
public class VendorFormValidation implements Validator {

	@Autowired
	private VendorRepository vendorRepository;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(VendorForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		VendorForm form = (VendorForm) target;
		if (StringUtils.equalsIgnoreCase("save", form.getMethod())
				|| StringUtils.equalsIgnoreCase("modify", form.getMethod())) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "vendorName", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "propsSplitRatio", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "uid", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "courseSplitRatio", "validate.empty.message");
		}
		if (StringUtils.equalsIgnoreCase("save", form.getMethod())) {
			if (isDuplicateUid(form.getUid())) {
				errors.rejectValue("uid", "vendor.validate.duplicate");
			}
		}
		if (StringUtils.isNotEmpty(form.getVendorCode())) {
			if(this.isDuplicateVendorCode(form.getUid(), form.getVendorCode())) {
				errors.rejectValue("vendorCode", "vendor.validate.code");
			}
		}
		// uid為email格式
		if (StringUtils.isNotEmpty(form.getUid())) {
			if (!ValidatorUtil.isMail(form.getUid())) {
				errors.rejectValue("uid", "validate.mail.format");
			}
		}
		if (StringUtils.isNotEmpty(form.getEmail())) {
			if (!ValidatorUtil.isMail(form.getEmail())) {
				errors.rejectValue("email", "validate.mail.format");
			}
		}
		if (StringUtils.isNotEmpty(form.getMobile())) {
			if (!ValidatorUtil.isMobile(form.getMobile())) {
				errors.rejectValue("mobile", "validate.mobile.format");
			}
		}
		if (StringUtils.isNotEmpty(form.getFaxNo())) {
			if (!ValidatorUtil.isNumber(form.getFaxNo())) {
				errors.rejectValue("faxNo", "validate.format.error");
			}
		}
		if (StringUtils.isNotEmpty(form.getAccount()) && StringUtils.isNotEmpty(form.getBankCode())) {
			if (!ValidatorUtil.isNumber(form.getAccount())) {
				errors.rejectValue("bankCode", "vendor.validate.account");
			} else if (!ValidatorUtil.isNumber(form.getAccount())) {
				errors.rejectValue("bankCode", "vendor.validate.account");
			}
		}

		if (StringUtils.isNotEmpty(form.getBankCode()) || StringUtils.isNotEmpty(form.getAccount())) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "bankCode", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "account", "validate.empty.message");
		}
		if (StringUtils.isNotEmpty(form.getTaxId())) {
			if (!ValidatorUtil.isNumber(form.getTaxId())) {
				errors.rejectValue("taxId", "validate.taxid.format");
			} else {
				if (!ValidatorUtil.isValidTWBID(form.getTaxId())) {
					errors.rejectValue("taxId", "validate.taxid.format");
				}
			}
			if(this.isDuplicateTaxId(form.getUid(), form.getTaxId())) {
				errors.rejectValue("taxId", "vendor.validate.taxId");
			}
		}
		
		this.validateTel(form, errors);
		
		this.validateCompanyPhone(form, errors);
		
		// 驗證圖片
		if (form.getPhoto() != null && form.getPhoto().getSize() > 0) {
			if (form.getPhoto().getSize() > Constant.UPLOAD_FILE_SIZE) {
				errors.rejectValue("photo", "validate.upload.size", new Object[] { Constant.UPLOAD_FILE_SIZE_MB },
						"system.error");
			} else {
//				String fileName = form.getPhoto().getOriginalFilename().trim();
				if (!E7ValidationUtils.isImageFile(form.getPhoto())) {
					errors.rejectValue("photo", "image.type.error");
				}
			}
		}
		// 驗證合約日期
		if (form.getContractSDate() != null || form.getContractEDate() != null) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "contractSDate", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "contractEDate", "validate.empty.message");
		}
		if (form.getContractSDate() != null && form.getContractEDate() != null) {
			if (form.getContractEDate().compareTo(form.getContractSDate()) < 0) {
				errors.rejectValue("contractEDate", "vendor.validate.contract");
			}
		}
		
		if (StringUtils.isNotEmpty(form.getPropsSplitRatio())) {
			if (!ValidatorUtil.isNumber(form.getPropsSplitRatio())) {
				errors.rejectValue("propsSplitRatio", "validate.number.format");
			}else {
				int propsSplitRatio = Integer.parseInt(form.getPropsSplitRatio());
				if(propsSplitRatio < 0 || propsSplitRatio > 100) {
					errors.rejectValue("propsSplitRatio", "vendor.validate.ratio");
				}
			}
		}
		
		if (StringUtils.isNotEmpty(form.getCourseSplitRatio())) {
			if (!ValidatorUtil.isNumber(form.getCourseSplitRatio())) {
				errors.rejectValue("courseSplitRatio", "validate.number.format");
			}else {
				int courseSplitRatio = Integer.parseInt(form.getCourseSplitRatio());
				if(courseSplitRatio < 0 || courseSplitRatio > 100) {
					errors.rejectValue("courseSplitRatio", "vendor.validate.ratio");
				}
			}
		}
		if (errors.hasErrors()) {
			return;
		}
	}

	private void validateTel(VendorForm form, Errors errors) {
		if (StringUtils.isNotEmpty(form.getAreaCode())) {
			if (!ValidatorUtil.isNumber(form.getAreaCode())) {
				errors.rejectValue("telphone", "validate.tel.format");
				return;
			}
			if (StringUtils.isEmpty(form.getTelphone())) {
				errors.rejectValue("telphone", "vendor.validate.tel");
				return;
			}
		}
		if (StringUtils.isNotEmpty(form.getTelphone())) {
			if (!ValidatorUtil.isNumber(form.getTelphone())) {
				errors.rejectValue("telphone", "validate.tel.format");
				return;
			}
			if (StringUtils.isEmpty(form.getAreaCode())) {
				errors.rejectValue("telphone", "vendor.validate.zipcode");
				return;
			}
			if (!form.getAreaCode().startsWith("0")) {
				errors.rejectValue("telphone", "validate.tel.format");
				return;
			}
		}
		if (StringUtils.isNotEmpty(form.getExt())) {
			if (!ValidatorUtil.isNumber(form.getExt())) {
				errors.rejectValue("telphone", "validate.tel.format");
				return;
			}
			if (StringUtils.isEmpty(form.getTelphone())) {
				errors.rejectValue("telphone", "vendor.validate.tel");
				return;
			}
			if (StringUtils.isEmpty(form.getAreaCode())) {
				errors.rejectValue("telphone", "vendor.validate.zipcode");
				return;
			}
		}
	}

	private void validateCompanyPhone(VendorForm form, Errors errors) {
		if (StringUtils.isNotEmpty(form.getAreaCode1())) {
			if (!ValidatorUtil.isNumber(form.getAreaCode1())) {
				errors.rejectValue("companyPhone", "validate.tel.format");
				return;
			}
			if (StringUtils.isEmpty(form.getCompanyPhone())) {
				errors.rejectValue("companyPhone", "vendor.validate.tel");
				return;
			}
		}
		if (StringUtils.isNotEmpty(form.getCompanyPhone())) {
			if (!ValidatorUtil.isNumber(form.getCompanyPhone())) {
				errors.rejectValue("companyPhone", "validate.tel.format");
				return;
			}
			if (StringUtils.isEmpty(form.getAreaCode1())) {
				errors.rejectValue("companyPhone", "vendor.validate.zipcode");
				return;
			}
			if (!form.getAreaCode1().startsWith("0")) {
				errors.rejectValue("companyPhone", "validate.tel.format");
				return;
			}
		}
		if (StringUtils.isNotEmpty(form.getExt1())) {
			if (!ValidatorUtil.isNumber(form.getExt1())) {
				errors.rejectValue("companyPhone", "validate.tel.format");
				return;
			}
			if (StringUtils.isEmpty(form.getCompanyPhone())) {
				errors.rejectValue("companyPhone", "vendor.validate.tel");
				return;
			}
			if (StringUtils.isEmpty(form.getAreaCode1())) {
				errors.rejectValue("companyPhone", "vendor.validate.zipcode");
				return;
			}
		}
	}

	private boolean isDuplicateUid(String uid) {
		Vendor vendor = vendorRepository.findOne(uid);
		if (vendor != null) {
			return true;
		}
		return false;
	}
	
	private boolean isDuplicateTaxId(String uid, String taxId) {
		Long result= null;
		if(uid == null) {
			result = vendorRepository.countByTaxId(taxId);
		}else {
			result = vendorRepository.countByTaxIdWithPk(taxId,uid);
		}
		if (result != null && result > 0) {
			return true;
		}
		return false;
	}
	
	private boolean isDuplicateVendorCode(String uid,String vendorCode) {
		Long result= null;
		if(uid == null) {
			result = vendorRepository.countByVendorCode(vendorCode);
		}else {
			result = vendorRepository.countByVendorCodeWithPk(vendorCode,uid);
		}
		if (result != null && result > 0) {
			return true;
		}
		return false;
	}
}