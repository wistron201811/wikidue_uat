package com.e7learning.vendor.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.service.FileStorageService;
import com.e7learning.repository.CourseRepository;
import com.e7learning.repository.ProductRepository;
import com.e7learning.repository.PropsRepository;
import com.e7learning.repository.RoleRepository;
import com.e7learning.repository.VendorRepository;
import com.e7learning.repository.model.Course;
import com.e7learning.repository.model.Product;
import com.e7learning.repository.model.Props;
import com.e7learning.repository.model.Role;
import com.e7learning.repository.model.RoleId;
import com.e7learning.repository.model.Vendor;
import com.e7learning.vendor.form.VendorForm;

/**
 * @author J
 *
 */
@Service
public class VendorService extends BaseService {

	private static final Logger log = Logger.getLogger(VendorService.class);

	@Autowired
	private VendorRepository vendorRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private FileStorageService fileStorageService;

	@Autowired
	private ConfigService configService;

	@Autowired
	private MessageSource resources;

	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private PropsRepository propsRepository;
	
	@Autowired
	private CourseRepository courseRepository;

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void addVendor(VendorForm form) throws ServiceException {
		try {
			Vendor vendor = new Vendor();
			BeanUtils.copyProperties(vendor, form);
			if (form.getPhoto() != null && !form.getPhoto().isEmpty()) {
				vendor.setPhotoId(fileStorageService.saveFile(form.getPhoto()).getPkFile());
			}
			vendor.setPropsSplitRatio(Integer.valueOf(form.getPropsSplitRatio()));
			vendor.setCourseSplitRatio(Integer.valueOf(form.getCourseSplitRatio()));
			vendor.setUpdateDt(new Date());
			vendor.setCreateDt(new Date());
			vendor.setCreateId(getCurrentUserId());
			vendor.setLastModifiedId(getCurrentUserId());
			// 重塞html
			vendor.setVendorIntroduction(
					fileStorageService.saveHtmlElementImage(form.getVendorIntroduction()).getBody());
			vendorRepository.save(vendor);

			// 加入Role
			Role role = new Role();
			RoleId roleId = new RoleId();
			role.setRoleId(roleId);
			roleId.setEmail(form.getUid());
			roleId.setRoleName("VENDOR");
			roleRepository.save(role);

		} catch (Exception e) {
			log.error("addVendor", e);
			throw new ServiceException(resources.getMessage("op.add.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.add.success", null, null));
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void saveVendor(VendorForm form) throws ServiceException {
		try {
			Vendor vendor = this.findByUid(form.getUid());
			if (vendor != null) {
				BeanUtils.copyProperties(vendor, form);
				vendor.setUpdateDt(new Date());
				if (form.getPhoto() != null && !form.getPhoto().isEmpty()) {
					fileStorageService.delFile(vendor.getPhotoId());
					vendor.setPhotoId(fileStorageService.saveFile(form.getPhoto()).getPkFile());
				}
				vendor.setPropsSplitRatio(Integer.valueOf(form.getPropsSplitRatio()));
				vendor.setCourseSplitRatio(Integer.valueOf(form.getCourseSplitRatio()));
				vendor.setUpdateDt(new Date());
				vendor.setLastModifiedId(getCurrentUserId());
				// 重塞html
				vendor.setVendorIntroduction(
						fileStorageService.updateHtmlElementImage(vendor.getVendorIntroduction(),form.getVendorIntroduction()).getBody());
			}
		} catch (Exception e) {
			log.error("editVendor", e);
			throw new ServiceException(resources.getMessage("op.modify.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.modify.success", null, null));
	}

	public Page<Vendor> queryVendor(VendorForm form, Integer page) throws ServiceException {
		try {
			Sort sort = new Sort(Direction.ASC, "vendorName");
			Pageable pageable = new PageRequest(page - 1, configService.getPageSize(), sort);
			Page<Vendor> result = vendorRepository.findAll(buildOperSpecification(form), pageable);
			return result;
		} catch (Exception e) {
			log.error("queryVendor", e);
			throw new ServiceException(resources.getMessage("op.query.error", null, null));
		}
	}

	public Vendor findByUid(String uid) {
		return vendorRepository.findOne(uid);
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void remove(String uid) throws ServiceException {
		String result = this.validateBeforeRemove(uid);
		if (StringUtils.isNotEmpty(result)) {
			throw new ServiceException(result);
		}
		try {
			Vendor vendor = vendorRepository.findOne(uid);
			vendorRepository.delete(vendor);
			if (StringUtils.isNotBlank(vendor.getPhotoId())) {
				fileStorageService.delFile(vendor.getPhotoId());
			}

			// 刪除Role
			List<Role> list = roleRepository.findUserEmail(uid, "VENDOR");
			if (list != null && list.size() > 0) {
				roleRepository.delete(list.get(0));
			}
		} catch (Exception e) {
			log.error("removeVendor", e);
			throw new ServiceException(resources.getMessage("op.del.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.del.success", null, null));
	}

	private String validateBeforeRemove(String uid) {
		String result = "";
		List<Product> list = productRepository.findByVendorUid(uid);
		if (list != null && list.size() > 0) {
			result = resources.getMessage("vendor.validate.product", null, null);
			return result;
		}
		List<Props> propsList = propsRepository.findByVendorUid(uid);
		if (propsList != null && propsList.size() > 0) {
			result = resources.getMessage("vendor.validate.product", null, null);
			return result;
		}
		List<Course> courseList = courseRepository.findByVendorUid(uid);
		if (courseList != null && courseList.size() > 0) {
			result = resources.getMessage("vendor.validate.product", null, null);
			return result;
		}
		return result;
	}

	private Specification<Vendor> buildOperSpecification(VendorForm form) {
		return new Specification<Vendor>() {
			@Override
			public Predicate toPredicate(Root<Vendor> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

				List<Predicate> predicates = new ArrayList<>();

				if (StringUtils.isNotEmpty(form.getUid())) {
					Path<String> np = root.get("uid");
					predicates.add(cb.like(np, "%" + form.getUid() + "%"));
				}
				if (StringUtils.isNotEmpty(form.getVendorName())) {
					Path<String> np = root.get("vendorName");
					predicates.add(cb.like(np, "%" + form.getVendorName() + "%"));
				}
				if (StringUtils.isNotEmpty(form.getVendorCode())) {
					Path<String> np = root.get("vendorCode");
					predicates.add(cb.like(np, "%" + form.getVendorCode() + "%"));
				}
				if (StringUtils.isNotEmpty(form.getVendorType())) {
					Path<String> np = root.get("vendorType");
					predicates.add(cb.equal(np, form.getVendorType()));
				}
				if (!predicates.isEmpty()) {
					return cb.and(predicates.toArray(new Predicate[predicates.size()]));
				}
				return cb.conjunction();
			}
		};
	}
}
