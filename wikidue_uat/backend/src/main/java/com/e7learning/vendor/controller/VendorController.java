package com.e7learning.vendor.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.e7learning.common.controller.BaseController;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.FileStorageService;
import com.e7learning.repository.model.Vendor;
import com.e7learning.vendor.form.VendorForm;
import com.e7learning.vendor.service.VendorService;
import com.e7learning.vendor.validation.VendorFormValidation;

/**
 * @author J
 *
 */
@Controller
@RequestMapping(value = "/vendor")
@SessionAttributes(VendorController.FORM_BEAN_NAME)
public class VendorController extends BaseController {

	private static final Logger log = Logger.getLogger(VendorController.class);

	public static final String FORM_BEAN_NAME = "vendorForm";
	
	private static final String QUERY_PAGE = "queryVendorPage";

	private static final String ADD_PAGE = "addVendorPage";
	
	private static final String EDIT_PAGE = "editVendorPage";

	@Autowired
	private VendorFormValidation vendorFormValidation;
	@Autowired
	private VendorService vendorService ;
	@Autowired
	private FileStorageService fileStorageService;
	
	@InitBinder(VendorController.FORM_BEAN_NAME)
	protected void initBinder(WebDataBinder binder, HttpSession session) {
		binder.setValidator(vendorFormValidation);
	}
	
	@RequestMapping(value = "/query", method = { RequestMethod.GET })
	public String goQuery(Model model) {
		VendorForm form = new VendorForm();
		try {
			form.setResult(vendorService.queryVendor(form, 1));
		} catch (ServiceException e) {
			
		}
		model.addAttribute(FORM_BEAN_NAME, form);
		return QUERY_PAGE;
	}

	@RequestMapping(value = "/query", params = "method=query")
	public String query(HttpServletRequest request, VendorForm form, Model model) {
		String pageName = new ParamEncoder("list").encodeParameterName(TableTagParameters.PARAMETER_PAGE);
		int nextPage = getPage(request.getParameter(pageName));
		try {
			form.setResult(vendorService.queryVendor(form, nextPage));
		} catch (ServiceException e) {
			
		}
		return QUERY_PAGE;
	}
	
	@RequestMapping(value = "/add", method = { RequestMethod.GET })
	public String goAdd(Model model) {
		VendorForm form = new VendorForm();
		model.addAttribute(FORM_BEAN_NAME, form);
		return ADD_PAGE;
	}

	@RequestMapping(params = "method=save", value = "/add", method = { RequestMethod.POST })
	public String add(@Validated VendorForm form, BindingResult result) {
		if (result.hasErrors()) {
			return ADD_PAGE;
		}
		try {
			vendorService.addVendor(form);
			return "redirect:/vendor/query?method=query";
		} catch (ServiceException e) {
			
		}
		return ADD_PAGE;
	}
	
	@RequestMapping(params = "method=remove", value = "/remove", method = { RequestMethod.POST })
	public String remove(VendorForm form, Model model) {
		if(StringUtils.isNotEmpty(form.getUid())) {
			Vendor vendor = vendorService.findByUid(form.getUid());
			if(vendor != null) {
				try {
					vendorService.remove(vendor.getUid());
					return "redirect:/vendor/query?method=query";
				} catch (ServiceException e) {
					// 避免display tag 查詢request發生405錯誤
					form.setResult(null);
					form.setUid("");
					form.setVendorName("");
					form.setVendorCode("");
					model.addAttribute(FORM_BEAN_NAME, form);
				}
			}
		}
		return QUERY_PAGE;
	}
	
	@RequestMapping(value = "/modify", method = { RequestMethod.POST })
	public String goModify(Model model, VendorForm form) {
		if(StringUtils.isNotEmpty(form.getUid())) {
			Vendor vendor = vendorService.findByUid(form.getUid());
			try {
				BeanUtils.copyProperties(form, vendor);
				if(vendor != null) {
					form.setPhoto(fileStorageService.getFileStorage(vendor.getPhotoId()).orElse(null));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			model.addAttribute(FORM_BEAN_NAME, form);
		}
		return EDIT_PAGE;
	}
	
	@RequestMapping(params = "method=modify", value = "/modify", method = { RequestMethod.POST })
	public String modify(@Validated VendorForm form, BindingResult result) {
		if (result.hasErrors()) {
			Vendor vendor = vendorService.findByUid(form.getUid());
			if(vendor != null) {
				form.setPhoto(fileStorageService.getFileStorage(vendor.getPhotoId()).orElse(null));
			}
			return EDIT_PAGE;
		}
		try {
			vendorService.saveVendor(form);
			return "redirect:/vendor/query?method=query";
		} catch (ServiceException e) {
			
		}
		return EDIT_PAGE;
	}
	
}
