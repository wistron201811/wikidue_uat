package com.e7learning.vendor.form;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import com.e7learning.common.enums.VendorTypeEnum;
import com.e7learning.common.form.BaseForm;
import com.e7learning.repository.model.Vendor;

/**
 * @author J
 * 
 * 廠商管理
 *
 */
public class VendorForm extends BaseForm<Vendor>{
	/**
	 * 廠商uid
	 */
	private String uid;
	/**
	 * 廠商名稱
	 */
	private String vendorName;
	/**
	 * 廠商分類
	 */
	private String vendorType;
	/**
	 * 區碼
	 */
	private String areaCode;
	/**
	 * 電話
	 */
	private String telphone;
	/**
	 * 分機
	 */
	private String ext;
	/**
	 * 手機
	 */
	private String mobile;
	/**
	 * 郵遞區號
	 */
	private String zipcode;
	
	/**
	 * 縣市
	 */
	private String county;
	
	/**
	 * 鄉鎮區
	 */
	private String district;
	/**
	 * 地址
	 */
	private String address;
	/**
	 * email
	 */
	private String email;
	/**
	 * 統編
	 */
	private String taxId;
	/**
	 * 廠商介紹
	 */
	private String vendorIntroduction;
	/**
	 * 照片
	 */
	private MultipartFile  photo;
	/**
	 * 是否刪除照片
	 */
	private boolean delPhoto=false;
	
	/**
	 * 廠商分類選單
	 */
	private List<VendorTypeEnum> vendorTypeList;
	
	private String faxNo;
	
	private String contact;
	
	private String homepage;
	
	private String remark;
	
	private String representative;
	
	private String bankCode;
	
	private String account;
	
	private String companyPhone;
	
	/**
	 * 區碼
	 */
	private String areaCode1;
	/**
	 * 分機
	 */
	private String ext1;
	
	private String propsSplitRatio;
	
	private String courseSplitRatio;
	
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date contractSDate;

	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date contractEDate;
	
	private String vendorCode;
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getVendorType() {
		return vendorType;
	}
	public void setVendorType(String vendorType) {
		this.vendorType = vendorType;
	}
	public String getTelphone() {
		return telphone;
	}
	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTaxId() {
		return taxId;
	}
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}
	public String getVendorIntroduction() {
		return vendorIntroduction;
	}
	public void setVendorIntroduction(String vendorIntroduction) {
		this.vendorIntroduction = vendorIntroduction;
	}
	public MultipartFile getPhoto() {
		return photo;
	}
	public void setPhoto(MultipartFile photo) {
		this.photo = photo;
	}
	public boolean isDelPhoto() {
		return delPhoto;
	}
	public void setDelPhoto(boolean delPhoto) {
		this.delPhoto = delPhoto;
	}
	
	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getExt() {
		return ext;
	}
	public void setExt(String ext) {
		this.ext = ext;
	}
	
	public String getFaxNo() {
		return faxNo;
	}
	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getHomepage() {
		return homepage;
	}
	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Date getContractSDate() {
		return contractSDate;
	}
	public void setContractSDate(Date contractSDate) {
		this.contractSDate = contractSDate;
	}
	public Date getContractEDate() {
		return contractEDate;
	}
	public void setContractEDate(Date contractEDate) {
		this.contractEDate = contractEDate;
	}
	public String getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	
	public String getRepresentative() {
		return representative;
	}
	public void setRepresentative(String representative) {
		this.representative = representative;
	}
	
	public String getCompanyPhone() {
		return companyPhone;
	}
	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}
	public List<VendorTypeEnum> getVendorTypeList() {
		this.vendorTypeList = new ArrayList<VendorTypeEnum>( Arrays.asList(VendorTypeEnum.values() ));
		return vendorTypeList;
	}
	public String getAreaCode1() {
		return areaCode1;
	}
	public void setAreaCode1(String areaCode1) {
		this.areaCode1 = areaCode1;
	}
	public String getExt1() {
		return ext1;
	}
	public void setExt1(String ext1) {
		this.ext1 = ext1;
	}
	public String getPropsSplitRatio() {
		return propsSplitRatio;
	}
	public void setPropsSplitRatio(String propsSplitRatio) {
		this.propsSplitRatio = propsSplitRatio;
	}
	public String getCourseSplitRatio() {
		return courseSplitRatio;
	}
	public void setCourseSplitRatio(String courseSplitRatio) {
		this.courseSplitRatio = courseSplitRatio;
	}
	
}
