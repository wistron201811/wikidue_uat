package com.e7learning.teacher.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.e7learning.common.controller.BaseController;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.FileStorageService;
import com.e7learning.repository.model.Teacher;
import com.e7learning.teacher.form.TeacherForm;
import com.e7learning.teacher.service.TeacherService;
import com.e7learning.teacher.validation.TeacherFormValidation;

/**
 * @author J
 *
 */
@Controller
@RequestMapping(value = "/teacher")
@SessionAttributes(TeacherController.FORM_BEAN_NAME)
public class TeacherController extends BaseController {

	private static final Logger log = Logger.getLogger(TeacherController.class);

	public static final String FORM_BEAN_NAME = "teacherForm";
	
	private static final String QUERY_PAGE = "queryTeacherPage";

	private static final String ADD_PAGE = "addTeacherPage";
	
	private static final String EDIT_PAGE = "editTeacherPage";

	@Autowired
	private TeacherFormValidation teacherFormValidation;
	@Autowired
	private TeacherService teacherService ;
	@Autowired
	private FileStorageService fileStorageService;
	
	@InitBinder(TeacherController.FORM_BEAN_NAME)
	protected void initBinder(WebDataBinder binder, HttpSession session) {
		binder.setValidator(teacherFormValidation);
	}
	
	@RequestMapping(value = "/query", method = { RequestMethod.GET })
	public String goQuery(Model model) {
		TeacherForm form = new TeacherForm();
		try {
			form.setResult(teacherService.queryTeacher(form, 1));
		} catch (ServiceException e) {
			
		}
		model.addAttribute(FORM_BEAN_NAME, form);
		return QUERY_PAGE;
	}

	@RequestMapping(value = "/query", params = "method=query")
	public String query(HttpServletRequest request, TeacherForm form, Model model) {
		String pageName = new ParamEncoder("list").encodeParameterName(TableTagParameters.PARAMETER_PAGE);
		int nextPage = getPage(request.getParameter(pageName));
		try {
			form.setResult(teacherService.queryTeacher(form, nextPage));
		} catch (ServiceException e) {
			
		}
		return QUERY_PAGE;
	}
	
	@RequestMapping(value = "/add", method = { RequestMethod.GET })
	public String goAdd(Model model) {
		TeacherForm form = new TeacherForm();
		model.addAttribute(FORM_BEAN_NAME, form);
		return ADD_PAGE;
	}

	@RequestMapping(params = "method=save", value = "/add", method = { RequestMethod.POST })
	public String add(@Validated TeacherForm form, BindingResult result) {
		if (result.hasErrors()) {
			return ADD_PAGE;
		}
		try {
			teacherService.addTeacher(form);
			return "redirect:/teacher/query?method=query";
		} catch (ServiceException e) {
			
		}
		return ADD_PAGE;
	}
	
	@RequestMapping(params = "method=remove", value = "/remove", method = { RequestMethod.POST })
	public String remove(TeacherForm form, Model model) {
		if(form.getPkTeacher() > 0) {
			Teacher teacher = teacherService.findByPkTeacher(form.getPkTeacher());
			if(teacher != null) {
				try {
					teacherService.remove(teacher.getPkTeacher());
					return "redirect:/teacher/query?method=query";
				} catch (ServiceException e) {
					// 避免display tag 查詢request發生405錯誤
					form.setResult(null);
					model.addAttribute(FORM_BEAN_NAME, form);
				}
			}
		}
		return QUERY_PAGE;
	}
	
	@RequestMapping(value = "/modify", method = { RequestMethod.POST })
	public String goModify(Model model, TeacherForm form) {
		if(form.getPkTeacher() > 0) {
			Teacher teacher = teacherService.findByPkTeacher(form.getPkTeacher());
			try {
				BeanUtils.copyProperties(form, teacher);
				if(teacher != null) {
					form.setPhoto(fileStorageService.getFileStorage(teacher.getPhotoId()).orElse(null));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			model.addAttribute(FORM_BEAN_NAME, form);
		}
		return EDIT_PAGE;
	}
	
	@RequestMapping(params = "method=modify", value = "/modify", method = { RequestMethod.POST })
	public String modify(@Validated TeacherForm form, BindingResult result) {
		if (result.hasErrors()) {
			Teacher teacher = teacherService.findByPkTeacher(form.getPkTeacher());
			if(teacher !=null ) {
				form.setPhoto(fileStorageService.getFileStorage(teacher.getPhotoId()).orElse(null));
			}
			return EDIT_PAGE;
		}
		try {
			teacherService.saveTeacher(form);
			return "redirect:/teacher/query?method=query";
		} catch (ServiceException e) {
			
		}
		return EDIT_PAGE;
	}
	
}
