package com.e7learning.teacher.form;

import org.springframework.web.multipart.MultipartFile;

import com.e7learning.common.form.BaseForm;
import com.e7learning.repository.model.Teacher;

/**
 * @author J
 * 
 * 講師管理
 *
 */
public class TeacherForm extends BaseForm<Teacher>{
	/**
	 * 講師PK
	 */
	private int pkTeacher;
	/**
	 * 講師姓名
	 */
	private String teacherName;
	/**
	 * 電子信箱
	 */
	private String email;
	/**
	 * 學歷
	 */
	private String education;
	/**
	 * 經歷
	 */
	private String experience;
	/**
	 * 現職
	 */
	private String job;
	/**
	 * 專長
	 */
	private String skill;
	/**
	 * 講師介紹
	 */
	private String teacherIntroduction;
	/**
	 * 照片
	 */
	private MultipartFile  photo;
	/**
	 * 啟用
	 */
	private String  active;
	/**
	 * 是否刪除照片
	 */
	private boolean delPhoto=false;
	
	private String nickname;
	
	private String phone;
	
	public int getPkTeacher() {
		return pkTeacher;
	}
	public void setPkTeacher(int pkTeacher) {
		this.pkTeacher = pkTeacher;
	}
	public String getTeacherName() {
		return teacherName;
	}
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}
	public String getEducation() {
		return education;
	}
	public void setEducation(String education) {
		this.education = education;
	}
	public String getExperience() {
		return experience;
	}
	public void setExperience(String experience) {
		this.experience = experience;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public String getSkill() {
		return skill;
	}
	public void setSkill(String skill) {
		this.skill = skill;
	}
	public MultipartFile getPhoto() {
		return photo;
	}
	public void setPhoto(MultipartFile photo) {
		this.photo = photo;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public boolean isDelPhoto() {
		return delPhoto;
	}
	public void setDelPhoto(boolean delPhoto) {
		this.delPhoto = delPhoto;
	}
	public String getTeacherIntroduction() {
		return teacherIntroduction;
	}
	public void setTeacherIntroduction(String teacherIntroduction) {
		this.teacherIntroduction = teacherIntroduction;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
}
