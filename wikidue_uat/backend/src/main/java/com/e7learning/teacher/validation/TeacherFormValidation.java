package com.e7learning.teacher.validation;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.e7learning.common.Constant;
import com.e7learning.common.ValidatorUtil;
import com.e7learning.common.utils.E7ValidationUtils;
import com.e7learning.repository.RoleRepository;
import com.e7learning.repository.TeacherRepository;
import com.e7learning.repository.model.Role;
import com.e7learning.repository.model.Teacher;
import com.e7learning.teacher.form.TeacherForm;

@Component
public class TeacherFormValidation implements Validator {

	@Autowired
	private TeacherRepository teacherRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(TeacherForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		TeacherForm form = (TeacherForm) target;
		if (StringUtils.equalsIgnoreCase("save", form.getMethod())
				|| StringUtils.equalsIgnoreCase("modify", form.getMethod())) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nickname", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "teacherName", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "validate.empty.message");
		}
		if (StringUtils.equalsIgnoreCase("save", form.getMethod()) && StringUtils.isNotEmpty(form.getEmail())) {
			if (!ValidatorUtil.isMail(form.getEmail())) {
				errors.rejectValue("email", "validate.mail.format");
			} else if (isDuplicateEmail(form.getEmail())) {
				errors.rejectValue("email", "teacher.email.duplicate");
			} else if(!hasUserRole(form.getEmail())) {
				errors.rejectValue("email", "teacher.validate.enroll");
			}
		}
		if (StringUtils.isNotEmpty(form.getNickname())) {
			if(this.isDuplicateNickname(form.getPkTeacher(), form.getNickname())) {
				errors.rejectValue("nickname", "teacher.validate.nickname");
			}
		}
		if(StringUtils.isNotBlank(form.getPhone()) ){
			if(!ValidatorUtil.isNumber(form.getPhone())) {
				errors.rejectValue("phone", "validate.number.format");
			}
		}
		// 驗證圖片
		if (form.getPhoto() != null && form.getPhoto().getSize() > 0) {
			if (form.getPhoto().getSize() > Constant.UPLOAD_FILE_SIZE) {
				errors.rejectValue("photo", "validate.upload.size", new Object[] { Constant.UPLOAD_FILE_SIZE_MB },
						"system.error");
			} else {
//				String fileName = form.getPhoto().getOriginalFilename().trim();
				if (!E7ValidationUtils.isImageFile(form.getPhoto())) {
					errors.rejectValue("photo", "image.type.error");
				}
			}
		}

		if (errors.hasErrors()) {
			return;
		}
	}

	private boolean isDuplicateEmail(String email) {
		List<Teacher> list = teacherRepository.findByEmail(email);
		if (list != null && list.size() > 0) {
			return true;
		}
		return false;
	}

	private boolean hasUserRole(String email) {
		List<Role> list = roleRepository.findUserEmail(email, "USER");
		if (list == null || list.size() == 0) {
			// 必須先有前台使用者帳號
			return false;
		} 
		return true;
	}
	
	private boolean isDuplicateNickname(Integer pkTeacher,String nickname) {
		Long result= null;
		if(pkTeacher == null || pkTeacher == 0) {
			result = teacherRepository.countByNickname(nickname);
		}else {
			result = teacherRepository.countByNicknameWithPk(nickname,pkTeacher);
		}
		if (result != null && result > 0) {
			return true;
		}
		return false;
	}
}