package com.e7learning.teacher.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.service.FileStorageService;
import com.e7learning.repository.CourseRepository;
import com.e7learning.repository.FileStorageRepository;
import com.e7learning.repository.RoleRepository;
import com.e7learning.repository.TeacherRepository;
import com.e7learning.repository.model.Course;
import com.e7learning.repository.model.FileStorage;
import com.e7learning.repository.model.Role;
import com.e7learning.repository.model.RoleId;
import com.e7learning.repository.model.Teacher;
import com.e7learning.teacher.form.TeacherForm;

/**
 * @author J
 *
 */
@Service
public class TeacherService extends BaseService {

	private static final Logger log = Logger.getLogger(TeacherService.class);

	@Autowired
	private TeacherRepository teacherRepository;
	
	@Autowired
	private CourseRepository courseRepository;
	
	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private FileStorageService fileStorageService;

	@Autowired
	private FileStorageRepository fileStorageRepository;

	@Autowired
	private ConfigService configService;

	@Autowired
	private MessageSource resources;

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void addTeacher(TeacherForm form) throws ServiceException {
		try {
			Teacher teacher = new Teacher();
			BeanUtils.copyProperties(teacher, form);
			if (form.getPhoto() != null && !form.getPhoto().isEmpty()) {
				teacher.setPhotoId(fileStorageService.saveFile(form.getPhoto()).getPkFile());
			}
			teacher.setUpdateDt(new Date());
			teacher.setCreateDt(new Date());
			teacher.setCreateId(getCurrentUserId());
			teacher.setLastModifiedId(getCurrentUserId());
			// 重塞html
			teacher.setTeacherIntroduction(
					fileStorageService.saveHtmlElementImage(form.getTeacherIntroduction()).getBody());

			teacherRepository.save(teacher);

			// 加入Role
			String reuslt = addRole(form.getEmail());
			
		} catch (Exception e) {
			log.error("addTeacher", e);
			throw new ServiceException(resources.getMessage("op.add.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.add.success", null, null));
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void saveTeacher(TeacherForm form) throws ServiceException {
		try {
			Teacher teacher = findByPkTeacher(form.getPkTeacher());
			if (teacher != null) {
				BeanUtils.copyProperties(teacher, form);
				if (form.getPhoto() != null && !form.getPhoto().isEmpty()) {
					fileStorageService.delFile(teacher.getPhotoId());
					teacher.setPhotoId(fileStorageService.saveFile(form.getPhoto()).getPkFile());
				}
				teacher.setUpdateDt(new Date());
				teacher.setLastModifiedId(getCurrentUserId());
				// 重塞html
				teacher.setTeacherIntroduction(
						fileStorageService.updateHtmlElementImage(teacher.getTeacherIntroduction(),form.getTeacherIntroduction()).getBody());
			}
		} catch (Exception e) {
			log.error("editTeacher", e);
			throw new ServiceException(resources.getMessage("op.modify.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.modify.success", null, null));
	}

	public Page<Teacher> queryTeacher(TeacherForm form, Integer page) throws ServiceException {
		try {
			Sort sort = new Sort(Direction.ASC, "teacherName");
			Pageable pageable = new PageRequest(page - 1, configService.getPageSize(), sort);
			Page<Teacher> result = teacherRepository.findAll(buildOperSpecification(form), pageable);
			return result;
		} catch (Exception e) {
			log.error("queryTeacher", e);
			throw new ServiceException(resources.getMessage("op.query.error", null, null));
		}
	}

	public List<Teacher> findActive() {
		return teacherRepository.findActive();
	}

	public Teacher findByPkTeacher(int pkTeacher) {
		return teacherRepository.findOne(pkTeacher);
	}

	public void remove(int id) throws ServiceException {
		delete(id);
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void delete(int id) throws ServiceException {
		String result = this.validateBeforeRemove(id);
		if (StringUtils.isNoneEmpty(result)) {
			throw new ServiceException(result);
		}
		try {
			Teacher teacher = teacherRepository.findOne(id);
			teacherRepository.delete(teacher);
			if(StringUtils.isNotBlank(teacher.getPhotoId())) {
				fileStorageService.delFile(teacher.getPhotoId());
			}
			// 調整角色
			String email = teacher.getEmail();
			changeRole(email);
		} catch (Exception e) {
			log.error("removeTeacher", e);
			throw new ServiceException(resources.getMessage("op.del.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.del.success", null, null));
	}

	public FileStorage findByPkFile(String pkFile) {
		return fileStorageRepository.findOne(pkFile);
	}

	private String validateBeforeRemove(int pkTeacher) {
		String result = "";
		List<Course> curseList = courseRepository.findByPkTeacher(pkTeacher);
		if (curseList != null && curseList.size() > 0) {
			result = resources.getMessage("teacher.validate.course", null, null);
		}
		return result;
	}

	private String addRole(String email) {
		String result="";
		List<Role> list = roleRepository.findUserEmail(email,"USER");
		if(list == null || list.size() == 0) {
			// 必須先有前台使用者帳號
			result = resources.getMessage("teacher.validate.enroll", null, null);
		}else {
			roleRepository.delete(list.get(0));
			Role role = new Role();
			RoleId roleId = new RoleId();
			role.setRoleId(roleId);
			roleId.setEmail(email);
			roleId.setRoleName("TEACHER");
			roleRepository.save(role);
		}
		return result;
	}
	
	private String changeRole(String email) {
		String result="";
		List<Role> list = roleRepository.findUserEmail(email,"TEACHER");
		if(list != null &&  list.size() > 0) {
			roleRepository.delete(list.get(0));
			Role role = new Role();
			RoleId roleId = new RoleId();
			role.setRoleId(roleId);
			roleId.setEmail(email);
			roleId.setRoleName("USER");
			roleRepository.save(role);
		}
		return result;
	}
	
	private Specification<Teacher> buildOperSpecification(TeacherForm form) {
		return new Specification<Teacher>() {
			@Override
			public Predicate toPredicate(Root<Teacher> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

				List<Predicate> predicates = new ArrayList<>();

				if (!StringUtils.isEmpty(form.getNickname())) {
					Path<String> np = root.get("nickname");
					predicates.add(cb.like(np, "%" + form.getNickname() + "%"));
				}
				if (!StringUtils.isEmpty(form.getTeacherName())) {
					Path<String> np = root.get("teacherName");
					predicates.add(cb.like(np, "%" + form.getTeacherName() + "%"));
				}
				if (!StringUtils.isEmpty(form.getPhone())) {
					Path<String> np = root.get("phone");
					predicates.add(cb.like(np, "%" + form.getPhone() + "%"));
				}
				if (!StringUtils.isEmpty(form.getEmail())) {
					Path<String> np = root.get("email");
					predicates.add(cb.like(np, "%" + form.getEmail() + "%"));
				}
				if (!predicates.isEmpty()) {
					return cb.and(predicates.toArray(new Predicate[predicates.size()]));
				}
				return cb.conjunction();
			}
		};
	}
}
