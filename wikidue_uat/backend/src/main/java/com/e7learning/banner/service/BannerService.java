package com.e7learning.banner.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.ad.service.AdService;
import com.e7learning.banner.form.BannerForm;
import com.e7learning.banner.form.PublishBannerForm;
import com.e7learning.banner.vo.BannerSearchVo;
import com.e7learning.common.Constant;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.service.FileStorageService;
import com.e7learning.repository.BannerRepository;
import com.e7learning.repository.SysConfigRepository;
import com.e7learning.repository.model.Banner;
import com.e7learning.repository.model.SysConfig;
import com.google.gson.Gson;

/**
 * @author ChrisTsai
 *
 */
@Service
public class BannerService extends BaseService {

	private static final Logger log = Logger.getLogger(AdService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private ConfigService configService;

	@Autowired
	private BannerRepository bannerRepository;

	@Autowired
	private FileStorageService fileStorageService;

	@Autowired
	private SysConfigRepository sysConfigRepository;

	public Page<Banner> queryBanner(BannerForm form, Integer page) throws ServiceException {
		try {
			ConditionSpec conditionSpec = new ConditionSpec(BannerSearchVo.builder()
					.withPageable(new PageRequest(page - 1, configService.getPageSize(),
							new Sort(Direction.ASC, "publishEDate")))
					.withPublish(form.getPublish())
					.withPublishSDate(form.getPublishSDate())
					.withPublishEDate(form.getPublishEDate())
					.withCreateId(getCurrentUserId())
					.withIsAdmin(isAdmin())
					.withTitle(form.getTitle()).build());
			Page<Banner> result = bannerRepository.findAll(conditionSpec, conditionSpec.searchBean.getPageable());
			return result;
		} catch (Exception e) {
			log.error("queryBanner", e);
			throw new ServiceException(resources.getMessage("query.banner.error", null, null));
		}

	}

	public PublishBannerForm queryBannerByPublish(PublishBannerForm form) throws ServiceException {
		try {
			if (form == null) {
				form = new PublishBannerForm();
			}
			form.setPublish(bannerRepository.findByPublishOrderByPublishSDate(Constant.FLASE));
			form.setPublishTo(bannerRepository.findByPublish(Constant.TRUE));
			try {
				form.setPlaySpeed(sysConfigRepository.findOne(Constant.BANNER_AUTO_PLAY_SPEED).getValue());
			} catch (Exception e) {
				log.error("queryBanner", e);
				form.setPlaySpeed("0");
			}
			log.info(new Gson().toJson(form.getPublishTo()));
			return form;
		} catch (Exception e) {
			log.error("queryBanner", e);
			throw new ServiceException(resources.getMessage("query.banner.error", null, null));
		}

	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public PublishBannerForm publish(PublishBannerForm form) throws ServiceException {
		try {
			// 儲存輪播速度
			SysConfig sysConfig = sysConfigRepository.findOne(Constant.BANNER_AUTO_PLAY_SPEED);
			sysConfig.setValue(form.getPlaySpeed());

			if (form.getMultiselect() != null && !form.getMultiselect().isEmpty()) {
				List<Banner> publishList = bannerRepository.findByBannerIdIn(form.getMultiselect());
				for (Banner tmp : publishList) {
					tmp.setOrder(99);
					tmp.setPublish(Constant.FLASE);
					bannerRepository.save(tmp);
				}
			}
			if (form.getMultiselectTo() != null && !form.getMultiselectTo().isEmpty()) {
				int order = 1;
				for (String bannderId : form.getMultiselectTo()) {
					if (bannerRepository.exists(bannderId)) {
						Banner tmp = bannerRepository.findOne(bannderId);
						tmp.setOrder(order++);
						tmp.setPublish(Constant.TRUE);
						bannerRepository.save(tmp);
					}
				}
				// List<Banner> publishToList =
				// bannerRepository.findByBannerIdIn(form.getMultiselectTo());
				// int order = 1;
				// for (Banner tmp : publishToList) {
				// tmp.setOrder(order++);
				// tmp.setPublish(Constant.TRUE);
				// bannerRepository.save(tmp);
				// }
			}

			return queryBannerByPublish(form);
		} catch (Exception e) {
			log.error("queryBanner", e);
			throw new ServiceException(resources.getMessage("query.banner.error", null, null));
		}
	}

	public BannerForm findBenner(String bannerId) throws ServiceException {
		if (StringUtils.isNotBlank(bannerId)) {
			if (bannerRepository.exists(bannerId)) {
				Banner banner = bannerRepository.findOne(bannerId);
				BannerForm form = new BannerForm();
				form.setBannerId(bannerId);
				form.setImageId(fileStorageService.getFileStorageById(banner.getImageId()));
				form.setPublish(banner.getPublish());
				form.setPublishSDate(banner.getPublishSDate());
				form.setPublishEDate(banner.getPublishEDate());
				form.setTitle(banner.getTitle());
				form.setUrl(banner.getUrl());
				return form;
			}
		}
		throw new ServiceException(resources.getMessage("query.banner.error", null, null));
	}

	public void delBannerl(String bannerId) throws ServiceException {
		try {
			if (StringUtils.isNotBlank(bannerId) && bannerRepository.exists(bannerId)) {
				Banner banner = bannerRepository.findOne(bannerId);
				if (!StringUtils.equals(banner.getCreateId(), getCurrentUserId())) {
					throw new ServiceException(resources.getMessage("delete.banner.error", null, null));
				}
				bannerRepository.delete(banner);
				fileStorageService.delFile(banner.getImageId());
			}
		} catch (Exception e) {
			log.error("delBannerl", e);
			throw new ServiceException(resources.getMessage("delete.banner.error", null, null));
		}

		throw new MessageException(resources.getMessage("delete.banner.success", null, null));
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void updateBanner(BannerForm form) throws ServiceException {
		try {
			if (StringUtils.isNotBlank(form.getBannerId())) {
				if (bannerRepository.exists(form.getBannerId())) {
					Banner banner = bannerRepository.findOne(form.getBannerId());
					if (StringUtils.isNotBlank(form.getTitle())) {
						banner.setTitle(form.getTitle());
					}
					if (form.getPublishSDate() != null) {
						banner.setPublishSDate(form.getPublishSDate());
					}
					if (form.getPublishEDate() != null) {
						banner.setPublishEDate(form.getPublishEDate());
					}
					if (form.getImageId() != null && !form.getImageId().isEmpty()) {
						fileStorageService.delFile(banner.getImageId());
						banner.setImageId(fileStorageService.saveFile(form.getImageId()).getPkFile());
					}
					banner.setUrl(form.getUrl());
					banner.setUpdateDt(new Date());
					banner.setLastModifiedId(getCurrentUserId());
				}
			}
		} catch (Exception e) {
			log.error("updateBanner", e);
			throw new ServiceException(resources.getMessage("modify.banner.error", null, null));
		}

		throw new MessageException(resources.getMessage("modify.banner.success", null, null));
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void createBanner(BannerForm form) throws ServiceException {

		try {
			Banner banner = new Banner();
			banner.setActive(Constant.TRUE);
			banner.setCreateDt(new Date());
			banner.setCreateId(getCurrentUserId());
			// 圖片
			if (form.getImageId() != null && !form.getImageId().isEmpty()) {
				banner.setImageId(fileStorageService.saveFile(form.getImageId()).getPkFile());
			}
			banner.setOrder(99);
			banner.setPublishSDate(form.getPublishSDate());
			banner.setPublishEDate(form.getPublishEDate());
			banner.setUrl(form.getUrl());
			banner.setTitle(form.getTitle());

			bannerRepository.save(banner);

		} catch (Exception e) {
			log.error("createBanner", e);
			throw new ServiceException(resources.getMessage("create.banner.error", null, null));
		}

		throw new MessageException(resources.getMessage("create.banner.success", null, null));
	}

	private static class ConditionSpec implements Specification<Banner> {

		private BannerSearchVo searchBean;

		public ConditionSpec(BannerSearchVo searchBean) {
			super();
			this.searchBean = searchBean;
		}

		@Override
		public Predicate toPredicate(Root<Banner> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

			List<Predicate> predicates = new ArrayList<>();

			if (StringUtils.isNotBlank(searchBean.getTitle())) {
				Path<String> np = root.get("title");
				predicates.add(cb.like(np, "%" + searchBean.getTitle() + "%"));
			}

			if (searchBean.getPublishSDate() != null) {
				Path<Date> np = root.get("publishSDate");
				predicates.add(cb.greaterThanOrEqualTo(np, searchBean.getPublishSDate()));
			}

			if (searchBean.getPublishEDate() != null) {
				Path<Date> np = root.get("publishEDate");
				predicates.add(cb.lessThanOrEqualTo(np, searchBean.getPublishEDate()));
			}

			if (StringUtils.isNotBlank(searchBean.getPublish())) {
				Path<String> np = root.get("publish");
				predicates.add(cb.equal(np, searchBean.getPublish()));
			}

			if (!searchBean.isAdmin())
				predicates.add(cb.equal(root.get("createId"), searchBean.getCreateId()));

			if (!predicates.isEmpty()) {
				return cb.and(predicates.toArray(new Predicate[predicates.size()]));
			}
			return cb.conjunction();
		}

	}
}
