package com.e7learning.banner.form;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import com.e7learning.common.enums.StatusCodeEnum;
import com.e7learning.common.form.BaseForm;
import com.e7learning.repository.model.Banner;

public class BannerForm extends BaseForm<Banner>{
	
	private String bannerId;

	private String title;

	private String url;

	private MultipartFile imageId;
	
	/** The publish E date. */
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date publishEDate;

	/** The publish S date. */
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date publishSDate;
	
	private String publish;
	
	private List<StatusCodeEnum> statusList = Arrays.asList(StatusCodeEnum.values());

	public String getBannerId() {
		return bannerId;
	}

	public void setBannerId(String bannerId) {
		this.bannerId = bannerId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public MultipartFile getImageId() {
		return imageId;
	}

	public void setImageId(MultipartFile imageId) {
		this.imageId = imageId;
	}

	public Date getPublishEDate() {
		return publishEDate;
	}

	public void setPublishEDate(Date publishEDate) {
		this.publishEDate = publishEDate;
	}

	public Date getPublishSDate() {
		return publishSDate;
	}

	public void setPublishSDate(Date publishSDate) {
		this.publishSDate = publishSDate;
	}

	public String getPublish() {
		return publish;
	}

	public void setPublish(String publish) {
		this.publish = publish;
	}

	public void setStatusList(List<StatusCodeEnum> statusList) {
		this.statusList = statusList;
	}

	public List<StatusCodeEnum> getStatusList() {
		return statusList;
	}

	
	
}
