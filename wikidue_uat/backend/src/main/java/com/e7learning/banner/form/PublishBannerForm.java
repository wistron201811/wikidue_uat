package com.e7learning.banner.form;

import java.util.ArrayList;
import java.util.List;

import com.e7learning.repository.model.Banner;

public class PublishBannerForm {
	
	private List<Banner> publish;
	
	private List<Banner> publishTo;
	
	private List<String> multiselect = new ArrayList<>();
	
	private List<String> multiselectTo = new ArrayList<>();
	
	private String playSpeed;

	public List<Banner> getPublish() {
		return publish;
	}

	public void setPublish(List<Banner> publish) {
		this.publish = publish;
	}

	public List<Banner> getPublishTo() {
		return publishTo;
	}

	public void setPublishTo(List<Banner> publishTo) {
		this.publishTo = publishTo;
	}

	public List<String> getMultiselect() {
		return multiselect;
	}

	public void setMultiselect(List<String> multiselect) {
		this.multiselect = multiselect;
	}

	public List<String> getMultiselectTo() {
		return multiselectTo;
	}

	public void setMultiselectTo(List<String> multiselectTo) {
		this.multiselectTo = multiselectTo;
	}

	public String getPlaySpeed() {
		return playSpeed;
	}

	public void setPlaySpeed(String playSpeed) {
		this.playSpeed = playSpeed;
	}
	
	
	

}
