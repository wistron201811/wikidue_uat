package com.e7learning.banner.validation;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.e7learning.banner.form.BannerForm;

@Component
public class BannerFormValidation implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(BannerForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {

		BannerForm form = (BannerForm) target;
		if (StringUtils.equalsIgnoreCase("save", form.getMethod())
				|| StringUtils.equalsIgnoreCase("modify", form.getMethod())) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "validate.empty.message");
			// ValidationUtils.rejectIfEmptyOrWhitespace(errors, "url",
			// "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "imageId", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "publishSDate", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "publishEDate", "validate.empty.message");
		}
		if (errors.hasErrors()) {
			return;
		}

		// 檢查格式
		UrlValidator urlValidator = new UrlValidator();
		if (StringUtils.isNotBlank(form.getUrl()) && !urlValidator.isValid(form.getUrl())) {
			errors.rejectValue("url", "validate.url.format");
		}

		if (StringUtils.equalsIgnoreCase("save", form.getMethod()) && form.getImageId().isEmpty()) {
			errors.rejectValue("imageId", "validate.empty.message");
		}

	}

}
