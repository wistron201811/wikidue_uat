package com.e7learning.banner.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.e7learning.banner.form.BannerForm;
import com.e7learning.banner.form.PublishBannerForm;
import com.e7learning.banner.service.BannerService;
import com.e7learning.banner.validation.BannerFormValidation;
import com.e7learning.common.Constant;
import com.e7learning.common.controller.BaseController;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.ServiceException;
import com.google.gson.Gson;

/**
 * @author ChrisTsai
 *
 */
@Controller
@RequestMapping(value = "/banner")
@SessionAttributes(BannerController.FORM_BEAN_NAME)
public class BannerController extends BaseController {

	private static final Logger log = Logger.getLogger(BannerController.class);

	public static final String FORM_BEAN_NAME = "bannerForm";

	public static final String PUBLISH_FORM_BEAN_NAME = "publishBannerForm";

	private static final String QUERY_PAGE = "queryBannerPage";

	private static final String ADD_PAGE = "addBannerPage";

	private static final String EDIT_PAGE = "editBannerPage";

	private static final String PUBILSH_PAGE = "publishPage";

	@Autowired
	private BannerFormValidation bannerFormValidation;

	@Autowired
	private BannerService bannerService;

	@Autowired
	private MessageSource resources;

	@InitBinder(BannerController.FORM_BEAN_NAME)
	protected void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
		binder.setValidator(bannerFormValidation);
	}

	@RequestMapping(value = "/query", method = { RequestMethod.GET })
	public String goQuery(Model model) {
		BannerForm form = new BannerForm();
		try {
			form.setResult(bannerService.queryBanner(form, 1));
		} catch (ServiceException e) {

		}
		model.addAttribute(FORM_BEAN_NAME, form);
		return QUERY_PAGE;
	}

	@RequestMapping(value = "/query", params = "method=query")
	public String query(HttpServletRequest request, BannerForm form) {

		String pageName = new ParamEncoder("result").encodeParameterName(TableTagParameters.PARAMETER_PAGE);
		int nextPage = getPage(request.getParameter(pageName));
		try {
			form.setResult(bannerService.queryBanner(form, nextPage));
		} catch (ServiceException e) {

		}
		return QUERY_PAGE;
	}

	@RequestMapping(value = "/add", method = { RequestMethod.GET })
	public String goAdd(Model model) {
		BannerForm form = new BannerForm();
		model.addAttribute(FORM_BEAN_NAME, form);
		return ADD_PAGE;
	}

	@RequestMapping(value = "/add", params = "method=save")
	public String add(HttpServletRequest request, @Validated BannerForm form, BindingResult result) {
		if (result.hasErrors()) {
			return ADD_PAGE;
		}
		try {
			bannerService.createBanner(form);
			return "redirect:/banner/query?method=query";
		} catch (ServiceException e) {

		}
		return ADD_PAGE;

	}

	@RequestMapping(value = "/modify", method = { RequestMethod.POST })
	public String goModify(Model model, BannerForm form) {
		try {
			form = bannerService.findBenner(form.getBannerId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute(FORM_BEAN_NAME, form);
		return EDIT_PAGE;
	}

	@RequestMapping(params = "method=modify", value = "/modify", method = { RequestMethod.POST })
	public String modify(@Validated BannerForm form, BindingResult result) {
		if (result.hasErrors()) {
			return EDIT_PAGE;
		}
		try {
			bannerService.updateBanner(form);
			return "redirect:/banner/query?method=query";
		} catch (ServiceException e) {

		}
		return EDIT_PAGE;
	}

	@RequestMapping(params = "method=delete", value = "/query", method = { RequestMethod.POST })
	public String remove(String bannerId) {
		try {
			bannerService.delBannerl(bannerId);

		} catch (ServiceException e) {

		}
		return "redirect:/banner/query?method=query";
	}

	@RequestMapping(value = "/publish", method = { RequestMethod.GET })
	public String goPublish(Model model, PublishBannerForm form) {
		try {
			form = bannerService.queryBannerByPublish(form);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute(PUBLISH_FORM_BEAN_NAME, form);
		return PUBILSH_PAGE;
	}

	@RequestMapping(value = "/publish", params = "method=publish", method = { RequestMethod.POST })
	public String publish(Model model, PublishBannerForm form) {
		try {
			log.info(new Gson().toJson(form));
			form = bannerService.publish(form);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute(PUBLISH_FORM_BEAN_NAME, form);
		model.addAttribute(Constant.MESSAGE_KEY, resources.getMessage("publish.banner.success", null, null));
		return PUBILSH_PAGE;
	}

}
