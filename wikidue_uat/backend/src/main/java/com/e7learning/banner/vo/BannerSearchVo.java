package com.e7learning.banner.vo;

import java.util.Date;

import org.springframework.data.domain.Pageable;
import javax.annotation.Generated;

public class BannerSearchVo {
	
	private String title;
	
	private String publish;
	
	private Date publishEDate;

	private Date publishSDate;
	
	private Pageable pageable;
	
	private String createId;
	
	private boolean admin;

	@Generated("SparkTools")
	private BannerSearchVo(Builder builder) {
		this.title = builder.title;
		this.publish = builder.publish;
		this.publishEDate = builder.publishEDate;
		this.publishSDate = builder.publishSDate;
		this.pageable = builder.pageable;
		this.createId = builder.createId;
		this.admin = builder.admin;
	}
	
	public String getCreateId() {
		return createId;
	}



	public void setCreateId(String createId) {
		this.createId = createId;
	}
	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPublish() {
		return publish;
	}

	public void setPublish(String publish) {
		this.publish = publish;
	}

	public Date getPublishEDate() {
		return publishEDate;
	}

	public void setPublishEDate(Date publishEDate) {
		this.publishEDate = publishEDate;
	}

	public Date getPublishSDate() {
		return publishSDate;
	}

	public void setPublishSDate(Date publishSDate) {
		this.publishSDate = publishSDate;
	}

	public Pageable getPageable() {
		return pageable;
	}

	public void setPageable(Pageable pageable) {
		this.pageable = pageable;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	/**
	 * Creates builder to build {@link BannerSearchVo}.
	 * @return created builder
	 */
	@Generated("SparkTools")
	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder to build {@link BannerSearchVo}.
	 */
	@Generated("SparkTools")
	public static final class Builder {
		private String title;
		private String publish;
		private Date publishEDate;
		private Date publishSDate;
		private Pageable pageable;
		private String createId;
		private boolean admin;

		private Builder() {
		}

		public Builder withTitle(String title) {
			this.title = title;
			return this;
		}

		public Builder withPublish(String publish) {
			this.publish = publish;
			return this;
		}

		public Builder withPublishEDate(Date publishEDate) {
			this.publishEDate = publishEDate;
			return this;
		}

		public Builder withPublishSDate(Date publishSDate) {
			this.publishSDate = publishSDate;
			return this;
		}

		public Builder withPageable(Pageable pageable) {
			this.pageable = pageable;
			return this;
		}

		public Builder withCreateId(String createId) {
			this.createId = createId;
			return this;
		}
		
		public Builder withIsAdmin(boolean admin) {
			this.admin = admin;
			return this;
		}

		public BannerSearchVo build() {
			return new BannerSearchVo(this);
		}
	}

	
	
	
}
