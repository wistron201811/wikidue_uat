package com.e7learning.course.form;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.e7learning.common.form.BaseForm;
import com.e7learning.common.vo.TagVo;
import com.e7learning.course.vo.CourseChapterVo;
import com.e7learning.course.vo.CourseFileVo;
import com.e7learning.repository.model.Course;
import com.e7learning.repository.model.Vendor;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
//import com.google.inject.internal.Nullable;

/**
 * @author Clarence
 * 
 *         課程管理 - 新增課程 表單
 *
 */
public class CourseForm extends BaseForm<Course> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 課程名稱
	 */
	private String courseName;
	/**
	 * 料號
	 */
	private String materialNum;
	/**
	 * 上架日期
	 */
	private String publishStartDate;
	/**
	 * 下架日期
	 */
	private String publishEndDate;
	/**
	 * 課程圖示-圖片一
	 */
	private MultipartFile imageOneFile;
	/**
	 * 課程圖示-圖片二
	 */
	private MultipartFile imageTwoFile;
	/**
	 * 原價
	 */
	private Integer price;
	/**
	 * 優惠價
	 */
	private Integer discountPrice;
	/**
	 * 進價
	 */
	private Integer costPrice;
	/**
	 * 課程簡述
	 */
	private String courseSummary;
	/**
	 * 課程目標
	 */
	private String courseExpectation;
	/**
	 * 課程介紹
	 */
	private String courseIntroduction;
	/**
	 * 適合對象
	 */
	private String courseTarget;
	/**
	 * 課前準備
	 */
	private String coursePreparation;
	/**
	 * 課程簡介-說明圖片
	 */
	private MultipartFile uploadFile;
	/**
	 * 課程簡介-說明圖片
	 */
	private List<CourseFileVo> courseFiles;
	/**
	 * 課程講師編號
	 */
	private Integer teacherId;
	/**
	 * 啟用
	 */
	private String active;
	/**
	 * 課程章節
	 */
	private List<CourseChapterVo> courseChapterList;

	private Integer chapterNo;

	private Integer unitNo;

	private Integer courseId;

	private String courseFileId;
	
	private String tagsValue;
	
	private Integer[] categories;
	
	private String videoUrl;
	
	/**
	 * 廠商名稱
	 * By J on 3/26
	 */
	private String vendorName;
	
	private String vendorUid;
	
	private List<Vendor> vendorList;

	public CourseForm() {
		this.price			= 0;
		this.discountPrice	= 0;
		this.costPrice 		= 0;
	}
	
	
	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getMaterialNum() {
		return materialNum;
	}

	public void setMaterialNum(String materialNum) {
		this.materialNum = materialNum;
	}

	public String getPublishStartDate() {
		return publishStartDate;
	}

	public void setPublishStartDate(String publishStartDate) {
		this.publishStartDate = publishStartDate;
	}

	public String getPublishEndDate() {
		return publishEndDate;
	}

	public void setPublishEndDate(String publishEndDate) {
		this.publishEndDate = publishEndDate;
	}

	public MultipartFile getImageOneFile() {
		return imageOneFile;
	}

	public void setImageOneFile(MultipartFile imageOneFile) {
		this.imageOneFile = imageOneFile;
	}

	public MultipartFile getImageTwoFile() {
		return imageTwoFile;
	}

	public void setImageTwoFile(MultipartFile imageTwoFile) {
		this.imageTwoFile = imageTwoFile;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Integer discountPrice) {
		this.discountPrice = discountPrice;
	}

	public Integer getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(Integer costPrice) {
		this.costPrice = costPrice;
	}

	public String getCourseSummary() {
		return courseSummary;
	}

	public void setCourseSummary(String courseSummary) {
		this.courseSummary = courseSummary;
	}

	public String getCourseExpectation() {
		return courseExpectation;
	}

	public void setCourseExpectation(String courseExpectation) {
		this.courseExpectation = courseExpectation;
	}

	public String getCourseIntroduction() {
		return courseIntroduction;
	}

	public void setCourseIntroduction(String courseIntroduction) {
		this.courseIntroduction = courseIntroduction;
	}

	public String getCourseTarget() {
		return courseTarget;
	}

	public void setCourseTarget(String courseTarget) {
		this.courseTarget = courseTarget;
	}

	public String getCoursePreparation() {
		return coursePreparation;
	}

	public void setCoursePreparation(String coursePreparation) {
		this.coursePreparation = coursePreparation;
	}

	public Integer getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Integer teacherId) {
		this.teacherId = teacherId;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public List<CourseChapterVo> getCourseChapterList() {
		return courseChapterList;
	}

	public void setCourseChapterList(List<CourseChapterVo> courseChapterList) {
		this.courseChapterList = courseChapterList;
	}

	public Integer getChapterNo() {
		return chapterNo;
	}

	public void setChapterNo(Integer chapterNo) {
		this.chapterNo = chapterNo;
	}

	public Integer getUnitNo() {
		return unitNo;
	}

	public void setUnitNo(Integer unitNo) {
		this.unitNo = unitNo;
	}

	public Integer getCourseId() {
		return courseId;
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}

	public MultipartFile getUploadFile() {
		return uploadFile;
	}

	public void setUploadFile(MultipartFile uploadFile) {
		this.uploadFile = uploadFile;
	}

	public List<CourseFileVo> getCourseFiles() {
		return courseFiles;
	}

	public void setCourseFiles(List<CourseFileVo> courseFiles) {
		this.courseFiles = courseFiles;
	}

	public String getCourseFileId() {
		return courseFileId;
	}

	public void setCourseFileId(String courseFileId) {
		this.courseFileId = courseFileId;
	}

	public List<TagVo> getTags() {
		return new Gson().fromJson(tagsValue, new TypeToken<List<TagVo>>(){}.getType());
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getTagsValue() {
		return tagsValue;
	}

	public void setTagsValue(String tagsValue) {
		this.tagsValue = tagsValue;
	}

	public Integer[] getCategories() {
		return categories;
	}

	public void setCategories(Integer[] categories) {
		this.categories = categories;
	}

	public String getVideoUrl() {
		return videoUrl;
	}

	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}

	public List<Vendor> getVendorList() {
		return vendorList;
	}

	public void setVendorList(List<Vendor> vendorList) {
		this.vendorList = vendorList;
	}

	public String getVendorUid() {
		return vendorUid;
	}

	public void setVendorUid(String vendorUid) {
		this.vendorUid = vendorUid;
	}
	
}
