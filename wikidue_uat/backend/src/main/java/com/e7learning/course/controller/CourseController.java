package com.e7learning.course.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.e7learning.api.domain.param.CategoryQueryParam;
import com.e7learning.category.service.CategoryService;
import com.e7learning.common.Constant;
import com.e7learning.common.controller.BaseController;
import com.e7learning.common.exception.RestException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.course.form.CourseForm;
import com.e7learning.course.service.CourseService;
import com.e7learning.course.validation.CourseFormValidation;
import com.e7learning.repository.model.Course;
import com.e7learning.repository.model.Vendor;
import com.google.gson.Gson;

@Controller
@RequestMapping(value = "/course")
@SessionAttributes(CourseController.FORM)
public class CourseController extends BaseController {

	private static final Logger LOG = Logger.getLogger(CourseController.class);

	private static final String QUERY_COURSE_PAGE = "queryCourse";
	private static final String ADD_COURSE_PAGE = "addCourse";
	private static final String EDIT_COURSE_PAGE = "editCourse";
	private static final String COURSE_VIDEO_STATUS_PAGE = "courseVideoStatus";
	private static final String COURSE_UPLOAD_FILE_PAGE = "courseUploadFile";
	public static final String FORM = "courseForm";
	private static final String TEACHER_LIST = "teacherList";
	private static final String CATEGORY_LIST = "categoryList";

	private static final String QUERY_PRODUCT_PAGE = "queryCourseForProduct";

	@Autowired
	private CourseService courseService;
	@Autowired
	private CourseFormValidation courseFormValidation;
	@Autowired
	private CategoryService categoryService;

	@InitBinder(FORM)
	protected void initBinder(WebDataBinder binder, HttpSession session) {
		binder.setValidator(courseFormValidation);
	}

	@RequestMapping(value = "/add", method = { RequestMethod.GET })
	public String goAdd(Model model) {
		model.addAttribute(TEACHER_LIST, courseService.findActiveTeacherList());
		try {
			CategoryQueryParam cp = new CategoryQueryParam();
			cp.setCategoryType("COURSE");
			model.addAttribute(CATEGORY_LIST, categoryService.queryCategory(cp));
		} catch (RestException e) {
			LOG.error("exception", e);
		}
		CourseForm courseForm = new CourseForm();
		List<Vendor> vendorList = courseService.getAllVendorList();
		// LOG.error("vendorList:"+new Gson().toJson(vendorList));
		courseForm.setVendorList(vendorList);
		if (!courseService.isAdmin()) {
			Vendor vendor = courseService.getVendor();
			if (vendor != null) {
				courseForm.setVendorUid(vendor.getUid());
				courseForm.setVendorName(vendor.getVendorName());
			}
		}
		model.addAttribute(FORM, courseForm);
		return ADD_COURSE_PAGE;
	}

	@RequestMapping(params = "method=addChapter", value = { "/add", "/modify" }, method = { RequestMethod.POST })
	public String addChapter(Model model, CourseForm form) {

		courseService.addChapter(form);
		model.addAttribute(TEACHER_LIST, courseService.findActiveTeacherList());
		try {
			CategoryQueryParam cp = new CategoryQueryParam();
			cp.setCategoryType("COURSE");
			model.addAttribute(CATEGORY_LIST, categoryService.queryCategory(cp));
		} catch (RestException e) {
			LOG.error("exception", e);
		}
		if (form.getCourseId() == null) {
			form.setVendorList(courseService.getAllVendorList());
			return ADD_COURSE_PAGE;
		} else {
			return EDIT_COURSE_PAGE;
		}
	}

	@RequestMapping(params = "method=removeChapter", value = { "/add", "/modify" }, method = { RequestMethod.POST })
	public String removeChapter(Model model, CourseForm form) {
		courseService.removeChapter(form);
		model.addAttribute(TEACHER_LIST, courseService.findActiveTeacherList());
		try {
			CategoryQueryParam cp = new CategoryQueryParam();
			cp.setCategoryType("COURSE");
			model.addAttribute(CATEGORY_LIST, categoryService.queryCategory(cp));
		} catch (RestException e) {
			LOG.error("exception", e);
		}
		if (form.getCourseId() == null) {
			form.setVendorList(courseService.getAllVendorList());
			return ADD_COURSE_PAGE;
		} else {
			return EDIT_COURSE_PAGE;
		}
	}

	@RequestMapping(params = "method=addUnit", value = { "/add", "/modify" }, method = { RequestMethod.POST })
	public String addUnit(Model model, CourseForm form) {
		courseService.addUnit(form);
		model.addAttribute(TEACHER_LIST, courseService.findActiveTeacherList());
		try {
			CategoryQueryParam cp = new CategoryQueryParam();
			cp.setCategoryType("COURSE");
			model.addAttribute(CATEGORY_LIST, categoryService.queryCategory(cp));
		} catch (RestException e) {
			LOG.error("exception", e);
		}
		if (form.getCourseId() == null) {
			form.setVendorList(courseService.getAllVendorList());
			return ADD_COURSE_PAGE;
		} else {
			return EDIT_COURSE_PAGE;
		}
	}

	@RequestMapping(params = "method=removeUnit", value = { "/add", "/modify" }, method = { RequestMethod.POST })
	public String removeUnit(Model model, CourseForm form) {
		courseService.removeUnit(form);
		model.addAttribute(TEACHER_LIST, courseService.findActiveTeacherList());
		try {
			CategoryQueryParam cp = new CategoryQueryParam();
			cp.setCategoryType("COURSE");
			model.addAttribute(CATEGORY_LIST, categoryService.queryCategory(cp));
		} catch (RestException e) {
			LOG.error("exception", e);
		}
		if (form.getCourseId() == null) {
			form.setVendorList(courseService.getAllVendorList());
			return ADD_COURSE_PAGE;
		} else {
			return EDIT_COURSE_PAGE;
		}
	}

	@RequestMapping(params = "method=add", value = "/add", method = { RequestMethod.POST })
	public String add(Model model, @Validated CourseForm form, BindingResult bindingResult) {
		try {
			System.out.println("add:" + new Gson().toJson(form));
			//int cc = bindingResult.getFieldErrorCount("price");
			//System.out.print("cc="+cc);
			if (!bindingResult.hasErrors()) {
				courseService.addCourse(form);
				return "redirect:/course/query";
			}
		} catch (Exception e) {
		}

		LOG.info("CourseForm:" + new Gson().toJson(form));
		model.addAttribute(TEACHER_LIST, courseService.findActiveTeacherList());
		try {
			CategoryQueryParam cp = new CategoryQueryParam();
			cp.setCategoryType("COURSE");
			model.addAttribute(CATEGORY_LIST, categoryService.queryCategory(cp));
		} catch (RestException e) {
			LOG.error("exception", e);
		}
		form.setVendorList(courseService.getAllVendorList());
		return ADD_COURSE_PAGE;
	}

	@RequestMapping(params = "method=remove", value = "/remove", method = { RequestMethod.POST })
	public String remove(CourseForm form, Model model) {
		if (form.getCourseId() > 0) {
			Course course = courseService.findById(form.getCourseId());
			if (course != null) {
				try {
					courseService.remove(course.getPkCourse());
					return "redirect:/course/query";
				} catch (ServiceException e) {
					// 避免display tag 查詢request發生405錯誤
					form.setResult(null);
					model.addAttribute(FORM, form);
				}
			}
		}
		return QUERY_COURSE_PAGE;
	}

	@RequestMapping(value = "/query", method = { RequestMethod.GET })
	public String goQuery(Model model) {
		CourseForm form = new CourseForm();
		try {
			form.setResult(courseService.query(form, 1));
		} catch (Exception e) {
		}
		model.addAttribute(FORM, form);
		return QUERY_COURSE_PAGE;
	}

	@RequestMapping(value = "/modify", method = { RequestMethod.POST })
	public String goModify(Model model, CourseForm form, HttpSession session) {
		if (form.getCourseId() != null) {
			// form.setVendorList(courseService.getAllVendorList());
			Course course = courseService.findById(form.getCourseId());
			if (course != null) {
				model.addAttribute(FORM, courseService.getEditForm(course));
				model.addAttribute(TEACHER_LIST, courseService.findActiveTeacherList());
				try {
					CategoryQueryParam cp = new CategoryQueryParam();
					cp.setCategoryType("COURSE");
					model.addAttribute(CATEGORY_LIST, categoryService.queryCategory(cp));
				} catch (RestException e) {
					LOG.error("exception", e);
				}

				return EDIT_COURSE_PAGE;
			}
		}
		session.setAttribute(Constant.ERROR_MESSAGE_KEY, "修改課程失敗");
		return "redirect:/course/query";
	}

	@RequestMapping(params = "method=modify", value = "/modify", method = { RequestMethod.POST })
	public String modify(Model model, @Validated CourseForm form, BindingResult bindingResult) {
		try {
			if (!bindingResult.hasErrors()) {
				courseService.editCourse(form);
				return "redirect:/course/query";
			}
		} catch (Exception e) {
		}
		model.addAttribute(TEACHER_LIST, courseService.findActiveTeacherList());
		try {
			CategoryQueryParam cp = new CategoryQueryParam();
			cp.setCategoryType("COURSE");
			model.addAttribute(CATEGORY_LIST, categoryService.queryCategory(cp));
		} catch (RestException e) {
			LOG.error("exception", e);
		}
		// form.setVendorList(courseService.getAllVendorList());
		return EDIT_COURSE_PAGE;
	}

	@RequestMapping(value = "/query", params = "method=query")
	public String goQuery(Model model, @Validated CourseForm form, HttpServletRequest request,
			BindingResult bindingResult) {
		try {
			if (!bindingResult.hasErrors()) {
				String pageName = new ParamEncoder("course").encodeParameterName(TableTagParameters.PARAMETER_PAGE);
				int nextPage = getPage(request.getParameter(pageName));
				form.setResult(courseService.query(form, nextPage));
			}
		} catch (Exception e) {
		}
		return QUERY_COURSE_PAGE;
	}

	@RequestMapping(value = "/queryVideoStatus")
	public String queryProduct(Model model, @RequestParam("courseId") Integer courseId) {
		try {
			if (courseId != null) {
				Course course = courseService.findById(courseId);
				model.addAttribute("courseChapterList", course.getCourseChapterList());
			}
		} catch (Exception e) {
		}
		return COURSE_VIDEO_STATUS_PAGE;
	}

	@RequestMapping(value = "/uploadFile", method = { RequestMethod.POST })
	public String goUploadFile(Model model, @Validated CourseForm form) {
		try {
			model.addAttribute(FORM, courseService.getUploadFileForm(form));
			return COURSE_UPLOAD_FILE_PAGE;
		} catch (Exception e) {
		}
		return "redirect:/course/query";
	}

	@RequestMapping(params = "method=uploadFile", value = "/uploadFile", method = { RequestMethod.POST })
	public String uploadFile(Model model, CourseForm form, BindingResult bindingResult) {
		try {
			if (!bindingResult.hasErrors()) {
				courseService.uploadFile(form);
				model.addAttribute(FORM, courseService.getUploadFileForm(form));
			}
		} catch (Exception e) {
		}
		return COURSE_UPLOAD_FILE_PAGE;
	}

	@RequestMapping(params = "method=deleteFile", value = "/uploadFile", method = { RequestMethod.POST })
	public String deleteFile(Model model, CourseForm form) {
		try {
			courseService.deleteFile(form);
			model.addAttribute(FORM, courseService.getUploadFileForm(form));
		} catch (Exception e) {
		}
		return COURSE_UPLOAD_FILE_PAGE;
	}

	// 商品上架-選擇課程 By J on 3/26
	@RequestMapping(value = "/queryCourse")
	public String queryCourse(Model model, CourseForm form, HttpServletRequest request) {
		try {
			String pageName = new ParamEncoder("course").encodeParameterName(TableTagParameters.PARAMETER_PAGE);
			int nextPage = getPage(request.getParameter(pageName));
			form.setResult(courseService.queryForProduct(form, nextPage));
		} catch (Exception e) {
		}
		return QUERY_PRODUCT_PAGE;
	}
}
