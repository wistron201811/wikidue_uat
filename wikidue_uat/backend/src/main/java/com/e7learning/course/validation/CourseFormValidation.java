package com.e7learning.course.validation;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

import com.e7learning.common.Constant;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.utils.E7ValidationUtils;
import com.e7learning.course.form.CourseForm;
import com.e7learning.repository.CourseRepository;
import com.e7learning.repository.PropsRepository;
import com.e7learning.repository.model.Course;
import com.e7learning.repository.model.Props;

@Component
public class CourseFormValidation implements Validator {

	private static final Logger LOG = Logger.getLogger(CourseFormValidation.class);

	private static final String DATE_FORMAT = "yyyy/MM/dd";

	@Autowired
	private PropsRepository propsRepository;

	@Autowired
	private CourseRepository courseRepository;

	@Autowired
	private ConfigService configService;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(CourseForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		CourseForm form = (CourseForm) target;
		String method = form.getMethod();
		if (StringUtils.equals(method, "add") || StringUtils.equals(method, "modify")) {
			validateEditForm(form, errors);
		} else if (StringUtils.equals(method, "query")) {
			validateQueryForm(form, errors);
		} else if (StringUtils.equals(method, "uploadFile")) {
			validateUploadFileForm(form, errors);
		}
	}

	private void validateEditForm(CourseForm form, Errors errors) {

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "courseName", "validate.empty.message");
		/*
		 * 2018/02/04:改由從課程影片統計時數長度
		 */
		// ValidationUtils.rejectIfEmptyOrWhitespace(errors, "courseHours",
		// "validate.empty.message", "");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "publishStartDate", "validate.empty.message");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "publishEndDate", "validate.empty.message");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price", "validate.empty.message");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "materialNum", "validate.empty.message");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "videoUrl", "validate.empty.message");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "courseSummary", "validate.empty.message");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "courseExpectation", "validate.empty.message");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "courseIntroduction", "validate.empty.message");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "courseTarget", "validate.empty.message");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "coursePreparation", "validate.empty.message");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "teacherId", "validate.empty.message");
		if (StringUtils.equals("add", form.getMethod())) {
			//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "vendorUid", "validate.empty.message");
		}
		if (StringUtils.equals(form.getMethod(), "add")) {
			if (form.getImageOneFile().isEmpty()) {
				//errors.rejectValue("imageOneFile", "validate.empty.upload.message");

			}
			if (form.getImageTwoFile().isEmpty()) {
				//errors.rejectValue("imageTwoFile", "validate.empty.upload.message");

			}
		}
		if (errors.hasErrors()) {
			return;
		}
		// 驗證料號是否重覆
		/*if (StringUtils.isNotBlank(form.getMaterialNum()) && this.isDuplicateMaterialNum(form)) {
			errors.rejectValue("materialNum", "props.material.duplicate");
		}*/

		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		try {
			formatter.parse(form.getPublishStartDate());
		} catch (Exception e) {
			errors.rejectValue("publishStartDate", "publish.start.date.format.error");
		}
		try {
			formatter.parse(form.getPublishEndDate());
		} catch (Exception e) {
			errors.rejectValue("publishEndDate", "publish.end.date.format.error");
		}

		/*MultipartFile imageOne = form.getImageOneFile();
		if (imageOne.getSize() > Constant.UPLOAD_FILE_SIZE) {
			errors.rejectValue("imageOneFile", "validate.upload.size", new Object[] { Constant.UPLOAD_FILE_SIZE_MB },
					"system.error");
		} else if (!E7ValidationUtils.isImageFile(imageOne)) {
			errors.rejectValue("imageOneFile", "image.type.error");

		}*/
		/*MultipartFile imageTwo = form.getImageTwoFile();
		if (imageTwo.getSize() > Constant.UPLOAD_FILE_SIZE) {
			errors.rejectValue("imageTwoFile", "validate.upload.size", new Object[] { Constant.UPLOAD_FILE_SIZE_MB },
					"system.error");
		} else if (!E7ValidationUtils.isImageFile(imageTwo)) {
			errors.rejectValue("imageTwoFile", "image.type.error");

		}*/
		if (errors.hasErrors()) {
			return;
		}

	}

	private void validateUploadFileForm(CourseForm form, Errors errors) {
		long sizeLimit = configService.getCourseFileSizeLimit() * 1024 * 1024;
		MultipartFile multipartFile = form.getUploadFile();
		if (multipartFile == null || multipartFile.isEmpty()) {
			errors.rejectValue("uploadFile", "validate.upload.format");
		} else {
			if (multipartFile.getSize() >= sizeLimit) {
				errors.rejectValue("uploadFile", "validate.upload.size", new Object[] { configService.getCourseFileSizeLimit() },
						"system.error");
			} else {

			}
		}

		if (errors.hasErrors()) {
			return;
		}

	}

	private void validateQueryForm(CourseForm form, Errors errors) {

		SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		Date publishStartDate = null;
		Date publishEndDate = null;
		if (!StringUtils.isEmpty(form.getPublishStartDate())) {
			try {
				publishStartDate = formatter.parse(form.getPublishStartDate());
			} catch (Exception e) {
				errors.reject("publishStartDate", "publish.start.date.format.error");
			}
		}
		if (!StringUtils.isEmpty(form.getPublishEndDate())) {
			try {
				publishEndDate = formatter.parse(form.getPublishEndDate());
			} catch (Exception e) {
				errors.reject("publishEndDate", "publish.end.date.format.error");
			}
		}
		if (errors.hasErrors()) {
			return;
		}
		if (publishStartDate != null && publishEndDate != null) {
			if (publishEndDate.before(publishStartDate)) {
				errors.reject("publishEndDate", "publish.end.date.early");
			}
		}
		if (errors.hasErrors()) {
			return;
		}

	}

	private boolean isDuplicateMaterialNum(CourseForm form) {
		if (form.getCourseId() != null) {
			List<Course> courseList = courseRepository.findByPkCourseAndMaterialNum(form.getCourseId(),
					form.getMaterialNum());
			if (courseList != null && !courseList.isEmpty()) {
				return true;
			}
		} else {
			List<Course> courseList = courseRepository.findByMaterialNum(form.getMaterialNum());
			if (courseList != null && !courseList.isEmpty()) {
				return true;
			}
		}
		List<Props> propsList = propsRepository.findByMaterialNum(form.getMaterialNum());
		if (propsList != null && !propsList.isEmpty()) {
			return true;
		}
		return false;
	}

}