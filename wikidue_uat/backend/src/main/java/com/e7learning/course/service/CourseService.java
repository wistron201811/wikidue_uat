package com.e7learning.course.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.common.Constant;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.service.FileStorageService;
import com.e7learning.common.utils.CommonUtils;
import com.e7learning.common.vo.TagVo;
import com.e7learning.course.form.CourseForm;
import com.e7learning.course.vo.CourseChapterVo;
import com.e7learning.course.vo.CourseFileVo;
import com.e7learning.course.vo.CourseUnitVo;
import com.e7learning.repository.CourseChapterRepository;
import com.e7learning.repository.CourseFileRepository;
import com.e7learning.repository.CourseRepository;
import com.e7learning.repository.CourseVideoRepository;
import com.e7learning.repository.LearningProgressRepository;
import com.e7learning.repository.ProductRepository;
import com.e7learning.repository.TagRepository;
import com.e7learning.repository.TeacherRepository;
import com.e7learning.repository.UserCourseRepository;
import com.e7learning.repository.VendorRepository;
import com.e7learning.repository.model.Category;
import com.e7learning.repository.model.Course;
import com.e7learning.repository.model.CourseChapter;
import com.e7learning.repository.model.CourseFile;
import com.e7learning.repository.model.CourseVideo;
import com.e7learning.repository.model.FileStorage;
import com.e7learning.repository.model.LearningProgress;
import com.e7learning.repository.model.Product;
import com.e7learning.repository.model.Tag;
import com.e7learning.repository.model.Teacher;
import com.e7learning.repository.model.UserCourse;
import com.e7learning.repository.model.Vendor;
import com.e7learning.repository.model.Video;
import com.google.gson.Gson;

@Service
public class CourseService extends BaseService {

	private static final Logger LOG = Logger.getLogger(CourseService.class);
	@Autowired
	private MessageSource msg;

	@Autowired
	private CourseRepository courseRepository;

	@Autowired
	private CourseFileRepository courseFileRepository;

	@Autowired
	private CourseChapterRepository courseChapterRepository;

	@Autowired
	private CourseVideoRepository courseVideoRepository;

	@Autowired
	private LearningProgressRepository learningProgressRepository;

	@Autowired
	private UserCourseRepository userCourseRepository;

	@Autowired
	private TagRepository tagRepository;

	@Autowired
	private TeacherRepository teacherRepository;

	@Autowired
	private FileStorageService fileStorageService;

	@Autowired
	private ConfigService configService;

	@Autowired
	private VendorRepository vendorRepository;

	@Autowired
	private ProductRepository productRepository;

	private static final String DATE_FORMAT = "yyyy/MM/dd";

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void editCourse(CourseForm form) throws ServiceException {
		try {
			Integer courseId = form.getCourseId();
			if (courseId == null) {
				return;
			}
			Course course = courseRepository.findOne(courseId);
			if (course == null) {
				return;
			}
			if (course.getTags() != null) {
				for (Tag tag : course.getTags()) {
					tagRepository.delete(tag);
				}
			}
			if (course.getCourseChapterList() != null && form.getCourseChapterList() != null) {
				for (CourseChapter chapter : course.getCourseChapterList()) {
					boolean exist = false;
					CourseChapterVo vo = null;
					for (CourseChapterVo tmp : form.getCourseChapterList()) {
						if (chapter.getPkCourseChapter() == tmp.getCourseChapterId()) {
							exist = true;
							vo = tmp;
							break;
						}
					}
					if (!exist) {
						courseVideoRepository.delete(chapter.getCourseVideoList());
						courseChapterRepository.delete(chapter);
					} else {
						if (chapter.getCourseVideoList() != null && vo.getCourseUnitList() != null) {
							for (CourseVideo video : chapter.getCourseVideoList()) {
								boolean videoExist = false;
								for (CourseUnitVo tmp2 : vo.getCourseUnitList()) {
									if (tmp2.getCourseUnitId() == video.getPkCourseVideo()) {
										videoExist = true;
										break;
									}
								}
								if (!videoExist) {
									courseVideoRepository.delete(video);
								}
							}
						}
					}
				}
			}

			Date now = new Date();
			BeanUtils.copyProperties(course, form);
			Teacher teacher = teacherRepository.findOne(form.getTeacherId());
			course.setTeacher(teacher);
			course.setUpdateDt(now);
			if (form.getImageOneFile() != null && !form.getImageOneFile().isEmpty()) {
				fileStorageService.delFile(course.getImageOne());
				course.setImageOne(fileStorageService.saveFile(form.getImageOneFile()).getPkFile());
			}
			if (form.getImageTwoFile() != null && !form.getImageTwoFile().isEmpty()) {
				fileStorageService.delFile(course.getImageTwo());
				course.setImageTwo(fileStorageService.saveFile(form.getImageTwoFile()).getPkFile());
			}
			SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
			if (!StringUtils.isEmpty(form.getPublishStartDate())) {
				try {
					course.setPublishSDate(formatter.parse(form.getPublishStartDate()));
				} catch (Exception e) {
				}
			}
			if (!StringUtils.isEmpty(form.getPublishEndDate())) {
				try {
					course.setPublishEDate(formatter.parse(form.getPublishEndDate()));
				} catch (Exception e) {
				}
			}
			course.setCourseIntroduction(
					fileStorageService.saveHtmlElementImage(form.getCourseIntroduction()).getBody());
			course.setCourseTarget(fileStorageService.saveHtmlElementImage(form.getCourseTarget()).getBody());
			course.setCourseExpectation(fileStorageService.saveHtmlElementImage(form.getCourseExpectation()).getBody());
			course.setCoursePreparation(fileStorageService.saveHtmlElementImage(form.getCoursePreparation()).getBody());
			course.setCourseChapterList(null);
			course.setTags(null);
			course = courseRepository.save(course);
			if (form.getCourseChapterList() != null && !form.getCourseChapterList().isEmpty()) {
				for (int c = 0; c < form.getCourseChapterList().size(); c++) {
					CourseChapterVo chapterVo = form.getCourseChapterList().get(c);
					if (chapterVo != null) {
						CourseChapter chapter = null;
						boolean newChapter = false;
						if (chapterVo.getCourseChapterId() != null) {
							chapter = courseChapterRepository.findOne(chapterVo.getCourseChapterId());
						}
						if (chapter == null) {
							chapter = new CourseChapter();
							newChapter = true;
						}
						chapter.setActive(Constant.TRUE);
						chapter.setCourse(course);
						chapter.setCourseChapterName(chapterVo.getName());
						chapter.setUpdateDt(now);
						if (newChapter) {
							chapter.setCreateDt(now);
							chapter.setCreateId(getCurrentUserId());

						}
						chapter.setCourseChapterNo(c + 1);
						chapter = courseChapterRepository.save(chapter);
						if (chapterVo.getCourseUnitList() != null && !chapterVo.getCourseUnitList().isEmpty()) {
							for (int u = 0; u < chapterVo.getCourseUnitList().size(); u++) {
								CourseUnitVo unitVo = chapterVo.getCourseUnitList().get(u);
								if (unitVo != null) {
									CourseVideo unit = null;
									boolean newUnit = false;
									if (unitVo.getCourseUnitId() != null) {
										unit = courseVideoRepository.findOne(unitVo.getCourseUnitId());
									}
									if (unit == null) {
										unit = new CourseVideo();
										newUnit = true;
									}
									unit.setActive(Constant.TRUE);
									unit.setCourseChapter(chapter);
									unit.setCourseVideoName(unitVo.getName());
									unit.setCourseVideoNo(u + 1);
									unit.setIsFree(unitVo.isFree() ? Constant.TRUE : Constant.FLASE);
									unit.setUpdateDt(now);
									if (newUnit) {
										Video video = new Video();
										video.setCreateDt(now);
										video.setActive(Constant.TRUE);
										video.setCreateId(getCurrentUserId());
										video.setUpdateDt(now);
										unit.setVideo(video);
										unit.setCreateDt(now);
										unit.setCreateId(getCurrentUserId());
									}
									courseVideoRepository.save(unit);
								}
							}
						}
					}
				}
			}

			if (form.getTags() != null) {
				for (TagVo vo : form.getTags()) {
					Tag tag = new Tag();
					tag.setActive(Constant.TRUE);
					tag.setCourse(course);
					tag.setTag(vo.getCode());
					tag.setTagLevel(vo.getLevel());
					tag.setTagName(vo.getName());
					tag.setCreateDt(now);
					tag.setCreateId(getCurrentUserId());
					tagRepository.save(tag);
				}
			}
			if (form.getCategories() != null && form.getCategories().length > 0) {
				for (Integer c : form.getCategories()) {
					Tag tag = new Tag();
					tag.setActive(Constant.TRUE);
					tag.setCourse(course);
					tag.setTag(StringUtils.EMPTY);
					tag.setTagLevel(StringUtils.EMPTY);
					tag.setTagName(StringUtils.EMPTY);
					Category category = new Category();
					category.setPkCategory(c);
					tag.setCategory(category);
					tag.setCreateDt(now);
					tag.setCreateId(getCurrentUserId());
					tagRepository.save(tag);
				}
			}

		} catch (Exception e) {
			LOG.error("exception", e);
			throw new ServiceException(msg.getMessage("op.modify.error", null, null));
		}
		throw new MessageException(msg.getMessage("op.modify.success", null, null));
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void addCourse(CourseForm form) throws ServiceException {
		try {
			Date now = new Date();
			Course course = new Course();
			BeanUtils.copyProperties(course, form);
			Teacher teacher = teacherRepository.findOne(form.getTeacherId());
			course.setTeacher(teacher);
			course.setCreateDt(now);
			course.setUpdateDt(now);
			course.setCreateId(getCurrentUserId());
			// 設定廠商 By J on 3/26
			course.setVendor(vendorRepository.findOne(form.getVendorUid()));
			if (form.getImageOneFile() != null && !form.getImageOneFile().isEmpty()) {
				course.setImageOne(fileStorageService.saveFile(form.getImageOneFile()).getPkFile());
			}
			if (form.getImageTwoFile() != null && !form.getImageTwoFile().isEmpty()) {
				course.setImageTwo(fileStorageService.saveFile(form.getImageTwoFile()).getPkFile());
			}
			SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
			if (!StringUtils.isEmpty(form.getPublishStartDate())) {
				try {
					course.setPublishSDate(formatter.parse(form.getPublishStartDate()));
				} catch (Exception e) {
				}
			}
			if (!StringUtils.isEmpty(form.getPublishEndDate())) {
				try {
					course.setPublishEDate(formatter.parse(form.getPublishEndDate()));
				} catch (Exception e) {
				}
			}
			course.setCourseIntroduction(
					fileStorageService.saveHtmlElementImage(form.getCourseIntroduction()).getBody());
			course.setCourseTarget(fileStorageService.saveHtmlElementImage(form.getCourseTarget()).getBody());
			course.setCourseExpectation(fileStorageService.saveHtmlElementImage(form.getCourseExpectation()).getBody());
			course.setCoursePreparation(fileStorageService.saveHtmlElementImage(form.getCoursePreparation()).getBody());
			course.setCourseChapterList(null);
			course = courseRepository.save(course);
			if (form.getCourseChapterList() != null && !form.getCourseChapterList().isEmpty()) {
				for (int c = 0; c < form.getCourseChapterList().size(); c++) {
					CourseChapterVo chapterVo = form.getCourseChapterList().get(c);
					if (chapterVo != null) {
						CourseChapter chapter = new CourseChapter();
						chapter.setActive(Constant.TRUE);
						chapter.setCourse(course);
						chapter.setCourseChapterName(chapterVo.getName());
						chapter.setCreateDt(now);
						chapter.setUpdateDt(now);
						chapter.setCreateId(getCurrentUserId());
						chapter.setCourseChapterNo(c + 1);
						chapter = courseChapterRepository.save(chapter);
						if (chapterVo.getCourseUnitList() != null && !chapterVo.getCourseUnitList().isEmpty()) {
							for (int u = 0; u < chapterVo.getCourseUnitList().size(); u++) {
								CourseUnitVo unitVo = chapterVo.getCourseUnitList().get(u);
								if (unitVo != null) {
									CourseVideo unit = new CourseVideo();
									Video video = new Video();
									video.setCreateDt(now);
									video.setActive(Constant.TRUE);
									video.setCreateId(getCurrentUserId());
									video.setUpdateDt(now);
									unit.setVideo(video);
									unit.setActive(Constant.TRUE);
									unit.setCourseChapter(chapter);
									unit.setCourseVideoName(unitVo.getName());
									unit.setIsFree(unitVo.isFree() ? Constant.TRUE : Constant.FLASE);
									unit.setCourseVideoNo(u + 1);
									unit.setCreateDt(now);
									unit.setUpdateDt(now);
									unit.setCreateId(getCurrentUserId());
									courseVideoRepository.save(unit);
								}
							}
						}
					}
				}
			}

			if (form.getTags() != null) {
				for (TagVo vo : form.getTags()) {
					Tag tag = new Tag();
					tag.setActive(Constant.TRUE);
					tag.setCourse(course);
					tag.setTag(vo.getCode());
					tag.setTagLevel(vo.getLevel());
					tag.setTagName(vo.getName());
					tag.setCreateDt(now);
					tag.setCreateId(getCurrentUserId());
					tagRepository.save(tag);
				}
			}
			if (form.getCategories() != null && form.getCategories().length > 0) {
				for (Integer c : form.getCategories()) {
					Tag tag = new Tag();
					tag.setActive(Constant.TRUE);
					tag.setCourse(course);
					tag.setTag(StringUtils.EMPTY);
					tag.setTagLevel(StringUtils.EMPTY);
					tag.setTagName(StringUtils.EMPTY);
					Category category = new Category();
					category.setPkCategory(c);
					tag.setCategory(category);
					tag.setCreateDt(now);
					tag.setCreateId(getCurrentUserId());
					tagRepository.save(tag);
				}
			}

		} catch (Exception e) {
			LOG.error("exception", e);
			throw new ServiceException(msg.getMessage("add.course.error", null, null));
		}
		throw new MessageException(msg.getMessage("add.course.success", null, null));
	}

	@Transactional(readOnly = true)
	public CourseForm getUploadFileForm(CourseForm form) throws ServiceException {
		try {
			Course course = courseRepository.findOne(form.getCourseId());
			form.setCourseName(course.getCourseName());
			form.setMaterialNum(course.getMaterialNum());
			if (course.getCourseFileList() != null) {
				course.getCourseFileList().size();
				List<CourseFileVo> fileList = new ArrayList<>();
				for (CourseFile courseFile : course.getCourseFileList()) {
					CourseFileVo vo = new CourseFileVo();
					vo.setCourseFileId(courseFile.getPkCourseFile());
					FileStorage fs = fileStorageService.getFileStorage(courseFile.getFileId()).orElse(null);
					if (fs != null) {
						vo.setFileName(fs.getFileName());
					}
					fileList.add(vo);
				}
				form.setCourseFiles(fileList);
			}
			return form;
		} catch (Exception e) {
			LOG.error("exception", e);
			throw new ServiceException(msg.getMessage("system.error", null, null));
		}

	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void deleteFile(CourseForm form) throws ServiceException {
		boolean result = false;
		try {
			CourseFile courseFile = courseFileRepository.findOne(form.getCourseFileId());
			if (courseFile != null) {
				if (courseFile.getCourse() != null && courseFile.getCourse().getPkCourse() == form.getCourseId()) {
					fileStorageService.delFile(courseFile.getFileId());
					courseFileRepository.delete(courseFile);
					result = true;
				}
			}
		} catch (Exception e) {
			LOG.error("exception", e);
		}
		if (result) {
			throw new MessageException(msg.getMessage("op.del.success", null, null));
		} else {
			throw new ServiceException(msg.getMessage("op.del.error", null, null));
		}
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void uploadFile(CourseForm form) throws ServiceException {
		boolean result = false;
		try {
			Course course = courseRepository.findOne(form.getCourseId());
			if (course != null && form.getUploadFile() != null && !form.getUploadFile().isEmpty()) {
				List<CourseFile> list = course.getCourseFileList();
				if (list != null && !list.isEmpty()) {
					long totalSize = 0L;
					for (CourseFile tmp : list) {
						FileStorage fs = fileStorageService.getFileStorageById(tmp.getFileId());
						totalSize += (long) fs.getFileSize();
					}
					totalSize += (long) form.getUploadFile().getBytes().length;
					if (totalSize >= (configService.getCourseFileSizeLimitTotal() * 1024 * 1024L)) {
						throw new ServiceException(msg.getMessage("course.flie.size.total.limit",
								new Object[] { configService.getCourseFileSizeLimitTotal() }, null));
					}
				}
				CourseFile courseFile = new CourseFile();
				courseFile.setFileId(fileStorageService.saveFile(form.getUploadFile()).getPkFile());
				courseFile.setCourse(course);
				courseFileRepository.save(courseFile);
				result = true;
			}
		} catch (ServiceException e) {
			throw e;
		} catch (Exception e) {
			LOG.error("exception", e);
		}
		if (result) {
			throw new MessageException(msg.getMessage("op.add.success", null, null));
		} else {
			throw new ServiceException(msg.getMessage("op.add.error", null, null));
		}

	}

	@Transactional(readOnly = true)
	public CourseForm getEditForm(Course course) {
		CourseForm form = new CourseForm();
		try {
			form.setCourseId(course.getPkCourse());
			BeanUtils.copyProperties(form, course);
			Teacher teacher = course.getTeacher();
			form.setTeacherId(teacher != null ? teacher.getPkTeacher() : null);
			form.setImageOneFile(StringUtils.isEmpty(course.getImageOne()) ? null
					: fileStorageService.getFileStorage(course.getImageOne()).orElse(null));
			form.setImageTwoFile(StringUtils.isEmpty(course.getImageTwo()) ? null
					: fileStorageService.getFileStorage(course.getImageTwo()).orElse(null));
			try {
				SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
				if (course.getPublishSDate() != null) {
					form.setPublishStartDate(formatter.format(course.getPublishSDate()));
					form.setPublishEndDate(formatter.format(course.getPublishEDate()));
				}
			} catch (Exception e) {

			}

			if (course.getCourseChapterList() != null) {
				List<CourseChapterVo> chapterList = new ArrayList<>();
				for (CourseChapter chapter : course.getCourseChapterList()) {
					CourseChapterVo chapterVo = new CourseChapterVo();
					chapterVo.setName(chapter.getCourseChapterName());
					chapterVo.setCourseChapterId(chapter.getPkCourseChapter());
					if (chapter.getCourseVideoList() != null) {
						List<CourseUnitVo> unitList = new ArrayList<>();
						for (CourseVideo courseVideo : chapter.getCourseVideoList()) {
							CourseUnitVo unitVo = new CourseUnitVo();
							unitVo.setCourseUnitId(courseVideo.getPkCourseVideo());
							unitVo.setName(courseVideo.getCourseVideoName());
							unitVo.setFree(StringUtils.equals(courseVideo.getIsFree(), Constant.TRUE));
							unitList.add(unitVo);
						}
						chapterVo.setCourseUnitList(unitList);
					}
					chapterList.add(chapterVo);
				}
				form.setCourseChapterList(chapterList);
			}

			if (course.getTags() != null) {
				List<Integer> categories = new ArrayList<>();
				List<TagVo> tagVos = new ArrayList<>();

				for (Tag tag : course.getTags()) {
					if (StringUtils.isEmpty(tag.getTagLevel())) {
						categories.add(tag.getCategory() != null ? tag.getCategory().getPkCategory() : null);
					} else {
						TagVo vo = new TagVo();
						vo.setCode(tag.getTag());
						vo.setLevel(tag.getTagLevel());
						vo.setName(tag.getTagName());
						tagVos.add(vo);
					}
				}

				Integer[] array = new Integer[categories.size()];
				form.setCategories(categories.toArray(array));
				form.setTagsValue(new Gson().toJson(tagVos));
			}

		} catch (Exception e) {
			LOG.error("exception", e);
		}
		return form;
	}

	public void addChapter(CourseForm form) {
		if (form != null) {
			if (form.getCourseChapterList() == null) {
				form.setCourseChapterList(new ArrayList<CourseChapterVo>());
			}
			CourseChapterVo chapter = new CourseChapterVo();
			List<CourseUnitVo> courseUnitList = new ArrayList<>();
			CourseUnitVo unit = new CourseUnitVo();
			courseUnitList.add(unit);
			chapter.setCourseUnitList(courseUnitList);
			form.getCourseChapterList().add(chapter);
		}
	}

	public void removeChapter(CourseForm form) {
		if (form != null && form.getCourseChapterList() != null && form.getChapterNo() != null) {
			form.getCourseChapterList().remove(form.getChapterNo().intValue());
		}
	}

	public void addUnit(CourseForm form) {
		if (form != null && form.getCourseChapterList() != null && form.getChapterNo() != null) {
			try {
				CourseChapterVo chapter = form.getCourseChapterList().get(form.getChapterNo());
				if (chapter.getCourseUnitList() == null) {
					chapter.setCourseUnitList(new ArrayList<CourseUnitVo>());
				}
				chapter.getCourseUnitList().add(new CourseUnitVo());
			} catch (Exception e) {

			}
		}
	}

	public void removeUnit(CourseForm form) {
		if (form != null && form.getCourseChapterList() != null && form.getChapterNo() != null
				&& form.getUnitNo() != null) {
			try {
				CourseChapterVo chapter = form.getCourseChapterList().get(form.getChapterNo());
				if (chapter.getCourseUnitList() != null) {
					chapter.getCourseUnitList().remove(form.getUnitNo().intValue());
				}
			} catch (Exception e) {

			}
		}
	}

	public List<Teacher> findActiveTeacherList() {
		return teacherRepository.findActive();
	}

	public Page<Course> findActive(int pageNo, int pageSize) throws ServiceException {
		try {
			return courseRepository.findActive(new PageRequest(pageNo - 1, pageSize));
		} catch (Exception e) {
			LOG.error("exception", e);
			throw new ServiceException(msg.getMessage("query.course.error", null, null));
		}
	}

	public Page<Course> query(CourseForm form, int pageNo) throws ServiceException {
		try {
			Pageable pageable = new PageRequest(pageNo - 1, configService.getPageSize());
			return courseRepository.findAll(buildOperSpecification(form), pageable);
		} catch (Exception e) {
			LOG.error("exception", e);
			throw new ServiceException(msg.getMessage("query.course.error", null, null));
		}
	}

	@Transactional(readOnly = true)
	public Course findById(Integer id) {
		Course course = courseRepository.findOne(id);
		course.getTags().size();
		course.getCourseChapterList().size();
		return course;
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void remove(int id) throws ServiceException {
		String result = this.validateBeforeRemove(id);
		if (StringUtils.isNotEmpty(result)) {
			throw new ServiceException(result);
		}
		try {
			Course course = courseRepository.findOne(id);
			if (course != null) {
				// 刪除Tag
				if (course.getTags() != null) {
					for (Tag tag : course.getTags()) {
						tagRepository.delete(tag);
					}
				}
				fileStorageService.delFile(course.getImageOne());
				fileStorageService.delFile(course.getImageTwo());
			}
			courseRepository.delete(id);
		} catch (Exception e) {
			LOG.error("removeCourse", e);
			throw new ServiceException(msg.getMessage("op.del.error", null, null));
		}
		throw new MessageException(msg.getMessage("op.del.success", null, null));
	}

	private Specification<Course> buildOperSpecification(CourseForm form) {
		return (Root<Course> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {

			List<Predicate> predicates = new ArrayList<>();
			SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
			if (!StringUtils.isEmpty(form.getCourseName())) {
				Path<String> np = root.get("courseName");
				predicates.add(cb.like(np, "%" + form.getCourseName() + "%"));
			}
			if (!StringUtils.isEmpty(form.getMaterialNum())) {
				Path<String> np = root.get("materialNum");
				predicates.add(cb.like(np, "%" + form.getMaterialNum() + "%"));
			}
			if (!StringUtils.isEmpty(form.getActive())) {
				Path<String> np = root.get("active");
				predicates.add(cb.equal(np, form.getActive()));
			}

			if (!StringUtils.isEmpty(form.getPublishStartDate())) {
				Date publishStartDate = null;
				try {
					publishStartDate = formatter.parse(form.getPublishStartDate());
				} catch (ParseException e) {
				}
				Path<Date> np = root.get("publishSDate");
				predicates.add(cb.greaterThanOrEqualTo(np, publishStartDate));
			}

			if (!StringUtils.isEmpty(form.getPublishEndDate())) {
				Date publishEndDate = null;
				try {
					publishEndDate = formatter.parse(form.getPublishEndDate());
				} catch (ParseException e) {
				}
				Path<Date> np = root.get("publishEDate");
				predicates.add(cb.lessThanOrEqualTo(np, publishEndDate));
			}
			// By J on 3/26
			if (isAdmin()) {
				if (!StringUtils.isEmpty(form.getVendorName())) {
					Join<Course, Vendor> vendorJoin = root
							.join(root.getModel().getSingularAttribute("vendor", Vendor.class), JoinType.LEFT);
					predicates.add(
							cb.like(vendorJoin.get("vendorName").as(String.class), "%" + form.getVendorName() + "%"));
				}
			} else {
				// 非管理者只能查屬於自己的課程
				Join<Course, Vendor> vendorJoin = root
						.join(root.getModel().getSingularAttribute("vendor", Vendor.class), JoinType.LEFT);
				predicates.add(cb.equal(vendorJoin.get("uid").as(String.class), getCurrentUserId()));
			}
			if (!predicates.isEmpty()) {
				return cb.and(predicates.toArray(new Predicate[predicates.size()]));
			}
			return cb.conjunction();

		};
	}

	private String validateBeforeRemove(int courseId) {
		String result = "";
		// 已有商品資料，不得刪除
		List<Product> list = productRepository
				.findAll((Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
					return cb.and(cb.equal(root.get("course").get("pkCourse"), courseId));
				});
		if (list != null && !list.isEmpty()) {
			result = msg.getMessage("props.has.product", null, null);
		} else {
			List<UserCourse> userCourseList = userCourseRepository
					.findAll((Root<UserCourse> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
						return cb.and(cb.equal(root.get("course").get("pkCourse"), courseId));
					});
			if (userCourseList != null && !userCourseList.isEmpty()) {
				result = msg.getMessage("course.is.sold", null, null);
			}
		}
		return result;
	}

	public List<Vendor> getAllVendorList() {
		return vendorRepository.findAll();
	}

	public Vendor getVendor() {
		return vendorRepository.findOne(getCurrentUserId());
	}

	/**
	 * 上架用 By J
	 * 
	 */
	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public Page<Course> queryForProduct(CourseForm form, int pageNo) throws ServiceException {
		try {
			Pageable pageable = new PageRequest(pageNo - 1, configService.getPageSize());
			Page<Course> list = courseRepository.findAll(buildOperSpecification(form), pageable);
			if (list != null) {
				// System.err.println("result size:"+list.getSize()+" ; check
				// video...");
				for (Course c : list) {
					this.checkVideo(c);
					// this.checkOnMarket(c);
				}
			}
			return list;
		} catch (Exception e) {
			LOG.error("exception", e);
			throw new ServiceException(msg.getMessage("query.course.error", null, null));
		}
	}

	private void checkVideo(Course c) {
		List<CourseChapter> chapterList = c.getCourseChapterList();
		c.setHasVideo(false);
		if (chapterList != null) {
			for (CourseChapter chapter : chapterList) {
				List<CourseVideo> videoList = chapter.getCourseVideoList();
				if (videoList != null) {
					for (CourseVideo courseVideo : videoList) {
						Video v = courseVideo.getVideo();
						if (v != null) {
							String result = v.getResult();
							if (result != null && StringUtils.equalsIgnoreCase("1", result)) {
								c.setHasVideo(true);
							} else {
								c.setHasVideo(false);
								return;
							}
						}
					}
				}
			}
		}
	}

	private void checkOnMarket(Course c) {
		if (c != null) {
			List<Product> list = productRepository.findByPkCourseAfterSysdate(c.getPkCourse(),
					CommonUtils.getTodayWithoutTimes());
			if (list != null && list.size() > 0) {
				c.setOnMarket(true);
			} else {
				c.setOnMarket(false);
			}
		}
	}

	@Transactional(readOnly = true)
	public Course getCourseAndProgess(Integer courseId, String uid) {
		if (courseRepository.exists(courseId)) {
			Course course = courseRepository.findOne(courseId);
			course.getCourseChapterList().stream().forEach(chapter -> {
				chapter.getCourseVideoList().stream().forEach(video -> {
					// 先查是否有播放紀錄
					List<LearningProgress> result = learningProgressRepository
							.findByFkCourseVideoAndUserId(video.getPkCourseVideo(), uid);
					if (!result.isEmpty()) {
						LearningProgress learningProgress = result.stream().findFirst().get();
						video.setVideoDuration(learningProgress.getVideoDuration());
						video.setLearningDuration(learningProgress.getLearningDuration());
						video.setLastWatchDt(learningProgress.getLastWatchDt() == null ? learningProgress.getCreateDt()
								: learningProgress.getLastWatchDt());
					}
				});
			});
			return course;
		}
		return null;
	}
}
