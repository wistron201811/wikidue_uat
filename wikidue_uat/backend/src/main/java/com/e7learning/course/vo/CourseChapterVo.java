package com.e7learning.course.vo;

import java.io.Serializable;
import java.util.List;

public class CourseChapterVo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;

	private Integer no;
	
	private List<CourseUnitVo> courseUnitList;
	
	private Integer courseChapterId;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNo() {
		return no;
	}

	public void setNo(Integer no) {
		this.no = no;
	}

	public List<CourseUnitVo> getCourseUnitList() {
		return courseUnitList;
	}

	public void setCourseUnitList(List<CourseUnitVo> courseUnitList) {
		this.courseUnitList = courseUnitList;
	}

	public Integer getCourseChapterId() {
		return courseChapterId;
	}

	public void setCourseChapterId(Integer courseChapterId) {
		this.courseChapterId = courseChapterId;
	}
	
	
}
