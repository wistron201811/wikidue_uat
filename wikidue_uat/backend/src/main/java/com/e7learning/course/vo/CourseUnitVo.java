package com.e7learning.course.vo;

import java.io.Serializable;

public class CourseUnitVo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String name;

	private Integer no;
	
	private Integer courseUnitId;
	
	private boolean free;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNo() {
		return no;
	}

	public void setNo(Integer no) {
		this.no = no;
	}

	public Integer getCourseUnitId() {
		return courseUnitId;
	}

	public void setCourseUnitId(Integer courseUnitId) {
		this.courseUnitId = courseUnitId;
	}

	public boolean isFree() {
		return free;
	}

	public void setFree(boolean free) {
		this.free = free;
	}

}
