package com.e7learning.course.vo;

import java.io.Serializable;

public class CourseFileVo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String courseFileId;

	private String courseId;

	private String fileId;
	
	private String fileName;

	public String getCourseFileId() {
		return courseFileId;
	}

	public void setCourseFileId(String courseFileId) {
		this.courseFileId = courseFileId;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	

}
