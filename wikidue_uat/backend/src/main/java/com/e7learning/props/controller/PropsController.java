package com.e7learning.props.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.e7learning.api.domain.param.CategoryQueryParam;
import com.e7learning.category.service.CategoryService;
import com.e7learning.common.controller.BaseController;
import com.e7learning.common.exception.RestException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.FileStorageService;
import com.e7learning.props.form.PropsForm;
import com.e7learning.props.service.PropsService;
import com.e7learning.props.validation.PropsFormValidation;
import com.e7learning.repository.model.Props;
import com.e7learning.repository.model.Vendor;

/**
 * @author J
 *
 */
@Controller
@RequestMapping(value = "/props")
@SessionAttributes(PropsController.FORM_BEAN_NAME)
public class PropsController extends BaseController {

	private static final Logger log = Logger.getLogger(PropsController.class);

	public static final String FORM_BEAN_NAME = "propsForm";
	
	private static final String QUERY_PAGE = "queryPropsPage";

	private static final String ADD_PAGE = "addPropsPage";
	
	private static final String EDIT_PAGE = "editPropsPage";
	
	private static final String QUERY_PRODUCT_PAGE ="queryPropsPageForProduct";
	
	@Autowired
	private PropsFormValidation propsFormValidation;
	@Autowired
	private PropsService propsService ;
	@Autowired
	private FileStorageService fileStorageService;
	@Autowired
	private CategoryService categoryService;
	
	@InitBinder(PropsController.FORM_BEAN_NAME)
	protected void initBinder(WebDataBinder binder, HttpSession session) {
		binder.setValidator(propsFormValidation);
	}
	
	@RequestMapping(value = "/query", method = { RequestMethod.GET })
	public String goQuery(Model model) {
		PropsForm form = new PropsForm();
		try {
			form.setResult(propsService.queryProps(form, 1));
		} catch (ServiceException e) {
			
		}
		model.addAttribute(FORM_BEAN_NAME, form);
		return QUERY_PAGE;
	}

	@RequestMapping(value = "/query", params = "method=query")
	public String query(HttpServletRequest request, PropsForm form, Model model) {
		String pageName = new ParamEncoder("list").encodeParameterName(TableTagParameters.PARAMETER_PAGE);
		int nextPage = getPage(request.getParameter(pageName));
		try {
			form.setResult(propsService.queryProps(form, nextPage));
		} catch (ServiceException e) {
			
		}
		return QUERY_PAGE;
	}
	
	@RequestMapping(value = "/add", method = { RequestMethod.GET })
	public String goAdd(Model model) {
		PropsForm propsForm = new PropsForm();
		propsForm.setVendorList(propsService.getAllVendorList());
		if(!propsService.isAdmin()) {
			Vendor vendor = propsService.getVendor();
			if(vendor!=null) {
				propsForm.setVendorUid(vendor.getUid());
				propsForm.setVendorName(vendor.getVendorName());
			}
		}
		try {
			CategoryQueryParam cp = new CategoryQueryParam();
			cp.setCategoryType("PROPS");
			propsForm.setCategoryList(categoryService.queryCategory(cp));
		} catch (RestException e) {
			e.printStackTrace();
		}
		model.addAttribute(FORM_BEAN_NAME, propsForm);
		return ADD_PAGE;
	}

	@RequestMapping(params = "method=save", value = "/add", method = { RequestMethod.POST })
	public String add(@Validated PropsForm form, BindingResult result) {
		if (result.hasErrors()) {
			form.setVendorList(propsService.getAllVendorList());
			return ADD_PAGE;
		}
		try {
			propsService.addProps(form);
			return "redirect:/props/query?method=query";
		} catch (ServiceException e) {
			
		}
		form.setVendorList(propsService.getAllVendorList());
		return ADD_PAGE;
	}
	
	@RequestMapping(params = "method=remove", value = "/remove", method = { RequestMethod.POST })
	public String remove(PropsForm form, Model model) {
		if(form.getPkProps() > 0) {
			Props props = propsService.findByPkProps(form.getPkProps());
			if(props != null) {
				try {
					propsService.remove(props.getPkProps());
					return "redirect:/props/query?method=query";
				} catch (ServiceException e) {
					// 避免display tag 查詢request發生405錯誤
					form.setResult(null);
					model.addAttribute(FORM_BEAN_NAME, form);
				}
			}
		}
		return QUERY_PAGE;
	}
	
	@RequestMapping(value = "/modify", method = { RequestMethod.POST })
	public String goModify(Model model, PropsForm form) {
		if(form.getPkProps() > 0) {
			form.setVendorList(propsService.getAllVendorList());
			Props props = propsService.findByPkProps(form.getPkProps());
			try {
				BeanUtils.copyProperties(form, props);
				form.setPhotoOneId(fileStorageService.getFileStorage(props.getImageOneId()).orElse(null));
				form.setPhotoTwoId(fileStorageService.getFileStorage(props.getImageTwoId()).orElse(null));
				form.setPhotoThirdId(fileStorageService.getFileStorage(props.getImageThirdId()).orElse(null));
				form.setPhotoFourthId(fileStorageService.getFileStorage(props.getImageFourthId()).orElse(null));
				form.setVendorUid(props.getVendor().getUid());
				form.setVendorName(props.getVendor().getVendorName());
				// 查詢分類
				CategoryQueryParam cp = new CategoryQueryParam();
				cp.setCategoryType("PROPS");
				form.setCategoryList(categoryService.queryCategory(cp));
				propsService.setPropsForm(props, form);
			} catch (Exception e) {
				e.printStackTrace();
			}
			model.addAttribute(FORM_BEAN_NAME, form);
		}
		return EDIT_PAGE;
	}
	
	@RequestMapping(params = "method=modify", value = "/modify", method = { RequestMethod.POST })
	public String modify(@Validated PropsForm form, BindingResult result) {
		if (result.hasErrors()) {
			Props props = propsService.findByPkProps(form.getPkProps());
			if(props != null) {
				form.setPhotoOneId(fileStorageService.getFileStorage(props.getImageOneId()).orElse(null));
				form.setPhotoTwoId(fileStorageService.getFileStorage(props.getImageTwoId()).orElse(null));
				form.setPhotoThirdId(fileStorageService.getFileStorage(props.getImageThirdId()).orElse(null));
				form.setPhotoFourthId(fileStorageService.getFileStorage(props.getImageFourthId()).orElse(null));
			}
			return EDIT_PAGE;
		}
		try {
			propsService.saveProps(form);
			return "redirect:/props/query?method=query";
		} catch (ServiceException e) {
			
		}
		return EDIT_PAGE;
	}
	
	@RequestMapping(value = "/queryProps")
	public String queryProps(HttpServletRequest request, PropsForm form, Model model) {
		String pageName = new ParamEncoder("list").encodeParameterName(TableTagParameters.PARAMETER_PAGE);
		int nextPage = getPage(request.getParameter(pageName));
		try {
			form.setResult(propsService.queryProps(form, nextPage));
		} catch (ServiceException e) {
			
		}
		return QUERY_PRODUCT_PAGE;
	}
	
}
