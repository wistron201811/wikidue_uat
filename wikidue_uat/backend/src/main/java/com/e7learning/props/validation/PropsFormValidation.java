package com.e7learning.props.validation;

import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

import com.e7learning.common.Constant;
import com.e7learning.common.ValidatorUtil;
import com.e7learning.common.utils.E7ValidationUtils;
import com.e7learning.props.form.PropsForm;
import com.e7learning.repository.CourseRepository;
import com.e7learning.repository.PropsRepository;
import com.e7learning.repository.model.Course;
import com.e7learning.repository.model.Props;

@Component
public class PropsFormValidation implements Validator {

	@Autowired
	private PropsRepository propsRepository;

	@Autowired
	private CourseRepository courseRepository;

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(PropsForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		PropsForm form = (PropsForm) target;

		if (StringUtils.equalsIgnoreCase("save", form.getMethod())
				|| StringUtils.equalsIgnoreCase("modify", form.getMethod())) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "propsName", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "materialNum", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "quantity", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "vendorUid", "validate.empty.message");
			// ValidationUtils.rejectIfEmptyOrWhitespace(errors, "publishSDate",
			// "validate.empty.message");
			// ValidationUtils.rejectIfEmptyOrWhitespace(errors, "publishEDate",
			// "validate.empty.message");
//			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "costPrice", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "discountPrice", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "propsSummary", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "propsIntroduction", "validate.empty.message");
		}
		if (StringUtils.equalsIgnoreCase("save", form.getMethod())) {
			MultipartFile image = form.getPhotoOneId();
			if (image == null || StringUtils.isEmpty(image.getOriginalFilename())) {
				errors.rejectValue("photoOneId", "validate.empty.message");
			}
		}
		// 驗證料號是否重覆
		if (StringUtils.isNotBlank(form.getMaterialNum())) {
			if (this.isDuplicateMaterialNum(form)) {
				errors.rejectValue("materialNum", "props.material.duplicate");
			}
		}
		if (StringUtils.isNotBlank(form.getQuantity())) {
			if (!ValidatorUtil.isNumber(form.getQuantity())) {
				errors.rejectValue("quantity", "validate.number.format");
			}else {
				if(Integer.valueOf(form.getQuantity()) < 0) {
					errors.rejectValue("quantity", "validate.quantity.negative");
				}
			}
		}
//		if (StringUtils.isNotBlank(form.getCostPrice())) {
//			if (!ValidatorUtil.isNumber(form.getCostPrice())) {
//				errors.rejectValue("costPrice", "validate.number.format");
//			}else {
//				if(Integer.valueOf(form.getCostPrice()) < 0) {
//					errors.rejectValue("costPrice", "validate.price.negative");
//				}
//			}
//		}
		if (StringUtils.isNotBlank(form.getPrice())) {
			if (!ValidatorUtil.isNumber(form.getPrice())) {
				errors.rejectValue("price", "validate.number.format");
			}else {
				if(Integer.valueOf(form.getPrice()) < 0) {
					errors.rejectValue("price", "validate.price.negative");
				}
			}
		}
		if (StringUtils.isNotBlank(form.getDiscountPrice())) {
			if (!ValidatorUtil.isNumber(form.getDiscountPrice())) {
				errors.rejectValue("discountPrice", "validate.number.format");
			}else {
				if(Integer.valueOf(form.getDiscountPrice()) < 0) {
					errors.rejectValue("discountPrice", "validate.price.negative");
				}
			}
		}

		// 驗證圖片一
		this.validateImage("photoOneId", form.getPhotoOneId(), errors);

		// 驗證圖片二
		this.validateImage("photoTwoId", form.getPhotoTwoId(), errors);

		// 驗證圖片三
		this.validateImage("photoThirdId", form.getPhotoThirdId(), errors);

		// 驗證圖片四
		this.validateImage("photoFourthId", form.getPhotoFourthId(), errors);

		// 驗證起迄日
		// if(form.getPublishEDate() != null && form.getPublishSDate() !=null) {
		// if(!form.getPublishEDate().after(form.getPublishSDate())) {
		// errors.rejectValue("publishEDate", "props.publish.date.error");
		// }
		// }

		// 驗證原價、優惠價
		if (StringUtils.isNotBlank(form.getDiscountPrice()) && StringUtils.isNotBlank(form.getPrice())
				&& ValidatorUtil.isNumber(form.getPrice()) && ValidatorUtil.isNumber(form.getDiscountPrice())) {
			if (Integer.parseInt(form.getDiscountPrice()) > Integer.parseInt(form.getPrice())) {
				errors.rejectValue("discountPrice", "props.price.error");
			}
		}

		if (form.getTagsValue().length() == 2 && form.getCategories().length == 0) {
			errors.rejectValue("categories", "props.tag.empty");
		}

		if (errors.hasErrors()) {
			return;
		}
	}

	private void validateImage(String field, MultipartFile image, Errors errors) {
		if (image != null && image.getSize() > 0) {
			if (image.getSize() > Constant.UPLOAD_FILE_SIZE) {
				errors.rejectValue(field, "validate.upload.size", new Object[] { Constant.UPLOAD_FILE_SIZE_MB },
						"system.error");
			} else {
				if (!E7ValidationUtils.isImageFile(image)) {
					errors.rejectValue(field, "image.type.error");
				}
			}
		}
	}

	private boolean isDuplicateMaterialNum(PropsForm form) {
		boolean result = false;
		if (form.getPkProps() != 0) {
			List<Props> propsList = propsRepository.findByPkPropsAndMaterialNum(form.getPkProps(),
					form.getMaterialNum());
			if (propsList != null && !propsList.isEmpty()) {
				return true;
			}
		} else {
			List<Props> propsList = propsRepository.findByMaterialNum(form.getMaterialNum());
			if (propsList != null && !propsList.isEmpty()) {
				return true;
			}
		}
		List<Course> coursesList = courseRepository.findByMaterialNum(form.getMaterialNum());
		if (coursesList != null && !coursesList.isEmpty()) {
			return true;
		}
		return result;
	}
}