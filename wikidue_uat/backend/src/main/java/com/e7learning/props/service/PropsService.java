package com.e7learning.props.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.common.Constant;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.service.FileStorageService;
import com.e7learning.common.vo.TagVo;
import com.e7learning.props.form.PropsForm;
import com.e7learning.repository.ProductRepository;
import com.e7learning.repository.PropsRepository;
import com.e7learning.repository.TagRepository;
import com.e7learning.repository.VendorRepository;
import com.e7learning.repository.model.Category;
import com.e7learning.repository.model.Product;
import com.e7learning.repository.model.Props;
import com.e7learning.repository.model.Tag;
import com.e7learning.repository.model.Vendor;
import com.google.gson.Gson;

/**
 * @author J
 *
 */
@Service
public class PropsService extends BaseService {

	private static final Logger log = Logger.getLogger(PropsService.class);

	@Autowired
	private PropsRepository propsRepository;

	@Autowired
	private FileStorageService fileStorageService;

	@Autowired
	private VendorRepository vendorRepository;

	@Autowired
	private ConfigService configService;
	
	@Autowired
	private TagRepository tagRepository;
	
	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private MessageSource resources;

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void addProps(PropsForm form) throws ServiceException {
		try {
			Props props = new Props();
			Date now = new Date();
			BeanUtils.copyProperties(props, form);
			if (form.getPhotoOneId() != null && !form.getPhotoOneId().isEmpty()) {
				props.setImageOneId(fileStorageService.saveFile(form.getPhotoOneId()).getPkFile());
			}
			if (form.getPhotoTwoId() != null && !form.getPhotoTwoId().isEmpty()) {
				props.setImageTwoId(fileStorageService.saveFile(form.getPhotoTwoId()).getPkFile());
			}
			if (form.getPhotoThirdId() != null && !form.getPhotoThirdId().isEmpty()) {
				props.setImageThirdId(fileStorageService.saveFile(form.getPhotoThirdId()).getPkFile());
			}
			if (form.getPhotoFourthId() != null && !form.getPhotoFourthId().isEmpty()) {
				props.setImageFourthId(fileStorageService.saveFile(form.getPhotoFourthId()).getPkFile());
			}
			props.setQuantity(Integer.valueOf(form.getQuantity()));
//			props.setCostPrice(Integer.valueOf(form.getCostPrice()));
			props.setPrice(Integer.valueOf(form.getPrice()));
			props.setDiscountPrice(Integer.valueOf(form.getDiscountPrice()));
			props.setVendor(vendorRepository.findOne(form.getVendorUid()));
			props.setUpdateDt(now);
			props.setCreateDt(now);
			props.setCreateId(getCurrentUserId());
			props.setLastModifiedId(getCurrentUserId());
			// 重塞html
			props.setPropsIntroduction(fileStorageService.saveHtmlElementImage(form.getPropsIntroduction()).getBody());
			props=propsRepository.save(props);
			
			// 設定Tag
			if (form.getTags() != null) {
				for (TagVo vo : form.getTags()) {
					Tag tag = new Tag();
					tag.setActive(Constant.TRUE);
					tag.setProps(props);
					tag.setTag(vo.getCode());
					tag.setTagLevel(vo.getLevel());
					tag.setTagName(vo.getName());
					tag.setCreateDt(now);
					tag.setCreateId(getCurrentUserId());
					tag.setLastModifiedId(getCurrentUserId());
					tagRepository.save(tag);
				}
			}
			if (form.getCategories() != null && form.getCategories().length > 0) {
				for (Integer c : form.getCategories()) {
					Tag tag = new Tag();
					tag.setActive(Constant.TRUE);
					tag.setProps(props);
					tag.setTag(StringUtils.EMPTY);
					tag.setTagLevel(StringUtils.EMPTY);
					tag.setTagName(StringUtils.EMPTY);
					Category category = new Category();
					category.setPkCategory(c);
					tag.setCategory(category);
					tag.setCreateDt(now);
					tag.setCreateId(getCurrentUserId());
					tag.setLastModifiedId(getCurrentUserId());
					tagRepository.save(tag);
				}
			}
		} catch (Exception e) {
			log.error("addProps", e);
			throw new ServiceException(resources.getMessage("op.add.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.add.success", null, null));
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void saveProps(PropsForm form) throws ServiceException {
		try {
			Date now = new Date();
			Props props = findByPkProps(form.getPkProps());
			if (props != null) {
				// 刪除Tag
				if (props.getTagList() != null) {
					for (Tag tag : props.getTagList()) {
						tagRepository.delete(tag);
					}
				}
				BeanUtils.copyProperties(props, form);
				props.setUpdateDt(new Date());
				if (form.getPhotoOneId() != null && !form.getPhotoOneId().isEmpty()) {
					fileStorageService.delFile(props.getImageOneId());
					props.setImageOneId(fileStorageService.saveFile(form.getPhotoOneId()).getPkFile());
				}
				if (form.getPhotoTwoId() != null && !form.getPhotoTwoId().isEmpty()) {
					fileStorageService.delFile(props.getImageTwoId());
					props.setImageTwoId(fileStorageService.saveFile(form.getPhotoTwoId()).getPkFile());
				}
				if (form.getPhotoThirdId() != null && !form.getPhotoThirdId().isEmpty()) {
					fileStorageService.delFile(props.getImageThirdId());
					props.setImageThirdId(fileStorageService.saveFile(form.getPhotoThirdId()).getPkFile());
				}
				if (form.getPhotoFourthId() != null && !form.getPhotoFourthId().isEmpty()) {
					fileStorageService.delFile(props.getImageFourthId());
					props.setImageFourthId(fileStorageService.saveFile(form.getPhotoFourthId()).getPkFile());
				}
				props.setQuantity(Integer.valueOf(form.getQuantity()));
//				props.setCostPrice(Integer.valueOf(form.getCostPrice()));
				props.setPrice(Integer.valueOf(form.getPrice()));
				props.setDiscountPrice(Integer.valueOf(form.getDiscountPrice()));
				props.setUpdateDt(now);
				props.setLastModifiedId(getCurrentUserId());
				// 重塞html
				props.setPropsIntroduction(fileStorageService
						.updateHtmlElementImage(props.getPropsIntroduction(), form.getPropsIntroduction()).getBody());
				
				// 設定Tag
				if (form.getTags() != null) {
					for (TagVo vo : form.getTags()) {
						Tag tag = new Tag();
						tag.setActive(Constant.TRUE);
						tag.setProps(props);
						tag.setTag(vo.getCode());
						tag.setTagLevel(vo.getLevel());
						tag.setTagName(vo.getName());
						tag.setCreateDt(now);
						tag.setCreateId(getCurrentUserId());
						tag.setLastModifiedId(getCurrentUserId());
						tagRepository.save(tag);
					}
				}
				if (form.getCategories() != null && form.getCategories().length > 0) {
					for (Integer c : form.getCategories()) {
						Tag tag = new Tag();
						tag.setActive(Constant.TRUE);
						tag.setProps(props);
						tag.setTag(StringUtils.EMPTY);
						tag.setTagLevel(StringUtils.EMPTY);
						tag.setTagName(StringUtils.EMPTY);
						Category category = new Category();
						category.setPkCategory(c);
						tag.setCategory(category);
						tag.setCreateDt(now);
						tag.setCreateId(getCurrentUserId());
						tag.setLastModifiedId(getCurrentUserId());
						tagRepository.save(tag);
					}
				}
			}
		} catch (Exception e) {
			log.error("editProps", e);
			throw new ServiceException(resources.getMessage("op.modify.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.modify.success", null, null));
	}

	public Page<Props> queryProps(PropsForm form, Integer page) throws ServiceException {
		try {
			Sort sort = new Sort(Direction.ASC, "materialNum");
			Pageable pageable = new PageRequest(page - 1, configService.getPageSize(), sort);
			Page<Props> result = propsRepository.findAll(buildOperSpecification(form), pageable);
//			for(Props p:result) {
//				List<Product> list = productRepository.findByPkPropsAfterSysdate(p.getPkProps(), this.getToday());
//				if(list !=null && list.size() > 0) {
//					p.setOnMarket(true);
//				}else {
//					p.setOnMarket(false);
//				}
//			}
			return result;
		} catch (Exception e) {
			log.error("queryProps", e);
			throw new ServiceException(resources.getMessage("op.query.error", null, null));
		}
	}

	@Transactional(readOnly = true)
	public Props findByPkProps(int pkProps) {
		Props props =  propsRepository.findOne(pkProps);
		if(props !=null) {
			props.getTagList().size();
		}
		return props;
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void remove(int id) throws ServiceException {
		String result = this.validateBeforeRemove(id);
		if (StringUtils.isNotEmpty(result)) {
			throw new ServiceException(result);
		}
		try {
			Props props = propsRepository.getOne(id);
			if(props !=null) {
				// 刪除Tag
				if (props.getTagList() != null) {
					for (Tag tag : props.getTagList()) {
						tagRepository.delete(tag);
					}
				}
				propsRepository.delete(props);
				if(StringUtils.isNotBlank(props.getImageOneId())) {
					fileStorageService.delFile(props.getImageOneId());
				}
				if(StringUtils.isNotBlank(props.getImageTwoId())) {
					fileStorageService.delFile(props.getImageTwoId());
				}
				if(StringUtils.isNotBlank(props.getImageThirdId())) {
					fileStorageService.delFile(props.getImageThirdId());
				}
				if(StringUtils.isNotBlank(props.getImageFourthId())) {
					fileStorageService.delFile(props.getImageFourthId());
				}
			}
		} catch (Exception e) {
			log.error("removeProps", e);
			throw new ServiceException(resources.getMessage("op.del.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.del.success", null, null));
	}

	private String validateBeforeRemove(int pkProps) {
		String result = "";
		// 已有商品資料，不得刪除
		List<Product> list=productRepository.getByPkProps(pkProps);
		if(list !=null && list.size() > 0) {
			result = resources.getMessage("props.has.product", null, null);
		}
		return result;
	}

	public List<Vendor> getAllVendorList(){
		return vendorRepository.findAll();
	}
	
	public Vendor getVendor() {
		return vendorRepository.findOne(getCurrentUserId());
	}
	
	public void setPropsForm(Props props,PropsForm form) {
		if(props !=null) {
			if (props.getTagList() != null) {
				List<Integer> categories = new ArrayList<>();
				List<TagVo> tagVos = new ArrayList<>();

				for (Tag tag : props.getTagList()) {
					if (StringUtils.isEmpty(tag.getTagLevel())) {
						categories.add(tag.getCategory() != null ? tag.getCategory().getPkCategory() : null);
					} else {
						TagVo vo = new TagVo();
						vo.setCode(tag.getTag());
						vo.setLevel(tag.getTagLevel());
						vo.setName(tag.getTagName());
						tagVos.add(vo);
					}
				}

				Integer[] array = new Integer[categories.size()];
				form.setCategories(categories.toArray(array));
				form.setTagsValue(new Gson().toJson(tagVos));
			}
		}
	}
	
	private Specification<Props> buildOperSpecification(PropsForm form) {
		return new Specification<Props>() {
			@Override
			public Predicate toPredicate(Root<Props> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

				List<Predicate> predicates = new ArrayList<>();
				if(isAdmin()) {
					if (!StringUtils.isEmpty(form.getVendorName())) {
	                    Join<Props,Vendor> vendorJoin = root.join(root.getModel().getSingularAttribute("vendor",Vendor.class),JoinType.LEFT);
	                    predicates.add(cb.like(vendorJoin.get("vendorName").as(String.class), "%" + form.getVendorName() + "%"));
					}
				}else {
					// 非管理者只能查屬於自己的教具
					Join<Props,Vendor> vendorJoin = root.join(root.getModel().getSingularAttribute("vendor",Vendor.class),JoinType.LEFT);
                    predicates.add(cb.equal(vendorJoin.get("uid").as(String.class), getCurrentUserId()));
				}
				if (!StringUtils.isEmpty(form.getPropsName())) {
					Path<String> np = root.get("propsName");
					predicates.add(cb.like(np, "%" + form.getPropsName() + "%"));
				}
				if (!StringUtils.isEmpty(form.getMaterialNum())) {
					Path<String> np = root.get("materialNum");
					predicates.add(cb.like(np, "%" + form.getMaterialNum() + "%"));
				}
				if (!StringUtils.isEmpty(form.getActive())) {
					Path<String> np = root.get("active");
					predicates.add(cb.equal(np, form.getActive()));
				}
				if (!predicates.isEmpty()) {
					return cb.and(predicates.toArray(new Predicate[predicates.size()]));
				}
				return cb.conjunction();
			}
		};
	}
}
