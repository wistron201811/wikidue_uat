package com.e7learning.recommend.vo;

import java.util.Date;

import org.springframework.data.domain.Pageable;

public class RecommendSearchVo {

	private String title;

	private String active;

	private Date createSDate;
	
	private Date createEDate;

	private Pageable pageable;

	private String createId;

	public String getTitle() {
		return title;
	}

	public RecommendSearchVo setTitle(String title) {
		this.title = title;
		return this;
	}

	public String getActive() {
		return active;
	}

	public RecommendSearchVo setActive(String active) {
		this.active = active;
		return this;
	}


	public Date getCreateSDate() {
		return createSDate;
	}

	public RecommendSearchVo setCreateSDate(Date createSDate) {
		this.createSDate = createSDate;
		return this;
	}

	public Date getCreateEDate() {
		return createEDate;
	}

	public RecommendSearchVo setCreateEDate(Date createEDate) {
		this.createEDate = createEDate;
		return this;
	}

	public Pageable getPageable() {
		return pageable;
	}

	public RecommendSearchVo setPageable(Pageable pageable) {
		this.pageable = pageable;
		return this;
	}

	public String getCreateId() {
		return createId;
	}

	public RecommendSearchVo setCreateId(String createId) {
		this.createId = createId;
		return this;
	}

}
