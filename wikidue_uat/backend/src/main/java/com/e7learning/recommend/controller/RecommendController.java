package com.e7learning.recommend.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.e7learning.common.controller.BaseController;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.FileStorageService;
import com.e7learning.recommend.form.RecommendForm;
import com.e7learning.recommend.service.RecommendService;
import com.e7learning.recommend.validation.RecommendFormValidation;
import com.e7learning.repository.model.Props;
import com.e7learning.repository.model.Recommend;

@Controller
@RequestMapping(value = "/recommend")
@SessionAttributes(RecommendController.FORM_BEAN_NAME)
public class RecommendController extends BaseController {

	private static final Logger log = Logger.getLogger(RecommendController.class);

	public static final String FORM_BEAN_NAME = "recommendForm";

	private static final String QUERY_PAGE = "queryRecommendPage";

	private static final String ADD_PAGE = "addRecommendPage";

	private static final String EDIT_PAGE = "editRecommendPage";
	
	private static final String RIGHT_PAGE = "recommendRightBlockPage";

	private static final String TEXT = "text";

	@Autowired
	private RecommendFormValidation recommendFormValidation;

	@Autowired
	private RecommendService recommendService;

	@Autowired
	private FileStorageService fileStorageService;

	@InitBinder(RecommendController.FORM_BEAN_NAME)
	protected void initBinder(WebDataBinder binder, HttpSession session) {
		binder.setValidator(recommendFormValidation);
	}

	@RequestMapping(value = "/query", method = { RequestMethod.GET })
	public String goQuery(Model model) {
		RecommendForm form = new RecommendForm();
		try {
			form.setResult(recommendService.queryRecommend(form, 1));
		} catch (ServiceException e) {
		}
		model.addAttribute(FORM_BEAN_NAME, form);
		return QUERY_PAGE;
	}

	@RequestMapping(value = "/query", params = "method=query")
	public String query(HttpServletRequest request, RecommendForm form, Model model) {
		String pageName = new ParamEncoder("result").encodeParameterName(TableTagParameters.PARAMETER_PAGE);
		int nextPage = getPage(request.getParameter(pageName));
		try {
			form.setResult(recommendService.queryRecommend(form, nextPage));
		} catch (ServiceException e) {

		}
		return QUERY_PAGE;
	}

	@RequestMapping(value = "/add", method = { RequestMethod.GET })
	public String goAdd(Model model) {
		model.addAttribute(FORM_BEAN_NAME, new RecommendForm());
		return ADD_PAGE;
	}

	@RequestMapping(params = "method=save", value = "/add", method = { RequestMethod.POST })
	public String add(@Validated RecommendForm form, BindingResult result) {
		if (result.hasErrors()) {
			return ADD_PAGE;
		}
		try {
			recommendService.createRecommend(form);
			return "redirect:/recommend/query?method=query";
		} catch (ServiceException e) {

		}
		return ADD_PAGE;
	}

	@RequestMapping(params = "method=remove", value = "/remove", method = { RequestMethod.POST })
	public String remove(RecommendForm form, Model model) {
		try {
			recommendService.delRecommend(form.getPkRecommend());
			return "redirect:/recommend/query?method=query";
		} catch (ServiceException e) {
			// 避免display tag 查詢request發生405錯誤
			form.setResult(null);
			model.addAttribute(FORM_BEAN_NAME, form);
		}
		return QUERY_PAGE;
	}

	@RequestMapping(value = "/modify", method = { RequestMethod.POST })
	public String goModify(Model model, RecommendForm form) {
		try {
			form = recommendService.findRecommend(form.getPkRecommend());
			model.addAttribute(FORM_BEAN_NAME, form);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return EDIT_PAGE;
	}

	@RequestMapping(params = "method=modify", value = "/modify", method = { RequestMethod.POST })
	public String modify(@Validated RecommendForm form, BindingResult result) {
		if (result.hasErrors()) {
			Recommend recommend = recommendService.findByRecommendPk(form.getPkRecommend());
			if (recommend != null) {
				form.setImageId(fileStorageService.getFileStorage(recommend.getImageId()).orElse(null));
			}
			return EDIT_PAGE;
		}
		try {
			recommendService.updateRecommend(form);
			return "redirect:/recommend/query?method=query";
		} catch (ServiceException e) {

		}
		return EDIT_PAGE;
	}

	@RequestMapping(value = "/right", method = { RequestMethod.GET })
	public String goRightSetting(Model model) {
		try {
			model.addAttribute(TEXT, recommendService.getRecommendRightBlock());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return RIGHT_PAGE;
	}

	@RequestMapping(value = "/right", method = { RequestMethod.POST })
	public String modifyRightSettin(Model model, String text) {
		try {
			recommendService.updateRecommendRightBlock(text);
			return "redirect:/recommend/right";
		} catch (ServiceException e) {
			model.addAttribute(TEXT, text);
		}
		return RIGHT_PAGE;
	}
}
