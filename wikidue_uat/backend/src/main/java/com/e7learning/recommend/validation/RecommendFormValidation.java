package com.e7learning.recommend.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

import com.e7learning.common.Constant;
import com.e7learning.common.utils.E7ValidationUtils;
import com.e7learning.recommend.form.RecommendForm;

@Component
public class RecommendFormValidation implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(RecommendForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		RecommendForm form = (RecommendForm) target;
		if (StringUtils.equalsIgnoreCase("save", form.getMethod())
				|| StringUtils.equalsIgnoreCase("modify", form.getMethod())) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "content", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "author", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "publishEDate", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "publishSDate", "validate.empty.message");

		}
		// 驗證圖片
		this.validateImage("imageId", form, errors);

		// 驗證起迄日
		if (form.getPublishEDate() != null && form.getPublishSDate() != null) {
			if (form.getPublishEDate().compareTo(form.getPublishSDate()) < 0) {
				errors.rejectValue("publishEDate", "product.publish.date.error");
			} 
		}

		if (errors.hasErrors()) {
			return;
		}
	}

	private void validateImage(String field, RecommendForm form , Errors errors) {
		MultipartFile image = form.getImageId();
		if (StringUtils.equalsIgnoreCase("save", form.getMethod())) {
			if(image == null || StringUtils.isEmpty(image.getOriginalFilename())) {
				errors.rejectValue(field, "validate.empty.message");
			}
		}
		if (image != null && image.getSize() > 0) {
			if (image.getSize() > Constant.UPLOAD_FILE_SIZE) {
				errors.rejectValue(field, "validate.upload.size", new Object[] { Constant.UPLOAD_FILE_SIZE_MB },
						"system.error");
			} else {
				if (!E7ValidationUtils.isImageFile(image)) {
					errors.rejectValue(field, "image.type.error");
				}
			}
		}
	}
}