package com.e7learning.recommend.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.ad.service.AdService;
import com.e7learning.common.Constant;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.service.FileStorageService;
import com.e7learning.recommend.form.RecommendForm;
import com.e7learning.recommend.vo.RecommendSearchVo;
import com.e7learning.repository.RecommendRepository;
import com.e7learning.repository.SysConfigRepository;
import com.e7learning.repository.model.Recommend;
import com.e7learning.repository.model.SysConfig;

@Service
public class RecommendService extends BaseService {

	private static final Logger log = Logger.getLogger(AdService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private ConfigService configService;

	@Autowired
	private FileStorageService fileStorageService;

	@Autowired
	private RecommendRepository recommendRepository;

	@Autowired
	private SysConfigRepository sysConfigRepository;

	public Page<Recommend> queryRecommend(RecommendForm form, Integer page) throws ServiceException {
		try {
			form.getSearch().setPageable(
					new PageRequest(page - 1, configService.getPageSize(), new Sort(Direction.DESC, "createDt")));
			if (!isAdmin())
				form.getSearch().setCreateId(getCurrentUserId());
			ConditionSpec conditionSpec = new ConditionSpec(form.getSearch());
			Page<Recommend> result = recommendRepository.findAll(conditionSpec, conditionSpec.searchBean.getPageable());
			log.info(result.getTotalElements());
			return result;
		} catch (Exception e) {
			log.error("queryRecommend", e);
			throw new ServiceException(resources.getMessage("query.recommend.error", null, null));
		}
	}

	public void createRecommend(RecommendForm form) throws ServiceException {
		try {
			Recommend recommend = new Recommend();
			recommend.setActive(form.getActive());
			recommend.setCreateDt(new Date());
			recommend.setCreateId(getCurrentUserId());

			recommend.setTitle(form.getTitle());
			recommend.setContent(fileStorageService.saveHtmlElementImage(form.getContent()).getBody());
			recommend.setAuthor(form.getAuthor());
			recommend.setPublishSDate(form.getPublishSDate());
			recommend.setPublishEDate(form.getPublishEDate());
			if (form.getImageId() != null && !form.getImageId().isEmpty()) {
				recommend.setImageId(fileStorageService.saveFile(form.getImageId()).getPkFile());
			}
			recommendRepository.save(recommend);
		} catch (Exception e) {
			log.error("createRecommend", e);
			throw new ServiceException(resources.getMessage("create.recommend.error", null, null));
		}

		throw new MessageException(resources.getMessage("create.recommend.success", null, null));
	}

	public void delRecommend(String pkRecommend) throws ServiceException {
		try {
			if (StringUtils.isNotBlank(pkRecommend) && recommendRepository.exists(pkRecommend)) {
				Recommend recommend = recommendRepository.findOne(pkRecommend);
				// if (!StringUtils.equals(recommend.getCreateId(), getCurrentUserId())) {
				// throw new ServiceException(resources.getMessage("delete.recommend.error",
				// null, null));
				// }
				recommendRepository.delete(recommend);
			}
		} catch (Exception e) {
			log.error("delBannerl", e);
			throw new ServiceException(resources.getMessage("delete.recommend.error", null, null));
		}

		throw new MessageException(resources.getMessage("delete.banner.success", null, null));
	}

	public RecommendForm findRecommend(String pkRecommend) throws ServiceException {

		if (StringUtils.isNotBlank(pkRecommend)) {
			if (recommendRepository.exists(pkRecommend)) {
				Recommend recommend = recommendRepository.findOne(pkRecommend);
				RecommendForm form = new RecommendForm();
				form.setActive(recommend.getActive());
				form.setContent(recommend.getContent());
				form.setPkRecommend(pkRecommend);
				form.setTitle(recommend.getTitle());
				form.setAuthor(recommend.getAuthor());
				form.setPublishSDate(recommend.getPublishSDate());
				form.setPublishEDate(recommend.getPublishEDate());
				form.setImageId(fileStorageService.getFileStorage(recommend.getImageId()).orElse(null));

				return form;
			}
		}
		throw new ServiceException(resources.getMessage("query.recommend.error", null, null));

	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public RecommendForm updateRecommend(RecommendForm form) throws ServiceException {
		try {
			if (StringUtils.isNotBlank(form.getPkRecommend())) {
				if (recommendRepository.exists(form.getPkRecommend())) {
					Recommend recommend = recommendRepository.findOne(form.getPkRecommend());
					if (StringUtils.isNotBlank(form.getTitle())) {
						recommend.setTitle(form.getTitle());
					}
					if (StringUtils.isNotBlank(form.getContent())) {
						recommend.setContent(fileStorageService
								.updateHtmlElementImage(recommend.getContent(), form.getContent()).getBody());
					}
					if (StringUtils.isNotBlank(form.getActive())) {
						recommend.setActive(form.getActive());
					}
					if (StringUtils.isNotBlank(form.getAuthor())) {
						recommend.setAuthor(form.getAuthor());
					}
					if (form.getPublishSDate() != null) {
						recommend.setPublishSDate(form.getPublishSDate());
					}
					if (form.getPublishEDate() != null) {
						recommend.setPublishEDate(form.getPublishEDate());
					}
					if (form.getImageId() != null && !form.getImageId().isEmpty()) {
						fileStorageService.delFile(recommend.getImageId());
						recommend.setImageId(fileStorageService.saveFile(form.getImageId()).getPkFile());
					}
					recommend.setUpdateDt(new Date());
					recommend.setLastModifiedId(getCurrentUserId());
				}
			}
		} catch (Exception e) {
			log.error("updateBanner", e);
			throw new ServiceException(resources.getMessage("modify.recommend.error", null, null));
		}

		throw new MessageException(resources.getMessage("modify.recommend.success", null, null));
	}

	public Recommend findByRecommendPk(String recommendPk) {
		if (StringUtils.isNotBlank(recommendPk)) {
			return recommendRepository.findOne(recommendPk);
		} else {
			return null;
		}
	}

	private static class ConditionSpec implements Specification<Recommend> {

		private RecommendSearchVo searchBean;

		public ConditionSpec(RecommendSearchVo searchBean) {
			super();
			this.searchBean = searchBean;
		}

		@Override
		public Predicate toPredicate(Root<Recommend> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

			List<Predicate> predicates = new ArrayList<>();

			if (StringUtils.isNotBlank(searchBean.getTitle())) {
				Path<String> np = root.get("title");
				predicates.add(cb.like(np, "%" + searchBean.getTitle() + "%"));
			}

			if (searchBean.getCreateSDate() != null) {
				Path<Date> np = root.get("createDt");
				predicates.add(cb.greaterThanOrEqualTo(np, searchBean.getCreateSDate()));
			}

			if (searchBean.getCreateEDate() != null) {
				Path<Date> np = root.get("createDt");
				predicates.add(cb.lessThanOrEqualTo(np, searchBean.getCreateEDate()));
			}

			if (StringUtils.isNotBlank(searchBean.getActive())) {
				Path<String> np = root.get("active");
				predicates.add(cb.equal(np, searchBean.getActive()));
			}

			if (StringUtils.isNotBlank(searchBean.getCreateId())) {
				predicates.add(cb.equal(root.get("createId"), searchBean.getCreateId()));
			}

			if (!predicates.isEmpty()) {
				return cb.and(predicates.toArray(new Predicate[predicates.size()]));
			}
			return cb.conjunction();
		}

	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public String getRecommendRightBlock() throws ServiceException {
		try {
			if (sysConfigRepository.exists(Constant.RECOMMEND_RIGHT_BLOCK)) {
				return sysConfigRepository.findOne(Constant.RECOMMEND_RIGHT_BLOCK).getText();
			} else {
				SysConfig sysConfig = new SysConfig();
				sysConfig.setCode(Constant.RECOMMEND_RIGHT_BLOCK);
				sysConfig.setText(StringUtils.EMPTY);
				sysConfig.setActive(Constant.TRUE);
				sysConfigRepository.save(sysConfig);
				return sysConfig.getText();
			}
		} catch (Exception e) {
			log.error("getRecommendRightBlock", e);
			throw new ServiceException(resources.getMessage("query.recommend.error", null, null));
		}

	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void updateRecommendRightBlock(String text) throws ServiceException {
		try {
			if (sysConfigRepository.exists(Constant.RECOMMEND_RIGHT_BLOCK)) {
				SysConfig sysConfig = sysConfigRepository.findOne(Constant.RECOMMEND_RIGHT_BLOCK);
				sysConfig.setText(fileStorageService
						.updateHtmlElementImage(StringUtils.trimToEmpty(sysConfig.getText()), text).getBody());
				sysConfigRepository.save(sysConfig);
			} else
				throw new ServiceException(resources.getMessage("modify.recommend.error", null, null));
		} catch (Exception e) {
			log.error("getRecommendRightBlock", e);
			throw new ServiceException(resources.getMessage("modify.recommend.error", null, null));
		}
		throw new MessageException(resources.getMessage("modify.recommend.success", null, null));

	}
}
