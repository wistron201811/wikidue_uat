package com.e7learning.recommend.form;

import java.util.Date;

import javax.persistence.Column;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import com.e7learning.common.form.BaseForm;
import com.e7learning.recommend.vo.RecommendSearchVo;
import com.e7learning.repository.model.Recommend;

public class RecommendForm extends BaseForm<Recommend>{
	
	
	private RecommendSearchVo search = new RecommendSearchVo();

	private String pkRecommend;

	private String title;

	private String content;
	
	private String active;
	
	private String author;
	
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date publishEDate;

	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date publishSDate;
	
	/**
	 * 圖片
	 */
	private MultipartFile imageId;

	public String getPkRecommend() {
		return pkRecommend;
	}

	public void setPkRecommend(String pkRecommend) {
		this.pkRecommend = pkRecommend;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public RecommendSearchVo getSearch() {
		return search;
	}

	public void setSearch(RecommendSearchVo search) {
		this.search = search;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Date getPublishEDate() {
		return publishEDate;
	}

	public void setPublishEDate(Date publishEDate) {
		this.publishEDate = publishEDate;
	}

	public Date getPublishSDate() {
		return publishSDate;
	}

	public void setPublishSDate(Date publishSDate) {
		this.publishSDate = publishSDate;
	}

	public MultipartFile getImageId() {
		return imageId;
	}

	public void setImageId(MultipartFile imageId) {
		this.imageId = imageId;
	}


}
