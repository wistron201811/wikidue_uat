package com.e7learning.accounting.service;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.BuiltinFormats;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.accounting.form.AccountingForm;
import com.e7learning.accounting.vo.MonthlyReportHeaderVo;
import com.e7learning.accounting.vo.SubOrderDetailVo;
import com.e7learning.common.enums.OrderStatusEnum;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.utils.CommonUtils;
import com.e7learning.repository.SubOrderDetailRepository;
import com.e7learning.repository.VendorRepository;
import com.e7learning.repository.model.MainOrder;
import com.e7learning.repository.model.Payment;
import com.e7learning.repository.model.Product;
import com.e7learning.repository.model.SubOrder;
import com.e7learning.repository.model.SubOrderDetail;
import com.e7learning.repository.model.Vendor;

/**
 * @author J
 *
 */
@Service
public class AccountingService extends BaseService {

	private static final Logger log = Logger.getLogger(AccountingService.class);

	@Autowired
	private VendorRepository vendorRepository;

	@Autowired
	private SubOrderDetailRepository subOrderDetailRepository;

	@Autowired
	private ConfigService configService;

	@Autowired
	private MessageSource resources;

	public List<Vendor> getAllVendorList() {
		return vendorRepository.findAllOrderByTaxId();
	}

	public Vendor getVnedorByUid(String uid) {
		if (StringUtils.isEmpty(uid)) {
			return null;
		}
		return vendorRepository.findOne(uid);
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public List<SubOrderDetailVo> getMonthlyReportByVendor(AccountingForm form) {
		List<SubOrderDetailVo> voList = new ArrayList<SubOrderDetailVo>();
		form.setReporList(voList);
		Map<String, Date> dateMap = this.getReportDate(form.getYearMonth());
		int totalAmount = 0; // 銷售總額
		int sumRatioTotalAmount = 0; // 進貨總額
		// 課程顯示付款時間
		List<SubOrderDetail> detailList = subOrderDetailRepository.findMonthlyCourseReortByVendor(form.getUid(),
				dateMap.get("start"), dateMap.get("end"));
		Vendor vendor = vendorRepository.findOne(form.getUid());
		try {
			for (SubOrderDetail detail : detailList) {
				SubOrderDetailVo vo = this.toSubOrderDetailVoForMonthly(detail, OrderStatusEnum.P.name());
				voList.add(vo);
				if(vo != null && vo.getTotalAmount() != null) {
					totalAmount += vo.getTotalAmount().intValue();
				}
				if(vo != null && vo.getRatioAmount() != null) {
					sumRatioTotalAmount += vo.getRatioAmount().intValue();
				}
			}
		} catch (Exception e) {
//			e.printStackTrace();
			log.error("getMonthlyReportByVendor",e);
		}
		// 教具顯示出貨時間
		detailList = subOrderDetailRepository.findMonthlyPropsReortByVendor(form.getUid(), dateMap.get("start"),
				dateMap.get("end"));
		try {
			for (SubOrderDetail detail : detailList) {
				SubOrderDetailVo vo = this.toSubOrderDetailVoForMonthly(detail, OrderStatusEnum.D.name());
				voList.add(vo);
				if(vo != null && vo.getTotalAmount() != null) {
					totalAmount += vo.getTotalAmount().intValue();
				}
				if(vo != null && vo.getRatioAmount() != null) {
					sumRatioTotalAmount += vo.getRatioAmount().intValue();
				}
			}
		} catch (Exception e) {
//			e.printStackTrace();
			log.error("getMonthlyReportByVendor",e);
		}
		// 退款顯示退貨時間
		detailList = subOrderDetailRepository.findMonthlyRefundReortByVendor(form.getUid(), dateMap.get("start"),
				dateMap.get("end"));
		try {
			SubOrderDetailVo vo = null;
			for (SubOrderDetail detail : detailList) {
				// 加一筆 教具[已付款]、[未出貨] 卻[已退貨]的項目
				if(detail.getProduct().getProps() != null && detail.getDeliveryDt() == null) {
					vo = this.toSubOrderDetailVoForMonthly(detail, OrderStatusEnum.P.name());
					voList.add(vo);
					if(vo != null && vo.getTotalAmount() != null) {
						totalAmount += vo.getTotalAmount().intValue();
					}
					if(vo != null && vo.getRatioAmount() != null) {
						sumRatioTotalAmount += vo.getRatioAmount().intValue();
					}
				}
				vo = this.toSubOrderDetailVoForMonthly(detail, detail.getStatus());
				voList.add(vo);
				if(vo != null && vo.getTotalAmount() != null) {
					totalAmount += vo.getTotalAmount().intValue();
				}
				if(vo != null && vo.getRatioAmount() != null) {
					sumRatioTotalAmount += vo.getRatioAmount().intValue();
				}
			}
		} catch (Exception e) {
//			e.printStackTrace();
			log.error("getMonthlyReportByVendor",e);
		}
		MonthlyReportHeaderVo vo = new MonthlyReportHeaderVo();
		form.setMonthlyReportHeaderVo(vo);
		vo.setYearMonth(form.getYearMonth());
		vo.setReportDate(CommonUtils.getTodayWithoutTimes());
		vo.setReportStart(dateMap.get("start"));
		vo.setReportEnd(dateMap.get("end"));
		vo.setSolds(voList.size());
		vo.setMonthlyAmount(totalAmount);
		vo.setMonthlyRatioAmount(sumRatioTotalAmount);
		if (vendor != null) {
			vo.setVendorName(vendor.getVendorName());
			vo.setVendorCode(vendor.getVendorCode());
			vo.setVendorTaxId(vendor.getTaxId());
		}
		try {
			double feeRate = Double.parseDouble(configService.getProcessingFeeRate());
			double processingFee = sumRatioTotalAmount * feeRate;
			int processingAmount = (int) Math.round(processingFee);
			vo.setProcessingFee(processingAmount);
			vo.setManagementFee(Integer.valueOf(configService.getManagementFee()));
			vo.setAdFee(Integer.valueOf(configService.getAdFee()));
			vo.setWikidueFee(processingAmount + Integer.valueOf(configService.getManagementFee()).intValue() + Integer.valueOf(configService.getAdFee()).intValue());
			vo.setSummaryAmount(vo.getMonthlyRatioAmount().intValue() - vo.getWikidueFee().intValue());
		} catch (NumberFormatException e) {
//			e.printStackTrace();
			log.error("getMonthlyReportByVendor",e);
		}
		// 拒絕退貨(出貨時間與拒絕退貨時間在同一個區間)
//		List<SubOrderDetail> refuseList = subOrderDetailRepository.findMonthlyRefuseReturnReortByVendor(form.getUid(),
//				dateMap.get("start"), dateMap.get("end"));
//		for (SubOrderDetail detail : refuseList) {
//			voList.add(this.toSubOrderDetailVoForMonthly(detail, true, detail.getStatus()));
//		}
		return voList;
	}

	private SubOrderDetailVo toSubOrderDetailVoForMonthly(SubOrderDetail detail, String status) {
		SubOrderDetailVo vo = null;
		int multiplier = 1;
		if(StringUtils.equalsIgnoreCase(status, OrderStatusEnum.R.name())) {
			multiplier = -1;
		}
		try {
			if (detail != null) {
				vo = new SubOrderDetailVo();
				SubOrder subOrder = detail.getSubOrder();
				MainOrder mainOrder = subOrder.getMainOrder();
				Product product = detail.getProduct();
				Payment payment = mainOrder.getPayment();
				if (payment == null) {
					payment = new Payment();
				}
				Integer ratio = null;
				if (product.getCourse() != null) {
					vo.setMaterialNum(product.getCourse().getMaterialNum());
					// 課程看付款時間
					vo.setStatusDt(payment.getPaymentDt());
					ratio = detail.getCourseSplitRatio();
				} else if (product.getProps() != null) {
					vo.setMaterialNum(product.getProps().getMaterialNum());
					Date stausDt = null;
					// 退貨看退款時間
					if(StringUtils.equalsIgnoreCase(status, OrderStatusEnum.R.name())) {
						stausDt = detail.getRefundDt();
					}else {
						// 教具看出貨時間
						status = OrderStatusEnum.D.name();
						stausDt = detail.getDeliveryDt();
						if(stausDt == null) {
							// 未出貨，則看付款時間
							stausDt = payment.getPaymentDt();
							status = OrderStatusEnum.P.name();
						}
					}
					vo.setStatusDt(stausDt);
					ratio = detail.getPropsSplitRatio();
				}
				vo.setStatus(status);
				vo.setProductName(detail.getProductName());
				vo.setOrderDt(mainOrder.getOrderDt());
				vo.setPkMainOrder(mainOrder.getPkMainOrder());
				vo.setInvoiceNo(payment.getInvoiceNo());
				if(detail.getQuantity() != null) {
					vo.setQuantity(detail.getQuantity().intValue() * multiplier);
				}
				if(detail.getUnitPrice() != null) {
					vo.setUnitPrice(detail.getUnitPrice().intValue() * multiplier);
				}
				if(detail.getTotalAmount() != null) {
					vo.setTotalAmount(detail.getTotalAmount().intValue() * multiplier);
				}
				vo.setRatio(ratio);
				if (ratio != null && detail.getUnitPrice() != null && detail.getQuantity() !=null) {
					double amount = detail.getUnitPrice().intValue() * ratio;
					amount = amount / 100;
					int ratioUnitAmount = (int) Math.round(amount);
					ratioUnitAmount = ratioUnitAmount * multiplier;
					vo.setRatioUnitPrice(ratioUnitAmount);
					vo.setRatioAmount(detail.getQuantity().intValue() * ratioUnitAmount);
				}
			}
		} catch (Exception e) {
//			e.printStackTrace();
			log.error("toSubOrderDetailVoForMonthly",e);
		}
		return vo;
	}

	private SubOrderDetailVo toSubOrderDetailVoForSummary(SubOrderDetail detail, boolean positive, String status) {
		SubOrderDetailVo vo = null;
		int multiplier = 1;
		if (!positive) {
			multiplier = -1;
		}
		try {
			if (detail != null) {
				vo = new SubOrderDetailVo();
				SubOrder subOrder = detail.getSubOrder();
				MainOrder mainOrder = subOrder.getMainOrder();
				Product product = detail.getProduct();
				Payment payment = mainOrder.getPayment();
				Integer ratio = 0;
				if (product.getCourse() != null) {
					vo.setMaterialNum(product.getCourse().getMaterialNum());
					ratio = detail.getCourseSplitRatio();
				} else if (product.getProps() != null) {
					vo.setMaterialNum(product.getProps().getMaterialNum());
					ratio = detail.getPropsSplitRatio();
				}
				// 20181018 塞子訂單編號 by Chris
				vo.setPkSubOrder(subOrder.getPkSubOrder());
				vo.setVendorCode(subOrder.getVendor().getVendorCode());
				vo.setVendorName(subOrder.getVendor().getVendorName());
				vo.setStatusDt(detail.getUpdateDt());
				vo.setStatus(status);
				vo.setProductName(detail.getProductName());
				vo.setOrderDt(mainOrder.getOrderDt());
				vo.setPkMainOrder(mainOrder.getPkMainOrder());
				vo.setInvoiceNo(payment.getInvoiceNo());
				if(detail.getQuantity() != null) {
					vo.setQuantity(detail.getQuantity().intValue() * multiplier);
				}
				if(detail.getUnitPrice() != null) {
					vo.setUnitPrice(detail.getUnitPrice().intValue() * multiplier);
				}
				if(detail.getTotalAmount() != null) {
					vo.setTotalAmount(detail.getTotalAmount().intValue() * multiplier);
				}
				vo.setRatio(ratio);
				if (ratio != null && detail.getUnitPrice() != null && detail.getQuantity() != null) {
					double amount = detail.getUnitPrice().intValue() * ratio;
					amount = amount / 100;
					int ratioUnitAmount = (int) Math.round(amount);
					ratioUnitAmount = ratioUnitAmount * multiplier;
					vo.setRatioUnitPrice(ratioUnitAmount);
					vo.setRatioAmount(detail.getQuantity().intValue() * ratioUnitAmount);
				}
			}
		} catch (Exception e) {
//			e.printStackTrace();
			log.error("toSubOrderDetailVoForSummary",e);
		}
		return vo;
	}

	// 上個月16日 00:00 ～ 本月15日 23:59
	private Map<String, Date> getReportDate(Date date) {
		HashMap<String, Date> map = new HashMap<String, Date>();
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MONTH, -1);
		c.add(Calendar.DATE, 15);
		map.put("start", c.getTime());
//		System.err.println("startTime:"+c.getTime());
		c.setTime(date);
		c.add(Calendar.DATE, 14);
		Date endDate = CommonUtils.getDateEndWithTimes(c.getTime());
		map.put("end", endDate);
//		System.err.println("endTime:"+endDate);
		return map;
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public List<SubOrderDetailVo> querySummary(AccountingForm form) throws ServiceException {
		List<SubOrderDetailVo> voList = new ArrayList<SubOrderDetailVo>();
		try {
			List<SubOrderDetail> result = subOrderDetailRepository.findAll(buildOperSpecification(form));
			for (SubOrderDetail detail : result) {
				boolean positive = true;
				if (StringUtils.equalsIgnoreCase(OrderStatusEnum.R.name(), detail.getStatus())) {
					positive = false;
					// 當有狀態為R，再寫一筆P/D的資料(利用月結報表轉換vo)
					SubOrderDetailVo vo = this.toSubOrderDetailVoForMonthly(detail, OrderStatusEnum.P.name());
					vo.setVendorName(detail.getSubOrder().getVendor().getVendorName());
					vo.setVendorCode(detail.getSubOrder().getVendor().getVendorCode());
					// 20181018 塞子訂單編號 by Chris
					vo.setPkSubOrder(detail.getSubOrder().getPkSubOrder());
					
					voList.add(vo);
				}
				voList.add(this.toSubOrderDetailVoForSummary(detail, positive, detail.getStatus()));
			}
			// Collection排序
			if(StringUtils.equalsIgnoreCase("ascending", form.getSorting())) {
				this.sortCollectionAsc(voList);
			}else {
				this.sortCollectionDesc(voList);
			}
			return voList;
		} catch (Exception e) {
//			e.printStackTrace();
			log.error("querySummary", e);
			throw new ServiceException(resources.getMessage("op.query.error", null, null));
		}
	}

	private Specification<SubOrderDetail> buildOperSpecification(AccountingForm form) {
		return new Specification<SubOrderDetail>() {
			@Override
			public Predicate toPredicate(Root<SubOrderDetail> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

				List<Predicate> predicates = new ArrayList<>();

				Join<SubOrderDetail, SubOrder> subOrderObj = root
						.join(root.getModel().getSingularAttribute("subOrder", SubOrder.class), JoinType.LEFT);

				if (!StringUtils.isEmpty(form.getPkMainOrder())) {
					predicates.add(cb.like(subOrderObj.get("mainOrder").get("pkMainOrder").as(String.class),
							"%" + form.getPkMainOrder() + "%"));

				}
				if (form.getReportStart() != null) {
					Date sDate = form.getReportStart();
					sDate = CommonUtils.getDateWithoutTimes(sDate);
					predicates.add(
							cb.greaterThanOrEqualTo(subOrderObj.get("mainOrder").get("orderDt").as(Date.class), sDate));
				}
				if (form.getReportEnd() != null) {
					Date eDate = form.getReportEnd();
					eDate = CommonUtils.getDateEndWithTimes(eDate);
					predicates.add(
							cb.lessThanOrEqualTo(subOrderObj.get("mainOrder").get("orderDt").as(Date.class), eDate));
				}
				if (!StringUtils.isEmpty(form.getUid())) {
					predicates.add(cb.equal(subOrderObj.get("vendor").get("uid").as(String.class), form.getUid()));
				}
				if (!form.getOrderStatusList().isEmpty()) {
					predicates.add(root.get("status").in(form.getOrderStatusList()));
				}

				if (!predicates.isEmpty()) {
					return cb.and(predicates.toArray(new Predicate[predicates.size()]));
				}
				return cb.conjunction();
			}
		};
	}

	private void sortCollectionAsc(List<SubOrderDetailVo> list) {
		Collections.sort(list, new Comparator<SubOrderDetailVo>() {
			public int compare(SubOrderDetailVo o1, SubOrderDetailVo o2) {
				try {
					Date dt1 = o1.getOrderDt();
					Date dt2 = o2.getOrderDt();
					if (dt1 != null && dt2 != null) {
						if (dt1.compareTo(dt2) < 0) {
							return -1;
						} else if (dt1.compareTo(dt2) > 0) {
							return 1;
						} else {
							if (!StringUtils.isEmpty(o1.getVendorCode())) {
								return o1.getVendorCode().compareTo(o2.getVendorCode());
							} else {
								return 0;
							}
						}
					} else {
						return 0;
					}
				} catch (Exception e) {

				}
				return 0;
			}
		});
	}
	
	private void sortCollectionDesc(List<SubOrderDetailVo> list) {
		Collections.sort(list, new Comparator<SubOrderDetailVo>() {
			public int compare(SubOrderDetailVo o1, SubOrderDetailVo o2) {
				try {
					Date dt1 = o1.getOrderDt();
					Date dt2 = o2.getOrderDt();
					if (dt1 != null && dt2 != null) {
						if (dt2.compareTo(dt1) < 0) {
							return -1;
						} else if (dt2.compareTo(dt1) > 0) {
							return 1;
						} else {
							if (!StringUtils.isEmpty(o1.getVendorCode())) {
								return o2.getVendorCode().compareTo(o1.getVendorCode());
							} else {
								return 0;
							}
						}
					} else {
						return 0;
					}
				} catch (Exception e) {

				}
				return 0;
			}
		});
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void exportMonthlyReport(AccountingForm form, OutputStream os) throws ServiceException {
		try {
			XSSFWorkbook wb = new XSSFWorkbook();
			XSSFSheet sheet = wb.createSheet("月結報表");
			Map<String, CellStyle> styleMap = this.getCellStyleMap(wb);
			this.setColumnWidth(sheet);
			this.setHeader(sheet, styleMap, form.getMonthlyReportHeaderVo());
			this.setTableHeader(sheet, styleMap);
			int rowNum = 5;
			for (SubOrderDetailVo vo : form.getReporList()) {
				if(vo != null) {
					this.setDetail(sheet, styleMap, vo, rowNum);
					rowNum++;
				}
			}
			this.setFooter(sheet, styleMap, form.getMonthlyReportHeaderVo(), rowNum);
			wb.write(os);
			os.flush();
			os.close();
		} catch (Exception e) {
//			e.printStackTrace();
			log.error("exportMonthlyReport", e);
		} finally {
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void setColumnWidth(XSSFSheet sheet) {
		sheet.setDefaultRowHeight((short) 400);
		sheet.setDefaultColumnWidth(11);
		sheet.setColumnWidth(0, 4000);
		sheet.setColumnWidth(1, 8000);
		sheet.setColumnWidth(2, 5000);
		sheet.setColumnWidth(3, 4000);
		sheet.setColumnWidth(4, 5000);
		sheet.setColumnWidth(5, 4000);
	}

	private Map<String, CellStyle> getCellStyleMap(XSSFWorkbook wb) {
		Map<String, CellStyle> map = new HashMap<String, CellStyle>();
		Font font = wb.createFont();
		// titlefont.setFontName("Times New Roman");
		font.setColor(HSSFColor.BLACK.index);// 顏色
		font.setBold(false); // 粗體

		Font fontBlod = wb.createFont();
		// titlefont.setFontName("Times New Roman");
		fontBlod.setColor(HSSFColor.BLACK.index);// 顏色
		fontBlod.setBold(true); // 粗體

		CellStyle style = wb.createCellStyle();
		style.setFont(fontBlod);// 設定字體
		style.setFillPattern(FillPatternType.NO_FILL);
		style.setVerticalAlignment(VerticalAlignment.CENTER);// 垂直置中
		style.setAlignment(HorizontalAlignment.LEFT);// 水平置中
		map.put("header", style);

		style = wb.createCellStyle();
		style.setFont(fontBlod);// 設定字體
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		style.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);// 填滿顏色
		style.setVerticalAlignment(VerticalAlignment.CENTER);// 垂直置中
		style.setAlignment(HorizontalAlignment.CENTER);// 水平置中
		// 設定框線
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderLeft(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		map.put("tableHeader", style);

		style = wb.createCellStyle();
		style.setFont(font);// 設定字體
		style.setFillPattern(FillPatternType.NO_FILL);
		style.setVerticalAlignment(VerticalAlignment.CENTER);// 垂直置中
		style.setAlignment(HorizontalAlignment.CENTER);// 水平置中
		// 設定框線
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderLeft(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		style.setWrapText(true);// 自動換行
		map.put("detailCenterAlignment", style);

		style = wb.createCellStyle();
		style.setFont(font);// 設定字體
		style.setFillPattern(FillPatternType.NO_FILL);
		style.setVerticalAlignment(VerticalAlignment.CENTER);// 垂直置中
		style.setAlignment(HorizontalAlignment.LEFT);// 水平置中
		// 設定框線
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderLeft(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		style.setWrapText(true);// 自動換行
		map.put("detailLeftAlignment", style);

		style = wb.createCellStyle();
		style.setFont(font);// 設定字體
		style.setFillPattern(FillPatternType.NO_FILL);
		style.setVerticalAlignment(VerticalAlignment.CENTER);// 垂直置中
		style.setAlignment(HorizontalAlignment.RIGHT);// 水平置中
		// 設定框線
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderLeft(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		DataFormat poiFormat = wb.createDataFormat();
		// Format: 3, "#,##0"
		final short format = poiFormat.getFormat(BuiltinFormats.getBuiltinFormat(3));
		style.setDataFormat(format);
		map.put("detailNumberic", style);

		style = wb.createCellStyle();
		style.setFont(font);// 設定字體
		style.setFillPattern(FillPatternType.NO_FILL);
		style.setVerticalAlignment(VerticalAlignment.CENTER);// 垂直置中
		style.setAlignment(HorizontalAlignment.CENTER);// 水平置中
		map.put("footer", style);
		
		style = wb.createCellStyle();
		style.setFont(fontBlod);// 設定字體
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		style.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);// 填滿顏色
		style.setVerticalAlignment(VerticalAlignment.CENTER);// 垂直置中
		style.setAlignment(HorizontalAlignment.CENTER);// 水平置中
		map.put("signature", style);

		return map;
	}

	private void setHeader(XSSFSheet sheet, Map<String, CellStyle> styleMap,
			MonthlyReportHeaderVo monthlyReportHeaderVo) {
		CellStyle styleRow = styleMap.get("header");
		XSSFRow row = sheet.createRow(0);
		row.setRowStyle(styleRow);
		XSSFCell cell = row.createCell(0);
		cell.setCellValue("報表年月：");
		cell = row.createCell(1);
		cell.setCellValue(CommonUtils.getDateString(monthlyReportHeaderVo.getYearMonth(), "yyyy/MM"));
		cell = row.createCell(3);
		cell.setCellValue("時間區間：");
		cell = row.createCell(4);
		cell.setCellValue(CommonUtils.getDateString(monthlyReportHeaderVo.getReportStart(), "yyyy/MM/dd") + " ~ "
				+ CommonUtils.getDateString(monthlyReportHeaderVo.getReportEnd(), "yyyy/MM/dd"));
		// cell = row.createCell(9);
		// cell.setCellValue("報表產生日期：");
		// cell = row.createCell(10);
		// cell.setCellValue(CommonUtils.getDateString(monthlyReportHeaderVo.getReportDate(),"yyyy/MM/dd"));
		row = sheet.createRow(1);
		cell = row.createCell(0);
		cell.setCellValue("廠商名稱：");
		cell = row.createCell(1);
		cell.setCellValue(monthlyReportHeaderVo.getVendorName());
		cell = row.createCell(3);
		cell.setCellValue("廠商編號：");
		cell = row.createCell(4);
		cell.setCellValue(monthlyReportHeaderVo.getVendorCode());
		cell = row.createCell(9);
		cell.setCellValue("廠商統編：");
		cell = row.createCell(10);
		cell.setCellValue(monthlyReportHeaderVo.getVendorTaxId());
		row = sheet.createRow(2);
		cell = row.createCell(0);
		cell.setCellValue("銷售訂單總筆數：");
		cell = row.createCell(1);
		cell.setCellValue(CommonUtils.getStringWithThousandths(monthlyReportHeaderVo.getSolds()));
		cell = row.createCell(3);
		cell.setCellValue("月銷售總額：");
		cell = row.createCell(4);
		cell.setCellValue(CommonUtils.getStringWithThousandths(monthlyReportHeaderVo.getMonthlyAmount()) + " (含稅)");
		cell = row.createCell(9);
		cell.setCellValue("月進貨總額：");
		cell = row.createCell(10);
		cell.setCellValue(CommonUtils.getStringWithThousandths(monthlyReportHeaderVo.getMonthlyRatioAmount()) + " (含稅)");
		row = sheet.createRow(3);
		cell = row.createCell(12);
		cell.setCellValue("此表皆為含稅金額");
		styleRow.setAlignment(HorizontalAlignment.RIGHT);
		cell.setCellStyle(styleRow);
	}

	private void setTableHeader(XSSFSheet sheet, Map<String, CellStyle> styleMap) {
		CellStyle styleRow = styleMap.get("tableHeader");

		String[] titleArray = { "料號", "品名", "訂單日期", "主訂單編號", "狀態日期", "狀態", "發票號碼", "銷售數量", "銷售單價", "銷售總額", "進價成數", "進貨單價",
				"進貨價總額", };
		XSSFRow headerRow = sheet.createRow(4);
		for (int i = 0; i < titleArray.length; i++) {
			XSSFCell cell = headerRow.createCell(i);
			cell.setCellStyle(styleRow);
			cell.setCellValue(titleArray[i]);
		}
	}

	private void setDetail(XSSFSheet sheet, Map<String, CellStyle> styleMap, SubOrderDetailVo vo, int rowNum) {
		try {
			CellStyle alignCenter = styleMap.get("detailCenterAlignment");
			CellStyle alignRightNum = styleMap.get("detailNumberic");
			CellStyle alignLeft = styleMap.get("detailLeftAlignment");

			XSSFRow row = sheet.createRow(rowNum);
			XSSFCell cell = null;
			for (int i = 0; i <= 12; i++) {
				cell = row.createCell(i);
				cell.setCellStyle(alignCenter);
			}
			cell = row.getCell(0);
			cell.setCellStyle(alignCenter);
			cell.setCellValue(vo.getMaterialNum());
			cell = row.getCell(1);
			cell.setCellStyle(alignLeft);
			cell.setCellValue(vo.getProductName());
			cell = row.getCell(2);
			cell.setCellStyle(alignCenter);
			cell.setCellValue(CommonUtils.getDateString(vo.getOrderDt(), "yyyy-MM-dd HH:mm"));
			cell = row.getCell(3);
			cell.setCellStyle(alignCenter);
			cell.setCellValue(vo.getPkMainOrder());
			cell = row.getCell(4);
			cell.setCellStyle(alignCenter);
			cell.setCellValue(CommonUtils.getDateString(vo.getStatusDt(), "yyyy-MM-dd HH:mm"));
			cell = row.getCell(5);
			cell.setCellStyle(alignLeft);
			cell.setCellValue(CommonUtils.getOrderStatusName(vo.getStatus()));
			cell = row.getCell(6);
			cell.setCellStyle(alignCenter);
			cell.setCellValue(vo.getInvoiceNo());
			cell = row.getCell(7);
			cell.setCellStyle(alignRightNum);
			cell.setCellValue(vo.getQuantity());
			cell = row.getCell(8);
			cell.setCellValue(vo.getUnitPrice());
			cell.setCellStyle(alignRightNum);
			cell = row.getCell(9);
			cell.setCellValue(vo.getTotalAmount());
			cell.setCellStyle(alignRightNum);
			cell = row.getCell(10);
			cell.setCellStyle(alignCenter);
			if (vo.getRatio() != null) {
				cell.setCellValue(vo.getRatio() + " %");
			}
			cell = row.getCell(11);
			cell.setCellStyle(alignRightNum);
			cell.setCellValue(vo.getRatioUnitPrice());
			cell = row.getCell(12);
			cell.setCellStyle(alignRightNum);
			cell.setCellValue(vo.getRatioAmount());
		} catch (Exception e) {
//			e.printStackTrace();
			log.error("exportReport.setDetail", e);
		}
	}

	private void setFooter(XSSFSheet sheet, Map<String, CellStyle> styleMap, MonthlyReportHeaderVo vo, int rowNum) {
		CellStyle styleRow = styleMap.get("footer");
		CellStyle signStyle = styleMap.get("signature");
		XSSFRow row = sheet.createRow(rowNum + 1);
		row.setRowStyle(styleRow);
		XSSFCell cell = row.createCell(0);
		cell.setCellValue("進貨價總金額   = 月進貨價總額 (" + CommonUtils.getStringWithThousandths(vo.getMonthlyRatioAmount())+")");
		row = sheet.createRow(rowNum + 2);
		row.setRowStyle(styleRow);
		cell = row.createCell(0);
		cell.setCellValue("Wikidue收費金額   =  交易手續費  (" + CommonUtils.getStringWithThousandths(vo.getProcessingFee())
				+ ") +  平台管理費 (" + CommonUtils.getStringWithThousandths(vo.getManagementFee()) + ") +  廠商廣告費 ("
				+ CommonUtils.getStringWithThousandths(vo.getAdFee())+")");
		cell = row.createCell(11);
		cell.setCellValue("廠商簽名：");
		row = sheet.createRow(rowNum + 3);
		row.setRowStyle(styleRow);
		cell = row.createCell(0);
		cell.setCellValue("本月結帳總額   =  進貨價總金額  (" + CommonUtils.getStringWithThousandths(vo.getMonthlyRatioAmount())
				+ ") -  Wikidue收費金額 (" + CommonUtils.getStringWithThousandths(vo.getWikidueFee()) + ") = ("
				+ CommonUtils.getStringWithThousandths(vo.getSummaryAmount())+")");
		cell = row.createCell(11);
		cell.setCellStyle(signStyle);
		cell = row.createCell(12);
		cell.setCellStyle(signStyle);
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void exportSummaryReport(AccountingForm form, OutputStream os) throws ServiceException {
		try {
			XSSFWorkbook wb = new XSSFWorkbook();
			XSSFSheet sheet = wb.createSheet("訂單總表");
			Map<String, CellStyle> styleMap = this.getCellStyleMap(wb);
			this.setSummaryColumnWidth(sheet);
			this.setSummaryHeader(sheet, styleMap);
			int rowNum = 1;
			for (SubOrderDetailVo vo : form.getReporList()) {
				if(vo != null) {
					this.setSummaryDetail(sheet, styleMap, vo, rowNum);
					rowNum++;
				}
			}
			wb.write(os);
			os.flush();
			os.close();
		} catch (Exception e) {
//			e.printStackTrace();
			log.error("exportSummaryReport", e);
		} finally {
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void setSummaryColumnWidth(XSSFSheet sheet) {
		sheet.setDefaultRowHeight((short) 400);
		sheet.setDefaultColumnWidth(11);
		sheet.setColumnWidth(0, 5000);
		sheet.setColumnWidth(1, 4000);
		sheet.setColumnWidth(2, 5000);
		sheet.setColumnWidth(3, 4000);
		sheet.setColumnWidth(4, 8000);
		sheet.setColumnWidth(5, 4000);
		sheet.setColumnWidth(6, 4000);
		sheet.setColumnWidth(7, 8000);
	}

	private void setSummaryHeader(XSSFSheet sheet, Map<String, CellStyle> styleMap) {
		CellStyle styleRow = styleMap.get("tableHeader");

		String[] titleArray = { "訂單日期", "主訂單編號", "狀態日期", "狀態", "供應商", "供應商編號", "料號", "品名", "發票號碼", "銷售數量", "銷售單價",
				"銷售總額", "進價成數", "進貨單價","進貨價總額", };
		XSSFRow headerRow = sheet.createRow(0);
		for (int i = 0; i < titleArray.length; i++) {
			XSSFCell cell = headerRow.createCell(i);
			cell.setCellStyle(styleRow);
			cell.setCellValue(titleArray[i]);
		}
	}

	private void setSummaryDetail(XSSFSheet sheet, Map<String, CellStyle> styleMap, SubOrderDetailVo vo, int rowNum) {
		try {
			CellStyle alignCenter = styleMap.get("detailCenterAlignment");
			CellStyle alignRightNum = styleMap.get("detailNumberic");
			CellStyle alignLeft = styleMap.get("detailLeftAlignment");

			XSSFRow row = sheet.createRow(rowNum);
			XSSFCell cell = null;
			for (int i = 0; i <= 14; i++) {
				cell = row.createCell(i);
				cell.setCellStyle(alignCenter);
			}
			cell = row.getCell(0);
			cell.setCellStyle(alignCenter);
			cell.setCellValue(CommonUtils.getDateString(vo.getOrderDt(), "yyyy-MM-dd HH:mm"));
			cell = row.getCell(1);
			cell.setCellStyle(alignCenter);
			cell.setCellValue(vo.getPkMainOrder());
			cell = row.getCell(2);
			cell.setCellStyle(alignCenter);
			cell.setCellValue(CommonUtils.getDateString(vo.getStatusDt(), "yyyy-MM-dd HH:mm"));
			cell = row.getCell(3);
			cell.setCellStyle(alignLeft);
			cell.setCellValue(CommonUtils.getOrderStatusName(vo.getStatus()));
			cell = row.getCell(4);
			cell.setCellStyle(alignLeft);
			cell.setCellValue(vo.getVendorName());
			cell = row.getCell(5);
			cell.setCellStyle(alignCenter);
			cell.setCellValue(vo.getVendorCode());
			cell = row.getCell(6);
			cell.setCellStyle(alignCenter);
			cell.setCellValue(vo.getMaterialNum());
			cell = row.getCell(7);
			cell.setCellStyle(alignLeft);
			cell.setCellValue(vo.getProductName());
			cell = row.getCell(8);
			cell.setCellStyle(alignCenter);
			cell.setCellValue(vo.getInvoiceNo());
			cell = row.getCell(9);
			cell.setCellStyle(alignRightNum);
			cell.setCellValue(vo.getQuantity());
			cell = row.getCell(10);
			cell.setCellStyle(alignRightNum);
			cell.setCellValue(vo.getUnitPrice());
			cell = row.getCell(11);
			cell.setCellStyle(alignRightNum);
			cell.setCellValue(vo.getTotalAmount());
			cell = row.getCell(12);
			cell.setCellStyle(alignCenter);
			if (vo.getRatio() != null) {
				cell.setCellValue(vo.getRatio() + " %");
			}
			cell = row.getCell(13);
			cell.setCellStyle(alignRightNum);
			cell.setCellValue(vo.getRatioUnitPrice());
			cell = row.getCell(14);
			cell.setCellStyle(alignRightNum);
			cell.setCellValue(vo.getRatioAmount());
		} catch (Exception e) {
//			e.printStackTrace();
			log.error("exportSummrayReport.setDetail", e);
		}
	}
}
