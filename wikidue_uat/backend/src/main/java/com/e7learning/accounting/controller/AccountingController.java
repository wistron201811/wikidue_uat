package com.e7learning.accounting.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.e7learning.accounting.form.AccountingForm;
import com.e7learning.accounting.service.AccountingService;
import com.e7learning.accounting.validation.AccountingFormValidation;
import com.e7learning.accounting.vo.SubOrderDetailVo;
import com.e7learning.common.controller.BaseController;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.utils.CommonUtils;
import com.e7learning.repository.model.Vendor;

/**
 * @author J
 *
 */
@Controller
@RequestMapping(value = "/accounting")
@SessionAttributes(AccountingController.FORM_BEAN_NAME)
public class AccountingController extends BaseController {

	private static final Logger log = Logger.getLogger(AccountingController.class);

	public static final String FORM_BEAN_NAME = "accountingForm";
	
	private static final String QUERY_MONTHLY_PAGE = "queryMonthlyPage";

	private static final String QUERY_SUMMARY_PAGE = "querySummaryPage";
	
	@Autowired
	private AccountingFormValidation accountingFormValidation;
	
	@Autowired
	private AccountingService accountingService;
	
	
	@InitBinder(AccountingController.FORM_BEAN_NAME)
	protected void initBinder(WebDataBinder binder, HttpSession session) {
		binder.setValidator(accountingFormValidation);
	}
	
	@RequestMapping(value = "/queryMonthly", method = { RequestMethod.GET })
	public String goQuery(Model model) {
		AccountingForm form = new AccountingForm();
		form.setVendorList(accountingService.getAllVendorList());
		model.addAttribute(FORM_BEAN_NAME, form);
		return QUERY_MONTHLY_PAGE;
	}
	
	@RequestMapping(value = "/queryMonthly", params = "method=queryMonthly", method = { RequestMethod.POST })
	public String query(HttpServletRequest request, AccountingForm form, Model model) {
		accountingService.getMonthlyReportByVendor(form);
		model.addAttribute("hasQueryResult", true);
		model.addAttribute(FORM_BEAN_NAME, form);
		return QUERY_MONTHLY_PAGE;
	}
	
	@RequestMapping(value = "/export", params = "method=exportMonthly", method = { RequestMethod.POST })
	public void exportMonthlyReport( HttpServletResponse res, AccountingForm form, Model model) throws IOException, ServiceException {
		Vendor vendor = accountingService.getVnedorByUid(form.getUid());
		String filename = "";
		if(vendor != null) {
			filename = vendor.getVendorName();
		}
		filename += "_"+CommonUtils.getDateString(form.getYearMonth(), "yyyyMM")+".xlsx";
		filename = URLEncoder.encode(filename, StandardCharsets.UTF_8.toString());
		res.setHeader("Content-Disposition", "attachment; filename=" + filename);
		res.setHeader("Content-Type", "application/octet-stream");
		accountingService.getMonthlyReportByVendor(form);
		accountingService.exportMonthlyReport(form, res.getOutputStream());
	}
	
	@RequestMapping(value = "/querySummary", method = { RequestMethod.GET })
	public String goQuerySummary(Model model) {
		AccountingForm form = new AccountingForm();
		if(accountingService.isAdmin()) {
			form.setVendorList(accountingService.getAllVendorList());
		}else {
			form.setUid(accountingService.getCurrentUserId());
		}
		model.addAttribute(FORM_BEAN_NAME, form);
		return QUERY_SUMMARY_PAGE;
	}
	
	@RequestMapping(value = "/querySummary", params = "method=querySummary", method = { RequestMethod.POST })
	public String querySummary(HttpServletRequest request, AccountingForm form, Model model) {
		try {
			List<SubOrderDetailVo> resultList = accountingService.querySummary(form);
			form.setReporList(resultList);
		} catch (ServiceException e) {
		}
		model.addAttribute("hasQueryResult", true);
		model.addAttribute(FORM_BEAN_NAME, form);
		return QUERY_SUMMARY_PAGE;
	}
	
	@RequestMapping(value = "/export", params = "method=exportSummary", method = { RequestMethod.POST })
	public void exportSummaryReport( HttpServletResponse res, AccountingForm form, Model model) throws IOException, ServiceException {
		String filename = "訂單總表_"+CommonUtils.getDateString(new Date(), "yyyyMMdd")+".xlsx";
		filename = URLEncoder.encode(filename, StandardCharsets.UTF_8.toString());
		res.setHeader("Content-Disposition", "attachment; filename=" + filename);
		res.setHeader("Content-Type", "application/octet-stream");
		List<SubOrderDetailVo> resultList = accountingService.querySummary(form);
		form.setReporList(resultList);
		accountingService.exportSummaryReport(form, res.getOutputStream());
	}
}
