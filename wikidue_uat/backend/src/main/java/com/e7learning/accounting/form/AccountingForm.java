package com.e7learning.accounting.form;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.format.annotation.DateTimeFormat;

import com.e7learning.accounting.vo.MonthlyReportHeaderVo;
import com.e7learning.accounting.vo.SubOrderDetailVo;
import com.e7learning.common.enums.OrderStatusEnum;
import com.e7learning.common.form.BaseForm;
import com.e7learning.repository.model.SubOrderDetail;
import com.e7learning.repository.model.Vendor;

/**
 * @author J
 * 
 * 分類管理
 *
 */
public class AccountingForm extends BaseForm<SubOrderDetail>{
	/**
	 * 報表年月
	 */
	@DateTimeFormat(pattern = "yyyy/MM")
	private Date yearMonth;
	
	/**
	 * 廠商名稱
	 */
	private String vendorName;
	/**
	 * 廠商編號
	 */
	private String vendorCode;
	/**
	 * 廠商統編
	 */
	private String taxId;
	
	/**
	 * 廠商帳號
	 */
	private String uid;
	
	/**
	 * 訂單開始時間
	 */
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date reportStart;
	
	/**
	 * 訂單結束時間
	 */
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date reportEnd;
	
	/**
	 * 主訂單編號
	 */
	private String pkMainOrder;
	
	/**
	 * 訂單狀態選單
	 */
	private Map<String,String> orderStatusEnumMap;
	
	/**
	 * 訂單狀態勾選清單
	 */
	private List<String> orderStatusList;
	
	/**
	 * 排序
	 */
	private String sorting;
	
	
	private List<Vendor> vendorList;
	
	private MonthlyReportHeaderVo monthlyReportHeaderVo;
	
	private List<SubOrderDetailVo> reporList;
	
	
	public Date getYearMonth() {
		return yearMonth;
	}
	public void setYearMonth(Date yearMonth) {
		this.yearMonth = yearMonth;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	public String getTaxId() {
		return taxId;
	}
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}
	public List<Vendor> getVendorList() {
		return vendorList;
	}
	public void setVendorList(List<Vendor> vendorList) {
		this.vendorList = vendorList;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public MonthlyReportHeaderVo getMonthlyReportHeaderVo() {
		return monthlyReportHeaderVo;
	}
	public void setMonthlyReportHeaderVo(MonthlyReportHeaderVo monthlyReportHeaderVo) {
		this.monthlyReportHeaderVo = monthlyReportHeaderVo;
	}
	public List<SubOrderDetailVo> getReporList() {
		return reporList;
	}
	public void setReporList(List<SubOrderDetailVo> reporList) {
		this.reporList = reporList;
	}
	public Date getReportStart() {
		return reportStart;
	}
	public void setReportStart(Date reportStart) {
		this.reportStart = reportStart;
	}
	public Date getReportEnd() {
		return reportEnd;
	}
	public void setReportEnd(Date reportEnd) {
		this.reportEnd = reportEnd;
	}
	public String getPkMainOrder() {
		return pkMainOrder;
	}
	public void setPkMainOrder(String pkMainOrder) {
		this.pkMainOrder = pkMainOrder;
	}
	public List<String> getOrderStatusList() {
		return orderStatusList;
	}
	public void setOrderStatusList(List<String> orderStatusList) {
		this.orderStatusList = orderStatusList;
	}
	public String getSorting() {
		return sorting;
	}
	public void setSorting(String sorting) {
		this.sorting = sorting;
	}
	public Map<String, String> getOrderStatusEnumMap() {
		this.orderStatusEnumMap = new LinkedHashMap<String,String>();
		orderStatusEnumMap.put(OrderStatusEnum.N.name(), OrderStatusEnum.N.getDisplayName());
		orderStatusEnumMap.put(OrderStatusEnum.P.name(), OrderStatusEnum.P.getDisplayName());
		orderStatusEnumMap.put(OrderStatusEnum.D.name(), OrderStatusEnum.D.getDisplayName());
		orderStatusEnumMap.put(OrderStatusEnum.H.name(), OrderStatusEnum.H.getDisplayName());
		orderStatusEnumMap.put(OrderStatusEnum.G.name(), OrderStatusEnum.G.getDisplayName());
		orderStatusEnumMap.put(OrderStatusEnum.R.name(), OrderStatusEnum.R.getDisplayName());
		orderStatusEnumMap.put(OrderStatusEnum.J.name(), OrderStatusEnum.J.getDisplayName());
		orderStatusEnumMap.put(OrderStatusEnum.C.name(), OrderStatusEnum.C.getDisplayName());
		orderStatusEnumMap.put(OrderStatusEnum.S.name(), OrderStatusEnum.S.getDisplayName());
		return orderStatusEnumMap;
	}
	
}
