package com.e7learning.accounting.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.e7learning.accounting.form.AccountingForm;

@Component
public class AccountingFormValidation implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(AccountingForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		AccountingForm form = (AccountingForm) target;
		if (StringUtils.equalsIgnoreCase("save", form.getMethod())
				|| StringUtils.equalsIgnoreCase("modify", form.getMethod())) {
//			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "categoryName", "validate.empty.message");
//			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "categoryCode", "validate.empty.message");
//			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "categoryType", "validate.empty.message");
		}
		if (errors.hasErrors()) {
			return;
		}
	}
}