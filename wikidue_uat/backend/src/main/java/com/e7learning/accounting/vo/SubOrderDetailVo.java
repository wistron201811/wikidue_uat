
package com.e7learning.accounting.vo;

import java.io.Serializable;
import java.util.Date;

public class SubOrderDetailVo implements Serializable {
	private static final long serialVersionUID = 1L;

	private String materialNum;
	
	private String productName;
	
	private Date orderDt;
	
	private String pkMainOrder;
	
	private Date statusDt;
	
	private String status;

	private String invoiceNo;
	
	private Integer quantity;
	
	private Integer unitPrice;
	
	private Integer totalAmount;
	
	// 進價成數
	private Integer ratio;
	
	// 進貨單價 Round(unitPrice x ratio , 0 )
	private Integer ratioUnitPrice;
	
	// 進貨價總額 Round(unitPrice x ratio , 0 ) x quantity
	private Integer ratioAmount;
	
	private String vendorName;
	
	private String vendorCode;
	
	private Integer pkSubOrder;
	
	public SubOrderDetailVo() {
	}

	public String getMaterialNum() {
		return materialNum;
	}

	public void setMaterialNum(String materialNum) {
		this.materialNum = materialNum;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Date getOrderDt() {
		return orderDt;
	}

	public void setOrderDt(Date orderDt) {
		this.orderDt = orderDt;
	}

	public String getPkMainOrder() {
		return pkMainOrder;
	}

	public void setPkMainOrder(String pkMainOrder) {
		this.pkMainOrder = pkMainOrder;
	}

	public Date getStatusDt() {
		return statusDt;
	}

	public void setStatusDt(Date statusDt) {
		this.statusDt = statusDt;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Integer unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Integer getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Integer getRatio() {
		return ratio;
	}

	public void setRatio(Integer ratio) {
		this.ratio = ratio;
	}

	public Integer getRatioAmount() {
		return ratioAmount;
	}

	public void setRatioAmount(Integer ratioAmount) {
		this.ratioAmount = ratioAmount;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public Integer getRatioUnitPrice() {
		return ratioUnitPrice;
	}

	public void setRatioUnitPrice(Integer ratioUnitPrice) {
		this.ratioUnitPrice = ratioUnitPrice;
	}

	public Integer getPkSubOrder() {
		return pkSubOrder;
	}

	public void setPkSubOrder(Integer pkSubOrder) {
		this.pkSubOrder = pkSubOrder;
	}

	
	
	
}