
package com.e7learning.accounting.vo;

import java.io.Serializable;
import java.util.Date;

public class MonthlyReportHeaderVo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Date yearMonth;
	
	private Integer solds;
	
	private Integer monthlyAmount;

	private Integer monthlyRatioAmount;

	private Date reportStart;

	private Date reportEnd;

	private Date reportDate;
	
	private String vendorName;
	
	private String vendorCode;
	
	private String vendorTaxId;
	
	private Integer processingFee;
	
	private Integer managementFee;
	
	private Integer adFee;
	
	private Integer wikidueFee;
	
	private Integer summaryAmount; 

	public Integer getSolds() {
		return solds;
	}

	public void setSolds(Integer solds) {
		this.solds = solds;
	}

	public Integer getMonthlyAmount() {
		return monthlyAmount;
	}

	public void setMonthlyAmount(Integer monthlyAmount) {
		this.monthlyAmount = monthlyAmount;
	}
	
	public Integer getMonthlyRatioAmount() {
		return monthlyRatioAmount;
	}

	public void setMonthlyRatioAmount(Integer monthlyRatioAmount) {
		this.monthlyRatioAmount = monthlyRatioAmount;
	}

	public Date getReportStart() {
		return reportStart;
	}

	public void setReportStart(Date reportStart) {
		this.reportStart = reportStart;
	}

	public Date getReportEnd() {
		return reportEnd;
	}

	public void setReportEnd(Date reportEnd) {
		this.reportEnd = reportEnd;
	}

	public Date getReportDate() {
		return reportDate;
	}

	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getVendorTaxId() {
		return vendorTaxId;
	}

	public void setVendorTaxId(String vendorTaxId) {
		this.vendorTaxId = vendorTaxId;
	}

	public Integer getProcessingFee() {
		return processingFee;
	}

	public void setProcessingFee(Integer processingFee) {
		this.processingFee = processingFee;
	}

	public Integer getManagementFee() {
		return managementFee;
	}

	public void setManagementFee(Integer managementFee) {
		this.managementFee = managementFee;
	}

	public Integer getAdFee() {
		return adFee;
	}

	public void setAdFee(Integer adFee) {
		this.adFee = adFee;
	}

	public Date getYearMonth() {
		return yearMonth;
	}

	public void setYearMonth(Date yearMonth) {
		this.yearMonth = yearMonth;
	}

	public Integer getWikidueFee() {
		return wikidueFee;
	}

	public void setWikidueFee(Integer wikidueFee) {
		this.wikidueFee = wikidueFee;
	}

	public Integer getSummaryAmount() {
		return summaryAmount;
	}

	public void setSummaryAmount(Integer summaryAmount) {
		this.summaryAmount = summaryAmount;
	}
	
}