package com.e7learning.video.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.common.Constant;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.CommonService;
import com.e7learning.common.service.MailService;
import com.e7learning.repository.CourseVideoRepository;
import com.e7learning.repository.VideoRepository;
import com.e7learning.repository.model.Course;
import com.e7learning.repository.model.CourseChapter;
import com.e7learning.repository.model.CourseVideo;
import com.e7learning.repository.model.Video;
import com.e7learning.repository.model.VideoData;
import com.e7learning.repository.model.VideoTag;
import com.e7learning.video.vo.VideoDataVo;
import com.e7learning.video.vo.VideoTagVo;
import com.e7learning.video.vo.VideoVo;

@Service
public class VideoService extends BaseService {

	private static final Logger LOG = Logger.getLogger(VideoService.class);

	private static final String API_RESULT_SUCCESS = "succeed";

	private static final String TIME_FORMAT = "yyyy/MM/dd HH:mm";

	@Autowired
	private VideoRepository videoRepository;

	@Autowired
	private CourseVideoRepository courseVideoRepository;

	@Autowired
	private MailService mailService;

	@Autowired
	private CommonService commonService;

	@Transactional(rollbackFor = { ServiceException.class }, isolation = Isolation.READ_COMMITTED)
	public void updateVideo(VideoVo vo) throws ServiceException {
		boolean result = false;
		String email = null;
		String courseName = null;
		String courseChapterName = null;
		String courseVideoName = null;
		String error = StringUtils.EMPTY;
		try {
			if (vo == null || StringUtils.isEmpty(vo.getTitle())) {
				LOG.error("影片參數錯誤");
				error = "影片參數錯誤";
				return;
			}
			Video video = videoRepository.findOne(vo.getTitle());

			if (video == null) {
				LOG.error("查無影片:" + vo.getTitle());
				error = "查無影片:" + vo.getTitle();
				return;
			}

			if (StringUtils.equals(video.getResult(), Constant.TRUE)) {
				LOG.error("影片狀態已更新");
				error = "影片狀態已更新";
				return;
			}

			video.setAccountName(vo.getAccountName());
			video.setDescription(vo.getDescription());
			video.setDuration(vo.getDuration());
			video.setEmbed(vo.getEmbed());
			video.setFileName(vo.getFileName());
			video.setImg(vo.getImg());
			video.setResult(
					StringUtils.equalsIgnoreCase(vo.getResult(), API_RESULT_SUCCESS) ? Constant.TRUE : Constant.FLASE);
			video.setThirdId(vo.getHbID());
			video.setTitle(vo.getTitle());
			video.setStream(vo.getStream());
			// 塞playerStreamUrl
			video.setPlayerStreamUrl(vo.getPlayerStreamUrl());
			video.setUpdateDt(new Date());

			if (vo.getFiles() != null && !vo.getFiles().isEmpty()) {
				List<VideoData> videoDataList = new ArrayList<>();
				for (VideoDataVo fileVo : vo.getFiles()) {
					VideoData data = new VideoData();
					data.setBitRate(fileVo.getBitrate());
					data.setHttp(fileVo.getHttp());
					data.setResolution(fileVo.getResolution());
					data.setVideo(video);
					videoDataList.add(data);
				}
				video.setVideoDataList(videoDataList);
			}

			if (vo.getTags() != null && !vo.getTags().isEmpty()) {
				List<VideoTag> tagList = new ArrayList<>();
				for (VideoTagVo tagVo : vo.getTags()) {
					VideoTag tag = new VideoTag();
					tag.setTagName(tagVo.getTag());
					tag.setVideo(video);
					tagList.add(tag);
				}
				video.setVideoTagList(tagList);
			}

			videoRepository.save(video);
			result = true;
		} catch (Exception e) {
			LOG.error("exception", e);
			result = false;
			error = "上傳影片失敗";
			throw new ServiceException("更新影片失敗");
		} finally {
			try {
				if (vo != null) {
					List<CourseVideo> list = courseVideoRepository
							.findAll((Root<CourseVideo> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
								query.distinct(true);
								return cb.equal(root.get("video").get("pkVideo"), vo.getTitle());
							});
					if (list != null && !list.isEmpty()) {
						CourseVideo cv = list.get(0);
						courseVideoName = cv.getCourseVideoName();
						CourseChapter cc = cv.getCourseChapter();
						if (cc != null) {
							courseChapterName = cc.getCourseChapterName();
							Course course = cc.getCourse();
							if (course != null) {
								courseName = course.getCourseName();
							}
						}
						SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT);
						Date dateTime = null;
						try {
							dateTime = dateFormatter.parse(vo.getDatetime());
						} catch (Exception e) {

						}

						List<String> adminEmailList = commonService.getAdminEmailList();
						Date sendDate = new Date();
						List<Map<String, String>> details = new ArrayList<>();
						Map<String, String> detail = new HashMap<>();
						String uploadDate = dateTime == null ? StringUtils.EMPTY : sdf.format(dateTime);
						detail.put("uploadDate", uploadDate);
						detail.put("name", StringUtils.isEmpty(courseVideoName) ? StringUtils.EMPTY : courseVideoName);
						detail.put("id", StringUtils.isEmpty(vo.getTitle()) ? StringUtils.EMPTY : vo.getTitle());
						detail.put("transferDate", uploadDate);
						if (!result) {
							detail.put("status", error);
						}
						details.add(detail);
						if (result) {
							mailService.sendVideoUploadSuccessEmail(
									adminEmailList.toArray(new String[adminEmailList.size()]), null, courseName,
									sendDate, details);
						} else {
							mailService.sendVideoUploadFailedEmail(
									adminEmailList.toArray(new String[adminEmailList.size()]), null, courseName,
									sendDate, details);
						}
					}
				}

			} catch (Exception e) {
				LOG.error("exception", e);
			}
		}

	}

}
