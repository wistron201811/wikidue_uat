package com.e7learning.video.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VideoVo {

	private String result;

	private String accountName;

	private String hbID;

	private String fileName;

	private String img;

	private String embed;

	private String title;

	private String description;

	private Integer duration;

	private List<VideoTagVo> tags;

	private List<VideoDataVo> files;

	private String stream;
	
	private String playerStreamUrl;
	
	private String datetime;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getHbID() {
		return hbID;
	}

	public void setHbID(String hbID) {
		this.hbID = hbID;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getEmbed() {
		return embed;
	}

	public void setEmbed(String embed) {
		this.embed = embed;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public List<VideoTagVo> getTags() {
		return tags;
	}

	public void setTags(List<VideoTagVo> tags) {
		this.tags = tags;
	}

	public List<VideoDataVo> getFiles() {
		return files;
	}

	public void setFiles(List<VideoDataVo> files) {
		this.files = files;
	}

	public String getStream() {
		return stream;
	}

	public void setStream(String stream) {
		this.stream = stream;
	}

	public String getPlayerStreamUrl() {
		return playerStreamUrl;
	}

	public void setPlayerStreamUrl(String playerStreamUrl) {
		this.playerStreamUrl = playerStreamUrl;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	
}
