package com.e7learning.camp.validation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

import com.e7learning.camp.form.CampForm;
import com.e7learning.common.Constant;
import com.e7learning.common.ValidatorUtil;
import com.e7learning.common.utils.CommonUtils;
import com.e7learning.common.utils.E7ValidationUtils;

@Component
public class CampFormValidation implements Validator {


	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(CampForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		CampForm form = (CampForm) target;
		if (StringUtils.equalsIgnoreCase("save", form.getMethod())
				|| StringUtils.equalsIgnoreCase("modify", form.getMethod())) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "campNum", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "campName", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "campType", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "discountPrice", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "externalLink", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "attendantTarget", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "organizer", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "activityCounty", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "activitySDate", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "activityEDate", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "signUpEndDate", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "campIntroduction", "validate.empty.message");
		}
		if (StringUtils.equalsIgnoreCase("save", form.getMethod())) {
			MultipartFile image = form.getPhoto();
			if (image == null || StringUtils.isEmpty(image.getOriginalFilename())) {
				errors.rejectValue("photo", "validate.empty.message");
			}
		}
		if (StringUtils.isNotBlank(form.getPrice())) {
			if (!ValidatorUtil.isNumber(form.getPrice())) {
				errors.rejectValue("price", "validate.number.format");
			}
		}
		if (StringUtils.isNotBlank(form.getDiscountPrice())) {
			if (!ValidatorUtil.isNumber(form.getDiscountPrice())) {
				errors.rejectValue("discountPrice", "validate.number.format");
			}
		}
		// 驗證原價、售價
		if (StringUtils.isNotBlank(form.getDiscountPrice()) && StringUtils.isNotBlank(form.getPrice())
				&& ValidatorUtil.isNumber(form.getPrice()) && ValidatorUtil.isNumber(form.getDiscountPrice())) {
			if (Integer.parseInt(form.getDiscountPrice()) > Integer.parseInt(form.getPrice())) {
				errors.rejectValue("discountPrice", "product.price.error");
			}
		}
		// 驗證起迄日
		if (form.getActivitySDate() != null && form.getActivityEDate() != null) {
			if (form.getActivityEDate().compareTo(form.getActivitySDate()) < 0) {
				errors.rejectValue("activityEDate", "camp.activity.date.err");
			}
		}
		// 驗證報名起迄日
		if (form.getSignUpStartDate() != null && form.getSignUpEndDate() != null) {
			if (form.getSignUpEndDate().compareTo(form.getSignUpStartDate()) < 0) {
				errors.rejectValue("signUpEndDate", "camp.sign.start.err");
			}
		}
		// 驗證報名截止日
		if (form.getActivitySDate() != null && form.getActivityEDate() != null && form.getSignUpEndDate() != null) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String dateString = sdf.format(form.getActivitySDate());
				Date activitySDate = sdf.parse(dateString);
				if (form.getSignUpEndDate().compareTo(activitySDate) >= 0) {
					errors.rejectValue("signUpEndDate", "camp.sign.end.err");
				} else {
					if (form.getSignUpEndDate().compareTo(CommonUtils.getTodayWithoutTimes()) < 0) {
						errors.rejectValue("signUpEndDate", "camp.activity.sign.today.err");
					}
				}
			} catch (ParseException e) {

			}
		}
		// 驗證圖片
		if (form.getPhoto() != null && form.getPhoto().getSize() > 0) {
			if (form.getPhoto().getSize() > Constant.UPLOAD_FILE_SIZE) {
				errors.rejectValue("photo", "validate.upload.size", new Object[] { Constant.UPLOAD_FILE_SIZE_MB },
						"system.error");
			} else {
//				String fileName = form.getPhoto().getOriginalFilename().trim();
				if (!E7ValidationUtils.isImageFile(form.getPhoto())) {
					errors.rejectValue("photo", "image.type.error");
				}
			}
		}
	}
}