package com.e7learning.camp.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.camp.form.CampForm;
import com.e7learning.common.Constant;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.service.FileStorageService;
import com.e7learning.common.utils.CommonUtils;
import com.e7learning.repository.CampRepository;
import com.e7learning.repository.CategoryRepository;
import com.e7learning.repository.model.Camp;
import com.e7learning.repository.model.Category;

/**
 * @author J
 *
 */
@Service
public class CampService extends BaseService {

	private static final Logger log = Logger.getLogger(CampService.class);

	@Autowired
	private FileStorageService fileStorageService;

	@Autowired
	private ConfigService configService;

	@Autowired
	private MessageSource resources;

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private CampRepository campRepository;

	public List<Category> getCategoryList() {
		return categoryRepository.findByCategoryTypeOrderByCategoryCode("CAMP").stream()
				.filter(v -> Constant.TRUE.equals(v.getActive())).collect(Collectors.toList());
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void addCamp(CampForm form) throws ServiceException {
		try {
			Camp camp = new Camp();
			BeanUtils.copyProperties(camp, form);
			if (form.getPhoto() != null && !form.getPhoto().isEmpty()) {
				camp.setImageId(fileStorageService.saveFile(form.getPhoto()).getPkFile());
			}
			if (StringUtils.isNotBlank(form.getCampType())) {
				camp.setCategory(categoryRepository.findOne(Integer.valueOf(form.getCampType())));
			}
			camp.setUpdateDt(new Date());
			camp.setCreateDt(new Date());
			camp.setCreateId(getCurrentUserId());
			camp.setLastModifiedId(getCurrentUserId());
			campRepository.save(camp);
		} catch (Exception e) {
			log.error("addCamp", e);
			throw new ServiceException(resources.getMessage("op.add.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.add.success", null, null));
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void saveCamp(CampForm form) throws ServiceException {
		try {
			Camp camp = this.findCampByPk(form.getPkCamp());
			if (camp != null) {
				BeanUtils.copyProperties(camp, form);
				if (form.getPhoto() != null && !form.getPhoto().isEmpty()) {
					fileStorageService.delFile(camp.getImageId());
					camp.setImageId(fileStorageService.saveFile(form.getPhoto()).getPkFile());
				}
				if (StringUtils.isNotBlank(form.getCampType())) {
					camp.setCategory(categoryRepository.findOne(Integer.valueOf(form.getCampType())));
				}
				camp.setUpdateDt(new Date());
				camp.setLastModifiedId(getCurrentUserId());
			}
		} catch (Exception e) {
			log.error("editCamp", e);
			throw new ServiceException(resources.getMessage("op.modify.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.modify.success", null, null));
	}

	public Page<Camp> queryCamp(CampForm form, Integer page) throws ServiceException {
		try {
			Sort sort = new Sort(Direction.DESC, "signUpEndDate");
			Pageable pageable = new PageRequest(page - 1, configService.getPageSize(), sort);
			Page<Camp> result = campRepository.findAll(buildOperSpecification(form), pageable);
			return result;
		} catch (Exception e) {
			log.error("queryCamp", e);
			throw new ServiceException(resources.getMessage("op.query.error", null, null));
		}
	}

	public Camp findCampByPk(Integer campPk) {
		return campRepository.findOne(campPk);
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void remove(Integer campPk) throws ServiceException {
		if (campPk != null) {
			String result = this.validateBeforeRemove(campPk);
			if (StringUtils.isNotEmpty(result)) {
				throw new ServiceException(result);
			}
			try {
				Camp camp = this.findCampByPk(campPk);
				campRepository.delete(camp);
				if (StringUtils.isNotBlank(camp.getImageId())) {
					fileStorageService.delFile(camp.getImageId());
				}
			} catch (Exception e) {
				log.error("removeCamp", e);
				throw new ServiceException(resources.getMessage("op.del.error", null, null));
			}
		} else {
			log.error("Can't find PK for " + campPk);
			throw new ServiceException(resources.getMessage("op.del.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.del.success", null, null));
	}

	private String validateBeforeRemove(Integer campPk) {
		String result = "";

		return result;
	}

	private Specification<Camp> buildOperSpecification(CampForm form) {
		return new Specification<Camp>() {
			@Override
			public Predicate toPredicate(Root<Camp> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

				List<Predicate> predicates = new ArrayList<>();

				if (StringUtils.isNotEmpty(form.getCampName())) {
					Path<String> np = root.get("campName");
					predicates.add(cb.like(np, "%" + form.getCampName() + "%"));
				}
				if (StringUtils.isNotEmpty(form.getCampNum())) {
					Path<String> np = root.get("campNum");
					predicates.add(cb.like(np, "%" + form.getCampNum() + "%"));
				}
				if (StringUtils.isNotEmpty(form.getCampType())) {
					Join<Camp, Category> categoryJoin = root
							.join(root.getModel().getSingularAttribute("category", Category.class), JoinType.LEFT);
					predicates.add(cb.equal(categoryJoin.get("pkCategory"), Integer.valueOf(form.getCampType())));
				}
				if (form.getSignUpEndDate() != null) {
					Path<Date> np = root.get("signUpEndDate");
					Date sDate = form.getSignUpEndDate();
					sDate = CommonUtils.getDateWithoutTimes(sDate);
					predicates.add(cb.greaterThanOrEqualTo(np, sDate));
				}
				if (form.getSignUpEndDateEnd() != null) {
					Path<Date> np = root.get("signUpEndDate");
					Date eDate = form.getSignUpEndDateEnd();
					eDate = CommonUtils.getDateEndWithTimes(eDate);
					predicates.add(cb.lessThanOrEqualTo(np, eDate));
				}
				if (!predicates.isEmpty()) {
					return cb.and(predicates.toArray(new Predicate[predicates.size()]));
				}
				return cb.conjunction();
			}
		};
	}
}
