package com.e7learning.camp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.e7learning.camp.form.CampForm;
import com.e7learning.camp.service.CampService;
import com.e7learning.camp.validation.CampFormValidation;
import com.e7learning.common.controller.BaseController;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.FileStorageService;
import com.e7learning.repository.CategoryRepository;
import com.e7learning.repository.model.Camp;

/**
 * @author J
 *
 */
@Controller
@RequestMapping(value = "/camp")
@SessionAttributes(CampController.FORM_BEAN_NAME)
public class CampController extends BaseController {

	private static final Logger log = Logger.getLogger(CampController.class);

	public static final String FORM_BEAN_NAME = "campForm";
	
	private static final String QUERY_PAGE = "queryCampPage";

	private static final String ADD_PAGE = "addCampPage";
	
	private static final String EDIT_PAGE = "editCampPage";

	@Autowired
	private CampFormValidation campFormValidation;
	
	@Autowired
	private CampService campService ;
	
	@Autowired
	private FileStorageService fileStorageService;
	
	@InitBinder(CampController.FORM_BEAN_NAME)
	protected void initBinder(WebDataBinder binder, HttpSession session) {
		binder.setValidator(campFormValidation);
	}
	
	@RequestMapping(value = "/query", method = { RequestMethod.GET })
	public String goQuery(Model model) {
		CampForm form = new CampForm();
		try {
			form.setResult(campService.queryCamp(form, 1));
			form.setCategoryList(campService.getCategoryList());
		} catch (ServiceException e) {
			
		}
		model.addAttribute(FORM_BEAN_NAME, form);
		return QUERY_PAGE;
	}

	@RequestMapping(value = "/query", params = "method=query")
	public String query(HttpServletRequest request, CampForm form, Model model) {
		String pageName = new ParamEncoder("list").encodeParameterName(TableTagParameters.PARAMETER_PAGE);
		int nextPage = getPage(request.getParameter(pageName));
		try {
			form.setResult(campService.queryCamp(form, nextPage));
			form.setCategoryList(campService.getCategoryList());
		} catch (ServiceException e) {
			
		}
		return QUERY_PAGE;
	}
	
	@RequestMapping(value = "/add", method = { RequestMethod.GET })
	public String goAdd(Model model) {
		CampForm form = new CampForm();
		form.setCategoryList(campService.getCategoryList());
		model.addAttribute(FORM_BEAN_NAME, form);
		return ADD_PAGE;
	}

	@RequestMapping(params = "method=save", value = "/add", method = { RequestMethod.POST })
	public String add(@Validated CampForm form, BindingResult result) {
		if (result.hasErrors()) {
			return ADD_PAGE;
		}
		try {
			campService.addCamp(form);
			return "redirect:/camp/query?method=query";
		} catch (Exception e) {
			
		}
		return ADD_PAGE;
	}
	
	@RequestMapping(params = "method=remove", value = "/remove", method = { RequestMethod.POST })
	public String remove(CampForm form, Model model) {
		try {
			campService.remove(form.getPkCamp());
			return "redirect:/camp/query?method=query";
		} catch (ServiceException e) {
			// 避免display tag 查詢request發生405錯誤
			form.setResult(null);
			model.addAttribute(FORM_BEAN_NAME, form);
		}
		return QUERY_PAGE;
	}
	
	@RequestMapping(value = "/modify", method = { RequestMethod.POST })
	public String goModify(Model model, CampForm form) {
		if(form.getPkCamp() != null) {
			Camp camp = campService.findCampByPk(form.getPkCamp());
			try {
				if(camp != null) {
					BeanUtils.copyProperties(form, camp);
					form.setPhoto(fileStorageService.getFileStorage(camp.getImageId()).orElse(null));
					form.setCategoryList(campService.getCategoryList());
					form.setCampType(camp.getCategory().getPkCategory().toString());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			model.addAttribute(FORM_BEAN_NAME, form);
		}
		return EDIT_PAGE;
	}
	
	@RequestMapping(params = "method=modify", value = "/modify", method = { RequestMethod.POST })
	public String modify(@Validated CampForm form, BindingResult result) {
		if (result.hasErrors()) {
			Camp camp = campService.findCampByPk(form.getPkCamp());
			if(camp != null) {
				form.setPhoto(fileStorageService.getFileStorage(camp.getImageId()).orElse(null));
				form.setCampType(camp.getCategory().getPkCategory().toString());
			}
			return EDIT_PAGE;
		}
		try {
			campService.saveCamp(form);
			return "redirect:/camp/query?method=query";
		} catch (ServiceException e) {
			
		}
		return EDIT_PAGE;
	}
	
}
