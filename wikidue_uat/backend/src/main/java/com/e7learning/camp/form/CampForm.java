package com.e7learning.camp.form;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import com.e7learning.common.form.BaseForm;
import com.e7learning.repository.model.Camp;
import com.e7learning.repository.model.Category;

/**
 * @author J
 * 
 * 營隊管理
 *
 */
public class CampForm extends BaseForm<Camp>{
	
	private Integer pkCamp;

	private String active;

	private String campNum;

	private String discountPrice;

	private String campName;

	private String price;

	private String costPrice;
	
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date activitySDate;

	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date activityEDate;
	
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date signUpEndDate;
	
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date signUpStartDate;
	
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date signUpEndDateEnd;
	
	private String attendantTarget;
	
	private String organizer;
	
	private MultipartFile photo;
	
	private String activityCounty;
	
	private String externalLink;

	private String campType;
	
	private String campIntroduction;
	
	private List<Category> categoryList;
	
	public Integer getPkCamp() {
		return pkCamp;
	}

	public void setPkCamp(Integer pkCamp) {
		this.pkCamp = pkCamp;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getCampNum() {
		return campNum;
	}

	public void setCampNum(String campNum) {
		this.campNum = campNum;
	}

	public String getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(String discountPrice) {
		this.discountPrice = discountPrice;
	}

	public String getCampName() {
		return campName;
	}

	public void setCampName(String campName) {
		this.campName = campName;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(String costPrice) {
		this.costPrice = costPrice;
	}

	public Date getActivitySDate() {
		return activitySDate;
	}

	public void setActivitySDate(Date activitySDate) {
		this.activitySDate = activitySDate;
	}

	public Date getActivityEDate() {
		return activityEDate;
	}

	public void setActivityEDate(Date activityEDate) {
		this.activityEDate = activityEDate;
	}

	public Date getSignUpEndDate() {
		return signUpEndDate;
	}

	public void setSignUpEndDate(Date signUpEndDate) {
		this.signUpEndDate = signUpEndDate;
	}

	public String getAttendantTarget() {
		return attendantTarget;
	}

	public void setAttendantTarget(String attendantTarget) {
		this.attendantTarget = attendantTarget;
	}

	public String getOrganizer() {
		return organizer;
	}

	public void setOrganizer(String organizer) {
		this.organizer = organizer;
	}


	public MultipartFile getPhoto() {
		return photo;
	}

	public void setPhoto(MultipartFile photo) {
		this.photo = photo;
	}

	public String getActivityCounty() {
		return activityCounty;
	}

	public void setActivityCounty(String activityCounty) {
		this.activityCounty = activityCounty;
	}

	public String getExternalLink() {
		return externalLink;
	}

	public void setExternalLink(String externalLink) {
		this.externalLink = externalLink;
	}

	public String getCampType() {
		return campType;
	}

	public void setCampType(String campType) {
		this.campType = campType;
	}

	
	public void setCategoryList(List<Category> categoryList) {
		this.categoryList = categoryList;
	}

	public List<Category> getCategoryList() {
		return categoryList;
	}

	public String getCampIntroduction() {
		return campIntroduction;
	}

	public void setCampIntroduction(String campIntroduction) {
		this.campIntroduction = campIntroduction;
	}

	public Date getSignUpStartDate() {
		return signUpStartDate;
	}

	public void setSignUpStartDate(Date signUpStartDate) {
		this.signUpStartDate = signUpStartDate;
	}

	public Date getSignUpEndDateEnd() {
		return signUpEndDateEnd;
	}

	public void setSignUpEndDateEnd(Date signUpEndDateEnd) {
		this.signUpEndDateEnd = signUpEndDateEnd;
	}

}
