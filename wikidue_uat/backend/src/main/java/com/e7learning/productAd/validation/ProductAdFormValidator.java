package com.e7learning.productAd.validation;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.e7learning.productAd.form.ProductAdForm;

@Component
public class ProductAdFormValidator implements Validator {

	private static final Logger LOG = Logger.getLogger(ProductAdFormValidator.class);

	private static final String DATE_FORMAT = "yyyy/MM/dd";

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(ProductAdForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {

		ProductAdForm form = (ProductAdForm) target;
		if (StringUtils.equalsIgnoreCase("add", form.getMethod())
				|| StringUtils.equalsIgnoreCase("modify", form.getMethod())) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "productId", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", "validate.empty.message");
//			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "type", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "publishEDate", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "publishSDate", "validate.empty.message");

			if (errors.hasErrors()) {
				return;
			}
			
			if(form.getPublishEDate().getTime()<=form.getPublishSDate().getTime()){
				errors.rejectValue("publishEDate", "publish.end.date.early");
			}
		}
	}

}
