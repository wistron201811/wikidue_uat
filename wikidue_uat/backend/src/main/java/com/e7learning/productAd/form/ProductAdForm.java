package com.e7learning.productAd.form;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.e7learning.common.form.BaseForm;
import com.e7learning.repository.model.ProductAd;

public class ProductAdForm extends BaseForm<ProductAd>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer productAdId;

	private String productId;
	
	private String productName;
	
	private String productImageId;
	
	private String description;

	private String type;
	
	/** The publish E date. */
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date publishEDate;

	/** The publish S date. */
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date publishSDate;
	
	private String active;

	public Integer getProductAdId() {
		return productAdId;
	}

	public void setProductAdId(Integer productAdId) {
		this.productAdId = productAdId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getPublishEDate() {
		return publishEDate;
	}

	public void setPublishEDate(Date publishEDate) {
		this.publishEDate = publishEDate;
	}

	public Date getPublishSDate() {
		return publishSDate;
	}

	public void setPublishSDate(Date publishSDate) {
		this.publishSDate = publishSDate;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getProductImageId() {
		return productImageId;
	}

	public void setProductImageId(String productImageId) {
		this.productImageId = productImageId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
