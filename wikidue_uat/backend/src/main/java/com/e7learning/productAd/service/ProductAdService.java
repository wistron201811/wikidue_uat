package com.e7learning.productAd.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.course.service.CourseService;
import com.e7learning.productAd.form.ProductAdForm;
import com.e7learning.repository.ProductAdRepository;
import com.e7learning.repository.ProductRepository;
import com.e7learning.repository.model.Product;
import com.e7learning.repository.model.ProductAd;

@Service
public class ProductAdService extends BaseService {

	private static final Logger LOG = Logger.getLogger(CourseService.class);
	@Autowired
	private MessageSource msg;

	@Autowired
	private ConfigService configService;

	@Autowired
	private ProductAdRepository productAdRepository;

	@Autowired
	private ProductRepository productRepository;

	public Page<Product> queryProduct(int pageNo) throws ServiceException {
		try {
			Pageable pageable = new PageRequest(pageNo - 1, configService.getPageSize());
			Specification<Product> spec = (Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
				List<Predicate> predicates = new ArrayList<>();
				Path<String> np = root.get("active");
				predicates.add(cb.equal(np, "1"));
				predicates.add(cb.greaterThanOrEqualTo(root.get("publishEDate"), new LocalDate().toDate()));
				return cb.and(predicates.toArray(new Predicate[predicates.size()]));
			};
			return productRepository.findAll(spec, pageable);
		} catch (Exception e) {
			LOG.error("exception", e);
			throw new ServiceException(msg.getMessage("product.query.error", null, null));
		}
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void addProductAd(ProductAdForm form) throws ServiceException {

		try {
			Product product = productRepository.findOne(form.getProductId());
			if (product == null) {
				throw new ServiceException(msg.getMessage("product.query.none", null, null));
			}

			Date now = new Date();
			ProductAd productAd = new ProductAd();
			productAd.setActive(form.getActive());
			productAd.setCreateDate(now);
			productAd.setCreateId(getCurrentUserId());
			productAd.setProduct(product);
			productAd.setDescription(form.getDescription());
			productAd.setPublishStartDate(form.getPublishSDate());
			productAd.setPublishEndDate(form.getPublishEDate());
//			productAd.setType(form.getType());
			productAd.setUpdateDate(now);

			productAdRepository.save(productAd);
		} catch (ServiceException e) {
			LOG.error("exception", e);
			throw e;
		}  catch (Exception e) {
			LOG.error("addProductAd", e);
			throw new ServiceException(msg.getMessage("productAd.create.error", null, null));
		}

		throw new MessageException(msg.getMessage("productAd.create.success", null, null));
	}

	public Page<ProductAd> query(ProductAdForm form, int pageNo) throws ServiceException {
		try {
			Pageable pageable = new PageRequest(pageNo - 1, configService.getPageSize());
			return productAdRepository.findAll(buildOperSpecification(form), pageable);
		} catch (Exception e) {
			LOG.error("exception", e);
			throw new ServiceException(msg.getMessage("productAd.query.error", null, null));
		}
	}

	public ProductAdForm getEditForm(Integer productAdId) throws ServiceException {
		try {
			ProductAd productAd = productAdRepository.findOne(productAdId);
			if (productAd == null) {
				throw new ServiceException(msg.getMessage("productAd.query.error", null, null));
			}
			ProductAdForm form = new ProductAdForm();
			form.setActive(productAd.getActive());
			form.setDescription(productAd.getDescription());
			form.setProductAdId(productAdId);
			form.setProductId(productAd.getProduct() != null ? productAd.getProduct().getPkProduct() : null);
			form.setProductImageId(productAd.getProduct() != null ? productAd.getProduct().getImageId() : null);
			form.setProductName(productAd.getProduct() != null ? productAd.getProduct().getProductName() : null);
			form.setPublishEDate(productAd.getPublishEndDate());
			form.setPublishSDate(productAd.getPublishStartDate());
//			form.setType(productAd.getType());
			return form;
		} catch (ServiceException e) {
			LOG.error("exception", e);
			throw e;
		}  catch (Exception e) {
			LOG.error("exception", e);
			throw new ServiceException(msg.getMessage("productAd.query.error", null, null));
		}
	}

	public void modifyProductAd(ProductAdForm form) throws ServiceException {
		try {
			ProductAd productAd = productAdRepository.findOne(form.getProductAdId());
			if (productAd == null) {
				throw new ServiceException(msg.getMessage("productAd.modify.error", null, null));
			}
			Product product = productRepository.findOne(form.getProductId());
			if (product == null) {
				throw new ServiceException(msg.getMessage("product.query.none", null, null));
			}

			Date now = new Date();
			productAd.setActive(form.getActive());
			productAd.setProduct(product);
			productAd.setDescription(form.getDescription());
			productAd.setPublishStartDate(form.getPublishSDate());
			productAd.setPublishEndDate(form.getPublishEDate());
//			productAd.setType(form.getType());
			productAd.setUpdateDate(now);

			productAdRepository.save(productAd);

		} catch (ServiceException e) {
			LOG.error("exception", e);
			throw e;
		}  catch (Exception e) {
			LOG.error("exception", e);
			throw new ServiceException(msg.getMessage("productAd.modify.error", null, null));
		}
		throw new MessageException(msg.getMessage("productAd.modify.success", null, null));
	}

	public void removeProductAd(Integer productAdId) throws ServiceException {
		try {
			if (productAdId == null) {
				throw new ServiceException(msg.getMessage("productAd.remove.error", null, null));
			}
			productAdRepository.delete(productAdId);

		} catch (ServiceException e) {
			LOG.error("exception", e);
			throw e;
		} catch (Exception e) {
			LOG.error("exception", e);
			throw new ServiceException(msg.getMessage("productAd.remove.error", null, null));
		}
		throw new MessageException(msg.getMessage("productAd.remove.success", null, null));
	}

	private Specification<ProductAd> buildOperSpecification(ProductAdForm form) {
		return (Root<ProductAd> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {

			List<Predicate> predicates = new ArrayList<>();
			if (!StringUtils.isEmpty(form.getProductName())) {
				Join<ProductAd, Product> product = root.join("product");
				predicates.add(cb.like(product.<String>get("productName"), "%" + form.getProductName() + "%"));
			}
			if (!StringUtils.isEmpty(form.getType())) {
				Path<String> np = root.get("type");
				predicates.add(cb.equal(np, form.getType()));
			}

			if (form.getPublishSDate() != null) {
				Path<Date> np = root.get("publishSDate");
				predicates.add(cb.greaterThanOrEqualTo(np, form.getPublishSDate()));
			}

			if (form.getPublishEDate() != null) {
				Path<Date> np = root.get("publishEDate");
				predicates.add(cb.lessThanOrEqualTo(np, form.getPublishEDate()));
			}
			if (!predicates.isEmpty()) {
				return cb.and(predicates.toArray(new Predicate[predicates.size()]));
			}
			return cb.conjunction();

		};
	}
}
