package com.e7learning.productAd.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.e7learning.common.controller.BaseController;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.productAd.form.ProductAdForm;
import com.e7learning.productAd.service.ProductAdService;
import com.e7learning.productAd.validation.ProductAdFormValidator;
import com.e7learning.repository.model.Product;

@Controller
@RequestMapping(value = "/productAd")
@SessionAttributes(ProductAdController.FORM)
public class ProductAdController extends BaseController {

	private static final Logger LOG = Logger.getLogger(ProductAdController.class);

	private static final String ADD_PRODUCTAD_PAGE = "addProductAd";

	private static final String QUERY_PRODUCTAD_PAGE = "queryProductAd";

	private static final String MODIFY_PRODUCTAD_PAGE = "modifyProductAd";

	private static final String CHOOSE_PRODUCT_PAGE = "chooseProduct";

	public static final String FORM = "productAdForm";

	@Autowired
	private ProductAdService productAdService;
	@Autowired
	private ProductAdFormValidator productAdFormValidation;

	@InitBinder(FORM)
	protected void initBinder(WebDataBinder binder, HttpSession session) {
		binder.setValidator(productAdFormValidation);
	}

	@RequestMapping(value = "/add", method = { RequestMethod.GET })
	public String goAdd(Model model) {
		model.addAttribute(FORM, new ProductAdForm());
		return ADD_PRODUCTAD_PAGE;
	}
	
	@RequestMapping(value = "/queryProduct", params = "method=query")
	public String queryProduct(Model model, ProductAdForm form, HttpServletRequest request) {
		try {
			String pageName = new ParamEncoder("product").encodeParameterName(TableTagParameters.PARAMETER_PAGE);
			int nextPage = getPage(request.getParameter(pageName));
			Page<Product> queryResult = productAdService.queryProduct(nextPage);
			model.addAttribute("queryResult", queryResult);

		} catch (Exception e) {
		}
		return CHOOSE_PRODUCT_PAGE;
	}

	@RequestMapping(params = "method=add", value = "/add", method = { RequestMethod.POST })
	public String add(Model model, @Validated @ModelAttribute(FORM) ProductAdForm form, BindingResult bindingResult) {
		try {
			if (!bindingResult.hasErrors()) {
				productAdService.addProductAd(form);
				return "redirect:/productAd/query";
			}
		} catch (Exception e) {
		}

		return ADD_PRODUCTAD_PAGE;
	}

	@RequestMapping(value = "/query", method = { RequestMethod.GET })
	public String goQuery(Model model) {
		ProductAdForm form = new ProductAdForm();
		try {
			form.setResult(productAdService.query(form, 1));
		} catch (ServiceException e) {

		}
		model.addAttribute(FORM, form);
		return QUERY_PRODUCTAD_PAGE;
	}

	@RequestMapping(value = "/query", params = "method=query")
	public String query(Model model, @Validated @ModelAttribute(FORM) ProductAdForm form, HttpServletRequest request,
			BindingResult bindingResult) {
		try {
			if (!bindingResult.hasErrors()) {
				String pageName = new ParamEncoder("productAd").encodeParameterName(TableTagParameters.PARAMETER_PAGE);
				int nextPage = getPage(request.getParameter(pageName));
				form.setResult(productAdService.query(form, nextPage));
			}
		} catch (Exception e) {
		}
		return QUERY_PRODUCTAD_PAGE;
	}

	@RequestMapping(value = "/modify", method = { RequestMethod.POST })
	public String goModify(Model model, ProductAdForm form) {
		try {
			model.addAttribute(FORM, productAdService.getEditForm(form.getProductAdId()));
		} catch (Exception e) {
			return "redirect:/productAd/query";
		}

		return MODIFY_PRODUCTAD_PAGE;
	}

	@RequestMapping(params = "method=modify", value = "/modify", method = { RequestMethod.POST })
	public String modify(@Validated ProductAdForm form, BindingResult result) {

		try {
			if (!result.hasErrors()) {
				productAdService.modifyProductAd(form);
				return "redirect:/productAd/query";
			}
		} catch (ServiceException e) {

		}
		return MODIFY_PRODUCTAD_PAGE;
	}

	@RequestMapping(params = "method=remove", value = "/query", method = { RequestMethod.POST })
	public String remove(ProductAdForm form) {
		try {
			productAdService.removeProductAd(form.getProductAdId());

		} catch (ServiceException e) {

		}
		return "redirect:/productAd/query";
	}

}
