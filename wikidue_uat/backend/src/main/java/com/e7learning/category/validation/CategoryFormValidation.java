package com.e7learning.category.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.e7learning.category.form.CategoryForm;

@Component
public class CategoryFormValidation implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.equals(CategoryForm.class);
	}

	@Override
	public void validate(Object target, Errors errors) {
		CategoryForm form = (CategoryForm) target;
		if (StringUtils.equalsIgnoreCase("save", form.getMethod())
				|| StringUtils.equalsIgnoreCase("modify", form.getMethod())) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "categoryName", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "categoryCode", "validate.empty.message");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "categoryType", "validate.empty.message");
		}
		if (errors.hasErrors()) {
			return;
		}
	}
}