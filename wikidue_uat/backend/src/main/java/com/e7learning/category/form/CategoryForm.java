package com.e7learning.category.form;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.e7learning.common.enums.CategoryTypeEnum;
import com.e7learning.common.form.BaseForm;
import com.e7learning.repository.model.Category;

/**
 * @author J
 * 
 * 分類管理
 *
 */
public class CategoryForm extends BaseForm<Category>{
	/**
	 * 分類PK
	 */
	private int pkCategory;
	/**
	 * 分類編號
	 */
	private String categoryCode;
	/**
	 * 分類名稱
	 */
	private String categoryName;
	/**
	 * 分類類型
	 */
	private String categoryType;
	/**
	 * 啟用
	 */
	private String  active;
	/**
	 * 分類類型選單
	 */
	private List<CategoryTypeEnum> categoryTypeList;
	
	public CategoryForm() {
		
	}
	public int getPkCategory() {
		return pkCategory;
	}
	public void setPkCategory(int pkCategory) {
		this.pkCategory = pkCategory;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public List<CategoryTypeEnum> getCategoryTypeList() {
		categoryTypeList = new ArrayList<CategoryTypeEnum>( Arrays.asList(CategoryTypeEnum.values() ));
		return categoryTypeList;
	}
}
