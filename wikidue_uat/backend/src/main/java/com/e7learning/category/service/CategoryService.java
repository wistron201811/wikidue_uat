package com.e7learning.category.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.api.domain.param.CategoryQueryParam;
import com.e7learning.api.domain.result.CategoryResult;
import com.e7learning.category.form.CategoryForm;
import com.e7learning.common.Constant;
import com.e7learning.common.enums.CategoryTypeEnum;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.RestException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.repository.CampRepository;
import com.e7learning.repository.CategoryRepository;
import com.e7learning.repository.ProductTagRepository;
import com.e7learning.repository.TagRepository;
import com.e7learning.repository.model.Camp;
import com.e7learning.repository.model.Category;
import com.e7learning.repository.model.Tag;

/**
 * @author J
 *
 */
@Service
public class CategoryService extends BaseService {

	private static final Logger log = Logger.getLogger(CategoryService.class);

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private ConfigService configService;

	@Autowired
	private MessageSource resources;

	@Autowired
	private CampRepository campRepository;

	@Autowired
	private TagRepository tagRepository;

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void addCategory(CategoryForm form) throws ServiceException {
		try {
			Category category = new Category();
			BeanUtils.copyProperties(category, form);
			category.setUpdateDt(new Date());
			category.setCreateDt(new Date());
			category.setCreateId(getCurrentUserId());
			category.setLastModifiedId(getCurrentUserId());
			categoryRepository.save(category);
		} catch (Exception e) {
			log.error("addCategory", e);
			throw new ServiceException(resources.getMessage("op.add.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.add.success", null, null));
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void saveCategory(CategoryForm form) throws ServiceException {
		try {
			Category category = findByPkCategory(form.getPkCategory());
			if (category != null) {
				BeanUtils.copyProperties(category, form);
				category.setUpdateDt(new Date());
				category.setLastModifiedId(getCurrentUserId());
			}
		} catch (Exception e) {
			log.error("editCategory", e);
			throw new ServiceException(resources.getMessage("op.modify.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.modify.success", null, null));
	}

	public Page<Category> queryCategory(CategoryForm form, Integer page) throws ServiceException {
		try {
			Sort sort = new Sort(Direction.ASC, "categoryCode");
			Pageable pageable = new PageRequest(page - 1, configService.getPageSize(), sort);
			Page<Category> result = categoryRepository.findAll(buildOperSpecification(form), pageable);
			return result;
		} catch (Exception e) {
			log.error("queryCategory", e);
			throw new ServiceException(resources.getMessage("op.query.error", null, null));
		}
	}

	public Category findByPkCategory(int pkCategory) {
		return categoryRepository.findByPkCategory(pkCategory);
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void remove(int id) throws ServiceException {
		String result = this.validateBeforeRemove(id);
		if (StringUtils.isNoneEmpty(result)) {
			throw new ServiceException(result);
		}
		try {
			categoryRepository.delete(id);
		} catch (Exception e) {
			log.error("removeCategory", e);
			throw new ServiceException(resources.getMessage("op.del.error", null, null));
		}
		throw new MessageException(resources.getMessage("op.del.success", null, null));
	}

	private String validateBeforeRemove(int id) {
		String result = "";
		for (CategoryTypeEnum e : CategoryTypeEnum.values()) {
			if (StringUtils.equalsIgnoreCase(e.name(), CategoryTypeEnum.CAMP.name())) {
				List<Camp> list = campRepository.findByFkCategory(id);
				if (list != null && !list.isEmpty()) {
					result = resources.getMessage("camp.category.exist", null, null);
				}
			}
			if (StringUtils.isBlank(result)) {
				List<Tag> list = tagRepository.findByFkCategory(id);
				if (list != null && !list.isEmpty()) {
					result = resources.getMessage("product.category.exist", null, null);
				}
			}
		}
		return result;
	}

	public List<CategoryResult> queryCategory(CategoryQueryParam param) throws RestException {
		List<CategoryResult> result = new ArrayList<>();

		try {
			List<Category> categorys = categoryRepository.findByCategoryTypeOrderByCreateDtDesc(param.getCategoryType())
					.stream().filter(v -> Constant.TRUE.equals(v.getActive())).collect(Collectors.toList());
			categorys.stream().forEach(v -> result.add(toCategoryResult(v)));
		} catch (Exception e) {
			log.error("queryCategory", e);
			throw new RestException(resources.getMessage("category.query.err", null, null));
		}

		return result;
	}

	private CategoryResult toCategoryResult(Category category) {
		CategoryResult result = new CategoryResult();
		result.setCategoryCode(category.getCategoryCode());
		result.setCategoryId(category.getPkCategory());
		result.setCategoryName(category.getCategoryName());
		result.setCategoryType(category.getCategoryType());
		return result;
	}

	private Specification<Category> buildOperSpecification(CategoryForm form) {
		return new Specification<Category>() {
			@Override
			public Predicate toPredicate(Root<Category> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

				List<Predicate> predicates = new ArrayList<>();

				if (StringUtils.isNotEmpty(form.getCategoryName())) {
					Path<String> np = root.get("categoryName");
					predicates.add(cb.like(np, "%" + form.getCategoryName() + "%"));
				}
				if (StringUtils.isNotEmpty(form.getCategoryCode())) {
					Path<String> np = root.get("categoryCode");
					predicates.add(cb.like(np, "%" + form.getCategoryCode() + "%"));
				}
				if (StringUtils.isNotEmpty(form.getCategoryType())) {
					Path<String> np = root.get("categoryType");
					predicates.add(cb.equal(np, form.getCategoryType()));
				}
				if (!predicates.isEmpty()) {
					return cb.and(predicates.toArray(new Predicate[predicates.size()]));
				}
				return cb.conjunction();
			}
		};
	}
}
