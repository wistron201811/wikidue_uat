package com.e7learning.category.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.e7learning.category.form.CategoryForm;
import com.e7learning.category.service.CategoryService;
import com.e7learning.category.validation.CategoryFormValidation;
import com.e7learning.common.controller.BaseController;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.repository.model.Category;

/**
 * @author J
 *
 */
@Controller
@RequestMapping(value = "/category")
@SessionAttributes(CategoryController.FORM_BEAN_NAME)
public class CategoryController extends BaseController {

	private static final Logger log = Logger.getLogger(CategoryController.class);

	public static final String FORM_BEAN_NAME = "categoryForm";
	
	private static final String QUERY_PAGE = "queryCategoryPage";

	private static final String ADD_PAGE = "addCategoryPage";
	
	private static final String EDIT_PAGE = "editCategoryPage";

	@Autowired
	private CategoryFormValidation categoryFormValidation;
	@Autowired
	private CategoryService categoryService ;
	
	@InitBinder(CategoryController.FORM_BEAN_NAME)
	protected void initBinder(WebDataBinder binder, HttpSession session) {
		binder.setValidator(categoryFormValidation);
	}
	
	@RequestMapping(value = "/query", method = { RequestMethod.GET })
	public String goQuery(Model model) {
		CategoryForm form = new CategoryForm();
		try {
			form.setResult(categoryService.queryCategory(form, 1));
		} catch (ServiceException e) {

		}
		model.addAttribute(FORM_BEAN_NAME, form);
		return QUERY_PAGE;
	}

	@RequestMapping(value = "/query", params = "method=query")
	public String query(HttpServletRequest request, CategoryForm form, Model model) {
		String pageName = new ParamEncoder("list").encodeParameterName(TableTagParameters.PARAMETER_PAGE);
		int nextPage = getPage(request.getParameter(pageName));
		try {
			form.setResult(categoryService.queryCategory(form, nextPage));
		} catch (ServiceException e) {

		}
		return QUERY_PAGE;
	}
	
	@RequestMapping(value = "/add", method = { RequestMethod.GET })
	public String goAdd(Model model) {
		CategoryForm form = new CategoryForm();
		model.addAttribute(FORM_BEAN_NAME, form);
		return ADD_PAGE;
	}

	@RequestMapping(params = "method=save", value = "/add", method = { RequestMethod.POST })
	public String add(@Validated CategoryForm form, BindingResult result) {
		if (result.hasErrors()) {
			return ADD_PAGE;
		}
		try {
			categoryService.addCategory(form);
			return "redirect:/category/query?method=query";
		} catch (ServiceException e) {
			
		}
		return QUERY_PAGE;
	}
	
	@RequestMapping(params = "method=remove", value = "/remove", method = { RequestMethod.POST })
	public String remove(CategoryForm form) {
		if(form.getPkCategory() > 0) {
			Category category = categoryService.findByPkCategory(form.getPkCategory());
			if(category != null) {
				try {
					categoryService.remove(category.getPkCategory());
					return "redirect:/category/query?method=query";
				} catch (ServiceException e) {
				
				}
			}
		}
		return QUERY_PAGE;
	}
	
	@RequestMapping(value = "/modify", method = { RequestMethod.POST })
	public String goModify(Model model, CategoryForm form) {
		if(form.getPkCategory() > 0) {
			Category category = categoryService.findByPkCategory(form.getPkCategory());
			try {
				BeanUtils.copyProperties(form, category);
			} catch (Exception e) {
				e.printStackTrace();
			}
			model.addAttribute(FORM_BEAN_NAME, form);
		}
		return EDIT_PAGE;
	}
	
	@RequestMapping(params = "method=modify", value = "/modify", method = { RequestMethod.POST })
	public String modify(@Validated CategoryForm form, BindingResult result) {
		if (result.hasErrors()) {
			return EDIT_PAGE;
		}
		try {
			categoryService.saveCategory(form);
			return "redirect:/category/query?method=query";
		} catch (ServiceException e) {
			
		}
		return EDIT_PAGE;
	}
}
