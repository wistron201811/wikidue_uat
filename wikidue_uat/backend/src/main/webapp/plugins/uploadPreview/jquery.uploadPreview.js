(function ($) {
  $.extend({
    uploadPreview: function (options) {

      // Options + Defaults
      var settings = $.extend({
        input_field: ".image-input",
        preview_box: ".image-preview",
        label_field: ".image-label",
        label_default: "選擇圖片",
        label_selected: "更換圖片",
        height: "",
        width: "",
        range_height: [],
        no_label: false,
        check_aspect_ratio: true,
        check_size: true,
        success_callback: null,
      }, options);

      // Check if FileReader is available
      if (window.File && window.FileList && window.FileReader) {
        var _URL = window.URL;
        if (typeof ($(settings.input_field)) !== 'undefined' && $(settings.input_field) !== null) {
          $(settings.input_field).change(function () {
            var files = this.files;

            if (files.length > 0) {
              var file = files[0];
              var reader = new FileReader();

              // Load file
              reader.addEventListener("load", function (event) {
                var loadedFile = event.target;
                // Check format
                if (file.type.match('image')) {
                  var setting_width = parseInt(settings.width.replace('px', ''));
                  var setting_height = parseInt(settings.height.replace('px', ''));
                  // 檢查長寬比
                  if (settings.check_aspect_ratio) {
                    var img = new Image();
                    img.onload = function () {
                      if (Math.round(setting_width / setting_height * 100) / 100 !=
                        Math.round(this.width / this.height * 100) / 100) {
                        alert("長寬比需為" + setting_width + "x" + setting_height);
                        $(settings.input_field).val('');
                        return false;
                      } else if (settings.check_size && setting_width > this.width) {
                        alert("不可小於建議尺寸：" + setting_width + "x" + setting_height);
                        $(settings.input_field).val('');
                      } else {
                        previewImg();
                      }
                    };
                    img.src = _URL.createObjectURL(file);
                  } else if (settings.check_size) {
                    // 需大於建議尺寸
                    var img = new Image();
                    img.onload = function () {
                    	 if (setting_width > this.width) {
                        alert("影像寬度為："+this.width+"；須大於建議尺寸：" + setting_width);
                        $(settings.input_field).val('');
                        return false;
                      } else if (setting_height > this.height) {
                        alert("影像高度為："+this.height+"；須大於建議尺寸：" + setting_height);
                        $(settings.input_field).val('');
                      } else if (settings.range_height.length && (settings.range_height[0] > this.height || settings.range_height[1] < this.height)) {
                        alert("影像高度為："+this.height+"；須介於尺寸：" + settings.range_height[0] + "~" + settings.range_height[1]);
                      } else {
                        previewImg();
                      }
                    };
                    img.src = _URL.createObjectURL(file);

                  }
                  var previewImg = function () {
                    // Image
                    $(settings.preview_box).css("background-image", "url(" + loadedFile.result + ")");
                    $(settings.preview_box).css("background-size", "contain");
                    $(settings.preview_box).css("background-position", "center center");
                    $(settings.preview_box).css("background-repeat", "no-repeat");

                    if (settings.input_field.indexOf('#tmp_') == 0) {
                      $(settings.input_field).attr('name', settings.input_field.substring(5));
                    }

                    //var a = $('#_imageOneFile').clone();a.attr('name','imageOneFile');$('#_imageOneFile').before(a);

                  }

                // } else if (file.type.match('audio')) {
                //   // Audio
                //   $(settings.preview_box).html("<audio controls><source src='" + loadedFile.result + "' type='" + file.type + "' />Your browser does not support the audio element.</audio>");
                } else {
                  alert("This file type is not supported yet.");
                  $(settings.input_field).val('');
                }
              });

              if (settings.no_label == false) {
                // Change label
                $(settings.label_field).html(settings.label_selected);
              }

              // Read the file
              reader.readAsDataURL(file);

              // Success callback function call
              if (settings.success_callback) {
                settings.success_callback();
              }

            } else {
              if (settings.no_label == false) {
                // Change label
                $(settings.label_field).html(settings.label_default);
              }

              // Clear background
              $(settings.preview_box).css("background-image", "none");

              // Remove Audio
              $(settings.preview_box + " audio").remove();
            }
          });
        }
      } else {
        alert("You need a browser with file reader support, to use this form properly.");
        return false;
      }
    }
  });
})(jQuery);
