<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
	$(function() {
		$('.datepicker').datepicker({
			format : 'yyyy/mm/dd',
			autoclose : true,
			todayBtn : true,
			todayHighlight : true
		});

		cancelPop();
	});

	function query() {
		$("#method").val("query");
		$.LoadingOverlay("show");
		setTimeout(function() {
			$("#campForm").submit();
		}, 500);
	}

	function remove(pkCamp) {
		if (confirm('確認刪除？')) {
			$("#pkCamp").val(pkCamp);
			document.campForm.action = '${pageContext.request.contextPath}/camp/remove';
			$("#method").val("remove");
			$.LoadingOverlay("show");
			setTimeout(function() {
				$("#campForm").submit();
			}, 500);
		}
	}
	function goModify(pkCamp) {
		$("#pkCamp").val(pkCamp);
		document.campForm.action = '${pageContext.request.contextPath}/camp/modify';
		document.campForm.campForm_cId.value="";
		$("#method").val("");
		$.LoadingOverlay("show");
		setTimeout(function() {
			$("#campForm").submit();
		}, 500);
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="campForm" name="campForm"
				action="${pageContext.request.contextPath}/camp/query#result"
				class="form-horizontal">
				<sessionConversation:insertSessionConversationId
					attributeName="campForm" />
				<form:hidden path="method" />
				<form:hidden path="pkCamp" value="" />
				<div class="box-body">
					<div class="form-group">
						<label for="campNum" class="col-sm-2 control-label">營隊編號</label>
						<div class="col-sm-10">
							<form:input path="campNum" class="form-control"
								placeholder="營隊編號" maxlength="30" />
							<form:errors path="campNum" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="campName" class="col-sm-2 control-label">營隊名稱</label>
						<div class="col-sm-10">
							<form:input path="campName" class="form-control"
								placeholder="營隊名稱" maxlength="50" />
							<form:errors path="campName" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="campType" class="col-sm-2 control-label">營隊分類</label>
						<div class="col-sm-10">
							<form:select path="campType" cssClass="form-control">
								<form:option value="">請選擇</form:option>
								<c:forEach items="${campForm.categoryList}" var="category">
									<form:option value="${category.pkCategory}">${category.categoryName}</form:option>
								</c:forEach>
							</form:select>
						</div>
					</div>
					<div class="form-group">
						<label for="signUpEndDate" class="col-sm-2 control-label">報名截止日(起)</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#signUpEndDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="signUpEndDate"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="signUpEndDate" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="signUpEndDateEnd" class="col-sm-2 control-label">報名截止日(迄)</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#signUpEndDateEnd').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="signUpEndDateEnd"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="signUpEndDateEnd" class="text-red" />
							</div>
						</div>
					</div>
					<button type="button" onclick="query()"
						class="btn btn-default pull-right">查詢</button>
				</div>
				<!-- /.box-body -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->

		<c:if test="${not empty campForm.result}">
			<div class="box" id="resultList">
				<div class="box-header">
					<a href="#" name="result"></a>
					<h3 class="box-title">查詢結果</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="dataTables_wrapper form-inline dt-bootstrap">
						<display:table name="campForm.result.content"
							pagesize="${campForm.result.size}"
							requestURI='?method=query&campForm_cId=${curr_campForm_cId}#result'
							id="list" cellspacing="0" cellpadding="0"
							class="table table-bordered table-hover" partialList="true"
							size="${campForm.result.totalElements.intValue()}"
							excludedParams="*">
							<display:column title='營隊編號' property="campNum" sortable="true" />
							<display:column title='營隊名稱' property="campName" sortable="true" />
							<display:column title='定價' sortable="true">
								<p style="text-align: right">
									<fmt:formatNumber value="${list.price}" type="number" />
								</p>
							</display:column>
							<display:column title='優專價' sortable="true">
								<p style="text-align: right">
									<fmt:formatNumber value="${list.discountPrice}" type="number" />
								</p>
							</display:column>
							<display:column title='報名截止日' sortable="true">
								<fmt:formatDate pattern="yyyy-MM-dd"
									value="${list.signUpEndDate}" />
							</display:column>
							<display:column title='活動地點' property="activityCounty"
								sortable="true" />
							<!-- 有修改權限才顯示 -->
							<sec:authorize url="/camp/modify">
								<display:column title='操作'>
									<button type="button" onclick="goModify('${list.pkCamp}');"
										class="btn btn-success btn-sm">修改</button>
									<button type="button" onclick="remove('${list.pkCamp}');"
										class="btn btn-danger btn-sm">刪除</button>
								</display:column>
							</sec:authorize>
						</display:table>
					</div>
				</div>
			</div>
		</c:if>
	</div>
</div>
