<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
	
<link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/datetimepicker/bootstrap-datetimepicker.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/datetimepicker/bootstrap-datetimepicker.min.css">
<!-- 預覽上傳的圖片 -->
<script
	src="${pageContext.request.contextPath}/plugins/uploadPreview/jquery.uploadPreview.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>

<script
	src="${pageContext.request.contextPath}/plugins/datetimepicker/bootstrap-datetimepicker.js"></script>
<script
	src="${pageContext.request.contextPath}/plugins/datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
	$(function() {
		
		$(".form_datetime").datetimepicker({
	        format: "yyyy/mm/dd",
	        autoclose: true,
	        todayHighlight : true,
	        todayBtn: true
	    });
		
		$('.datepicker').datepicker({
			format : 'yyyy/mm/dd',
			autoclose : true,
			todayBtn : true,
			todayHighlight : true
		});
	
		$('#twzipcode2').twzipcode({
			'countyName' : 'activityCounty' // 自訂城市 select 標籤的 name 值

		});

		$('#twzipcode2 select').addClass("form-control input-sm twzip-group");

		//init
		fillAddress();
	});

	//鄉鎮預設
	function fillAddress() {
		if ('${campForm.activityCounty}' != '') {
			$('#twzipcode2').find('select[name="activityCounty"]').val('${campForm.activityCounty}').prop('selected', true).trigger('change');
		}
	}

	function addCamp() {
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#campForm").submit();
        }, 500);
	}
	function goQuery() {
		document.campForm.action = '${pageContext.request.contextPath}/camp/query';
		$("#method").val("query");
		$("#campName").val("");
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#campForm").submit();
        }, 500);
	}
</script>


<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="campForm" name="campForm"
				action="${pageContext.request.contextPath}/camp/add" method="post"
				enctype="multipart/form-data" class="form-horizontal">
				<sessionConversation:insertSessionConversationId
					attributeName="campForm" />
				<form:hidden path="method" value="save" />
				<div class="box-body">
					<div class="form-group">
						<label for="campNum" class="col-sm-2 control-label">*營隊編號</label>
						<div class="col-sm-10">
							<form:input path="campNum" class="form-control"
								placeholder="營隊編號" maxlength="30" />
							<form:errors path="campNum" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="campName" class="col-sm-2 control-label">*營隊名稱</label>
						<div class="col-sm-10">
							<form:input path="campName" class="form-control"
								placeholder="營隊名稱" maxlength="50" />
							<form:errors path="campName" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="campType" class="col-sm-2 control-label">*營隊分類</label>
						<div class="col-sm-10">
							<form:select path="campType" cssClass="form-control">
								<form:option value="">請選擇</form:option>
								<c:forEach items="${campForm.categoryList}" var="category">
									<form:option value="${category.pkCategory}">${category.categoryName}</form:option>
								</c:forEach>
							</form:select>
							<form:errors path="campType" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="price" class="col-sm-2 control-label">*原價</label>
						<div class="col-sm-10">
							<form:input path="price" class="form-control" maxlength="8"
								placeholder="原價" />
							<form:errors path="price" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="discountPrice" class="col-sm-2 control-label">*優惠價</label>
						<div class="col-sm-10">
							<form:input path="discountPrice" class="form-control"
								maxlength="8" placeholder="優惠價" />
							<form:errors path="discountPrice" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="photo" class="col-sm-2 control-label">*照片<br>(建議尺寸700x430或等比例<br>但不得小於建議尺寸)</label>
						<div class="col-sm-10">
							<e7:previewImage attribute="photo" width="700px" height="430px" 
								fileStorage="${campForm.photo}" />
							<form:errors path="photo" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="campIntroduction" class="col-sm-2 control-label">*營隊說明</label>
						<div class="col-sm-10">
							<form:textarea path="campIntroduction" placeholder="營隊說明"
								class="form-control" maxlength="300" />
							<form:errors path="campIntroduction" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="externalLink" class="col-sm-2 control-label">*活動網址</label>
						<div class="col-sm-10">
							<form:input path="externalLink" class="form-control"
								placeholder="活動網址" maxlength="100" />
							<form:errors path="externalLink" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="attendantTarget" class="col-sm-2 control-label">*招生對象</label>
						<div class="col-sm-10">
							<form:input path="attendantTarget" class="form-control"
								placeholder="招生對象" maxlength="50" />
							<form:errors path="attendantTarget" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="organizer" class="col-sm-2 control-label">*主辦單位</label>
						<div class="col-sm-10">
							<form:input path="organizer" class="form-control"
								placeholder="主辦單位" maxlength="50" />
							<form:errors path="organizer" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="activityCounty" class="col-sm-2 control-label">*活動地點</label>
						<div class="col-sm-10">
							<div id="twzipcode2">
								<div data-role="activityCounty"></div>
								<div data-role="district" data-style="hide"></div>
								<div data-role="zipcode" data-style="hide"></div>
							</div>
							<form:errors path="activityCounty" class="text-red" />
						</div>
					</div>
					<div class="form-group date">
						<label for="activitySDate" class="col-sm-2 control-label">*活動開始時間</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#activitySDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="activitySDate"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="activitySDate" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="activityEDate" class="col-sm-2 control-label">*活動結束時間</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#activityEDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="activityEDate"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="activityEDate" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="signUpStartDate" class="col-sm-2 control-label">報名開始日<br>(未輸入代表即日起)</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#signUpStartDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="signUpStartDate"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="signUpEndDate" class="col-sm-2 control-label">*報名截止日</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#signUpEndDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="signUpEndDate"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="signUpEndDate" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="active" class="col-sm-2 control-label">*啟用</label>
						<div class="col-sm-10">
							<form:radiobutton path="active" id="radio1" value="1"
								checked="checked" />
							<label for="radio1">是</label><br />
							<form:radiobutton path="active" id="radio2" value="0" />
							<label for="radio2">否</label>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="button" onclick="addCamp()" class="btn btn-info">儲存</button>
					<a href="${pageContext.request.contextPath}/camp/query#result"
						class="btn btn-default pull-right" onclick="goQuery();">取消</a>
				</div>
				<!-- /.box-footer -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>