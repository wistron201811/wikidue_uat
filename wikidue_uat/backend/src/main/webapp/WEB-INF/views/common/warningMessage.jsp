<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${not empty message}">
	<br/>
	<div class="alert alert-success alert-dismissible">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  <h4><i class="icon fa fa-check"></i> 提示</h4>
	  	${message}
	  	<c:remove var="message" scope="session" />
	</div>
</c:if>
<c:if test="${not empty error_message}">
	<br/>
	<div class="alert alert-warning alert-dismissible">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  <h4><i class="icon fa fa-warning"></i> 提示</h4>
	  	${error_message}
	  	<c:remove var="error_message" scope="session" />
	</div>
</c:if>

