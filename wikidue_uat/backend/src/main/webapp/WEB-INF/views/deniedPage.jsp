<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<div class="error-page">
  <h2 class="headline text-red">401</h2>

  <div class="error-content">
    <h3><i class="fa fa-warning text-red"></i> 權限不足.</h3>
    <p>
      	目前使用的帳號沒有權限存取
    </p>
  </div>
</div>
<!-- /.error-page -->