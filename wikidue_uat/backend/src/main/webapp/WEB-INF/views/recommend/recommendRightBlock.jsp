<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
	
<script type="text/javascript">
	$(function() {
		CKEDITOR.replace('text');
	});
	function modify() {
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#rightForm").submit();
        }, 500);
	}
	function goQuery() {
		$.LoadingOverlay("show");
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form name="rightForm" id="rightForm"
				action="${pageContext.request.contextPath}/recommend/right"
				method="post" class="form-horizontal">
				
				<div class="box-body">
					<div class="form-group">
						<label for="content" class="col-sm-2 control-label">右側區塊內容</label>
						<div class="col-sm-10">
							<textarea id="text" name="text" class="form-control">
								${e7:previewHtmlImg(pageContext.request.contextPath,text)}
							</textarea>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="button" onclick="modify()" class="btn btn-info">儲存</button>
					<a href="${pageContext.request.contextPath}/recommend/query#result"
						class="btn btn-default pull-right" onclick="goQuery();">返回</a>
				</div>
				<!-- /.box-footer -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>