<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<script type="text/javascript">
	function query() {
		$("#method").val("query");
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#recommendForm").submit();
        }, 500);
	}

	function remove(pkRecommend, title) {
		if (confirm('確認刪除['+title+']？')) {
			$("#pkRecommend").val(pkRecommend);
			document.recommendForm.action = '${pageContext.request.contextPath}/recommend/remove';
			$("#method").val("remove");
			$.LoadingOverlay("show");
	        setTimeout(function(){
	        	$("#recommendForm").submit();
	        }, 500);
		}
	}
	function goModify(pkRecommend) {
		document.recommendForm.pkRecommend.value=pkRecommend;
		document.recommendForm.action = '${pageContext.request.contextPath}/recommend/modify';
		document.recommendForm.recommendForm_cId.value="";
		$("#method").val("");
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#recommendForm").submit();
        }, 500);
	} 
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="recommendForm" name="recommendForm"
				action="${pageContext.request.contextPath}/recommend/query#result"
				class="form-horizontal">
				<sessionConversation:insertSessionConversationId
					attributeName="recommendForm" />
				<form:hidden path="pkRecommend"/>
				<form:hidden path="method" />
				<div class="box-body">
					<div class="form-group">
						<label for="propsName" class="col-sm-2 control-label">標題</label>
						<div class="col-sm-10">
							<form:input path="search.title" cssClass="form-control"
								placeholder="文章標題" />
						</div>
					</div>
					<div class="form-group">
						<label for="active" class="col-sm-2 control-label">狀態</label>
						<div class="col-sm-10">
							<form:select path="search.active" cssClass="form-control">
								<form:option value="">不限</form:option>
								<form:option value="1">上架</form:option>
								<form:option value="0">下架</form:option>
							</form:select>
						</div>
					</div>
					<button type="button" onclick="query()" class="btn btn-default pull-right">查詢</button>
				</div>
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->

		<c:if test="${not empty recommendForm.result}">
			<div class="box" id="resultList">
				<div class="box-header">
					<a href="#" name="result"></a>
					<h3 class="box-title">查詢結果</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="dataTables_wrapper form-inline dt-bootstrap">
						<display:table name="recommendForm.result.content"
							pagesize="${recommendForm.result.size}"
							requestURI='?method=query&recommendForm_cId=${curr_recommendForm_cId}#result'
							id="result" cellspacing="0" cellpadding="0"
							class="table table-bordered table-hover" partialList="true"
							size="${recommendForm.result.totalElements.intValue()}"
							excludedParams="*">
							<display:column title='標題' property="title" sortable="true">
							</display:column>
							<display:column title='作者' property="author" sortable="true">
							</display:column>
							<display:column title='狀態' sortable="true">
								<c:if test="${result.active=='1'}">
									<p class="text-success">已上架</p>
								</c:if>
								<c:if test="${result.active=='0'}">
									<p class="text-danger">未上架</p>
								</c:if>
							</display:column>
							<display:column title='上架日期' property="publishSDate" format="{0,date,yyyy/MM/dd}" sortable="true">
							</display:column>
							<display:column title='下架日期' property="publishEDate" format="{0,date,yyyy/MM/dd}" sortable="true">
							</display:column>
							<!-- 有修改權限才顯示 -->
							<%-- <sec:authorize url="/account/modify"> --%>
							<display:column title='操作'>
								<button type="button" onclick="goModify('${result.pkRecommend}');"
									class="btn btn-success btn-sm">修改</button>
								<button type="button" onclick="remove('${result.pkRecommend}','${result.title}');"
									class="btn btn-danger btn-sm">刪除</button>
							</display:column>
							<%-- </sec:authorize> --%>
						</display:table>
					</div>
				</div>
			</div>
		</c:if>
	</div>
</div>
