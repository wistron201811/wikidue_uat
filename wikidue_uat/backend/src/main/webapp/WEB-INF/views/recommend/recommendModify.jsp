<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<script type="text/javascript">
	$(function() {
		$('.datepicker').datepicker({
			format : 'yyyy/mm/dd',
			todayBtn : true,
			todayHighlight : true,
			autoclose : true
		});
		CKEDITOR.replace('content');
	});
	function modify() {
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#recommendForm").submit();
        }, 500);
	}
	function goQuery() {
		$.LoadingOverlay("show");
	}
</script>

<!-- 預覽上傳的圖片 -->
<script
	src="${pageContext.request.contextPath}/plugins/uploadPreview/jquery.uploadPreview.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
	
<form name="backForm" id="backForm"
	action='${pageContext.request.contextPath}/recommend/query'>
	<sessionConversation:insertSessionConversationId
		attributeName="recommendForm" />
	<input type="hidden" name="method" value="query" />
</form>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="recommendForm" name="recommendForm"
				action="${pageContext.request.contextPath}/recommend/modify"
				enctype="multipart/form-data" method="post" class="form-horizontal">
				<form:hidden path="pkRecommend" />
				<form:hidden path="method" value="modify" />
				<sessionConversation:insertSessionConversationId
					attributeName="recommendForm" />
				<div class="box-body">
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">*標題</label>
						<div class="col-sm-10">
							<form:input path="title" class="form-control" placeholder="文章標題"
								maxlength="30" />
							<form:errors path="title" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="author" class="col-sm-2 control-label">*作者</label>
						<div class="col-sm-10">
							<form:input path="author" class="form-control" placeholder="作者"
								maxlength="30" />
							<form:errors path="author" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">*圖片<br>(建議尺寸700x430或等比例<br>但不得小於建議尺寸)</label>
						<div class="col-sm-10">
							<e7:previewImage attribute="imageId" height="430px" width="700px"
								fileStorage="${recommendForm.imageId}" />
							<form:errors path="imageId" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="content" class="col-sm-2 control-label">*文章內容</label>
						<div class="col-sm-10">
							<textarea id="content" name="content" class="form-control">
								${e7:previewHtmlImg(pageContext.request.contextPath,recommendForm.content)}
							</textarea>
							<form:errors path="content" class="text-red" />
						</div>
					</div>
					<div class="form-group date">
						<label for="publishSDate" class="col-sm-2 control-label">*上架日期</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#publishSDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="publishSDate"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="publishSDate" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="publishEDate" class="col-sm-2 control-label">*下架日期</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#publishEDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="publishEDate"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="publishEDate" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="active" class="col-sm-2 control-label">狀態</label>
						<div class="col-sm-10">
							<form:radiobutton path="active" id="radio1" value="1"
								checked="checked" />
							<label for="radio1">上架</label><br />
							<form:radiobutton path="active" id="radio2" value="0" />
							<label for="radio2">下架</label>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="button" onclick="modify()" class="btn btn-info">儲存</button>
					<a href="${pageContext.request.contextPath}/recommend/query#result"
						class="btn btn-default pull-right" onclick="goQuery();">取消</a>
				</div>
				<!-- /.box-footer -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>