<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<script type="text/javascript">
	function query() {
		$("#method").val("query");
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#teacherForm").submit();
        }, 500);
	}

	function remove(pkTeacher) {
		if(confirm('確認刪除？')){
			$("#pkTeacher").val(pkTeacher);
			document.teacherForm.action = '${pageContext.request.contextPath}/teacher/remove';
			$("#method").val("remove");
			$.LoadingOverlay("show");
	        setTimeout(function(){
	        	$("#teacherForm").submit();
	        }, 500);
		}
	}
	function goModify(pkTeacher) {
		$("#pkTeacher").val(pkTeacher);
		document.teacherForm.action = '${pageContext.request.contextPath}/teacher/modify';
		document.teacherForm.teacherForm_cId.value="";
		$("#method").val("");
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#teacherForm").submit();
        }, 500);
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="teacherForm" name="teacherForm"
				action="${pageContext.request.contextPath}/teacher/query#result"
				class="form-horizontal">
				<sessionConversation:insertSessionConversationId attributeName="teacherForm"/>
				<form:hidden path="method" />
				<form:hidden path="pkTeacher" />
				<div class="box-body">
					<div class="form-group">
						<label for="nickname" class="col-sm-2 control-label">講師代號</label>
						<div class="col-sm-10">
						<form:input path="nickname" cssClass="form-control"
							placeholder="講師代號" />
						</div>
					</div>
					<div class="form-group">
						<label for="teacherName" class="col-sm-2 control-label">講師姓名</label>
						<div class="col-sm-10">
						<form:input path="teacherName" cssClass="form-control"
							placeholder="講師姓名" />
						</div>
					</div>
					<div class="form-group">
						<label for="phone" class="col-sm-2 control-label">連絡電話</label>
						<div class="col-sm-10">
							<form:input path="phone" class="form-control"
								placeholder="連絡電話" />
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="col-sm-2 control-label">電子郵件</label>
						<div class="col-sm-10">
							<form:input path="email" class="form-control"
								placeholder="電子郵件" />
						</div>
					</div>
					<button type="button" onclick="query()" class="btn btn-default pull-right">查詢</button>
				</div>
				<!-- /.box-body -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->
		 <% 
		 request.setAttribute("vEnter", "\r\n");   
		 %>
		<c:if test="${not empty teacherForm.result}">
			<div class="box" id="resultList">
				<div class="box-header">
					<a href="#" name="result"></a>
					<h3 class="box-title">查詢結果</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="dataTables_wrapper form-inline dt-bootstrap">
						<display:table name="teacherForm.result.content"
							pagesize="${teacherForm.result.size}"
							requestURI='?method=query&teacherForm_cId=${curr_teacherForm_cId}#result' id="list" cellspacing="0"
							cellpadding="0" class="table table-bordered table-hover"
							partialList="true"
							size="${teacherForm.result.totalElements.intValue()}"
							excludedParams="*">
							<display:column title='講師代號'  property="nickname" sortable="true">
							</display:column>
							<display:column title='講師姓名'  property="teacherName" sortable="true">
							</display:column>
							<display:column title='連絡電話'  property="phone" sortable="true">
							</display:column>
							<display:column title='電子郵件'  property="email" sortable="true">
							</display:column>
							<display:column title='學歷' sortable="true">
								<c:if test="${fn:indexOf(list.education, vEnter) > 0}">
									${fn:substringBefore(list.education,vEnter) }
								</c:if>
								<c:if test="${fn:indexOf(list.education, vEnter) <= 0}">
									${list.education}
								</c:if>
							</display:column>
							<display:column title='經歷' sortable="true">
								<c:if test="${fn:indexOf(list.experience, vEnter) > 0}">
									${fn:substringBefore(list.experience,vEnter) }
								</c:if>
								<c:if test="${fn:indexOf(list.experience, vEnter) <= 0}">
									${list.experience}
								</c:if>
							</display:column>
							<display:column title='現職' property="job" sortable="true">
							</display:column>
							<display:column title='專長 ' property="skill" sortable="true">
							</display:column>
							<display:column title='啟用 '  sortable="true">
								<e7:code type="1" code="${list.active}"/>
							</display:column>
							<!-- 有修改權限才顯示 -->
							<sec:authorize url="/teacher/modify">
							<display:column title='操作'>
								<button type="button" onclick="goModify('${list.pkTeacher}');" class="btn btn-success btn-sm">修改</button>
								<button type="button" onclick="remove('${list.pkTeacher}');" class="btn btn-danger btn-sm">刪除</button>
							</display:column>
							</sec:authorize> 
						</display:table>
					</div>
				</div>
			</div>
		</c:if>
	</div>
</div>
