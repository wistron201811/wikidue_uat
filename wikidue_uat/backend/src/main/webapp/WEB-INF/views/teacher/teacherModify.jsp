<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 預覽上傳的圖片 -->
<script
	src="${pageContext.request.contextPath}/plugins/uploadPreview/jquery.uploadPreview.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
<script type="text/javascript">
	$(function() {
		CKEDITOR.replace('teacherIntroduction');
	});
	function modifyTeacher() {
		$("#method").val("modify");
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#teacherForm").submit();
        }, 500);
	}
	function goQuery() {
		$.LoadingOverlay("show");
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="teacherForm" name="teacherForm"
				action="${pageContext.request.contextPath}/teacher/modify"
				method="post" enctype="multipart/form-data" class="form-horizontal">
				<sessionConversation:insertSessionConversationId attributeName="teacherForm"/>
				<form:hidden path="method" value="modify" />
				<form:hidden path="pkTeacher" />
				<div class="box-body">
					<div class="form-group">
						<label for="nickname" class="col-sm-2 control-label">*講師代號</label>
						<div class="col-sm-10">
							<form:input path="nickname" class="form-control"
								placeholder="講師代號" maxlength="50" />
							<form:errors path="nickname" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="teacherName" class="col-sm-2 control-label">*講師姓名</label>
						<div class="col-sm-10">
							<form:input path="teacherName" class="form-control"
								placeholder="講師姓名" maxlength="50" />
							<form:errors path="teacherName" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="phone" class="col-sm-2 control-label">*連絡電話</label>
						<div class="col-sm-10">
							<form:input path="phone" class="form-control"
								placeholder="連絡電話" maxlength="10" />
							<form:errors path="phone" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="col-sm-2 control-label">*電子郵件</label>
						<div class="col-sm-10">
							<form:input path="email" class="form-control" 
								placeholder="電子郵件" maxlength="50"  readonly="true"/>
							<form:errors path="email" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="photo" class="col-sm-2 control-label">照片<br>(建議尺寸500x500或等比例<br>但不得小於建議尺寸)</label>
						<div class="col-sm-10">
							<e7:previewImage attribute="photo" height="500px" width="500px"
								fileStorage="${teacherForm.photo}" />
							<form:errors path="photo" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="education" class="col-sm-2 control-label">學歷</label>
						<div class="col-sm-10">
							<form:textarea path="education" class="form-control" rows="5"
								maxlength="50" placeholder="學歷" />
							<form:errors path="education" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="experience" class="col-sm-2 control-label">經歷</label>
						<div class="col-sm-10">
							<form:textarea path="experience" class="form-control"
								placeholder="經歷" rows="5" maxlength="150" />
							<form:errors path="experience" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="job" class="col-sm-2 control-label">現職</label>
						<div class="col-sm-10">
							<form:input path="job" class="form-control" placeholder="現職"
								maxlength="50" />
							<form:errors path="job" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="skill" class="col-sm-2 control-label">專長</label>
						<div class="col-sm-10">
							<form:textarea path="skill" class="form-control" placeholder="專長"
								rows="5" maxlength="50" />
							<form:errors path="skill" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="teacherIntroduction" class="col-sm-2 control-label">講師介紹</label>
						<div class="col-sm-10">
							<%-- <form:textarea path="teacherIntroduction" class="form-control" /> --%>
							<textarea id="teacherIntroduction" name="teacherIntroduction" class="form-control">
								${e7:previewHtmlImg(pageContext.request.contextPath,teacherForm.teacherIntroduction)}
							</textarea>
							<form:errors path="teacherIntroduction" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="active" class="col-sm-2 control-label">啟用</label>
						<div class="col-sm-10">
							<form:radiobutton path="active" id="radio1" value="1" />
							<label for="radio1">是</label><br />
							<form:radiobutton path="active" id="radio2" value="0" />
							<label for="radio2">否</label>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button onclick="modifyTeacher()" class="btn btn-info">儲存</button>
					<a href="${pageContext.request.contextPath}/teacher/query#result"
						class="btn btn-default pull-right" onclick="goQuery();">取消</a>
				</div>
				<!-- /.box-footer -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>