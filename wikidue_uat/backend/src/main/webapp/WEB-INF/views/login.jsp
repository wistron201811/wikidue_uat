<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Wikidue Store 教育商城管理後台</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#">Wikidue Store 教育商城管理後台</a>
  </div>
  <jsp:include page="/WEB-INF/views/common/warningMessage.jsp"/>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">歡迎光臨</p>
    <form name="logoutForm" action="${sso_logout_page}">
		<input type="hidden" name="series" value="${series}"/>
		<input type="hidden" name="redirect_uri" value="${logout_redirect_uri}"/>
		<%-- <input type="hidden" name="redirect_uri" value="http://localhost:8080/backend/logout?d=1558591117142"/>--%>
	</form>
    <form name='login' action="${ssoLoginPage}" method="post">
    		<input type="hidden" name="client_id" value="${clientId}">
        <input type="hidden" name="response_type" value="${responseType}">
        <input type="hidden" name="redirect_uri" value="${redirectUri}">
        <%-- <input type="hidden" name="redirect_uri" value="http://localhost:8080/backend/login/auth">--%>
	    <div class="login-box-msg">
	      <button type="submit" class="btn btn-primary btn-block">進入後台</button>
	    </div>
    </form>
    <c:if test="${series!=null}">
	<div class="login-box-msg">
      <button onclick="ssoLogout();" class="btn btn-default btn-block">登出</button>
    </div>
	</c:if>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="${pageContext.request.contextPath}/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="${pageContext.request.contextPath}/plugins/iCheck/icheck.min.js"></script>

<script type="text/javascript">
function ssoLogout(){
	document.logoutForm.submit();
}
</script>
</body>
</html>