<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
	$(function() {
		$('.datepicker').datepicker({
			format : 'yyyy/mm',
			autoclose : true,
			startView : "months",
			minViewMode : "months"
		});
	});

	function query() {
		var uid = $("#uid").val();
		var yearMonth = $("#yearMonth").val();
		if (yearMonth == "") {
			alert("請選擇報表年月");
		} else if (uid == "") {
			alert("請選擇廠商");
		} else {
			document.accountingForm.action = '${pageContext.request.contextPath}/accounting/queryMonthly#result';
			$("#method").val("queryMonthly");
			$.LoadingOverlay("show");
			setTimeout(function() {
				$("#accountingForm").submit();
			}, 500);
		}
	}

	function showVendorCode() {
		var v = $("#taxId").val();
		$("#uid").val(v);
		$("#vendorCode").val(v);
	}
	function showTaxId() {
		var v = $("#vendorCode").val();
		$("#uid").val(v);
		$("#taxId").val(v);
	}
	function exportReport() {
		document.accountingForm.action = '${pageContext.request.contextPath}/accounting/export';
		$("#method").val("exportMonthly");
		$("#accountingForm").submit();
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="accountingForm" name="accountingForm"
				action="${pageContext.request.contextPath}/accounting/queryMonthly#result"
				class="form-horizontal">
				<sessionConversation:insertSessionConversationId
					attributeName="accountingForm" />
				<form:hidden path="method" />
				<form:hidden path="uid" />
				<div class="box-body">
					<div class="form-group date">
						<label for="yearMonth" class="col-sm-2 control-label">*報表年月</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon" onclick="$('#yearMonth').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="yearMonth"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM" maxlength="7" />
								<form:errors path="yearMonth" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="uid" class="col-sm-2 control-label">*廠商統編</label>
						<div class="col-sm-10">
							<form:select path="taxId" cssClass="form-control"
								onchange="showVendorCode();">
								<form:option value="">請選擇(統編 - 名稱)</form:option>
								<c:forEach items="${accountingForm.vendorList}" var="vendor">
									<form:option value="${vendor.uid}">${vendor.taxId} - ${vendor.vendorName }</form:option>
								</c:forEach>
							</form:select>
						</div>
					</div>
					<div class="form-group">
						<label for="uid" class="col-sm-2 control-label">*廠商編號</label>
						<div class="col-sm-10">
							<form:select path="vendorCode" cssClass="form-control"
								onchange="showTaxId();">
								<form:option value="">請選擇(編號 - 名稱)</form:option>
								<c:forEach items="${accountingForm.vendorList}" var="vendor">
									<form:option value="${vendor.uid}">${vendor.vendorCode} - ${vendor.vendorName }</form:option>
								</c:forEach>
							</form:select>
						</div>
					</div>
					<button type="button" onclick="query()"
						class="btn btn-default pull-right">查詢</button>
				</div>
				<!-- /.box-body -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->

		<c:if test="${not empty accountingForm.reporList}">
			<div class="box" id="resultList">
				<div class="box-header">
					<a href="#" name="result"></a>
					<h3 class="box-title">查詢結果</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body" id="resultList" style="overflow: scroll;">
					<div class="form-group">
						<div class="col-sm-12 control-label">
							<button type="button" onclick="exportReport()"
								class="btn btn-info pull-right">Excel 匯出</button>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label"> 報表年月：<fmt:formatDate
								pattern="yyyy/MM"
								value="${accountingForm.monthlyReportHeaderVo.yearMonth}" />
						</label> <label class="col-sm-4 control-label"> 時間區間：<fmt:formatDate
								pattern="yyyy/MM/dd"
								value="${accountingForm.monthlyReportHeaderVo.reportStart}" />
							~ <fmt:formatDate pattern="yyyy/MM/dd"
								value="${accountingForm.monthlyReportHeaderVo.reportEnd}" />
						</label> <label class="col-sm-4 control-label"> <%-- 報表產生日期：<fmt:formatDate pattern="yyyy/MM/dd" value="${accountingForm.monthlyReportHeaderVo.reportDate}" /> --%>
							&nbsp;
						</label>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label">
							廠商名稱：${accountingForm.monthlyReportHeaderVo.vendorName} </label> <label
							class="col-sm-4 control-label">
							廠商編號：${accountingForm.monthlyReportHeaderVo.vendorCode} </label> <label
							class="col-sm-4 control-label">
							廠商統編：${accountingForm.monthlyReportHeaderVo.vendorTaxId} </label>
					</div>
					<div class="form-group">
						<label class="col-sm-4 control-label"> 銷售訂單總筆數： <fmt:formatNumber
								value="${accountingForm.monthlyReportHeaderVo.solds}"
								type="number" />
						</label> <label class="col-sm-4 control-label"> 月銷售總額： <fmt:formatNumber
								value="${accountingForm.monthlyReportHeaderVo.monthlyAmount}"
								type="number" /> (含稅)
						</label> <label class="col-sm-4 control-label"> 月進貨總額： <fmt:formatNumber
								value="${accountingForm.monthlyReportHeaderVo.monthlyRatioAmount}"
								type="number" /> (含稅)
						</label>
					</div>
					<div class="form-group">
						<div class="col-sm-12 control-label">
							<label class="pull-right"> 此表皆為含稅金額 </label>
						</div>
					</div>
					<br>
					<table class="table table-bordered  table-striped text-center">
						<thead>
							<tr class="th-background">
								<td>料號</td>
								<td style="width: 20%">品名</td>
								<td>訂單日期</td>
								<td>主訂單編號</td>
								<td>狀態日期</td>
								<td style="width: 10%">狀態</td>
								<td>發票號碼</td>
								<td>銷售數量</td>
								<td>銷售單價</td>
								<td>銷售總額</td>
								<td>進價成數</td>
								<td>進貨單價</td>
								<td>進貨價總額</td>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="detailVo" items="${accountingForm.reporList}">
								<tr>
									<td style="text-align: left">${detailVo.materialNum }</td>
									<td style="text-align: left">${detailVo.productName}</td>
									<td style="text-align: center"><fmt:formatDate
											pattern="yyyy-MM-dd" value="${detailVo.orderDt}" /> <br>
										<fmt:formatDate pattern="HH:mm" value="${detailVo.orderDt}" /></td>
									<td style="text-align: center">${detailVo.pkMainOrder}</td>
									<td style="text-align: center"><fmt:formatDate
											pattern="yyyy-MM-dd" value="${detailVo.statusDt}" /> <br>
										<fmt:formatDate pattern="HH:mm"
											value="${detailVo.statusDt}" /></td>
									<td><e7:code type="5" code="${detailVo.status}" /></td>
									<td style="text-align: center">${detailVo.invoiceNo}</td>
									<td style="text-align: center"><fmt:formatNumber
											value="${detailVo.quantity}" type="number" /></td>
									<td style="text-align: right"><fmt:formatNumber
											value="${detailVo.unitPrice}" type="number" /></td>
									<td style="text-align: right"><fmt:formatNumber
											value="${detailVo.totalAmount}" type="number" /></td>
									<td style="text-align: center"><c:if
											test="${not empty detailVo.ratio }">${detailVo.ratio}%</c:if>
									</td>
									<td style="text-align: right"><fmt:formatNumber
											value="${detailVo.ratioUnitPrice}" type="number" /></td>
									<td style="text-align: right"><fmt:formatNumber
											value="${detailVo.ratioAmount}" type="number" /></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<BR>
					<div class="form-group">
						<label class="col-sm-12 control-label"> 進貨價總金額&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;=&nbsp; 月進貨價總額 &nbsp;<input
							value='<fmt:formatNumber value="${accountingForm.monthlyReportHeaderVo.monthlyRatioAmount}" type="number" />'
							readonly="readonly" disabled="disabled"
							style="text-align: center; width: 100px;">
						</label>
						<label class="col-sm-12 control-label"> Wikidue收費金額&nbsp;
							&nbsp;=&nbsp; 交易手續費 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input
							value='<fmt:formatNumber value="${accountingForm.monthlyReportHeaderVo.processingFee}" type="number" />'
							readonly="readonly" disabled="disabled"
							style="text-align: center; width: 100px;">
							&nbsp;+&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;平台管理費&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input
							value='<fmt:formatNumber value="${accountingForm.monthlyReportHeaderVo.managementFee}" type="number" />'
							readonly="readonly" disabled="disabled"
							style="text-align: center; width: 100px;">
							&nbsp;+&nbsp;廠商廣告費&nbsp;<input
							value='<fmt:formatNumber value="${accountingForm.monthlyReportHeaderVo.adFee}" type="number" />'
							readonly="readonly" disabled="disabled"
							style="text-align: center; width: 100px;">
						</label>
						<label class="col-sm-12 control-label"> 本月結帳總額&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;=&nbsp; 進貨價總金額 &nbsp;<input
							value='<fmt:formatNumber value="${accountingForm.monthlyReportHeaderVo.monthlyRatioAmount}" type="number" />'
							readonly="readonly" disabled="disabled"
							style="text-align: center; width: 100px;">
							&nbsp;-&nbsp;Wikidue收費金額&nbsp;<input
							value='<fmt:formatNumber value="${accountingForm.monthlyReportHeaderVo.wikidueFee}" type="number" />'
							readonly="readonly" disabled="disabled"
							style="text-align: center; width: 100px;">
							&nbsp;=&nbsp;&nbsp;<input
							value='<fmt:formatNumber value="${accountingForm.monthlyReportHeaderVo.summaryAmount}" type="number" />'
							readonly="readonly" disabled="disabled"
							style="text-align: center; width: 100px;">
						</label>
					</div>
				</div>
			</div>
		</c:if>
		<c:if test="${empty accountingForm.reporList && hasQueryResult}">
			<div class="box" id="resultList">
				<div class="box-header">
					<a href="#" name="result"></a>
					<h3 class="box-title">查詢結果</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">查無資料.</div>
			</div>
		</c:if>
	</div>
</div>
