<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<script type="text/javascript">
	$(function() {
		$('.datepicker').datepicker({
			format : 'yyyy/mm/dd',
			autoclose : true,
			todayBtn : true,
			todayHighlight : true
		});
	});

	$(document).ready(function() {
		$("#checkAll").click(function() {
			if ($("#checkAll").prop("checked")) {//如果全選按鈕有被選擇的話（被選擇是true）
				$("input[name='orderStatusList']").each(function() {
					$(this).prop("checked", true);//把所有的核取方框的property都變成勾選
				})
			} else {
				$("input[name='orderStatusList']").each(function() {
					$(this).prop("checked", false);//把所有的核方框的property都取消勾選
				})
			}
		})
	})

	function query() {
		$("#method").val("querySummary");
		$.LoadingOverlay("show");
		setTimeout(function() {
			document.accountingForm.action = '${pageContext.request.contextPath}/accounting/querySummary#result';
			$("#accountingForm").submit();
		}, 500);
	}

	function showVendorCode() {
		var v = $("#taxId").val();
		$("#uid").val(v);
		$("#vendorCode").val(v);
	}
	function showTaxId() {
		var v = $("#vendorCode").val();
		$("#uid").val(v);
		$("#taxId").val(v);
	}
	function exportReport() {
		document.accountingForm.action = '${pageContext.request.contextPath}/accounting/export';
		$("#method").val("exportSummary");
		$("#accountingForm").submit();
	}
	function queryOrderDeatil(pkMainOrder) {
		$.fancybox({
			'autoSize' : false,
			'fitToView' : false,
			'width' : "100%",
			'scrolling' : 'yes',
			'transitionIn' : 'none',
			'transitionOut' : 'none',
			'type' : 'iframe',
			'padding' : '0',
			'closeBtn' : true,
			'href' : '${pageContext.request.contextPath}/order/queryDetail/'
					+ pkMainOrder + '/' + new Date().getTime()
		});
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="accountingForm" name="accountingForm"
				action="${pageContext.request.contextPath}/accounting/querySummary#result"
				class="form-horizontal">
				<sessionConversation:insertSessionConversationId
					attributeName="accountingForm" />
				<form:hidden path="method" />
				<form:hidden path="uid" />
				<div class="box-body">
					<div class="form-group date">
						<label for="reportStart" class="col-sm-2 control-label">訂單日期區間</label>
						<div class="col-sm-5">
							<div class="input-group date">
								<div class="input-group-addon" onclick="$('#yearMonth').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="reportStart"
									cssClass="form-control pull-right  datepicker"
									placeholder="開始時間" maxlength="7" />
								<form:errors path="reportStart" class="text-red" />
							</div>
						</div>
						<div class="col-sm-5">
							<div class="input-group date">
								<div class="input-group-addon" onclick="$('#yearMonth').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="reportEnd"
									cssClass="form-control pull-right  datepicker"
									placeholder="結束時間" maxlength="7" />
								<form:errors path="reportEnd" class="text-red" />
							</div>
						</div>
					</div>
					<c:if test="${user_auth.admin}">
					<div class="form-group">
						<label for="uid" class="col-sm-2 control-label">廠商統編</label>
						<div class="col-sm-10">
							<form:select path="taxId" cssClass="form-control"
								onchange="showVendorCode();">
								<form:option value="">全部</form:option>
								<c:forEach items="${accountingForm.vendorList}" var="vendor">
									<form:option value="${vendor.uid}">${vendor.taxId} - ${vendor.vendorName }</form:option>
								</c:forEach>
							</form:select>
						</div>
					</div>
					<div class="form-group">
						<label for="uid" class="col-sm-2 control-label">廠商編號</label>
						<div class="col-sm-10">
							<form:select path="vendorCode" cssClass="form-control"
								onchange="showTaxId();">
								<form:option value="">全部</form:option>
								<c:forEach items="${accountingForm.vendorList}" var="vendor">
									<form:option value="${vendor.uid}">${vendor.vendorCode} - ${vendor.vendorName }</form:option>
								</c:forEach>
							</form:select>
						</div>
					</div>
					</c:if>
					<div class="form-group">
						<label for="pkMainOrder" class="col-sm-2 control-label">主訂單編號</label>
						<div class="col-sm-10">
							<form:input path="pkMainOrder" class="form-control"
								placeholder="主訂單編號" maxlength="50" />
							<form:errors path="pkMainOrder" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="checkAll" class="checkbox col-sm-2 control-label"><input
							type="checkbox" id="checkAll" name="checkAll" /><b>訂單狀態</b></label>
						<div class="col-sm-10">
							<c:forEach items="${accountingForm.orderStatusEnumMap}"
								var="orderStatus">
								<label class="checkbox-inline"><form:checkbox
										path="orderStatusList" value="${orderStatus.key }"
										label="${orderStatus.value}" /></label>
							</c:forEach>
						</div>
					</div>
					<div class="form-group">
						<label for="sorting" class="col-sm-2 control-label">訂單日期排序</label>
						<div class="col-sm-10">
							<form:radiobutton path="sorting" id="radio1" value="ascending"
								checked="checked" />
							<label for="radio1">由舊到新</label><br />
							<form:radiobutton path="sorting" id="radio2" value="descending" />
							<label for="radio2">由新到舊</label>
						</div>
					</div>
					<button type="button" onclick="query()"
						class="btn btn-default pull-right">查詢</button>
				</div>
				<!-- /.box-body -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->

		<c:if test="${not empty accountingForm.reporList}">
			<div class="box" id="resultList">
				<div class="box-header">
					<a href="#" name="result"></a>
					<h3 class="box-title">查詢結果</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body" id="resultList" style="overflow-x: scroll;">
					<div class="form-group">
						<div class="col-sm-12 control-label">
							<button type="button" onclick="exportReport()"
								class="btn btn-info pull-right">Excel 匯出</button>
						</div>
					</div>
					<br>
					<table class="table table-bordered  table-striped text-center">
						<thead>
							<tr class="th-background">
								<td style="width: 15%">訂單日期</td>
								<td>主訂單編號</td>
								<td style="width: 15%">狀態日期</td>
								<td style="width: 15%">狀態</td>
								<td style="width: 15%">供應商</td>
								<td>供應商編號</td>
								<td>料號</td>
								<td style="width: 20%">品名</td>
								<td>發票號碼</td>
								<td style="width: 10%">銷售數量</td>
								<td>銷售單價</td>
								<td>銷售總額</td>
								<td>進價成數</td>
								<td>進貨單價</td>
								<td>進貨價總額</td>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="detailVo" items="${accountingForm.reporList}">
								<tr>
									<td style="text-align: center"><fmt:formatDate
											pattern="yyyy-MM-dd" value="${detailVo.orderDt}" /> <br>
										<fmt:formatDate pattern="HH:mm" value="${detailVo.orderDt}" /></td>
									<td style="text-align: center">
										<c:if test="${user_auth.admin}">
											<a href="#" onclick="queryOrderDeatil('${detailVo.pkMainOrder}');return false;">${detailVo.pkMainOrder}</a>
										</c:if>
										<c:if test="${!user_auth.admin}">
											<a href="#" onclick="queryOrderDeatil('${detailVo.pkSubOrder}');return false;">${detailVo.pkMainOrder}</a>
										</c:if>
									</td>
									<td style="text-align: center"><fmt:formatDate
											pattern="yyyy-MM-dd" value="${detailVo.statusDt}" /> <br>
										<fmt:formatDate pattern="HH:mm"
											value="${detailVo.statusDt}" /></td>
									<td><e7:code type="5" code="${detailVo.status}" /></td>
									<td style="text-align: left">${detailVo.vendorName }</td>
									<td style="text-align: left">${detailVo.vendorCode }</td>
									<td style="text-align: left">${detailVo.materialNum }</td>
									<td style="text-align: left">${detailVo.productName}</td>
									<td style="text-align: center">${detailVo.invoiceNo}</td>
									<td style="text-align: center"><fmt:formatNumber
											value="${detailVo.quantity}" type="number" /></td>
									<td style="text-align: right"><fmt:formatNumber
											value="${detailVo.unitPrice}" type="number" /></td>
									<td style="text-align: right"><fmt:formatNumber
											value="${detailVo.totalAmount}" type="number" /></td>
									<td style="text-align: center">
									<c:if test="${not empty detailVo.ratio }">${detailVo.ratio}%</c:if>
									</td>
									<td style="text-align: right"><fmt:formatNumber
											value="${detailVo.ratioUnitPrice}" type="number" /></td>
									<td style="text-align: right"><fmt:formatNumber
											value="${detailVo.ratioAmount}" type="number" /></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<BR>
				</div>
			</div>
		</c:if>
		<c:if test="${empty accountingForm.reporList && hasQueryResult}">
			<div class="box" id="resultList">
				<div class="box-header">
					<a href="#" name="result"></a>
					<h3 class="box-title">查詢結果</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">查無資料.</div>
			</div>
		</c:if>
	</div>
</div>
