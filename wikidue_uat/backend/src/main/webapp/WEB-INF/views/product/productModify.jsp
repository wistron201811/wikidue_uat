<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 預覽上傳的圖片 -->
<script
	src="${pageContext.request.contextPath}/plugins/uploadPreview/jquery.uploadPreview.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>

<script type="text/javascript">
	$(function() {
		$('.datepicker').datepicker({
			format : 'yyyy/mm/dd',
			autoclose : true,
			todayBtn : true,
			todayHighlight : true
		});
		if ($("#categoryType").val() == "COURSE") {
			$("#qtyDiv").hide();
		}
		$('.datepicker').datepicker("option", "disabled", true);
	});
	function modifyProduct() {
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#productForm").submit();
        }, 500);
	}
	function goQuery() {
		$.LoadingOverlay("show");
	}
	function updateTag() {
		document.productForm.action = '${pageContext.request.contextPath}/product/updateTag';
		$("#method").val("update");
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#productForm").submit();
        }, 500);
	}
	function chooseProps() {
		$.fancybox({
			'autoSize' : false,
			'fitToView' : false,
			'width' : "100%",
			'scrolling' : 'auto',
			'transitionIn' : 'none',
			'transitionOut' : 'none',
			'type' : 'iframe',
			'padding' : '0',
			'closeBtn' : true,
			'href' : '${pageContext.request.contextPath}/props/queryProps'
		});
	}
	function chooseCourse() {
		$.fancybox({
			'autoSize' : false,
			'fitToView' : false,
			'width' : "100%",
			'scrolling' : 'auto',
			'transitionIn' : 'none',
			'transitionOut' : 'none',
			'type' : 'iframe',
			'padding' : '0',
			'closeBtn' : true,
			'href' : '${pageContext.request.contextPath}/course/queryCourse'
		});
	}
	function showProps(pkProps, productName, price, discountPrice, quantity) {
		clearErrors();
		$("#categoryType").val("PROPS");
		$("#pkProps").val(pkProps);
		$("#pkCourse").val("");
		$("#productName").val(productName);
		$("#price").val(price);
		$("#discountPrice").val(discountPrice);
		$("#quantity").val(quantity);
		$("#qtyDiv").show();
	}
	function showCourse(pkCourse, productName, price, discountPrice) {
		clearErrors();
		$("#categoryType").val("COURSE");
		$("#pkCourse").val(pkCourse);
		$("#pkProps").val("");
		$("#productName").val(productName);
		$("#price").val(price);
		$("#discountPrice").val(discountPrice);
		$("#quantity").val("");
		$("#qtyDiv").hide();
	}
	function clearErrors(){
		$('span[id*="error"]').text('');
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="productForm" name="productForm"
				action="${pageContext.request.contextPath}/product/modify"
				method="post" enctype="multipart/form-data" class="form-horizontal">
				<sessionConversation:insertSessionConversationId
					attributeName="productForm" />
				<form:hidden path="method" value="modify" />
				<form:hidden path="pkProduct" />
				<form:hidden path="pkCourse" />
				<form:hidden path="pkProps" />
				<form:hidden path="categoryType" />
				<div class="box-body">
					<%-- 
					<div class="form-group">
						<label for="name" class="col-sm-2 control-label">*選擇商品</label>
						<div class="col-sm-10">
							<button type="button" onclick="chooseCourse('COURSE');return false;" class="btn btn-info" disabled>選擇課程</button>
							<button type="button" onclick="chooseProps('PROPS');return false;" class="btn btn-info" disabled>選擇教具</button>
						</div>
					</div>
				--%>
					<div class="form-group">
						<label for="name" class="col-sm-2 control-label">更新商品標韱</label>
						<div class="col-sm-10">
							<button type="button" onclick="updateTag()"
								class="btn btn-warning">更新</button>
						</div>
					</div>
					<div class="form-group">
						<label for="productName" class="col-sm-2 control-label">*商品名稱</label>
						<div class="col-sm-10">
							<form:input path="productName" class="form-control"
								placeholder="商品名稱" maxlength="5" readonly="true" />
							<form:errors path="productName" class="text-red" />
						</div>
					</div>
					<c:if test="${not empty productForm.pkProps }">
						<div class="form-group" id="qtyDiv">
							<label for="quantity" class="col-sm-2 control-label">*商品數量</label>
							<div class="col-sm-10">
								<form:input path="quantity" class="form-control"
									placeholder="商品數量" maxlength="5" readonly="true" />
								<form:errors path="quantity" class="text-red" />
							</div>
						</div>
					</c:if>
					<div class="form-group">
						<label for="price" class="col-sm-2 control-label">*原價</label>
						<div class="col-sm-10">
							<c:if test="${not productForm.pageReadonly}">
								<form:input path="price" class="form-control" maxlength="8"
									placeholder="原價" />
							</c:if>
							<c:if test="${productForm.pageReadonly}">
								<form:input path="price" class="form-control" maxlength="8"
									placeholder="原價" readonly="true" />
							</c:if>
							<form:errors path="price" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="discountPrice" class="col-sm-2 control-label">*優惠價</label>
						<div class="col-sm-10">
							<c:if test="${not productForm.pageReadonly}">
								<form:input path="discountPrice" class="form-control"
									maxlength="8" placeholder="優惠價" />
							</c:if>
							<c:if test="${productForm.pageReadonly}">
								<form:input path="discountPrice" class="form-control"
									maxlength="8" placeholder="優惠價" readonly="true" />
							</c:if>
							<form:errors path="discountPrice" class="text-red" />
						</div>
					</div>
					<div class="form-group date">
						<label for="publishSDate" class="col-sm-2 control-label">*上架日期</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#publishSDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<c:if test="${productForm.sdateReadonly }">
									<form:input path="publishSDate"
										cssClass="form-control pull-right " readonly="true"
										placeholder="yyyy/MM/dd" maxlength="10" />
								</c:if>
								<c:if test="${not productForm.sdateReadonly }">
									<form:input path="publishSDate"
										cssClass="form-control pull-right  datepicker"
										placeholder="yyyy/MM/dd" maxlength="10" />
								</c:if>
								<form:errors path="publishSDate" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="publishEDate" class="col-sm-2 control-label">*下架日期</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#publishEDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<c:if test="${not productForm.pageReadonly}">
									<form:input path="publishEDate"
										cssClass="form-control pull-right  datepicker"
										placeholder="yyyy/MM/dd" maxlength="10" />
								</c:if>
								<c:if test="${productForm.pageReadonly}">
									<form:input path="publishEDate"
										cssClass="form-control pull-right  datepicker"
										placeholder="yyyy/MM/dd" maxlength="10" readonly="true" />
								</c:if>
								<form:errors path="publishEDate" class="text-red" />
							</div>
						</div>
					</div>
					<c:if test="${user_auth.admin}">
					<div class="form-group">
						<label for="weights" class="col-sm-2 control-label">*排序權重</label>
						<div class="col-sm-10">
							<form:input path="weights" class="form-control" maxlength="4"
								placeholder="請輸入0~9999(含)之間的數字" />
							<form:errors path="weights" class="text-red" />
						</div>
					</div>
					</c:if>
					<div class="form-group">
						<label for="active" class="col-sm-2 control-label">啟用</label>
						<div class="col-sm-10">
							<form:radiobutton path="active" id="radio1" value="1"
								checked="checked" />
							<label for="radio1">是</label><br />
							<form:radiobutton path="active" id="radio2" value="0" />
							<label for="radio2">否</label>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<c:if test="${ productForm.pageReadonly}">
						<button type="button" onclick="modifyProduct()"
							class="btn btn-info" disabled>儲存</button>
					</c:if>
					<c:if test="${not productForm.pageReadonly}">
						<button type="button" onclick="modifyProduct()"
							class="btn btn-info">儲存</button>
					</c:if>
					<a href="${pageContext.request.contextPath}/product/query#result"
						class="btn btn-default pull-right" onclick="goQuery();">取消</a>
				</div>
				<!-- /.box-footer -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>