<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript">
	$(function() {
		$('.datepicker').datepicker({
			format : 'yyyy/mm/dd',
			autoclose : true,
			todayBtn : true,
			todayHighlight : true
		});
	});

	function query() {
		$("#method").val("query");

		$.LoadingOverlay("show");
		setTimeout(function() {
			$("#productForm").submit();
		}, 500);
	}

	function remove(pkProduct) {
		if (confirm('確認刪除？')) {
			$("#pkProduct").val(pkProduct);
			document.productForm.action = '${pageContext.request.contextPath}/product/remove';
			$("#method").val("remove");

			$.LoadingOverlay("show");
			setTimeout(function() {
				$("#productForm").submit();
			}, 500);
		}
	}
	function goModify(pkProduct) {
		$("#pkProduct").val(pkProduct);
		document.productForm.action = '${pageContext.request.contextPath}/product/modify';
		document.productForm.productForm_cId.value="";
		$("#method").val("");

		$.LoadingOverlay("show");
		setTimeout(function() {
			$("#productForm").submit();
		}, 500);
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="productForm" name="productForm"
				action="${pageContext.request.contextPath}/product/query#result"
				class="form-horizontal">
				<sessionConversation:insertSessionConversationId
					attributeName="productForm" />
				<form:hidden path="method" />
				<form:hidden path="pkProduct" />
				<div class="box-body">
					<div class="form-group">
						<label for="name" class="col-sm-2 control-label">商品名稱</label>
						<div class="col-sm-10">
							<form:input path="productName" cssClass="form-control"
								placeholder="商品名稱" />
						</div>
					</div>
					<c:if test="${user_auth.admin}">
						<div class="form-group">
							<label for="vendorName" class="col-sm-2 control-label">廠商名稱</label>
							<div class="col-sm-10">
								<form:input path="vendorName" class="form-control"
									placeholder="廠商名稱" maxlength="50" />
								<form:errors path="vendorName" class="text-red" />
							</div>
						</div>
					</c:if>
					<div class="form-group">
						<label for="publishSDate" class="col-sm-2 control-label">上架日期</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#publishSDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="publishSDate"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="publishSDate" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="publishEDate" class="col-sm-2 control-label">下架日期</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#publishEDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="publishEDate"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="publishEDate" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="categoryType" class="col-sm-2 control-label">商品類型</label>
						<div class="col-sm-10">
							<form:select path="categoryType" cssClass="form-control">
								<form:option value="">請選擇</form:option>
								<form:option value="COURSE">課程</form:option>
								<form:option value="PROPS">教具</form:option>
								<%-- <form:option value="CAMP">營隊</form:option>  --%>
							</form:select>
						</div>
					</div>
					<div class="form-group">
						<label for="categoryType" class="col-sm-2 control-label">商品狀態</label>
						<div class="col-sm-10">
							<form:checkbox path="onMarket" value="true" label="正在上架" />
						</div>
					</div>
					<button type="button" onclick="query()"
						class="btn btn-default pull-right">查詢</button>
				</div>
				<!-- /.box-body -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->

		<c:if test="${not empty productForm.result}">
			<div class="box" id="resultList">
				<div class="box-header">
					<a href="#" name="result"></a>
					<h3 class="box-title">查詢結果</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="dataTables_wrapper form-inline dt-bootstrap">
						<display:table name="productForm.result.content"
							pagesize="${productForm.result.size}"
							requestURI='?method=query&productForm_cId=${curr_productForm_cId}#result'
							id="list" cellspacing="0" cellpadding="0"
							class="table table-bordered table-hover" partialList="true"
							size="${productForm.result.totalElements.intValue()}"
							excludedParams="*">
							<display:column title='料號' sortable="true">
								<c:if test="${not empty list.course}">${list.course.materialNum }</c:if>
								<c:if test="${not empty list.props}">${list.props.materialNum }</c:if>
							</display:column>
							<display:column title='商品名稱' property="productName"
								sortable="true">
							</display:column>
							<c:if test="${user_auth.admin}">
								<display:column title='廠商名稱' sortable="true">
									<c:if test="${not empty list.vendor }">
									${list.vendor.vendorName}
								</c:if>
								</display:column>
							</c:if>
							<display:column title='商品類別' sortable="true">
								<c:if test="${not empty list.course}">課程</c:if>
								<c:if test="${not empty list.props}">教具</c:if>
								<c:if test="${not empty list.camp}">營隊</c:if>
							</display:column>
							<display:column title='上架日期' sortable="true">
								<fmt:formatDate pattern="yyyy-MM-dd"
									value="${list.publishSDate}" />
							</display:column>
							<display:column title='下架日期' sortable="true">
								<fmt:formatDate pattern="yyyy-MM-dd"
									value="${list.publishEDate}" />
							</display:column>
							<display:column title='原價' sortable="true">
								<p style="text-align: right">
									<fmt:formatNumber value="${list.price}" type="number" />
								</p>
							</display:column>
							<display:column title='優惠價' sortable="true">
								<p style="text-align: right">
									<fmt:formatNumber value="${list.discountPrice}" type="number" />
								</p>
							</display:column>
							<display:column title='庫存量' sortable="true">
								<c:if test="${not empty list.props }">
									<p style="text-align: right">
										<fmt:formatNumber value="${list.props.quantity}" type="number" />
									</p>
								</c:if>
							</display:column>
							<!-- 有修改權限才顯示 -->
							<sec:authorize url="/product/modify">
								<display:column title='操作'>
									<button type="button" onclick="goModify('${list.pkProduct}');"
										class="btn btn-success btn-sm">修改</button>
									<button type="button" onclick="remove('${list.pkProduct}');"
										class="btn btn-danger btn-sm">刪除</button>
								</display:column>
							</sec:authorize>
						</display:table>
					</div>
				</div>
			</div>
		</c:if>
	</div>
</div>
