<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script type="text/javascript">
	function query() {
		$("#method").val("query");
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#propsForm").submit();
        }, 500);
	}

	function remove(pkProps) {
		if (confirm('確認刪除？')) {
			$("#pkProps").val(pkProps);
			document.propsForm.action = '${pageContext.request.contextPath}/props/remove';
			$("#method").val("remove");
			$.LoadingOverlay("show");
	        setTimeout(function(){
	        	$("#propsForm").submit();
	        }, 500);
		}
	}
	function goModify(pkProps) {
		$("#pkProps").val(pkProps);
		document.propsForm.action = '${pageContext.request.contextPath}/props/modify';
		document.propsForm.propsForm_cId.value="";
		$("#method").val("");
		$.LoadingOverlay("show");
	    setTimeout(function(){
	    	$("#propsForm").submit();
	    }, 500);
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="propsForm" name="propsForm"
				action="${pageContext.request.contextPath}/props/query#result"
				class="form-horizontal">
				<sessionConversation:insertSessionConversationId
					attributeName="propsForm" />
				<form:hidden path="method" />
				<form:hidden path="pkProps" />
				<div class="box-body">
					<div class="form-group">
						<label for="materialNum" class="col-sm-2 control-label">料號</label>
						<div class="col-sm-10">
							<form:input path="materialNum" cssClass="form-control"
								placeholder="料號" />
						</div>
					</div>
					<div class="form-group">
						<label for="propsName" class="col-sm-2 control-label">商品名稱</label>
						<div class="col-sm-10">
							<form:input path="propsName" cssClass="form-control"
								placeholder="商品名稱" />
						</div>
					</div>
					<c:if test="${user_auth.admin}">
					<div class="form-group">
						<label for="vendorName" class="col-sm-2 control-label">廠商名稱</label>
						<div class="col-sm-10">
							<form:input path="vendorName" class="form-control"
								placeholder="廠商名稱" maxlength="50" />
							<form:errors path="vendorName" class="text-red" />
						</div>
					</div>
					</c:if>
					<div class="form-group">
						<label for="active" class="col-sm-2 control-label">狀態</label>
						<div class="col-sm-10">
							<form:select path="active" class="form-control">
								<form:option value="">請選擇</form:option>
								<form:option value="1">可上架</form:option>
								<form:option value="0">不可上架</form:option>
							</form:select>
						</div>
					</div>
					<button type="button" onclick="query()" class="btn btn-default pull-right">查詢</button>
				</div>
				<!-- /.box-body -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->

		<c:if test="${not empty propsForm.result}">
			<div class="box" id="resultList">
				<div class="box-header">
					<a href="#" name="result"></a>
					<h3 class="box-title">查詢結果</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="dataTables_wrapper form-inline dt-bootstrap">
						<display:table name="propsForm.result.content"
							pagesize="${propsForm.result.size}"
							requestURI='?method=query&propsForm_cId=${curr_propsForm_cId}#result'
							id="list" cellspacing="0" cellpadding="0"
							class="table table-bordered table-hover" partialList="true"
							size="${propsForm.result.totalElements.intValue()}"
							excludedParams="*">
							<display:column title='料號' property="materialNum" sortable="true"/>
							<display:column title='商品名稱' property="propsName" sortable="true"/>
							<c:if test="${user_auth.admin}">
							<display:column title='廠商名稱'  property="vendor.vendorName" sortable="true">
							</display:column>
							</c:if>
							<display:column title='進價' sortable="true">
								<p style="text-align:right">
								<fmt:formatNumber value="${list.costPrice}" type="number"/>
								</p>
							</display:column>
							<display:column title='定價' sortable="true" >
								<p style="text-align:right">
								<fmt:formatNumber value="${list.price}" type="number"/>
								</p>
							</display:column>
							<display:column title='優專價' sortable="true">
								<p style="text-align:right">
								<fmt:formatNumber value="${list.discountPrice}" type="number"/>
								</p>
							</display:column>
							<display:column title='數量' sortable="true" >
								<p style="text-align:right">
								<fmt:formatNumber value="${list.quantity}" type="number"/>
								</p>
							</display:column>
							<display:column title='狀態' sortable="true">
								<e7:code type="6" code="${list.active}" />
							</display:column>
							<!-- 有修改權限才顯示 -->
							<sec:authorize url="/props/modify"> 
							<display:column title='操作'>
								<button type="button" onclick="goModify('${list.pkProps}');"
									class="btn btn-success btn-sm">修改</button>
								<button type="button" onclick="remove('${list.pkProps}');"
									class="btn btn-danger btn-sm">刪除</button>
							</display:column>
							</sec:authorize>
						</display:table>
					</div>
				</div>
			</div>
		</c:if>
	</div>
</div>