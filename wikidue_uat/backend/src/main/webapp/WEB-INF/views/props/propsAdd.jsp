<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 預覽上傳的圖片 -->
<script
	src="${pageContext.request.contextPath}/plugins/uploadPreview/jquery.uploadPreview.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>

<script type="text/javascript">
var gradeButton;//現在被按的年級 Tag,如沒有TAG被按則為null
var subjectButton;//現在被按的科目 Tag,如沒有TAG被按則為null
var chapterButton;//現在被按的章節 Tag,如沒有TAG被按則為null
var categoryButton;//現在被按的分類 Tag,如沒有TAG被按則為null
var count = 0;
	$(function() {
		CKEDITOR.replace('propsIntroduction');

		$('.datepicker').datepicker({
			format : 'yyyy/mm/dd',
			todayBtn : true,
			todayHighlight : true,
			autoclose : true
		});
		
		//查詢年級
		queryTags(null,"0");
		//隱藏科目Div
		$("#subjectGroup").hide();
		//隱藏章節Div
		$("#chapterGroup").hide();
	});
	
	//查詢知識地圖，tag為上一層的code,level為等級(0:年級,1:科目,3:章節)
	function queryTags(tag,level) {
		//將要查詢的TAG清單清空，並顯示"查詢中"
		if(level=='0'){
			$('#gradeContent').html('查詢中...');
		}else if(level=='1'){
			$('#subjectContent').html('查詢中...');
		}else if(level=='3'){
			$('#chapterContent').html('查詢中...');
		}
		
		$.ajax({
			type : "POST",
			url : '${pageContext.request.contextPath}/api/tag/query',
			cache : false,
			contentType : 'application/json',
			data : JSON.stringify({"tag":tag,"level":level}),//查詢的參數
			success : function(result) {
				//查完後，將原本"查詢中"的提示訊息移除
				if(level=='0'){
					$('#gradeContent').html('');
				}else if(level=='1'){
					$('#subjectContent').html('');
				}else if(level=='3'){
					$('#chapterContent').html('');
				}
				//檢查查詢結果及是否有資料
				if (result != null && result!='' && result.success) {
					var data = result.result;
					//依序在TAG清單新增TAG
					if(level=='0'){
						$.each(data, function(i) {
							$('#gradeContent').append("<label name='grade' value='"+data[i].tag+"' onclick='clickGrade(this)' class='btn btn-default btn-sm media-heading' >"+data[i].name+"</label> ");							
						});
					}else if(level=='1'){
						$.each(data, function(i) {
							$('#subjectContent').append("<label name='subject' value='"+data[i].tag+"' onclick='clickSubject(this)' class='btn btn-default btn-sm media-heading'>"+data[i].name+"</label> ");							
						});
					}else if(level=='3'){
						$.each(data, function(i) {
							$('#chapterContent').append("<label name='chapter' value='"+data[i].tag+"' onclick='clickChapter(this)' class='btn btn-default btn-sm media-heading'>"+data[i].name+"</label> ");
						});
					}
				} else {
					//查無資料提示
				}
			},
			error : function(xhr, ajaxOptions, thrownError) {
				//查完後，將原本"查詢中"的提示訊息移除
				if(level=='0'){
					$('#gradeContent').html('');
				}else if(level=='1'){
					$('#subjectContent').html('');
				}else if(level=='3'){
					$('#chapterContent').html('');
				}
				console.log("資料傳輸發生錯誤：" + xhr.statusText + thrownError);
			}
		});
	}
	
	//點擊年級TAG，參數me為自己的element
	function clickGrade(me){
		if(gradeButton){
			$(gradeButton).removeClass( "active" );
			if(gradeButton!=me){
				gradeButton = me;
				$(gradeButton).addClass( "active" );
				$("#subjectGroup").show();
				$("#chapterGroup").hide();
				var tagValue = $(gradeButton).attr("value");
				queryTags(tagValue,"1");
			}else{
				gradeButton = null;
				$("#subjectGroup").hide();
				$("#chapterGroup").hide();
			}
		}else{
			gradeButton = me;
			$(gradeButton).addClass( "active" );
			$("#subjectGroup").show();
			$("#chapterGroup").hide();
			var tagValue = $(gradeButton).attr("value");
			queryTags(tagValue,"1");
		}
	}
	
	function clickSubject(me){
		if(subjectButton){
			$(subjectButton).removeClass( "active" );
			if(subjectButton!=me){
				subjectButton = me;
				$(subjectButton).addClass( "active" );
				$("#chapterGroup").show();
				var tagValue = $(subjectButton).attr("value");
				queryTags(tagValue,"3");
			}else{
				subjectButton = null;
				$("#chapterGroup").hide();
			}
		}else{
			subjectButton = me;
			$(subjectButton).addClass( "active" );
			$("#chapterGroup").show();
			var tagValue = $(subjectButton).attr("value");
			queryTags(tagValue,"3");
		}
	}
	
	function clickChapter(me){
		if(chapterButton){
			$(chapterButton).removeClass( "active" );
			if(chapterButton!=me){
				chapterButton = me;
				$(chapterButton).addClass( "active" );
			}else{
				chapterButton = null;
			}
		}else{
			chapterButton = me;
			$(chapterButton).addClass( "active" );
		}
	}
	
	function removeTag(me){
		$(me).parent("label").remove();
	}
	
	function addTag(){
		//年級code
		var gradeValue = $(".active[name='grade']").attr("value");
		//科目code
		var subjectValue = $(".active[name='subject']").attr("value");
		//章節code
		var chapterValue = $(".active[name='chapter']").attr("value");
		//code
		var tagValue;
		//名稱
		var tagName;
		//等級
		var level;
		if(gradeValue){
			//如果選的tag有年級...
			tagValue = gradeValue;
			tagName = $(".active[name='grade']").text();
			tagLevel = "0";
			if(subjectValue){
				//如果選的tag有科目...
				tagValue = subjectValue;
				tagName += ("/"+$(".active[name='subject']").text());
				tagLevel = "1";
				if(chapterValue){
					//如果選的tag有章節...
					tagValue = chapterValue;
					tagName += ("/"+$(".active[name='chapter']").text());
					tagLevel = "3";
				}
			}
		}
		if(tagValue){
			//如果tagValue有值則新增tag，在label裡面塞 name,code,level，submit 表單時會將label裡的值組成JSON字串回傳
			$('#tagContent').append("<label class='btn-sm tag-box'>"+tagName+"<a href='#' class='tag-remove fa fa-remove' onclick='removeTag(this);return false;'></a><input type='hidden' name='code' value='"+tagValue+"'><input type='hidden' name='name' value='"+tagName+"'><input type='hidden' name='level' value='"+tagLevel+"'></label> ");
		}
	}
	function addProps() {
		//在submit 表單前，將選取的TAG組成JSON格式的字串塞進tagsValue欄位
		var tagsValue = [];
		//將tag的區塊底下所有的label 迴圈
		$("#tagContent").children('label').each(function () {
			//塞進陣列裡
			tagsValue.push({
				"code":$(this).children("[name='code']").val(),
				"name":$(this).children("[name='name']").val(),
				"level":$(this).children("[name='level']").val()
			});
		});
		$("#tagsValue").val(JSON.stringify(tagsValue));
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#propsForm").submit();
        }, 500);
	}
	function goQuery() {
		$.LoadingOverlay("show");
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="propsForm" name="propsForm"
				action="${pageContext.request.contextPath}/props/add" method="post"
				enctype="multipart/form-data" class="form-horizontal">
				<sessionConversation:insertSessionConversationId
					attributeName="propsForm" />
				<form:hidden path="method" value="save" />
				<form:hidden path="tagsValue"/>
				<div class="box-body">
					<div class="form-group">
						<label for="materialNum" class="col-sm-2 control-label">*料號</label>
						<div class="col-sm-10">
							<form:input path="materialNum" class="form-control"
								placeholder="料號" maxlength="30" />
							<form:errors path="materialNum" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="propsName" class="col-sm-2 control-label">*商品名稱</label>
						<div class="col-sm-10">
							<form:input path="propsName" class="form-control"
								placeholder="商品名稱" maxlength="30" />
							<form:errors path="propsName" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="quantity" class="col-sm-2 control-label">*商品數量</label>
						<div class="col-sm-10">
							<form:input path="quantity" class="form-control"
								placeholder="商品數量" maxlength="5" />
							<form:errors path="quantity" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="active" class="col-sm-2 control-label">*狀態</label>
						<div class="col-sm-10">
							<form:radiobutton path="active" id="radio1" value="1"
								checked="checked" />
							<label for="radio1">可上架</label><br />
							<form:radiobutton path="active" id="radio2" value="0" />
							<label for="radio2">不可上架</label>
						</div>
					</div>
					<%-- 
					<div class="form-group date">
						<label for="publishSDate" class="col-sm-2 control-label">*上架日期</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#publishSDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="publishSDate"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="publishSDate" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="publishEDate" class="col-sm-2 control-label">*下架日期</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#publishEDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="publishEDate"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="publishEDate" class="text-red" />
							</div>
						</div>
					</div>
					--%>
					<c:if test="${user_auth.admin}">
						<div class="form-group">
							<label for="vendorUid" class="col-sm-2 control-label">*供貨廠商</label>
							<div class="col-sm-10">
								<form:select path="vendorUid" cssClass="form-control">
									<form:option value="">請選擇</form:option>
									<form:options items="${propsForm.vendorList}"
										itemLabel="vendorName" itemValue="uid" />
								</form:select>
								<form:errors path="vendorUid" class="text-red" />
							</div>
						</div>
					</c:if>
					<c:if test="${not user_auth.admin}">
						<form:hidden path="vendorUid" value="${propsForm.vendorUid }" />
						<div class="form-group">
							<label for="vendorName" class="col-sm-2 control-label">*供貨廠商</label>
							<div class="col-sm-10">
								<form:input path="vendorName" class="form-control"
									placeholder="供貨廠商" maxlength="50" readonly="true" />
								<form:errors path="vendorName" class="text-red" />
							</div>
						</div>
					</c:if>
					<div class="form-group">
						<label for="photoOneId" class="col-sm-2 control-label">*圖片1<br>(建議尺寸700x430或等比例<br>但不得小於建議尺寸)</label>
						<div class="col-sm-10">
							<e7:previewImage attribute="photoOneId" width="700px" height="430px"
								fileStorage="${propsForm.photoOneId}" />
							<form:errors path="photoOneId" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="photoTwoId" class="col-sm-2 control-label">圖片2<br>(建議尺寸700x430或等比例<br>但不得小於建議尺寸)</label>
						<div class="col-sm-10">
							<e7:previewImage attribute="photoTwoId" width="700px" height="430px"
								fileStorage="${propsForm.photoTwoId}" />
							<form:errors path="photoTwoId" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="photoThirdId" class="col-sm-2 control-label">圖片3<br>(建議尺寸700x430或等比例<br>但不得小於建議尺寸)</label>
						<div class="col-sm-10">
							<e7:previewImage attribute="photoThirdId" width="700px" height="430px"
								fileStorage="${propsForm.photoThirdId}" />
							<form:errors path="photoThirdId" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="photoFourthId" class="col-sm-2 control-label">圖片4<br>(建議尺寸700x430或等比例<br>但不得小於建議尺寸)</label>
						<div class="col-sm-10">
							<e7:previewImage attribute="photoFourthId" width="700px" height="430px"
								fileStorage="${propsForm.photoFourthId}" />
							<form:errors path="photoFourthId" class="text-red" />
						</div>
					</div>
					<%-- <div class="form-group">
						<label for="costPrice" class="col-sm-2 control-label">*進價</label>
						<div class="col-sm-10">
							<form:input path="costPrice" class="form-control" maxlength="8"
								placeholder="進價" />
							<form:errors path="costPrice" class="text-red" />
						</div>
					</div> --%>
					<div class="form-group">
						<label for="price" class="col-sm-2 control-label">*定價</label>
						<div class="col-sm-10">
							<form:input path="price" class="form-control" maxlength="8"
								placeholder="原價" />
							<form:errors path="price" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="discountPrice" class="col-sm-2 control-label">*優惠價</label>
						<div class="col-sm-10">
							<form:input path="discountPrice" class="form-control"
								maxlength="8" placeholder="優惠價" />
							<form:errors path="discountPrice" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="propsSummary" class="col-sm-2 control-label">*商品簡述</label>
						<div class="col-sm-10">
							<form:textarea path="propsSummary" placeholder="商品簡述"
								class="form-control" maxlength="300" />
							<form:errors path="propsSummary" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="propsIntroduction" class="col-sm-2 control-label">*商品說明</label>
						<div class="col-sm-10">
							<%-- <form:textarea path="propsIntroduction" class="form-control" /> --%>
							<textarea id="propsIntroduction" name="propsIntroduction"
								class="form-control">
								${e7:previewHtmlImg(pageContext.request.contextPath,propsForm.propsIntroduction)}
							</textarea>
							<form:errors path="propsIntroduction" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="tagContent" class="col-sm-2 control-label">*教具分類</label>
						<div class="col-sm-10">
							<div class="form-group">
								<div class="col-sm-offset-1 col-sm-11" id="tagContent">
									<c:forEach items="${propsForm.tags}" var="tag" varStatus="loop">
										<label class='btn-sm tag-box'>${tag.name}<a href='#'
											class='tag-remove fa fa-remove'
											onclick='removeTag(this);return false;'></a><input
											type='hidden' name='code' value='${tag.code}'><input
											type='hidden' name='name' value='${tag.name}'><input
											type='hidden' name='level' value='${tag.level}'></label>
									</c:forEach>
								</div>
							</div>
							<div class="form-group" id="gradeGroup">
								<label class="col-sm-1 control-label">年級</label>
								<div class="col-sm-11" id="gradeContent"></div>
							</div>
							<div class="form-group" id="subjectGroup">
								<label class="col-sm-1 control-label">科目</label>
								<div class="col-sm-11" id="subjectContent"></div>
							</div>
							<div class="form-group" id="chapterGroup">
								<label class="col-sm-1 control-label">章節</label>
								<div class="col-sm-11" id="chapterContent"></div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-1 col-sm-11">
									<button type="button" class="btn btn-sm btn-warning"
										onclick="addTag()">新增分類</button>
								</div>
							</div>
							<div class="form-group" id="categoryGroup">
								<label class="col-sm-1 control-label">類別</label>
								<div class="col-sm-11 checkbox" id="categoryContent">
									<c:forEach items="${propsForm.categoryList}" var="category">
										<label>
											<form:checkbox path="categories" value="${category.categoryId}" />${category.categoryName}
										</label>
									</c:forEach>
								</div>
							</div>
							<form:errors path="categories" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="notice" class="col-sm-2 control-label">其他注意事項</label>
						<div class="col-sm-10">
							<form:textarea path="notice" class="form-control"
								placeholder="其他注意事項" maxlength="50" />
							<form:errors path="notice" class="text-red" />
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="button" onclick="addProps()" class="btn btn-info">儲存</button>
					<a href="${pageContext.request.contextPath}/props/query#result"
						class="btn btn-default pull-right" onclick="goQuery();">取消</a>
				</div>
				<!-- /.box-footer -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>