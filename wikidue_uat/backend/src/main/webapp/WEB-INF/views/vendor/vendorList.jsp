<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<script type="text/javascript">
	function query() {
		$("#method").val("query");
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#vendorForm").submit();
        }, 500);
	}

	function remove(uid) {
		if (confirm('確認刪除？')) {
			$("#uid").val(uid);
			document.vendorForm.action = '${pageContext.request.contextPath}/vendor/remove';
			$("#method").val("remove");
			$.LoadingOverlay("show");
	        setTimeout(function(){
	        	$("#vendorForm").submit();
	        }, 500);
		}
	}
	function goModify(uid) {
		$("#uid").val(uid);
		document.vendorForm.action = '${pageContext.request.contextPath}/vendor/modify';
		document.vendorForm.vendorForm_cId.value="";
		$("#method").val("");
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#vendorForm").submit();
        }, 500);
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="vendorForm" name="vendorForm"
				action="${pageContext.request.contextPath}/vendor/query#result"
				class="form-horizontal">
				<sessionConversation:insertSessionConversationId
					attributeName="vendorForm" />
				<form:hidden path="method" />
				<div class="box-body">
					<div class="form-group">
							<label for="vendorCode" class="col-sm-2 control-label">廠商編號</label>
							<div class="col-sm-10">
								<form:input path="vendorCode" class="form-control" placeholder="廠商編號"
									maxlength="50" />
								<form:errors path="vendorCode" class="text-red" />
							</div>
						</div>
					<div class="form-group">
						<label for="uid" class="col-sm-2 control-label">登入帳號(email)</label>
						<div class="col-sm-10">
							<form:input path="uid" class="form-control" placeholder="登入帳號(email)"
								maxlength="30" />
							<form:errors path="uid" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="vendorName" class="col-sm-2 control-label">廠商名稱</label>
						<div class="col-sm-10">
							<form:input path="vendorName" cssClass="form-control"
								placeholder="廠商名稱" maxlength="50" />
						</div>
					</div>
					<%-- <div class="form-group">
						<label for="vendorType" class="col-sm-2 control-label">廠商分類</label>
						<div class="col-sm-10">
							<form:select path="vendorType" cssClass="form-control">
								<form:option value="">請選擇</form:option>
								
								<form:option value="PRODUCT">商品</form:option>
								<form:option value="ADVERTISEMENT">廣告</form:option>
								<form:option value="RECOMMAND">推薦</form:option>
								
								<form:options items="${vendorForm.vendorTypeList}"
									itemLabel="displayName" />
							</form:select>
						</div>
					</div> --%>
					<button type="button" onclick="query()" class="btn btn-default pull-right">查詢</button>
				</div>
				<!-- /.box-body -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->

		<c:if test="${not empty vendorForm.result}">
			<div class="box" id="resultList">
				<div class="box-header">
					<a href="#" name="result"></a>
					<h3 class="box-title">查詢結果</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="dataTables_wrapper form-inline dt-bootstrap">
						<display:table name="vendorForm.result.content"
							pagesize="${vendorForm.result.size}"
							requestURI='?method=query&vendorForm_cId=${curr_vendorForm_cId}#result'
							id="list" cellspacing="0" cellpadding="0"
							class="table table-bordered table-hover" partialList="true"
							size="${vendorForm.result.totalElements.intValue()}"
							excludedParams="*">
							<display:column title='廠商編號' property="vendorCode" sortable="true"/>
							<display:column title='廠商名稱' property="vendorName" sortable="true"/>
							<display:column title='聯絡人' property="contact" sortable="true"/>
							<display:column title='聯絡電話'  sortable="true">
							${list.areaCode }${list.telphone }
							<c:if test="${not empty list.ext }">#${list.ext }</c:if>							
							</display:column>
							<display:column title='聯絡手機' property="mobile" sortable="true"/>
							<display:column title='email' property="email" sortable="true"/>
							<!-- 有修改權限才顯示 -->
							<%-- <sec:authorize url="/account/modify"> --%>
							<display:column title='操作'>
								<button type="button" onclick="goModify('${list.uid}');"
									class="btn btn-success btn-sm">修改</button>
								<button type="button" onclick="remove('${list.uid}');"
									class="btn btn-danger btn-sm">刪除</button>
							</display:column>
							<%-- </sec:authorize> --%>
						</display:table>
					</div>
				</div>
			</div>
		</c:if>
	</div>
</div>
