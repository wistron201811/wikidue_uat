<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 預覽上傳的圖片 -->
<script
	src="${pageContext.request.contextPath}/plugins/uploadPreview/jquery.uploadPreview.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
<style>
.areaCode {
	width: 70px;
	display: inline-block;
	margin-right: 5px;
}

.telphone {
	width: 120px;
	display: inline-block;
	margin-right: 5px;
}

 #bankCode{
 	width: 100px;
	display: inline-block;
	margin-right: 5px;
 }
 
  #account{
 	width: 200px;
	display: inline-block;
	margin-right: 5px;
 }
</style>
<script type="text/javascript">
	$(function() {
		CKEDITOR.replace('vendorIntroduction');
		
		$('.datepicker').datepicker({
			format : 'yyyy/mm/dd',
			todayBtn : true,
			todayHighlight : true,
			autoclose : true
		});
		
		$('#twzipcode').twzipcode({
			'countyName' : 'county', // 預設值為 county
			'districtName' : 'district', // 預設值為 district
			'zipcodeName' : 'zipcode', // 預設值為 zipcode
			//'zipcodeIntoDistrict':true, // 隱藏郵遞區號輸入框，並顯示於鄉鎮市區清單內。
			'readonly' : true
		});

		$('#twzipcode select').addClass("form-control input-sm twzip-group");
		$('#twzipcode input').addClass("form-control input-sm twzip-group");
		
		//init
		fillAddress();
		
	});
	
	//鄉鎮預設
	function fillAddress() {
		if ('${vendorForm.county}' != '') {
			$('#twzipcode').twzipcode('set', {
				'county' : '${vendorForm.county}'
			});
			$('#twzipcode').twzipcode('set', {
				'district' : '${vendorForm.district}'
			});
			$('#twzipcode').twzipcode('set', {
				'zipcode' : '${vendorForm.zipcode}'
			});
			//$('#twzipcode').find('select[name="county"]').val('${vendorForm.county}').prop('selected', true).trigger('change');
			//$('#twzipcode').find('select[name="district"]').val('${vendorForm.district}').prop('selected', true).trigger('change');
			//$('#twzipcode').find('select[name="zipcode"]').val('${vendorForm.zipcode}');
		}
	}
	
	function addVendor() {
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#vendorForm").submit();
        }, 500);
	}
	function goQuery() {
		$.LoadingOverlay("show");
	}
</script>


<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="vendorForm" name="vendorForm"
				action="${pageContext.request.contextPath}/vendor/add" method="post"
				enctype="multipart/form-data" class="form-horizontal">
				<sessionConversation:insertSessionConversationId
					attributeName="vendorForm" />
				<form:hidden path="method" value="save" />
				<div class="box-body">
					<div class="form-group">
						<label for="vendorCode" class="col-sm-2 control-label">廠商編號</label>
						<div class="col-sm-10">
							<form:input path="vendorCode" class="form-control"
								placeholder="廠商編號" maxlength="30" />
							<form:errors path="vendorCode" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="uid" class="col-sm-2 control-label">*登入帳號(email)</label>
						<div class="col-sm-10">
							<form:input path="uid" class="form-control" placeholder="登入帳號(email)"
								maxlength="50" />
							<form:errors path="uid" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="vendorName" class="col-sm-2 control-label">*廠商名稱</label>
						<div class="col-sm-10">
							<form:input path="vendorName" class="form-control"
								placeholder="請輸入廠商的完整名稱" maxlength="50" />
							<form:errors path="vendorName" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="courseSplitRatio" class="col-sm-2 control-label">*線上課程進價成數</label>
						<div class="col-sm-10">
							<form:input path="courseSplitRatio" class="form-control" maxlength="3"
								placeholder="請輸入0~100(含)之間的數字" />
							<form:errors path="courseSplitRatio" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="propsSplitRatio" class="col-sm-2 control-label">*實體商品進價成數</label>
						<div class="col-sm-10">
							<form:input path="propsSplitRatio" class="form-control" maxlength="3"
								placeholder="請輸入0~100(含)之間的數字" />
							<form:errors path="propsSplitRatio" class="text-red" />
						</div>
					</div>
					<%-- <div class="form-group">
						<label for="vendorType" class="col-sm-2 control-label">廠商分類</label>
						<div class="col-sm-10">
							<form:select path="vendorType" class="form-control">
								<form:option value="">請選擇</form:option>
								
								<form:option value="PRODUCT">商品</form:option>
								<form:option value="ADVERTISEMENT">廣告</form:option>
								<form:option value="RECOMMAND">推薦</form:option>
								
								<form:options items="${vendorForm.vendorTypeList}"
									itemLabel="displayName" />
							</form:select>
							<form:errors path="vendorType" class="text-red" />
						</div>
					</div> --%>
					<div class="form-group">
						<label for="photo" class="col-sm-2 control-label">廠商商標圖片<br>(建議尺寸500x500)</label>
						<div class="col-sm-10">
							<e7:previewImage attribute="photo"
								fileStorage="${vendorForm.photo}" height="500px" width="500px"/>
							<form:errors path="photo" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="contact" class="col-sm-2 control-label">聯絡人</label>
						<div class="col-sm-10">
							<form:input path="contact" class="form-control"
								placeholder="聯絡人" maxlength="50" />
							<form:errors path="contact" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="telphone" class="col-sm-2 control-label">聯絡電話</label>
						<div class="col-sm-10">
							<form:input path="areaCode" class="form-control areaCode" placeholder="區碼"
								maxlength="3" />
							<form:input path="telphone" class="form-control telphone" placeholder="電話"
								maxlength="8" />
							<form:input path="ext" class="form-control areaCode" placeholder="分機"
								maxlength="5" />
							<br />
							<form:errors path="telphone" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="companyPhone" class="col-sm-2 control-label">公司電話</label>
						<div class="col-sm-10">
							<form:input path="areaCode1" class="form-control areaCode"
								placeholder="區碼" maxlength="3" />
							<form:input path="companyPhone" class="form-control telphone"
								placeholder="公司電話" maxlength="8" />
							<form:input path="ext1" class="form-control areaCode" placeholder="分機"
								maxlength="5" />
							<br />
							<form:errors path="companyPhone" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="mobile" class="col-sm-2 control-label">聯絡手機</label>
						<div class="col-sm-10">
							<form:input path="mobile" class="form-control" placeholder="聯絡手機"
								maxlength="10" />
							<form:errors path="mobile" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="col-sm-2 control-label">聯絡人email</label>
						<div class="col-sm-10">
							<form:input path="email" class="form-control" placeholder="聯絡人email"
								maxlength="50" />
							<form:errors path="email" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="faxNo" class="col-sm-2 control-label">傳真</label>
						<div class="col-sm-10">
							<form:input path="faxNo" class="form-control" placeholder="傳真"
								maxlength="10" />
							<form:errors path="faxNo" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="address" class="col-sm-2 control-label">公司地址</label>
						<div class="col-sm-10">
							<div id="twzipcode"></div>
							<form:input path="address" class="form-control"
								placeholder="詳細地址" maxlength="30" />
							<form:errors path="address" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="homepage" class="col-sm-2 control-label">廠商網頁</label>
						<div class="col-sm-10">
							<form:input path="homepage" class="form-control" placeholder="廠商網頁"
								maxlength="80" />
							<form:errors path="homepage" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="representative" class="col-sm-2 control-label">負責人</label>
						<div class="col-sm-10">
							<form:input path="representative" class="form-control" placeholder="負責人"
								maxlength="30" />
							<form:errors path="representative" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="taxId" class="col-sm-2 control-label">統一編號</label>
						<div class="col-sm-10">
							<form:input path="taxId" class="form-control" placeholder="統一編號"
								maxlength="8" />
							<form:errors path="taxId" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="bankCode" class="col-sm-2 control-label">匯款帳號</label>
						<div class="col-sm-10">
							<form:input path="bankCode" class="form-control" placeholder="銀行代碼"
								maxlength="3" />
							<form:input path="account" class="form-control" placeholder="匯款帳號"
								maxlength="20" /><br/>
							<form:errors path="bankCode" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="vendorIntroduction" class="col-sm-2 control-label">廠商簡介</label>
						<div class="col-sm-10">
							<%-- <form:textarea path="vendorIntroduction" class="form-control" /> --%>
							<textarea id="vendorIntroduction" name="vendorIntroduction" class="form-control">
								${e7:previewHtmlImg(pageContext.request.contextPath,vendorForm.vendorIntroduction)}
							</textarea>
							<form:errors path="vendorIntroduction" class="text-red" />
						</div>
					</div>
					<div class="form-group date">
						<label for="contractSDate" class="col-sm-2 control-label">合約開始日期</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#contractSDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="contractSDate"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="contractSDate" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="contractEDate" class="col-sm-2 control-label">合約結束日期</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#contractEDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="contractEDate"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="contractEDate" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="remark" class="col-sm-2 control-label">備註</label>
						<div class="col-sm-10">
							<form:input path="remark" class="form-control" placeholder="備註"
								maxlength="30" />
							<form:errors path="remark" class="text-red" />
						</div>
					</div>
					<%-- 
					<div class="form-group">
						<label for="active" class="col-sm-2 control-label">啟用</label>
						<div class="col-sm-10">
							<form:radiobutton path="active" id="radio1" value="1"
								checked="checked" />
							<label for="radio1">是</label><br />
							<form:radiobutton path="active" id="radio2" value="0" />
							<label for="radio2">否</label>
						</div>
					</div>
					--%>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="button" onclick="addVendor()" class="btn btn-info">儲存</button>
					<a href="${pageContext.request.contextPath}/vendor/query#result"
						class="btn btn-default pull-right" onclick="goQuery();">取消</a>
				</div>
				<!-- /.box-footer -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>