<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Wikidue Store 教育商城 管理後台 - <tiles:getAsString name="pageTitle"/></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!--Favicon-->
  <link rel="shortcut icon" href="${pageContext.request.contextPath}/dist/img/favico.png" type="image/png">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
       
  <link rel="stylesheet" href="${pageContext.request.contextPath}/dist/css/skins/skin-blue-light.min.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/datepicker/datepicker3.css">
  <!-- s -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/dist/css/e7learning.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/dist/css/fixed.css">
  

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <script src="${pageContext.request.contextPath}/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
  <!-- SlimScroll -->
  <script src="${pageContext.request.contextPath}/plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="${pageContext.request.contextPath}/plugins/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="${pageContext.request.contextPath}/dist/js/app.min.js"></script>
  <!-- datepicker -->
  <script src="${pageContext.request.contextPath}/plugins/datepicker/bootstrap-datepicker.js"></script>
  <!-- distpicker-->
  <script src="${pageContext.request.contextPath}/plugins/distpicker/distpicker.min.js"></script>
  <script src="${pageContext.request.contextPath}/plugins/distpicker/jquery.twzipcode-1.7.10.min.js"></script>
  <!-- Add fancyBox -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
  <script type="text/javascript" src="${pageContext.request.contextPath}/plugins/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
  <!-- Optionally add helpers - button, thumbnail and/or media -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
  <script type="text/javascript" src="${pageContext.request.contextPath}/plugins/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/plugins/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
 
 
  <script type="text/javascript" src="${pageContext.request.contextPath}/plugins/ckeditor/ckeditor.js"></script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/plugins/textarea-autogrow/textarea-autogrow.min.js"></script>
  
  <script type="text/javascript" src="${pageContext.request.contextPath}/plugins/twzipcode/jquery.twzipcode-1.7.14.js"></script>
  
  <!-- stemcell-->
  <script src="${pageContext.request.contextPath}/dist/js/e7learning.js"></script>
  <style>
	<!--
	.datepicker {
	   z-index: 9999 !important;
	}
	-->
  </style>
</head>
<!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
<!-- the fixed layout is not compatible with sidebar-mini -->
<body class="skin-blue-light">
<div class="pad margin">
<!-- Site wrapper -->
	<jsp:include page="/WEB-INF/views/common/warningMessage.jsp"/>
    <tiles:insertAttribute name="content" />
</div>

</body>
</html>
