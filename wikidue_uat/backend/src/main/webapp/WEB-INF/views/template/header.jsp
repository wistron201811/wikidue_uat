<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
  <header class="main-header">
    <!-- Logo -->
    <a href="${pageContext.request.contextPath}/index" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>W</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>wikidue store</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" data-toggle="modal" data-target="#userAuthDetail" class="dropdown-toggle">
              ${user_auth.name}
            </a>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" onclick="ssoLogout()" >登出</a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
