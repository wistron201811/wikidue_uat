<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">
			<e7:authorize
				urls="/banner/query,/banner/add,/banner/publish,/productAd/query,/productAd/add,/videoAd/query,/videoAd/add">
				<li class="treeview"><a href="#"> <i class="fa fa-files-o"></i>
						<span>首頁管理</span> <span class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
					<ul class="treeview-menu">
						<sec:authorize url="/banner/query">
							<li><a
								href="${pageContext.request.contextPath}/banner/query"> <i
									class="fa fa-circle-o"></i> <span>Banner列表</span>
							</a></li>
						</sec:authorize>
						<sec:authorize url="/banner/add">
							<li><a href="${pageContext.request.contextPath}/banner/add">
									<i class="fa fa-circle-o"></i> <span>新增Banner</span>
							</a></li>
						</sec:authorize>
						<sec:authorize url="/banner/publish">
							<li><a
								href="${pageContext.request.contextPath}/banner/publish"> <i
									class="fa fa-circle-o"></i> <span>Banner上下架</span>
							</a></li>
						</sec:authorize>
						<sec:authorize url="/productAd/query">
							<li><a
								href="${pageContext.request.contextPath}/productAd/query"> <i
									class="fa fa-circle-o"></i> <span>首頁主打產品列表</span>
							</a></li>
						</sec:authorize>
						<sec:authorize url="/productAd/add">
							<li><a
								href="${pageContext.request.contextPath}/productAd/add"> <i
									class="fa fa-circle-o"></i> <span>新增首頁主打產品</span>
							</a></li>
						</sec:authorize>
						<sec:authorize url="/videoAd/query">
							<li><a
								href="${pageContext.request.contextPath}/videoAd/query"> <i
									class="fa fa-circle-o"></i> <span>影片列表</span>
							</a></li>
						</sec:authorize>
						<sec:authorize url="/videoAd/add">
							<li><a href="${pageContext.request.contextPath}/videoAd/add">
									<i class="fa fa-circle-o"></i> <span>新增影片</span>
							</a></li>
						</sec:authorize>
					</ul></li>
			</e7:authorize>
			<e7:authorize
				urls="/adCategory/query,/adCategory/add,/ad/query,/ad/add">
				<li class="treeview"><a href="#"> <i class="fa fa-files-o"></i>
						<span>廣告管理</span> <span class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
					<ul class="treeview-menu">
						<sec:authorize url="/adCategory/query">
							<li><a
								href="${pageContext.request.contextPath}/adCategory/query">
									<i class="fa fa-circle-o"></i> <span>廣告分類列表</span>
							</a></li>
						</sec:authorize>
						<sec:authorize url="/adCategory/add">
							<li><a
								href="${pageContext.request.contextPath}/adCategory/add"> <i
									class="fa fa-circle-o"></i> <span>新增廣告分類</span>
							</a></li>
						</sec:authorize>
						<sec:authorize url="/ad/query">
							<li><a href="${pageContext.request.contextPath}/ad/query">
									<i class="fa fa-circle-o"></i> <span>廣告列表</span>
							</a></li>
						</sec:authorize>
						<sec:authorize url="/ad/add">
							<li><a href="${pageContext.request.contextPath}/ad/add">
									<i class="fa fa-circle-o"></i> <span>新增廣告</span>
							</a></li>
						</sec:authorize>
					</ul></li>
			</e7:authorize>
			<e7:authorize urls="/category/query,/category/add">
				<li class="treeview"><a href="#"> <i class="fa fa-files-o"></i>
						<span>產品屬性分類管理</span> <span class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
					<ul class="treeview-menu">
						<sec:authorize url="/category/query">
							<li><a
								href="${pageContext.request.contextPath}/category/query"> <i
									class="fa fa-circle-o"></i> <span>分類列表</span>
							</a></li>
						</sec:authorize>
						<sec:authorize url="/category/add">
							<li><a
								href="${pageContext.request.contextPath}/category/add"> <i
									class="fa fa-circle-o"></i> <span>新增分類</span>
							</a></li>
						</sec:authorize>
					</ul></li>
			</e7:authorize>
			<e7:authorize urls="/product/query,/product/add">
				<li class="treeview"><a href="#"> <i class="fa fa-files-o"></i>
						<span>商品管理</span> <span class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
					<ul class="treeview-menu">
						<sec:authorize url="/product/query">
							<li><a
								href="${pageContext.request.contextPath}/product/query"> <i
									class="fa fa-circle-o"></i> <span>商品列表</span>
							</a></li>
						</sec:authorize>
						<sec:authorize url="product/add">
							<li><a href="${pageContext.request.contextPath}/product/add">
									<i class="fa fa-circle-o"></i> <span>商品上架</span>
							</a></li>
						</sec:authorize>
					</ul></li>
			</e7:authorize>
			<e7:authorize urls="/course/query,/course/add">
				<li class="treeview"><a href="#"> <i class="fa fa-files-o"></i>
						<span>線上課程管理</span> <span class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
					<ul class="treeview-menu">
						<sec:authorize url="/course/query">
							<li><a
								href="${pageContext.request.contextPath}/course/query"> <i
									class="fa fa-circle-o"></i> <span>課程列表</span>
							</a></li>
						</sec:authorize>
						<sec:authorize url="/course/add">
							<li><a href="${pageContext.request.contextPath}/course/add">
									<i class="fa fa-circle-o"></i> <span>新增課程</span>
							</a></li>
						</sec:authorize>
					</ul></li>
			</e7:authorize>
			<e7:authorize urls="/props/query,/props/add">
				<li class="treeview"><a href="#"> <i class="fa fa-files-o"></i>
						<span>教具管理</span> <span class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
					<ul class="treeview-menu">
						<sec:authorize url="/props/query">
							<li><a href="${pageContext.request.contextPath}/props/query">
									<i class="fa fa-circle-o"></i> <span>教具列表</span>
							</a></li>
						</sec:authorize>
						<sec:authorize url="/props/add">
							<li><a href="${pageContext.request.contextPath}/props/add">
									<i class="fa fa-circle-o"></i> <span>新增教具</span>
							</a></li>
						</sec:authorize>
					</ul></li>
			</e7:authorize>
			<e7:authorize urls="/camp/query,/camp/add">
				<li class="treeview"><a href="#"> <i class="fa fa-files-o"></i>
						<span>營隊管理</span> <span class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
					<ul class="treeview-menu">
						<sec:authorize url="/camp/query">
							<li><a href="${pageContext.request.contextPath}/camp/query">
									<i class="fa fa-circle-o"></i> <span>營隊列表</span>
							</a></li>
						</sec:authorize>
						<sec:authorize url="/camp/add">
							<li><a href="${pageContext.request.contextPath}/camp/add">
									<i class="fa fa-circle-o"></i> <span>新增營隊</span>
							</a></li>
						</sec:authorize>
					</ul></li>
			</e7:authorize>
			<e7:authorize urls="/vendor/query,/vendor/add">
				<li class="treeview"><a href="#"> <i class="fa fa-files-o"></i>
						<span>廠商管理</span> <span class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
					<ul class="treeview-menu">
						<sec:authorize url="/vendor/query">
							<li><a
								href="${pageContext.request.contextPath}/vendor/query"> <i
									class="fa fa-circle-o"></i> <span>廠商列表</span>
							</a></li>
						</sec:authorize>
						<sec:authorize url="/vendor/add">
							<li><a href="${pageContext.request.contextPath}/vendor/add">
									<i class="fa fa-circle-o"></i> <span>新增廠商</span>
							</a></li>
						</sec:authorize>
					</ul></li>
			</e7:authorize>
			<e7:authorize urls="/teacher/query,/teacher/add">
				<li class="treeview"><a href="#"> <i class="fa fa-files-o"></i>
						<span>講師管理</span> <span class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
					<ul class="treeview-menu">
						<sec:authorize url="/teacher/query">
							<li><a
								href="${pageContext.request.contextPath}/teacher/query"> <i
									class="fa fa-circle-o"></i> <span>講師列表</span>
							</a></li>
						</sec:authorize>
						<sec:authorize url="/teacher/add">
							<li><a href="${pageContext.request.contextPath}/teacher/add">
									<i class="fa fa-circle-o"></i> <span>新增講師</span>
							</a></li>
						</sec:authorize>
					</ul></li>
			</e7:authorize>
			<e7:authorize urls="/order/query,/accounting/querySummary">
				<li class="treeview"><a href="#"> <i class="fa fa-files-o"></i>
						<span>訂單管理</span> <span class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
					<ul class="treeview-menu">
						<sec:authorize url="/order/query">
							<li><a href="${pageContext.request.contextPath}/order/query">
									<i class="fa fa-circle-o"></i> <span>訂單列表</span>
							</a></li>
						</sec:authorize>
						<sec:authorize url="/accounting/querySummary">
							<li><a
								href="${pageContext.request.contextPath}/accounting/querySummary">
									<i class="fa fa-circle-o"></i> <span>會計訂單總表</span>
							</a></li>
						</sec:authorize>
					</ul></li>
			</e7:authorize>
			<e7:authorize urls="/accusation/query">
				<li class="treeview"><a href="#"> <i class="fa fa-files-o"></i>
						<span>分享園地管理</span> <span class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
					<ul class="treeview-menu">
						<sec:authorize url="/accusation/query">
							<li><a
								href="${pageContext.request.contextPath}/accusation/query">
									<i class="fa fa-circle-o"></i> <span>分享園地列表</span>
							</a></li>
						</sec:authorize>
					</ul></li>
			</e7:authorize>
			<e7:authorize urls="/recommend/query,/recommend/add,/recommend/right">
				<li class="treeview"><a href="#"> <i class="fa fa-files-o"></i>
						<span>名人推薦管理</span> <span class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
					<ul class="treeview-menu">
						<sec:authorize url="/recommend/query">
							<li><a
								href="${pageContext.request.contextPath}/recommend/query"> <i
									class="fa fa-circle-o"></i> <span>名人推薦列表</span>
							</a></li>
						</sec:authorize>
						<sec:authorize url="/recommend/add">
							<li><a
								href="${pageContext.request.contextPath}/recommend/add"> <i
									class="fa fa-circle-o"></i> <span>新增名人推薦</span>
							</a></li>
						</sec:authorize>
						<sec:authorize url="/recommend/right">
							<li><a
								href="${pageContext.request.contextPath}/recommend/right"> <i
									class="fa fa-circle-o"></i> <span>右側區塊</span>
							</a></li>
						</sec:authorize>
					</ul></li>
			</e7:authorize>
			<e7:authorize urls="/accounting/queryMonthly">
				<li class="treeview"><a href="#"> <i class="fa fa-files-o"></i>
						<span>會計報表</span> <span class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
					<ul class="treeview-menu">
						<li><a
							href="${pageContext.request.contextPath}/accounting/queryMonthly">
								<i class="fa fa-circle-o"></i> <span>會計月結報表</span>
						</a></li>
					</ul></li>
			</e7:authorize>
			<sec:authorize url="/admin/only">
				<li><a
					href='<spring:eval expression="@configService.mapManagementUrl" />'
					target="_blank"> <i class="fa fa-files-o"></i> <span>地圖編輯</span>
				</a></li>
				<li><a
					href='<spring:eval expression="@configService.evaluationUploadUrl" />'
					target="_blank"> <i class="fa fa-files-o"></i> <span>試題上傳</span>
				</a></li>
				<li><a
					href='<spring:eval expression="@configService.evaluationManagementUrl" />'
					target="_blank"> <i class="fa fa-files-o"></i> <span>所有試題管理</span>
				</a></li>
				<li><a
					href='<spring:eval expression="@configService.accountManagementUrl" />'
					target="_blank"> <i class="fa fa-files-o"></i> <span>帳號權限編輯</span>
				</a></li>
			</sec:authorize>
			<li>
				<a href='http://bit.ly/Wikidue' target="_blank">
					<i class="fa fa-files-o"></i> <span>廠商專區</span>
				</a>
			</li>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>