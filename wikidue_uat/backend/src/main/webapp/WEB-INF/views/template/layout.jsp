<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Wikidue Store 教育商城管理後台 - <tiles:getAsString name="pageTitle"/></title>
  <meta name="wikidue version" content="<spring:eval expression="@configService.applicationVersion" />">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!--Favicon-->
  <link rel="shortcut icon" href="${pageContext.request.contextPath}/dist/img/favico.png" type="image/png">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
       
  <link rel="stylesheet" href="${pageContext.request.contextPath}/dist/css/skins/skin-blue-light.min.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/datepicker/datepicker3.css">
  <!-- s -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/dist/css/e7learning.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/dist/css/fixed.css">
  

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <script src="${pageContext.request.contextPath}/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- Bootstrap 3.3.6 -->
  <script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
  <!-- SlimScroll -->
  <script src="${pageContext.request.contextPath}/plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="${pageContext.request.contextPath}/plugins/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="${pageContext.request.contextPath}/dist/js/app.min.js"></script>
  <!-- datepicker -->
  <script src="${pageContext.request.contextPath}/plugins/datepicker/bootstrap-datepicker.js"></script>
  <!-- distpicker-->
  <script src="${pageContext.request.contextPath}/plugins/distpicker/distpicker.min.js"></script>
  <script src="${pageContext.request.contextPath}/plugins/distpicker/jquery.twzipcode-1.7.10.min.js"></script>
  <!-- Add fancyBox -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
  <script type="text/javascript" src="${pageContext.request.contextPath}/plugins/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
  <!-- Optionally add helpers - button, thumbnail and/or media -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
  <script type="text/javascript" src="${pageContext.request.contextPath}/plugins/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/plugins/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
 
 
  <script type="text/javascript" src="${pageContext.request.contextPath}/plugins/ckeditor/ckeditor.js"></script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/plugins/textarea-autogrow/textarea-autogrow.min.js"></script>
  
  <script type="text/javascript" src="${pageContext.request.contextPath}/plugins/twzipcode/jquery.twzipcode-1.7.14.js"></script>
  
  <!-- stemcell-->
  <script src="${pageContext.request.contextPath}/dist/js/e7learning.js"></script>
  
  <script type="text/javascript" src="${pageContext.request.contextPath}/plugins/jquery-loading-overlay-2.1.3/loadingoverlay.min.js"></script>
  <style>
	<!--
	.datepicker {
	   z-index: 1000 !important;
	}
	-->
  </style>
</head>
<!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
<!-- the fixed layout is not compatible with sidebar-mini -->
<body class="hold-transition skin-blue-light fixed sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <tiles:insertAttribute name="header" /> 

  <!-- =============================================== -->

  <tiles:insertAttribute name="menu" />

  <!-- =============================================== -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       	<tiles:getAsString name="pageTitle"/>
        <small></small>
      </h1>
      <ol class="breadcrumb">
      </ol>
    </section>
	<jsp:include page="/WEB-INF/views/common/warningMessage.jsp"/>
    <!-- Main content -->
    <section class="content">
      <tiles:insertAttribute name="content" />
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  
  <tiles:insertAttribute name="footer" />
  
</div>
<!-- ./wrapper -->
<div class="modal fade" id="userAuthDetail" tabindex="-1" role="dialog" aria-labelledby="userModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">個人資料</h4>
      </div>
      <div class="modal-body">
		<table class="table">
			<tr>
				<th width="30%">會員編號</th>
				<td>${user_auth.userId}</td>
			</tr>
			<tr>
				<th>姓名</th>
				<td>${user_auth.name}</td>
			</tr>
			<tr>
				<th>電話</th>
				<td>${user_auth.mobile}</td>
			</tr>
			<tr>
				<th>email</th>
				<td>${user_auth.email}</td>
			</tr>
			<tr>
				<th>角色</th>
				<td>
					<c:forEach items="${user_auth.authorities}" var="role">
						${role}<br/>
					</c:forEach>
				</td>
			</tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
      </div>
    </div>
  </div>
</div>
<form name="logoutForm" action="${sso_logout_page}">
	<input type="hidden" name="series" value="${user_auth.series}"/>
	<input type="hidden" name="redirect_uri" value="${logout_redirect_uri}"/>
</form>
<script>
	$(window).unload(function() {
		$.LoadingOverlay("hide");
	});
	var path = window.location.pathname;
	/* //帳號管理
	if(path.indexOf('${pageContext.request.contextPath}/account') > -1){
		path = '${pageContext.request.contextPath}/account/query';
	}
	//客戶檔案
	else if(path.indexOf('${pageContext.request.contextPath}/customer') > -1){
		path = '${pageContext.request.contextPath}/customer/query';
	}
	//收案作業
	else if(path.indexOf('${pageContext.request.contextPath}/case') > -1){
		path = '${pageContext.request.contextPath}/case/query';
	}
	//客戶追蹤
	else if(path.indexOf('${pageContext.request.contextPath}/service') > -1){
		path = '${pageContext.request.contextPath}/service/query';
	}
	//報告作業
	else if(path.indexOf('${pageContext.request.contextPath}/report') > -1){
		path = '${pageContext.request.contextPath}/report/query';
	}
	//庫存管理-入庫 
	else if(path.indexOf('${pageContext.request.contextPath}/stock/in') > -1){
		path = '${pageContext.request.contextPath}/stock/in/query';
	}
	//庫存管理-修改儲位位置 
	else if(path.indexOf('${pageContext.request.contextPath}/stock/replace') > -1){
		path = '${pageContext.request.contextPath}/stock/replace';
	}
	//庫存管理
	else if(path.indexOf('${pageContext.request.contextPath}/stock') > -1){
		path = '${pageContext.request.contextPath}/stock/query';
	} */
	
	
	//打開目前選單
	$('a[href="'+path+'"]').parents('li').addClass('active');
	//breadcrumb
	var breadcrumb = "";
	$('a[href="'+path+'"]').parents('li').each(function (i,value){
		breadcrumb = "<li>"+$(value).children('a').children('span').html()+"</li>"+breadcrumb;
	});
	$('.breadcrumb').append('<li><a href="#"><i class="fa fa-dashboard"></i> 首頁</a></li>'+breadcrumb);
	
	$('#pagebanner').html($('#showbanner').html());
	$('#showbanner').html('');

	function changePage(url){
		$.LoadingOverlay("show");
        setTimeout(function(){
        		location.href = url;
        }, 500);
	}
	
	function loading(boxElement){
		$("#"+boxElement+" .box-body").after('<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>');
	}
	function removeLoading(boxElement){
		$("#"+boxElement+" .overlay").remove();
	}
	$(function(){
		// 隱藏沒有子選單的選單
		$('.treeview').each(function( index , value) {
		  if($(value).find('.treeview-menu li').length<1)$(value).remove();
		});
		
		$.each($('a'),function(i,v){
			if($(v).attr('href')!='#'&&$('a')){
				var tmp = $(v).attr('href');
				if(tmp.indexOf('?')>-1){
					$(v).attr('href',tmp.indexOf('#')>-1?(tmp.substring(0,tmp.indexOf('#'))+'&d='+new Date().getTime()+tmp.substring(tmp.indexOf('#'))):(tmp+'&d='+new Date().getTime()));
				}else
					$(v).attr('href',tmp.indexOf('#')>-1?(tmp.substring(0,tmp.indexOf('#'))+'?d='+new Date().getTime()+tmp.substring(tmp.indexOf('#'))):(tmp+'?d='+new Date().getTime()));
			}}
		);

		$('.pagination a').each(function(i,v){
			var href = $(v).attr('href');
			if(href!="#")
				$(v).click(function() {
					changePage(href);
				});
			$(v).attr('href','javascript:;');
		});
	});

	function ssoLogout(){
		document.logoutForm.submit();
	}
</script>
</body>
</html>
