<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<div class="error-page">
  <h2 class="headline text-red">404</h2>

  <div class="error-content">
    <h3><i class="fa fa-warning text-red"></i> 系統發生錯誤.</h3>
    <p>
      	找不到此頁面
    </p>
  </div>
</div>
<!-- /.error-page -->