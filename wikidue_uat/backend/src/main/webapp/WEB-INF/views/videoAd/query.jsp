<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<script>
	$(function() {
	});
	function query(){
		$("#method").val("query");
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#videoAdForm").submit();
        }, 500);
	}
	function goModify(videoAdId) {
		$("#videoAdId").val(videoAdId);
		document.videoAdForm.action = '${pageContext.request.contextPath}/videoAd/modify';
		document.videoAdForm.videoAdForm_cId.value="";
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#videoAdForm").submit();
        }, 500);
	}
	function remove(videoAdId) {
		if (confirm('確認刪除？')) {
			$("#videoAdId").val(videoAdId);
			$("#method").val("remove");
			$.LoadingOverlay("show");
	        setTimeout(function(){
	        	$("#videoAdForm").submit();
	        }, 500);
		}
	}
</script>
<form:form modelAttribute="videoAdForm" name="videoAdForm"
	action="${pageContext.request.contextPath}/videoAd/query#result" method="post"
	autoComplete="Off">
	<input type="hidden" name="method" id="method" />
	<input type="hidden" name="videoAdId" id="videoAdId" />
	<sessionConversation:insertSessionConversationId attributeName="videoAdForm"/>
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-body">
					<div class="form-group">
						<label for="title">影片廣告名稱</label>
						<form:input path="title" cssClass="form-control"
							placeholder="影片廣告名稱" />
					</div>
					<div class="form-group">
						<button onclick="query()" class="btn btn-default pull-right">查詢</button>
					</div>
				</div>
				</div>
				<c:if test="${not empty videoAdForm.result}">
				<div class="box" id="resultList">
					<div class="box-header">
						<a href="#" id="result"></a>
						<h3 class="box-title">查詢結果</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="dataTables_wrapper form-inline dt-bootstrap">
							<display:table name="videoAdForm.result.content" pagesize="${videoAdForm.result.size}"
								partialList="true" requestURI='?method=query&videoAdForm_cId=${curr_videoAdForm_cId}#result' id="videoAd"
								cellspacing="0" cellpadding="0"
								class="table table-bordered table-hover" size="${videoAdForm.result.totalElements.intValue()}"
								excludedParams="*">
								<display:column title='狀態 ' property="title" sortable="true"/>
								<display:column title='狀態 ' sortable="true">
									<c:if test="${videoAd.active=='1'}">
										<p class="text-success">已上架</p>
									</c:if>
									<c:if test="${videoAd.active=='0'}">
										<p class="text-danger">未上架</p>
									</c:if>
								</display:column>							
								<display:column title='操作'>
									<!-- 有修改權限才顯示 -->
									<sec:authorize url="/videoAd/modify">
									<button type="button"
										 onclick="goModify('${videoAd.pkVideoAd}');"
										class="btn btn-primary btn-sm">修改</button>
									<button type="button" onclick="remove('${videoAd.pkVideoAd}');" class="btn btn-danger btn-sm">刪除</button>
									</sec:authorize>
	
								</display:column>
							</display:table>
						</div>
					</div>
				</div>
				</c:if>
			</div>
		</div>
</form:form>