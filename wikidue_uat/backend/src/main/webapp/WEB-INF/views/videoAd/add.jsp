<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 預覽上傳的圖片 -->
<script
	src="${pageContext.request.contextPath}/plugins/uploadPreview/jquery.uploadPreview.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>

<script type="text/javascript">
	$(function() {

	});
	function save() {
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#videoAdForm").submit();
        }, 500);
	}
	function goQuery() {
		$.LoadingOverlay("show");
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="videoAdForm" name="videoAdForm" enctype="multipart/form-data" 
				action="${pageContext.request.contextPath}/videoAd/add" method="post"
				 class="form-horizontal">
				<form:hidden path="method" value="add" />
				<sessionConversation:insertSessionConversationId
					attributeName="videoAdForm" />
				<div class="box-body">
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">*標題</label>
						<div class="col-sm-10">
							<form:input path="title" class="form-control"
								placeholder="影片廣告標題" maxlength="30" />
							<form:errors path="title" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="image" class="col-sm-2 control-label">*預覽圖片<br>(建議尺寸700x430或等比例<br>但不得小於建議尺寸)</label>
						<div class="col-sm-10">
							<form:errors path="image" class="text-red" />
							<e7:previewImage attribute="image" fileStorage="${videoAdForm.image}" width="700px" height="430px"/>
						</div>
					</div>
					<div class="form-group">
						<label for="videoUrl" class="col-sm-2 control-label">*Youtube影片ID</label>
						<div class="col-sm-10">
							<form:input path="videoUrl" class="form-control"
								placeholder="https://www.youtube.com/watch?v=xxxxxxx 請填v=後面的字串" maxlength="20" />
							<form:errors path="videoUrl" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="contentUrl" class="col-sm-2 control-label">*連結網址</label>
						<div class="col-sm-10">
							<form:input path="contentUrl" class="form-control"
								placeholder="連結網址" maxlength="200" />
							<form:errors path="contentUrl" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="brief" class="col-sm-2 control-label">*影片廣告簡介</label>
						<div class="col-sm-10">
							<form:textarea path="brief" class="form-control"
								placeholder="影片廣告簡介" maxlength="200" ></form:textarea>
							<form:errors path="brief" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="active" class="col-sm-2 control-label">啟用</label>
						<div class="col-sm-10">
							<form:radiobutton path="active" id="radio1" value="1"
								checked="checked" />
							<label for="radio1">是</label><br />
							<form:radiobutton path="active" id="radio2" value="0" />
							<label for="radio2">否</label>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="button" onclick="save()" class="btn btn-info">儲存</button>
					<a href="${pageContext.request.contextPath}/videoAd/query"
						class="btn btn-default pull-right" onclick="goQuery();">取消</a>
				</div>
				<!-- /.box-footer -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>