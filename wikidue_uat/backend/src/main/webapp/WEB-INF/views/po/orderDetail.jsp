<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
.headInfo {
	border-left: 3px solid #3c8dbc;
	font-size: 20px;
}

.subHeadInfo {
	border-left: 3px solid #FF8800;
	font-size: 16px;
}

.th-background {
	background-color: #DCDCDC;
}
</style>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/plugins/jquery-loading-overlay-2.1.3/loadingoverlay.min.js"></script>
<script type="text/javascript">
	function handleReturn(pkSubOrderDetail, status) {
		$("#pkSubOrderDetail").val(pkSubOrderDetail);
		$("#subOrderDetailStatus").val(status);
		$("#method").val("returnHandling");
		if (status == 'G') {
			if (confirm('確認已收到退貨商品？')) {
				$.LoadingOverlay("show");
				setTimeout(function() {
					$("#mainOrderForm").submit();
				}, 500);
			}
		} else if (status == 'J') {
			if (confirm('確認拒絕退件？')) {
				$.LoadingOverlay("show");
				setTimeout(function() {
					$("#mainOrderForm").submit();
				}, 500);
			}
		}
	}
	function cancelPop() {
		$("#detailVendorRemark").val("");
		$("#logistics").val("");
		$("#logisticsNum").val("");
	}
	function deliver() {
		var v1 = $("#logistics").val().trim();
		var v2 = $("#logisticsNum").val().trim();
		if (v1 == "") {
			alert("請輸入物流公司");
		} else if (v2 == "") {
			alert("請輸入物流編號");
		} else {
			document.mainOrderForm.action = '${pageContext.request.contextPath}/order/deliever';
			$("#method").val("modify");
			$.LoadingOverlay("show");
			setTimeout(function() {
				$("#mainOrderForm").submit();
			}, 500);
		}
	}
	function goRemark(pkSubOrderDetail, detailUserRemarkId,
			detailVendorRemarkId) {
		$("#pkSubOrderDetail").val(pkSubOrderDetail);
		var v1 = $("#" + detailUserRemarkId).val().trim();
		var v2 = $("#" + detailVendorRemarkId).val().trim();
		$("#detailUserRemark").val(v1);
		$("#detailVendorRemark").val(v2);
	}
	function saveVendorRemark() {
		document.mainOrderForm.action = '${pageContext.request.contextPath}/order/remark';
		$("#method").val("saveVendorRemark");
		$.LoadingOverlay("show");
		setTimeout(function() {
			$("#mainOrderForm").submit();
		}, 500);
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="mainOrderForm" name="mainOrderForm"
				action="${pageContext.request.contextPath}/order/return"
				class="form-horizontal">
				<sessionConversation:insertSessionConversationId
					attributeName="mainOrderForm" />
				<form:hidden path="method" />
				<form:hidden path="pkSubOrder" value="${subOrder.pkSubOrder}" />
				<form:hidden path="pkSubOrderDetail" />
				<form:hidden path="subOrderDetailStatus" />
				<!-- The Modal -->
				<div class="modal fade" id="remarkModal" role="dialog">
					<div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"
									onclick="cancelPop()">&times;</button>
								<h4 class="modal-title">備註</h4>
							</div>
							<div class="modal-body">
								<div class="form-group">
									<label for="detailUserRemark" class="col-sm-4 control-label">給消費者(前台)</label>
									<div class="col-sm-8">
										<textarea rows="3" id="detailUserRemark" class="form-control"
											placeholder="" readonly="readonly"></textarea>
									</div>
								</div>
							</div>
							<div class="modal-body">
								<div class="form-group">
									<label for="detailVendorRemark" class="col-sm-4 control-label">廠商備註</label>
									<div class="col-sm-8">
										<form:textarea path="detailVendorRemark" class="form-control"
											rows="3" maxlength="50" placeholder="廠商備註" />
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" onclick="saveVendorRemark()"
									class="btn btn-success pull-right">確認</button>
								<button type="button" class="btn btn-default"
									onclick="cancelPop()" data-dismiss="modal">取消</button>
							</div>
						</div>

					</div>
				</div>
				<!-- The Modal -->

				<!-- The Modal -->
				<div class="modal fade" id="myModal" role="dialog">
					<div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"
									onclick="cancelPop()">&times;</button>
								<h4 class="modal-title">出貨資訊</h4>
							</div>
							<div class="modal-body">
								<div class="form-group">
									<label for="logistics" class="col-sm-2 control-label">貨運公司</label>
									<div class="col-sm-10">
										<form:input path="logistics" class="form-control"
											placeholder="貨運公司" maxlength="50" />
										<form:errors path="logistics" class="text-red" />
									</div>
								</div>
								<div class="form-group">
									<label for="logisticsNum" class="col-sm-2 control-label">物流編號</label>
									<div class="col-sm-10">
										<form:input path="logisticsNum" class="form-control"
											placeholder="物流編號" maxlength="50" />
										<form:errors path="logisticsNum" class="text-red" />
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" onclick="deliver()"
									class="btn btn-success pull-right">確認</button>
								<button type="button" class="btn btn-default"
									onclick="cancelPop()" data-dismiss="modal">取消</button>
							</div>
						</div>

					</div>
				</div>
				<!-- The Modal -->

				<div class="box-body">
					<h1 class="main-title mt0 mb20">${subOrder.vendor.vendorName}</h1>
					<h3 class="headInfo">訂單資訊</h3>
					<table class="table table-bordered  table-striped text-center">
						<thead>
							<tr class="th-background">
								<td>主訂單編號</td>
								<td>廠商訂單編號</td>
								<td>訂單成立時間</td>
								<td>訂單金額</td>
								<td>主訂單狀態</td>
								<!-- <td>貨運公司</td>
								<td>物流編號</td> -->
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>${subOrder.mainOrder.pkMainOrder}</td>
								<td>${subOrder.pkSubOrder}</td>
								<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss"
										value="${subOrder.mainOrder.orderDt}" /></td>
								<td><fmt:formatNumber value="${subOrder.orderAmount}"
										type="number" /></td>
								<td><e7:code type="5"
										code="${subOrder.mainOrder.orderStatus}" /></td>
								<%-- <td>${subOrder.logistics}</td>
								<td>${subOrder.logisticsNum}</td> --%>
							</tr>
						</tbody>
					</table>
					<BR>
					<h3 class="headInfo">綠界付款資訊</h3>
					<table class="table table-bordered  table-striped text-center">
						<thead>
							<tr class="th-background">
								<td>綠界訂單號碼</td>
								<td>發票號碼</td>
								<td>付款方式</td>
								<!-- <td>繳款金額</td> -->
								<td>付款狀態</td>
							</tr>
						</thead>
						<tbody>
							<c:if test="${subOrder.mainOrder.payment.status == 'P'}">
								<tr>
									<td>${subOrder.mainOrder.payment.tradeNo}</td>
									<td>${subOrder.mainOrder.payment.invoiceNo}</td>
									<td><e7:getPaymentType type="${subOrder.mainOrder.payment.paymentType}"/></td>
									<%-- <td><fmt:formatNumber value="${subOrder.payment.tradeAmt}"
										type="number" /></td> --%>
									<td><e7:code type="5"
											code="${subOrder.mainOrder.payment.status}" /></td>
								</tr>
							</c:if>
						</tbody>
					</table>
					<BR>
					<h3 class="headInfo">購買人資訊</h3>
					<table class="table table-bordered  table-striped text-center">
						<thead>
							<tr class="th-background">
								<td>購買人</td>
								<td>收件人</td>
								<td>連絡電話</td>
								<td>送貨地址</td>
								<!-- <td>出貨時間</td> -->
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>${subOrder.mainOrder.purchaser}<BR>${subOrder.mainOrder.purchaserId}</td>
								<td>${subOrder.mainOrder.recipient}</td>
								<td>${subOrder.mainOrder.mobile}<%-- <br>${subOrder.mainOrder.telphone} --%>
								</td>
								<td>${subOrder.mainOrder.address}</td>
								<%-- <td><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss"
										value="${subOrder.deliveryDt}" /></td> --%>
							</tr>
						</tbody>
					</table>
					<h3 class="headInfo">給消費者備註</h3>
					<div class="form-group">
						<div class="col-sm-12">
							<textarea class="form-control" rows="5" readonly="readonly">${subOrder.mainOrder.userRemark}</textarea>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<!-- /.box-footer -->
			</form:form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
		<c:if test="${not empty subOrder.subOrderDetailList}">
			<div class="box" id="resultList">
				<div class="box-header">
					<c:if test="${showDeliverBtn}">
						<div class="navbar-btn pull-right">
							<button class="btn btn-info btn-sm" data-toggle="modal"
								data-target="#myModal">出貨</button>
						</div>
					</c:if>
					<c:if test="${not showDeliverBtn}">
						<div class="navbar-btn pull-right">
							<button class="btn btn-info btn-sm" data-toggle="modal"
								disabled="disabled" data-target="#myModal">出貨</button>
						</div>
					</c:if>
					<h3 class="subHeadInfo">訂單明細</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="dataTables_wrapper form-inline dt-bootstrap">
						<table class="table table-bordered  table-striped text-center">
							<thead>
								<tr class="th-background">
									<td>料號</td>
									<td>商品名稱</td>
									<td>數量</td>
									<td>單價</td>
									<td>小計</td>
									<td>狀態</td>
									<!-- <td>客戶備註</td>
									<td>廠商備註</td> -->
									<td>出貨時間</td>
									<td>貨運公司</td>
									<td>物流編號</td>
									<td>退貨原因</td>
									<td>操作</td>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="detailVo" items="${subOrder.subOrderDetailList}">
									<tr>
										<td style="text-align: center"><c:if
												test="${not empty detailVo.product.course }">
											${detailVo.product.course.materialNum }
										</c:if> <c:if test="${not empty detailVo.product.props }">
											${detailVo.product.props.materialNum }
										</c:if></td>
										<td style="text-align: left">${detailVo.productName}</td>
										<td style="text-align: center"><fmt:formatNumber
												value="${detailVo.quantity}" type="number" /></td>
										<td style="text-align: right"><fmt:formatNumber
												value="${detailVo.unitPrice}" type="number" /></td>
										<td style="text-align: right"><fmt:formatNumber
												value="${detailVo.totalAmount}" type="number" /></td>
										<td><e7:code type="5" code="${detailVo.status}" /></td>
										<td style="display: none;"><textarea class="form-control"
												rows="3" id="${detailVo.pkSubOrderDetail}-userRemark"
												readonly="readonly">
												${detailVo.userRemark}
												</textarea></td>
										<td style="display: none;"><textarea class="form-control"
												rows="3" id="${detailVo.pkSubOrderDetail}-vendorRemark">
												${detailVo.vendorRemark}
												</textarea></td>
										<td><c:if test="${not empty  detailVo.deliveryDt}">
												<fmt:formatDate pattern="yyyy-MM-dd"
													value="${detailVo.deliveryDt}" />
												<br>
												<fmt:formatDate pattern="HH:mm:ss"
													value="${detailVo.deliveryDt}" />
											</c:if></td>
										<td><c:if test="${not empty  detailVo.deliveryDt}">${subOrder.logistics}</c:if>
										</td>
										<td><c:if test="${not empty  detailVo.deliveryDt}">${subOrder.logisticsNum}</c:if>
										</td>
										<td>${detailVo.returnReason }</td>
										<td style="text-align: center">
											<button class="btn btn-success btn-sm" data-toggle="modal"
												data-target="#remarkModal"
												onclick="goRemark('${detailVo.pkSubOrderDetail}','${detailVo.pkSubOrderDetail}-userRemark','${detailVo.pkSubOrderDetail }-vendorRemark')">備註</button>
											<c:if
												test="${detailVo.product.props !=null && detailVo.status == 'H' }">
												<button type="button" class="btn btn-warning btn-sm"
													onclick="handleReturn('${detailVo.pkSubOrderDetail}','G')">確認退貨</button>
												<button type="button" class="btn btn-danger btn-sm"
													onclick="handleReturn('${detailVo.pkSubOrderDetail}','J')">拒絕退件</button>
											</c:if>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</c:if>
	</div>
</div>
<script>
	//Get the modal
	var modal = document.getElementById('myModal');

	var remarkModal = document.getElementById('remarkModal');

	window.onclick = function(event) {
		if (event.target == modal || event.target == remarkModal) {
			$("#detailVendorRemark").val("");
			$("#logistics").val("");
			$("#logisticsNum").val("");
		}
	}
</script>
