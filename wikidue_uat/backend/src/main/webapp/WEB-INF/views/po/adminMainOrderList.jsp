<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
</style>
<script type="text/javascript">
	$(function() {
		$('.datepicker').datepicker({
			format : 'yyyy/mm/dd',
			autoclose : true,
			todayBtn : true,
			todayHighlight : true
		});

		cancelPop();
	});

	function query() {
		$("#method").val("query");
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#mainOrderForm").submit();
        }, 500);
	}

	function deliver() {
		var v1 = $("#logistics").val().trim();
		var v2 = $("#logisticsNum").val().trim();
		if (v1 == "") {
			alert("請輸入物流公司");
		} else if (v2 == "") {
			alert("請輸入物流編號");
		} else {
			document.mainOrderForm.action = '${pageContext.request.contextPath}/order/deliever';
			$("#method").val("modify");
			$.LoadingOverlay("show");
	        setTimeout(function(){
	        	$("#mainOrderForm").submit();
	        }, 500);
		}
	}

	function queryDeatil(pkMainOrder) {
		$.fancybox({
			'autoSize' : false,
			'fitToView' : false,
			'width' : "100%",
			'scrolling' : 'yes',
			'transitionIn' : 'none',
			'transitionOut' : 'none',
			'type' : 'iframe',
			'padding' : '0',
			'closeBtn' : true,
			'href' : '${pageContext.request.contextPath}/order/queryDetail/'
					+ pkMainOrder + '/' + new Date().getTime()
		});
	}

	function cancelPop() {
		$("#logistics").val("");
		$("#logisticsNum").val("");
	}

	function confirmpkSubOrder(pkMainOrder) {
		$("#pkMainOrder").val(pkMainOrder);
	}
	
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="mainOrderForm" name="mainOrderForm"
				action="${pageContext.request.contextPath}/order/query#result"
				class="form-horizontal">
				<sessionConversation:insertSessionConversationId
					attributeName="mainOrderForm" />
				<form:hidden path="method" />

				<!-- The Modal -->
				<div class="modal fade" id="myModal" role="dialog">
					<div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"
									onclick="cancelPop()">&times;</button>
								<h4 class="modal-title">出貨資訊</h4>
							</div>
							<div class="modal-body">
								<div class="form-group">
									<label for="logistics" class="col-sm-2 control-label">貨運公司</label>
									<div class="col-sm-10">
										<form:input path="logistics" class="form-control"
											placeholder="貨運公司" maxlength="50" />
										<form:errors path="logistics" class="text-red" />
									</div>
								</div>
								<div class="form-group">
									<label for="logisticsNum" class="col-sm-2 control-label">物流編號</label>
									<div class="col-sm-10">
										<form:input path="logisticsNum" class="form-control"
											placeholder="物流編號" maxlength="50" />
										<form:errors path="logisticsNum" class="text-red" />
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" onclick="deliver()"
									class="btn btn-success pull-right">確認</button>
								<button type="button" class="btn btn-default"
									onclick="cancelPop()" data-dismiss="modal">取消</button>
							</div>
						</div>

					</div>
				</div>
				<!-- The Modal -->

				<div class="box-body">
					<div class="form-group">
						<label for="pkMainOrder" class="col-sm-2 control-label">主訂單編號</label>
						<div class="col-sm-10">
							<form:input path="pkMainOrder" class="form-control"
								placeholder="主訂單編號" maxlength="50" />
							<form:errors path="pkMainOrder" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="orderDt" class="col-sm-2 control-label">訂單日期(起)</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon" onclick="$('#orderDt').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="orderDt"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="orderDt" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="orderDtEnd" class="col-sm-2 control-label">訂單日期(迄)</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon" onclick="$('#orderDtEnd').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="orderDtEnd"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="orderDtEnd" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="orderStatus" class="col-sm-2 control-label">訂單狀態</label>
						<div class="col-sm-10">
							<form:select path="orderStatus" cssClass="form-control">
								<form:option value="">請選擇</form:option>
								<form:option value="E">訂單成立</form:option>
								<form:option value="C">已取消</form:option>
								<form:option value="S">已逾期</form:option>
							</form:select>
							<form:errors path="orderStatus" class="text-red" />
						</div>
					</div>
					<button type="button" onclick="query()"
						class="btn btn-default pull-right">查詢</button>
				</div>
				<!-- /.box-body -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->
		<c:if test="${not empty mainOrderForm.result}">
			<div class="box" id="resultList">
				<div class="box-header">
					<a href="#" name="result"></a>
					<h3 class="box-title">查詢結果</h3>
				</div>
				<div class="form-group">
					<label for="totalAmount" class="col-sm-12 control-label">訂單總金額：$
					<fmt:formatNumber value="${mainOrderForm.totalAmount}" type="number" />
					</label>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="dataTables_wrapper form-inline dt-bootstrap">
						<display:table name="mainOrderForm.result.content"
							pagesize="${mainOrderForm.result.size}"
							requestURI='?method=query&mainOrderForm_cId=${curr_mainOrderForm_cId}#result'
							id="list" cellspacing="0" cellpadding="0"
							class="table table-bordered table-hover" partialList="true"
							size="${mainOrderForm.result.totalElements.intValue()}"
							excludedParams="*">
							<display:column title='購買人' property="purchaser" sortable="true" />
							<display:column title='主訂單編號' property="pkMainOrder"
								sortable="true">
							</display:column>
							<display:column title='訂購成立時間' sortable="true">
								<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss"
									value="${list.orderDt}" />
							</display:column>
							<display:column title='訂單狀態' sortable="true">
								<e7:code type="5" code="${list.orderStatus}" />
							</display:column>
							<display:column title='付款方式' 
								sortable="true" ><e7:getPaymentType type="${list.payment.paymentType}"/></display:column>
							<display:column title='訂購金額' sortable="true">
								<p style="text-align: right">
									<fmt:formatNumber value="${list.orderAmount}" type="number" />
								</p>
							</display:column>
							<sec:authorize url="/po/modify">
								<display:column title='操作'>
									<button type="button"
										onclick="queryDeatil('${list.pkMainOrder}');return false;"
										class="btn btn-info btn-sm">檢視</button>
									<c:if test="${not user_auth.admin && list.orderStatus == 'P' }">
										<button class="btn btn-success btn-sm" data-toggle="modal"
											onclick="confirmpkSubOrder('${list.pkMainOrder}')"
											data-target="#myModal">出貨</button>
									</c:if>
								</display:column>
							</sec:authorize>
						</display:table>
					</div>
				</div>
			</div>
		</c:if>
	</div>
</div>
<script>
	//Get the modal
	var modal = document.getElementById('myModal');

	window.onclick = function(event) {
		if (event.target == modal) {
			$("#logistics").val("");
			$("#logisticsNum").val("");
		}
	}
</script>
