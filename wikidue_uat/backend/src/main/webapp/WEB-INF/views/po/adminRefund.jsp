<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
.headInfo {
	border-left: 3px solid #3c8dbc;
	font-size: 20px;
}

.subHeadInfo {
	border-left: 3px solid #FF8800;
	font-size: 16px;
}

.th-background {
	background-color: #DCDCDC;
}
</style>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/plugins/jquery-loading-overlay-2.1.3/loadingoverlay.min.js"></script>
<script type="text/javascript">
	$(function() {
		$("#goBackUrl")
				.attr(
						"href",
						"${pageContext.request.contextPath}/order/queryDetail/${mainOrderForm.pkMainOrder}/"
								+ new Date().getTime());

	});

	$(document).ready(function() {
		$("#checkAll").click(function() {
			if ($("#checkAll").prop("checked")) {//如果全選按鈕有被選擇的話（被選擇是true）
				$("input[name='returnProductList']").each(function() {
					$(this).prop("checked", true);//把所有的核取方框的property都變成勾選
				})
			} else {
				$("input[name='returnProductList']").each(function() {
					$(this).prop("checked", false);//把所有的核方框的property都取消勾選
				})
			}
		})
	})

	function returnProduct() {
		var v1 = $('input[name="returnProductList"]:checked').length
		var v2 = $("#returnReason").val().trim();
		if (v1 == 0) {
			alert("請勾選退貨商品");
		} else if (v2 == "") {
			alert("請輸入退貨原因");
		} else {
			$.LoadingOverlay("show");
			setTimeout(function() {
				$("#mainOrderForm").submit();
			}, 500);
		}
	}

	function refund() {
		if (confirm('確認退款？')) {
			$.LoadingOverlay("show");
			setTimeout(function() {
				$("#method").val("refundProduct");
				$("#mainOrderForm").submit();
			}, 500);
		}
	}
	function goBack() {
		$.LoadingOverlay("show");
	}
</script>
<div class="row">
	<div class="col-md-12">
		<c:if test="${not empty detailList}">
			<form:form modelAttribute="mainOrderForm" name="mainOrderForm"
				action="${pageContext.request.contextPath}/order/return#result"
				class="form-horizontal">
				<div class="box box-primary">
					<sessionConversation:insertSessionConversationId
						attributeName="mainOrderForm" />
					<form:hidden path="method" value="returnProduct" />
					<form:hidden path="pkMainOrder" />
					<!-- The Modal -->
					<!-- /.box-body -->
					<!-- /.box-footer -->
					<!-- /.box-body -->
					<!-- /.box -->
				</div>
				<h3 class="subHeadInfo">商品清單</h3>
				<div class="box-body">
					<div class="dataTables_wrapper form-inline dt-bootstrap">
						<table class="table table-bordered  table-striped text-center">
							<thead>
								<tr class="th-background">
									<td>全選 <c:if test="${showReturnBtn }">
											<input type="checkbox" id="checkAll" name="checkAll" />
										</c:if>
										<c:if test="${showRefundBtn }">
											<input type="checkbox" id="checkAll" name="checkAll" disabled="disabled"/>
										</c:if>
									</td>
									<td>商品名稱</td>
									<td>數量</td>
									<td>單價</td>
									<td>小計</td>
									<td>狀態</td>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="obj" items="${detailList}" varStatus="loop">
									<tr>
										<td style="text-align: center"><c:if
												test="${(obj.status == 'P' || obj.status == 'D') && showReturnBtn}">
												<input name="returnProductList"
													value="${obj.pkSubOrderDetail}" type="checkbox" />
											</c:if> <c:if test="${(obj.status == 'G') && showRefundBtn}">
												<input name="returnProductList"
													value="${obj.pkSubOrderDetail}" type="checkbox" checked="checked"
													disabled="disabled" />
											</c:if></td>
										<td style="text-align: left">${obj.productName}</td>
										<td style="text-align: center"><fmt:formatNumber
												value="${obj.quantity}" type="number" /></td>
										<td style="text-align: right"><fmt:formatNumber
												value="${obj.unitPrice}" type="number" /></td>
										<td style="text-align: right"><fmt:formatNumber
												value="${obj.totalAmount}" type="number" /></td>
										<td><e7:code type="5" code="${obj.status}" /></td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<!-- 			<div class="form-group">
				<label for="returnAmt" class="col-sm-2 control-label">*退貨金額</label>
				<div class="col-sm-10">
					<input type="number" name="returnAmt" class="form-control" value="0"
						max="10000" min="0" />
				</div>
			</div> -->
					<c:if test="${showReturnBtn}">
						<div class="form-group">
							<label for="returnReason" class="col-sm-2 control-label">*退貨原因</label>
							<div class="col-sm-10">
								<input id="returnReason" type="text" name="returnReason"
									class="form-control" maxLength="60" />
							</div>
						</div>
					</c:if>
				</div>
				<div class="box-footer">
					<c:if test="${showReturnBtn }">
						<button type="button" onclick="returnProduct()"
							class="btn btn-info">退貨</button>
						<button type="button" class="btn btn-warning" disabled="disabled">退款</button>
					</c:if>
					<c:if test="${showRefundBtn }">
						<button type="button" class="btn btn-info" disabled="disabled">退貨</button>
						<button type="button" onclick="refund()" class="btn btn-warning">退款</button>
					</c:if>
					<c:if test="${not showReturnBtn && not showRefundBtn}">
						<button type="button" class="btn btn-info" disabled="disabled">退貨</button>
						<button type="button" class="btn btn-warning" disabled="disabled">退款</button>
					</c:if>

					<a id="goBackUrl" href="#" class="btn btn-default pull-right"
						onclick="goBack();">取消</a>
				</div>
				<hr
					style="width: 100%; color: #95d600; height: 3px; background-color: #95d600;" />

				<!-- /.box-header -->
			</form:form>
		</c:if>
	</div>
</div>
<script>
	
</script>
