<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
.headInfo {
	border-left: 3px solid #3c8dbc;
	font-size: 20px;
}

.subHeadInfo {
	border-left: 3px solid #FF8800;
	font-size: 16px;
}

.th-background {
	background-color: #DCDCDC;
}
</style>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/plugins/jquery-loading-overlay-2.1.3/loadingoverlay.min.js"></script>
<script type="text/javascript">
	function cancelPop() {
		$("#returnReason").val("");
		$("#detailUserRemark").val("");
		$("#detailAdminRemark").val("");
	}

	function goReturn() {
		document.mainOrderForm.action = '${pageContext.request.contextPath}/order/return';
		$("#method").val("goReturn");
		$.LoadingOverlay("show");
		setTimeout(function() {
			$("#mainOrderForm").submit();
		}, 500);
	}
	function refundProduct(pkSubOrderDetail) {
		document.mainOrderForm.action = '${pageContext.request.contextPath}/order/return';
		$("#pkSubOrderDetail").val(pkSubOrderDetail);
		$("#method").val("refundProduct");
		if (confirm('確認退款？')) {
			$.LoadingOverlay("show");
			setTimeout(function() {
				$("#mainOrderForm").submit();
			}, 500);
		}
	}
	function cancelPo() {
		var v1 = $("#returnReason").val().trim();
		if (v1 == "") {
			alert("請輸入取消訂單原因");
		} else {
			document.mainOrderForm.action = '${pageContext.request.contextPath}/order/return';
			$("#orderStatus").val("R");
			$("#method").val("cancelPo");
			$.LoadingOverlay("show");
			setTimeout(function() {
				$("#mainOrderForm").submit();
			}, 500);
		}
	}
	function saveMainRemark() {
		document.mainOrderForm.action = '${pageContext.request.contextPath}/order/remark';
		$("#method").val("saveMainRemark");
		$.LoadingOverlay("show");
		setTimeout(function() {
			$("#mainOrderForm").submit();
		}, 500);
	}
	function goRemark(pkSubOrderDetail, detailUserRemarkId,
			detailAdminRemarkId, detailVendorRemarkId) {
		$("#pkSubOrderDetail").val(pkSubOrderDetail);
		var v1 = $("#" + detailUserRemarkId).val().trim();
		var v2 = $("#" + detailAdminRemarkId).val().trim();
		var v3 = $("#" + detailVendorRemarkId).val().trim();
		$("#detailUserRemark").val(v1);
		$("#detailAdminRemark").val(v2);
		$("#detailVendorRemark").val(v3);
	}
	function saveDeatilRemark() {
		document.mainOrderForm.action = '${pageContext.request.contextPath}/order/remark';
		$("#method").val("saveDeatilRemark");
		$.LoadingOverlay("show");
		setTimeout(function() {
			$("#mainOrderForm").submit();
		}, 500);
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="mainOrderForm" name="mainOrderForm"
				action="${pageContext.request.contextPath}/order/return#result"
				class="form-horizontal">
				<sessionConversation:insertSessionConversationId
					attributeName="mainOrderForm" />
				<form:hidden path="method" />
				<form:hidden path="pkSubOrderDetail" />
				<form:hidden path="pkMainOrder" value="${mainOrder.pkMainOrder}" />
				<!-- The Modal -->
				<div class="modal fade" id="returnModal" role="dialog">
					<div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"
									onclick="cancelPop()">&times;</button>
								<h4 class="modal-title">取消訂單資訊</h4>
							</div>
							<div class="modal-body">
								<div class="form-group">
									<label for="returnReason" class="col-sm-4 control-label">取消訂單原因</label>
									<div class="col-sm-8">
										<form:input path="returnReason" class="form-control"
											placeholder="取消訂單原因" maxlength="50" />
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" onclick="cancelPo()"
									class="btn btn-success pull-right">確認</button>
								<button type="button" class="btn btn-default"
									onclick="cancelPop()" data-dismiss="modal">取消</button>
							</div>
						</div>

					</div>
				</div>
				<!-- The Modal -->

				<!-- The Modal -->
				<div class="modal fade" id="remarkModal" role="dialog">
					<div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"
									onclick="cancelPop()">&times;</button>
								<h4 class="modal-title">備註</h4>
							</div>
							<div class="modal-body">
								<div class="form-group">
									<label for="detailUserRemark" class="col-sm-4 control-label">給消費者(前台)</label>
									<div class="col-sm-8">
										<form:textarea path="detailUserRemark" class="form-control"
											rows="3" maxlength="50" placeholder="客戶備註" />
									</div>
								</div>
							</div>
							<div class="modal-body">
								<div class="form-group">
									<label for="detailAdminRemark" class="col-sm-4 control-label">管理者備註</label>
									<div class="col-sm-8">
										<form:textarea path="detailAdminRemark" class="form-control"
											rows="3" maxlength="50" placeholder="管理者備註" />
									</div>
								</div>
							</div>
							<div class="modal-body">
								<div class="form-group">
									<label for="detailVendorRemark" class="col-sm-4 control-label">廠商備註</label>
									<div class="col-sm-8">
										<textarea rows="3" id="detailVendorRemark"
											class="form-control" placeholder="廠商備註" readonly="readonly"></textarea>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" onclick="saveDeatilRemark()"
									class="btn btn-success pull-right">確認</button>
								<button type="button" class="btn btn-default"
									onclick="cancelPop()" data-dismiss="modal">取消</button>
							</div>
						</div>

					</div>
				</div>
				<!-- The Modal -->
				<div class="box-body">
					<div class="navbar-btn pull-right">
						<c:if test="${mainOrder.payment.status == 'N' }">
							<button class="btn btn-danger btn-sm" disabled="disabled">取消訂單</button>
							<button class="btn btn-warning btn-sm" disabled="disabled">退貨申請</button>
						</c:if>
						<c:if test="${returnBtnStatus == 'C' && mainOrder.payment.status != 'N'}">
							<a class="btn btn-danger btn-sm" data-toggle="modal"
								href="#returnModal">取消訂單</a>
							<button class="btn btn-warning btn-sm" onclick="goReturn()">退貨申請</button>
						</c:if>
						<c:if
							test="${returnBtnStatus != 'C' && mainOrder.payment.status != 'N'}">
							<button class="btn btn-danger btn-sm" disabled="disabled">取消訂單</button>
							<button class="btn btn-warning btn-sm" onclick="goReturn()">退貨申請</button>
						</c:if>
					</div>
					<h3 class="headInfo">主訂單資訊</h3>
					<table class="table table-bordered  table-striped text-center">
						<thead>
							<tr class="th-background">
								<td>主訂單編號</td>
								<td>訂購成立時間</td>
								<td>訂單金額</td>
								<td>訂單狀態</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>${mainOrder.pkMainOrder}</td>
								<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss"
										value="${mainOrder.createDt}" /></td>
								<td><fmt:formatNumber value="${mainOrder.orderAmount}"
										type="number" /></td>
								<td><e7:code type="5" code="${mainOrder.orderStatus}" /></td>
							</tr>
						</tbody>
					</table>
					<BR>
					<h3 class="headInfo">綠界付款資訊</h3>
					<table class="table table-bordered  table-striped text-center">
						<thead>
							<tr class="th-background">
								<td>綠界訂單號碼</td>
								<td>發票號碼</td>
								<td>付款方式</td>
								<td>繳款金額</td>
								<td>付款狀態</td>
							</tr>
						</thead>
						<tbody>
							<c:if test="${mainOrder.payment.status == 'P'}">
								<tr>
									<td>${mainOrder.payment.tradeNo}</td>
									<td>${mainOrder.payment.invoiceNo}</td>
									<td><e7:getPaymentType type="${mainOrder.payment.paymentType}"/></td>
									<td><fmt:formatNumber
											value="${mainOrder.payment.tradeAmt}" type="number" /></td>
									<td><e7:code type="5" code="${mainOrder.payment.status}" /></td>
								</tr>
							</c:if>
						</tbody>
					</table>
					<BR>
					<h3 class="headInfo">購買人資訊</h3>
					<table class="table table-bordered  table-striped text-center">
						<thead>
							<tr class="th-background">
								<td>購買人</td>
								<td>收件人</td>
								<td>連絡電話</td>
								<td>送貨地址</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>${mainOrder.purchaser}<BR>${mainOrder.purchaserId}</td>
								<td>${mainOrder.recipient}</td>
								<td>${mainOrder.mobile}<%-- <br>${mainOrder.telphone} --%>
								</td>
								<td>${mainOrder.address}</td>
							</tr>

						</tbody>
					</table>
					<BR>
					<div class="form-group pull-right">
						<button type="button" onclick="saveMainRemark()"
							class="btn btn-info pull-right">儲存備註</button>
					</div>
					<h3 class="headInfo">主訂單備註</h3>
					<div class="form-group">
						<label for="adminRemark" class="col-sm-3 control-label">管理者備註</label>
						<div class="col-sm-9">
							<form:textarea path="adminRemark" class="form-control" rows="5"
								maxlength="100" placeholder="管理理備註" />
							<form:errors path="adminRemark" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="userRemark" class="col-sm-3 control-label">給消費者(前台)</label>
						<div class="col-sm-9">
							<form:textarea path="userRemark" class="form-control" rows="5"
								maxlength="100" placeholder="使用者備註" />
							<form:errors path="userRemark" class="text-red" />
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<!-- /.box-footer -->
			</form:form>
			<!-- /.box-body -->
		</div>
		<!-- /.box -->
		<c:if test="${not empty mainOrder.subOrderList}">
			<div class="box" id="resultList">
				<c:forEach items="${mainOrder.subOrderList}" var="subOrder">
					<h3 class="subHeadInfo">訂單資訊 - <label>${subOrder.vendor.vendorName}</label></h3>
					<div class="form-group">
						<label class="col-sm-6 control-label">
						廠商訂單編號：${subOrder.pkSubOrder}
						</label>
						<label class="col-sm-6 control-label">
						廠商編號：${subOrder.vendor.vendorCode}
						</label>
					</div>
					<div class="form-group">
						<label class="col-sm-6 control-label">
						廠商統編：${subOrder.vendor.taxId}
						</label>
						<label class="col-sm-6 control-label">
						訂單金額：<fmt:formatNumber
										value="${subOrder.orderAmount}" type="number" />
						</label>
					</div>
					<table class="table table-bordered  table-striped text-center">
						<thead>
							<tr class="th-background">
								<td>料號</td>
								<td>商品名稱</td>
								<td>數量</td>
								<td>單價</td>
								<td>小計</td>
								<td>狀態</td>
								<!-- <td>客戶備註</td>
										<td>廠商備註</td>
										<td>管理者備註</td> -->
								<td>觀看比例</td>
								<td>出貨時間</td>
								<td>貨運公司</td>
								<td>物流編號</td>
								<td>退貨原因</td>
								<td>操作</td>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="detailVo" items="${subOrder.subOrderDetailList}">
								<tr>
									<td style="text-align: center">
										<c:if test="${not empty detailVo.product.course }">
											${detailVo.product.course.materialNum }
										</c:if>
										<c:if test="${not empty detailVo.product.props }">
											${detailVo.product.props.materialNum }
										</c:if>
									</td>
									<td style="text-align: left">${detailVo.productName}</td>
									<td style="text-align: center"><fmt:formatNumber
											value="${detailVo.quantity}" type="number" /></td>
									<td style="text-align: right"><fmt:formatNumber
											value="${detailVo.unitPrice}" type="number" /></td>
									<td style="text-align: right"><fmt:formatNumber
											value="${detailVo.totalAmount}" type="number" /></td>
									<td><e7:code type="5" code="${detailVo.status}" /></td>
									<td style="display: none;"><textarea class="form-control"
											rows="3" id="${detailVo.pkSubOrderDetail}-userRemark"
											readonly="readonly">
												${detailVo.userRemark}
												</textarea></td>
									<td style="display: none;"><textarea class="form-control"
											rows="3" readonly="readonly"
											id="${detailVo.pkSubOrderDetail}-vendorRemark">
												${detailVo.vendorRemark}
												</textarea></td>
									<td style="display: none;"><textarea class="form-control"
											rows="3" id="${detailVo.pkSubOrderDetail}-adminRemark"
											readonly="readonly">
												${detailVo.adminRemark}
												</textarea></td>
									<td>
										<c:if test="${not empty detailVo.product.course}">
											<e7:getCourseProgress uid="${detailVo.createId}" courseId="${detailVo.product.course.pkCourse}"/>
										</c:if>
										<c:if test="${empty detailVo.product.course}">
											-
										</c:if>
									</td>
									<td><c:if test="${not empty  detailVo.deliveryDt}">
											<fmt:formatDate pattern="yyyy-MM-dd"
												value="${detailVo.deliveryDt}" />
											<br>
											<fmt:formatDate pattern="HH:mm:ss"
												value="${detailVo.deliveryDt}" />
										</c:if></td>
									<td><c:if test="${not empty  detailVo.deliveryDt}">${subOrder.logistics}</c:if>
									</td>
									<td><c:if test="${not empty  detailVo.deliveryDt}">${subOrder.logisticsNum}</c:if>
									</td>
									<td>${detailVo.returnReason }</td>
									<td style="text-align: center">
										<button class="btn btn-success btn-sm" data-toggle="modal"
											data-target="#remarkModal"
											onclick="goRemark('${detailVo.pkSubOrderDetail}','${detailVo.pkSubOrderDetail}-userRemark','${detailVo.pkSubOrderDetail}-adminRemark','${detailVo.pkSubOrderDetail }-vendorRemark')">備註</button>
										<%-- <c:if
													test="${detailVo.status == 'G' }">
													<button type="button" class="btn btn-warning btn-sm"
														onclick="refundProduct('${detailVo.pkSubOrderDetail}')">退款</button>
												</c:if> --%>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<%-- <h3 class="subHeadInfo">訂單明細 </h3>
					<div class="box-body">
						<div class="dataTables_wrapper form-inline dt-bootstrap">
							<table class="table table-bordered  table-striped text-center">
								<thead>
									<tr class="th-background">
										<td>商品名稱</td>
										<td>數量</td>
										<td>單價</td>
										<td>小計</td>
										<td>狀態</td>
										<!-- <td>客戶備註</td>
										<td>廠商備註</td>
										<td>管理者備註</td> -->
										<td>操作</td>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="detailVo"
										items="${subOrder.subOrderDetailList}">
										<tr>
											<td style="text-align: left">${detailVo.productName}</td>
											<td style="text-align: center"><fmt:formatNumber
													value="${detailVo.quantity}" type="number" /></td>
											<td style="text-align: right"><fmt:formatNumber
													value="${detailVo.unitPrice}" type="number" /></td>
											<td style="text-align: right"><fmt:formatNumber
													value="${detailVo.totalAmount}" type="number" /></td>
											<td><e7:code type="5" code="${detailVo.status}" /></td>
											<td style="display: none;"><textarea
													class="form-control" rows="3"
													id="${detailVo.pkSubOrderDetail}-userRemark"
													readonly="readonly">
												${detailVo.userRemark}
												</textarea></td>
											<td style="display: none;"><textarea
													class="form-control" rows="3" readonly="readonly"
													id="${detailVo.pkSubOrderDetail}-vendorRemark">
												${detailVo.vendorRemark}
												</textarea></td>
											<td style="display: none;"><textarea
													class="form-control" rows="3"
													id="${detailVo.pkSubOrderDetail}-adminRemark"
													readonly="readonly">
												${detailVo.adminRemark}
												</textarea></td>
											<td style="text-align: center">
												<button class="btn btn-success btn-sm" data-toggle="modal"
													data-target="#remarkModal"
													onclick="goRemark('${detailVo.pkSubOrderDetail}','${detailVo.pkSubOrderDetail}-userRemark','${detailVo.pkSubOrderDetail}-adminRemark','${detailVo.pkSubOrderDetail }-vendorRemark')">備註</button>
												<c:if
													test="${detailVo.status == 'G' }">
													<button type="button" class="btn btn-warning btn-sm"
														onclick="refundProduct('${detailVo.pkSubOrderDetail}')">退款</button>
												</c:if>
											</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div> --%>
					<%-- <c:if test="${subOrder.hasReturnRecord}">
						<h3 class="subHeadInfo">退貨(刷)記錄 -
							${subOrder.vendor.vendorName}</h3>
						<div class="box-body">
							<div class="dataTables_wrapper form-inline dt-bootstrap">
								<table class="table table-bordered  table-striped text-center">
									<thead>
										<tr class="th-background">
											<td>商品名稱</td>
											<td>退貨數量</td>
											<td>退貨金額</td>
											<td>退貨原因</td>
											<td>退貨時間</td>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="detailVo"
											items="${subOrder.subOrderDetailList}">
											<c:if
												test="${detailVo.status == 'R' || detailVo.status == 'H' }">
												<tr>
													<td style="text-align: left">${detailVo.productName}</td>
													<td style="text-align: center"><fmt:formatNumber
															value="${detailVo.quantity}" type="number" /></td>
													<td style="text-align: right"><fmt:formatNumber
															value="${detailVo.totalAmount}" type="number" /></td>
													<td style="text-align: left">${detailVo.returnReason}</td>
													<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss"
															value="${detailVo.updateDt}" /></td>
												</tr>
											</c:if>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</c:if> --%>
					<hr
						style="width: 100%; color: #95d600; height: 3px; background-color: #95d600;" />
				</c:forEach>
			</div>
			<!-- /.box-header -->
		</c:if>
	</div>
</div>
<script>
	//Get the modal
	var returnModal = document.getElementById('returnModal');
	var remarkModal = document.getElementById('remarkModal');

	window.onclick = function(event) {
		if (event.target == returnModal || event.target == remarkModal) {
			$("#returnReason").val("");
			$("#detailUserRemark").val("");
			$("#detailAdminRemark").val("");
		}
	}
</script>
