<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
</style>
<script type="text/javascript">
	$(function() {
		$('.datepicker').datepicker({
			format : 'yyyy/mm/dd',
			autoclose : true,
			todayBtn : true,
			todayHighlight : true
		});

	});

	function query() {
		$("#method").val("query");
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#mainOrderForm").submit();
        }, 500);
	}

	function queryDeatil(pkMainOrder) {
		$.fancybox({
			'autoSize' : false,
			'fitToView' : false,
			'width' : "100%",
			'scrolling' : 'auto',
			'transitionIn' : 'none',
			'transitionOut' : 'none',
			'type' : 'iframe',
			'padding' : '0',
			'closeBtn' : true,
			'href' : '${pageContext.request.contextPath}/order/queryDetail/'
					+ pkMainOrder + '/' + new Date().getTime()
		});
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="mainOrderForm"
				name="mainOrderForm"
				action="${pageContext.request.contextPath}/order/query#result"
				class="form-horizontal">
				<sessionConversation:insertSessionConversationId
					attributeName="mainOrderForm" />
				<form:hidden path="method" />
				<form:hidden path="pkSubOrder" />
				<div class="box-body">
					<div class="form-group">
						<label for="pkMainOrder" class="col-sm-2 control-label">主訂單編號</label>
						<div class="col-sm-10">
							<form:input path="pkMainOrder" class="form-control"
								placeholder="主訂單編號" maxlength="50" />
							<form:errors path="pkMainOrder" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="orderDt" class="col-sm-2 control-label">訂單日期(起)</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon" onclick="$('#orderDt').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="orderDt"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="orderDt" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="orderDtEnd" class="col-sm-2 control-label">訂單日期(迄)</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon" onclick="$('#orderDtEnd').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="orderDtEnd"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="orderDtEnd" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="orderStatus" class="col-sm-2 control-label">訂單狀態</label>
						<div class="col-sm-10">
							<form:select path="orderStatus" cssClass="form-control">
								<form:option value="">請選擇</form:option>
								<form:option value="E">訂單成立</form:option>
								<form:option value="C">已取消</form:option>
								<form:option value="S">已逾期</form:option>
							</form:select>
							<form:errors path="orderStatus" class="text-red" />
						</div>
					</div>
					<button type="button" onclick="query()"
						class="btn btn-default pull-right">查詢</button>
				</div>
				<!-- /.box-body -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->
		<c:if test="${not empty mainOrderForm.subOrderResult}">
			<div class="box" id="resultList">
				<div class="box-header">
					<a href="#" name="result"></a>
					<h3 class="box-title">查詢結果</h3>
				</div>
				<div class="form-group">
					<label for="totalAmount" class="col-sm-12 control-label">訂單總金額：$
					<fmt:formatNumber value="${mainOrderForm.totalAmount}" type="number" />
					</label>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="dataTables_wrapper form-inline dt-bootstrap">
						<display:table name="mainOrderForm.subOrderResult.content"
							pagesize="${mainOrderForm.subOrderResult.size}"
							requestURI='?method=query&mainOrderForm_cId=${curr_mainOrderForm_cId}#result'
							id="list" cellspacing="0" cellpadding="0"
							class="table table-bordered table-hover" partialList="true"
							size="${mainOrderForm.subOrderResult.totalElements.intValue()}"
							excludedParams="*">
							<display:column title='購買人' property="mainOrder.purchaser" sortable="true"/>
							<display:column title='主訂單編號' property="mainOrder.pkMainOrder"
								sortable="true">
							</display:column>
							<display:column title='訂購成立時間' sortable="true">
								<fmt:formatDate pattern="yyyy-MM-dd HH:mm:ss"
									value="${list.mainOrder.orderDt}" />
							</display:column>
							<display:column title='訂單狀態' sortable="true">
								<e7:code type="5" code="${list.mainOrder.orderStatus}" />
							</display:column>
							<display:column title='付款方式' sortable="true" ><e7:getPaymentType type="${list.mainOrder.payment.paymentType}"/></display:column>
							<display:column title='訂購金額' sortable="true">
								<p style="text-align: right">
									<fmt:formatNumber value="${list.orderAmount}" type="number" />
								</p>
							</display:column>
							<sec:authorize url="/po/modify">
								<display:column title='操作'>
									<button type="button"
										onclick="queryDeatil('${list.pkSubOrder}');return false;"
										class="btn btn-info btn-sm">檢視</button>
								</display:column>
							</sec:authorize>
						</display:table>
					</div>
				</div>
			</div>
		</c:if>
	</div>
</div>
