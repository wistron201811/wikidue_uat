<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
.headInfo {
	border-left: 3px solid #3c8dbc;
	font-size: 20px;
}

.subHeadInfo {
	border-left: 3px solid #FF8800;
	font-size: 16px;
}

.th-background {
	background-color: #DCDCDC;
}
</style>
<script type="text/javascript">
</script>
<div class="row">
	<div class="col-md-12">
		<c:if test="${not empty statusHstList}">
			<form:form modelAttribute="mainOrderForm" name="mainOrderForm"
				action="${pageContext.request.contextPath}/order/queryStatusHst#result"
				class="form-horizontal">
				<div class="box box-primary">
					<sessionConversation:insertSessionConversationId
						attributeName="mainOrderForm" />
					<!-- The Modal -->
					<!-- /.box-body -->
					<!-- /.box-footer -->
					<!-- /.box-body -->
					<!-- /.box -->
				</div>
				<h3 class="subHeadInfo">訂單狀態歷程 - ${mainOrderForm.pkMainOrder}</h3>
				<div class="box-body">
					<div class="dataTables_wrapper form-inline dt-bootstrap">
						<table class="table table-bordered  table-striped text-center">
							<thead>
								<tr class="th-background">
									<td>廠商訂單編號</td>
									<td>商品訂單編號</td>
									<td>商品名稱</td>
									<td>異動類別</td>
									<td>狀態日期</td>
									<td>狀態</td>
									<td>異動人員</td>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="obj" items="${statusHstList}" varStatus="loop">
									<tr>
										<td style="text-align: left">${obj.subOrder.pkSubOrder}</td>
										<td style="text-align: left">${obj.subOrderDetail.pkSubOrderDetail}</td>
										<td style="text-align: left">${obj.productName}</td>
										<td style="text-align: left">${obj.type}</td>
										<td style="text-align: left">${obj.updateDt}</td>
										<td><e7:code type="5" code="${obj.status}" /></td>
										<td style="text-align: left">${obj.lastModifiedId}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
				<div class="box-footer">
				</div>
				<hr
					style="width: 100%; color: #3c8dbc; height: 3px; background-color: #3c8dbc;" />

				<!-- /.box-header -->
			</form:form>
		</c:if>
	</div>
</div>
