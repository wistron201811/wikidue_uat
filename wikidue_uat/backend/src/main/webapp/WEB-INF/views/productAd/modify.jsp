<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 預覽上傳的圖片 -->
<script
	src="${pageContext.request.contextPath}/plugins/uploadPreview/jquery.uploadPreview.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
<style type="text/css">

</style>
<script type="text/javascript">
	$(function() {
		$('.datepicker2').datepicker({
			format : 'yyyy/mm/dd',
			autoclose : true
		});

		if("${productAdForm.productImageId}"==""){
			$("#productImage").hide();
		}
	});
	function save() {
		$("#method").val("modify");
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#productAdForm").submit();
        }, 500);
	}
	
	function chooseProduct(){
		$.fancybox({
			'autoSize' : false,
		    'fitToView': false,
		    'width': "100%",
			'scrolling' : 'auto',
			'transitionIn' : 'none',
			'transitionOut' : 'none',
			'type' : 'iframe',
			'padding' : '0',
			'closeBtn' : true,
			'href' : '${pageContext.request.contextPath}/productAd/queryProduct?method=query&d='+new Date().getTime()
        });
	}
	
	function showProduct(productId,productName,productImage){
		$("#productId").val(productId);
		$("#productName").val(productName);
		$("#productImageId").val(productImage);
		$("#productNameSpan").text(productName);
		$("#productImage").show();
		$("#productImage").attr("src",'${pageContext.request.contextPath}/common/query/image/'+productImage);
	}
	
	function goQuery() {
		$.LoadingOverlay("show");
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="productAdForm" name="productAdForm"
				action="${pageContext.request.contextPath}/productAd/modify"
				class="form-horizontal">
				<form:hidden path="method"/>
				<form:hidden path="productAdId"/>
				<form:hidden path="productId"/>
				<form:hidden path="productName"/>
				<form:hidden path="productImageId"/>
				<sessionConversation:insertSessionConversationId
					attributeName="productAdForm" />
				<div class="box-body">
					<div class="form-group">
						<label for="productId" class="col-sm-2 control-label">*商品</label>
						<div class="col-sm-10">
							<button type="button" onclick="chooseProduct();return false;" class="btn btn-info">選擇商品</button>
							<span style="margin-left:10px" id="productNameSpan">${productAdForm.productName}</span>
							<br>
							<br>
							<img id="productImage" style=" max-height: 350px;" src="${pageContext.request.contextPath}/common/query/image/${productAdForm.productImageId}"></img>
						
							<form:errors path="productId" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="description" class="col-sm-2 control-label">*商品描述</label>
						<div class="col-sm-10">
							<form:input path="description" class="form-control"
								placeholder="標題" maxlength="500" />
							<form:errors path="description" class="text-red" />
						</div>
					</div>
					<div class="form-group date">
						<label for="publishSDate" class="col-sm-2 control-label">*上架日期</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#publishSDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="publishSDate"
									cssClass="form-control pull-right  datepicker2"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="publishSDate" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="publishEDate" class="col-sm-2 control-label">*下架日期</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#publishEDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="publishEDate"
									cssClass="form-control pull-right  datepicker2"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="publishEDate" class="text-red" />
							</div>
						</div>
					</div>
<!-- 					<div class="form-group"> -->
<!-- 						<label for="type" class="col-sm-2 control-label">類型</label> -->
<!-- 						<div class="col-sm-10"> -->
<%-- 							<form:radiobutton path="type" id="radio3" value="1" --%>
<%-- 								checked="checked" /> --%>
<!-- 							<label for="radio1">大圖</label><br /> -->
<%-- 							<form:radiobutton path="type" id="radio4" value="2" /> --%>
<!-- 							<label for="radio2">小圖</label> -->
<!-- 						</div> -->
<!-- 					</div> -->
					<div class="form-group">
						<label for="active" class="col-sm-2 control-label">啟用</label>
						<div class="col-sm-10">
							<form:radiobutton path="active" id="radio1" value="1"
								checked="checked" />
							<label for="radio1">是</label><br />
							<form:radiobutton path="active" id="radio2" value="0" />
							<label for="radio2">否</label>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="button" onclick="save()" class="btn btn-info">儲存</button>
					<a href="${pageContext.request.contextPath}/productAd/query"
						class="btn btn-default pull-right" onclick="goQuery();">取消</a>
				</div>
				<!-- /.box-footer -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>