<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<script>
	$(function() {
	});
	function query(){
		$("#method").val("query");
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#productAdForm").submit();
        }, 500);
	}
	function goModify(productAdId) {
		$("#productAdId").val(productAdId);
		document.productAdForm.action = '${pageContext.request.contextPath}/productAd/modify';
		document.productAdForm.productAdForm_cId.value="";
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#productAdForm").submit();
        }, 500);
	}
	function remove(productAdId) {
		if (confirm('確認刪除？')) {
			$("#productAdId").val(productAdId);
			$("#method").val("remove");
			$.LoadingOverlay("show");
	        setTimeout(function(){
	        	$("#productAdForm").submit();
	        }, 500);
		}
	}
</script>
<form:form modelAttribute="productAdForm" name="productAdForm"
	action="${pageContext.request.contextPath}/productAd/query#result" method="post"
	autoComplete="Off">
	<input type="hidden" name="method" id="method" />
	<input type="hidden" name="productAdId" id="productAdId" />
	<sessionConversation:insertSessionConversationId attributeName="productAdForm"/>
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-body">
					<div class="form-group">
						<label for="productName">商品名稱</label>
						<form:input path="productName" cssClass="form-control"
							placeholder="商品名稱" />
					</div>
<!-- 					<div class="form-group"> -->
<!-- 						<label for="type">類型</label> -->
<%-- 						<form:select path="type" cssClass="form-control"> --%>
<%-- 							<form:option value="">請選擇</form:option> --%>
<%-- 							<form:option value="1">大圖</form:option> --%>
<%-- 							<form:option value="2">小圖</form:option> --%>
<%-- 						</form:select> --%>
<!-- 					</div> -->
					<div class="form-group">
						<button onclick="query()" class="btn btn-default pull-right">查詢</button>
					</div>
				</div>
				</div>
				<c:if test="${not empty productAdForm.result}">
				<div class="box" id="resultList">
					<div class="box-header">
						<a href="#" id="result"></a>
						<h3 class="box-title">查詢結果</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="dataTables_wrapper form-inline dt-bootstrap">
							<display:table name="productAdForm.result.content" pagesize="${productAdForm.result.size}"
								partialList="true" requestURI='?method=query&productAdForm_cId=${curr_productAdForm_cId}#result' id="productAd"
								cellspacing="0" cellpadding="0"
								class="table table-bordered table-hover" size="${productAdForm.result.totalElements.intValue()}"
								excludedParams="*">
								<display:column title='商品名稱' property="product.productName"
									sortable="true" />
								<display:column title='上架日期' sortable="true">
									<fmt:formatDate pattern="yyyy/MM/dd"
										value="${productAd.publishStartDate}" />
								</display:column>
								<display:column title='下架日期' sortable="true">
									<fmt:formatDate pattern="yyyy/MM/dd"
										value="${productAd.publishEndDate}" />
								</display:column>
								<display:column title='價格' property="product.price" sortable="true" />
<%-- 								<display:column title='類型' sortable="true" > --%>
<%-- 									<c:if test="${productAd.type == '1'}">大圖</c:if> --%>
<%-- 									<c:if test="${productAd.type == '2'}">小圖</c:if> --%>
<%-- 								</display:column> --%>
								<display:column title='狀態 ' sortable="true">
									<c:if test="${productAd.active=='1'}">
										<p class="text-success">已上架</p>
									</c:if>
									<c:if test="${productAd.active=='0'}">
										<p class="text-danger">未上架</p>
									</c:if>
								</display:column>							
								<display:column title='操作'>
									<!-- 有修改權限才顯示 -->
									<sec:authorize url="/productAd/modify">
									<button type="button"
										 onclick="goModify('${productAd.pkProductAd}');"
										class="btn btn-primary btn-sm">修改</button>
									<button type="button" onclick="remove('${productAd.pkProductAd}');" class="btn btn-danger btn-sm">刪除</button>
									</sec:authorize>
	
								</display:column>
							</display:table>
						</div>
					</div>
				</div>
				</c:if>
			</div>
		</div>
</form:form>