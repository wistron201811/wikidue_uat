<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/plugins/jquery-loading-overlay-2.1.3/loadingoverlay.min.js"></script>
<script>
	$(function() {

	});
	
	function add(pkProduct,productName,productImage){
		parent.showProduct(pkProduct,productName,productImage);
        parent.$.fancybox.close();
	}
</script>
<form:form modelAttribute="productAdForm" name="productAdForm"
	action="${pageContext.request.contextPath}/productAd/queryProduct#result" method="post"
	autoComplete="Off">
	<input type="hidden" name="method" id="method" />
	<sessionConversation:insertSessionConversationId attributeName="productAdForm"/>
	<div class="row">
		<div class="col-md-12">
			<h1>
		       	商品清單
		        <small></small>
		    </h1>
			<div class="box box-primary">
				<c:if test="${not empty queryResult}">
				<div class="box" id="resultList">
					<a href="#" id="result"></a>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="dataTables_wrapper form-inline dt-bootstrap">
							<display:table name="queryResult" pagesize="${queryResult.size}"
								partialList="true" requestURI='?method=query&productAdForm_cId=${curr_productAdForm_cId}#result' id="product"
								cellspacing="0" cellpadding="0"
								class="table table-bordered table-hover" size="${queryResult.totalElements.intValue()}"
								excludedParams="*">
								<display:column title='商品名稱' property="productName"
									sortable="true" />
								<display:column title='上架日期' sortable="true">
									<fmt:formatDate pattern="yyyy/MM/dd"
										value="${product.publishSDate}" />
								</display:column>
								<display:column title='下架日期' sortable="true">
									<fmt:formatDate pattern="yyyy/MM/dd"
										value="${product.publishEDate}" />
								</display:column>
								<display:column title='價格' property="price" sortable="true" />						
								<display:column title='促銷價格' property="discountPrice" sortable="true" />						
								<display:column title='操作'>
									<button type="button"
										 onclick="add('${product.pkProduct}','${fn:escapeXml(e7:htmlEscape(product.productName))}','${product.imageId}');"
										class="btn btn-primary btn-sm">選擇</button>
								</display:column>
							</display:table>
						</div>
					</div>
				</div>
				</c:if>
			</div>
		</div>
	</div>
</form:form>