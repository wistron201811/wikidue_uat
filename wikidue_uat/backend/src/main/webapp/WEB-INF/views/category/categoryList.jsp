<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<script type="text/javascript">
	function query() {
		$("#method").val("query");
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#categoryForm").submit();
        }, 500);
	}

	function remove(pkCategory) {
		if (confirm('確認刪除？')) {
			$("#pkCategory").val(pkCategory);
			document.categoryForm.action = '${pageContext.request.contextPath}/category/remove';
			$("#method").val("remove");
			$.LoadingOverlay("show");
	        setTimeout(function(){
	        	$("#categoryForm").submit();
	        }, 500);
		}
	}
	function goModify(pkCategory) {
		$("#pkCategory").val(pkCategory);
		document.categoryForm.action = '${pageContext.request.contextPath}/category/modify';
		document.categoryForm.categoryForm_cId.value="";
		$("#method").val("");
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#categoryForm").submit();
        }, 500);
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="categoryForm" name="categoryForm"
				action="${pageContext.request.contextPath}/category/query#result"
				class="form-horizontal">
				<sessionConversation:insertSessionConversationId
					attributeName="categoryForm" />
				<form:hidden path="method" />
				<form:hidden path="pkCategory" />
				<div class="box-body">
					<div class="form-group">
						<label for="categoryName" class="col-sm-2 control-label">分類名稱</label>
						<div class="col-sm-10">
							<form:input path="categoryName" class="form-control"
								placeholder="分類名稱" maxlength="50" />
							<form:errors path="categoryName" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="categoryCode" class="col-sm-2 control-label">分類編號</label>
						<div class="col-sm-10">
							<form:input path="categoryCode" class="form-control"
								maxlength="10" placeholder="分類編號" />
							<form:errors path="categoryCode" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="categoryType" class="col-sm-2 control-label">分類類型</label>
						<div class="col-sm-10">
							<form:select path="categoryType" cssClass="form-control">
								<form:option value="">請選擇</form:option>
								<%-- 
								<form:option value="COURSE">課程</form:option>
								<form:option value="PRODUCT">商品</form:option> 
								<form:option value="CAMP">營隊</form:option>  
								--%>
								<form:options items="${categoryForm.categoryTypeList}"
									itemLabel="displayName" />
							</form:select>
						</div>
					</div>
					<button type="button" onclick="query()" class="btn btn-default pull-right">查詢</button>
				</div>
				<!-- /.box-body -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->

		<c:if test="${not empty categoryForm.result}">
			<div class="box" id="resultList">
				<div class="box-header">
					<a href="#" name="result"></a>
					<h3 class="box-title">查詢結果</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="dataTables_wrapper form-inline dt-bootstrap">
						<display:table name="categoryForm.result.content"
							pagesize="${categoryForm.result.size}"
							requestURI='?method=query&categoryForm_cId=${curr_categoryForm_cId}#result'
							id="list" cellspacing="0" cellpadding="0"
							class="table table-bordered table-hover" partialList="true"
							size="${categoryForm.result.totalElements.intValue()}"
							excludedParams="*">
							<display:column title='分類名稱' property="categoryName"
								sortable="true">
							</display:column>
							<display:column title='分類編號' property="categoryCode"
								sortable="true">
							</display:column>
							<display:column title='分類類型' sortable="true">
								<e7:code type="2" code="${list.categoryType}" />
							</display:column>
							<display:column title='啟用 ' sortable="true">
								<e7:code type="1" code="${list.active}" />
							</display:column>
							<display:column title='操作'>
								<!-- 有修改權限才顯示 -->
								<sec:authorize url="/category/modify"> 
								<button type="button" onclick="goModify('${list.pkCategory}');"
									class="btn btn-success btn-sm">修改</button>
								<button type="button" onclick="remove('${list.pkCategory}');"
									class="btn btn-danger btn-sm">刪除</button>
								</sec:authorize>
							</display:column>
						</display:table>
					</div>
				</div>
			</div>
		</c:if>
	</div>
</div>
