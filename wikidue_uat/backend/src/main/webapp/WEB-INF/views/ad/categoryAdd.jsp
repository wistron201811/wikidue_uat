<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation" uri="/WEB-INF/tld/sessionConversation.tld"%>
<script type="text/javascript">
	function addCategory() {
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#adCategoryForm").submit();
        }, 500);
	}
	function goQuery() {
		$.LoadingOverlay("show");
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="adCategoryForm" name="adCategoryForm"
				action="${pageContext.request.contextPath}/adCategory/add"
				method="post" enctype="multipart/form-data" class="form-horizontal">
				<form:hidden path="method" value="save" />
				<sessionConversation:insertSessionConversationId
					attributeName="adCategoryForm" />
				<div class="box-body">
					<div class="form-group">
						<label for="categoryName" class="col-sm-2 control-label">*分類名稱</label>
						<div class="col-sm-10">
							<form:input path="categoryName" class="form-control"
								placeholder="分類名稱" maxlength="50" />
							<form:errors path="categoryName" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="categoryCode" class="col-sm-2 control-label">*分類編號</label>
						<div class="col-sm-10">
							<form:input path="categoryCode" class="form-control"
								maxlength="10" placeholder="分類編號" />
							<form:errors path="categoryCode" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="categoryType" class="col-sm-2 control-label">*platform</label>
						<div class="col-sm-10">
							<form:select path="categoryType" cssClass="form-control">
								<form:option value="">請選擇</form:option>
								<form:options items="${adCategoryForm.categoryTypeList}" itemLabel="displayName"  />
							</form:select>
							<form:errors path="categoryType" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="active" class="col-sm-2 control-label">啟用</label>
						<div class="col-sm-10">
							<form:radiobutton path="active" id="radio1" value="1"
								checked="checked" />
							<label for="radio1">是</label><br />
							<form:radiobutton path="active" id="radio2" value="0" />
							<label for="radio2">否</label>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="button" onclick="addCategory()" class="btn btn-info">儲存</button>
					<a href="${pageContext.request.contextPath}/adCategory/query"
						class="btn btn-default pull-right" onclick="goQuery();">取消</a>
				</div>
				<!-- /.box-footer -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>