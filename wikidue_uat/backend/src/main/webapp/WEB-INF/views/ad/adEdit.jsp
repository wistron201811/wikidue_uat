<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>  
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib prefix="sessionConversation" uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 預覽上傳的圖片 -->
<script
	src="${pageContext.request.contextPath}/plugins/uploadPreview/jquery.uploadPreview.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
<script type="text/javascript">
	function modify(){
		document.adForm.method.value = 'modify';
		$.LoadingOverlay("show");
        setTimeout(function(){
        	document.adForm.submit();
        }, 500);
	}
	$(function() {
		$('.datepicker').datepicker({
			format : 'yyyy/mm/dd',
			todayBtn : true,
			todayHighlight : true,
			autoclose : true
		});

		if('${adForm.adCategoryId}'){
			$.each(categoryList, function(key, value) {
				$.each(value, function(index, data) {
					if('${adForm.adCategoryId}' == data.pkAdCategory.toString()){
						changeCategoryType(key);
						document.adForm.adCategoryId.value='${adForm.adCategoryId}';
						document.adForm.categoryType.value=key;
					}
				});
			});
		}
	});
	var categoryList = {};
	function categoryTypeInit(){
		
		<c:forEach items="${adForm.categoryList}" var="type">
		categoryList['${type.key}'] = [];
		<c:forEach items="${type.value}" var="data">
		categoryList['${type.key}'].push({
			pkAdCategory:${data.pkAdCategory},
			categoryCode:'${data.categoryCode}',
			categoryName:'${data.categoryName}',
			categoryType:'${data.categoryType}'
			});
		</c:forEach>
		</c:forEach>
	}
	categoryTypeInit();
	function changeCategoryType(type){
		$("#adCategoryId option").remove();
		$.each(categoryList[type], function(key, value) {
			$('#adCategoryId').append($("<option></option>").attr("value", value.pkAdCategory).text(value.categoryName+'-'+value.categoryCode));
		});
	}
	function goQuery() {
		$.LoadingOverlay("show");
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="adForm" name="adForm"
				action="${pageContext.request.contextPath}/ad/modify"
				enctype="multipart/form-data" class="form-horizontal">
				<form:hidden path="method" value="save" />
				<form:hidden path="pkAdvertisement"/>
				<sessionConversation:insertSessionConversationId
					attributeName="adForm" />
				<div class="box-body">
					<div class="form-group">
						<label for="empNo" class="col-sm-2 control-label">*廣告名稱</label>
						<div class="col-sm-10">
							<form:input path="adsName" class="form-control"
								placeholder="廣告名稱" maxlength="50" />
							<form:errors path="adsName" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="empNo" class="col-sm-2 control-label">*廣告編號</label>
						<div class="col-sm-10">
							<form:input path="bannerCode" class="form-control"
								placeholder="廣告編號" maxlength="50" />
							<form:errors path="bannerCode" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="empNo" class="col-sm-2 control-label">*供應廠商</label>
						<div class="col-sm-10">
							<form:input path="supplier" class="form-control"
								placeholder="供應廠商" maxlength="50" />
							<form:errors path="supplier" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="empNo" class="col-sm-2 control-label">*廠商編號</label>
						<div class="col-sm-10">
							<form:input path="supplierCode" class="form-control"
								placeholder="廠商編號" maxlength="50" />
							<form:errors path="supplierCode" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">*圖片1<br>(所有圖片的寬度要相同
							高度可自訂)</label>
						<div class="col-sm-10">
							<form:errors path="imageOneId" class="text-red" />
							<e7:previewImage attribute="imageOneId" fileStorage="${adForm.imageOneId}" width="250px" height="250px" checkAspectRatio="false" checkSize="false"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"></label>
						<div class="col-sm-10">
							圖片1, 圖片2 在前端為隨機出現
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">圖片2<br>(所有圖片的寬度要相同
							高度可自訂)</label>
						<div class="col-sm-10">
							<form:errors path="imageTwoId" class="text-red" />
							<e7:previewImage attribute="imageTwoId" fileStorage="${adForm.imageTwoId}" width="250px" height="250px" checkAspectRatio="false" checkSize="false"/>
						</div>
					</div>
					<div class="form-group">
						<label for="unit" class="col-sm-2 control-label">*platform</label>
						<div class="col-sm-10">
							<c:forEach items="${adForm.categoryTypeList}" var="type">
								<label class="radio-inline">
							      <input type="radio" name="categoryType" value="${type}" onclick="changeCategoryType(this.value)">${type.displayName}
							    </label>
							</c:forEach>
							<form:select path="adCategoryId" cssClass="form-control">
								
							</form:select>
							
						</div>
					</div>
					<div class="form-group">
						<label for="unit" class="col-sm-2 control-label">*角色/年級</label>
						<div class="col-sm-10">
							<form:checkbox path="roleGrades" value="r1" label="一般會員(訪客)"/><br>
							<form:checkbox path="roleGrades" value="r3" label="教師"/><br>
							<form:checkbox path="roleGrades" value="r5" label="家長"/><br>
							<label>學生：</label><br><form:checkbox path="roleGrades" value="g1" label="一年級"/>、
							<form:checkbox path="roleGrades" value="g2" label="二年級"/>、 
							<form:checkbox path="roleGrades" value="g3" label="三年級"/>、 
							<form:checkbox path="roleGrades" value="g4" label="四年級"/>、 
							<form:checkbox path="roleGrades" value="g5" label="五年級"/>、 
							<form:checkbox path="roleGrades" value="g6" label="六年級"/>、 
							<form:checkbox path="roleGrades" value="g7" label="七年級"/>、 
							<form:checkbox path="roleGrades" value="g8" label="八年級"/>、 
							<form:checkbox path="roleGrades" value="g9" label="九年級"/>、 
							<form:checkbox path="roleGrades" value="g10" label="高一"/>、 
							<form:checkbox path="roleGrades" value="g11" label="高二"/>、 
							<form:checkbox path="roleGrades" value="g12" label="高三"/>
							<%-- <form:checkboxes items="${adForm.roleGradeList}" path="roleGrades" itemValue="code" itemLabel="name" delimiter="、"/> --%>
							<form:errors path="roleGrades" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="unit" class="col-sm-2 control-label">連結網址</label>
						<div class="col-sm-10">
							<form:input path="externalLink" class="form-control"
								placeholder="連結網址" maxlength="50" />
							<form:errors path="externalLink" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="notice" class="col-sm-2 control-label">其他注意事項</label>
						<div class="col-sm-10">
							<form:textarea path="notice" class="form-control"
								placeholder="其他注意事項" maxlength="20" />
							<form:errors path="notice" class="text-red" />
						</div>
					</div>
					<div class="form-group date">
						<label for="publishSDate" class="col-sm-2 control-label">*上架日期</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#publishSDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="publishSDate"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="publishSDate" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="publishEDate" class="col-sm-2 control-label">*下架日期</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#publishEDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="publishEDate"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="publishEDate" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="active" class="col-sm-2 control-label">啟用</label>
						<div class="col-sm-10">
							<form:radiobutton path="active" id="radio1" value="1"
								checked="checked" />
							<label for="radio1">是</label><br />
							<form:radiobutton path="active" id="radio2" value="0" />
							<label for="radio2">否</label>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="button" onclick="modify()" class="btn btn-info">儲存</button>
					<a href="${pageContext.request.contextPath}/ad/query"
						class="btn btn-default pull-right" onclick="goQuery();">取消</a>
				</div>
				<!-- /.box-footer -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>