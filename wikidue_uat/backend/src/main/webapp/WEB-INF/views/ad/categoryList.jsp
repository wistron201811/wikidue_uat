<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<script type="text/javascript">
	function query() {
		$("#method").val("query");
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#adCategoryForm").submit();
        }, 500);
	}

	function remove(pkAdCategory) {
		if (confirm('確認刪除？')) {
			$("#pkAdCategory").val(pkAdCategory);
			document.adCategoryForm.action = '${pageContext.request.contextPath}/adCategory/remove';
			$("#method").val("remove");
			$.LoadingOverlay("show");
	        setTimeout(function(){
	        	$("#adCategoryForm").submit();
	        }, 500);
		}
	}
	function goModify(pkAdCategory) {
		$("#pkAdCategory").val(pkAdCategory);
		document.adCategoryForm.action = '${pageContext.request.contextPath}/adCategory/modify';
		document.adCategoryForm.adCategoryForm_cId.value="";
		$("#method").val("");
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#adCategoryForm").submit();
        }, 500);
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="adCategoryForm" name="adCategoryForm"
				action="${pageContext.request.contextPath}/adCategory/query#result"
				class="form-horizontal">
				<sessionConversation:insertSessionConversationId
					attributeName="adCategoryForm" />
				<form:hidden path="method" />
				<form:hidden path="pkAdCategory" />
				<div class="box-body">
					<div class="form-group">
						<label for="categoryName" class="col-sm-2 control-label">分類名稱</label>
						<div class="col-sm-10">
							<form:input path="categoryName" class="form-control"
								placeholder="分類名稱" maxlength="50" />
							<form:errors path="categoryName" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="categoryCode" class="col-sm-2 control-label">分類編號</label>
						<div class="col-sm-10">
							<form:input path="categoryCode" class="form-control"
								maxlength="10" placeholder="分類編號" />
							<form:errors path="categoryCode" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="categoryType" class="col-sm-2 control-label">platform</label>
						<div class="col-sm-10">
							<form:select path="categoryType" cssClass="form-control">
								<form:option value="">請選擇</form:option>
								<form:options items="${adCategoryForm.categoryTypeList}"
									itemLabel="displayName" />
							</form:select>
						</div>
					</div>
					<button type="button" onclick="query()" class="btn btn-default pull-right">查詢</button>
				</div>
				<!-- /.box-body -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->

		<c:if test="${not empty adCategoryForm.result}">
			<div class="box" id="resultList">
				<div class="box-header">
					<a href="#" name="result"></a>
					<h3 class="box-title">查詢結果</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="dataTables_wrapper form-inline dt-bootstrap">
						<display:table name="adCategoryForm.result.content"
							pagesize="${adCategoryForm.result.size}"
							requestURI='?method=query&adCategoryForm_cId=${curr_adCategoryForm_cId}#result'
							id="list" cellspacing="0" cellpadding="0"
							class="table table-bordered table-hover" partialList="true"
							size="${adCategoryForm.result.totalElements.intValue()}"
							excludedParams="*">
							<display:column title='platform' sortable="true">
								<e7:code type="4" code="${list.categoryType}" />
							</display:column>
							<display:column title='分類名稱' property="categoryName"
								sortable="true">
							</display:column>
							<display:column title='分類編號' property="categoryCode"
								sortable="true">
							</display:column>
							<display:column title='啟用 ' sortable="true">
								<e7:code type="1" code="${list.active}" />
							</display:column>
							<display:column title='操作'>
								<!-- 有修改權限才顯示 -->
								<%-- <sec:authorize url="/account/modify"> --%>
								<button type="button" onclick="goModify('${list.pkAdCategory}');"
									class="btn btn-success btn-sm">修改</button>
								<button type="button" onclick="remove('${list.pkAdCategory}');"
									class="btn btn-danger btn-sm">刪除</button>
								<%-- </sec:authorize> --%>
							</display:column>
						</display:table>
					</div>
				</div>
			</div>
		</c:if>
	</div>
</div>
