<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<script type="text/javascript">
	function query() {
		document.adForm.method.value = 'query';
		$.LoadingOverlay("show");
        setTimeout(function(){
        	document.adForm.submit();
        }, 500);
	}

	function goModify(pkAdvertisement) {
		document.modifyForm.pkAdvertisement.value = pkAdvertisement;
		/* document.modifyForm.adForm_cId.value=""; */
		$.LoadingOverlay("show");
        setTimeout(function(){
        	document.modifyForm.submit();
        }, 500);
	}
	function goQuery() {
		$.LoadingOverlay("show");
	}
</script>
<div class="row">
	<div class="col-md-12">
		<form action="${pageContext.request.contextPath}/ad/modify"
			name="modifyForm" method="post">
			<input type="hidden" name="pkAdvertisement" value="" />
		</form>
		<div class="box box-primary">
			<form:form modelAttribute="adForm" name="adForm"
				action="${pageContext.request.contextPath}/ad/query#result"
				class="form-horizontal">
				<sessionConversation:insertSessionConversationId attributeName="adForm"/>
				<form:hidden path="method" value="query" />
				<div class="box-body">
					<div class="form-group">
						<label for="adsName" class="col-sm-2 control-label">廣告名稱</label>
						<div class="col-sm-10">
							<form:input path="adsName" class="form-control"
								placeholder="廣告名稱" />
						</div>
					</div>
					<button type="button" onclick="query()" class="btn btn-default pull-right">查詢</button>
				</div>
				<!-- /.box-body -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->

		<c:if test="${not empty adForm.result}">
			<div class="box" id="resultList">
				<div class="box-header">
					<a href="#" name="result"></a>
					<h3 class="box-title">查詢結果</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="dataTables_wrapper form-inline dt-bootstrap">
						<display:table name="adForm.result.content"
							pagesize="${adForm.result.size}"
							requestURI='?method=query&adForm_cId=${curr_adForm_cId}#result' id="result" cellspacing="0"
							cellpadding="0" class="table table-bordered table-hover"
							partialList="true"
							size="${adForm.result.totalElements.intValue()}"
							excludedParams="*">
							<display:column title='廣告編號' property="bannerCode" sortable="true">
							</display:column>
							<display:column title='供應廠商' property="supplier" sortable="true">
							</display:column>
							<display:column title='廣告名稱' property="adsName" sortable="true">
							</display:column>
							<display:column title='上架日期' property="publishSDate" format="{0,date,yyyy/MM/dd}" sortable="true">
							</display:column>
							<display:column title='下架日期' property="publishEDate" format="{0,date,yyyy/MM/dd}" sortable="true">
							</display:column>
							<display:column title='狀態' sortable="true">
								<c:if test="${result.active=='1'}">
									<p class="text-success">已上架</p>
								</c:if>
								<c:if test="${result.active=='0'}">
									<p class="text-danger">未上架</p>
								</c:if>
							</display:column>

							<display:column title='操作'>
								<!-- 有修改權限才顯示 -->
								<%-- <sec:authorize url="/account/modify"> --%>
								<button type="button"
									onclick="goModify('${result.pkAdvertisement}')"
									class="btn btn-primary btn-sm">修改</button>
								<%-- </sec:authorize> --%>

							</display:column>
						</display:table>
					</div>
				</div>
			</div>
		</c:if>
	</div>
</div>
