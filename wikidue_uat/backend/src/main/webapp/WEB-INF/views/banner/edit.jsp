<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 預覽上傳的圖片 -->
<script
	src="${pageContext.request.contextPath}/plugins/uploadPreview/jquery.uploadPreview.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
<script type="text/javascript">
	function modify() {
		document.bannerForm.method.value = 'modify';
		$.LoadingOverlay("show");
		setTimeout(function() {
			document.bannerForm.submit();
		}, 500);
	}
	function goQuery() {
		$.LoadingOverlay("show");
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="bannerForm" name="bannerForm"
				action="${pageContext.request.contextPath}/banner/modify"
				method="post" enctype="multipart/form-data" class="form-horizontal">
				<form:hidden path="method" value="modify" />
				<form:hidden path="bannerId" />
				<div class="box-body">
					<div class="form-group">
						<label for="empNo" class="col-sm-2 control-label">*標題</label>
						<div class="col-sm-10">
							<form:input path="title" class="form-control"
								placeholder="標題" maxlength="50" />
							<form:errors path="title" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="imageId" class="col-sm-2 control-label">*圖片<br>(寬度須為
							1500 <br>高度介於550~615之間<br>所有圖片高度須一致)
						</label>
						<div class="col-sm-10">
							<form:errors path="imageId" class="text-red" />
							<e7:previewImage attribute="imageId" fileStorage="${bannerForm.imageId}" previewWidth="750px" previewHeight="300px" width="1500px" height="550px" checkAspectRatio="false" rangeHeight="[550,615]"/>
						</div>
					</div>
					<div class="form-group">
						<label for="unit" class="col-sm-2 control-label">連結網址</label>
						<div class="col-sm-10">
							<form:input path="url" class="form-control"
								placeholder="http://example.com" maxlength="150" />
							<form:errors path="url" class="text-red" />
						</div>
					</div>
					<div class="form-group date">
						<label for="publishSDate" class="col-sm-2 control-label">*上架日期</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#publishSDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="publishSDate"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="publishSDate" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="publishEDate" class="col-sm-2 control-label">*下架日期</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#publishEDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="publishEDate"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="publishEDate" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="publish" class="col-sm-2 control-label">上架狀態</label>
						<div class="col-sm-10">
							<c:if test="${bannerForm.publish=='1'}">
								<div class="text-success">已上架</div>
							</c:if>
							<c:if test="${bannerForm.publish=='0'}">
								<div class="text-danger">未上架</div>
							</c:if>
						</div>
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<button onclick="modify()" class="btn btn-info">儲存</button>
					<a href="${pageContext.request.contextPath}/banner/query"
						class="btn btn-default pull-right" onclick="goQuery();">取消</a>
				</div>
				<!-- /.box-footer -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->
	</div>
</div>