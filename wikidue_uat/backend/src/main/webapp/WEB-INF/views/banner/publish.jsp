<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
	
<script src="${pageContext.request.contextPath}/plugins/multiselect/multiselect-e7.min.js"></script>
<script type="text/javascript">
	$(function() {
		//$('#multiselect').multiselect();

		$('#multiselect').multiselect({
	        search: {
	            left: '<input type="text" name="q" class="form-control" placeholder="搜尋..." />',
	            right: '<input type="text" name="q" class="form-control" placeholder="搜尋..." />',
	        },
	        fireSearch: function(value) {
	            return value.length > 1;
	        },
	        keepRenderingSort: true
	    });
	});
	function doPublish() {

		if(isNaN(document.publishBannerForm.playSpeed.value)){
			alert("輪播速度須為數字");
			return false;
		}
		
		$.each($('#multiselect option'), function(key, value) {
			$("#publishBannerForm").append($("<input type='hidden'/>").attr('name','multiselect').attr("value", $(value).val()));
		});

		$.each($('#multiselect_to option'), function(key, value) {
			$("#publishBannerForm").append($("<input type='hidden'/>").attr('name','multiselectTo').attr("value", $(value).val()));
		});

		
		document.publishBannerForm.method.value = 'publish';
		$.LoadingOverlay("show");
        setTimeout(function(){
        	document.publishBannerForm.submit();
        }, 500);
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<form:form modelAttribute="publishBannerForm" name="publishBannerForm"
				action="${pageContext.request.contextPath}/banner/publish#result">
				<input type="hidden" name="method" value="query" />
			<div class="col-sm-5">
				<div class="box box-primary">
					<div class="box-header">未上架</div>
					<div class="box-body">
				        <select name="from[]" id="multiselect" class="form-control" size="8" multiple="multiple">
				            <c:forEach items="${publishBannerForm.publish}" var="publish">
				            		<option value="${publish.bannerId}">${publish.title}(<fmt:formatDate pattern="yyyy-MM-dd" value="${publish.publishSDate}"/> ~ <fmt:formatDate pattern="yyyy-MM-dd" value="${publish.publishEDate}"/>)</option>
				            </c:forEach>
				        </select>
				    </div>
			    </div>
		    </div>
		    <div class="col-sm-2">
		        <button type="button" id="multiselect_rightAll" class="btn btn-default btn-block"><i class="glyphicon glyphicon-forward"></i></button>
		        <button type="button" id="multiselect_rightSelected" class="btn btn-default btn-block"><i class="glyphicon glyphicon-chevron-right"></i></button>
		        <button type="button" id="multiselect_leftSelected" class="btn btn-default btn-block"><i class="glyphicon glyphicon-chevron-left"></i></button>
		        <button type="button" id="multiselect_leftAll" class="btn btn-default btn-block"><i class="glyphicon glyphicon-backward"></i></button>
		        <br>
		        輪播速度(ms)：<form:input path="playSpeed" class="form-control" maxlength="10"/><br>
		        <button type="button" onclick="doPublish()" class="btn btn-info btn-block">儲存</button>
		    </div>
		    
		    <div class="col-sm-5">
		    		<div class="box box-primary">
			    		<div class="box-header">已上架</div>
					<div class="box-body">
				        <select name="to[]" id="multiselect_to" class="form-control" size="8" multiple="multiple">
				        		<c:forEach items="${publishBannerForm.publishTo}" var="publish">
				            		<option value="${publish.bannerId}">${publish.title}(<fmt:formatDate pattern="yyyy-MM-dd" value="${publish.publishSDate}"/> ~ <fmt:formatDate pattern="yyyy-MM-dd" value="${publish.publishEDate}"/>)</option>
				            </c:forEach>
				        </select>
				        <div class="row">
				            <div class="col-sm-6">
				                <button type="button" id="multiselect_move_up" class="btn btn-default btn-block"><i class="glyphicon glyphicon-arrow-up"></i></button>
				            </div>
				            <div class="col-sm-6">
				                <button type="button" id="multiselect_move_down" class="btn btn-default btn-block col-sm-6"><i class="glyphicon glyphicon-arrow-down"></i></button>
				            </div>
				        </div>
			        </div>
		        </div>
		    </div>
		    </form:form>
		</div>
	</div>
</div>
