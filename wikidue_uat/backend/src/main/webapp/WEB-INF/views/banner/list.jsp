<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<script type="text/javascript">
	$(function() {
		$('.datepicker').datepicker({
			format : 'yyyy/mm/dd',
			autoclose : true,
			todayBtn : true,
			todayHighlight : true
		});
	});
	function query() {
		document.bannerForm.method.value = 'query';
		$.LoadingOverlay("show");
        setTimeout(function(){
        	document.bannerForm.submit();
        }, 500);
	}

	function goModify(bannerId) {
		document.modifyForm.bannerId.value = bannerId;
		/* document.modifyForm.bannerForm_cId.value=""; */
		$.LoadingOverlay("show");
        setTimeout(function(){
        	document.modifyForm.submit();
        }, 500);
	}

	function remove(bannerId, title) {
		if(confirm('確認刪除？['+title+"]")){
			document.deleteForm.bannerId.value = bannerId;
			$.LoadingOverlay("show");
	        setTimeout(function(){
	        	document.deleteForm.submit();
	        }, 500);
		}
	}
</script>
<div class="row">
	<div class="col-md-12">
		<form action="${pageContext.request.contextPath}/banner/modify"
			name="modifyForm" method="post">
			<input type="hidden" name="bannerId" value="" />
		</form>
		<form action="${pageContext.request.contextPath}/banner/query"
			name="deleteForm" method="post">
			<input type="hidden" name="method" value="delete" />
			<input type="hidden" name="bannerId" value="" />
		</form>
		<div class="box box-primary">
			<form:form modelAttribute="bannerForm" name="bannerForm"
				action="${pageContext.request.contextPath}/banner/query#result"
				class="form-horizontal">
				<sessionConversation:insertSessionConversationId attributeName="bannerForm"/>
				<form:hidden path="method" value="query" />
				<div class="box-body">
					<div class="form-group">
						<label for="title" class="col-sm-2 control-label">banner標題</label>
						<div class="col-sm-10">
							<form:input path="title" class="form-control"
								placeholder="標題" />
						</div>
					</div>
					<div class="form-group date">
						<label for="publishSDate" class="col-sm-2 control-label">上架日期</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#publishSDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="publishSDate"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="publishSDate" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="publishEDate" class="col-sm-2 control-label">下架日期</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#publishEDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="publishEDate"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="publishEDate" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="publish" class="col-sm-2 control-label">狀態</label>
						<div class="col-sm-10">
							<form:select path="publish" cssClass="form-control">
								<form:options items="${bannerForm.statusList}"
									itemLabel="name" itemValue="code"/>
							</form:select>
						</div>
					</div>
					<button type="button" onclick="query()" class="btn btn-default pull-right">查詢</button>
				</div>
				<!-- /.box-body -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->

		<c:if test="${not empty bannerForm.result}">
			<div class="box" id="resultList">
				<div class="box-header">
					<a href="#" name="result"></a>
					<h3 class="box-title">查詢結果</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="dataTables_wrapper form-inline dt-bootstrap">
						<display:table name="bannerForm.result.content"
							pagesize="${bannerForm.result.size}"
							requestURI='?method=query&bannerForm_cId=${curr_bannerForm_cId}#result' id="result" cellspacing="0"
							cellpadding="0" class="table table-bordered table-hover"
							partialList="true"
							size="${bannerForm.result.totalElements.intValue()}"
							excludedParams="*">
							<display:column title='標題' property="title" sortable="true">
							</display:column>
							<display:column title='上架日期' property="publishSDate" format="{0,date,yyyy/MM/dd}" sortable="true">
							</display:column>
							<display:column title='下架日期' property="publishEDate" format="{0,date,yyyy/MM/dd}" sortable="true">
							</display:column>
							<display:column title='狀態' sortable="true">
								<c:if test="${result.publish=='1'}">
									<p class="text-success">已上架</p>
								</c:if>
								<c:if test="${result.publish=='0'}">
									<p class="text-danger">未上架</p>
								</c:if>
							</display:column>

							<display:column title='操作'>
								<!-- 有修改權限才顯示 -->
								<%-- <sec:authorize url="/account/modify"> --%>
								<button type="button"
									onclick="goModify('${result.bannerId}')"
									class="btn btn-primary btn-sm">修改</button>
								<button type="button" onclick="remove('${result.bannerId}','${fn:escapeXml(e7:htmlEscape(result.title))}');" class="btn btn-danger btn-sm">刪除</button>
								<%-- </sec:authorize> --%>

							</display:column>
						</display:table>
					</div>
				</div>
			</div>
		</c:if>
	</div>
</div>
