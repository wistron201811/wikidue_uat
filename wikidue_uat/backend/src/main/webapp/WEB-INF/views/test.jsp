<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>sql test</title>
<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
<script src="https://cdn.bootcss.com/jquery.textcomplete/1.8.4/jquery.textcomplete.min.js"></script>
<link href="${pageContext.request.contextPath}/css/jquery.textcomplete.css" type="text/css" rel="stylesheet">
<style type="text/css">
/* Sample */

.dropdown-menu {
    border: 1px solid #ddd;
    background-color: white;
}

.dropdown-menu li {
    border-top: 1px solid #ddd;
    padding: 2px 5px;
}

.dropdown-menu li:first-child {
    border-top: none;
}

.dropdown-menu li:hover,
.dropdown-menu .active {
    background-color: rgb(110, 183, 219);
}


/* SHOULD not modify */

.dropdown-menu {
    list-style: none;
    padding: 0;
    margin: 0;
}

.dropdown-menu a:hover {
    cursor: pointer;
}

</style>
<script type="text/javascript">
$(function() {
	$('#sql').textcomplete([
	    { // tech companies
	        words: ${allTable},
	        match: /\b(\w{1,})$/,
	        search: function (term, callback) {
	            callback($.map(this.words, function (word) {
	                return word.indexOf(term) === 0 ? word : null;
	            }));
	        },
	        index: 1,
	        replace: function (word) {
	            return word + '';
	        }
	    }

	]);
}); 
</script>
</head>
<body>
	<div><b>${error}</b></div>
	<form name="form1" method='post' action='${pageContext.request.contextPath}/common/sys/sql'>
		<%-- <input type="text" name="sql" id="sql" value="${sql}" size="100"> --%>
		<textarea name="sql" id="sql" style="width:500px; height:100px">${sql}</textarea>
		<input type="hidden" name="method" id="method">
		<input type="button" onclick="document.form1.method.value='query';document.form1.submit()" value="查詢"/>
		<input type="button" onclick="document.form1.method.value='execute';document.form1.execute()" value="執行"/>
	</form>
	<div>
		<c:choose>
				<c:when test="${method == 'query'}">
					<table style="width:100%" border="1">
						<c:forEach items="${result}" var="data" varStatus="index">
							<c:if test="${index.index == 0}">
								<tr>
									<c:forEach items="${data}" var="dataMap">
										<th>${dataMap.key}</th>
									</c:forEach>
								</tr>
							</c:if>
							<tr>
								<c:forEach items="${data}" var="dataMap">
									<td>${dataMap.value}</td>
								</c:forEach>
							</tr>
						</c:forEach>
					</table>
				</c:when>
				<c:otherwise>
					${result}
				</c:otherwise>
		</c:choose>
	</div>
</body>
</html>