<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script type="text/javascript">
	$(function() {
		$('.datepicker').datepicker({
			format : 'yyyy/mm/dd',
			autoclose : true,
			todayBtn : true,
			todayHighlight : true
		});
	});

	function query() {
		$("#method").val("query");
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#accusationForm").submit();
        }, 500);
	}

	function approve(pkAccusation, status) {
		var msg = "";
		if (status == 'Y') {
			msg = "確認核可？";
		} else if (status == 'N') {
			msg = "確認下架？";
		}
		if (confirm(msg)) {
			$("#pkAccusation").val(pkAccusation);
			$("#approveStatus").val(status);
			document.accusationForm.action = '${pageContext.request.contextPath}/accusation/modify';
			$("#method").val("modify");
			$.LoadingOverlay("show");
	        setTimeout(function(){
	        	$("#accusationForm").submit();
	        }, 500);
		}
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="accusationForm" name="accusationForm"
				action="${pageContext.request.contextPath}/accusation/query#result"
				class="form-horizontal">
				<sessionConversation:insertSessionConversationId
					attributeName="accusationForm" />
				<form:hidden path="method" />
				<form:hidden path="pkAccusation" />
				<form:hidden path="approveStatus" />
				<div class="box-body">
					<div class="form-group">
						<label for="createDt" class="col-sm-2 control-label">檢舉日期(起)</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon" onclick="$('#createDt').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="createDt"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="createDt" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="createDtEnd" class="col-sm-2 control-label">檢舉日期(迄)</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon" onclick="$('#createDtEnd').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="createDtEnd"
									cssClass="form-control pull-right  datepicker"
									placeholder="yyyy/MM/dd" maxlength="10" />
								<form:errors path="createDtEnd" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="queryStatus" class="col-sm-2 control-label">審核狀態</label>
						<div class="col-sm-10">
							<form:select path="queryStatus" cssClass="form-control">
								<form:option value="">請選擇</form:option>
								<form:option value=" ">審核中</form:option>
								<form:option value="Y">已核可</form:option>
								<form:option value="N">已下架</form:option>
							</form:select>
						</div>
					</div>
					<button type="button" onclick="query()" class="btn btn-default pull-right">查詢</button>
				</div>
				<!-- /.box-body -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->

		<c:if test="${not empty accusationForm.result}">
			<div class="box" id="resultList">
				<div class="box-header">
					<a href="#" name="result"></a>
					<h3 class="box-title">查詢結果</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="dataTables_wrapper form-inline dt-bootstrap">
						<display:table name="accusationForm.result.content"
							pagesize="${accusationForm.result.size}"
							requestURI='?method=query&accusationForm_cId=${curr_accusationForm_cId}#result'
							id="list" cellspacing="0" cellpadding="0"
							class="table table-bordered table-hover" partialList="true"
							size="${accusationForm.result.totalElements.intValue()}"
							excludedParams="*">
							<display:column title='日期' sortable="true">
							<fmt:formatDate pattern="yyyy-MM-dd"  value="${list.createDt}" />
							</display:column>
							<display:column title='檢舉人' property="informer" sortable="true">
							</display:column>
							<display:column title='分享名稱' sortable="true">
							
							<a href="<spring:eval expression="@configService.shareCenterUrl" />${list.shareCenter.pkShare}"
							target="_blank">${list.shareCenter.shareName}</a>
							</display:column>
							<display:column title='檢舉原因' property="reportReason" 
								sortable="true">
							</display:column>
							<!-- 有修改權限才顯示 -->
							<%-- <sec:authorize url="/account/modify"> --%>
							<display:column title='操作'>
								<button type="button"
									onclick="approve('${list.pkAccusation}','Y');"
									class="btn btn-success btn-sm" <c:if test="${list.approveStatus == 'Y' }">disabled</c:if>>核可</button>
								<button type="button"
									onclick="approve('${list.pkAccusation}','N');"
									class="btn btn-danger btn-sm" <c:if test="${list.approveStatus == 'N' }">disabled</c:if>>下架</button>
							</display:column>
							<%-- </sec:authorize> --%>
						</display:table>
					</div>
				</div>
			</div>
		</c:if>
	</div>
</div>
