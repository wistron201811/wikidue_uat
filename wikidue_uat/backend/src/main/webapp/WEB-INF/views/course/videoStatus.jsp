<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/plugins/jquery-loading-overlay-2.1.3/loadingoverlay.min.js"></script>

<script type="text/javascript">
	
</script>
<c:forEach items="${courseChapterList}" var="courseChapter" varStatus="chapterLoop">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">第${chapterLoop.index+1}章-${courseChapter.courseChapterName}</h3>
		</div>
		<div class="box-body no-padding">
			<table class="table table-condensed">
				<tr>
					<th style="width: 10px">#</th>
					<th>單元名稱</th>
					<th style="width: 200px">影片ID</th>
					<th style="width: 80px">影片狀態</th>
				</tr>
				<c:forEach items="${courseChapter.courseVideoList}" var="courseVideo" varStatus="videoLoop">
				<tr>
					<td>${videoLoop.index+1}.</td>
					<td>${courseVideo.courseVideoName}</td>
					<td>${courseVideo.video.pkVideo}</td>
					<td><c:choose>
						<c:when test="${courseVideo.video.result == '1'}"><span class="text-green">已上傳</span></c:when>
						<c:otherwise><span class="text-red">未上傳/未轉檔完</span></c:otherwise>
						</c:choose>
					</td>
				</tr>
				</c:forEach>
			</table>
		</div>
	</div>
</c:forEach>