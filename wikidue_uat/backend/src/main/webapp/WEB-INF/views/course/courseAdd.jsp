<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- 預覽上傳的圖片 -->
<script
	src="${pageContext.request.contextPath}/plugins/uploadPreview/jquery.uploadPreview.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
<script>
var gradeButton;//現在被按的年級 Tag,如沒有TAG被按則為null
var subjectButton;//現在被按的科目 Tag,如沒有TAG被按則為null
var chapterButton;//現在被按的章節 Tag,如沒有TAG被按則為null
var categoryButton;//現在被按的分類 Tag,如沒有TAG被按則為null
var count = 0;
	$(function() {
		$('.datepicker2').datepicker({
			format : 'yyyy/mm/dd',
			autoclose : true,
			todayBtn : true,
			todayHighlight : true
		})
		CKEDITOR.replace('courseExpectation');
		CKEDITOR.replace('coursePreparation');
		CKEDITOR.replace('courseTarget');
		CKEDITOR.replace('courseIntroduction');
		//查詢年級
		queryTags(null,"0");
		//隱藏科目Div
		$("#subjectGroup").hide();
		//隱藏章節Div
		$("#chapterGroup").hide();
	});
	function addCourse() {
		$('#courseForm').attr('action', '${pageContext.request.contextPath}/course/add');
		$("#method").val("add");
		submitForm();
	}
	
	function removeChapter(chapterNo){
		$('#courseForm').attr('action', '${pageContext.request.contextPath}/course/add#tab');
		$("#chapterNo").val(chapterNo);
		$("#method").val("removeChapter");
		submitForm();
	}
	
	function addChapter(){
		$('#courseForm').attr('action', '${pageContext.request.contextPath}/course/add#tab');
		$("#method").val("addChapter");
		submitForm();
	}
	
	function removeUnit(chapterNo,unitNo){
		$('#courseForm').attr('action', '${pageContext.request.contextPath}/course/add#tab');
		$("#chapterNo").val(chapterNo);
		$("#unitNo").val(unitNo);
		$("#method").val("removeUnit");
		submitForm();
	}
	
	function submitForm(){
		//在submit 表單前，將選取的TAG組成JSON格式的字串塞進tagsValue欄位
		var tagsValue = [];
		//將tag的區塊底下所有的label 迴圈
		$("#tagContent").children('label').each(function () {
			//塞進陣列裡
			tagsValue.push({
				"code":$(this).children("[name='code']").val(),
				"name":$(this).children("[name='name']").val(),
				"level":$(this).children("[name='level']").val()
			});
		});
		$("#tagsValue").val(JSON.stringify(tagsValue));
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#courseForm").submit();
        }, 500);
	}
	
	function addUnit(chapterNo){
		$('#courseForm').attr('action', '${pageContext.request.contextPath}/course/add#tab');
		$("#chapterNo").val(chapterNo);
		$("#method").val("addUnit");
		submitForm();
	}
	
	//查詢知識地圖，tag為上一層的code,level為等級(0:年級,1:科目,3:章節)
	function queryTags(tag,level) {
		//將要查詢的TAG清單清空，並顯示"查詢中"
		if(level=='0'){
			$('#gradeContent').html('查詢中...');
		}else if(level=='1'){
			$('#subjectContent').html('查詢中...');
		}else if(level=='3'){
			$('#chapterContent').html('查詢中...');
		}
		
		$.ajax({
			type : "POST",
			url : '${pageContext.request.contextPath}/api/tag/query',
			cache : false,
			contentType : 'application/json',
			data : JSON.stringify({"tag":tag,"level":level}),//查詢的參數
			success : function(result) {
				//查完後，將原本"查詢中"的提示訊息移除
				if(level=='0'){
					$('#gradeContent').html('');
				}else if(level=='1'){
					$('#subjectContent').html('');
				}else if(level=='3'){
					$('#chapterContent').html('');
				}
				//檢查查詢結果及是否有資料
				if (result != null && result!='' && result.success) {
					var data = result.result;
					//依序在TAG清單新增TAG
					if(level=='0'){
						$.each(data, function(i) {
							$('#gradeContent').append("<label name='grade' value='"+data[i].tag+"' onclick='clickGrade(this)' class='btn btn-default btn-sm media-heading' >"+data[i].name+"</label> ");							
						});
					}else if(level=='1'){
						$.each(data, function(i) {
							$('#subjectContent').append("<label name='subject' value='"+data[i].tag+"' onclick='clickSubject(this)' class='btn btn-default btn-sm media-heading'>"+data[i].name+"</label> ");							
						});
					}else if(level=='3'){
						$.each(data, function(i) {
							$('#chapterContent').append("<label name='chapter' value='"+data[i].tag+"' onclick='clickChapter(this)' class='btn btn-default btn-sm media-heading'>"+data[i].name+"</label> ");
						});
					}
				} else {
					//查無資料提示
				}
			},
			error : function(xhr, ajaxOptions, thrownError) {
				//查完後，將原本"查詢中"的提示訊息移除
				if(level=='0'){
					$('#gradeContent').html('');
				}else if(level=='1'){
					$('#subjectContent').html('');
				}else if(level=='3'){
					$('#chapterContent').html('');
				}
				console.log("資料傳輸發生錯誤：" + xhr.statusText + thrownError);
			}
		});
	}
	
	//點擊年級TAG，參數me為自己的element
	function clickGrade(me){
		if(gradeButton){
			$(gradeButton).removeClass( "active" );
			if(gradeButton!=me){
				gradeButton = me;
				$(gradeButton).addClass( "active" );
				$("#subjectGroup").show();
				$("#chapterGroup").hide();
				var tagValue = $(gradeButton).attr("value");
				queryTags(tagValue,"1");
			}else{
				gradeButton = null;
				$("#subjectGroup").hide();
				$("#chapterGroup").hide();
			}
		}else{
			gradeButton = me;
			$(gradeButton).addClass( "active" );
			$("#subjectGroup").show();
			$("#chapterGroup").hide();
			var tagValue = $(gradeButton).attr("value");
			queryTags(tagValue,"1");
		}
	}
	
	function clickSubject(me){
		if(subjectButton){
			$(subjectButton).removeClass( "active" );
			if(subjectButton!=me){
				subjectButton = me;
				$(subjectButton).addClass( "active" );
				$("#chapterGroup").show();
				var tagValue = $(subjectButton).attr("value");
				queryTags(tagValue,"3");
			}else{
				subjectButton = null;
				$("#chapterGroup").hide();
			}
		}else{
			subjectButton = me;
			$(subjectButton).addClass( "active" );
			$("#chapterGroup").show();
			var tagValue = $(subjectButton).attr("value");
			queryTags(tagValue,"3");
		}
	}
	
	function clickChapter(me){
		if(chapterButton){
			$(chapterButton).removeClass( "active" );
			if(chapterButton!=me){
				chapterButton = me;
				$(chapterButton).addClass( "active" );
			}else{
				chapterButton = null;
			}
		}else{
			chapterButton = me;
			$(chapterButton).addClass( "active" );
		}
	}
	
	function removeTag(me){
		$(me).parent("label").remove();
	}
	
	function addTag(){
		//年級code
		var gradeValue = $(".active[name='grade']").attr("value");
		//科目code
		var subjectValue = $(".active[name='subject']").attr("value");
		//章節code
		var chapterValue = $(".active[name='chapter']").attr("value");
		//code
		var tagValue;
		//名稱
		var tagName;
		//等級
		var level;
		if(gradeValue){
			//如果選的tag有年級...
			tagValue = gradeValue;
			tagName = $(".active[name='grade']").text();
			tagLevel = "0";
			if(subjectValue){
				//如果選的tag有科目...
				tagValue = subjectValue;
				tagName += ("/"+$(".active[name='subject']").text());
				tagLevel = "1";
				if(chapterValue){
					//如果選的tag有章節...
					tagValue = chapterValue;
					tagName += ("/"+$(".active[name='chapter']").text());
					tagLevel = "3";
				}
			}
		}
		if(tagValue){
			//如果tagValue有值則新增tag，在label裡面塞 name,code,level，submit 表單時會將label裡的值組成JSON字串回傳
			$('#tagContent').append("<label class='btn-sm tag-box'>"+tagName+"<a href='#' class='tag-remove fa fa-remove' onclick='removeTag(this);return false;'></a><input type='hidden' name='code' value='"+tagValue+"'><input type='hidden' name='name' value='"+tagName+"'><input type='hidden' name='level' value='"+tagLevel+"'></label> ");
		}
	}
	function goQuery() {
		$.LoadingOverlay("show");
	}
</script>
<form:form modelAttribute="courseForm" enctype="multipart/form-data"
	action="${pageContext.request.contextPath}/course/add" method="post"
	autoComplete="Off" class="form-horizontal">
	<input type="hidden" id="method" name="method" />
	<input type="hidden" id="chapterNo" name="chapterNo" />
	<input type="hidden" id="unitNo" name="unitNo" />
	<form:hidden path="tagsValue" />

	<sessionConversation:insertSessionConversationId
		attributeName="courseForm" />
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">新增課程</h3>
				</div>
				<div class="box-body">
					<div class="form-group">
						<label for="courseName" class="col-sm-2 control-label">*課程名稱</label>
						<div class="col-sm-10">
							<form:input path="courseName" cssClass="form-control"
								placeholder="課程名稱" maxlength="20" />
							<form:errors path="courseName" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="materialNum" class="col-sm-2 control-label">料號</label>
						<div class="col-sm-10">
							<form:input path="materialNum" cssClass="form-control"
								placeholder="料號" maxlength="30" />
							<form:errors path="materialNum" class="text-red" />
						</div>
					</div>
					<c:if test="${user_auth.admin}">
						<div class="form-group">
							<label for="vendorUid" class="col-sm-2 control-label">廠商</label>
							<div class="col-sm-10">
								<form:select path="vendorUid" cssClass="form-control">
									<form:option value="">請選擇</form:option>
									<form:options items="${courseForm.vendorList}"
										itemLabel="vendorName" itemValue="uid" />
								</form:select>
								<form:errors path="vendorUid" class="text-red" />
							</div>
						</div>
					</c:if>
					<c:if test="${not user_auth.admin}">
						<form:hidden path="vendorUid" />
						<div class="form-group">
							<label for="vendorName" class="col-sm-2 control-label">廠商</label>
							<div class="col-sm-10">
								<form:input path="vendorName" class="form-control"
									placeholder="廠商" maxlength="50" readonly="true" />
								<form:errors path="vendorName" class="text-red" />
							</div>
						</div>
					</c:if>
					<div class="form-group">
						<label for="publishStartDate" class="col-sm-2 control-label">*上架日期</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#publishStartDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="publishStartDate"
									cssClass="form-control pull-right datepicker2" />
								<form:errors path="publishStartDate" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="publishEndDate" class="col-sm-2 control-label">*下架日期</label>
						<div class="col-sm-10">
							<div class="input-group date">
								<div class="input-group-addon"
									onclick="$('#publishEndDate').focus()">
									<i class="fa fa-calendar"></i>
								</div>
								<form:input path="publishEndDate"
									cssClass="form-control pull-right datepicker2" />
								<form:errors path="publishEndDate" class="text-red" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="active" class="col-sm-2 control-label">*啟用</label>
						<div class="col-sm-10">
							<form:radiobutton path="active" id="radio1" value="1"
								checked="checked" />
							<label for="radio1">是</label><br />
							<form:radiobutton path="active" id="radio2" value="0" />
							<label for="radio2">否</label>
						</div>
					</div>
					<div class="form-group">
						<label for="imageOneFile" class="col-sm-2 control-label">*課程圖示一<br>(建議尺寸700x430或等比例<br>但不得小於建議尺寸)
						</label>
						<div class="col-sm-10">
							<form:errors path="imageOneFile" class="text-red" />
							<e7:previewImage attribute="imageOneFile"
								fileStorage="${courseForm.imageOneFile}" width="700px"
								height="430px" />
						</div>
					</div>
					<div class="form-group">
						<label for="imageTwoFile" class="col-sm-2 control-label">*課程圖示二<br>(建議尺寸700x430或等比例<br>但不得小於建議尺寸)
						</label>
						<div class="col-sm-10">
							<form:errors path="imageTwoFile" class="text-red" />
							<e7:previewImage attribute="imageTwoFile"
								fileStorage="${courseForm.imageTwoFile}" width="700px"
								height="430px" />
						</div>
					</div>
					<div class="form-group">
						<label for="videoUrl" class="col-sm-2 control-label">*Youtube影片ID</label>
						<div class="col-sm-10">
							<form:input path="videoUrl" class="form-control"
								placeholder="https://www.youtube.com/watch?v=xxxxxxx 請填v=後面的字串"
								maxlength="20" />
							<form:errors path="videoUrl" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="price" class="col-sm-2 control-label">收費價格</label>
						<div class="col-sm-10">
							<form:input type="number" path="price" cssClass="form-control"
								maxlength="8" placeholder="原價" />
							<form:errors path="price" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<form:input type="number" path="discountPrice"
								cssClass="form-control" placeholder="優惠價" maxlength="8" />
							<form:errors path="discountPrice" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<form:input type="number" path="costPrice"
								cssClass="form-control" placeholder="進價" maxlength="8" />
							<form:errors path="costPrice" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="courseSummary" class="col-sm-2 control-label">課程簡述</label>
						<div class="col-sm-10">
							<form:input path="courseSummary" cssClass="form-control"
								placeholder="課程簡述" maxlength="90" />
							<form:errors path="courseSummary" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="courseExpectation" class="col-sm-2 control-label">課程目標</label>
						<div class="col-sm-10">
							<textarea id="courseExpectation" name="courseExpectation"
								class="form-control" maxlength="150">
								${e7:previewHtmlImg(pageContext.request.contextPath,courseForm.courseExpectation)}
							</textarea>
							<form:errors path="courseExpectation" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="courseIntroduction" class="col-sm-2 control-label">課程介紹</label>
						<div class="col-sm-10">
							<textarea id="courseIntroduction" name="courseIntroduction"
								class="form-control" maxlength="150">
								${e7:previewHtmlImg(pageContext.request.contextPath,courseForm.courseIntroduction)}
							</textarea>
							<form:errors path="courseIntroduction" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<label for="coursePreparation" class="col-sm-2 control-label">課前準備</label>
						<div class="col-sm-10">
							<textarea id="coursePreparation" name="coursePreparation"
								class="form-control" maxlength="150">
								${e7:previewHtmlImg(pageContext.request.contextPath,courseForm.coursePreparation)}
							</textarea>
							<form:errors path="coursePreparation" class="text-red" />
						</div>
					</div>

					<div class="form-group">
						<label for="courseTarget" class="col-sm-2 control-label">適合對象</label>
						<div class="col-sm-10">
							<textarea id="courseTarget" name="courseTarget"
								class="form-control" maxlength="150">
								${e7:previewHtmlImg(pageContext.request.contextPath,courseForm.courseTarget)}
							</textarea>
							<form:errors path="courseTarget" class="text-red" />
						</div>
					</div>
					<div class="form-group">
						<a id="tab" href="#"></a> <label class="col-sm-2 control-label">
							課程內容<br>
							<button type="button" class="btn btn-sm btn-info"
								onclick="addChapter()">新增章節</button>
						</label>
						<div class="col-sm-10">
							<c:choose>
								<c:when test="${not empty courseForm.courseChapterList}">
									<c:forEach items="${courseForm.courseChapterList}"
										var="courseChapter" varStatus="loop">
										<h3>第${loop.index+1}章</h3>
										<div class="form-group">
											<div class="col-sm-offset-2 col-sm-7">
												<input type="text" class="form-control"
													name="courseChapterList[${loop.index}].name"
													placeholder="章節名稱" value="${courseChapter.name}" />
											</div>
											<div class="col-sm-3">
												<button type="button" class="btn btn-sm btn-info"
													onclick="addUnit(${loop.index})">新增單元</button>
												<button type="button" class="btn btn-sm btn-danger"
													onclick="removeChapter(${loop.index})">刪除章節</button>
											</div>
										</div>
										<c:if test="${not empty courseChapter.courseUnitList}">
											<c:forEach items="${courseChapter.courseUnitList}"
												var="courseUnit" varStatus="loop2">
												<div class="form-group">
													<label class="col-sm-2 control-label">第${loop2.index+1}單元</label>
													<div class="col-sm-5">
														<input type="text" class="form-control"
															name="courseChapterList[${loop.index}].courseUnitList[${loop2.index}].name"
															placeholder="單元名稱" value="${courseUnit.name}" />
													</div>
													<div class="col-sm-2 ">
														<div class="checkbox">
															<label> <input type="checkbox"
																name="courseChapterList[${loop.index}].courseUnitList[${loop2.index}].free"
																<c:if test="${courseUnit.free}"> checked</c:if> />免費單元
															</label>
														</div>
													</div>
													<div class="col-sm-3">
														<button type="button" class="btn btn-sm btn-danger"
															onclick="removeUnit(${loop.index},${loop2.index})">刪除</button>
													</div>
												</div>
											</c:forEach>
										</c:if>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<h3>第1章</h3>
									<div class="form-group">
										<div class="col-sm-offset-2 col-sm-7">
											<input type="text" class="form-control"
												name="courseChapterList[0].name" placeholder="章節名稱"
												maxlength="20" />
										</div>
										<div class="col-sm-3">
											<button type="button" class="btn btn-sm btn-info"
												onclick="addUnit(0)">新增單元</button>
											<button type="button" class="btn btn-sm btn-danger"
												onclick="removeChapter(0)">刪除章節</button>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">第1單元</label>
										<div class="col-sm-5">
											<input type="text" class="form-control"
												name="courseChapterList[0].courseUnitList[0].name"
												placeholder="單元名稱" value="" maxlength="20" />
										</div>
										<div class="col-sm-2">
											<div class="checkbox">
												<label> <input type="checkbox"
													name="courseChapterList[0].courseUnitList[0].free" />免費單元
												</label>
											</div>
										</div>
										<div class="col-sm-3">
											<button type="button" class="btn btn-sm btn-danger"
												onclick="removeUnit(0,0)">刪除</button>
										</div>
									</div>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
					<div class="form-group">
						<label for="teacherId" class="col-sm-2 control-label">課程分類</label>
						<div class="col-sm-10">
							<div class="form-group">
								<div class="col-sm-offset-1 col-sm-11" id="tagContent">
									<c:forEach items="${courseForm.tags}" var="tag"
										varStatus="loop">
										<label class='btn-sm tag-box'>${tag.name}<a href='#'
											class='tag-remove fa fa-remove'
											onclick='removeTag(this);return false;'></a><input
											type='hidden' name='code' value='${tag.code}'><input
											type='hidden' name='name' value='${tag.name}'><input
											type='hidden' name='level' value='${tag.level}'></label>
									</c:forEach>
								</div>
							</div>
							<div class="form-group" id="gradeGroup">
								<label class="col-sm-1 control-label">年級</label>
								<div class="col-sm-11" id="gradeContent"></div>
							</div>
							<div class="form-group" id="subjectGroup">
								<label class="col-sm-1 control-label">科目</label>
								<div class="col-sm-11" id="subjectContent"></div>
							</div>
							<div class="form-group" id="chapterGroup">
								<label class="col-sm-1 control-label">章節</label>
								<div class="col-sm-11" id="chapterContent"></div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-1 col-sm-11">
									<button type="button" class="btn btn-sm btn-info"
										onclick="addTag()">新增分類</button>
								</div>
							</div>
							<div class="form-group" id="categoryGroup">
								<label class="col-sm-1 control-label">分類</label>
								<div class="col-sm-11 checkbox" id="categoryContent">
									<c:forEach items="${categoryList}" var="category">
										<label> <form:checkbox path="categories"
												value="${category.categoryId}" />${category.categoryName}
										</label>
									</c:forEach>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="teacherId" class="col-sm-2 control-label">課程講師</label>
						<div class="col-sm-10">
							<form:select path="teacherId" cssClass="form-control"
								style="width: 100%;" items="${teacherList}"
								itemLabel="teacherName" itemValue="pkTeacher">
							</form:select>
							<form:errors path="teacherId" class="text-red" />
						</div>
					</div>
				</div>
				<div class="box-footer">
					<button type="button" onclick="addCourse()" class="btn btn-info">儲存</button>
					<a href="${pageContext.request.contextPath}/course/query"
						class="btn btn-default pull-right" onclick="goQuery();">取消</a>
				</div>
			</div>
		</div>
	</div>
</form:form>