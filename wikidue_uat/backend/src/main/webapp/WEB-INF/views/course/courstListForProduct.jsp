<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/plugins/jquery-loading-overlay-2.1.3/loadingoverlay.min.js"></script>
<script type="text/javascript">
	$(function() {

	});
	function queryCourse() {
		$("#method").val("query");
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#courseForm").submit();
        }, 500);
	}
	function addCourse(pkProps, propsName, price, discountPrice) {
		parent.showCourse(pkProps, propsName, price, discountPrice);
		parent.$.fancybox.close();
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="courseForm" name="courseForm"
				action="${pageContext.request.contextPath}/course/queryCourse#result"
				class="form-horizontal">
				<sessionConversation:insertSessionConversationId
					attributeName="courseForm" />
				<input type="hidden" name="method" id="method" />
				<input type="hidden" name="courseId" id="courseId" />
				<div class="box-body">
					<c:if test="${user_auth.admin}">
						<div class="form-group">
							<label for="vendorName" class="col-sm-2 control-label">廠商名稱</label>
							<div class="col-sm-10">
								<form:input path="vendorName" class="form-control"
									placeholder="廠商名稱" maxlength="50" />
								<form:errors path="vendorName" class="text-red" />
							</div>
						</div>
					</c:if>
					<div class="form-group">
						<label for="courseName" class="col-sm-2 control-label">課程名稱</label>
						<div class="col-sm-10">
							<form:input path="courseName" class="form-control"
								placeholder="課程名稱" />
						</div>
					</div>
					<div class="form-group">
						<label for="materialNum" class="col-sm-2 control-label">料號</label>
						<div class="col-sm-10">
							<form:input path="materialNum" class="form-control"
								placeholder="料號" />
						</div>
					</div>
					<div class="form-group">
						<label for="type" class="col-sm-2 control-label">狀態</label>
						<div class="col-sm-10">
							<form:select path="active" class="form-control">
								<form:option value="">請選擇</form:option>
								<form:option value="1">啟用</form:option>
								<form:option value="0">停用</form:option>
							</form:select>
						</div>
					</div>
					<button onclick="queryCourse()" class="btn btn-default pull-right">查詢</button>
				</div>
				<!-- /.box-body -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->

		<c:if test="${not empty courseForm.result}">
			<div class="box" id="resultList">
				<div class="box-header">
					<a href="#" name="result"></a>
					<h3 class="box-title">查詢結果</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="dataTables_wrapper form-inline dt-bootstrap">
						<display:table name="courseForm.result.content"
							pagesize="${courseForm.result.size}"
							requestURI='?method=queryCourse&courseForm_cId=${curr_courseForm_cId}#result'
							id="course" cellspacing="0" cellpadding="0"
							class="table table-bordered table-hover" partialList="true"
							size="${courseForm.result.totalElements.intValue()}"
							excludedParams="*">
							<display:column title='料號' property="materialNum" sortable="true" />
							<display:column title='課程名稱' property="courseName"
								sortable="true" />
							<c:if test="${user_auth.admin}">
								<display:column title='廠商名稱' property="vendor.vendorName"
									sortable="true">
								</display:column>
							</c:if>
							<%-- 
							<display:column title='上架日期' sortable="true">
								<fmt:formatDate pattern="yyyy/MM/dd"
									value="${course.publishSDate}" />
							</display:column>
							<display:column title='下架日期' sortable="true">
								<fmt:formatDate pattern="yyyy/MM/dd"
									value="${course.publishEDate}" />
							</display:column>
							--%>
							<display:column title='講師' property="teacher.teacherName"
								sortable="true" />
							<display:column title='原價' sortable="true" >
								<p style="text-align:right">
								<fmt:formatNumber value="${course.price}" type="number"/>
								</p>
							</display:column>
							<display:column title='優專價' sortable="true">
								<p style="text-align:right">
								<fmt:formatNumber value="${course.discountPrice}" type="number"/>
								</p>
							</display:column>
							<display:column title='完整影片' sortable="true">
								<c:if test="${course.hasVideo}">
								<p style="text-align:center">是</p>
								</c:if>
							</display:column>
							<display:column title='啟用 ' sortable="true">
								<p style="text-align:center"><e7:code type="1" code="${course.active}" /></p>
							</display:column>
							<%-- <display:column title='已上架' sortable="true">
								<c:if test="${course.onMarket}">
								<p style="text-align:center">是</p>
								</c:if>
							</display:column> --%>
							<display:column title='操作'>
							<%--<c:if test="${course.active eq '1' && course.hasVideo }">--%>
								<button type="button"
									onclick="addCourse('${course.pkCourse}','${fn:escapeXml(e7:htmlEscape(course.courseName))}','${course.price }','${course.discountPrice }');"
									class="btn btn-primary btn-sm">選擇</button>
							<%--</c:if>--%>
							</display:column>
						</display:table>
					</div>
				</div>
			</div>
		</c:if>
	</div>
</div>
