<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<script type="text/javascript">
	$(function() {
	
	});
	function queryCourse(){
		$("#method").val("query");
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#courseForm").submit();
        }, 500);
	}
	function goModify(courseId) {
		$("#courseId").val(courseId);
		document.courseForm.action = '${pageContext.request.contextPath}/course/modify';
		document.courseForm.courseForm_cId.value="";
		$("#method").val("");
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#courseForm").submit();
        }, 500);
	}
	function goUploadFile(courseId) {
		$("#courseId").val(courseId);
		document.courseForm.action = '${pageContext.request.contextPath}/course/uploadFile';
		$("#method").val("");
		$.LoadingOverlay("show");
        setTimeout(function(){
        	$("#courseForm").submit();
        }, 500);
	}
	function remove(courseId) {
		if (confirm('確認刪除？')) {
			$("#courseId").val(courseId);
			document.courseForm.action = '${pageContext.request.contextPath}/course/remove';
			$("#method").val("remove");
			$.LoadingOverlay("show");
	        setTimeout(function(){
	        	$("#courseForm").submit();
	        }, 500);
		}
	}
	
	function queryVideoStatus(courseId){
		$.fancybox({
			'autoSize' : false,
		    'fitToView': false,
		    'width': "100%",
			'scrolling' : 'auto',
			'transitionIn' : 'none',
			'transitionOut' : 'none',
			'type' : 'iframe',
			'padding' : '0',
			'closeBtn' : true,
			'href' : '${pageContext.request.contextPath}/course/queryVideoStatus?courseId='+courseId+'&d='+new Date().getTime()
	    });
	}
</script>
<div class="row">
	<div class="col-md-12">
		<div class="box box-primary">
			<form:form modelAttribute="courseForm" name="courseForm"
				action="${pageContext.request.contextPath}/course/query#result"
				class="form-horizontal">
				<sessionConversation:insertSessionConversationId
					attributeName="courseForm" />
				<input type="hidden" name="method" id="method" />
				<input type="hidden" name="courseId" id="courseId" />
				<div class="box-body">
					<c:if test="${user_auth.admin}">
						<div class="form-group">
							<label for="vendorName" class="col-sm-2 control-label">廠商名稱</label>
							<div class="col-sm-10">
								<form:input path="vendorName" class="form-control"
									placeholder="廠商名稱" maxlength="50" />
								<form:errors path="vendorName" class="text-red" />
							</div>
						</div>
					</c:if>
					<div class="form-group">
						<label for="courseName" class="col-sm-2 control-label">課程名稱</label>
						<div class="col-sm-10">
							<form:input path="courseName" class="form-control"
								placeholder="課程名稱" />
						</div>
					</div>
					<div class="form-group">
						<label for="materialNum" class="col-sm-2 control-label">料號</label>
						<div class="col-sm-10">
							<form:input path="materialNum" class="form-control"
								placeholder="料號" />
						</div>
					</div>
					<div class="form-group">
						<label for="type" class="col-sm-2 control-label">狀態</label>
						<div class="col-sm-10">
							<form:select path="active" class="form-control">
								<form:option value="">請選擇</form:option>
								<form:option value="1">啟用</form:option>
								<form:option value="0">停用</form:option>
							</form:select>
						</div>
					</div>
					<button onclick="queryCourse()" class="btn btn-default pull-right">查詢</button>
				</div>
				<!-- /.box-body -->
			</form:form>

			<!-- /.box-body -->
		</div>
		<!-- /.box -->

		<c:if test="${not empty courseForm.result}">
			<div class="box" id="resultList">
				<div class="box-header">
					<a href="#" name="result"></a>
					<h3 class="box-title">查詢結果</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="dataTables_wrapper form-inline dt-bootstrap">
						<display:table name="courseForm.result.content"
							pagesize="${courseForm.result.size}"
							requestURI='?method=query&courseForm_cId=${curr_courseForm_cId}#result'
							id="course" cellspacing="0" cellpadding="0"
							class="table table-bordered table-hover" partialList="true"
							size="${courseForm.result.totalElements.intValue()}"
							excludedParams="*">
							<display:column title='料號' property="materialNum" sortable="true" />
							<display:column title='課程名稱' property="courseName"
								sortable="true" />
							<c:if test="${user_auth.admin}">
								<display:column title='廠商名稱' property="vendor.vendorName"
									sortable="true">
								</display:column>
							</c:if>
							<display:column title='上架日期' sortable="true">
								<fmt:formatDate pattern="yyyy/MM/dd"
									value="${course.publishSDate}" />
							</display:column>
							<display:column title='下架日期' sortable="true">
								<fmt:formatDate pattern="yyyy/MM/dd"
									value="${course.publishEDate}" />
							</display:column>
							<display:column title='講師' property="teacher.teacherName"
								sortable="true" />
							<display:column title='價格' property="price" sortable="true" />
							<display:column title='啟用 ' sortable="true">
								<e7:code type="1" code="${course.active}" />
							</display:column>
							<display:column title='操作'>
									<sec:authorize url="/course/queryVideoStatus">
									<button type="button"
										 onclick="queryVideoStatus(${course.pkCourse});"
										class="btn btn-info btn-sm">查看影片狀態</button>
									</sec:authorize>
									<sec:authorize url="/course/uploadFile">
									<button type="button"
										 onclick="goUploadFile(${course.pkCourse});"
										class="btn btn-success btn-sm">檔案管理</button>
									</sec:authorize>
									<!-- 有修改權限才顯示 -->
									<sec:authorize url="/course/modify">
									<button type="button"
										 onclick="goModify('${course.pkCourse}');"
										class="btn btn-primary btn-sm">修改</button>
									<button type="button" onclick="remove('${course.pkCourse}');" class="btn btn-danger btn-sm">刪除</button>
									</sec:authorize>
								</display:column>
						</display:table>
					</div>
				</div>
			</div>
		</c:if>
	</div>
</div>
