<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib prefix="sessionConversation"
	uri="/WEB-INF/tld/sessionConversation.tld"%>

<script>
	var FILE_SIZE_LIMIT = 15*1024*1024;
	$(function() {
	});
	
	function upload() {
		var file = document.getElementById('uploadFile');
		if(file.files.length == 0 ){
			alert("請選擇要上傳的檔案");
		} else if(file.files[0].size>FILE_SIZE_LIMIT){
			alert("檔案大小不得超過15mb");
		} else if(file.files[0].type.startsWith("video")||file.files[0].type.startsWith("audio")){
			alert("課程檔案不得為影音");
		} else{
			$("#method").val("uploadFile");
			$.LoadingOverlay("show");
	        setTimeout(function(){
	        	$("#courseForm").submit();
	        }, 500);
		}
	}
	
	function deleteFile(courseFileId,fileName){
		if(confirm("確認刪除檔案?:"+fileName)){
			$("#courseFileId").val(courseFileId);
			$("#method").val("deleteFile");
			$.LoadingOverlay("show");
	        setTimeout(function(){
	        	$("#courseForm").submit();
	        }, 500);
		}
	}
	function goQuery() {
		$.LoadingOverlay("show");
	}
</script>
<form:form modelAttribute="courseForm" enctype="multipart/form-data"
	action="${pageContext.request.contextPath}/course/uploadFile" method="post"
	autoComplete="Off" class="form-horizontal">
	<input type="hidden" id="method" name="method" />
	<form:hidden path="courseId"/>
	<form:hidden path="courseFileId"/>
	<form:hidden path="courseName"/>
	<form:hidden path="materialNum"/>
	
	<sessionConversation:insertSessionConversationId
					attributeName="courseForm" />
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">課程檔案上傳</h3>
				</div>
				<div class="box-body">
					<div class="form-group">
						<label class="col-sm-2 control-label">課程名稱</label>
						<label class="col-sm-10 checkbox-inline pull-left">${courseForm.courseName}</label>
						
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">料號</label>
						<label class="col-sm-10 checkbox-inline pull-left">${courseForm.materialNum}</label>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">課程檔案清單</label>
							<c:choose>
							<c:when test="${not empty courseForm.courseFiles}">
								<div class="col-sm-10">
									<c:forEach items="${courseForm.courseFiles}" var="courseFile">
										<div class="form-group">
											<label class="col-sm-10 checkbox-inline">${courseFile.fileName}</label>
											<div class="col-sm-2">												
											<button type="button" class="btn btn-sm btn-danger" onclick="deleteFile('${courseFile.courseFileId}','${courseFile.fileName}')">刪除</button>
											</div>
			                  			</div>
									</c:forEach>
								</div>
							</c:when>
							<c:otherwise>
							<label class="col-sm-10 checkbox-inline pull-left">暫無檔案</label>
							</c:otherwise>
							</c:choose>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">新增檔案</label>
						<div class="col-sm-10">
							<div class="col-sm-10 checkbox-inline ">
               					<input type="file" name="uploadFile" id="uploadFile">
								<form:errors path="uploadFile" class="text-red" /><br>
               					檔案限"非影音檔"，單一檔案最大為15mb
               				</div>
               				<div class="col-sm-2">												
								<button type="button" class="btn btn-sm btn-info" onclick="upload()">上傳</button>
							</div>
                		</div>
            		</div>
				</div>
				<div class="box-footer">
					<a href="${pageContext.request.contextPath}/course/query"
						class="btn btn-default pull-right" onclick="goQuery();">回上一頁</a>
				</div>
			</div>
		</div>
	</div>
</form:form>