<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isErrorPage="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Redirect Error Page</title>
</head>
<body>
	<%
		if (exception != null)
			System.out.println(exception);
		
		String page403 = "error";
		String page404 = "404";
		String page500 = "error";
		String page502 = "error";
		String errorCode = request.getParameter("errorCode");
		if ("403".equals(errorCode))
			response.sendRedirect(request.getContextPath()+"/"+response.encodeURL(page403));
		else if ("404".equals(errorCode))
			response.sendRedirect(request.getContextPath()+"/"+response.encodeURL(page404));
		else if ("500".equals(errorCode))
			response.sendRedirect(request.getContextPath()+"/"+response.encodeURL(page500));
		else if ("502".equals(errorCode))
			response.sendRedirect(request.getContextPath()+"/"+response.encodeURL(page502));
		else
			response.sendRedirect(request.getContextPath()+"/"+response.encodeURL(page500));
	%>
</body>
</html>