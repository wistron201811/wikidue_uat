package com.e7learning.favourite.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/favourite")
public class FavouriteController {
	
	private static final String PAGE = "favouritePage";
	
	@RequestMapping(value = "/home", method = { RequestMethod.GET })
	public String goHome(Model model) {
		return PAGE;
	}

}
