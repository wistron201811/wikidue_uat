package com.e7learning.common.taglib;

import java.util.Base64;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.MultipartFile;

import com.e7learning.common.service.ConfigService;
import com.e7learning.repository.model.FileStorage;

public class ImagePreviewTag extends SimpleTagSupport {

	/***/
	private static final Logger LOG = Logger.getLogger(ImagePreviewTag.class);

	private String attribute;

	private String height = "200px";

	private String width = "auto";
	
	private String previewHeight = "";

	private String previewWidth = "";
	
	private boolean checkAspectRatio = true;
	
	private boolean checkSize = true;
	
	private String rangeHeight = "[]";
	
	private String css = "";

	private MultipartFile fileStorage;

	@Override
	public void doTag() {
		try {
			PageContext pageContext = (PageContext) getJspContext();
			ServletContext servletContext = pageContext.getServletContext();
			
			WebApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(
	                pageContext.getServletContext());
			ConfigService configService = context.getBean(ConfigService.class);
	 

			StringBuffer html = new StringBuffer();
			String attrName = attribute;
			if (fileStorage != null && StringUtils.isNotBlank(fileStorage.getName())
					&& !(fileStorage instanceof FileStorage) && !fileStorage.isEmpty()) {
				attrName = "tmp_"+attrName;
			}
			if(StringUtils.isBlank(previewHeight)) {
				previewHeight = height;
			}
			if(StringUtils.isBlank(previewWidth)) {
				previewWidth = width;
			}

			html.append("<div id=\"" + attribute + "-preview\" class=\"image-preview\" style=\"width: " + previewWidth
					+ ";height: " + previewHeight
					+ ";position: relative;overflow: hidden;background-color: #d2d2d2;color: #ecf0f1;\">");
			html.append("<label for=\"" + attrName + "\" id=\"" + attribute + "-label\">選擇圖片</label> ");
			html.append("<input type=\"file\" name=\"" + attrName + "\" id=\"" + attrName + "\" value=\"\" />");
			html.append("</div>");
			html.append("<script>");
			html.append("document.ready(function () {");
			html.append("$.uploadPreview({");
			html.append("input_field : \"#" + attrName + "\",");
			html.append("preview_box : \"#" + attribute + "-preview\",");
			html.append("label_field : \"#" + attribute + "-label\",");
			html.append("width : \"" + width + "\",");
			html.append("height : \""+ height + "\",");
			html.append("range_height : \""+ rangeHeight + "\",");
			if (configService != null)
				html.append("maxSize : " + configService.getImageMaxUploadSize() + ",");
			html.append("check_aspect_ratio : "+ checkAspectRatio + ",");
			html.append("check_size : "+ checkSize + ",");
			html.append("label_selected : \"更換圖片\",");
			html.append("success_callback: function() {");
			html.append("if($(\"#" + attribute + "\").val().length == 0){");
			html.append("$(\"#" + attribute + "-preview\").css(\"background-image\",\"\");}");
			if (fileStorage != null && StringUtils.isNotBlank(fileStorage.getName())) {
				if (fileStorage instanceof FileStorage) {
					html.append("else {$(\"#" + attribute + "-preview\").css(\"background-image\", \"url("
							+ servletContext.getContextPath() + "/api/query/image/" + fileStorage.getName() + "?d="
							+ new Date().getTime() + ")\");}");
				} else {
					html.append("else {$(\"#" + attribute
							+ "-preview\").css(\"background-image\", \"url('data:image/jpeg;base64,"
							+ Base64.getEncoder().encodeToString(fileStorage.getBytes()) + "')\");}");
				}

			}
			html.append("}");
			html.append("});");
			if (fileStorage != null && StringUtils.isNotBlank(fileStorage.getName())) {
				if (fileStorage instanceof FileStorage) {
					html.append("$(\"#" + attribute + "-preview\").css(\"background-image\", \"url("
							+ servletContext.getContextPath() + "/api/query/image/" + fileStorage.getName() + "?d="
							+ new Date().getTime() + ")\");");
				} else {
					html.append(
							"$(\"#" + attribute + "-preview\").css(\"background-image\", \"url('data:image/jpeg;base64,"
									+ Base64.getEncoder().encodeToString(fileStorage.getBytes()) + "')\");");
				}
				html.append("$(\"#" + attribute + "-preview\").css(\"background-size\", \"contain\");");
				html.append("$(\"#" + attribute + "-preview\").css(\"background-position\", \"center center\");");
				html.append("$(\"#" + attribute + "-preview\").css(\"background-repeat\", \"no-repeat\");");
			}
			html.append("});");
			html.append("</script>");

			JspWriter out = getJspContext().getOut();
			out.print(html.toString());
		} catch (Exception e) {
			LOG.error("Exception:", e);
		}
	}

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public void setWidth(String width) {
		this.width = width;
	}

	public void setFileStorage(MultipartFile fileStorage) {
		this.fileStorage = fileStorage;
	}

	public void setCheckAspectRatio(boolean checkAspectRatio) {
		this.checkAspectRatio = checkAspectRatio;
	}

	public void setCheckSize(boolean checkSize) {
		this.checkSize = checkSize;
	}

	public void setRangeHeight(String rangeHeight) {
		this.rangeHeight = rangeHeight;
	}

	public void setPreviewHeight(String previewHeight) {
		this.previewHeight = previewHeight;
	}

	public void setPreviewWidth(String previewWidth) {
		this.previewWidth = previewWidth;
	}

	public void setCss(String css) {
		this.css = css;
	}
	
	
	

}
