package com.e7learning.common.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.e7learning.api.domain.result.CategoryResult;
import com.e7learning.api.domain.result.KnowledgeMapResult;

public class TagUtils {

	public static List<KnowledgeMapResult> expandTag(List<KnowledgeMapResult> tags) {
		Set<KnowledgeMapResult> result = new HashSet<>();
		tags.stream().forEach(v -> {
			String[] tagL = v.getTag().split("-");
			StringBuilder nTag = new StringBuilder();
			Arrays.stream(tagL).forEach(val -> {
				if (nTag.length() > 0) {
					nTag.append("-");
				}
				nTag.append(val);
				KnowledgeMapResult t = new KnowledgeMapResult();
				t.setName(v.getName());
				t.setTag(nTag.toString());
				t.setLevel(String.valueOf(t.getTag().split("-").length - 1));

				result.add(t);
			});
		});

		List<KnowledgeMapResult> newTags = new ArrayList<>();
		newTags.addAll(result);
		return newTags;

	}

	public static List<KnowledgeMapResult> appendCategory(List<KnowledgeMapResult> tags,
			List<CategoryResult> category) {
		Set<KnowledgeMapResult> result = new HashSet<>();
		category.stream().forEach(c -> {
			tags.stream().forEach(tag -> {
				KnowledgeMapResult t = new KnowledgeMapResult();
				t.setName(tag.getName());
				t.setTag(tag.getTag() + "-" + c.getCategoryId());
				t.setLevel(StringUtils.EMPTY);
				result.add(t);
			});
			// 新增一筆category
			KnowledgeMapResult t = new KnowledgeMapResult();
			t.setName(StringUtils.EMPTY);
			t.setTag(String.valueOf(c.getCategoryId()));
			t.setLevel(StringUtils.EMPTY);
			result.add(t);
		});
		List<KnowledgeMapResult> newTags = new ArrayList<>();
		newTags.addAll(tags);
		newTags.addAll(result);
		return newTags;

	}
}