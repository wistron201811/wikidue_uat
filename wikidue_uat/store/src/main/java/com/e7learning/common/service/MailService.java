package com.e7learning.common.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import com.e7learning.common.utils.MailLogService;

@Service
public class MailService {

	@Autowired
	private JavaMailSender mailSender;
	@Autowired
	private ConfigService configService;
	@Autowired
	private TemplateService templateService;

	private static final Logger log = Logger.getLogger(MailService.class);

	private static final String DATE_FORMAT = "yyyy/MM/dd";

	public void sendShopSuccessNotificationEmail(String[] email, String[] copy, Date sendDate, String buyerName,
			String orderNo, List<Map<String, String>> details, String total, String recipient, String telPhone,
			String mobile, String address, boolean mail) {
		if (email == null) {
			email = new String[] {};
		}
		if (copy == null) {
			copy = new String[] {};
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			MimeMessagePreparator preparator = getMessagePreparator4ShopSuccessNotification(email, copy,
					configService.getHost(), configService.getImageUrl(), sdf.format(sendDate), buyerName,
					configService.getMyOrderUrl(), orderNo, details, total, recipient, telPhone, mobile, address,
					configService.getServiceUrl(), mail);
			mailSender.send(preparator);
		} catch (Exception e) {
			MailLogService.writeErrorLog(String.format("Wikidue Store 教育商城 - 訂單成立通知信(訂單編號 %s)", orderNo), email, copy,
					e);
		}
	}

	public void sendSellNotificationEmail(String[] email, String[] copy, Date sendDate, String vendorName,
			String buyerName, Date orderDate, List<Map<String, String>> details, String total, String recipient,
			String telPhone, String mobile, String address, String orderNo, boolean mail) {
		if (email == null) {
			email = new String[] {};
		}
		if (copy == null) {
			copy = new String[] {};
		}

		try {
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			MimeMessagePreparator preparator = getMessagePreparator4SellNotification(email, copy,
					configService.getHost(), configService.getImageUrl(), sdf.format(sendDate), vendorName, buyerName,
					sdf.format(orderDate), configService.getBackendUrl(), details, total, recipient, telPhone, mobile,
					address, orderNo, configService.getBackendManualUrl(), configService.getServiceUrl(), mail);
			mailSender.send(preparator);
		} catch (Exception e) {
			MailLogService.writeErrorLog(String.format("Wikidue Store 教育商城 - 訂單編號 %s 訂單成立通知信", orderNo), email, copy,
					e);
		}
	}

	public void sendCancelOrderNotificationBuyerEmail(String[] email, String[] copy, Date sendDate, String buyerName,
			Date cancelDate, String orderNo, List<Map<String, String>> details, String total) {
		if (email == null) {
			email = new String[] {};
		}
		if (copy == null) {
			copy = new String[] {};
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			MimeMessagePreparator preparator = getMessagePreparator4CancelOrderNotificationBuyer(email, copy,
					configService.getHost(), sdf.format(sendDate), buyerName, configService.getImageUrl(),
					sdf.format(cancelDate), orderNo, details, total, configService.getMyOrderUrl(),
					configService.getServiceUrl());
			mailSender.send(preparator);
		} catch (Exception e) {
			MailLogService.writeErrorLog(String.format("Wikidue Store 教育商城 - 訂單編號 %s 取消訂單通知信", orderNo), email, copy,
					e);
		}
	}

	public void sendCancelOrderNotificationVendorEmail(String[] email, String[] copy, Date sendDate, String vendorName,
			String orderNo, Date cancelDate, List<Map<String, String>> details, String total) {
		if (email == null) {
			email = new String[] {};
		}
		if (copy == null) {
			copy = new String[] {};
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			MimeMessagePreparator preparator = getMessagePreparator4CancelOrderNotificationVendor(email, copy,
					configService.getHost(), configService.getImageUrl(), sdf.format(sendDate), vendorName, orderNo,
					sdf.format(cancelDate), configService.getBackendUrl(), details, total,
					configService.getBackendManualUrl(), configService.getServiceUrl());
			mailSender.send(preparator);
		} catch (Exception e) {
			MailLogService.writeErrorLog(String.format("Wikidue Store 教育商城 - 訂單編號 %s 取消訂單通知信", orderNo), email, copy,
					e);
		}
	}

	public void sendSellNotificationAdminEmail(String[] email, String[] copy, Date sendDate, String buyerName,
			Date orderDate, String orderNo, String invoiceNo, String payWay, String total, String ratioTotal,
			String recipient, String telPhone, String mobile, String address, List<Map<String, String>> details,
			List<Map<String, String>> vendorList, boolean mail) {
		if (email == null) {
			email = new String[] {};
		}
		if (copy == null) {
			copy = new String[] {};
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			MimeMessagePreparator preparator = getMessagePreparator4SellNotificationAdmin(email, copy,
					configService.getHost(), configService.getImageUrl(), sdf.format(sendDate), buyerName,
					sdf.format(orderDate), configService.getBackendUrl(), orderNo, invoiceNo, payWay, total, ratioTotal,
					recipient, telPhone, mobile, address, details, vendorList, mail);
			mailSender.send(preparator);
		} catch (Exception e) {
			MailLogService.writeErrorLog(String.format("Wikidue Store 教育商城 - 訂單編號 %s 訂單成立通知信", orderNo), email, copy,
					e);
		}
	}

	public void sendAccusationNotificationEmail(String[] email, String[] copy, Date sendDate, String member,
			Date accuseDate, String accuser, String shareTitle, String accuseReason) {
		if (email == null) {
			email = new String[] {};
		}
		if (copy == null) {
			copy = new String[] {};
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			MimeMessagePreparator preparator = getMessagePreparator4AccusationNotification(email, copy,
					configService.getHost(), configService.getImageUrl(), sdf.format(sendDate), member,
					sdf.format(accuseDate), accuser, shareTitle, accuseReason);
			mailSender.send(preparator);
		} catch (Exception e) {
			MailLogService.writeErrorLog("Wikidue Store 教育商城 - 分享園地檢舉通知信", email, copy, e);
		}
	}

	private MimeMessagePreparator getMessagePreparator4ShopSuccessNotification(String[] email, String[] copy,
			String storeUrl, String imageUrl, String sendDate, String buyerName, String myOrderUrl, String orderNo,
			List<Map<String, String>> details, String total, String recipient, String telPhone, String mobile,
			String address, String serviceUrl, boolean mail) {

		return new MimeMessagePreparator() {

			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");

				helper.setSubject(String.format("Wikidue Store 教育商城 - 訂單成立通知信(訂單編號 %s)", orderNo));
				helper.setTo(email);
				helper.setCc(copy);
				helper.setFrom(configService.getEmailSender());
				helper.setText(templateService.getShopSuccessNotification(storeUrl, imageUrl, sendDate, buyerName,
						myOrderUrl, orderNo, details, total, recipient, telPhone, mobile, address, serviceUrl, mail),
						true);
			}
		};
	}

	private MimeMessagePreparator getMessagePreparator4SellNotification(String[] email, String[] copy, String storeUrl,
			String imageUrl, String sendDate, String vendorName, String buyerName, String orderDate, String backendUrl,
			List<Map<String, String>> details, String total, String recipient, String telPhone, String mobile,
			String address, String orderNo, String backendManualUrl, String serviceUrl, boolean mail) {

		return new MimeMessagePreparator() {

			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");

				helper.setSubject(String.format("Wikidue Store 教育商城 - 訂單編號 %s 訂單成立通知信", orderNo));
				helper.setTo(email);
				helper.setCc(copy);
				helper.setFrom(configService.getEmailSender());
				helper.setText(templateService.getSellNotification(storeUrl, imageUrl, sendDate, vendorName, buyerName,
						orderDate, backendUrl, details, total, recipient, telPhone, mobile, address, orderNo,
						backendManualUrl, serviceUrl, mail), true);
			}
		};
	}

	private MimeMessagePreparator getMessagePreparator4CancelOrderNotificationBuyer(String[] email, String[] copy,
			String storeUrl, String sendDate, String buyerName, String imageUrl, String cancelDate, String orderNo,
			List<Map<String, String>> details, String total, String myOrderUrl, String serviceUrl) {

		return new MimeMessagePreparator() {

			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");

				helper.setSubject(String.format("Wikidue Store 教育商城 - 訂單編號 %s 取消訂單通知信", orderNo));
				helper.setTo(email);
				helper.setCc(copy);
				helper.setFrom(configService.getEmailSender());
				helper.setText(templateService.getCancelOrderNotificationBuyer(storeUrl, sendDate, buyerName, imageUrl,
						cancelDate, orderNo, details, total, myOrderUrl, serviceUrl), true);
			}
		};
	}

	private MimeMessagePreparator getMessagePreparator4CancelOrderNotificationVendor(String[] email, String[] copy,
			String storeUrl, String imageUrl, String sendDate, String vendorName, String orderNo, String cancelDate,
			String backendUrl, List<Map<String, String>> details, String total, String backendManualUrl,
			String serviceUrl) {

		return new MimeMessagePreparator() {

			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");

				helper.setSubject(String.format("Wikidue Store 教育商城 - 訂單編號 %s 取消訂單通知信", orderNo));
				helper.setTo(email);
				helper.setCc(copy);
				helper.setFrom(configService.getEmailSender());
				helper.setText(templateService.getCancelOrderNotificationVendor(storeUrl, imageUrl, sendDate,
						vendorName, orderNo, cancelDate, backendUrl, details, total, backendManualUrl, serviceUrl),
						true);
			}
		};
	}

	private MimeMessagePreparator getMessagePreparator4SellNotificationAdmin(String[] email, String[] copy,
			String storeUrl, String imageUrl, String sendDate, String buyerName, String orderDate, String backendUrl,
			String orderNo, String invoiceNo, String payWay, String total, String ratioTotal, String recipient,
			String telPhone, String mobile, String address, List<Map<String, String>> details,
			List<Map<String, String>> vendorList, boolean mail) {

		return new MimeMessagePreparator() {

			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");

				helper.setSubject(String.format("Wikidue Store 教育商城 - 訂單編號 %s 訂單成立通知信", orderNo));
				helper.setTo(email);
				helper.setCc(copy);
				helper.setFrom(configService.getEmailSender());
				helper.setText(templateService.getSellNotificationAdmin(storeUrl, imageUrl, sendDate, buyerName,
						orderDate, backendUrl, orderNo, invoiceNo, payWay, total, ratioTotal, recipient, telPhone,
						mobile, address, details, vendorList, mail), true);
			}
		};
	}

	private MimeMessagePreparator getMessagePreparator4AccusationNotification(String[] email, String[] copy,
			String storeUrl, String imageUrl, String sendDate, String member, String accuseDate, String accuser,
			String shareTitle, String accuseReason) {

		return new MimeMessagePreparator() {

			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");

				helper.setSubject("Wikidue Store 教育商城 - 分享園地檢舉通知信");
				helper.setTo(email);
				helper.setCc(copy);
				helper.setFrom(configService.getEmailSender());
				helper.setText(templateService.getAccusationNotification(storeUrl, imageUrl, sendDate, member,
						accuseDate, accuser, shareTitle, accuseReason), true);
			}
		};
	}
}
