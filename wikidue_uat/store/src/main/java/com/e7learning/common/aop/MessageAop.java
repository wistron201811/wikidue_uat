package com.e7learning.common.aop;

import java.util.Calendar;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.time.StopWatch;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.WebUtils;

import com.e7learning.common.Constant;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.RestException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.service.SsoService;
import com.e7learning.common.vo.SsoUserInfoResponse;
import com.e7learning.common.vo.UserAuthVo;
import com.google.gson.Gson;

@Aspect
@Order(1)
@Component
public class MessageAop {
	/**
	 * log
	 */
	private static final Logger LOG = Logger.getLogger(MessageAop.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private ConfigService configService;

	@Autowired
	private SsoService ssoService;

	@Around("execution(* com.e7learning.*.controller..*.*(..))")
	public Object accessAuth(ProceedingJoinPoint joinPoint) throws Throwable {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getRequest();
		HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
				.getResponse();
		AuthThreadLocal.setRequest(request);
		request.setAttribute(Constant.LOGOUT_REDIRECT_URI, configService.getHost()
				+ configService.getSsoLogoutRedirectUri() + "?d=" + Calendar.getInstance().getTimeInMillis());
		request.setAttribute(Constant.SSO_LOGOUT_PAGE, configService.getSsoLogoutPage());

		request.setAttribute("ssoLoginPage", configService.getSsoLoginPage());
		request.setAttribute("clientId", configService.getSsoClientId());
		request.setAttribute("responseType", configService.getSsoLoginResponseType());
		request.setAttribute("redirectUri", configService.getHost() + configService.getSsoLoginRedirectUri());
		checkAuth(request, response);

		return joinPoint.proceed();
		// return logging(joinPoint);
	}

	@Pointcut("execution(* com.e7learning.*.service..*.*(..))")
	public void service() {
	}

	@Around("service()")
	public Object logging(ProceedingJoinPoint joinPoint) throws Throwable {
		String clazz = joinPoint.getTarget().getClass().getSimpleName();
		String method = joinPoint.getSignature().getName();
		StopWatch watch = new StopWatch();
		watch.start();
		Object result = null;
		try {
			result = joinPoint.proceed();
		} catch (ServiceException e) {
			AuthThreadLocal.getRequest().setAttribute(Constant.ERROR_MESSAGE_KEY, e.getMessage());
			AuthThreadLocal.getRequest().getSession().setAttribute(Constant.ERROR_MESSAGE_KEY, e.getMessage());
			LOG.info("set err msg");
			LOG.error(clazz + "." + method, e);
			throw e;
		} catch (MessageException e) {
			AuthThreadLocal.getRequest().setAttribute(Constant.MESSAGE_KEY, e.getMessage());
			AuthThreadLocal.getRequest().getSession().setAttribute(Constant.MESSAGE_KEY, e.getMessage());
		} catch (RestException e) {
			throw e;
		} catch (Throwable e) {
			LOG.error(clazz + "." + method, e);
			throw new ServiceException(resources.getMessage("system.error", null, null));
		}

		watch.stop();
		String log = "token: " + AuthThreadLocal.getToken() + " >>>> " + clazz + " Run " + method + " complete in "
				+ watch.getTime() + " ms";
		LOG.info(log);
		return result;
	}

	private void checkAuth(HttpServletRequest request, HttpServletResponse response) {
		try {
			Cookie accessToken = WebUtils.getCookie(request, Constant.COOKIE_WIKIDUE_TOKEN);
			if (accessToken != null && StringUtils.isNotBlank(accessToken.getValue())) {
				if (!setLoginUser(request)) {
					try {
						Map<String, String> data = new Gson().fromJson(accessToken.getValue(), Map.class);
						SsoUserInfoResponse ssoUserInfoResponse = ssoService.getUserInfo(data.get("accessToken"));
						if (!Constant.SSO_STATUS_ERROR.equals(ssoUserInfoResponse.getHeader().getStatus())) {
							// 自動登入
							ssoService.authUser(ssoUserInfoResponse.getBody().getUserInfo(), data.get("refreshToken"),
									data.get("series"), false);
							setLoginUser(request);
						} else {
							Cookie cookie = new Cookie(Constant.COOKIE_WIKIDUE_TOKEN, StringUtils.EMPTY);
//							cookie.setDomain(configService.getDomain());
							cookie.setMaxAge(0);
							cookie.setPath("/");
							response.addCookie(cookie);
						}
					} catch (Exception e) {
						LOG.error("自動登入失敗：", e);
					}
				}

			} else {
				// 登出
				SecurityContextHolder.clearContext();
				AuthThreadLocal.setLoginUser(null);
				request.getSession().removeAttribute(Constant.USER_VO_KEY);
			}
		} finally {

		}

	}

	private boolean setLoginUser(HttpServletRequest request) {
		if (SecurityContextHolder.getContext().getAuthentication() != null
				&& SecurityContextHolder.getContext().getAuthentication().isAuthenticated()
				&& !(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken)) {
			UserAuthVo loginUser = (UserAuthVo) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			AuthThreadLocal.setLoginUser(loginUser);
			request.getSession().setAttribute(Constant.USER_VO_KEY, loginUser);
			return true;
		}
		return false;
	}
}
