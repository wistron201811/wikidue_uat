package com.e7learning.common.security;
import java.util.Collection;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

/**
* AccessdecisionManager在Spring security中是很重要的。
 *
 * 在驗證部分簡略提過了，所有的Authentication實現需要保存在一個GrantedAuthority對像數組中。這就是賦予給主體的權限。
 * GrantedAuthority對象通過AuthenticationManager 保存到
 * Authentication對象裡，然後從AccessDecisionManager讀出來，進行授權判斷。
 *
 * Spring Security提供了一些攔截器，來控制對安全對象的訪問權限，例如方法調用或web請求。
 * 一個是否允許執行調用的預調用決定，是由AccessDecisionManager實現的。這個 AccessDecisionManager
 * 被AbstractSecurityInterceptor調用， 它用來作最終訪問控制的決定。
 * 這個AccessDecisionManager接口包含三個方法：
 *
 * void decide(Authentication authentication, Object secureObject,
 * List<ConfigAttributeDefinition> config) throws AccessDeniedException; boolean
 * supports(ConfigAttribute attribute); boolean supports(Class clazz);
 *
 * 從第一個方法可以看出來，AccessDecisionManager使用方法參數傳遞所有信息，這好像在認證評估時進行決定。
 * 特別是，在真實的安全方法期望調用的時候，傳遞安全Object啟用那些參數。比如，讓我們假設安全對像是一個MethodInvocation。
 * 很容易為任何Customer參數查詢MethodInvocation，
 * 然後在AccessDecisionManager裡實現一些有序的安全邏輯，來確認主體是否允許在那個客戶上操作。
 * 如果訪問被拒絕，實現將拋出一個AccessDeniedException異常。
 *
 * 這個 supports(ConfigAttribute) 方法在啟動的時候被
 * AbstractSecurityInterceptor調用，來決定AccessDecisionManager
 * 是否可以執行傳遞ConfigAttribute。 supports(Class)方法被安全攔截器實現調用，
 * 包含安全攔截器將顯示的AccessDecisionManager支持安全對象的類型。
 */
public class MyAccessDecisionManager implements AccessDecisionManager {
	
	private static final Logger log = Logger.getLogger(MyAccessDecisionManager.class);

    public void decide(Authentication authentication, Object object,
            Collection<ConfigAttribute> configAttributes)
            throws AccessDeniedException, InsufficientAuthenticationException {

        if (configAttributes == null) {
            return;
        }

        Iterator<ConfigAttribute> ite = configAttributes.iterator();

        while (ite.hasNext()) {

            ConfigAttribute ca = ite.next();
            String needRole = ((SecurityConfig) ca).getAttribute();

//            log.info("authentication size " + authentication.getAuthorities().size()+", needRole:"+needRole);
            // ga 为用户所被赋予的权限。 needRole 为访问相应的资源应该具有的权限。
            for (GrantedAuthority ga : authentication.getAuthorities()) {

                if (needRole.trim().equals(ga.getAuthority().trim())) {

                    return;
                }

            }

        }

        throw new AccessDeniedException("");

    }

    public boolean supports(ConfigAttribute attribute) {

        return true;

    }

    public boolean supports(Class<?> clazz) {
        return true;

    }

}
