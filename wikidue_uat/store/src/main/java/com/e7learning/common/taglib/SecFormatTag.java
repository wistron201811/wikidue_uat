package com.e7learning.common.taglib;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.log4j.Logger;

public class SecFormatTag extends SimpleTagSupport {

	/***/
	private static final Logger LOG = Logger.getLogger(SecFormatTag.class);

	private static final DateFormat df = new SimpleDateFormat("HH:mm:ss");

	private Integer sec;

	@Override
	public void doTag() {
		try {

			JspWriter out = getJspContext().getOut();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(new Date(0));
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.SECOND, sec);
			out.print(df.format(calendar.getTime()));
		} catch (Exception e) {
			LOG.error("Exception:", e);
		}
	}

	public void setSec(Integer sec) {
		this.sec = sec;
	}

}
