package com.e7learning.common.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;

public class MyInvocationSecurityMetadataSourceService implements FilterInvocationSecurityMetadataSource {

	private static final Logger log = Logger.getLogger(MyInvocationSecurityMetadataSourceService.class);

	@Resource(name = "resourceMap")
	private Map<String, Collection<ConfigAttribute>> resourceMap = null;

	/**
	 * 取得所有權限對應
	 */
	public void loadResourceDefine() {

		log.info(resourceMap);
		// for(String key:resourceMap.keySet()) {
		// log.info(key+">>");
		// resourceMap.get(key).stream().forEach(v-> System.out.println(v.getClass()));
		// }

	}

	@Override
	public Collection<ConfigAttribute> getAllConfigAttributes() {

		return null;
	}

	// 根据URL，找到相关的权限配置。
	@Override
	public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {

		// object 是一个URL，被用户请求的url。
		String url = ((FilterInvocation) object).getRequestUrl();
		log.info("url" + url);
		int firstQuestionMarkIndex = url.indexOf("?");

		if (firstQuestionMarkIndex != -1) {
			url = url.substring(0, firstQuestionMarkIndex);
		}

		if (resourceMap.containsKey(url)) {
			return resourceMap.get(url);
		} else if (resourceMap.containsKey(url.substring(0, url.lastIndexOf("/")) + "/*")) {
			return resourceMap.get(url.substring(0, url.lastIndexOf("/")) + "/*");
		}

		/*Iterator<String> ite = resourceMap.keySet().iterator();

		while (ite.hasNext()) {
			String resURL = ite.next();

			if (url.equals(resURL)) {

				return resourceMap.get(resURL);
			}
		}*/

		return null;
	}
/*
	public boolean isAuthorizeByUri(String uri) {
		Iterator<String> ite = resourceMap.keySet().iterator();
		List<String> roles = null;
		while (ite.hasNext()) {
			String resURL = ite.next();
			if (resURL.equals(uri)) {
				roles = new ArrayList<String>();
				for (ConfigAttribute configAttribute : resourceMap.get(resURL)) {
					roles.add(configAttribute.getAttribute());
				}
			}
		}
		if (roles != null) {
			for (GrantedAuthority grantedAuthority : SecurityContextHolder.getContext().getAuthentication()
					.getAuthorities()) {
				if (roles.contains(grantedAuthority.getAuthority())) {
					return true;
				}
			}
		}
		return false;

	}
	*/

	@Override
	public boolean supports(Class<?> arg0) {

		return true;
	}

}