package com.e7learning.common.security;

import org.apache.log4j.Logger;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

/**
 * @author ChrisTsai
 *
 */
@Service
public class MyUserDetailsService implements UserDetailsService {

	private static final Logger log = Logger.getLogger(MyUserDetailsService.class);

	@Override
	public UserDetails loadUserByUsername(String username){
		log.info("username:" + username);
		return null;

	}

}
