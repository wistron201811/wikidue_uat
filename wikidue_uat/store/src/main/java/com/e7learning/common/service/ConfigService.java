package com.e7learning.common.service;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 讀取config檔
 * 
 * @author ChrisTsai
 *
 */
@Component
public class ConfigService{
	
	@Value("${page.size}")
	private Integer pageSize;
	
	@Value("${sso.login.page}")
	private String ssoLoginPage;
	
	@Value("${sso.sign.up.page}")
	private String ssoSignUpPage;
	
	@Value("${sso.token.page}")
	private String ssoTokenPage;
	
	@Value("${sso.clientId}")
	private String ssoClientId;
	
	@Value("${sso.login.responseType}")
	private String ssoLoginResponseType;
	
	@Value("${sso.login.redirectUri}")
	private String ssoLoginRedirectUri;
	
	@Value("${sso.clientSecret}")
	private String clientSecret;
	
	@Value("${sso.token.grantType}")
	private String ssoTokenGrantType;
	
	@Value("${host}")
	private String host;
	
	@Value("${sso.userInfo.api}")
	private String ssoUserInfoApi;
	
	@Value("${sso.logout.page}")
	private String ssoLogoutPage;
	
	@Value("${sso.logout.redirect.uri}")
	private String ssoLogoutRedirectUri;
	
	@Value("${exam.center.url}")
	private String examCenterUrl;
	
	@Value("${exam.record.url}")
	private String examRecordUrl;
	
	@Value("${member.center.url}")
	private String memberCenterUrl;
	
	
	@Value("${image.api}")
	private String imageApi;
	
	@Value("${video.token.expire.sec}")
	private Integer videoTokenExpireSec;
	
	@Value("${payment.return.url}")
	private String paymentReturnUrl;
	
	@Value("${client.back.url}")
	private String clientBackUrl;
	
	@Value("${order.result.url}")
	private String orderResultUrl;
	
	@Value("${max.upload.size}")
	private Long maxUploadSize;
	
	@Value("${max.single.file.size}")
	private Long maxSingleUploadSize;
	
	@Value("${image.max.upload.size}")
	private Long imageMaxUploadSize;
	
	
	@Value("${payment.due.date}")
	private Integer paymentDueDate;
	
	
	@Value("${application.version}")
	private String applicationVersion;
	
	@Value("${fb.app.id}")
	private String fbAppId;
			
	@Value("${trade.desc}")
	private String tradeDesc;
	
	@Value("${skip.invoice}")
	private Boolean skipInvoice;

	@Value("${image.url}")
	private String imageUrl;

	@Value("${backend.url}")
	private String backendUrl;
	
	@Value("${myorder.url}")
	private String myOrderUrl;
	
	@Value("${email.sender}")
	private String emailSender;
	
	@Value("${service.url}")
	private String serviceUrl;

	@Value("${backend.manual.url}")
	private String backendManualUrl;
	
	public Integer getPageSize() {
		return pageSize;
	}

	public String getSsoLoginPage() {
		return ssoLoginPage;
	}


	public String getSsoClientId() {
		return ssoClientId;
	}


	public String getSsoLoginResponseType() {
		return ssoLoginResponseType;
	}


	public String getSsoLoginRedirectUri() {
		return ssoLoginRedirectUri;
	}


	public String getClientSecret() {
		return clientSecret;
	}


	public String getSsoTokenGrantType() {
		return ssoTokenGrantType;
	}


	public String getHost() {
		return host;
	}


	public String getSsoTokenPage() {
		return ssoTokenPage;
	}


	public String getSsoUserInfoApi() {
		return ssoUserInfoApi;
	}


	public String getSsoLogoutPage() {
		return ssoLogoutPage;
	}


	public String getSsoLogoutRedirectUri() {
		return ssoLogoutRedirectUri;
	}


	public String getImageApi() {
		return imageApi;
	}


	public String getExamCenterUrl() {
		return examCenterUrl;
	}


	public Integer getVideoTokenExpireSec() {
		return videoTokenExpireSec;
	}


	public String getPaymentReturnUrl() {
		return paymentReturnUrl;
	}


	public String getClientBackUrl() {
		return clientBackUrl;
	}


	public String getOrderResultUrl() {
		return orderResultUrl;
	}


	public Long getMaxUploadSize() {
		return maxUploadSize;
	}


	public Integer getPaymentDueDate() {
		return paymentDueDate;
	}


	public String getApplicationVersion() {
		return applicationVersion;
	}


	public String getFbAppId() {
		return fbAppId;
	}


	public String getExamRecordUrl() {
		return examRecordUrl;
	}


	public String getSsoSignUpPage() {
		return ssoSignUpPage;
	}
	
	public String getDomain() {
		URI uri;
		try {
			uri = new URI(host);
			return uri.getHost();
		} catch (URISyntaxException e) {
			
		}
		return null;
	}

	public Long getMaxSingleUploadSize() {
		return maxSingleUploadSize;
	}

	public Long getImageMaxUploadSize() {
		return imageMaxUploadSize;
	}

	public Boolean getSkipInvoice() {
		return skipInvoice;
	}

	public String getTradeDesc() {
		return tradeDesc;
	}

	public String getMemberCenterUrl() {
		return memberCenterUrl;
	}

	public String getImageUrl() {
		return host+imageUrl;
	}

	public String getBackendUrl() {
		return backendUrl;
	}

	public String getMyOrderUrl() {
		return host+myOrderUrl;
	}

	public String getEmailSender() {
		return emailSender;
	}

	public String getServiceUrl() {
		return serviceUrl;
	}

	public String getBackendManualUrl() {
		return backendManualUrl;
	}
	
}
