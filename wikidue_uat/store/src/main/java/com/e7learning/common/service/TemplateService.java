package com.e7learning.common.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import freemarker.template.Configuration;

@Service
public class TemplateService {

	private static final Logger log = Logger.getLogger(TemplateService.class);

	@Autowired
	private Configuration freemarkerConfiguration;

	public String getSellNotification(String storeUrl, String imageUrl, String sendDate, String vendorName,
			String buyerName, String orderDate, String backendUrl, List<Map<String, String>> details, String total,
			String recipient, String telPhone, String mobile, String address, String orderNo, String backendManualUrl,
			String serviceUrl, boolean mail) {

		StringBuilder content = new StringBuilder();
		Map<String, Object> map = new HashMap<>();
		map.put("storeUrl", storeUrl);
		map.put("imageUrl", imageUrl);
		map.put("sendDate", sendDate);
		map.put("vendorName", vendorName);
		map.put("buyerName", buyerName);
		map.put("orderDate", orderDate);
		map.put("backendUrl", backendUrl);
		map.put("details", details);
		map.put("total", total);
		map.put("recipient", recipient);
		map.put("telPhone", telPhone);
		map.put("mobile", mobile);
		map.put("address", address);
		map.put("orderNo", orderNo);
		map.put("backendManualUrl", backendManualUrl);
		map.put("serviceUrl", serviceUrl);
		map.put("mail", mail);
		try {
			content.append(FreeMarkerTemplateUtils
					.processTemplateIntoString(freemarkerConfiguration.getTemplate("mail_02.html", "UTF-8"), map));
			return content.toString();
		} catch (Exception e) {
			log.error("Exception occured while processing fmtemplate:" + e.getMessage());
			return StringUtils.EMPTY;
		}
	}

	public String getShopSuccessNotification(String storeUrl, String imageUrl, String sendDate, String buyerName,
			String myOrderUrl, String orderNo, List<Map<String, String>> details, String total, String recipient,
			String telPhone, String mobile, String address, String serviceUrl, boolean mail) {
		Map<String, Object> map = new HashMap<>();
		map.put("storeUrl", storeUrl);
		map.put("imageUrl", imageUrl);
		map.put("sendDate", sendDate);
		map.put("buyerName", buyerName);
		map.put("myOrderUrl", myOrderUrl);
		map.put("orderNo", orderNo);
		map.put("details", details);
		map.put("total", total);
		map.put("recipient", recipient);
		map.put("telPhone", telPhone);
		map.put("mobile", mobile);
		map.put("address", address);
		map.put("serviceUrl", serviceUrl);
		map.put("mail", mail);
		try {
			StringBuilder content = new StringBuilder();

			content.append(FreeMarkerTemplateUtils
					.processTemplateIntoString(freemarkerConfiguration.getTemplate("mail_01.html", "UTF-8"), map));
			return content.toString();
		} catch (Exception e) {
			log.error("Exception occured while processing fmtemplate:" + e.getMessage());
			return StringUtils.EMPTY;
		}
	}

	/**
	 * 取消訂單通知信-買家
	 * 
	 * @return
	 */
	public String getCancelOrderNotificationBuyer(String storeUrl, String sendDate, String buyerName, String imageUrl,
			String cancelDate, String orderNo, List<Map<String, String>> details, String total, String myOrderUrl,
			String serviceUrl) {
		try {
			StringBuilder content = new StringBuilder();
			Map<String, Object> map = new HashMap<>();
			map.put("storeUrl", storeUrl);
			map.put("sendDate", sendDate);
			map.put("buyerName", buyerName);
			map.put("imageUrl", imageUrl);
			map.put("cancelDate", cancelDate);
			map.put("orderNo", orderNo);
			map.put("details", details);
			map.put("total", total);
			map.put("myOrderUrl", myOrderUrl);
			map.put("serviceUrl", serviceUrl);

			content.append(FreeMarkerTemplateUtils
					.processTemplateIntoString(freemarkerConfiguration.getTemplate("mail_41.html", "UTF-8"), map));
			return content.toString();
		} catch (Exception e) {
			log.error("Exception occured while processing fmtemplate:" + e.getMessage());
			return StringUtils.EMPTY;
		}
	}

	/**
	 * 取消訂單通知信-廠商
	 * 
	 * @return
	 */
	public String getCancelOrderNotificationVendor(String storeUrl, String imageUrl, String sendDate, String vendorName,
			String orderNo, String cancelDate, String backendUrl, List<Map<String, String>> details, String total,
			String backendManualUrl, String serviceUrl) {
		try {
			StringBuilder content = new StringBuilder();
			Map<String, Object> map = new HashMap<>();
			map.put("storeUrl", storeUrl);
			map.put("imageUrl", imageUrl);
			map.put("sendDate", sendDate);
			map.put("vendorName", vendorName);
			map.put("orderNo", orderNo);
			map.put("cancelDate", cancelDate);
			map.put("backendUrl", backendUrl);
			map.put("details", details);
			map.put("total", total);
			map.put("backendManualUrl", backendManualUrl);
			map.put("serviceUrl", serviceUrl);

			content.append(FreeMarkerTemplateUtils
					.processTemplateIntoString(freemarkerConfiguration.getTemplate("mail_42.html", "UTF-8"), map));
			return content.toString();
		} catch (Exception e) {
			log.error("Exception occured while processing fmtemplate:" + e.getMessage());
			return StringUtils.EMPTY;
		}
	}

	/**
	 * 訂單成立通知信-管理員
	 * 
	 * @return
	 */
	public String getSellNotificationAdmin(String storeUrl, String imageUrl, String sendDate, String buyerName,
			String orderDate, String backendUrl, String orderNo, String invoiceNo, String payWay, String total,
			String ratioTotal, String recipient, String telPhone, String mobile, String address,
			List<Map<String, String>> details, List<Map<String, String>> vendorList, boolean mail) {
		try {
			StringBuilder content = new StringBuilder();
			Map<String, Object> map = new HashMap<>();
			map.put("storeUrl", storeUrl);
			map.put("imageUrl", imageUrl);
			map.put("sendDate", sendDate);
			map.put("buyerName", buyerName);
			map.put("orderDate", orderDate);
			map.put("backendUrl", backendUrl);
			map.put("vendorList", vendorList);
			map.put("orderNo", orderNo);
			map.put("invoiceNo", invoiceNo);
			map.put("payWay", payWay);
			map.put("details", details);
			map.put("total", total);
			map.put("ratioTotal", ratioTotal);
			map.put("recipient", recipient);
			map.put("telPhone", telPhone);
			map.put("mobile", mobile);
			map.put("address", address);
			map.put("mail", mail);

			content.append(FreeMarkerTemplateUtils
					.processTemplateIntoString(freemarkerConfiguration.getTemplate("mail_03.html", "UTF-8"), map));
			return content.toString();
		} catch (Exception e) {
			log.error("Exception occured while processing fmtemplate:" + e.getMessage());
			return StringUtils.EMPTY;
		}
	}

	/**
	 * 分享園地檢舉通知信
	 * 
	 * @return
	 */
	public String getAccusationNotification(String storeUrl, String imageUrl, String sendDate, String member,
			String accuseDate, String accuser, String shareTitle, String accuseReason) {
		try {
			StringBuilder content = new StringBuilder();
			Map<String, Object> map = new HashMap<>();
			map.put("storeUrl", storeUrl);
			map.put("imageUrl", imageUrl);
			map.put("sendDate", sendDate);
			map.put("member", member);
			map.put("accuseDate", accuseDate);
			map.put("accuser", accuser);
			map.put("shareTitle", shareTitle);
			map.put("accuseReason", accuseReason);

			content.append(FreeMarkerTemplateUtils
					.processTemplateIntoString(freemarkerConfiguration.getTemplate("accusation.html", "UTF-8"), map));
			return content.toString();
		} catch (Exception e) {
			log.error("Exception occured while processing fmtemplate:" + e.getMessage());
			return StringUtils.EMPTY;
		}
	}
}
