package com.e7learning.course.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.e7learning.tag.vo.TagVo;
import com.e7learning.teacher.vo.TeacherVo;

public class CourseVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String productId;

	private Integer courseId;

	private String courseExpectation;

	private String courseIntroduction;

	private String courseName;

	private String coursePreparation;

	private String courseSummary;

	private String courseTarget;

	private Integer discountPrice;

	private Integer price;

	private Date publishEndDate;

	private Date publishStartDate;

	private String imageOneId;

	private String imageTwoId;

	private TeacherVo teacher;

	private List<CourseChapterVo> courseChapterList;

	private String materialNum;

	private Integer costPrice;

	private String videoUrl;

	private Integer studentAmount;

	private List<CourseFileVo> courseFileList;

	private List<TagVo> tags;

	private Set<String> tagNameList;

	private Set<String> categoryNameList;

	private boolean ownCourse = false;

	public Integer getCourseId() {
		return courseId;
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}

	public String getCourseExpectation() {
		return courseExpectation;
	}

	public void setCourseExpectation(String courseExpectation) {
		this.courseExpectation = courseExpectation;
	}

	public String getCourseIntroduction() {
		return courseIntroduction;
	}

	public void setCourseIntroduction(String courseIntroduction) {
		this.courseIntroduction = courseIntroduction;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCoursePreparation() {
		return coursePreparation;
	}

	public void setCoursePreparation(String coursePreparation) {
		this.coursePreparation = coursePreparation;
	}

	public String getCourseSummary() {
		return courseSummary;
	}

	public void setCourseSummary(String courseSummary) {
		this.courseSummary = courseSummary;
	}

	public String getCourseTarget() {
		return courseTarget;
	}

	public void setCourseTarget(String courseTarget) {
		this.courseTarget = courseTarget;
	}

	public Integer getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Integer discountPrice) {
		this.discountPrice = discountPrice;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Date getPublishEndDate() {
		return publishEndDate;
	}

	public void setPublishEndDate(Date publishEndDate) {
		this.publishEndDate = publishEndDate;
	}

	public Date getPublishStartDate() {
		return publishStartDate;
	}

	public void setPublishStartDate(Date publishStartDate) {
		this.publishStartDate = publishStartDate;
	}

	public String getImageOneId() {
		return imageOneId;
	}

	public void setImageOneId(String imageOneId) {
		this.imageOneId = imageOneId;
	}

	public String getImageTwoId() {
		return imageTwoId;
	}

	public void setImageTwoId(String imageTwoId) {
		this.imageTwoId = imageTwoId;
	}

	public TeacherVo getTeacher() {
		return teacher;
	}

	public void setTeacher(TeacherVo teacher) {
		this.teacher = teacher;
	}

	public List<CourseChapterVo> getCourseChapterList() {
		return courseChapterList;
	}

	public void setCourseChapterList(List<CourseChapterVo> courseChapterList) {
		this.courseChapterList = courseChapterList;
	}

	public String getMaterialNum() {
		return materialNum;
	}

	public void setMaterialNum(String materialNum) {
		this.materialNum = materialNum;
	}

	public Integer getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(Integer costPrice) {
		this.costPrice = costPrice;
	}

	public String getVideoUrl() {
		return videoUrl;
	}

	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}

	public List<CourseFileVo> getCourseFileList() {
		return courseFileList;
	}

	public void setCourseFileList(List<CourseFileVo> courseFileList) {
		this.courseFileList = courseFileList;
	}

	public List<TagVo> getTags() {
		return tags;
	}

	public void setTags(List<TagVo> tags) {
		this.tags = tags;
	}

	public Set<String> getTagNameList() {
		return tagNameList;
	}

	public void setTagNameList(Set<String> tagNameList) {
		this.tagNameList = tagNameList;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public Set<String> getCategoryNameList() {
		return categoryNameList;
	}

	public void setCategoryNameList(Set<String> categoryNameList) {
		this.categoryNameList = categoryNameList;
	}

	public Integer getStudentAmount() {
		return studentAmount;
	}

	public void setStudentAmount(Integer studentAmount) {
		this.studentAmount = studentAmount;
	}

	public boolean isOwnCourse() {
		return ownCourse;
	}

	public void setOwnCourse(boolean ownCourse) {
		this.ownCourse = ownCourse;
	}

	public Integer getProgress() {
		if (courseChapterList != null) {
			return Math.round((Float
					.valueOf(courseChapterList.stream().mapToInt(v -> v.getProgress())
							.reduce((total, progress) -> total + progress).getAsInt())
					/ Float.valueOf(courseChapterList.size())));
		}
		return 0;
	}
}
