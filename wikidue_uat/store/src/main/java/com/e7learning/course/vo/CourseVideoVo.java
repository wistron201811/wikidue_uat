package com.e7learning.course.vo;

import java.io.Serializable;
import java.util.Date;

import com.e7learning.video.vo.VideoVo;

/**
 * @author Clarence 課程影片
 */
public class CourseVideoVo implements Serializable, Comparable<CourseVideoVo> {
	private static final long serialVersionUID = 1L;

	/**
	 * 課程影片編號
	 */
	private Integer courseVideoId;

	/**
	 * 課程影片名稱
	 */
	private String courseVideoName;

	/**
	 * 課程影片順序
	 */
	private Integer courseVideoNo;

	/**
	 * 平台影片外來鍵
	 */
	private VideoVo video;

	/**
	 * 影片長度(秒)
	 */
	private Integer videoDuration;

	/**
	 * 已播進度(秒)
	 */
	private Integer learningDuration;
	
	/**
	 * 最後觀看時間
	 */
	private Date lastWatchDt;

	/** 是否免費 */
	private boolean free;

	public Integer getCourseVideoId() {
		return courseVideoId;
	}

	public void setCourseVideoId(Integer courseVideoId) {
		this.courseVideoId = courseVideoId;
	}

	public String getCourseVideoName() {
		return courseVideoName;
	}

	public void setCourseVideoName(String courseVideoName) {
		this.courseVideoName = courseVideoName;
	}

	public Integer getCourseVideoNo() {
		return courseVideoNo;
	}

	public void setCourseVideoNo(Integer courseVideoNo) {
		this.courseVideoNo = courseVideoNo;
	}

	public VideoVo getVideo() {
		return video;
	}

	public void setVideo(VideoVo video) {
		this.video = video;
	}

	public Integer getVideoDuration() {
		return videoDuration;
	}

	public void setVideoDuration(Integer videoDuration) {
		this.videoDuration = videoDuration;
	}

	public Integer getLearningDuration() {
		return learningDuration;
	}

	public void setLearningDuration(Integer learningDuration) {
		this.learningDuration = learningDuration;
	}

	public boolean isFree() {
		return free;
	}

	public void setFree(boolean free) {
		this.free = free;
	}
	
	public Date getLastWatchDt() {
		return lastWatchDt;
	}

	public void setLastWatchDt(Date lastWatchDt) {
		this.lastWatchDt = lastWatchDt;
	}

	public boolean isComplete() {
		// 播放長度超過總長度一半就算完成
		if (videoDuration != null && learningDuration != null && (Double.valueOf(learningDuration) / Double.valueOf(videoDuration) * 100 >= 50)) {
			return true;
		}

		return false;
	}

	@Override
	public int compareTo(CourseVideoVo o) {
		if (this.getCourseVideoNo() != null && o.getCourseVideoNo() != null) {
			return this.getCourseVideoNo() - o.getCourseVideoNo();
		}
		return 0;
	}

}
