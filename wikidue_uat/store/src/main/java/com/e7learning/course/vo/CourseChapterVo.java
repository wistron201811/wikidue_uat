package com.e7learning.course.vo;

import java.io.Serializable;
import java.util.List;

/**
 * @author Clarence 課程章節
 */
public class CourseChapterVo implements Serializable, Comparable<CourseChapterVo> {
	private static final long serialVersionUID = 1L;

	/**
	 * 課程章節編號
	 */
	private Integer courseChapterId;

	/**
	 * 課程章節名稱
	 */
	private String courseChapterName;

	/**
	 * 課程章節順序
	 */
	private Integer courseChapterNo;

	private List<CourseVideoVo> courseVideoList;

	public String getCourseChapterName() {
		return courseChapterName;
	}

	public void setCourseChapterName(String courseChapterName) {
		this.courseChapterName = courseChapterName;
	}

	public Integer getCourseChapterNo() {
		return courseChapterNo;
	}

	public void setCourseChapterNo(Integer courseChapterNo) {
		this.courseChapterNo = courseChapterNo;
	}

	public List<CourseVideoVo> getCourseVideoList() {
		return courseVideoList;
	}

	public void setCourseVideoList(List<CourseVideoVo> courseVideoList) {
		this.courseVideoList = courseVideoList;
	}

	public Integer getCourseChapterId() {
		return courseChapterId;
	}

	public void setCourseChapterId(Integer courseChapterId) {
		this.courseChapterId = courseChapterId;
	}

	public Integer getProgress() {
		if (courseVideoList != null) {
			return Math.round((Float.valueOf(courseVideoList.stream().filter(v -> v.isComplete()).count())
					/ Float.valueOf(courseVideoList.size()) * 100));
		}
		return 0;
	}

	@Override
	public int compareTo(CourseChapterVo o) {
		if (this.courseChapterNo != null && o.getCourseChapterNo() != null) {
			return this.courseChapterNo - o.getCourseChapterNo();
		}
		return 0;
	}

}
