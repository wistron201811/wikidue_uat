package com.e7learning.course.vo;

import java.io.Serializable;

/**
 * @author Clarence 課程檔案
 *
 */
public class CourseFileVo implements Serializable {
	private static final long serialVersionUID = 1L;

	private String courseFileId;

	private String fileName;

	private String fileId;
	
	private String durationText;

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getCourseFileId() {
		return courseFileId;
	}

	public void setCourseFileId(String courseFileId) {
		this.courseFileId = courseFileId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getDurationText() {
		return durationText;
	}

	public void setDurationText(String durationText) {
		this.durationText = durationText;
	}

}
