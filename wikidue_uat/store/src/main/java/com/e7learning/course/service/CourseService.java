package com.e7learning.course.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.common.Constant;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.FileStorageService;
import com.e7learning.course.vo.CourseChapterVo;
import com.e7learning.course.vo.CourseFileVo;
import com.e7learning.course.vo.CourseVideoVo;
import com.e7learning.course.vo.CourseVo;
import com.e7learning.repository.CourseRepository;
import com.e7learning.repository.KnowledgeMapRepository;
import com.e7learning.repository.LearningProgressRepository;
import com.e7learning.repository.ProductRepository;
import com.e7learning.repository.TagRepository;
import com.e7learning.repository.UserCourseRepository;
import com.e7learning.repository.VideoTokenRepository;
import com.e7learning.repository.model.Course;
import com.e7learning.repository.model.CourseChapter;
import com.e7learning.repository.model.CourseFile;
import com.e7learning.repository.model.CourseVideo;
import com.e7learning.repository.model.FileStorage;
import com.e7learning.repository.model.KnowledgeMap;
import com.e7learning.repository.model.LearningProgress;
import com.e7learning.repository.model.Product;
import com.e7learning.repository.model.Tag;
import com.e7learning.repository.model.Teacher;
import com.e7learning.repository.model.UserCourse;
import com.e7learning.repository.model.Video;
import com.e7learning.repository.model.VideoToken;
import com.e7learning.tag.vo.TagVo;
import com.e7learning.teacher.vo.TeacherVo;
import com.e7learning.video.vo.VideoVo;

@Service
public class CourseService extends BaseService {

	private static final Logger LOG = Logger.getLogger(CourseService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private FileStorageService fileStorageService;

	@Autowired
	private CourseRepository courseRepository;

	@Autowired
	private KnowledgeMapRepository knowledgeMapRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private UserCourseRepository userCourseRepository;

	@Autowired
	private LearningProgressRepository learningProgressRepository;

	@Autowired
	private VideoTokenRepository videoTokenRepository;

	@Autowired
	private TagRepository tagRepository;

	@Transactional(readOnly = true)
	public TagVo getCourseTag(Integer tagId) {
		TagVo tagVo = null;
		try {
			if (tagId != null && tagRepository.exists(tagId)) {
				Tag tag = tagRepository.findOne(tagId);
				tagVo = new TagVo();
				tagVo.setId(tag.getPkTag());
				tagVo.setTag(tag.getTag());
				tagVo.setTagLevel(tag.getTagLevel());
				tagVo.setTagName(tag.getTagName());
			}
		} catch (Exception e) {
			LOG.error("getCourseTag:", e);
		}
		return tagVo;
	}

	@Transactional(readOnly = true)
	public List<Course> queryIndexCourseList() {
		try {
			Date now = new Date();
			return courseRepository
					.findTop4ByPublishSDateLessThanEqualAndPublishEDateGreaterThanEqualAndActiveOrderByPublishSDateDesc(
							now, new LocalDate().toDate(), Constant.TRUE);
		} catch (Exception e) {
			LOG.error("exception:", e);
		}
		return new ArrayList<>();
	}

	@Transactional(readOnly = true)
	public Course findByProductPk(String productPk) {
		try {
			Product product = productRepository.findOne(productPk);
			if (product == null) {
				return null;
			}
			return product.getCourse();
		} catch (Exception e) {
			LOG.error("exception:", e);
		}
		return null;
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public CourseVo findAvailableByCourseId(Integer courseId) {
		try {
			Course course = courseRepository.findOne(courseId);
			if (course == null) {
				return null;
			}

			course.getCourseChapterList().size();
			course.getCourseFileList().size();
			course.getTags().size();

			CourseVo courseVo = generateVo(null, course);
			if (courseVo.isOwnCourse()) {
				// 登入且有購買課程
				return addProgess(courseVo);
			}
			return courseVo;
		} catch (Exception e) {
			LOG.error("exception:", e);
		}
		return null;
	}

	/**
	 * 用料號查課程
	 * 
	 * @param materialNum
	 * @return
	 */
	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public CourseVo findAvailableByMaterialNum(String materialNum) {
		if (StringUtils.isBlank(materialNum)) {
			return null;
		}
		List<Product> products = productRepository
				.findAll((Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
					List<Predicate> predicates = new ArrayList<>();
					query.distinct(true);
					predicates.add(cb.isNotNull(root.get("course")));
					predicates.add(cb.equal(root.get("course").get("materialNum"), materialNum));
					// 已上架
					predicates.add(cb.equal(root.get("active"), Constant.TRUE));
					predicates.add(cb.lessThanOrEqualTo(root.get("publishSDate"), new Date()));
					predicates.add(cb.greaterThanOrEqualTo(root.get("publishEDate"), new Date()));
					return cb.and(predicates.toArray(new Predicate[predicates.size()]));
				});
		CourseVo courseVo = null;
		if (!products.isEmpty()) {
			courseVo = findAvailableByProductId(products.get(0).getPkProduct());
			if (courseVo != null)
				courseVo.setProductId(products.get(0).getPkProduct());
		}
		return courseVo;
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public CourseVo findAvailableByProductId(String productPk) {
		try {
			Product product = productRepository.findOne(productPk);
			if (product == null) {
				return null;
			}
			Course course = product.getCourse();
			if (course == null) {
				return null;
			}

			course.getCourseChapterList().size();
			course.getCourseFileList().size();
			course.getTags().size();

			return generateVo(product, course);
		} catch (Exception e) {
			LOG.error("exception:", e);
		}
		return null;
	}

	public CourseVo generateVo(Product product, Course course) {
		CourseVo courseVo = new CourseVo();

		// 檢查是否有購買
		if (getCurrentUser() != null) {
			boolean ownCourse = (int) userCourseRepository
					.count((Root<UserCourse> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
						return cb.and(cb.equal(root.get("course").get("pkCourse"), course.getPkCourse()),
								cb.equal(root.get("userId"), getCurrentUser().getLoginId()));
					}) > 0;
			courseVo.setOwnCourse(ownCourse);
		}

		courseVo.setCourseId(course.getPkCourse());
		courseVo.setCostPrice(course.getCostPrice());
		courseVo.setCourseExpectation(course.getCourseExpectation());
		courseVo.setCourseId(course.getPkCourse());
		courseVo.setCourseIntroduction(course.getCourseIntroduction());
		courseVo.setCourseName(course.getCourseName());
		courseVo.setCoursePreparation(course.getCoursePreparation());
		courseVo.setCourseSummary(course.getCourseSummary());
		courseVo.setCourseTarget(course.getCourseTarget());
		courseVo.setDiscountPrice(product != null ? product.getDiscountPrice() : course.getDiscountPrice());
		courseVo.setImageOneId(course.getImageOne());
		courseVo.setImageTwoId(course.getImageTwo());
		courseVo.setMaterialNum(course.getMaterialNum());
		courseVo.setPrice(product != null ? product.getPrice() : course.getPrice());
		courseVo.setPublishEndDate(course.getPublishEDate());
		courseVo.setPublishStartDate(course.getPublishSDate());
		courseVo.setVideoUrl(course.getVideoUrl());

		if (course.getTeacher() != null) {
			Teacher teacher = course.getTeacher();
			TeacherVo teacherVo = new TeacherVo();
			teacherVo.setEducation(teacher.getEducation());
			teacherVo.setEmail(teacher.getEmail());
			teacherVo.setExperience(teacher.getExperience());
			teacherVo.setJob(teacher.getJob());
			teacherVo.setNickname(teacher.getNickname());
			teacherVo.setPhone(teacher.getPhone());
			teacherVo.setPhotoId(teacher.getPhotoId());
			teacherVo.setSkill(teacher.getSkill());
			teacherVo.setTeacherId(teacher.getPkTeacher());
			teacherVo.setTeacherIntroduction(teacher.getTeacherIntroduction());
			teacherVo.setTeacherName(teacher.getTeacherName());
			courseVo.setTeacher(teacherVo);
		}
		if (course.getCourseFileList() != null && !course.getCourseFileList().isEmpty()) {
			List<CourseFileVo> courseFileList = new ArrayList<>();
			for (CourseFile courseFile : course.getCourseFileList()) {
				CourseFileVo courseFileVo = new CourseFileVo();
				FileStorage fileStorage = fileStorageService.getFileStorageById(courseFile.getFileId());
				courseFileVo.setCourseFileId(courseFile.getPkCourseFile());
				courseFileVo.setFileId(courseFile.getFileId());
				courseFileVo.setFileName(fileStorage.getFileName());
				courseFileList.add(courseFileVo);
			}
			courseVo.setCourseFileList(courseFileList);
		}
		if (course.getCourseChapterList() != null && !course.getCourseChapterList().isEmpty()) {
			List<CourseChapterVo> courseChapterList = new ArrayList<>();
			for (CourseChapter courseChapter : course.getCourseChapterList()) {
				CourseChapterVo courseChapterVo = new CourseChapterVo();
				courseChapterVo.setCourseChapterName(courseChapter.getCourseChapterName());
				courseChapterVo.setCourseChapterNo(courseChapter.getCourseChapterNo());
				courseChapterVo.setCourseChapterId(courseChapter.getPkCourseChapter());
				if (courseChapter.getCourseVideoList() != null && !courseChapter.getCourseVideoList().isEmpty()) {
					List<CourseVideoVo> courseVideoList = new ArrayList<>();
					for (CourseVideo courseVideo : courseChapter.getCourseVideoList()) {
						CourseVideoVo courseVideoVo = new CourseVideoVo();
						courseVideoVo.setCourseVideoId(courseVideo.getPkCourseVideo());
						courseVideoVo.setCourseVideoName(courseVideo.getCourseVideoName());
						courseVideoVo.setCourseVideoNo(courseVideo.getCourseVideoNo());
						courseVideoVo.setFree(Constant.TRUE.equals(courseVideo.getIsFree()));
						Video video = courseVideo.getVideo();
						if (video != null) {
							VideoVo videoVo = new VideoVo();
							videoVo.setVideoId(video.getPkVideo());
							videoVo.setHbID(video.getThirdId());
							if (courseVideoVo.isFree() && !courseVo.isOwnCourse()) {
								// 免費影片，要先把token產好
								try {
									videoVo.setVideoStreamUrl(
											video.getPlayerStreamUrl() + URLEncoder.encode(
													"&token=" + getVideoToken(video.getThirdId()) + "&uid="
															+ Constant.STYSTEM + "&hbID=" + video.getThirdId(),
													"UTF-8"));
								} catch (UnsupportedEncodingException e) {
									LOG.error("setVideoStreamUrl", e);
								}
							} else
								videoVo.setVideoStreamUrl(video.getPlayerStreamUrl());
							videoVo.setImg(video.getImg());
							videoVo.setDuration(video.getDuration());
							courseVideoVo.setVideo(videoVo);
						}
						courseVideoList.add(courseVideoVo);
					}
					Collections.sort(courseVideoList);
					courseChapterVo.setCourseVideoList(courseVideoList);
				}
				courseChapterList.add(courseChapterVo);
			}
			Collections.sort(courseChapterList);
			courseVo.setCourseChapterList(courseChapterList);
		}

		if (course.getTags() != null && !course.getTags().isEmpty()) {
			List<TagVo> tagList = new ArrayList<>();
			Set<String> tagNameList = new HashSet<>();
			Set<String> categoryNameList = new HashSet<>();
			for (Tag tag : course.getTags()) {
				TagVo tagVo = new TagVo();
				tagVo.setId(tag.getPkTag());
				tagVo.setTag(tag.getTag());
				tagVo.setTagLevel(tag.getTagLevel());
				tagVo.setTagName(tag.getTagName());
				tagList.add(tagVo);

				String[] tags = StringUtils.splitPreserveAllTokens(tag.getTag(), "-");
				if (tags != null && tags.length > 0) {
					StringBuilder tagValue = new StringBuilder();
					StringBuilder tagName = new StringBuilder();
					int i = 0;
					for (String tagCode : tags) {
						if (!StringUtils.isEmpty(tagValue.toString())) {
							tagValue.append("-");
						}
						tagValue.append(tagCode);
						KnowledgeMap knowledgeMap = knowledgeMapRepository.findOne(tagValue.toString());
						if (knowledgeMap != null) {
							if (i > 0) {
								tagName.append("/");
							}
							tagName.append(knowledgeMap.getTagName());
						}
						i++;
					}
					tagNameList.add(tagName.toString());
				}
				if (tag.getCategory() != null && Constant.TRUE.equals(tag.getCategory().getActive())) {
					categoryNameList.add(tag.getCategory().getCategoryName());
				}
			}
			courseVo.setTags(tagList);
			courseVo.setTagNameList(tagNameList);
			courseVo.setCategoryNameList(categoryNameList);
		}

		int studentAmount = (int) userCourseRepository
				.count((Root<UserCourse> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
					return cb.equal(root.get("course").get("pkCourse"), course.getPkCourse());
				});
		courseVo.setStudentAmount(studentAmount);

		return courseVo;
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public CourseVo addProgess(CourseVo courseVo) {
		if (CollectionUtils.isNotEmpty(courseVo.getCourseChapterList())) {
			for (CourseChapterVo courseChapterVo : courseVo.getCourseChapterList()) {
				if (CollectionUtils.isNotEmpty(courseChapterVo.getCourseVideoList())) {
					for (CourseVideoVo courseVideoVo : courseChapterVo.getCourseVideoList()) {
						VideoVo video = courseVideoVo.getVideo();
						if (video != null) {
							// 先查是否有播放紀錄
							List<LearningProgress> result = learningProgressRepository
									.findByFkCourseVideoAndUserId(courseVideoVo.getCourseVideoId(), getCurrentUserId());
							LearningProgress learningProgress = null;
							if (result.isEmpty()) {
								// 沒播放紀錄 新增一筆
								Date today = new Date();
								learningProgress = new LearningProgress();
								learningProgress.setLearningDuration(0);
								learningProgress.setVideoDuration(video.getDuration());
								learningProgress.setActive(Constant.TRUE);
								learningProgress.setCreateDt(today);
								learningProgress.setCreateId(getCurrentUserId());
								learningProgress.setUserId(getCurrentUserId());
								learningProgress.setFkCourseVideo(courseVideoVo.getCourseVideoId());
								learningProgress = learningProgressRepository.save(learningProgress);
								result.add(learningProgress);
							}
							// 正常只有一筆，取第一筆
							learningProgress = result.stream().findFirst().get();
							courseVideoVo.setVideoDuration(learningProgress.getVideoDuration());
							courseVideoVo.setLearningDuration(learningProgress.getLearningDuration());
							courseVideoVo.setLastWatchDt(learningProgress.getLastWatchDt());
						}

					}
				}
			}
		}
		return courseVo;
	}

	private String getVideoToken(String videoId) {
		DateTime dateTime = new DateTime();
		VideoToken videoToken = new VideoToken();
		videoToken.setActive(Constant.TRUE);
		videoToken.setCreateDt(dateTime.toDate());
		videoToken.setUpdateDt(dateTime.toDate());
		videoToken.setCreateId(Constant.STYSTEM);
		videoToken.setExpireDt(dateTime.plusDays(1).toDate());
		videoToken.setLoginId(getCurrentUserId());
		videoToken.setVideoId(videoId);
		videoToken.setIp("0.0.0.0");

		videoToken = videoTokenRepository.save(videoToken);
		return videoToken.getToken();
	}
}
