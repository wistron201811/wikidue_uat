package com.e7learning.course.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.e7learning.cart.service.CartService;
import com.e7learning.course.service.CourseService;
import com.e7learning.course.vo.CourseVo;

@Controller
@RequestMapping(value = "/course")
public class CourseController {
	private static String HOME_PAGE = "courseHomePage";
	private static String DETAIL_PAGE = "courseIn";
	private static String PLAY_PAGE = "coursePlay";
	
	private static String HOME_PATH = "home";

	@Autowired
	private CourseService courseService;

	@Autowired
	private CartService cartService;

	@RequestMapping(value = "/home", method = { RequestMethod.GET })
	public String goHome(Model model, Integer tagId) {
		model.addAttribute("tag", courseService.getCourseTag(tagId));
		return HOME_PAGE;
	}
	
	/**
	 * 用料號查
	 * 
	 * @param materialNum
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/mn/{materialNum}", method = { RequestMethod.GET })
	public String goDetailByMaterialNum(@PathVariable("materialNum") String materialNum, Model model) {
		CourseVo result = courseService.findAvailableByMaterialNum(materialNum);
		if (result == null) {
			return "redirect:/course/home";
		}
		result.setProductId(result.getProductId());
		model.addAttribute("course", result);
		model.addAttribute("existCart", cartService.existCart(result.getProductId()));
		return DETAIL_PAGE;
	}

	@RequestMapping(value = "/detail/{pkProduct}", method = { RequestMethod.GET })
	public String goDetail(@PathVariable("pkProduct") String pkProduct, Model model) {
		CourseVo result = courseService.findAvailableByProductId(pkProduct);
		if (result == null) {
			return "redirect:/";
		}
		result.setProductId(pkProduct);
		model.addAttribute("course", result);
		model.addAttribute("existCart", cartService.existCart(pkProduct));
		return DETAIL_PAGE;
	}

	@RequestMapping(value = "/{courseId}", method = { RequestMethod.GET })
	public String goDetail(@PathVariable("courseId") Integer courseId, Model model) {
		CourseVo result = courseService.findAvailableByCourseId(courseId);
		if (result == null || !result.isOwnCourse()) {
			return "redirect:/";
		}
		result.setProductId(null);
		model.addAttribute("course", result);
		return DETAIL_PAGE;
	}

	@RequestMapping(value = "/play", method = { RequestMethod.GET })
	public String goPlay(@RequestParam("id") Integer courseId, String productId, String videoId, Model model) {
		CourseVo result = courseService.findAvailableByCourseId(courseId);
		if (result == null) {
			return "redirect:/";
		}
		model.addAttribute("productId", productId);
		model.addAttribute("course", result);
		if (StringUtils.isEmpty(videoId)) {
			if (!CollectionUtils.isEmpty(result.getCourseChapterList())) {
				if (!CollectionUtils.isEmpty(result.getCourseChapterList().get(0).getCourseVideoList())) {
					if (result.getCourseChapterList().get(0).getCourseVideoList().get(0).getVideo() != null) {
						model.addAttribute("videoId", result.getCourseChapterList().get(0).getCourseVideoList().get(0)
								.getVideo().getVideoId());
					}
				}
			}

		} else
			model.addAttribute("videoId", videoId);
		return PLAY_PAGE;
	}
}
