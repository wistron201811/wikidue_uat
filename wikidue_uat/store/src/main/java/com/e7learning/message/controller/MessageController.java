package com.e7learning.message.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/message")
public class MessageController {

	private static final String PAGE = "messageListPage";
	
	private static final String DETAIL_PAGE = "messageDetailPage";

	private static final Logger LOG = Logger.getLogger(MessageController.class);

	@RequestMapping(value = "/list", method = { RequestMethod.GET })
	public String goMyOrder(Model model) {
		return PAGE;
	}

	@RequestMapping(value = "/detail/{messageId}", method = { RequestMethod.GET })
	public String goDetail(Model model,@PathVariable("messageId") String messageId) {
		model.addAttribute("messageId", messageId);
		return DETAIL_PAGE;
	}
}
