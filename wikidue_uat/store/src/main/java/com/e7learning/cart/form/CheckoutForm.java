package com.e7learning.cart.form;

import java.io.Serializable;

/**
 * @author Clarence
 * 結帳表單
 *
 */
public class CheckoutForm implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 連絡電話
	 */
	private String telPhone;
	/**
	 * 發票 連絡電話
	 */
	private String receiptTelPhone;
	/**
	 * 郵遞區號
	 */
	private String zipCode;
	/**
	 * 縣市
	 */
	private String county;
	/**
	 * 行政區鄉鎮
	 */
	private String district;
	/**
	 * 發票 郵遞區號
	 */
	private String receiptZipCode;
	/**
	 * 發票 縣市
	 */
	private String receiptCounty;
	/**
	 * 發票 行政區鄉鎮
	 */
	private String receiptDistrict;
	/**
	 * 地址
	 */
	private String address;
	/**
	 * 發票 地址
	 */
	private String receiptAddress;
	/**
	 * 發票 電子信箱
	 */
	private String receiptEmail;
	/**
	 * 發票 購買人
	 */
	private String receiptName;
	/**
	 * 購買人帳號
	 */
	private String purchaserId;
	/**
	 * 收件者
	 */
	private String recipient;
	/**
	 * 訂單金額
	 */
	private Integer orderAmount;
	/**
	 * 付款金額
	 */
	private Integer paidAmount;
	/**
	 * 商品名稱
	 */
	private String itemName;
	/**
	 * 商品描述
	 */
	private String itemDesc;
	/**
	 * 訂單編號
	 */
	private String mainOrderNo;
	/**
	 * 暫存訂單編號
	 */
	private String tempOrderNo;
	/**
	 * 訂單是否需填收件者資訊(訂單商品有教具的話則需要填)
	 */
	private boolean mail;
	/**
	 * 公司統編
	 */
	private String companyNo;
	/**
	 * 公司名稱
	 */
	private String companyName;
	/**
	 * 愛心碼
	 */
	private String loveCode;
	/**
	 * 手機條碼
	 */
	private String mobileCode;
	/**
	 * 自然人憑證編號
	 */
	private String moicaNo;
	/**
	 * 發票類型
	 * 1:個人
	 * 2:捐贈
	 * 3:公司
	 */
	private String invoiceType;
	/**
	 * 會員載具類型
	 * 1:會員載具
	 * 2:自然人憑證
	 * 3:手機條碼
	 * 4:無載具(寄送)
	 */
	private String memberDeviceType;
	
	/**
	 * 企業載具類型
	 * 4:無載具(寄送)
	 */
	private String companyDeviceType;
	/**
	 * 捐贈對象名稱
	 */
	private String loveCodeName;
	/**
	 * 記住資訊
	 */
	private boolean remember;
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPurchaserId() {
		return purchaserId;
	}
	public void setPurchaserId(String purchaserId) {
		this.purchaserId = purchaserId;
	}
	public String getRecipient() {
		return recipient;
	}
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	public Integer getOrderAmount() {
		return orderAmount;
	}
	public void setOrderAmount(Integer orderAmount) {
		this.orderAmount = orderAmount;
	}
	public Integer getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(Integer paidAmount) {
		this.paidAmount = paidAmount;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getReceiptZipCode() {
		return receiptZipCode;
	}
	public void setReceiptZipCode(String receiptZipCode) {
		this.receiptZipCode = receiptZipCode;
	}
	public String getReceiptAddress() {
		return receiptAddress;
	}
	public void setReceiptAddress(String receiptAddress) {
		this.receiptAddress = receiptAddress;
	}
	public String getReceiptEmail() {
		return receiptEmail;
	}
	public void setReceiptEmail(String receiptEmail) {
		this.receiptEmail = receiptEmail;
	}
	public String getTelPhone() {
		return telPhone;
	}
	public void setTelPhone(String telPhone) {
		this.telPhone = telPhone;
	}
	public String getReceiptTelPhone() {
		return receiptTelPhone;
	}
	public void setReceiptTelPhone(String receiptTelPhone) {
		this.receiptTelPhone = receiptTelPhone;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getItemDesc() {
		return itemDesc;
	}
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}
	public String getCounty() {
		return county;
	}
	public void setCounty(String county) {
		this.county = county;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getReceiptCounty() {
		return receiptCounty;
	}
	public void setReceiptCounty(String receiptCounty) {
		this.receiptCounty = receiptCounty;
	}
	public String getReceiptDistrict() {
		return receiptDistrict;
	}
	public void setReceiptDistrict(String receiptDistrict) {
		this.receiptDistrict = receiptDistrict;
	}
	public String getReceiptName() {
		return receiptName;
	}
	public void setReceiptName(String receiptName) {
		this.receiptName = receiptName;
	}
	public String getMainOrderNo() {
		return mainOrderNo;
	}
	public void setMainOrderNo(String mainOrderNo) {
		this.mainOrderNo = mainOrderNo;
	}
	public boolean isMail() {
		return mail;
	}
	public void setMail(boolean mail) {
		this.mail = mail;
	}
	public String getTempOrderNo() {
		return tempOrderNo;
	}
	public void setTempOrderNo(String tempOrderNo) {
		this.tempOrderNo = tempOrderNo;
	}
	public String getCompanyNo() {
		return companyNo;
	}
	public void setCompanyNo(String companyNo) {
		this.companyNo = companyNo;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getLoveCode() {
		return loveCode;
	}
	public void setLoveCode(String loveCode) {
		this.loveCode = loveCode;
	}
	public String getMobileCode() {
		return mobileCode;
	}
	public void setMobileCode(String mobileCode) {
		this.mobileCode = mobileCode;
	}
	public String getMoicaNo() {
		return moicaNo;
	}
	public void setMoicaNo(String moicaNo) {
		this.moicaNo = moicaNo;
	}
	public String getInvoiceType() {
		return invoiceType;
	}
	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}
	public String getLoveCodeName() {
		return loveCodeName;
	}
	public void setLoveCodeName(String loveCodeName) {
		this.loveCodeName = loveCodeName;
	}
	public boolean isRemember() {
		return remember;
	}
	public void setRemember(boolean remember) {
		this.remember = remember;
	}
	public String getMemberDeviceType() {
		return memberDeviceType;
	}
	public void setMemberDeviceType(String memberDeviceType) {
		this.memberDeviceType = memberDeviceType;
	}
	public String getCompanyDeviceType() {
		return companyDeviceType;
	}
	public void setCompanyDeviceType(String companyDeviceType) {
		this.companyDeviceType = companyDeviceType;
	}

}
