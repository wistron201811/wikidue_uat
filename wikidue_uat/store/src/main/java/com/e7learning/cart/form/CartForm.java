package com.e7learning.cart.form;

import java.io.Serializable;
import java.util.List;

import com.e7learning.api.domain.result.CartResult;

public class CartForm implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	List<CartResult> list;

	public List<CartResult> getList() {
		return list;
	}

	public void setList(List<CartResult> list) {
		this.list = list;
	}
	
	
}
