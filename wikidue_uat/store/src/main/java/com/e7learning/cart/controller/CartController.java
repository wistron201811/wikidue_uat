package com.e7learning.cart.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.e7learning.cart.form.CheckoutForm;
import com.e7learning.cart.service.CartService;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.payment.service.PaymentService;
import com.google.gson.Gson;

@Controller
@RequestMapping(value = "/cart")
public class CartController {

	public static final String FORM_BEAN_NAME = "cartForm";

	private static final Logger LOG = Logger.getLogger(CartController.class);

	private static String CHECKOUT_PAGE = "cartCheckoutPage";
	private static String STEP1_NOMAIL_PAGE = "cartToolCheckoutStep1NoMail";
	private static String STEP1_PAGE = "cartCheckoutStep1Page";
	private static String STEP2_PAGE = "cartCheckoutStep2Page";
	private static String RESULT_PAGE = "cartCheckoutResultPage";

	@Autowired
	private CartService cartService;
	
	@Autowired
	private PaymentService paymentService;

	@RequestMapping(value = "/checkout", method = { RequestMethod.GET })
	public String goHome(Model model) {
		return CHECKOUT_PAGE;
	}
	
	@RequestMapping(params = "method=cancel",value = "/checkout", method = { RequestMethod.GET })
	public String cancel(@RequestParam("mainOrderNo") String mainOrderNo ,Model model) {
		try {
			cartService.cancelOrder(mainOrderNo);
		} catch (ServiceException e) {
		}
		return "redirect:/order/myOrder";
	}

	@RequestMapping(params = "method=re",value = "/checkout", method = { RequestMethod.GET })
	public String goCheckOutAgain(@RequestParam("mainOrderNo") String mainOrderNo ,Model model) {
		try {
			CheckoutForm checkoutForm = cartService.goCheckOut(mainOrderNo);
			model.addAttribute("checkoutForm", checkoutForm);
			if (checkoutForm.isMail()) {
				return STEP1_PAGE;
			}
			return STEP1_NOMAIL_PAGE;
		} catch (ServiceException e) {
			return "redirect:/order/myOrder";
		}
	}

	@RequestMapping(params = "method=step1", value = "/checkout", method = { RequestMethod.POST })
	public String goStep1(Model model, @ModelAttribute("checkoutForm") CheckoutForm form) {
		try {
			CheckoutForm checkoutForm = cartService.goCheckOut();
			model.addAttribute("checkoutForm", checkoutForm);
			if (checkoutForm.isMail()) {
				return STEP1_PAGE;
			}
			return STEP1_NOMAIL_PAGE;
		} catch (ServiceException e) {
			return CHECKOUT_PAGE;
		}
	}
	
	@RequestMapping(params = "method=step2", value = "/checkout", method = { RequestMethod.POST })
	public String goStep2(Model model, @ModelAttribute("checkoutForm") CheckoutForm form) {
		return STEP2_PAGE;
	}

	@RequestMapping(params = "method=goBack", value = "/checkout", method = { RequestMethod.POST })
	public String goBackToStep1(Model model, @ModelAttribute("checkoutForm") CheckoutForm form) {
		return STEP1_PAGE;
	}
	
	@RequestMapping(params = "method=checkoutValidate", value = "/checkout", method = RequestMethod.POST)
	public String checkoutValidate(@ModelAttribute("checkoutForm") CheckoutForm form,HttpSession session) {
		try {
			cartService.validateCheckout(form);
			session.setAttribute("checkoutForm", form);
		} catch (ServiceException e) {
			LOG.error("e", e);
			return CHECKOUT_PAGE;
		}
		return "redirect:/cart/checkout?method=checkout";
	}

	@RequestMapping(params = "method=checkout", value = "/checkout", produces = "text/html;charset=UTF-8")
	public @ResponseBody String checkout(HttpSession session) {
		CheckoutForm form =(CheckoutForm) session.getAttribute("checkoutForm");
		try {
			return cartService.checkout(form);
		} catch (ServiceException e) {
			LOG.error("e", e);
		}
		return null;
	}

	@RequestMapping(value = "/result", method = { RequestMethod.POST })
	public String result(Model model, @RequestParam Map<String, Object> result) {
		//{"MerchantID":"2000132","MerchantTradeNo":"fa5518a5c7df4de5859f","PaymentDate":"2018/04/23 00:46:22","PaymentType":"Credit_CreditCard","PaymentTypeChargeFee":"1","RtnCode":"1","RtnMsg":"Succeeded","SimulatePaid":"0","TradeAmt":"399","TradeDate":"2018/04/23 00:44:13","TradeNo":"1804230044138593","CheckMacValue":"20E556F23BB397A9C49F8F010A68B32CA5542CA32E23C61C8794CEAB8FDE9686"}
		LOG.error("checkout result:" + new Gson().toJson(result));
		model.addAttribute("RtnCode", result.get("RtnCode"));
		model.addAttribute("MerchantTradeNo", result.get("MerchantTradeNo"));
		try {
			paymentService.setPaymentProcessing(result);
		} catch (ServiceException e) {
			LOG.error("setPaymentProcessing", e);
		}
		return RESULT_PAGE;
	}
}
