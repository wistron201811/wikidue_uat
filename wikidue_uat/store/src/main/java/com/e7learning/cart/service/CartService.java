package com.e7learning.cart.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.api.domain.param.CartQueryParam;
import com.e7learning.api.domain.result.CartResult;
import com.e7learning.api.service.CartApiService;
import com.e7learning.cart.form.CartForm;
import com.e7learning.cart.form.CheckoutForm;
import com.e7learning.common.Constant;
import com.e7learning.common.enums.OrderStatusEnum;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.RestException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.CommonService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.service.MailService;
import com.e7learning.common.service.MainOrderNoService;
import com.e7learning.common.service.SsoService;
import com.e7learning.common.service.TemplateService;
import com.e7learning.common.vo.SsoUserInfoResponse;
import com.e7learning.payment.integration.PaymentAllInOne;
import com.e7learning.payment.integration.domain.AioCheckOutOneTime;
import com.e7learning.payment.integration.domain.InvoiceObj;
import com.e7learning.repository.CartRepository;
import com.e7learning.repository.E7MessageRepository;
import com.e7learning.repository.MainOrderRepository;
import com.e7learning.repository.MemberInfoRepository;
import com.e7learning.repository.PaymentRepository;
import com.e7learning.repository.ProductRepository;
import com.e7learning.repository.PropsRepository;
import com.e7learning.repository.SubOrderDetailRepository;
import com.e7learning.repository.SubOrderRepository;
import com.e7learning.repository.TempOrderRepository;
import com.e7learning.repository.UserCourseRepository;
import com.e7learning.repository.UserInfoRepository;
import com.e7learning.repository.model.Cart;
import com.e7learning.repository.model.Course;
import com.e7learning.repository.model.E7Message;
import com.e7learning.repository.model.MainOrder;
import com.e7learning.repository.model.MemberInfo;
import com.e7learning.repository.model.MsgCategory;
import com.e7learning.repository.model.Payment;
import com.e7learning.repository.model.Product;
import com.e7learning.repository.model.Props;
import com.e7learning.repository.model.SubOrder;
import com.e7learning.repository.model.SubOrderDetail;
import com.e7learning.repository.model.TempOrder;
import com.e7learning.repository.model.UserCourse;
import com.e7learning.repository.model.Vendor;

@Service
public class CartService extends BaseService {

	private static final Logger log = Logger.getLogger(CartApiService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private CartRepository cartRepository;

	@Autowired
	private ConfigService configService;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private CartApiService cartApiService;

	@Autowired
	private SubOrderRepository subOrderRepository;

	@Autowired
	private SubOrderDetailRepository subOrderDetailRepository;

	@Autowired
	private PaymentRepository paymentRepository;

	@Autowired
	private PropsRepository propsRepository;

	@Autowired
	private TempOrderRepository tempOrderRepository;

	@Autowired
	private PaymentAllInOne paymentAllInOne;

	@Autowired
	private MainOrderNoService mainOrderNoService;

	@Autowired
	private MemberInfoRepository memberInfoRepository;

	@Autowired
	private TemplateService templateService;

	@Autowired
	private E7MessageRepository e7MessageRepository;

	@Autowired
	private MainOrderRepository mainOrderRepository;

	@Autowired
	private MailService mailService;

	@Autowired
	private CommonService commonService;

	@Autowired
	private UserCourseRepository userCourseRepository;

	@Autowired
	private UserInfoRepository userInfoRepository;

	@Autowired
	private SsoService ssoService;

	private static final String DATE_FORMAT = "yyyy/MM/dd";

	public Integer queryCartAmount() {
		try {
			CartQueryParam param = new CartQueryParam();
			param.setQueryStock(true);
			List<CartResult> list = cartApiService.queryCart(param);
			if (list != null && !list.isEmpty()) {
				Integer result = 0;
				for (CartResult tmp : list) {
					result += (tmp.getUnitPrice() != null ? tmp.getUnitPrice() : 0);
				}
				return result;
			}
		} catch (Exception e) {
			log.error("e", e);
		}
		return 0;
	}

	public CartForm queryCart() throws RestException {
		List<CartResult> result = new ArrayList<>();

		CartForm form = new CartForm();
		try {
			List<Cart> carts = cartRepository.findAll((Root<Cart> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
				List<Predicate> predicates = new ArrayList<>();

				predicates.add(cb.equal(root.get("purchaser"), getCurrentUserId()));
				predicates.add(cb.equal(root.get("status"), Constant.CART_STATUS_AVAILABLE));

				if (!predicates.isEmpty()) {
					return cb.and(predicates.toArray(new Predicate[predicates.size()]));
				}

				return cb.conjunction();
			}, new Sort(Direction.DESC, "updateDt"));
			carts.stream().forEach(v -> result.add(toCartResult(v)));

			form.setList(result);
		} catch (Exception e) {
			log.error("queryCategory", e);
			throw new RestException(resources.getMessage("category.query.err", null, null));
		}

		return form;
	}

	public CartResult toCartResult(Cart cart) {
		CartResult cartResult = new CartResult();
		cartResult.setCreateDt(cart.getCreateDt());
		cartResult.setCreateId(cart.getCreateId());
		cartResult.setFkProduct(cart.getFkProduct());
		cartResult.setLastModifiedId(cart.getLastModifiedId());
		cartResult.setListPrice(cart.getListPrice());
		cartResult.setPkCart(cart.getPkCart());
		cartResult.setProductImage(configService.getHost() + configService.getImageApi() + cart.getProductImage());
		cartResult.setProductName(cart.getProductName());
		cartResult.setPurchaser(cart.getPurchaser());
		cartResult.setQuantity(cart.getQuantity());
		cartResult.setStatus(cartResult.getStatus());
		cartResult.setUnitPrice(cart.getUnitPrice());
		cartResult.setUpdateDt(cart.getUpdateDt());

		return cartResult;
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void cancelOrder(String mainOrderNo) throws ServiceException {
		Date now = new Date();
		MainOrder mainOrder = null;
		List<SubOrder> subOrderList = null;
		List<SubOrderDetail> sodList = new ArrayList<>();
		boolean result = false;
		try {
			mainOrder = mainOrderRepository.findOne(mainOrderNo);
			if (mainOrder == null
					|| !StringUtils.equalsIgnoreCase(mainOrder.getOrderStatus(), OrderStatusEnum.E.toString())
					|| mainOrder.getSubOrderList() == null) {
				throw new ServiceException("無此訂單資訊");
			}

			// 搜尋購物車還未轉訂單的商品
			subOrderList = mainOrder.getSubOrderList();
			if (subOrderList == null || subOrderList.isEmpty()) {
				throw new ServiceException("無此訂單資訊");
			}
			mainOrder.setOrderStatus(OrderStatusEnum.C.toString());
			for (SubOrder so : subOrderList) {
				so.setUpdateDt(now);
				so.setLastModifiedId(getCurrentUserId());
				so.setOrderStatus(OrderStatusEnum.C.toString());
				if (so.getSubOrderDetailList() != null) {
					so.getSubOrderDetailList().size();
					for (SubOrderDetail detail : so.getSubOrderDetailList()) {
						detail.setStatus(OrderStatusEnum.C.toString());
						detail.setUpdateDt(now);
						detail.setLastModifiedId(getCurrentUserId());
						subOrderDetailRepository.save(detail);
						sodList.add(detail);
						// 補回庫存
						Props props = detail.getProduct().getProps();
						if (props != null) {
							props.setQuantity(props.getQuantity() + detail.getQuantity());
							propsRepository.save(props);
						}
					}
				}
			}
			subOrderRepository.save(subOrderList);
			result = true;
		} catch (ServiceException e) {
			throw e;
		} catch (Exception e) {
			log.error("e", e);
			throw new ServiceException("取消訂單失敗");
		} finally {
			if (result) {
				Date sendDate = now;
				Date cancelDate = now;
				String orderNo = mainOrder.getPkMainOrder();
				List<String> adminEmailList = commonService.getAdminEmailList();
				for (SubOrder so : subOrderList) {
					Vendor vendor = so.getVendor();
					List<Map<String, String>> details = new ArrayList<>();
					for (SubOrderDetail sod : so.getSubOrderDetailList()) {
						Map<String, String> detail = new HashMap<>();
						detail.put("name", sod.getProductName());
						detail.put("price", String.valueOf(sod.getUnitPrice()));
						detail.put("quantity", String.valueOf(sod.getQuantity()));
						detail.put("total", String.valueOf(sod.getTotalAmount()));
						details.add(detail);
					}
					mailService.sendCancelOrderNotificationVendorEmail(
							new String[] {
									StringUtils.isEmpty(vendor.getEmail()) ? vendor.getUid() : vendor.getEmail() },
							adminEmailList.toArray(new String[adminEmailList.size()]), sendDate, vendor.getVendorName(),
							orderNo, cancelDate, details, String.valueOf(so.getOrderAmount()));
				}
				String buyerEmail = mainOrder.getReceiptEmail();
				String buyerName = mainOrder.getPurchaser();
				List<Map<String, String>> details = new ArrayList<>();
				int total = 0;
				for (SubOrderDetail sod : sodList) {
					Map<String, String> detail = new HashMap<>();
					detail.put("name", sod.getProductName());
					detail.put("price", String.valueOf(sod.getUnitPrice()));
					detail.put("quantity", String.valueOf(sod.getQuantity()));
					detail.put("total", String.valueOf(sod.getTotalAmount()));
					details.add(detail);
					total += sod.getTotalAmount();
				}
				SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
				mailService.sendCancelOrderNotificationBuyerEmail(new String[] { buyerEmail }, null, sendDate,
						buyerName, cancelDate, orderNo, details, String.valueOf(total));
				sendNotification(String.format("Wikidue Store 教育商城 - 訂單編號 %s 取消訂單通知信", orderNo),
						templateService.getCancelOrderNotificationBuyer(configService.getHost(), sdf.format(sendDate),
								buyerName, configService.getImageUrl(), sdf.format(cancelDate), orderNo, details,
								String.valueOf(total), configService.getMyOrderUrl(), configService.getServiceUrl()),
						mainOrder.getCreateId(), 1);
			}
		}

	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public CheckoutForm goCheckOut(String mainOrderNo) throws ServiceException {
		CheckoutForm form = new CheckoutForm();
		try {
			MainOrder mainOrder = mainOrderRepository.findOne(mainOrderNo);
			if (mainOrder == null
					|| !StringUtils.equalsIgnoreCase(mainOrder.getOrderStatus(), OrderStatusEnum.E.toString())
					|| mainOrder.getSubOrderList() == null || mainOrder.getPaymentDueDate() == null
					|| mainOrder.getPaymentDueDate().before(new Date())) {
				throw new ServiceException("無此結帳資訊");
			}
			List<SubOrder> subOrderList = mainOrder.getSubOrderList();
			if (subOrderList == null || subOrderList.isEmpty()) {
				throw new ServiceException("無此結帳資訊");
			}
			form.setMainOrderNo(mainOrderNo);
			form.setOrderAmount(mainOrder.getOrderAmount());
			for (SubOrder po : subOrderList) {
				if (po.getSubOrderDetailList() != null) {
					po.getSubOrderDetailList().size();
					for (SubOrderDetail tmp : po.getSubOrderDetailList()) {
						if (tmp.getProduct() != null && tmp.getProduct().getProps() != null) {
							form.setMail(true);
						}
					}
				}
			}
			MemberInfo memberInfo = memberInfoRepository.findOne(getCurrentUserId());
			if (memberInfo != null) {
				if (!StringUtils.isEmpty(memberInfo.getInvoiceType())) {
					form.setRemember(true);
					form.setReceiptZipCode(memberInfo.getInvoiceZipcode());
					form.setReceiptTelPhone(memberInfo.getInvoiceTelphone());
					form.setReceiptName(memberInfo.getInvoiceName());
					// form.setReceiptEmail(memberInfo.getInvoiceEmail());
					// 改為sso的mail modified by chris
					form.setReceiptEmail(getSsoEmailData());
					form.setReceiptAddress(memberInfo.getInvoiceAddress());
					form.setInvoiceType(memberInfo.getInvoiceType());
					switch (memberInfo.getInvoiceType()) {
					case "1":
						if (StringUtils.equals(memberInfo.getDeviceType(), "2")) {
							form.setMoicaNo(memberInfo.getMoicaNo());
						} else if (StringUtils.equals(memberInfo.getDeviceType(), "3")) {
							form.setMobileCode(memberInfo.getMobileCode());
						}
						form.setMemberDeviceType(memberInfo.getDeviceType());
						break;
					case "2":
						form.setLoveCode(memberInfo.getLoveCode());
						break;
					case "3":
						form.setCompanyName(memberInfo.getCompanyName());
						form.setCompanyNo(memberInfo.getCompanyNo());
						form.setCompanyDeviceType(memberInfo.getDeviceType());
						break;
					default:
					}
					if (form.isMail()) {
						form.setRecipient(memberInfo.getMailName());
						form.setAddress(memberInfo.getMailAddress());
						form.setZipCode(memberInfo.getMailZipcode());
						form.setTelPhone(memberInfo.getMailTelPhone());
					}
				}

			} else {
				form.setInvoiceType("1");
			}
			return form;
		} catch (Exception e) {
			log.error("e", e);
			throw new ServiceException("結帳失敗");
		}
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public CheckoutForm goCheckOut() throws ServiceException {
		CheckoutForm form = new CheckoutForm();
		try {
			// 搜尋購物車還未轉訂單的商品
			List<Cart> carts = cartRepository.findAll((Root<Cart> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
				List<Predicate> predicates = new ArrayList<>();

				predicates.add(cb.equal(root.get("purchaser"), getCurrentUserId()));
				predicates.add(cb.equal(root.get("status"), Constant.CART_STATUS_AVAILABLE));

				if (!predicates.isEmpty()) {
					return cb.and(predicates.toArray(new Predicate[predicates.size()]));
				}

				return cb.conjunction();
			});
			if (carts == null || carts.isEmpty()) {
				throw new ServiceException("購物車無商品");
			}
			// 轉為暫存訂單
			Date now = new Date();
			String tempOrderNo = UUID.randomUUID().toString();
			int paidAmount = 0;
			for (Cart cart : carts) {
				TempOrder to = new TempOrder();
				to.setCheckOutId(tempOrderNo);
				to.setCreateDt(now);
				to.setCreateId(getCurrentUserId());
				to.setFkCart(cart.getPkCart());
				to.setFkProduct(cart.getFkProduct());
				to.setListPrice(cart.getListPrice());
				to.setProductName(cart.getProductName());
				to.setPurchaser(cart.getPurchaser());
				to.setQuantity(cart.getQuantity());
				to.setUnitPrice(cart.getUnitPrice());
				tempOrderRepository.save(to);

				paidAmount += (cart.getQuantity() * cart.getUnitPrice());
				Product product = productRepository.findOne(cart.getFkProduct());
				Props props = product.getProps();
				if (props != null) {
					if (props.getQuantity() < to.getQuantity()) {
						throw new ServiceException(resources.getMessage("product.stock.out", null, null));
					}
					form.setMail(true);
				}
			}

			form.setTempOrderNo(tempOrderNo);
			form.setOrderAmount(paidAmount);
			MemberInfo memberInfo = memberInfoRepository.findOne(getCurrentUserId());
			if (memberInfo != null) {
				if (!StringUtils.isEmpty(memberInfo.getInvoiceType())) {
					form.setRemember(true);
					form.setReceiptZipCode(memberInfo.getInvoiceZipcode());
					form.setReceiptTelPhone(memberInfo.getInvoiceTelphone());
					// form.setReceiptEmail(memberInfo.getInvoiceEmail());
					// 改為sso的mail modified by chris
					form.setReceiptEmail(getSsoEmailData());
					form.setReceiptName(memberInfo.getInvoiceName());
					form.setReceiptAddress(memberInfo.getInvoiceAddress());
					form.setInvoiceType(memberInfo.getInvoiceType());
					switch (memberInfo.getInvoiceType()) {
					case "1":
						if (StringUtils.equals(memberInfo.getDeviceType(), "2")) {
							form.setMoicaNo(memberInfo.getMoicaNo());
						} else if (StringUtils.equals(memberInfo.getDeviceType(), "3")) {
							form.setMobileCode(memberInfo.getMobileCode());
						}
						form.setMemberDeviceType(memberInfo.getDeviceType());
						break;
					case "2":
						form.setLoveCode(memberInfo.getLoveCode());
						break;
					case "3":
						form.setCompanyName(memberInfo.getCompanyName());
						form.setCompanyNo(memberInfo.getCompanyNo());
						form.setCompanyDeviceType(memberInfo.getDeviceType());
						break;
					default:
					}
					if (form.isMail()) {
						form.setRecipient(memberInfo.getMailName());
						form.setAddress(memberInfo.getMailAddress());
						form.setZipCode(memberInfo.getMailZipcode());
						form.setTelPhone(memberInfo.getMailTelPhone());
					}
				}

			} else {
				form.setInvoiceType("1");
				// 改為sso的mail modified by chris
				form.setReceiptEmail(getSsoEmailData());
			}
			return form;
		} catch (ServiceException e) {
			throw e;
		} catch (Exception e) {
			log.error("e", e);
			throw new ServiceException("結帳失敗");
		}
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void validateCheckout(CheckoutForm form) throws ServiceException {
		try {
			if ((StringUtils.isEmpty(form.getMainOrderNo()) && StringUtils.isEmpty(form.getTempOrderNo()))
					|| (!StringUtils.isEmpty(form.getMainOrderNo()) && !StringUtils.isEmpty(form.getTempOrderNo()))) {
				// 訂單編號或暫存訂單只能其中一個有值
				throw new ServiceException("訂單有誤");
			}
			if (!StringUtils.isEmpty(form.getMainOrderNo())) {
				MainOrder mainOrder = mainOrderRepository.findOne(form.getMainOrderNo());

				if (mainOrder == null
						|| !StringUtils.equalsIgnoreCase(mainOrder.getOrderStatus(), OrderStatusEnum.E.toString())) {
					throw new ServiceException("無此訂單");
				}

				// 結帳尚未付款的訂單
				List<SubOrder> subOrderList = mainOrder.getSubOrderList();
				if (subOrderList == null || subOrderList.isEmpty()) {
					throw new ServiceException("無此訂單");
				}
			} else {
				// 將暫存訂單轉為訂單再結帳
				List<TempOrder> tempOrderList = tempOrderRepository
						.findAll((Root<TempOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
							List<Predicate> predicates = new ArrayList<>();
							predicates.add(cb.equal(root.get("checkOutId"), form.getTempOrderNo()));

							if (!predicates.isEmpty()) {
								return cb.and(predicates.toArray(new Predicate[predicates.size()]));
							}

							return cb.conjunction();
						});
				if (tempOrderList == null || tempOrderList.isEmpty()) {
					throw new ServiceException("訂單有誤");
				}

				for (TempOrder to : tempOrderList) {
					if (StringUtils.equals(to.getStatus(), "T")) {
						throw new ServiceException("購物車商品已成立訂單，請至「我的訂單」查看");
					}
				}
			}
		} catch (ServiceException e) {
			log.error("e", e);
			throw e;
		} catch (Exception e) {
			log.error("e", e);
			throw new ServiceException("結帳失敗");
		}

	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public String checkout(CheckoutForm form) throws ServiceException {
		Date now = new Date();
		try {
			if ((StringUtils.isEmpty(form.getMainOrderNo()) && StringUtils.isEmpty(form.getTempOrderNo()))
					|| (!StringUtils.isEmpty(form.getMainOrderNo()) && !StringUtils.isEmpty(form.getTempOrderNo()))) {
				// 訂單編號或暫存訂單只能其中一個有值
				throw new ServiceException("訂單有誤");
			}
			String mainOrderNo;
			if (StringUtils.isEmpty(form.getMainOrderNo())) {
				mainOrderNo = mainOrderNoService.genMainOrderNo();
			} else {
				mainOrderNo = form.getMainOrderNo();
			} 
			// 綠界結帳編號，為20碼亂數

			String paymentId = (mainOrderNo + UUID.randomUUID().toString()).replaceAll("-", "").substring(0, 20);
			List<SubOrder> subOrderList = null;
			MainOrder mainOrder = null;
			// 新增結帳資訊
			Payment payment = new Payment();
			payment.setActive(Constant.TRUE);
			payment.setCreateDt(now);
			payment.setCreateId(getCurrentUserId());
			payment.setPkPayment(paymentId);
			payment.setUpdateDt(now);
			payment.setStatus(OrderStatusEnum.N.toString());

			if (form.getInvoiceType() != null) {
				String invoiceType = null;
				if (StringUtils.equals(form.getInvoiceType(), "1")) {
					if (StringUtils.equals(form.getMemberDeviceType(), "1")) {
						invoiceType = "1";
					} else if (StringUtils.equals(form.getMemberDeviceType(), "4")) {
						invoiceType = "4";
					}
				}
				switch (form.getInvoiceType()) {
				case "1":
					if (StringUtils.equals(form.getMemberDeviceType(), "2")) {
						invoiceType = "2";
						payment.setMoicaNo(form.getMoicaNo());
					} else if (StringUtils.equals(form.getMemberDeviceType(), "3")) {
						invoiceType = "3";
						payment.setMobileCode(form.getMobileCode());
					}
					break;
				case "2":
					invoiceType = "5";
					payment.setLoveCode(form.getLoveCode());
					break;
				case "3":
					invoiceType = "6";
					payment.setCompanyName(form.getCompanyName());
					payment.setCompanyNo(form.getCompanyNo());
					break;
				default:

				}
				payment.setInvoiceType(invoiceType);
			}

			payment = paymentRepository.save(payment);
			if (!StringUtils.isEmpty(form.getMainOrderNo())) {
				mainOrder = mainOrderRepository.findOne(form.getMainOrderNo());

				if (mainOrder == null
						|| !StringUtils.equalsIgnoreCase(mainOrder.getOrderStatus(), OrderStatusEnum.E.toString())) {
					throw new ServiceException("無此訂單");
				}

				// 結帳尚未付款的訂單
				subOrderList = mainOrder.getSubOrderList();
				if (subOrderList == null || subOrderList.isEmpty()) {
					throw new ServiceException("無此訂單");
				}
				mainOrder.setPayment(payment);
				mainOrder.setMobile(form.getTelPhone());
				mainOrder.setPurchaser(
						StringUtils.isEmpty(form.getReceiptName()) ? getCurrentUserId() : form.getReceiptName());
				mainOrder.setPurchaserId(getCurrentUserId());
				mainOrder.setRecipient(form.getRecipient());
				mainOrder.setReceiptEmail(getSsoEmailData());
				mainOrder.setReceiptAddress(form.getReceiptCounty() + form.getReceiptDistrict()
						+ form.getReceiptZipCode() + form.getReceiptAddress());
				mainOrder.setReceiptTelphone(form.getReceiptTelPhone());
				mainOrder.setTelphone(form.getTelPhone());
				if (StringUtils.isNoneBlank(form.getAddress())) {
					mainOrder.setAddress(form.getCounty() + form.getDistrict() + form.getZipCode() + form.getAddress());
				}
				// 最後付款期限
				mainOrder.setPaymentDueDate(
						new DateTime(mainOrder.getCreateDt()).plusDays(configService.getPaymentDueDate()).toDate());
				mainOrder.setUpdateDt(now);
				mainOrder.setLastModifiedId(getCurrentUserId());
				mainOrderRepository.save(mainOrder);

			} else {
				int totalAmount = 0;
				// 將暫存訂單轉為訂單再結帳
				List<TempOrder> tempOrderList = tempOrderRepository
						.findAll((Root<TempOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
							List<Predicate> predicates = new ArrayList<>();
							predicates.add(cb.equal(root.get("checkOutId"), form.getTempOrderNo()));

							if (!predicates.isEmpty()) {
								return cb.and(predicates.toArray(new Predicate[predicates.size()]));
							}

							return cb.conjunction();
						});
				if (tempOrderList == null || tempOrderList.isEmpty()) {
					throw new ServiceException("訂單有誤");
				}

				for (TempOrder to : tempOrderList) {
					if (StringUtils.equals(to.getStatus(), "T")) {
						throw new ServiceException("購物車商品已成立訂單，請至「我的訂單」查看");
					}

					totalAmount += (to.getQuantity() * to.getUnitPrice());
				}
				mainOrder = new MainOrder();
				mainOrder.setPkMainOrder(mainOrderNo);
				mainOrder.setCreateDt(now);
				mainOrder.setUpdateDt(now);
				mainOrder.setOrderDt(now);
				mainOrder.setOrderStatus(OrderStatusEnum.E.toString());
				mainOrder.setPayment(payment);
				mainOrder.setCreateId(getCurrentUserId());
				mainOrder.setLastModifiedId(getCurrentUserId());
				if (StringUtils.isNoneBlank(form.getAddress())) {
					mainOrder.setAddress(form.getCounty() + form.getDistrict() + form.getZipCode() + form.getAddress());
				}
				mainOrder.setLastModifiedId(getCurrentUserId());
				mainOrder.setMobile(form.getTelPhone());
				mainOrder.setPurchaser(
						StringUtils.isEmpty(form.getReceiptName()) ? getCurrentUserId() : form.getReceiptName());
				mainOrder.setPurchaserId(getCurrentUserId());
				mainOrder.setRecipient(form.getRecipient());
				mainOrder.setReceiptEmail(getSsoEmailData());
				mainOrder.setReceiptAddress(form.getReceiptCounty() + form.getReceiptDistrict()
						+ form.getReceiptZipCode() + form.getReceiptAddress());
				mainOrder.setReceiptTelphone(form.getReceiptTelPhone());
				mainOrder.setTelphone(form.getTelPhone());
				// 最後付款期限
				mainOrder.setPaymentDueDate(
						new DateTime(mainOrder.getCreateDt()).plusDays(configService.getPaymentDueDate()).toDate());
				mainOrder.setOrderAmount(totalAmount);
				mainOrder = mainOrderRepository.save(mainOrder);

				subOrderList = new ArrayList<>();
				form.setMainOrderNo(mainOrderNo);

				Map<String, List<TempOrder>> vendorMap = new HashMap<>();
				// 將購物車商品依廠商分組
				for (TempOrder to : tempOrderList) {
					Product product = productRepository.findOne(to.getFkProduct());
					if (product == null) {
						continue;
					}
					if (vendorMap.get(product.getVendor().getUid()) == null) {
						vendorMap.put(product.getVendor().getUid(), new ArrayList<>());
					}
					vendorMap.get(product.getVendor().getUid()).add(to);
				}
				// 新增訂單
				for (String vendorUid : vendorMap.keySet()) {
					SubOrder subOrder = new SubOrder();
					List<TempOrder> orderList = vendorMap.get(vendorUid);
					int orderAmount = 0;

					for (TempOrder cart : orderList) {
						orderAmount += (cart.getUnitPrice() * cart.getQuantity());
					}
					Product product = productRepository.findOne(orderList.get(0).getFkProduct());
					if (product != null) {
						subOrder.setVendor(product.getVendor());
					}
					subOrder.setCreateDt(now);
					subOrder.setCreateId(getCurrentUserId());
					subOrder.setLastModifiedId(getCurrentUserId());
					subOrder.setOrderAmount(orderAmount);
					subOrder.setOrderStatus(OrderStatusEnum.N.toString());
					subOrder.setUpdateDt(now);
					subOrder.setMainOrder(mainOrder);
					subOrder = subOrderRepository.save(subOrder);
					// 新增訂單商品詳細資料
					List<SubOrderDetail> odList = new ArrayList<>();
					for (TempOrder to : orderList) {
						SubOrderDetail detail = new SubOrderDetail();
						Product p = productRepository.findOne(to.getFkProduct());
						if (p != null) {
							if (p.getProps() != null) {
								// 扣教具數量
								Props props = p.getProps();
								props.setQuantity(props.getQuantity() - to.getQuantity());
								propsRepository.save(props);
								form.setMail(true);
							}
							detail.setStatus(OrderStatusEnum.N.toString());
							detail.setCreateDt(now);
							detail.setUpdateDt(now);
							detail.setCreateId(getCurrentUserId());
							detail.setLastModifiedId(getCurrentUserId());
							detail.setSubOrder(subOrder);
							detail.setProduct(p);
							detail.setProductName(p.getProductName());
							detail.setQuantity(to.getQuantity());
							detail.setTotalAmount(to.getQuantity() * to.getUnitPrice());
							detail.setUnitPrice(to.getUnitPrice());
							detail.setUpdateDt(now);
							if (p.getProps() != null) {
								detail.setPropsSplitRatio(p.getVendor().getPropsSplitRatio());
							} else if (p.getCourse() != null) {
								detail.setCourseSplitRatio(p.getVendor().getCourseSplitRatio());
							}
							detail = subOrderDetailRepository.save(detail);
							odList.add(detail);
							// 將購物車商品狀態轉為 (T : 已轉訂單)
							Cart cart = cartRepository.findOne(to.getFkCart());
							if (cart != null) {
								cart.setStatus("T");
								cart.setUpdateDt(now);
								cart.setLastModifiedId(getCurrentUserId());
								cartRepository.save(cart);
							}
							to.setUpdateDt(now);
							to.setLastModifiedId(getCurrentUserId());
							to.setStatus("T");
						}
					}
					subOrder.setSubOrderDetailList(odList);
					subOrderList.add(subOrder);
				}
				mainOrder.setSubOrderList(subOrderList);

			}
			// 組成錄界結帳的商品名稱，綠界規範為用#符號分隔商品名稱
			StringBuilder itemName = new StringBuilder();
			int orderAmount = 0;
			for (SubOrder subOrder : subOrderList) {
				if (subOrder.getSubOrderDetailList() != null) {
					subOrder.getSubOrderDetailList().size();
					for (SubOrderDetail detail : subOrder.getSubOrderDetailList()) {
						if (!StringUtils.isEmpty(itemName.toString())) {
							itemName.append("#");
						}
						itemName.append(String.format("%s x %s", detail.getProductName(),
								String.valueOf(detail.getQuantity())));
						orderAmount += (detail.getTotalAmount());
					}
				}
			}
			mainOrder.setOrderAmount(orderAmount);
			mainOrder = mainOrderRepository.save(mainOrder);
			mainOrder.setSubOrderList(subOrderList);
			payment.setTradeAmt(orderAmount);
			payment.setMainOrder(mainOrder);
			payment = paymentRepository.save(payment);
			AioCheckOutOneTime aio = new AioCheckOutOneTime();
			// 目前沒有發票
			InvoiceObj invoice = null;
			// 用剛剛的訂單id
			aio.setMerchantTradeNo(paymentId);
			// 廠商可自行決定交易時間
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			aio.setMerchantTradeDate(sdf.format(now));
			// 從廠商DB撈出的商品資訊
			aio.setItemName(StringUtils.replaceChars(itemName.toString(), "\"", "'"));
			aio.setTotalAmount(String.valueOf(orderAmount));
			aio.setTradeDesc(configService.getTradeDesc());
			// 廠商可自行決定是否延遲撥款
			aio.setHoldTradeAMT("0");
			// 後端設定付款完成通知回傳網址
			aio.setReturnURL(configService.getHost() + configService.getPaymentReturnUrl());
			aio.setClientBackURL(configService.getHost() + configService.getClientBackUrl());
			aio.setOrderResultURL(configService.getHost() + configService.getOrderResultUrl());

			String html = paymentAllInOne.aioCheckOut(aio, invoice);
			log.error(html);
			return html;
		} catch (

		Exception e) {
			log.error("e", e);
			throw new ServiceException();
		} finally {
			if (form.isRemember()) {
				MemberInfo memberInfo = memberInfoRepository.findOne(getCurrentUserId());
				if (memberInfo == null) {
					memberInfo = new MemberInfo();
					memberInfo.setMemberUid(getCurrentUserId());
					memberInfo.setCreateDt(now);
				} else {
					memberInfo.setUpdateDt(now);
					memberInfo.setLastModifiedId(getCurrentUserId());
				}

				memberInfo.setInvoiceAddress(form.getReceiptAddress());
				// memberInfo.setInvoiceEmail(form.getReceiptEmail());
				// 改為sso的mail modified by chris
				memberInfo.setInvoiceEmail(getSsoEmailData());
				memberInfo.setInvoiceName(form.getReceiptName());
				memberInfo.setInvoiceTelphone(form.getReceiptTelPhone());
				memberInfo.setInvoiceType(form.getInvoiceType());
				memberInfo.setInvoiceZipcode(form.getReceiptZipCode());
				memberInfo.setCompanyName(form.getCompanyName());
				memberInfo.setCompanyNo(form.getCompanyNo());
				memberInfo.setLoveCode(form.getLoveCode());
				if (StringUtils.equals(form.getInvoiceType(), "1")) {
					memberInfo.setDeviceType(form.getMemberDeviceType());
				} else if (StringUtils.equals(form.getInvoiceType(), "3")) {
					memberInfo.setDeviceType(form.getCompanyDeviceType());
				}
				if (!StringUtils.isEmpty(form.getRecipient())) {
					memberInfo.setMailAddress(form.getAddress());
					memberInfo.setMailName(form.getRecipient());
					memberInfo.setMailTelPhone(form.getTelPhone());
					memberInfo.setMailZipcode(form.getZipCode());
				}

				memberInfoRepository.save(memberInfo);
			} else {
				if (memberInfoRepository.exists(getCurrentUserId())) {
					memberInfoRepository.delete(getCurrentUserId());
				}
			}
		}
	}

	// private void sendCreateOrderNotification(MainOrder mainOrder) {
	//
	// try {
	// long totalAmount = 0;
	// String buyerName = mainOrder.getPurchaser();
	// String mainOrderNo = mainOrder.getPkMainOrder();
	// String paymentDueDate = DateTimeFormat.forPattern("yyyy/MM/dd HH:mm:ss")
	// .print(mainOrder.getPaymentDueDate().getTime());
	// String createDt = DateTimeFormat.forPattern("yyyy/MM/dd
	// HH:mm:ss").print(mainOrder.getCreateDt().getTime());
	// List<Map<String, String>> details = new ArrayList<>();
	// for (SubOrder subOrder : mainOrder.getSubOrderList()) {
	// List<SubOrderDetail> odList = subOrder.getSubOrderDetailList();
	// if (odList != null) {
	// for (SubOrderDetail od : odList) {
	// Map<String, String> map = new HashMap<>();
	// map.put("name", od.getProductName());
	// map.put("price", String.valueOf(od.getUnitPrice()));
	// map.put("num", String.valueOf(od.getQuantity()));
	// map.put("total", String.valueOf(od.getTotalAmount()));
	// totalAmount += od.getTotalAmount();
	// details.add(map);
	// }
	// }
	// }
	// String recipient = mainOrder.getRecipient();
	// String phone = mainOrder.getTelphone();
	// String address = mainOrder.getAddress();
	// String email = mainOrder.getPurchaserId();
	//
	// String message = templateService.getOrderCreateNotification(buyerName,
	// mainOrderNo, createDt,
	// paymentDueDate, details, String.valueOf(totalAmount), recipient, phone,
	// address, email);
	//
	// E7Message e7Message = new E7Message();
	// e7Message.setActive(Constant.TRUE);
	// e7Message.setContent(message);
	// e7Message.setCreateDt(new Date());
	// e7Message.setCreateId(Constant.STYSTEM);
	// e7Message.setOwnerID(mainOrder.getCreateId());
	// e7Message.setIsRead(Constant.FLASE);
	// e7Message.setTitle("訂單「" + mainOrderNo + "」建立通知");
	// MsgCategory msgCategory = new MsgCategory();
	// msgCategory.setPkMsgCategory(1);
	// e7Message.setMsgCategory(msgCategory);
	// e7MessageRepository.save(e7Message);
	// } catch (Exception e) {
	// log.error("sendCreateOrderNotification:", e);
	// }
	//
	// }

	public boolean existCart(String productPk) {
		if (getCurrentUser() == null)
			return false;
		return cartRepository.count((Root<Cart> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
			return cb.and(cb.equal(root.get("createId"), getCurrentUserId()),
					cb.equal(root.get("fkProduct"), productPk),
					cb.equal(root.get("status"), Constant.CART_STATUS_AVAILABLE));
		}) > 0;
	}

	@Transactional(rollbackFor = { RestException.class }, noRollbackFor = { MessageException.class })
	public void buyFreeCourse(String productPk) throws RestException {
		log.error("productPk:" + productPk);
		MainOrder mainOrder = null;
		SubOrder subOrder = null;
		SubOrderDetail subOrderDetail = null;
		Date now = new Date();
		try {
			Product product = productRepository.findOne(productPk);
			if (product != null && product.getCourse() != null && product.getDiscountPrice() <= 0) {
				Course course = product.getCourse();
				List<UserCourse> userCourseList = userCourseRepository
						.findByCoursePkCourseAndUserId(course.getPkCourse(), getCurrentUserId());
				if (userCourseList != null && !userCourseList.isEmpty()) {
					throw new RestException("使用者已購買此課程");
				} else {
					String mainOrderNo = mainOrderNoService.genMainOrderNo();
					mainOrder = new MainOrder();
					mainOrder.setPkMainOrder(mainOrderNo);
					mainOrder.setCreateDt(now);
					mainOrder.setUpdateDt(now);
					mainOrder.setOrderDt(now);
					mainOrder.setOrderStatus(OrderStatusEnum.E.toString());
					mainOrder.setCreateId(getCurrentUserId());
					mainOrder.setLastModifiedId(getCurrentUserId());
					mainOrder.setPurchaser(getCurrentUser().getName());
					mainOrder.setPurchaserId(getCurrentUserId());
					mainOrder.setOrderAmount(0);
					mainOrder = mainOrderRepository.save(mainOrder);
					subOrder = new SubOrder();
					log.error("product.getVendor():" + product.getVendor());
					subOrder.setVendor(product.getVendor());
					subOrder.setCreateDt(now);
					subOrder.setCreateId(getCurrentUserId());
					subOrder.setLastModifiedId(getCurrentUserId());
					subOrder.setOrderAmount(0);
					subOrder.setOrderStatus(OrderStatusEnum.P.toString());
					subOrder.setUpdateDt(now);
					subOrder.setMainOrder(mainOrder);
					subOrder = subOrderRepository.save(subOrder);
					// 新增訂單商品詳細資料
					subOrderDetail = new SubOrderDetail();
					subOrderDetail.setStatus(OrderStatusEnum.P.toString());
					subOrderDetail.setCreateDt(now);
					subOrderDetail.setUpdateDt(now);
					subOrderDetail.setCreateId(getCurrentUserId());
					subOrderDetail.setLastModifiedId(getCurrentUserId());
					subOrderDetail.setSubOrder(subOrder);
					subOrderDetail.setProduct(product);
					subOrderDetail.setProductName(product.getProductName());
					subOrderDetail.setQuantity(1);
					subOrderDetail.setTotalAmount(0);
					subOrderDetail.setUnitPrice(0);
					subOrderDetail.setUpdateDt(now);
					subOrderDetail.setCourseSplitRatio(product.getVendor().getCourseSplitRatio());

					subOrderDetail = subOrderDetailRepository.save(subOrderDetail);

					UserCourse userCourse = new UserCourse();
					userCourse.setCourse(product.getCourse());
					userCourse.setActive(Constant.TRUE);
					userCourse.setCreateDt(now);
					userCourse.setCreateId("free course");
					userCourse.setUserId(getCurrentUserId());
					userCourseRepository.save(userCourse);
				}
			} else {
				throw new RestException("購買課程失敗");
			}
		} catch (RestException e) {
			throw e;
		} catch (Exception e) {
			log.error("buyFreeCourse", e);
		}
	}

	private void sendNotification(String title, String message, String createId, Integer msgCategoryPk) {

		E7Message e7Message = new E7Message();
		e7Message.setActive(Constant.TRUE);
		e7Message.setContent(message);
		e7Message.setCreateDt(new Date());
		e7Message.setCreateId(Constant.STYSTEM);
		e7Message.setOwnerID(createId);
		e7Message.setIsRead(Constant.FLASE);
		e7Message.setTitle(title);
		MsgCategory msgCategory = new MsgCategory();
		msgCategory.setPkMsgCategory(1);
		e7Message.setMsgCategory(msgCategory);
		e7MessageRepository.save(e7Message);
	}

	private String getSsoEmailData() {
		String ssoEmail = "";
		// 從sso拿
		try {
			SsoUserInfoResponse ssoUserInfoResponse = ssoService.getUserInfo(getCurrentUser().getAccessToken());
			ssoEmail = ssoUserInfoResponse.getBody().getUserInfo().getEmail();
		} catch (Exception e) {
			log.error("getUserInfo", e);
			if (userInfoRepository.exists(getCurrentUserId())) {
				ssoEmail = userInfoRepository.findOne(getCurrentUserId()).getEmail();
			}
		}
		return ssoEmail;
	}
}
