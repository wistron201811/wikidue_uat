package com.e7learning.banner.service;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.e7learning.common.Constant;
import com.e7learning.common.service.BaseService;
import com.e7learning.repository.BannerRepository;
import com.e7learning.repository.SysConfigRepository;
import com.e7learning.repository.model.Banner;

@Service
public class BannerService extends BaseService {

	private static final Logger LOG = Logger.getLogger(BannerService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private BannerRepository bannerRepository;

	@Autowired
	private SysConfigRepository sysConfigRepository;

	public List<Banner> queryIndexBannerList() {
		try {
			Date now = new Date();
			return bannerRepository
					.findByPublishSDateLessThanEqualAndPublishEDateGreaterThanEqualAndPublishOrderByOrder(now, new LocalDate().toDate(),
							Constant.TRUE);
		} catch (Exception e) {
			LOG.error("exception:", e);
		}
		return null;
	}

	public String getBannerAutoPlaySpeed() {
		if (sysConfigRepository.exists(Constant.BANNER_AUTO_PLAY_SPEED))
			return sysConfigRepository.findOne(Constant.BANNER_AUTO_PLAY_SPEED).getValue();
		else
			return "5000";
	}

}
