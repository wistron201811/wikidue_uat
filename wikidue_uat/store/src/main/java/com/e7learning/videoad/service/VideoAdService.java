package com.e7learning.videoad.service;

import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.e7learning.common.Constant;
import com.e7learning.common.service.BaseService;
import com.e7learning.repository.VideoAdRepository;
import com.e7learning.repository.model.VideoAd;

@Service
public class VideoAdService extends BaseService {

	private static final Logger LOG = Logger.getLogger(VideoAdService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private VideoAdRepository videoAdRepository;

	public VideoAd queryRandomActiveVideAd() {
		try {
			List<VideoAd> list = videoAdRepository.findByActive(Constant.TRUE);
			if (list != null && !list.isEmpty()) {
				int index = new Random().nextInt(list.size());
				return list.get(index);
			}
		} catch (Exception e) {
			LOG.error("exception:", e);
		}
		return null;
	}

}
