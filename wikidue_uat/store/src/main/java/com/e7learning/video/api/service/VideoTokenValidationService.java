package com.e7learning.video.api.service;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.e7learning.common.Constant;
import com.e7learning.common.exception.RestException;
import com.e7learning.common.service.BaseService;
import com.e7learning.repository.VideoTokenRepository;
import com.e7learning.repository.model.VideoToken;

@Service
public class VideoTokenValidationService {

	private static final Logger log = Logger.getLogger(VideoTokenValidationService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private VideoTokenRepository videoTokenRepository;

	/**
	 * 驗證token 是否有效
	 * 
	 * @param token
	 * @param hbID
	 * @param ip
	 * @throws RestException
	 */
	public void validation(String token, String hbID, String ip) throws RestException {
		if (videoTokenRepository.exists(token)) {
			VideoToken videoToken = videoTokenRepository.findOne(token);
			if (!Constant.STYSTEM.equals(videoToken.getCreateId())) {
				
				
				videoToken.setCount(videoToken.getCount() + 1);
				videoTokenRepository.save(videoToken);
			}
			if (!StringUtils.equals(videoToken.getActive(), Constant.TRUE)) {
				log.error("無效的token");
				throw new RestException("無效的token");
			}
			if (videoToken.getCount() > 1) {
				log.error("此token已使用過");
				throw new RestException("此token已使用過");
			}
			if (!StringUtils.equals(videoToken.getVideoId(), hbID)) {
				log.error("影片ID不相符");
				throw new RestException("影片ID不相符");
			}
			if (new Date().after(videoToken.getExpireDt())) {
				log.error("token已過期" + videoToken.getExpireDt() + " vs " + new Date());
				throw new RestException("token已過期");
			}
		} else {
			throw new RestException("token不存在");
		}
	}
}
