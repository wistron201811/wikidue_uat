package com.e7learning.video.api.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Rest Service回應結果
 *
 */
public class RestResult implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private String result;
	
	public RestResult() {
	}
	
	public RestResult(boolean res) {
		if(res) {
			result = "success";
		}else {
			result = "failed";
		}
	}

	public String getResult() {
		return result;
	}


	public void setResult(String result) {
		this.result = result;
	}


	@Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
