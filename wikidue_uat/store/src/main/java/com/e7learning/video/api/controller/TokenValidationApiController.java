package com.e7learning.video.api.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.e7learning.common.exception.RestException;
import com.e7learning.video.api.domain.RestResult;
import com.e7learning.video.api.service.VideoTokenValidationService;

@RestController
@RequestMapping(value = "/api")
public class TokenValidationApiController {

	private static final Logger log = Logger.getLogger(TokenValidationApiController.class);

	@Autowired
	private VideoTokenValidationService service;

	@RequestMapping(value = "/video/token/validation", method = { RequestMethod.GET })
	public RestResult validation(String token, String hbID, String ip) throws RestException {
		log.error("validation :" + "token:" + token + ",hbID:" + hbID + ",ip:" + ip);

		try {
			service.validation(token, hbID, ip);
		} catch (Exception e) {
			log.error("validation error:" + "token:" + token + ",hbID:" + hbID + ",ip:" + ip, e);
			return new RestResult(false);
		}
		return new RestResult(true);
	}
}
