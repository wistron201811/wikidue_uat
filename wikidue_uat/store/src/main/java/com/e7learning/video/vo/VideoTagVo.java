package com.e7learning.video.vo;

import java.io.Serializable;

/**
 * @author Clarence 
 * 影片標籤
 */
public class VideoTagVo implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer videoTagId;
	
	/**
	 * 標籤名稱下載連結
	 */
	private String tagName;

	public Integer getVideoTagId() {
		return videoTagId;
	}

	public void setVideoTagId(Integer videoTagId) {
		this.videoTagId = videoTagId;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	
}
