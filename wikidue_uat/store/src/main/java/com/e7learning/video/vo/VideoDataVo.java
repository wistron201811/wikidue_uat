package com.e7learning.video.vo;

import java.io.Serializable;

/**
 * @author Clarence 
 * 影片檔案資料
 */
public class VideoDataVo implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer videoDataId;

	/**
	 * 下載連結
	 */
	private String http;

	/**
	 * 影音編碼率
	 */
	private Integer bitRate;

	/**
	 * 影片解析度
	 */
	private String resolution;

	public Integer getVideoDataId() {
		return videoDataId;
	}

	public void setVideoDataId(Integer videoDataId) {
		this.videoDataId = videoDataId;
	}

	public String getHttp() {
		return http;
	}

	public void setHttp(String http) {
		this.http = http;
	}

	public Integer getBitRate() {
		return bitRate;
	}

	public void setBitRate(Integer bitRate) {
		this.bitRate = bitRate;
	}

	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

}
