package com.e7learning.video.vo;

import java.io.Serializable;
import java.util.List;

import com.e7learning.repository.model.VideoData;
import com.e7learning.repository.model.VideoTag;

/**
 * @author Clarence 平台影片
 */

public class VideoVo implements Serializable{
	private static final long serialVersionUID = 1L;

	/**
	 * 平台影片主鍵
	 */
	private String videoId;

	/**
	 * 影片資料清單
	 */
	private List<VideoData> videoDataList;

	/**
	 * 影片資料清單
	 */
	private List<VideoTag> videoTagList;

	/**
	 * 轉檔結果(成功或失敗)
	 */
	private String result;

	/**
	 * HLS 連結
	 */
	private String stream;

	/**
	 * 影片長度
	 */
	private Integer duration;

	/**
	 * 影片名稱
	 */
	private String title;

	/**
	 * 帳號名稱
	 */
	private String accountName;

	/**
	 * 簡述
	 */
	private String description;

	/**
	 * 影片擷圖
	 */
	private String img;

	/**
	 * 影音原始檔名
	 */
	private String fileName;

	/**
	 * 影音內嵌碼
	 */
	private String embed;

	/**
	 * 影音平台ID
	 */
	private String hbID;
	
	private String videoStreamUrl;

	public String getVideoId() {
		return videoId;
	}

	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}

	public List<VideoData> getVideoDataList() {
		return videoDataList;
	}

	public void setVideoDataList(List<VideoData> videoDataList) {
		this.videoDataList = videoDataList;
	}

	public List<VideoTag> getVideoTagList() {
		return videoTagList;
	}

	public void setVideoTagList(List<VideoTag> videoTagList) {
		this.videoTagList = videoTagList;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getStream() {
		return stream;
	}

	public void setStream(String stream) {
		this.stream = stream;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getEmbed() {
		return embed;
	}

	public void setEmbed(String embed) {
		this.embed = embed;
	}

	public String getHbID() {
		return hbID;
	}

	public void setHbID(String hbID) {
		this.hbID = hbID;
	}

	public String getVideoStreamUrl() {
		return videoStreamUrl;
	}

	public void setVideoStreamUrl(String videoStreamUrl) {
		this.videoStreamUrl = videoStreamUrl;
	}

	

}
