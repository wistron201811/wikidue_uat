package com.e7learning.share.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.e7learning.common.aop.AuthThreadLocal;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.ConfigService;
import com.e7learning.share.form.ShareCenterForm;
import com.e7learning.share.service.ShareCenterService;
import com.e7learning.share.validation.ShareCenterFormValidation;

@Controller
@RequestMapping(value = "/share")
public class ShareCenterController {

	public static final String FORM_BEAN_NAME = "shareForm";

	private static final String HOME_PAGE = "shareHomePage";

	private static final String EDIT_PAGE = "shareEditPage";

	private static final String ADD_PAGE = "shareAddPage";

	private static final String DETAIL_PAGE = "shareDetailPage";

	@Autowired
	private ShareCenterFormValidation validation;

	@Autowired
	private ShareCenterService shareCenterService;

	@Autowired
	private ConfigService configService;

	protected void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
		binder.setValidator(validation);
	}

	@RequestMapping(value = "/home", method = { RequestMethod.GET })
	public String goHome(Model model) {
		return HOME_PAGE;
	}

	@RequestMapping(value = "/add", method = { RequestMethod.GET })
	public String goAdd(Model model) {
		model.addAttribute(FORM_BEAN_NAME, shareCenterService.genShareCenterForm());
		return ADD_PAGE;
	}

	@RequestMapping(value = "/add", method = { RequestMethod.POST })
	public String add(Model model, @Validated ShareCenterForm form, BindingResult result) {
		if (result.hasErrors()) {
			return ADD_PAGE;
		}
		try {
			shareCenterService.createShare(form);
			return "redirect:/share/home";
		} catch (ServiceException e) {

		}
		return ADD_PAGE;
	}

	@RequestMapping(value = "/edit", method = { RequestMethod.GET })
	public String goEdit(Model model, Integer id) {
		try {
			ShareCenterForm form = shareCenterService.findShare(id);
			if (StringUtils.equals(form.getOwnerId(), AuthThreadLocal.getLoginUser().getLoginId()))
				model.addAttribute(FORM_BEAN_NAME, form);
			else
				return "redirect:/share/detail/" + id;
		} catch (ServiceException e) {
			return "redirect:/share/home";
		}
		return EDIT_PAGE;
	}

	@RequestMapping(value = "/edit", method = { RequestMethod.POST })
	public String edit(Model model, @Validated ShareCenterForm form, BindingResult result) {
		if (result.hasErrors()) {
			return EDIT_PAGE;
		}
		try {
			model.addAttribute(FORM_BEAN_NAME, shareCenterService.updateShare(form));
		} catch (ServiceException e) {
			return EDIT_PAGE;
		}
		return "redirect:/share/home";
	}

	@RequestMapping(value = "/delete", method = { RequestMethod.POST })
	public String delete(Model model, Integer shareId) {

		try {
			shareCenterService.deleteShare(shareId);
		} catch (ServiceException e) {
			return EDIT_PAGE;
		}
		return "redirect:/share/home";
	}

	@RequestMapping(value = "/detail/{shareId}", method = { RequestMethod.GET })
	public String goDetail(Model model, @PathVariable("shareId") Integer shareId) {
		try {
			model.addAttribute(FORM_BEAN_NAME, shareCenterService.findShare(shareId));
		} catch (ServiceException e) {
			return "redirect:/share/home";
		}
		return DETAIL_PAGE;
	}

	@RequestMapping(value = "/detail/download/{shareId}", method = { RequestMethod.GET })
	public void downloadAll(Model model, @PathVariable("shareId") Integer shareId, HttpServletResponse res)
			throws ServiceException, IOException {
		res.setHeader("Content-Disposition", "attachment; filename=" + shareId + ".zip");
		res.setHeader("Content-Type", "application/zip");
		shareCenterService.downloadAll(shareId, res.getOutputStream());
	}
}
