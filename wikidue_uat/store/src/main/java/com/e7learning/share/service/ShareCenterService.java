package com.e7learning.share.service;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.api.domain.result.CategoryResult;
import com.e7learning.api.domain.result.KnowledgeMapResult;
import com.e7learning.common.Constant;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.CommonCfgService;
import com.e7learning.common.service.FileStorageService;
import com.e7learning.common.utils.TagUtils;
import com.e7learning.repository.KnowledgeMapRepository;
import com.e7learning.repository.ShareCenterRepository;
import com.e7learning.repository.ShareFileRepository;
import com.e7learning.repository.ShareTagRepository;
import com.e7learning.repository.TagRepository;
import com.e7learning.repository.UserFavouriteRepository;
import com.e7learning.repository.model.Category;
import com.e7learning.repository.model.FileStorage;
import com.e7learning.repository.model.ShareCenter;
import com.e7learning.repository.model.ShareFile;
import com.e7learning.repository.model.ShareTag;
import com.e7learning.repository.model.Tag;
import com.e7learning.share.form.ShareCenterForm;
import com.google.gson.Gson;

import net.lingala.zip4j.io.ZipOutputStream;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

@Service
public class ShareCenterService extends BaseService {

	private static final Logger log = Logger.getLogger(ShareCenterService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private ShareCenterRepository shareCenterRepository;

	@Autowired
	private FileStorageService fileStorageService;

	@Autowired
	private ShareFileRepository shareFileRepository;

	@Autowired
	private TagRepository tagRepository;

	@Autowired
	private KnowledgeMapRepository knowledgeMapRepository;

	@Autowired
	private ShareTagRepository shareTagRepository;

	@Autowired
	private CommonCfgService commonCfgService;

	@Autowired
	private UserFavouriteRepository userFavouriteRepository;

	public ShareCenterForm genShareCenterForm() {
		ShareCenterForm form = new ShareCenterForm();
		form.setAuthor(getCurrentUser() != null ? getCurrentUser().getName() : StringUtils.EMPTY);
		return form;
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void createShare(ShareCenterForm form) throws ServiceException {
		try {
			ShareCenter shareCenter = new ShareCenter();
			shareCenter.setApproveStatus(Constant.APPROVE_STATUS_Y);
			shareCenter.setActive(Constant.TRUE);
			shareCenter.setCreateDt(new Date());
			shareCenter.setCreateId(getCurrentUserId());

			shareCenter.setShareName(form.getTitle());
			shareCenter.setShareIntroduction(form.getContent());

			if (form.getImage() != null && !form.getImage().isEmpty()) {
				// 上傳影像
				shareCenter.setShareImage(fileStorageService.saveFile(form.getImage()).getPkFile());
			}

			shareCenter.setShareUid(form.getAuthor());

			shareCenter = shareCenterRepository.save(shareCenter);

			if (form.getAttachments() != null && form.getAttachments().length > 0
					&& !form.getAttachments()[0].isEmpty()) {
				// 上傳附件
				List<FileStorage> files = fileStorageService.saveFiles(form.getAttachments());
				for (FileStorage file : files) {
					ShareFile shareFile = new ShareFile();
					shareFile.setShareCenter(shareCenter);
					shareFile.setFileId(file.getPkFile());
					shareFileRepository.save(shareFile);
				}
			}

			// 設定tag
			KnowledgeMapResult[] tags = new Gson().fromJson(form.getTag(), KnowledgeMapResult[].class);
			for (KnowledgeMapResult tag : tags) {
				Tag t = new Tag();
				t.setActive(Constant.TRUE);
				t.setShareCenter(shareCenter);
				t.setTag(tag.getTag());
				/** "":只有類別,"0":年級,"1":科目,"3":章節 */
				t.setTagLevel(tag.getLevel());
				t.setTagName(tag.getName());

				t.setCreateDt(new Date());
				t.setCreateId(getCurrentUserId());
				tagRepository.save(t);
			}

			// 上傳CategoryResult
			CategoryResult[] categorys = new Gson().fromJson(form.getCategory(), CategoryResult[].class);
			for (CategoryResult cr : categorys) {
				Tag t = new Tag();
				t.setActive(Constant.TRUE);
				t.setShareCenter(shareCenter);
				t.setTag(StringUtils.EMPTY);
				t.setTagLevel(StringUtils.EMPTY);
				t.setTagName(StringUtils.EMPTY);
				Category category = new Category();
				category.setPkCategory(cr.getCategoryId());
				t.setCategory(category);

				t.setCreateDt(new Date());
				t.setCreateId(getCurrentUserId());
				tagRepository.save(t);
			}

			// 展開tag
			List<KnowledgeMapResult> expTag = TagUtils.appendCategory(TagUtils.expandTag(Arrays.asList(tags)),
					Arrays.asList(categorys));
			log.info(new Gson().toJson(expTag));
			for (KnowledgeMapResult tmp : expTag) {
				ShareTag shareTag = new ShareTag();
				shareTag.setActive(Constant.TRUE);
				shareTag.setCreateDt(new Date());
				shareTag.setCreateId(getCurrentUserId());

				shareTag.setShareCenter(shareCenter);
				shareTag.setTag(tmp.getTag());
				shareTag.setTagLevel(tmp.getLevel());
				if (StringUtils.isNotBlank(tmp.getLevel())) {
					if (knowledgeMapRepository.exists(shareTag.getTag())) {
						shareTag.setTagName(knowledgeMapRepository.findOne(shareTag.getTag()).getTagName());
					} else {
						shareTag.setTagName(tmp.getName());
					}
				} else {
					shareTag.setTagName(tmp.getName());
				}

				shareTagRepository.save(shareTag);
			}

		} catch (Exception e) {
			log.error("createShare", e);
			throw new ServiceException(resources.getMessage("create.share.error", null, null));
		}

		throw new MessageException(resources.getMessage("create.share.success", null, null));
	}

	@Transactional(readOnly = true)
	public ShareCenterForm findShare(Integer shareId) throws ServiceException {
		if (shareId != null) {
			if (shareCenterRepository.exists(shareId)) {
				ShareCenter shareCenter = shareCenterRepository.findOne(shareId);
				ShareCenterForm form = new ShareCenterForm();
				form.setShareId(shareCenter.getPkShare());
				form.setAuthor(shareCenter.getShareUid());
				form.setImage(fileStorageService.getFileStorageById(shareCenter.getShareImage()));
				if (shareCenter.getFiles() != null && !shareCenter.getFiles().isEmpty()) {
					List<FileStorage> files = new ArrayList<>();
					shareCenter.getFiles().stream().forEach(v -> {
						files.add(fileStorageService.getFileStorageById(v.getFileId()));
					});
					form.setOrigAttachments(files.toArray(new FileStorage[files.size()]));
				}
				form.setOwnerId(shareCenter.getCreateId());
				form.setTitle(shareCenter.getShareName());
				form.setContent(shareCenter.getShareIntroduction());

				List<KnowledgeMapResult> tags = new ArrayList<>();
				shareCenter.getTags().size();
				shareCenter.getTags().stream().filter(v -> v.getCategory() == null).forEach(v -> {
					KnowledgeMapResult tmp = new KnowledgeMapResult();
					tmp.setLevel(v.getTagLevel());
					tmp.setName(v.getTagName());
					tmp.setTag(v.getTag());
					tags.add(tmp);
				});
				form.setTag(new Gson().toJson(tags));

				List<CategoryResult> categorys = new ArrayList<>();
				shareCenter.getTags().stream().filter(v -> v.getCategory() != null).forEach(v -> {
					CategoryResult tmp = new CategoryResult();
					tmp.setCategoryCode(v.getCategory().getCategoryCode());
					tmp.setCategoryId(v.getCategory().getPkCategory());
					tmp.setCategoryName(v.getCategory().getCategoryName());
					tmp.setCategoryType(v.getCategory().getCategoryType());
					categorys.add(tmp);
				});
				form.setCategory(new Gson().toJson(categorys));
				form.setCnt(userFavouriteRepository.countByshareCenters(shareId, "1"));
				return form;
			}
		}
		throw new ServiceException(resources.getMessage("query.share.error", null, null));
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public ShareCenterForm updateShare(ShareCenterForm form) throws ServiceException {
		try {
			if (form.getShareId() != null) {
				if (shareCenterRepository.exists(form.getShareId())) {
					ShareCenter shareCenter = shareCenterRepository.findOne(form.getShareId());
					if (!StringUtils.equals(shareCenter.getCreateId(), getCurrentUserId())) {
						// 只可以本人修改
						throw new ServiceException(resources.getMessage("modify.share.error", null, null));
					}

					if (StringUtils.isNotBlank(form.getTitle())) {
						shareCenter.setShareName(form.getTitle());
					}
					if (StringUtils.isNotBlank(form.getContent())) {
						shareCenter.setShareIntroduction(form.getContent());
					}
					if (StringUtils.isNotBlank(form.getAuthor())) {
						shareCenter.setShareUid(form.getAuthor());
					}
					if (StringUtils.isNotBlank(form.getTag())) {
						// 先移除舊的
						shareCenter.getTags().forEach(tagRepository::delete);
						shareTagRepository.deleteByShareCenterPkShare(shareCenter.getPkShare());

						// 設定tag
						KnowledgeMapResult[] tags = new Gson().fromJson(form.getTag(), KnowledgeMapResult[].class);
						for (KnowledgeMapResult tag : tags) {
							Tag t = new Tag();
							t.setActive(Constant.TRUE);
							t.setShareCenter(shareCenter);
							t.setTag(tag.getTag());
							/** "":只有類別,"0":年級,"1":科目,"3":章節 */
							t.setTagLevel(tag.getLevel());
							t.setTagName(tag.getName());

							t.setCreateDt(new Date());
							t.setCreateId(getCurrentUserId());
							tagRepository.save(t);
						}

						// 上傳CategoryResult
						CategoryResult[] categorys = new Gson().fromJson(form.getCategory(), CategoryResult[].class);
						for (CategoryResult cr : categorys) {
							Tag t = new Tag();
							t.setActive(Constant.TRUE);
							t.setShareCenter(shareCenter);
							t.setTag(StringUtils.EMPTY);
							t.setTagLevel(StringUtils.EMPTY);
							t.setTagName(StringUtils.EMPTY);
							Category category = new Category();
							category.setPkCategory(cr.getCategoryId());
							t.setCategory(category);

							t.setCreateDt(new Date());
							t.setCreateId(getCurrentUserId());
							tagRepository.save(t);
						}

						// 展開tag
						List<KnowledgeMapResult> expTag = TagUtils
								.appendCategory(TagUtils.expandTag(Arrays.asList(tags)), Arrays.asList(categorys));
						log.info(new Gson().toJson(expTag));
						for (KnowledgeMapResult tmp : expTag) {
							ShareTag shareTag = new ShareTag();
							shareTag.setActive(Constant.TRUE);
							shareTag.setCreateDt(new Date());
							shareTag.setCreateId(getCurrentUserId());

							shareTag.setShareCenter(shareCenter);
							shareTag.setTag(tmp.getTag());
							shareTag.setTagLevel(tmp.getLevel());
							if (StringUtils.isNotBlank(tmp.getLevel())) {
								if (knowledgeMapRepository.exists(shareTag.getTag())) {
									shareTag.setTagName(knowledgeMapRepository.findOne(shareTag.getTag()).getTagName());
								} else {
									shareTag.setTagName(tmp.getName());
								}
							} else {
								shareTag.setTagName(tmp.getName());
							}
							shareTagRepository.save(shareTag);
						}

					}
					if (form.getDelFile() != null && form.getDelFile().length > 0) {
						// 刪除檔案
						Arrays.stream(form.getDelFile()).forEach(v -> {
							fileStorageService.delFile(v);
							shareFileRepository.deleteByFileId(v);
						});

					}
					if (form.getAttachments() != null && form.getAttachments().length > 0
							&& !form.getAttachments()[0].isEmpty()) {
						// 上傳附件
						List<FileStorage> files = fileStorageService.saveFiles(form.getAttachments());
						for (FileStorage file : files) {
							ShareFile shareFile = new ShareFile();
							shareFile.setShareCenter(shareCenter);
							shareFile.setFileId(file.getPkFile());
							shareFileRepository.save(shareFile);
						}
					}
					if (form.getImage() != null && !form.getImage().isEmpty()) {
						fileStorageService.delFile(shareCenter.getShareImage());
						// 上傳影像
						shareCenter.setShareImage(fileStorageService.saveFile(form.getImage()).getPkFile());
					}
					shareCenter.setUpdateDt(new Date());
					shareCenter.setLastModifiedId(getCurrentUserId());
					// shareCenterRepository.save(shareCenter);
				}
			}
		} catch (Exception e) {
			log.error("updateShare", e);
			throw new ServiceException(resources.getMessage("modify.share.error", null, null));
		}

		throw new MessageException(resources.getMessage("modify.share.success", null, null));
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void deleteShare(Integer shareId) throws ServiceException {
		try {

			if (shareCenterRepository.exists(shareId)) {
				ShareCenter shareCenter = shareCenterRepository.findOne(shareId);
				if (!StringUtils.equals(shareCenter.getCreateId(), getCurrentUserId())) {
					// 只可以本人修改
					throw new ServiceException(resources.getMessage("delete.share.error", null, null));
				}
				// 移除tag & shareTag
				shareCenter.getTags().forEach(tagRepository::delete);
				shareTagRepository.deleteByShareCenterPkShare(shareCenter.getPkShare());
				// 刪除檔案
				shareCenter.getFiles().stream().forEach(v -> {
					fileStorageService.delFile(v.getFileId());
					shareFileRepository.delete(v);
				});
				// 刪除圖檔
				fileStorageService.delFile(shareCenter.getShareImage());
				// 移除主檔案
				shareCenter.setActive(Constant.FLASE);
				// shareCenterRepository.save(shareCenter);
				// shareCenterRepository.delete(shareCenter);
			}
		} catch (Exception e) {
			log.error("deleteShare", e);
			throw new ServiceException(resources.getMessage("delete.share.error", null, null));
		}

		throw new MessageException(resources.getMessage("delete.share.success", null, null));
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void downloadAll(Integer shareId, OutputStream ops) throws ServiceException {
		ShareCenterForm form = findShare(shareId);
		ZipOutputStream zos = null;

		try {
			zos = new ZipOutputStream(ops);

			ZipParameters parameters = new ZipParameters();
			parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
			parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);

			parameters.setSourceExternalStream(true);
			for (FileStorage fs : (FileStorage[]) form.getOrigAttachments()) {
				if (fs != null) {
					parameters.setFileNameInZip(fs.getFileName());
					zos.putNextEntry(null, parameters);
					FileInputStream fis = new FileInputStream(
							fs.getResource(commonCfgService.getStoragePath()).getFile());
					try {
						byte[] buffer = new byte[4092];
						int byteCount = 0;
						while ((byteCount = fis.read(buffer)) != -1) {
							zos.write(buffer, 0, byteCount);
						}
					} catch (Exception e) {
						log.error("downloadTimeTable read " + fs.getName() + " err:", e);
						e.printStackTrace();
					}
					fis.close();
					zos.closeEntry();
				}
			}
			zos.flush();
			zos.finish();
		} catch (Exception e) {
			log.error("downloadTimeTable", e);
			throw new ServiceException(resources.getMessage("download.file.err", null, null));
		} finally {
			if (null != zos) {
				try {
					zos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
