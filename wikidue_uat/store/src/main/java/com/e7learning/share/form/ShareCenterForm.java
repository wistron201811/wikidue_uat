package com.e7learning.share.form;

import org.springframework.web.multipart.MultipartFile;

public class ShareCenterForm {

	private Integer shareId;

	private String title;

	private String content;

	private MultipartFile image;

	private String author;

	private MultipartFile[] attachments;

	private MultipartFile[] origAttachments;

	private String[] delFile;

	private String tag;

	private String category;

	private String ownerId;

	private int cnt = 0;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public MultipartFile getImage() {
		return image;
	}

	public void setImage(MultipartFile image) {
		this.image = image;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public MultipartFile[] getAttachments() {
		return attachments;
	}

	public void setAttachments(MultipartFile[] attachments) {
		this.attachments = attachments;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public MultipartFile[] getOrigAttachments() {
		return origAttachments;
	}

	public void setOrigAttachments(MultipartFile[] origAttachments) {
		this.origAttachments = origAttachments;
	}

	public String[] getDelFile() {
		return delFile;
	}

	public void setDelFile(String[] delFile) {
		this.delFile = delFile;
	}

	public Integer getShareId() {
		return shareId;
	}

	public void setShareId(Integer shareId) {
		this.shareId = shareId;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getCnt() {
		return cnt;
	}

	public void setCnt(int cnt) {
		this.cnt = cnt;
	}

}
