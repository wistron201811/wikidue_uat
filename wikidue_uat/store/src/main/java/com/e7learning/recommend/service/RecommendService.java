package com.e7learning.recommend.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.e7learning.common.Constant;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.recommend.form.RecommendForm;
import com.e7learning.repository.RecommendRepository;
import com.e7learning.repository.SysConfigRepository;
import com.e7learning.repository.model.Recommend;
import com.e7learning.repository.model.SysConfig;

@Service
public class RecommendService extends BaseService {

	private static final Logger LOG = Logger.getLogger(RecommendService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private RecommendRepository recommendRepository;

	@Autowired
	private SysConfigRepository sysConfigRepository;

	public List<Recommend> queryIndexRecommendList() {
		try {
			Date now = new Date();
			return recommendRepository
					.findTop3ByPublishSDateLessThanEqualAndPublishEDateGreaterThanEqualAndActiveOrderByPublishSDateDesc(
							now, new LocalDate().toDate(), Constant.TRUE);
		} catch (Exception e) {
			LOG.error("exception:", e);
		}
		return null;
	}

	public RecommendForm queryRecommendById(String recommendId) throws ServiceException {
		RecommendForm form = null;
		SysConfig sysConfig = null;
		try {
			if (StringUtils.isNotBlank(recommendId)) {
				Recommend recommend = recommendRepository.findOne(recommendId);
				if (sysConfigRepository.exists(Constant.RECOMMEND_RIGHT_BLOCK))
					sysConfig = sysConfigRepository.findOne(Constant.RECOMMEND_RIGHT_BLOCK);
				if (recommend != null) {
					form = new RecommendForm();
					form.setTitle(recommend.getTitle());
					form.setContent(recommend.getContent());
					form.setAuthor(recommend.getAuthor());
					form.setCreateDt(recommend.getCreateDt());
					form.setRightBlock(sysConfig != null ? sysConfig.getText() : StringUtils.EMPTY);
				}
			}
		} catch (Exception e) {
			LOG.error("exception:", e);
			throw new ServiceException(resources.getMessage("op.query.error", null, null));
		}
		return form;
	}
}
