package com.e7learning.recommend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.e7learning.common.exception.ServiceException;
import com.e7learning.recommend.service.RecommendService;

@Controller
@RequestMapping(value = "/recommend")
public class RecommendController {

	public static final String FORM_BEAN_NAME = "recommendForm";
	private static String HOME_PAGE = "recommendHomePage";
	private static final String DETAIL_PAGE = "recommendDetailPage";

	@Autowired
	private RecommendService recommendService;

	@RequestMapping(value = "/home", method = { RequestMethod.GET })
	public String goHome(Model model) {
		return HOME_PAGE;
	}

	@RequestMapping(value = "/detail/{recommendId}", method = { RequestMethod.GET })
	public String goDetail(Model model, @PathVariable("recommendId") String recommendId) {
		try {
			model.addAttribute(FORM_BEAN_NAME, recommendService.queryRecommendById(recommendId));
		} catch (ServiceException e) {
			return "redirect:/recommend/home";
		}
		return DETAIL_PAGE;
	}
}
