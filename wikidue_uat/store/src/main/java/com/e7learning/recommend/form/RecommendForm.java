package com.e7learning.recommend.form;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class RecommendForm {
	/**
	 * 作者
	 */
	private String author;
	/**
	 * 標題
	 */
	private String title;
	/**
	 * 內容
	 */
	private String content;
	/**
	 * 上架日期
	 */
	@JsonFormat(pattern="yyyy/MM/dd", timezone = "GMT+8")
	private Date createDt;
	
	private String rightBlock;


	public RecommendForm() {
	}


	public String getAuthor() {
		return author;
	}


	public void setAuthor(String author) {
		this.author = author;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}


	public Date getCreateDt() {
		return createDt;
	}


	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}


	public String getRightBlock() {
		return rightBlock;
	}


	public void setRightBlock(String rightBlock) {
		this.rightBlock = rightBlock;
	}

	
	
}
