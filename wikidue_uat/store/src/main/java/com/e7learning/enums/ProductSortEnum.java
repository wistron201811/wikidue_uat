package com.e7learning.enums;

public enum ProductSortEnum {

	PUBLISH_START_DATE_DESC, PUBLISH_START_DATE_ASC, PRICE_DESC, PRICE_ASC , WEIGHTS_DESC
}
