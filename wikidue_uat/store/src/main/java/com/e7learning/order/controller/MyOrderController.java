package com.e7learning.order.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/order")
public class MyOrderController {

	private static final String PAGE = "myOrderPage";
	
	private static final String DETAIL_PAGE = "myOrderDetailPage";

	private static final Logger LOG = Logger.getLogger(MyOrderController.class);

	@RequestMapping(value = "/myOrder", method = { RequestMethod.GET })
	public String goMyOrder(Model model) {

		return PAGE;
	}

	@RequestMapping(value = "/detail/{mainOrderNo}", method = { RequestMethod.GET })
	public String goDetail(Model model,@PathVariable("mainOrderNo") String mainOrderNo) {
		model.addAttribute("mainOrderNo", mainOrderNo);
		return DETAIL_PAGE;
	}
}
