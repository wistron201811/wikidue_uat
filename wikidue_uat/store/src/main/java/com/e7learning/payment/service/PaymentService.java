package com.e7learning.payment.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.common.Constant;
import com.e7learning.common.enums.OrderStatusEnum;
import com.e7learning.common.enums.PayWayEnum;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.CommonService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.service.MailService;
import com.e7learning.common.service.TemplateService;
import com.e7learning.common.utils.ParamUtils;
import com.e7learning.invoice.integration.InvoiceAllInOne;
import com.e7learning.invoice.integration.domain.CheckLoveCodeObj;
import com.e7learning.invoice.integration.domain.CheckMobileBarCodeObj;
import com.e7learning.invoice.integration.domain.IssueObj;
import com.e7learning.payment.vo.PaymentResultVo;
import com.e7learning.repository.E7MessageRepository;
import com.e7learning.repository.PaymentRepository;
import com.e7learning.repository.SubOrderDetailRepository;
import com.e7learning.repository.SubOrderRepository;
import com.e7learning.repository.UserCourseRepository;
import com.e7learning.repository.UserInfoRepository;
import com.e7learning.repository.model.Course;
import com.e7learning.repository.model.E7Message;
import com.e7learning.repository.model.MainOrder;
import com.e7learning.repository.model.MsgCategory;
import com.e7learning.repository.model.Payment;
import com.e7learning.repository.model.Product;
import com.e7learning.repository.model.Props;
import com.e7learning.repository.model.SubOrder;
import com.e7learning.repository.model.SubOrderDetail;
import com.e7learning.repository.model.UserCourse;
import com.e7learning.repository.model.UserInfo;
import com.e7learning.repository.model.Vendor;
import com.google.gson.Gson;

@Service
public class PaymentService extends BaseService {

	private static final Logger LOG = Logger.getLogger(PaymentService.class);

	@Autowired
	private PaymentRepository paymentRepository;

	@Autowired
	private SubOrderRepository subOrderRepository;

	@Autowired
	private SubOrderDetailRepository subOrderDetailRepository;

	@Autowired
	private UserCourseRepository userCourseRepository;

	@Autowired
	private MailService mailService;

	@Autowired
	private InvoiceAllInOne invoiceAllInOne;

	@Autowired
	private TemplateService templateService;

	@Autowired
	private E7MessageRepository e7MessageRepository;

	@Autowired
	private UserInfoRepository userInfoRepository;

	@Autowired
	private CommonService commonService;

	@Autowired
	private ConfigService configService;

	private static final String DATE_FORMAT = "yyyy/MM/dd";

	public boolean checkLoveCode(String loveCode) {
		try {
			CheckLoveCodeObj obj = new CheckLoveCodeObj();
			obj.setLoveCode(loveCode);
			String resultText = invoiceAllInOne.checkLoveCode(obj);

			Map<String, String> resultMap = ParamUtils.solveParams(resultText);
			LOG.error("checkLoveCode:" + resultText);
			return (resultMap != null && StringUtils.equals(resultMap.get("RtnCode"), "1")
					&& StringUtils.equals(resultMap.get("IsExist"), "Y"));
		} catch (Exception e) {
			LOG.error("e", e);
			return true;
		}

	}

	public boolean checkMobileCode(String mobileCode) {
		try {
			CheckMobileBarCodeObj obj = new CheckMobileBarCodeObj();
			obj.setBarCode(mobileCode);
			String resultText = invoiceAllInOne.checkMobileBarCode(obj);

			Map<String, String> resultMap = ParamUtils.solveParams(resultText);
			LOG.error("checkMobileCode:" + resultText);
			return (resultMap != null && StringUtils.equals(resultMap.get("RtnCode"), "1")
					&& StringUtils.equals(resultMap.get("IsExist"), "Y"));
		} catch (Exception e) {
			LOG.error("e", e);
			return true;
		}

	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public boolean handleResult(PaymentResultVo paymentResultVo) throws ServiceException {
		boolean result = false;
		Integer totalAmount = 0;
		List<SubOrder> orderList = null;
		MainOrder mainOrder = null;
		Payment payment = null;
		try {
			if (paymentResultVo != null) {
				payment = paymentRepository.findOne(paymentResultVo.getMerchantTradeNo());
				if (payment == null || (!StringUtils.equals(OrderStatusEnum.N.toString(), payment.getStatus())
						&& !StringUtils.equals(OrderStatusEnum.W.toString(), payment.getStatus()))) {
					if (payment == null) {
						LOG.error(String.format("handleResult error:no payment for merchantTradeNo:%s",
								paymentResultVo.getMerchantTradeNo()));
					} else {
						LOG.error(String.format("handleResult error:payment:%s status:%s",
								paymentResultVo.getMerchantTradeNo(), payment.getStatus()));
					}

					return false;
				}
				Date now = new Date();
				mainOrder = payment.getMainOrder();
				mainOrder.getSubOrderList().size();

				Integer chargeFee = null;
				try {
					chargeFee = Integer.parseInt(paymentResultVo.getPaymentTypeChargeFee());
				} catch (Exception e) {

				}
				payment.setChargeFee(chargeFee);

				payment.setCheckMacValue(paymentResultVo.getCheckMacValue());
				payment.setPaymentDt(now);
				payment.setReturnMessage(paymentResultVo.getRtnMsg());
				payment.setPaymentType(paymentResultVo.getPaymentType());
				Integer returnCode = null;
				try {
					returnCode = Integer.parseInt(paymentResultVo.getRtnCode());
					if (returnCode == 1) {
						payment.setStatus(OrderStatusEnum.P.toString());
					}
				} catch (Exception e) {

				}
				payment.setReturnCode(returnCode);
				payment.setTradeNo(paymentResultVo.getTradeNo());

				payment = paymentRepository.save(payment);
				totalAmount = payment.getTradeAmt();
				final String purchaserId = mainOrder.getPurchaserId();
				List<SubOrderDetail> odList = new ArrayList<>();
				if (StringUtils.equals(OrderStatusEnum.P.toString(), payment.getStatus())
						&& !mainOrder.getSubOrderList().isEmpty()) {
					orderList = mainOrder.getSubOrderList();

					for (SubOrder order : orderList) {
						order.setOrderStatus(OrderStatusEnum.P.toString());
						order.setUpdateDt(now);
						subOrderRepository.save(order);

						if (order.getSubOrderDetailList() != null) {
							order.getSubOrderDetailList().size();
							odList.addAll(order.getSubOrderDetailList());
							for (SubOrderDetail orderDetail : order.getSubOrderDetailList()) {
								orderDetail.setStatus(OrderStatusEnum.P.toString());
								orderDetail.setUpdateDt(now);
								subOrderDetailRepository.save(orderDetail);
								if (orderDetail.getProduct() != null && orderDetail.getProduct().getCourse() != null) {
									Course course = orderDetail.getProduct().getCourse();
									long count = userCourseRepository.count(
											(Root<UserCourse> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
												List<Predicate> predicates = new ArrayList<>();

												predicates.add(cb.equal(root.get("course").get("pkCourse"),
														course.getPkCourse()));
												predicates.add(cb.equal(root.get("userId"), purchaserId));

												if (!predicates.isEmpty()) {
													return cb.and(predicates.toArray(new Predicate[predicates.size()]));
												}

												return cb.conjunction();
											});
									if (count < 1) {
										UserCourse userCourse = new UserCourse();
										userCourse.setCourse(course);
										userCourse.setActive(Constant.TRUE);
										userCourse.setCreateDt(now);
										userCourse.setCreateId("payment result");
										userCourse.setUserId(mainOrder.getPurchaserId());
										userCourseRepository.save(userCourse);
									}
								}
							}
						}

					}
				} else {
					// 付款失敗
					LOG.error(String.format("handleResult error:payment:%s status:%s",
							paymentResultVo.getMerchantTradeNo(), payment.getStatus()));
					LOG.error(String.format("handleResult error:mainOrder.getSubOrderList().isEmpty():%s",
							String.valueOf(mainOrder.getSubOrderList().isEmpty())));

					return false;
				}
				StringBuilder itemAmount = new StringBuilder();
				StringBuilder itemCount = new StringBuilder();
				StringBuilder itemName = new StringBuilder();
				StringBuilder itemPrice = new StringBuilder();
				StringBuilder itemWord = new StringBuilder();
				for (int i = 0; i < odList.size(); i++) {
					SubOrderDetail od = odList.get(i);
					if (i > 0) {
						itemAmount.append("|");
						itemCount.append("|");
						itemName.append("|");
						itemPrice.append("|");
						itemWord.append("|");
					}
					itemName.append(od.getProductName());
					itemCount.append(od.getQuantity());
					if (od.getProduct().getCourse() != null) {
						itemWord.append("堂");
					} else {
						itemWord.append("個");
					}
					itemPrice.append(od.getUnitPrice());
					itemAmount.append((od.getQuantity() * od.getUnitPrice()));
				}

				if (!StringUtils.isEmpty(payment.getInvoiceType())) {
					IssueObj issueObj = new IssueObj();
					issueObj.setDonation("0");
					issueObj.setPrint("0");
					switch (payment.getInvoiceType()) {
					case "1":
						issueObj.setCarruerType("1");
						break;
					case "2":
						issueObj.setCarruerType("2");
						issueObj.setCarruerNum(payment.getMoicaNo());
						break;
					case "3":
						issueObj.setCarruerType("3");
						issueObj.setCarruerNum(payment.getMobileCode());
						break;
					case "4":
						issueObj.setPrint("1");
						break;
					case "5":
						issueObj.setDonation("1");
						issueObj.setLoveCode(payment.getLoveCode());
						break;
					case "6":
						issueObj.setCustomerIdentifier(payment.getCompanyNo());
						issueObj.setCustomerName(payment.getCompanyName());
						issueObj.setPrint("1");
						break;
					default:
						return true;
					}
					if (StringUtils.isEmpty(issueObj.getCustomerName())) {
						issueObj.setCustomerName(mainOrder.getPurchaser());
					}
					issueObj.setCustomerAddr(mainOrder.getReceiptAddress());
					issueObj.setCustomerEmail(mainOrder.getReceiptEmail());
					issueObj.setCustomerPhone(mainOrder.getReceiptTelphone());
					issueObj.setInvType("07");
					issueObj.setItemAmount(itemAmount.toString());
					issueObj.setItemCount(itemCount.toString());
					issueObj.setItemName(itemName.toString());
					issueObj.setItemPrice(itemPrice.toString());
					issueObj.setItemWord(itemWord.toString());
					String relateNumber = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 30);
					issueObj.setRelateNumber(relateNumber);
					issueObj.setSalesAmount(String.valueOf(payment.getTradeAmt()));
					issueObj.setTaxType("1");
					LOG.error("issue invoice param:" + new Gson().toJson(issueObj));
					String issueResult = invoiceAllInOne.issue(issueObj);
					Map<String, String> resultMap = ParamUtils.solveParams(issueResult);
					LOG.error("issue invoice result:" + new Gson().toJson(resultMap));
					if (resultMap != null && StringUtils.equals(resultMap.get("RtnCode"), "1")) {
						payment.setInvoiceNo(resultMap.get("InvoiceNumber"));
						payment.setInvoiceRandomNo(resultMap.get("RandomNumber"));
						payment.setRelateNumber(relateNumber);
						paymentRepository.save(payment);
						result = true;
					} else {
						result = configService.getSkipInvoice();
					}
				}

			}
			if (!result) {
				throw new ServiceException("付款作業發生錯誤");
			}
		} catch (ServiceException e) {
			LOG.error("e", e);
			throw e;
		} catch (Exception e) {
			LOG.error("e", e);
			throw new ServiceException("付款作業發生錯誤");
		} finally {
			if (result) {
				// 如果成功，要寄通知信
				if (orderList != null && !orderList.isEmpty()) {
					Date now = new Date();
					Date sendDate = now;
					Date orderDate = now;
					String orderNo = mainOrder.getPkMainOrder();
					String recipient = StringUtils.isEmpty(mainOrder.getRecipient()) ? StringUtils.EMPTY
							: mainOrder.getRecipient();
					String telPhone = StringUtils.EMPTY;
					String mobile = StringUtils.isEmpty(mainOrder.getTelphone()) ? StringUtils.EMPTY
							: mainOrder.getTelphone();
					String address = StringUtils.isEmpty(mainOrder.getAddress()) ? StringUtils.EMPTY
							: mainOrder.getAddress();
					String buyerName = mainOrder.getPurchaser();
					for (SubOrder order : orderList) {
						// 寄給廠商
						Vendor vendor = order.getVendor();
						if (vendor == null) {
							continue;
						}
						String vendorName = vendor.getVendorName();
						List<Map<String, String>> details = new ArrayList<>();
						List<SubOrderDetail> odList = order.getSubOrderDetailList();
						boolean mail = false;
						if (odList != null) {
							for (SubOrderDetail od : odList) {
								Map<String, String> map = new HashMap<>();
								map.put("name", od.getProductName());
								map.put("price", String.valueOf(od.getUnitPrice()));
								map.put("quantity", String.valueOf(od.getQuantity()));
								map.put("total", String.valueOf(od.getTotalAmount()));

								details.add(map);
								if (od.getProduct().getProps() != null) {
									mail = true;
								}
							}
						}
						// TODO 如果廠商連絡人email沒有填就改抓uid(註冊時的帳號)
						String email = StringUtils.isBlank(vendor.getEmail()) ? vendor.getUid() : vendor.getEmail();
						try {
							mailService.sendSellNotificationEmail(new String[] { email }, null, sendDate, vendorName,
									buyerName, orderDate, details, String.valueOf(order.getOrderAmount()), recipient,
									telPhone, mobile, address, orderNo, mail);
						} catch (Exception e) {
							LOG.error("e", e);
						}
					}
					// 寄給買家
					boolean mail = false;
					List<Map<String, String>> details = new ArrayList<>();
					for (SubOrder o : orderList) {
						List<SubOrderDetail> odList = o.getSubOrderDetailList();
						if (odList != null) {
							for (SubOrderDetail od : odList) {
								Map<String, String> map = new HashMap<>();
								map.put("name", od.getProductName());
								map.put("price", String.valueOf(od.getUnitPrice()));
								map.put("quantity", String.valueOf(od.getQuantity()));
								map.put("total", String.valueOf(od.getTotalAmount()));

								details.add(map);
								if (od.getProduct().getProps() != null) {
									mail = true;
								}
							}
						}
					}
					String total = String.valueOf(mainOrder.getOrderAmount());
					String email = StringUtils.EMPTY;
					if (userInfoRepository.exists(mainOrder.getPurchaserId())) {
						UserInfo userInfo = userInfoRepository.findOne(mainOrder.getPurchaserId());
						email = userInfo.getEmail();
					} else {
						email = mainOrder.getPurchaserId();
					}
					try {
						mailService.sendShopSuccessNotificationEmail(new String[] { email }, null, sendDate, buyerName,
								orderNo, details, total, recipient, telPhone, mobile, address, mail);
					} catch (Exception e) {
						LOG.error("e", e);
					}
					SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
					// 發送通知
					String message = templateService.getShopSuccessNotification(configService.getHost(),
							configService.getImageUrl(), sdf.format(sendDate), buyerName, configService.getMyOrderUrl(),
							orderNo, details, total, recipient, telPhone, mobile, address,
							configService.getServiceUrl(), mail);
					String title = String.format("Wikidue Store 教育商城 - 訂單成立通知信(訂單編號 %s)", orderNo);
					sendNotification(message, mainOrder.getCreateId(), title);

					int ratioTotal = 0;
					details = new ArrayList<>();
					List<Map<String, String>> vendorList = new ArrayList<>();
					for (SubOrder o : orderList) {
						Vendor vendor = o.getVendor();
						String vendorName = StringUtils.isEmpty(vendor.getVendorName()) ? StringUtils.EMPTY
								: vendor.getVendorName();
						if (vendor != null) {
							Map<String, String> vendorMap = new HashMap<>();
							vendorMap.put("name", vendorName);
							vendorMap.put("code", StringUtils.isEmpty(vendor.getVendorCode()) ? StringUtils.EMPTY
									: vendor.getVendorCode());
							vendorMap.put("companyNo",
									StringUtils.isEmpty(vendor.getTaxId()) ? StringUtils.EMPTY : vendor.getTaxId());
							vendorMap.put("telPhone", StringUtils.isEmpty(vendor.getTelphone()) ? StringUtils.EMPTY
									: vendor.getTelphone());
							vendorList.add(vendorMap);
						}
						o.getSubOrderDetailList().size();
						for (SubOrderDetail sod : o.getSubOrderDetailList()) {
							Map<String, String> detail = new HashMap<>();
							Product product = sod.getProduct();
							Props props = product.getProps();
							Course course = product.getCourse();
							String materialNum = StringUtils.EMPTY;
							int ratio = 0;
							if (props != null) {
								materialNum = props.getMaterialNum();
								ratio = sod.getPropsSplitRatio();
							} else if (course != null) {
								materialNum = course.getMaterialNum();
								ratio = sod.getCourseSplitRatio();
							}
							int multiplier = 1;
							double amount = sod.getUnitPrice() * ratio;
							amount = amount / 100;
							int ratioUnitAmount = (int) Math.round(amount);
							ratioUnitAmount = ratioUnitAmount * multiplier;
							int ratioAmount = sod.getQuantity() * ratioUnitAmount;
							detail.put("name", StringUtils.isEmpty(sod.getProductName()) ? StringUtils.EMPTY
									: sod.getProductName());
							detail.put("materialNum",
									StringUtils.isEmpty(materialNum) ? StringUtils.EMPTY : materialNum);
							detail.put("vendorName", vendorName);
							detail.put("price", String.valueOf(sod.getUnitPrice()));
							detail.put("quantity", String.valueOf(sod.getQuantity()));
							detail.put("total", String.valueOf(sod.getTotalAmount()));
							detail.put("ratio", String.valueOf(ratio));
							detail.put("ratioUnitPrice", String.valueOf(ratioUnitAmount));
							detail.put("ratioAmount", String.valueOf(ratioAmount));
							details.add(detail);
							ratioTotal += ratioAmount;
						}

					}
					String invoiceNo = payment.getInvoiceNo();

					PayWayEnum payWay = PayWayEnum.getByCode(payment.getPaymentType());
					String payWayType = payWay == null ? StringUtils.EMPTY : payWay.getName();
					List<String> adminEmailList = commonService.getAdminEmailList();
					mailService.sendSellNotificationAdminEmail(
							adminEmailList.toArray(new String[adminEmailList.size()]), null, sendDate, buyerName,
							orderDate, orderNo, invoiceNo, payWayType, total, String.valueOf(ratioTotal), recipient,
							telPhone, mobile, address, details, vendorList, mail);

				}
			}
		}
		return result;
	}

	private void sendNotification(String message, String createId, String title) {
		try {
			E7Message e7Message = new E7Message();
			e7Message.setActive(Constant.TRUE);
			e7Message.setContent(message);
			e7Message.setCreateDt(new Date());
			e7Message.setCreateId(Constant.STYSTEM);
			e7Message.setOwnerID(createId);
			e7Message.setIsRead(Constant.FLASE);
			e7Message.setTitle(title);
			MsgCategory msgCategory = new MsgCategory();
			msgCategory.setPkMsgCategory(1);
			e7Message.setMsgCategory(msgCategory);
			e7MessageRepository.save(e7Message);
		} catch (Exception e) {
			LOG.error("sendNotification:", e);
		}

	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void setPaymentProcessing(Map<String, Object> paymentResult) throws ServiceException {
		if ("1".equals(paymentResult.get("RtnCode"))) {
			if (paymentRepository.exists(String.valueOf(paymentResult.get("MerchantTradeNo")))) {
				paymentRepository.updatePaymentStatus(String.valueOf(paymentResult.get("MerchantTradeNo")),
						OrderStatusEnum.N.name(), OrderStatusEnum.W.name());
				return;
			} else {
				throw new ServiceException("交易失敗");
			}
		}
		throw new ServiceException("付款失敗");
	}

	@Transactional(readOnly = true)
	public PaymentResultVo checkPaymentResult(PaymentResultVo paymentResultVo) throws ServiceException {
		if (paymentResultVo.getMerchantTradeNo() == null)
			return null;
		if (paymentRepository.exists(paymentResultVo.getMerchantTradeNo())) {
			Payment payment = paymentRepository.findOne(paymentResultVo.getMerchantTradeNo());
			if (OrderStatusEnum.W.name().equals(payment.getStatus())) {
				return null;
			} else {
				// paymentResultVo.setMerchantID();
				// paymentResultVo.setSimulatePaid();
				// paymentResultVo.setTradeDate();

				paymentResultVo.setPaymentTypeChargeFee(String.valueOf(payment.getChargeFee()));
				paymentResultVo.setCheckMacValue(payment.getCheckMacValue());
				paymentResultVo.setPaymentDate(payment.getPaymentDt() != null
						? DateTimeFormat.forPattern("yyyy/MM/dd HH:mm:ss").print(payment.getPaymentDt().getTime())
						: null);
				paymentResultVo.setPaymentType(payment.getPaymentType());
				paymentResultVo.setRtnCode(String.valueOf(payment.getReturnCode()));
				paymentResultVo.setRtnMsg(payment.getReturnMessage());
				paymentResultVo.setTradeAmt(String.valueOf(payment.getTradeAmt()));
				paymentResultVo.setTradeNo(payment.getTradeNo());
				return paymentResultVo;
			}
		} else {
			throw new ServiceException("交易失敗");
		}
	}

	public static void main(String[] addd) {
		System.out.println("00".getBytes().length);
	}
}
