package com.e7learning.payment.vo;

import java.io.Serializable;

public class PaymentResultVo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String MerchantID;

	private String MerchantTradeNo;

	private String PaymentDate;

	private String PaymentType;

	private String PaymentTypeChargeFee;

	private String RtnCode;

	private String RtnMsg;

	private String SimulatePaid;

	private String TradeAmt;

	private String TradeDate;

	private String TradeNo;

	private String CheckMacValue;

	public String getMerchantID() {
		return MerchantID;
	}

	public void setMerchantID(String merchantID) {
		MerchantID = merchantID;
	}

	public String getMerchantTradeNo() {
		return MerchantTradeNo;
	}

	public void setMerchantTradeNo(String merchantTradeNo) {
		MerchantTradeNo = merchantTradeNo;
	}

	public String getPaymentDate() {
		return PaymentDate;
	}

	public void setPaymentDate(String paymentDate) {
		PaymentDate = paymentDate;
	}

	public String getPaymentType() {
		return PaymentType;
	}

	public void setPaymentType(String paymentType) {
		PaymentType = paymentType;
	}

	public String getPaymentTypeChargeFee() {
		return PaymentTypeChargeFee;
	}

	public void setPaymentTypeChargeFee(String paymentTypeChargeFee) {
		PaymentTypeChargeFee = paymentTypeChargeFee;
	}

	public String getRtnCode() {
		return RtnCode;
	}

	public void setRtnCode(String rtnCode) {
		RtnCode = rtnCode;
	}

	public String getRtnMsg() {
		return RtnMsg;
	}

	public void setRtnMsg(String rtnMsg) {
		RtnMsg = rtnMsg;
	}

	public String getSimulatePaid() {
		return SimulatePaid;
	}

	public void setSimulatePaid(String simulatePaid) {
		SimulatePaid = simulatePaid;
	}

	public String getTradeAmt() {
		return TradeAmt;
	}

	public void setTradeAmt(String tradeAmt) {
		TradeAmt = tradeAmt;
	}

	public String getTradeDate() {
		return TradeDate;
	}

	public void setTradeDate(String tradeDate) {
		TradeDate = tradeDate;
	}

	public String getTradeNo() {
		return TradeNo;
	}

	public void setTradeNo(String tradeNo) {
		TradeNo = tradeNo;
	}

	public String getCheckMacValue() {
		return CheckMacValue;
	}

	public void setCheckMacValue(String checkMacValue) {
		CheckMacValue = checkMacValue;
	}

}
