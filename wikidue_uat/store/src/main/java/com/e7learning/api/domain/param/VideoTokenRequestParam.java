package com.e7learning.api.domain.param;

import java.io.Serializable;

public class VideoTokenRequestParam implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String videoId;

	public String getVideoId() {
		return videoId;
	}

	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}
	
	
	
}
