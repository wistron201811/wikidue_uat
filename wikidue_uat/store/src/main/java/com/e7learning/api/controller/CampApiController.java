package com.e7learning.api.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.e7learning.api.annotation.E7ApiController;
import com.e7learning.api.domain.RestResult;
import com.e7learning.api.domain.param.CampQueryParam;
import com.e7learning.api.domain.result.CampQueryResult;
import com.e7learning.api.service.CampApiService;
import com.e7learning.common.exception.RestException;

/**
 * @author ChrisTsai
 *
 */
@E7ApiController
public class CampApiController {

	@Autowired
	private CampApiService campApiService;

	@RequestMapping(value = "/camp/query", method = { RequestMethod.POST })
	public RestResult<Page<CampQueryResult>> queryCamp(@RequestBody CampQueryParam param) throws RestException {
		return new RestResult<Page<CampQueryResult>>(true, campApiService.queryCamp(param));
	}

	@RequestMapping(value = "/camp/list", method = { RequestMethod.POST })
	public RestResult<Map<String, List<CampQueryResult>>> queryAll(@RequestBody CampQueryParam param)
			throws RestException {
		return new RestResult<Map<String, List<CampQueryResult>>>(true, campApiService.queryAllByCategory());
	}
}
