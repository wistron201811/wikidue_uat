package com.e7learning.api.domain.param;

import java.io.Serializable;

import com.e7learning.common.enums.CategoryTypeEnum;

public class TagQueryParam implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String[] tag;
	private String level;
	private CategoryTypeEnum type;

	public String[] getTag() {
		return tag;
	}

	public void setTag(String[] tag) {
		this.tag = tag;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public CategoryTypeEnum getType() {
		return type;
	}

	public void setType(CategoryTypeEnum type) {
		this.type = type;
	}
	
	

}
