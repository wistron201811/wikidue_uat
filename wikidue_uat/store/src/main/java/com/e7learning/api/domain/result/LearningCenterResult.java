package com.e7learning.api.domain.result;

import java.io.Serializable;

public class LearningCenterResult implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer courseId;

	private String title;
	
	private String image;

	public Integer getCourseId() {
		return courseId;
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}
