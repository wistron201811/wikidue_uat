package com.e7learning.api.domain.param;

import com.e7learning.common.enums.CategoryTypeEnum;
import com.e7learning.enums.ProductSortEnum;

public class FavouriteQueryParam {

	private CategoryTypeEnum type;

	private String keyword;

	private ProductSortEnum order;

	public CategoryTypeEnum getType() {
		return type;
	}

	public void setType(CategoryTypeEnum type) {
		this.type = type;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public ProductSortEnum getOrder() {
		return order;
	}

	public void setOrder(ProductSortEnum order) {
		this.order = order;
	}

	

}
