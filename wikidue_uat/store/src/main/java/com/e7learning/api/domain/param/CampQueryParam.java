package com.e7learning.api.domain.param;

import com.e7learning.enums.ProductSortEnum;

public class CampQueryParam {

	private String keyword;

	private ProductSortEnum order1 = ProductSortEnum.PUBLISH_START_DATE_DESC;

	private ProductSortEnum order2;
	
	private String city;

	private Integer page = 1;
	
	
	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public ProductSortEnum getOrder1() {
		return order1;
	}

	public void setOrder1(ProductSortEnum order1) {
		this.order1 = order1;
	}

	public ProductSortEnum getOrder2() {
		return order2;
	}

	public void setOrder2(ProductSortEnum order2) {
		this.order2 = order2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	

}
