package com.e7learning.api.domain.result;

public class FavouriteRequestResult2 extends FavouriteRequestResult {

	private int count = 0;

	public FavouriteRequestResult2() {
	}

	public FavouriteRequestResult2(boolean result) {
		this.setResult(result);
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
