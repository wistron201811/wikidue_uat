package com.e7learning.api.domain.param;

import java.io.Serializable;

public class CartQueryParam implements Serializable {
	
	private String token;

	/** 是否查詢庫存 */
	private boolean queryStock = false;

	public boolean isQueryStock() {
		return queryStock;
	}

	public void setQueryStock(boolean queryStock) {
		this.queryStock = queryStock;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	
}
