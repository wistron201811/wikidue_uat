package com.e7learning.api.domain.result;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class CartResult {

	private Integer pkCart;

	@JsonFormat(pattern="yyyy/MM/dd", timezone = "GMT+8")
	private Date createDt;

	private String createId;

	private String lastModifiedId;

	private Integer listPrice;

	private String fkProduct;

	private String productName;
	
	private String productImage;

	private String purchaser;

	private Integer quantity;
	
	/** 庫存 -1:無數量限制,0:已售完 */
	private Integer stock;

	private String status;

	private Integer unitPrice;
	
	private String type;

	@JsonFormat(pattern="yyyy/MM/dd", timezone = "GMT+8")
	private Date updateDt;

	public Integer getPkCart() {
		return pkCart;
	}

	public void setPkCart(Integer pkCart) {
		this.pkCart = pkCart;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}

	public Integer getListPrice() {
		return listPrice;
	}

	public void setListPrice(Integer listPrice) {
		this.listPrice = listPrice;
	}

	public String getFkProduct() {
		return fkProduct;
	}

	public void setFkProduct(String fkProduct) {
		this.fkProduct = fkProduct;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPurchaser() {
		return purchaser;
	}

	public void setPurchaser(String purchaser) {
		this.purchaser = purchaser;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Integer unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Date getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public String getProductImage() {
		return productImage;
	}

	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
