package com.e7learning.api.domain.result;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.e7learning.common.Constant;
import com.e7learning.repository.model.E7Message;
import com.fasterxml.jackson.annotation.JsonFormat;

public class MessageQueryResult implements Serializable {

	private String messageId;
	private String categoryName;
	private String title;
	private String content;
	@JsonFormat(pattern="yyyy/MM/dd", timezone = "GMT+8")
	private Date createDt;
	private boolean read;

	public MessageQueryResult() {
	}

	public MessageQueryResult(E7Message msg) {
		this.messageId = msg.getPkMessage();
		this.title = msg.getTitle();
		this.categoryName = msg.getMsgCategory().getName();
		this.content = msg.getContent();
		this.read = StringUtils.equals(msg.getIsRead(), Constant.TRUE);
		this.createDt = msg.getCreateDt();
		
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public boolean isRead() {
		return read;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}
	
	

}
