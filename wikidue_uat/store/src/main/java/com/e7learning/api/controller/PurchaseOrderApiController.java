package com.e7learning.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.e7learning.api.annotation.E7ApiController;
import com.e7learning.api.domain.RestResult;
import com.e7learning.api.domain.param.PoQueryParam;
import com.e7learning.api.domain.result.PoQueryResult;
import com.e7learning.api.service.PurchaseOrderService;
import com.e7learning.common.exception.RestException;

@E7ApiController
public class PurchaseOrderApiController {

	@Autowired
	private PurchaseOrderService purchaseOrderService;
	
	@RequestMapping(value = "/myorder/query")
	public RestResult<Page<PoQueryResult>> queryMainOrder(@RequestBody PoQueryParam param) throws RestException {
		return new RestResult<>(true, purchaseOrderService.queryMainOrderList(param));
	}
	
	@RequestMapping(value = "/myorder/detail/query")
	public RestResult<PoQueryResult> queryOrderDetail(@RequestBody PoQueryParam param) throws RestException {
		return new RestResult<>(true, purchaseOrderService.queryOrderDetail(param));
	}
}
