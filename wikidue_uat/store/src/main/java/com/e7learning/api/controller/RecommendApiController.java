package com.e7learning.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.e7learning.api.annotation.E7ApiController;
import com.e7learning.api.domain.RestResult;
import com.e7learning.api.domain.param.RecommendQueryParam;
import com.e7learning.api.domain.result.RecommendQueryResult;
import com.e7learning.api.service.RecommendApiService;
import com.e7learning.common.exception.RestException;

/**
 * @author ChrisTsai
 *
 */
@E7ApiController
public class RecommendApiController {

	@Autowired
	private RecommendApiService recommendApiService;

	@RequestMapping(value = "/recommend/query")
	public RestResult<Page<RecommendQueryResult>> queryPo(@RequestBody RecommendQueryParam param) throws RestException {
		return new RestResult<>(true, recommendApiService.queryRecommend(param));
	}
}
