package com.e7learning.api.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.e7learning.api.domain.param.CategoryQueryParam;
import com.e7learning.api.domain.result.CategoryResult;
import com.e7learning.common.Constant;
import com.e7learning.common.exception.RestException;
import com.e7learning.repository.CategoryRepository;
import com.e7learning.repository.model.Category;

@Service
public class CategoryApiService {

	private static final Logger log = Logger.getLogger(CategoryApiService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private CategoryRepository categoryRepository;

	public List<CategoryResult> queryCategory(CategoryQueryParam param) throws RestException {
		List<CategoryResult> result = new ArrayList<>();

		try {
			List<Category> categorys = categoryRepository
					.findByCategoryTypeAndActiveOrderByCreateDtDesc(param.getCategoryType(), Constant.TRUE);
			categorys.stream().forEach(v -> result.add(toCategoryResult(v)));
		} catch (Exception e) {
			log.error("queryCategory", e);
			throw new RestException(resources.getMessage("category.query.err", null, null));
		}

		return result;
	}

	private CategoryResult toCategoryResult(Category category) {
		CategoryResult result = new CategoryResult();
		result.setCategoryCode(category.getCategoryCode());
		result.setCategoryId(category.getPkCategory());
		result.setCategoryName(category.getCategoryName());
		result.setCategoryType(category.getCategoryType());
		return result;
	}

}
