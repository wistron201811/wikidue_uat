package com.e7learning.api.domain.result;

import java.io.Serializable;

public class AdQueryResult implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String adsName;
	private String image1;
	private String image2;
	private String externalLink;
	private String desc;

	public String getAdsName() {
		return adsName;
	}

	public void setAdsName(String adsName) {
		this.adsName = adsName;
	}

	public String getImage1() {
		return image1;
	}

	public void setImage1(String image1) {
		this.image1 = image1;
	}

	public String getImage2() {
		return image2;
	}

	public void setImage2(String image2) {
		this.image2 = image2;
	}

	public String getExternalLink() {
		return externalLink;
	}

	public void setExternalLink(String externalLink) {
		this.externalLink = externalLink;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
