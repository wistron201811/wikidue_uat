package com.e7learning.api.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.e7learning.api.domain.param.ProductQueryParam;
import com.e7learning.api.domain.result.ProductResult;
import com.e7learning.common.Constant;
import com.e7learning.common.enums.CategoryTypeEnum;
import com.e7learning.common.exception.RestException;
import com.e7learning.common.service.ConfigService;
import com.e7learning.repository.ProductRepository;
import com.e7learning.repository.model.Product;
import com.google.gson.Gson;

@Service
public class ProductApiService {

	private static final Logger log = Logger.getLogger(ProductApiService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ConfigService configService;

	public Page<ProductResult> queryProduct(ProductQueryParam param) throws RestException {
		try {
			log.info(new Gson().toJson(param));

			Sort sort = genSort(param);
			int pageSize;
			if (param.getPageSize() != null) {
				pageSize = param.getPageSize();
			} else {
				pageSize = configService.getPageSize();
			}
			Pageable pageable = new PageRequest(param.getPage() - 1, pageSize, sort);

			Page<Product> products = productRepository
					.findAll((Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
						List<Predicate> predicates = new ArrayList<>();
						query.distinct(true);
						// 類別
						if (param.getType() != null) {
							switch (param.getType()) {
							case COURSE:
								predicates.add(cb.isNotNull(root.get("course")));
								// 關鍵字查詢
								if (StringUtils.isNotBlank(param.getKeyword())) {
									predicates
											.add(cb.or(cb.like(root.get("productName"), "%" + param.getKeyword() + "%"),
													cb.like(root.get("course").get("courseSummary"),
															"%" + param.getKeyword() + "%")));
								}

								break;
							case PROPS:
								predicates.add(cb.isNotNull(root.get("props")));
								// 關鍵字查詢
								if (StringUtils.isNotBlank(param.getKeyword())) {
									predicates
											.add(cb.or(cb.like(root.get("productName"), "%" + param.getKeyword() + "%"),
													cb.like(root.get("props").get("propsSummary"),
															"%" + param.getKeyword() + "%")));
								}
								break;
							case CAMP:
								predicates.add(cb.isNotNull(root.get("camp")));
								break;
							default:
								break;
							}
						}

						if (param.getTags() != null && param.getTags().length > 0) {
							predicates.add(cb.and(root.join("productTags").get("tag").in(param.getTags())));
						}

						// 已上架
						predicates.add(cb.equal(root.get("active"), Constant.TRUE));
						predicates.add(cb.lessThanOrEqualTo(root.get("publishSDate"), new Date()));
						predicates.add(cb.greaterThanOrEqualTo(root.get("publishEDate"), new Date()));

						if (!predicates.isEmpty()) {
							return cb.and(predicates.toArray(new Predicate[predicates.size()]));
						}

						return cb.conjunction();
					}, pageable);
			log.info("getTotalPages:" + products.getTotalPages());
			log.info("getNumber:" + products.getNumber());
			log.info("getNumberOfElements:" + products.getNumberOfElements());
			log.info("getSize:" + products.getSize());
			log.info("getTotalElements:" + products.getTotalElements());
			return products.map(v -> toProductResult(v, param.getType()));
		} catch (Exception e) {
			log.error("queryTag", e);
			throw new RestException(resources.getMessage("product.query.err", null, null));
		}
	}

	private ProductResult toProductResult(Product product, CategoryTypeEnum type) {
		ProductResult productResult = new ProductResult();
		switch (type) {
		case COURSE:
			productResult.setPkProduct(product.getPkProduct());
			productResult.setDiscountPrice(product.getDiscountPrice());
			productResult.setPrice(product.getPrice());
			productResult.setTitle(product.getProductName());
			productResult.setSummary(product.getCourse().getCourseSummary());
			productResult.setImage(
					configService.getHost() + configService.getImageApi() + product.getCourse().getImageOne());
			productResult.setQuantity(-1);
			break;
		case PROPS:

			productResult.setPkProduct(product.getPkProduct());
			productResult.setDiscountPrice(product.getDiscountPrice());
			productResult.setPrice(product.getPrice());
			productResult.setTitle(product.getProductName());
			productResult.setSummary(product.getProps().getPropsSummary());
			if (StringUtils.isNotBlank(product.getProps().getImageOneId()))
				productResult.setImage(
						configService.getHost() + configService.getImageApi() + product.getProps().getImageOneId());
			productResult.setQuantity(product.getProps().getQuantity());
			break;
		case CAMP:
			break;
		default:
			break;
		}

		return productResult;
	}

	private Sort genSort(ProductQueryParam param) {

		Sort sort = null;
		if (param.getOrder1() != null) {
			switch (param.getOrder1()) {
			case PUBLISH_START_DATE_DESC:
				sort = new Sort(Direction.DESC, "publishSDate");
				break;
			case PUBLISH_START_DATE_ASC:
				sort = new Sort(Direction.ASC, "publishSDate");
				break;
			case PRICE_DESC:
				sort = new Sort(Direction.DESC, "discountPrice");
				break;
			case PRICE_ASC:
				sort = new Sort(Direction.ASC, "discountPrice");
				break;
			case WEIGHTS_DESC:
				sort = new Sort(Direction.DESC, "weights");
				break;

			default:
				break;
			}
		}

		if (param.getOrder2() != null) {
			switch (param.getOrder2()) {
			case PUBLISH_START_DATE_DESC:
				if (sort == null)
					sort = new Sort(Direction.DESC, "publishSDate");
				else
					sort.and(new Sort(Direction.DESC, "publishSDate"));
				break;
			case PUBLISH_START_DATE_ASC:
				if (sort == null)
					sort = new Sort(Direction.ASC, "publishSDate");
				else
					sort.and(new Sort(Direction.ASC, "publishSDate"));
				break;
			case PRICE_DESC:
				if (sort == null)
					sort = new Sort(Direction.DESC, "discountPrice");
				else
					sort.and(new Sort(Direction.DESC, "discountPrice"));
				break;
			case PRICE_ASC:
				if (sort == null)
					sort = new Sort(Direction.ASC, "discountPrice");
				else
					sort.and(new Sort(Direction.ASC, "discountPrice"));
				break;

			default:
				break;
			}
		}
		if (sort == null)
			return new Sort(Direction.DESC, "publishSDate");
		return sort;
	}

}
