package com.e7learning.api.domain.result;

public class MessageCountResult {
	
	private Integer unreadCount = 0;
	
	private Integer totalCount = 0;

	public Integer getUnreadCount() {
		return unreadCount;
	}

	public void setUnreadCount(Integer unreadCount) {
		this.unreadCount = unreadCount;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	
	

}
