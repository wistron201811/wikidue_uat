package com.e7learning.api.domain.param;

public class CartRemoveParam {
	
	private String token;
	
	private Integer cartId;

	public Integer getCartId() {
		return cartId;
	}

	public void setCartId(Integer cartId) {
		this.cartId = cartId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	

}
