package com.e7learning.api.domain.param;

public class AccusationRequestParam {

	private Integer shareId;

	private String reportReason;

	public Integer getShareId() {
		return shareId;
	}

	public void setShareId(Integer shareId) {
		this.shareId = shareId;
	}

	public String getReportReason() {
		return reportReason;
	}

	public void setReportReason(String reportReason) {
		this.reportReason = reportReason;
	}
	
	
}
