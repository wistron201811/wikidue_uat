package com.e7learning.api.domain.result;

import java.io.Serializable;

public class InvoiceResult implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/*
	 * 發票號碼
	 * 
	 * */
	private String invoiceNumber;
	
	/*
	 * 抬頭
	 * 
	 * */
	private String customerName;
	/*
	 * 統編
	 * 
	 * */
	private String customerId;
	
	/*
	 * 金額
	 * 
	 * */
	private Integer salesAmount;
	
	/*
	 * 載具類別
	 * 1:綠界科技電子發票載具
	 * 2:自然人憑證
	 * 3:手機條碼
	 * 空:無載具
	 * 
	 * */
	private String carruerType;
	
	/*
	 * 載具類別描述
	 * 
	 * */
	private String carruerTypeDesc;
	
	/*
	 * 載具編號
	 * 
	 * */
	private String carruerNumber;
	
	/*
	 * 發票開立時間
	 * 
	 * */
	private String createDtText;
	
	/*
	 * 發票隨機碼
	 * 
	 * */
	private String randomNumber;

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public Integer getSalesAmount() {
		return salesAmount;
	}

	public void setSalesAmount(Integer salesAmount) {
		this.salesAmount = salesAmount;
	}

	public String getCarruerType() {
		return carruerType;
	}

	public void setCarruerType(String carruerType) {
		this.carruerType = carruerType;
	}

	public String getCarruerTypeDesc() {
		return carruerTypeDesc;
	}

	public void setCarruerTypeDesc(String carruerTypeDesc) {
		this.carruerTypeDesc = carruerTypeDesc;
	}

	public String getCarruerNumber() {
		return carruerNumber;
	}

	public void setCarruerNumber(String carruerNumber) {
		this.carruerNumber = carruerNumber;
	}

	public String getCreateDtText() {
		return createDtText;
	}

	public void setCreateDtText(String createDtText) {
		this.createDtText = createDtText;
	}

	public String getRandomNumber() {
		return randomNumber;
	}

	public void setRandomNumber(String randomNumber) {
		this.randomNumber = randomNumber;
	}

}
