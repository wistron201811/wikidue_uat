package com.e7learning.api.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.e7learning.api.annotation.E7ApiController;
import com.e7learning.api.domain.RestResult;
import com.e7learning.api.domain.param.VideoTokenRequestParam;
import com.e7learning.api.domain.result.VideoTokenResult;
import com.e7learning.api.service.VideoTokenApiService;
import com.e7learning.common.exception.RestException;

/**
 * @author ChrisTsai
 *
 */
@E7ApiController
public class VideoTokenApiController {

	@Autowired
	private VideoTokenApiService videoTokenApiService;

	@RequestMapping(value = "/video/token", method = { RequestMethod.POST })
	public RestResult<VideoTokenResult> queryTag(@RequestBody VideoTokenRequestParam param, HttpServletRequest request)
			throws RestException {
		return new RestResult<VideoTokenResult>(true, videoTokenApiService.genToken(param, request));
	}

	@RequestMapping(value = "/video/count", method = { RequestMethod.POST })
	public RestResult<Long> countVideoWatched(@RequestBody VideoTokenRequestParam param, HttpServletRequest request) {
		return new RestResult<Long>(true, videoTokenApiService.countVideoWatched(param));
	}
}
