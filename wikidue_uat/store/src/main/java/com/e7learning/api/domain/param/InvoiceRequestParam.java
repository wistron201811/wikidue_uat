package com.e7learning.api.domain.param;

public class InvoiceRequestParam {

	private String paymentId;

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

}
