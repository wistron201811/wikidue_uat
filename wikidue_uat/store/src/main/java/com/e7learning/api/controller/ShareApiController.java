package com.e7learning.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.e7learning.api.annotation.E7ApiController;
import com.e7learning.api.domain.RestResult;
import com.e7learning.api.domain.param.AccusationRequestParam;
import com.e7learning.api.domain.param.ShareQueryParam;
import com.e7learning.api.domain.result.ShareResult;
import com.e7learning.api.service.ShareApiService;
import com.e7learning.common.exception.RestException;

/**
 * @author ChrisTsai
 *
 */
@E7ApiController
public class ShareApiController {

	@Autowired
	private ShareApiService shareApiService;

	@RequestMapping(value = "/share/query", method = { RequestMethod.POST })
	public RestResult<Page<ShareResult>> queryTag(@RequestBody ShareQueryParam param) throws RestException {
		return new RestResult<Page<ShareResult>>(true, shareApiService.queryShareCenter(param));
	}

	@RequestMapping(value = "/share/accusation/check", method = { RequestMethod.POST })
	public RestResult<?> checkAccusation(@RequestBody AccusationRequestParam param) throws RestException {
		shareApiService.checkAccusation(param);
		return new RestResult<Page<ShareResult>>(true);
	}

	@RequestMapping(value = "/share/accusation/submit", method = { RequestMethod.POST })
	public RestResult<?> submitAccusation(@RequestBody AccusationRequestParam param) throws RestException {
		shareApiService.submitAccusation(param);
		return new RestResult<Page<ShareResult>>(true);
	}

}
