package com.e7learning.api.domain.param;

import com.e7learning.common.enums.CategoryTypeEnum;
import com.e7learning.enums.ProductSortEnum;

public class ProductQueryParam {

	private CategoryTypeEnum type;

	private String[] tags;

	private String keyword;

	private String difficulty;

	private ProductSortEnum order1 = ProductSortEnum.WEIGHTS_DESC;

	private ProductSortEnum order2 = ProductSortEnum.PUBLISH_START_DATE_DESC;

	private Integer page = 1;
	
	private Integer pageSize = null;

	public CategoryTypeEnum getType() {
		return type;
	}

	public void setType(CategoryTypeEnum type) {
		this.type = type;
	}

	public String[] getTags() {
		return tags;
	}

	public void setTags(String[] tags) {
		this.tags = tags;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public ProductSortEnum getOrder1() {
		return order1;
	}

	public void setOrder1(ProductSortEnum order1) {
		this.order1 = order1;
	}

	public ProductSortEnum getOrder2() {
		return order2;
	}

	public void setOrder2(ProductSortEnum order2) {
		this.order2 = order2;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

}
