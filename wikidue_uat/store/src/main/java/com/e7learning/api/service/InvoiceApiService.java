package com.e7learning.api.service;

import java.net.URLDecoder;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Service;

import com.e7learning.api.domain.result.InvoiceResult;
import com.e7learning.common.exception.RestException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.utils.ParamUtils;
import com.e7learning.invoice.integration.InvoiceAllInOne;
import com.e7learning.invoice.integration.domain.QueryIssueObj;
import com.e7learning.repository.PaymentRepository;
import com.e7learning.repository.model.Payment;

@Service
public class InvoiceApiService extends BaseService {

	private static final Logger LOG = Logger.getLogger(InvoiceApiService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private InvoiceAllInOne invoiceAllInOne;

	@Autowired
	private PaymentRepository paymentRepository;

	public InvoiceResult queryInvoiceDetail(String pkPayment) throws RestException {
		try {
			Payment payment = paymentRepository.findOne(pkPayment);
			if (payment != null) {
				QueryIssueObj obj = new QueryIssueObj();
				obj.setRelateNumber(payment.getRelateNumber());
				String resultText = invoiceAllInOne.queryIssue(obj);
				Map<String, String> resultMap = ParamUtils.solveParams(resultText);
				if (resultMap != null && StringUtils.equals(resultMap.get("RtnCode"), "1")) {
					InvoiceResult result = new InvoiceResult();
					result.setCarruerNumber(resultMap.get("IIS_Carruer_Num"));
					String carruerType = resultMap.get("IIS_Carruer_Type");
					if (!StringUtils.isEmpty(carruerType)) {
						result.setCarruerType(carruerType);
						String carruerTypeDesc = null;
						switch (carruerType) {
						case "1":
							carruerTypeDesc = "綠界科技電子發票載具";
							break;
						case "2":
							carruerTypeDesc = "自然人憑證";
							break;
						case "3":
							carruerTypeDesc = "手機條碼";
							break;
						}
						result.setCarruerTypeDesc(carruerTypeDesc);
					}
					if (!StringUtils.isEmpty(resultMap.get("IIS_Identifier"))
							&& !StringUtils.equals(resultMap.get("IIS_Identifier"), "0000000000")) {
						result.setCustomerId(resultMap.get("IIS_Identifier"));
					}
					if (!StringUtils.isEmpty(resultMap.get("IIS_Customer_Name"))) {
						String customerName = URLDecoder.decode(resultMap.get("IIS_Customer_Name"), "UTF-8");
						result.setCustomerName(customerName);
					}

					result.setInvoiceNumber(resultMap.get("IIS_Number"));
					result.setCreateDtText(resultMap.get("IIS_Create_Date"));
					result.setRandomNumber(resultMap.get("IIS_Random_Number"));
					try {
						result.setSalesAmount(Integer.parseInt(resultMap.get("IIS_Sales_Amount")));
					} catch (Exception e) {

					}
					return result;
				}
			}
		} catch (Exception e) {
			LOG.error("e", e);
			throw new RestException(resources.getMessage("category.query.err", null, null));
		}
		return null;
	}

}
