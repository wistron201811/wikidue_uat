package com.e7learning.api.domain.param;

public class RecommendQueryParam {
	
	private Integer page = 1;

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}
	
	

}
