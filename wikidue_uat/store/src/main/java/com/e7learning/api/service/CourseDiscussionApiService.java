package com.e7learning.api.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.api.domain.param.QuestionRequestParam;
import com.e7learning.api.domain.result.QuestionReplyResult;
import com.e7learning.api.domain.result.QuestionRequestResult;
import com.e7learning.common.Constant;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.RestException;
import com.e7learning.common.service.BaseService;
import com.e7learning.repository.CourseQuestionRepository;
import com.e7learning.repository.CourseRepository;
import com.e7learning.repository.QuestionReplyRepository;
import com.e7learning.repository.model.Course;
import com.e7learning.repository.model.CourseQuestion;
import com.e7learning.repository.model.QuestionReply;

@Service
public class CourseDiscussionApiService extends BaseService {

	private static final Logger log = Logger.getLogger(CourseDiscussionApiService.class);

	@Autowired
	private CourseQuestionRepository courseQuestionRepository;

	@Autowired
	private QuestionReplyRepository questionReplyRepository;

	@Autowired
	private CourseRepository courseRepository;

	@Autowired
	private MessageSource resources;

	@Transactional(rollbackFor = { RestException.class }, noRollbackFor = { MessageException.class })
	public void askQuestion(QuestionRequestParam param) throws RestException {

		if (courseRepository.exists(param.getCourseId())) {
			CourseQuestion courseQuestion = new CourseQuestion();
			courseQuestion.setActive(Constant.TRUE);
			courseQuestion.setCreateDt(new Date());
			courseQuestion.setUpdateDt(courseQuestion.getCreateDt());
			Course course = new Course();
			course.setPkCourse(param.getCourseId());
			courseQuestion.setCourse(course);
			courseQuestion.setCreateId(getCurrentUserId());
			courseQuestion.setUserName(getCurrentUser().getName());
			courseQuestion.setTopic(param.getTopic());
			courseQuestion.setContent(param.getContent());
			courseQuestionRepository.save(courseQuestion);
		} else {
			throw new RestException(resources.getMessage("ask.question.error", null, null));
		}

	}

	@Transactional(rollbackFor = { RestException.class }, noRollbackFor = { MessageException.class })
	public void replyQuestion(QuestionRequestParam param) throws RestException {

		if (courseQuestionRepository.exists(param.getQuestionId())) {
			QuestionReply questionReply = new QuestionReply();
			questionReply.setActive(Constant.TRUE);
			questionReply.setCreateDt(new Date());
			questionReply.setUpdateDt(questionReply.getCreateDt());
			CourseQuestion courseQuestion = new CourseQuestion();
			courseQuestion.setPkCourseQuestion(param.getQuestionId());
			questionReply.setCourseQuestion(courseQuestion);
			questionReply.setCreateId(getCurrentUserId());
			questionReply.setUserName(getCurrentUser().getName());
			questionReply.setContent(param.getContent());
			questionReplyRepository.save(questionReply);
		} else {
			throw new RestException(resources.getMessage("reply.question.error", null, null));
		}

	}

	@Transactional(readOnly = true)
	public List<QuestionRequestResult> queryQuestions(QuestionRequestParam param) throws RestException {
		if (courseRepository.exists(param.getCourseId())) {
			List<CourseQuestion> courseQuestions = courseQuestionRepository
					.findAll((Root<CourseQuestion> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
						List<Predicate> predicates = new ArrayList<>();
						if (StringUtils.isNotBlank(param.getSearchKey())) {
							predicates.add(cb.or(cb.like(root.get("topic"), "%" + param.getSearchKey() + "%"),
									cb.like(root.get("content"), "%" + param.getSearchKey() + "%")));
						}
						predicates.add(cb.equal(root.get("course").get("pkCourse"), param.getCourseId()));
						return cb.and(predicates.toArray(new Predicate[predicates.size()]));
					}, new Sort(Direction.DESC, "createDt"));
			// init lazy
			for (CourseQuestion courseQuestion : courseQuestions)
				courseQuestion.getQuestionReplies().size();
			return courseQuestions.stream().map(CourseDiscussionApiService::toQuestionRequestResult)
					.collect(Collectors.toList());
		} else {
			throw new RestException(resources.getMessage("query.question.error", null, null));
		}
	}

	private static QuestionRequestResult toQuestionRequestResult(CourseQuestion courseQuestion) {
		QuestionRequestResult questionRequestResult = new QuestionRequestResult();
		questionRequestResult.setContent(courseQuestion.getContent());
		questionRequestResult.setCourseTeacher(StringUtils
				.equalsIgnoreCase(courseQuestion.getCourse().getTeacher().getEmail(), courseQuestion.getCreateId()));
		questionRequestResult.setCreateDt(courseQuestion.getCreateDt());
		questionRequestResult.setCreateId(courseQuestion.getCreateId());
		questionRequestResult.setUserName(courseQuestion.getUserName());
		questionRequestResult.setCourseId(courseQuestion.getCourse().getPkCourse());

		questionRequestResult.setQuestionReplies(courseQuestion.getQuestionReplies().stream()
				.map(CourseDiscussionApiService::toQuestionReplyResult).collect(Collectors.toList()));
		questionRequestResult.setUpdateDt(courseQuestion.getUpdateDt());
		questionRequestResult.setQuestionId(courseQuestion.getPkCourseQuestion());
		questionRequestResult.setTopic(courseQuestion.getTopic());
		return questionRequestResult;
	}

	private static QuestionReplyResult toQuestionReplyResult(QuestionReply questionReply) {
		QuestionReplyResult questionReplyResult = new QuestionReplyResult();
		questionReplyResult.setContent(questionReply.getContent());
		questionReplyResult.setCreateDt(questionReply.getCreateDt());
		questionReplyResult.setCreateId(questionReply.getCreateId());
		questionReplyResult.setUserName(questionReply.getUserName());
		questionReplyResult.setCourseTeacher(StringUtils.equalsIgnoreCase(
				questionReply.getCourseQuestion().getCourse().getTeacher().getEmail(), questionReply.getCreateId()));
		questionReplyResult.setQuestionId(questionReply.getCourseQuestion().getPkCourseQuestion());
		questionReplyResult.setUpdateDt(questionReply.getUpdateDt());

		return questionReplyResult;
	}

}
