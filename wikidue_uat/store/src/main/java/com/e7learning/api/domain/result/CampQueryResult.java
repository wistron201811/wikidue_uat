package com.e7learning.api.domain.result;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class CampQueryResult {

	private Integer pkCamp;
	
	@JsonFormat(pattern="yyyy/MM/dd", timezone = "GMT+8")
	private Date createDt;
	
	private String campNum;

	private Integer discountPrice;

	private String campName;

	private Integer price;

	@JsonFormat(pattern="yyyy年MM月dd日", timezone = "GMT+8", locale = "zh")
	private Date activitySDate;

	@JsonFormat(pattern="yyyy年MM月dd日", timezone = "GMT+8", locale = "zh")
	private Date activityEDate;

	@JsonFormat(pattern="yyyy年MM月dd日", timezone = "GMT+8")
	private Date signUpStartDate;
	
	@JsonFormat(pattern="yyyy年MM月dd日", timezone = "GMT+8")
	private Date signUpEndDate;

	private String attendantTarget;

	private String organizer;

	private String image;

	private String activityCounty;

	private String externalLink;

	private String campIntroduction;

	private String categoryName;

	public Integer getPkCamp() {
		return pkCamp;
	}

	public void setPkCamp(Integer pkCamp) {
		this.pkCamp = pkCamp;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}
	
	public String getCampNum() {
		return campNum;
	}

	public void setCampNum(String campNum) {
		this.campNum = campNum;
	}

	public Integer getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Integer discountPrice) {
		this.discountPrice = discountPrice;
	}

	public String getCampName() {
		return campName;
	}

	public void setCampName(String campName) {
		this.campName = campName;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Date getActivitySDate() {
		return activitySDate;
	}

	public void setActivitySDate(Date activitySDate) {
		this.activitySDate = activitySDate;
	}

	public Date getActivityEDate() {
		return activityEDate;
	}

	public void setActivityEDate(Date activityEDate) {
		this.activityEDate = activityEDate;
	}

	public Date getSignUpEndDate() {
		return signUpEndDate;
	}

	public void setSignUpEndDate(Date signUpEndDate) {
		this.signUpEndDate = signUpEndDate;
	}
	
	public Date getSignUpStartDate() {
		return signUpStartDate;
	}

	public void setSignUpStartDate(Date signUpStartDate) {
		this.signUpStartDate = signUpStartDate;
	}

	public String getAttendantTarget() {
		return attendantTarget;
	}

	public void setAttendantTarget(String attendantTarget) {
		this.attendantTarget = attendantTarget;
	}

	public String getOrganizer() {
		return organizer;
	}

	public void setOrganizer(String organizer) {
		this.organizer = organizer;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getActivityCounty() {
		return activityCounty;
	}

	public void setActivityCounty(String activityCounty) {
		this.activityCounty = activityCounty;
	}

	public String getExternalLink() {
		return externalLink;
	}

	public void setExternalLink(String externalLink) {
		this.externalLink = externalLink;
	}

	public String getCampIntroduction() {
		return campIntroduction;
	}

	public void setCampIntroduction(String campIntroduction) {
		this.campIntroduction = campIntroduction;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
	
	
}
