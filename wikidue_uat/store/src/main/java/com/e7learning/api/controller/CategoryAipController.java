package com.e7learning.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.e7learning.api.annotation.E7ApiController;
import com.e7learning.api.domain.RestResult;
import com.e7learning.api.domain.param.CategoryQueryParam;
import com.e7learning.api.domain.result.CategoryResult;
import com.e7learning.api.service.CategoryApiService;
import com.e7learning.common.exception.RestException;

/**
 * @author ChrisTsai
 *
 */
@E7ApiController
public class CategoryAipController {
	
	@Autowired
	private CategoryApiService categoryService;
	
	@RequestMapping(value = "/category/query", method = { RequestMethod.POST })
	public RestResult<List<CategoryResult>> queryTag(@RequestBody CategoryQueryParam param) throws RestException {
		return new RestResult<List<CategoryResult>>(true, categoryService.queryCategory(param));
	}
}
