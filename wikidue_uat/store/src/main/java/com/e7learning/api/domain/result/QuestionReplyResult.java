package com.e7learning.api.domain.result;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class QuestionReplyResult {

	private Integer questionId;
	
	private String content;
	
	private boolean courseTeacher = false;
	
	private String createId;
	
	private String userName;
	
	@JsonFormat(pattern="yyyy年MM月dd日 HH:mm", timezone = "GMT+8")
	private Date createDt;
	
	@JsonFormat(pattern="yyyy年MM月dd日 HH:mm", timezone = "GMT+8")
	private Date updateDt;

	public Integer getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public boolean isCourseTeacher() {
		return courseTeacher;
	}

	public void setCourseTeacher(boolean courseTeacher) {
		this.courseTeacher = courseTeacher;
	}

	public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public Date getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
	
}
