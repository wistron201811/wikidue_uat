package com.e7learning.api.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.e7learning.api.domain.param.CampQueryParam;
import com.e7learning.api.domain.result.CampQueryResult;
import com.e7learning.common.Constant;
import com.e7learning.common.exception.RestException;
import com.e7learning.common.service.ConfigService;
import com.e7learning.repository.CampRepository;
import com.e7learning.repository.model.Camp;

@Service
public class CampApiService {

	private static final Logger log = Logger.getLogger(CampApiService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private CampRepository campRepository;

	@Autowired
	private ConfigService configService;

	public Map<String, List<CampQueryResult>> queryAllByCategory() throws RestException {
		try {
			Map<String, List<CampQueryResult>> groupResult = new TreeMap<>();

			Sort sort = new Sort(Direction.DESC, "activitySDate");
			campRepository.findAll((Root<Camp> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
				List<Predicate> predicates = new ArrayList<>();

				// 已上架
				predicates.add(cb.equal(root.get("active"), Constant.TRUE));
				// 報名截止日之前
				predicates.add(cb.greaterThanOrEqualTo(root.get("signUpEndDate"), new Date()));

				if (!predicates.isEmpty()) {
					return cb.and(predicates.toArray(new Predicate[predicates.size()]));
				}
				return cb.conjunction();
			}, sort).stream()
					.forEach(v -> groupResult.computeIfAbsent(v.getCategory().getCategoryName(), t -> new ArrayList<>())
							.add(toCampQueryResult(v, configService)));

			return groupResult;
		} catch (Exception e) {
			log.error("queryAllByCategory", e);
			throw new RestException(resources.getMessage("op.query.error", null, null));
		}
	}

	private static CampQueryResult toCampQueryResult(Camp camp, ConfigService configService) {
		CampQueryResult campQueryResult = new CampQueryResult();
		campQueryResult.setActivityCounty(camp.getActivityCounty());
		campQueryResult.setActivityEDate(camp.getActivityEDate());
		campQueryResult.setActivitySDate(camp.getActivitySDate());
		/** 招生對象 */
		campQueryResult.setAttendantTarget(camp.getAttendantTarget());
		campQueryResult.setCampIntroduction(camp.getCampIntroduction());
		campQueryResult.setCampName(camp.getCampName());
		campQueryResult.setCampNum(camp.getCampNum());
		campQueryResult.setCategoryName(camp.getCategory().getCategoryName());
		campQueryResult.setCreateDt(camp.getCreateDt());
		campQueryResult.setDiscountPrice(camp.getDiscountPrice());
		campQueryResult.setExternalLink(camp.getExternalLink());
		campQueryResult.setImage(configService.getHost() + configService.getImageApi() + camp.getImageId());
		campQueryResult.setOrganizer(camp.getOrganizer());
		campQueryResult.setPkCamp(camp.getPkCamp());
		campQueryResult.setPrice(camp.getPrice());
		campQueryResult.setSignUpStartDate(camp.getSignUpStartDate());
		campQueryResult.setSignUpEndDate(camp.getSignUpEndDate());

		return campQueryResult;
	}

	public Page<CampQueryResult> queryCamp(CampQueryParam param) throws RestException {
		try {
			Sort sort = genSort(param);
			Pageable pageable = new PageRequest(param.getPage() - 1, configService.getPageSize(), sort);

			return campRepository.findAll((Root<Camp> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
				List<Predicate> predicates = new ArrayList<>();

				// 關鍵字
				if (StringUtils.isNotBlank(param.getKeyword())) {
					predicates.add(cb.or(cb.like(root.get("campName"), "%" + param.getKeyword() + "%"),
							cb.like(root.get("campIntroduction"), "%" + param.getKeyword() + "%")));
				}
				// 縣市
				if (StringUtils.isNotBlank(param.getCity())) {
					predicates.add(cb.equal(root.get("activityCounty"), param.getCity()));
				}
				
				// 已上架
				predicates.add(cb.equal(root.get("active"), Constant.TRUE));
				// 報名截止日之前
				predicates.add(cb.greaterThanOrEqualTo(root.get("signUpEndDate"), new Date()));

				if (!predicates.isEmpty()) {
					return cb.and(predicates.toArray(new Predicate[predicates.size()]));
				}

				return cb.conjunction();
			}, pageable).map(v -> toCampQueryResult(v, configService));
		} catch (Exception e) {
			log.error("queryCamp", e);
			throw new RestException(resources.getMessage("op.query.error", null, null));
		}
	}

	private Sort genSort(CampQueryParam param) {

		Sort sort = null;
		if (param.getOrder1() != null) {
			switch (param.getOrder1()) {
			case PUBLISH_START_DATE_DESC:
				sort = new Sort(Direction.DESC, "activitySDate");
				break;
			case PUBLISH_START_DATE_ASC:
				sort = new Sort(Direction.ASC, "activitySDate");
				break;
			case PRICE_DESC:
				sort = new Sort(Direction.DESC, "discountPrice");
				break;
			case PRICE_ASC:
				sort = new Sort(Direction.ASC, "discountPrice");
				break;

			default:
				break;
			}
		}

		if (param.getOrder2() != null) {
			switch (param.getOrder2()) {
			case PUBLISH_START_DATE_DESC:
				if (sort == null)
					sort = new Sort(Direction.DESC, "activitySDate");
				else
					sort.and(new Sort(Direction.DESC, "activitySDate"));
				break;
			case PUBLISH_START_DATE_ASC:
				if (sort == null)
					sort = new Sort(Direction.ASC, "activitySDate");
				else
					sort.and(new Sort(Direction.ASC, "activitySDate"));
				break;
			case PRICE_DESC:
				if (sort == null)
					sort = new Sort(Direction.DESC, "discountPrice");
				else
					sort.and(new Sort(Direction.DESC, "discountPrice"));
				break;
			case PRICE_ASC:
				if (sort == null)
					sort = new Sort(Direction.ASC, "discountPrice");
				else
					sort.and(new Sort(Direction.ASC, "discountPrice"));
				break;

			default:
				break;
			}
		}
		if (sort == null)
			return new Sort(Direction.DESC, "activitySDate");
		return sort;
	}

}
