package com.e7learning.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.e7learning.api.annotation.E7ApiController;
import com.e7learning.api.domain.RestResult;
import com.e7learning.api.domain.param.TagQueryParam;
import com.e7learning.api.domain.result.KnowledgeMapResult;
import com.e7learning.api.service.KnowledgeMapApiService;
import com.e7learning.common.exception.RestException;

/**
 * @author ChrisTsai
 *
 */
@E7ApiController
public class KnowledgeMapApiController {

	@Autowired
	private KnowledgeMapApiService knowledgeMapService;

	/**
	 * Query tag.
	 *
	 * @param param the param
	 * @return the rest result
	 * @throws RestException the rest exception
	 */
	@RequestMapping(value = "/tag/query", method = { RequestMethod.POST })
	public RestResult<List<KnowledgeMapResult>> queryTag(@RequestBody TagQueryParam param) throws RestException {
		return new RestResult<List<KnowledgeMapResult>>(true, knowledgeMapService.queryTag(param));
	}
	
	/**
	 * Query product tag.
	 *
	 * @param param the param
	 * @return the rest result
	 * @throws RestException the rest exception
	 */
	@RequestMapping(value = "/product/tag/query", method = { RequestMethod.POST })
	public RestResult<List<KnowledgeMapResult>> queryProductTag(@RequestBody TagQueryParam param) throws RestException {
		return new RestResult<List<KnowledgeMapResult>>(true, knowledgeMapService.queryProductTag(param));
	}
	
	@RequestMapping(value = "/share/tag/query", method = { RequestMethod.POST })
	public RestResult<List<KnowledgeMapResult>> queryShareTag(@RequestBody TagQueryParam param) throws RestException {
		return new RestResult<List<KnowledgeMapResult>>(true, knowledgeMapService.queryShareTag(param));
	}
}
