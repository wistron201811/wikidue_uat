package com.e7learning.api.service;

import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.api.domain.param.VideoTokenRequestParam;
import com.e7learning.api.domain.result.VideoTokenResult;
import com.e7learning.common.Constant;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.RestException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.utils.NetworkUtils;
import com.e7learning.repository.CourseVideoRepository;
import com.e7learning.repository.LearningProgressRepository;
import com.e7learning.repository.UserCourseRepository;
import com.e7learning.repository.VideoTokenRepository;
import com.e7learning.repository.model.CourseVideo;
import com.e7learning.repository.model.LearningProgress;
import com.e7learning.repository.model.UserCourse;
import com.e7learning.repository.model.VideoToken;

@Service
public class VideoTokenApiService extends BaseService {

	private static final Logger log = Logger.getLogger(VideoTokenApiService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private VideoTokenRepository videoTokenRepository;

	@Autowired
	private LearningProgressRepository learningProgressRepository;

	@Autowired
	private CourseVideoRepository courseVideoRepository;

	@Autowired
	private UserCourseRepository userCourseRepository;

	@Autowired
	private ConfigService configService;

	@Transactional(rollbackFor = { RestException.class }, noRollbackFor = { MessageException.class })
	public VideoTokenResult genToken(VideoTokenRequestParam param, HttpServletRequest request) throws RestException {

		if (param == null || StringUtils.isBlank(param.getVideoId())) {
			throw new RestException(resources.getMessage("video.id.empty.err", null, null));
		}
		try {
			List<CourseVideo> courseVideoList = courseVideoRepository
					.findAll((Root<CourseVideo> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
						query.distinct(true);
						return cb.equal(root.get("video").get("thirdId"), param.getVideoId());
					});
			CourseVideo cv = courseVideoList.stream().findFirst().orElse(null);
			if (!courseVideoList.isEmpty() && cv != null) {
				List<LearningProgress> learningProgressList = learningProgressRepository
						.findAll((Root<LearningProgress> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
							query.distinct(true);
							return cb.and(cb.equal(root.get("fkCourseVideo"), cv.getPkCourseVideo()),
									cb.equal(root.get("userId"), getCurrentUserId()));
						});
				Date today = new Date();
				if (learningProgressList.isEmpty()) {

					if (userCourseRepository
							.count((Root<UserCourse> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
								return cb.equal(root.get("course").get("pkCourse"),
										cv.getCourseChapter().getCourse().getPkCourse());
							}) == 0) {
						throw new RestException(resources.getMessage("video.token.gen.denied", null, null));
					}
					// 沒播放紀錄 新增一筆
					LearningProgress learningProgress = new LearningProgress();
					learningProgress.setLearningDuration(0);
					learningProgress.setVideoDuration(cv.getVideo() != null ? cv.getVideo().getDuration() : 0);
					learningProgress.setActive(Constant.TRUE);
					learningProgress.setCreateDt(today);
					learningProgress.setCreateId(getCurrentUserId());
					learningProgress.setUserId(getCurrentUserId());
					learningProgress.setFkCourseVideo(cv.getPkCourseVideo());
					learningProgress.setLastWatchDt(today);
					learningProgressRepository.save(learningProgress);
				}else {
					learningProgressList.stream().forEach(v->v.setLastWatchDt(today));
				}

				DateTime dateTime = new DateTime();
				VideoToken videoToken = new VideoToken();
				videoToken.setActive(Constant.TRUE);
				videoToken.setCreateDt(dateTime.toDate());
				videoToken.setUpdateDt(dateTime.toDate());
				videoToken.setCreateId(getCurrentUserId());
				videoToken.setExpireDt(dateTime.plusSeconds(configService.getVideoTokenExpireSec()).toDate());
				videoToken.setLoginId(getCurrentUserId());
				videoToken.setVideoId(param.getVideoId());
				videoToken.setIp(NetworkUtils.getClientIp(request));

				videoToken = videoTokenRepository.save(videoToken);
				VideoTokenResult result = new VideoTokenResult();
				result.setToken(videoToken.getToken());
				result.setUid(getCurrentUserId());
				return result;
			}
			throw new RestException(resources.getMessage("video.not.found", null, null));
		} catch (RestException e) {
			throw e;
		} catch (Exception e) {
			log.error("genToken", e);
			throw new RestException(resources.getMessage("video.token.gen.err", null, null));
		}
	}

	public Long countVideoWatched(VideoTokenRequestParam param) {
		return videoTokenRepository.count((Root<VideoToken> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
			return cb.and(cb.equal(root.get("videoId"), param.getVideoId()),
					cb.equal(root.get("loginId"), getCurrentUserId()));
		});
	}

}
