package com.e7learning.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.e7learning.api.annotation.E7ApiController;
import com.e7learning.api.domain.RestResult;
import com.e7learning.api.domain.param.ProductQueryParam;
import com.e7learning.api.domain.result.ProductResult;
import com.e7learning.api.service.ProductApiService;
import com.e7learning.common.exception.RestException;

/**
 * @author ChrisTsai
 *
 */
@E7ApiController
public class ProductApiController {

	@Autowired
	private ProductApiService productApiService;

	@RequestMapping(value = "/product/query", method = { RequestMethod.POST })
	public RestResult<Page<ProductResult>> queryTag(@RequestBody ProductQueryParam param) throws RestException {
		return new RestResult<Page<ProductResult>>(true, productApiService.queryProduct(param));
	}
}
