package com.e7learning.api.domain.result;

import java.io.Serializable;

public class VideoTokenResult implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String token;
	
	private String uid;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	

}
