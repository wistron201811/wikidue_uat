package com.e7learning.api.domain.param;

import java.io.Serializable;

public class PoQueryParam implements Serializable {

	/** 主訂單號號 */
	private String mainOrderNo;

	private int pageNo = 1;
	
	public String getMainOrderNo() {
		return mainOrderNo;
	}

	public void setMainOrderNo(String mainOrderNo) {
		this.mainOrderNo = mainOrderNo;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}


}
