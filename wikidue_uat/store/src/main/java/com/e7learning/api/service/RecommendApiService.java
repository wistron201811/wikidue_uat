package com.e7learning.api.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.e7learning.api.domain.param.RecommendQueryParam;
import com.e7learning.api.domain.result.RecommendQueryResult;
import com.e7learning.common.Constant;
import com.e7learning.common.exception.RestException;
import com.e7learning.common.service.ConfigService;
import com.e7learning.repository.RecommendRepository;
import com.e7learning.repository.model.Recommend;

@Service
public class RecommendApiService {

	private static final Logger log = Logger.getLogger(RecommendApiService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private RecommendRepository recommendRepository;

	@Autowired
	private ConfigService configService;

	public Page<RecommendQueryResult> queryRecommend(RecommendQueryParam param) throws RestException {
		try {

			Sort sort = new Sort(Direction.DESC, "publishSDate");
			Pageable pageable = new PageRequest(param.getPage() - 1, configService.getPageSize(), sort);

			Page<Recommend> recommends = recommendRepository
					.findAll((Root<Recommend> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
						List<Predicate> predicates = new ArrayList<>();
						// 已上架
						predicates.add(cb.equal(root.get("active"), Constant.TRUE));
						predicates.add(cb.lessThanOrEqualTo(root.get("publishSDate"), new Date()));
						predicates.add(cb.greaterThanOrEqualTo(root.get("publishEDate"), new LocalDate().toDate()));

						if (!predicates.isEmpty()) {
							return cb.and(predicates.toArray(new Predicate[predicates.size()]));
						}

						return cb.conjunction();
					}, pageable);
			return recommends.map(v -> toRecommendQueryResult(v));
		} catch (Exception e) {
			log.error("queryTag", e);
			throw new RestException(resources.getMessage("product.query.err", null, null));
		}
	}

	private RecommendQueryResult toRecommendQueryResult(Recommend recommend) {
		RecommendQueryResult recommendQueryResult = new RecommendQueryResult();
		recommendQueryResult.setPkRecommend(recommend.getPkRecommend());
		recommendQueryResult.setAuthor(recommend.getAuthor());
		recommendQueryResult.setCreateDt(recommend.getCreateDt());
		recommendQueryResult.setImage(configService.getHost() + configService.getImageApi() + recommend.getImageId());
		recommendQueryResult.setTitle(recommend.getTitle());
		recommendQueryResult.setSummary(Jsoup.parse(recommend.getContent()).text());

		return recommendQueryResult;
	}

}
