package com.e7learning.api.domain.result;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

public class QuestionRequestResult {
	
	private Integer questionId;

	private Integer courseId;
	
	private String topic;
	
	private String content;
	
	private boolean courseTeacher = false;
	
	private String createId;
	
	private String userName;
	
	@JsonFormat(pattern="yyyy年MM月dd日 HH:mm", timezone = "GMT+8")
	private Date createDt;
	
	@JsonFormat(pattern="yyyy年MM月dd日 HH:mm", timezone = "GMT+8")
	private Date updateDt;
	
	private List<QuestionReplyResult> questionReplies;

	public Integer getCourseId() {
		return courseId;
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public boolean isCourseTeacher() {
		return courseTeacher;
	}

	public void setCourseTeacher(boolean courseTeacher) {
		this.courseTeacher = courseTeacher;
	}

	public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public Date getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public List<QuestionReplyResult> getQuestionReplies() {
		return questionReplies;
	}

	public void setQuestionReplies(List<QuestionReplyResult> questionReplies) {
		this.questionReplies = questionReplies;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getQuestionId() {
		return questionId;
	}

	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}
	
	
	
	
	
}
