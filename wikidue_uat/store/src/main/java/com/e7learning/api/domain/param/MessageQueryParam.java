package com.e7learning.api.domain.param;

import java.io.Serializable;

public class MessageQueryParam implements Serializable {

	private String token;
	
	private Integer page = 1;
	
	private String messageId;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	
	
}
