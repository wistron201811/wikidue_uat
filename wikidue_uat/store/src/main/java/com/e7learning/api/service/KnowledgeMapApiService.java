package com.e7learning.api.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.e7learning.api.domain.param.TagQueryParam;
import com.e7learning.api.domain.result.KnowledgeMapResult;
import com.e7learning.common.Constant;
import com.e7learning.common.exception.RestException;
import com.e7learning.repository.KnowledgeMapRepository;
import com.e7learning.repository.ProductTagRepository;
import com.e7learning.repository.ShareTagRepository;
import com.e7learning.repository.model.KnowledgeMap;
import com.e7learning.repository.model.ProductTag;
import com.e7learning.repository.model.ShareTag;
import com.google.gson.Gson;

@Service
public class KnowledgeMapApiService {

	private static final Logger log = Logger.getLogger(KnowledgeMapApiService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private KnowledgeMapRepository knowledgeMapRepository;

	@Autowired
	private ProductTagRepository productTagRepository;

	@Autowired
	private ShareTagRepository shareTagRepository;

	public List<KnowledgeMapResult> queryTag(TagQueryParam param) throws RestException {
		List<KnowledgeMapResult> result = new ArrayList<>();

		try {
			log.info(new Gson().toJson(param));
			List<KnowledgeMap> tags = knowledgeMapRepository.findAll(buildOperSpecification(param));
			tags.stream().map(KnowledgeMapApiService::toKnowledgeMapResult).forEach(result::add);
		} catch (Exception e) {
			log.error("queryTag", e);
			throw new RestException(resources.getMessage("tag.query.err", null, null));
		}

		return result;
	}

	public List<KnowledgeMapResult> queryProductTag(final TagQueryParam param) throws RestException {
		List<KnowledgeMapResult> result = new ArrayList<>();

		try {
			log.info(new Gson().toJson(param));
			List<ProductTag> tags = productTagRepository
					.findAll((Root<ProductTag> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
						List<Predicate> predicates = new ArrayList<>();
						if (StringUtils.isNotBlank(param.getLevel())) {
							Path<String> np = root.get("tagLevel");
							predicates.add(cb.equal(np, param.getLevel()));
						}

						if (param.getType() != null) {
							switch (param.getType()) {
							case COURSE:
								predicates.add(cb.isNotNull(root.get("product").get("course")));
								break;
							case PROPS:
								predicates.add(cb.isNotNull(root.get("product").get("props")));
								break;
							case CAMP:
								predicates.add(cb.isNotNull(root.get("product").get("camp")));
								break;
							default:
								break;
							}
						}

						if (param.getTag() != null && param.getTag().length > 0) {
							Path<String> np = root.get("tag");
							Predicate[] likes = new Predicate[param.getTag().length];
							for (int i = 0; i < param.getTag().length; i++) {
								likes[i] = cb.like(np, param.getTag()[i] + "%");
							}

							predicates.add(cb.or(likes));
						}

						// 已上架
						predicates.add(cb.equal(root.get("product").get("active"), Constant.TRUE));
						predicates.add(cb.lessThanOrEqualTo(root.get("product").get("publishSDate"), new Date()));
						predicates.add(cb.greaterThanOrEqualTo(root.get("product").get("publishEDate"), new Date()));

						if (!predicates.isEmpty()) {
							return cb.and(predicates.toArray(new Predicate[predicates.size()]));
						}
						return cb.conjunction();
					});
			tags.stream().map(KnowledgeMapApiService::toKnowledgeMapResult).forEach(v -> addTag(result, v));
		} catch (Exception e) {
			log.error("queryTag", e);
			throw new RestException(resources.getMessage("tag.query.err", null, null));
		}

		return result;
	}

	public static void addTag(List<KnowledgeMapResult> result, KnowledgeMapResult knowledgeMapResult) {
		if (result.stream().filter(tmp -> StringUtils.equals(tmp.getTag(), knowledgeMapResult.getTag())).count() == 0) {
			result.add(knowledgeMapResult);
		}
	}

	public List<KnowledgeMapResult> queryShareTag(TagQueryParam param) throws RestException {
		List<KnowledgeMapResult> result = new ArrayList<>();

		try {
			log.info(new Gson().toJson(param));
			List<ShareTag> tags = shareTagRepository
					.findAll((Root<ShareTag> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
						List<Predicate> predicates = new ArrayList<>();
						if (StringUtils.isNotBlank(param.getLevel())) {
							Path<String> np = root.get("tagLevel");
							predicates.add(cb.equal(np, param.getLevel()));
						}

						if (param.getTag() != null && param.getTag().length > 0) {
							Path<String> np = root.get("tag");
							Predicate[] likes = new Predicate[param.getTag().length];
							for (int i = 0; i < param.getTag().length; i++) {
								likes[i] = cb.like(np, param.getTag()[i] + "%");
							}

							predicates.add(cb.or(likes));
						}
						if (!predicates.isEmpty()) {
							return cb.and(predicates.toArray(new Predicate[predicates.size()]));
						}
						return cb.conjunction();
					});
			tags.stream().map(KnowledgeMapApiService::toKnowledgeMapResult).forEach(v -> addTag(result, v));
		} catch (Exception e) {
			log.error("queryTag", e);
			throw new RestException(resources.getMessage("tag.query.err", null, null));
		}

		return result;
	}

	private static KnowledgeMapResult toKnowledgeMapResult(ShareTag tag) {
		KnowledgeMapResult result = new KnowledgeMapResult();
		result.setLevel(tag.getTagLevel());
		result.setName(tag.getTagName());
		result.setTag(tag.getTag());
		return result;
	}

	private static KnowledgeMapResult toKnowledgeMapResult(ProductTag tag) {
		KnowledgeMapResult result = new KnowledgeMapResult();
		result.setLevel(tag.getTagLevel());
		result.setName(tag.getTagName());
		result.setTag(tag.getTag());
		return result;
	}

	private static KnowledgeMapResult toKnowledgeMapResult(KnowledgeMap map) {
		KnowledgeMapResult result = new KnowledgeMapResult();
		result.setLevel(map.getTagLevel());
		result.setName(map.getTagName());
		result.setTag(map.getPkKnowledgeMap());
		return result;
	}

	private Specification<KnowledgeMap> buildOperSpecification(TagQueryParam param) {
		return new Specification<KnowledgeMap>() {
			@Override
			public Predicate toPredicate(Root<KnowledgeMap> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

				List<Predicate> predicates = new ArrayList<>();

				if (StringUtils.isNotBlank(param.getLevel())) {
					Path<String> np = root.get("tagLevel");
					predicates.add(cb.equal(np, param.getLevel()));
				}
				if (param.getTag() != null && param.getTag().length > 0) {
					Path<String> np = root.get("pkKnowledgeMap");
					Predicate[] likes = new Predicate[param.getTag().length];
					for (int i = 0; i < param.getTag().length; i++) {
						likes[i] = cb.like(np, param.getTag()[i] + "%");
					}

					predicates.add(cb.or(likes));
				}
				if (!predicates.isEmpty()) {
					return cb.and(predicates.toArray(new Predicate[predicates.size()]));
				}
				return cb.conjunction();
			}
		};
	}
}
