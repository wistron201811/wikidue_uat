package com.e7learning.api.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.api.domain.RestResult;
import com.e7learning.api.domain.param.CartAddParam;
import com.e7learning.api.domain.param.CartQueryParam;
import com.e7learning.api.domain.param.CartRemoveParam;
import com.e7learning.api.domain.result.CartResult;
import com.e7learning.common.Constant;
import com.e7learning.common.enums.CategoryTypeEnum;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.RestException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.service.SsoService;
import com.e7learning.common.vo.SsoUserInfoResponse;
import com.e7learning.repository.CartRepository;
import com.e7learning.repository.ProductRepository;
import com.e7learning.repository.model.Cart;
import com.e7learning.repository.model.Product;

@Service
public class CartApiService extends BaseService {

	private static final Logger log = Logger.getLogger(CartApiService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private CartRepository cartRepository;

	@Autowired
	private ConfigService configService;

	@Autowired
	private SsoService ssoService;

	@Autowired
	private ProductRepository productRepository;

	@Transactional(readOnly = true)
	public List<CartResult> queryCart(CartQueryParam param) throws RestException {
		List<CartResult> result = new ArrayList<>();

		try {
			final String loginId = getCurrentUserBySessionOrToken(param.getToken());
			List<Cart> carts = cartRepository.findAll((Root<Cart> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
				List<Predicate> predicates = new ArrayList<>();
				predicates.add(cb.equal(root.get("purchaser"), loginId));
				predicates.add(cb.equal(root.get("status"), Constant.CART_STATUS_AVAILABLE));

				if (!predicates.isEmpty()) {
					return cb.and(predicates.toArray(new Predicate[predicates.size()]));
				}

				return cb.conjunction();
			}, new Sort(Direction.DESC, "updateDt"));
			if (param.isQueryStock()) {
				carts.stream().forEach(v -> {
					if (productRepository.exists(v.getFkProduct())) {
						Product product = productRepository.findOne(v.getFkProduct());
						if (product.getProps() != null) {
							v.setStock(product.getProps().getQuantity());
						} else {
							v.setStock(-1);
						}
					} else {
						v.setStock(0);
					}
					result.add(toCartResult(v));
				});
			} else {
				carts.stream().forEach(v -> result.add(toCartResult(v)));
			}
		} catch (RestException e) {
			log.error("queryCart", e);
			throw e;
		} catch (Exception e) {
			log.error("queryCart", e);
			throw new RestException(resources.getMessage("cart.query.error", null, null));
		}

		return result;
	}

	@Transactional(rollbackFor = { RestException.class }, noRollbackFor = { MessageException.class })
	public void removeCart(CartRemoveParam param) throws RestException {
		try {
			final String loginId = getCurrentUserBySessionOrToken(param.getToken());
			if (cartRepository.exists(param.getCartId())) {
				Cart cart = cartRepository.findOne(param.getCartId());
				if (StringUtils.equals(cart.getPurchaser(), loginId)
						&& Constant.CART_STATUS_AVAILABLE.equals(cart.getStatus())) {
					cartRepository.delete(cart);
				} else
					throw new RestException(resources.getMessage("remove.cart.error", null, null));
			} else {
				throw new RestException(resources.getMessage("cart.not.exist.error", null, null));
			}

		} catch (RestException e) {
			log.error("addCart", e);
			throw e;
		} catch (Exception e) {
			log.error("removeCart", e);
			throw new RestException(resources.getMessage("remove.cart.error", null, null));
		}
	}

	@Transactional(rollbackFor = { RestException.class }, noRollbackFor = { MessageException.class })
	public CartResult addCart(CartAddParam param) throws RestException {
		try {
			if (productRepository.exists(param.getProductId())) {
				Product product = productRepository.findOne(param.getProductId());

				List<Cart> cartList = cartRepository
						.findAll((Root<Cart> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
							return cb.and(cb.equal(root.get("createId"), getCurrentUserId()),
									cb.equal(root.get("fkProduct"), param.getProductId()),
									cb.equal(root.get("status"), Constant.CART_STATUS_AVAILABLE));
						});
				// 檢查購物車是否已有商品
				if (cartList.isEmpty()) {
					Cart cart = new Cart();
					// 檢查商品是否可用
					if (Constant.TRUE.equals(product.getActive()) && new Date().after(product.getPublishSDate())
							&& new Date().before(product.getPublishEDate())
							&& (product.getProps() != null || product.getCourse() != null)) {
						cart.setCreateDt(new Date());
						cart.setCreateId(getCurrentUserId());
						cart.setUpdateDt(cart.getCreateDt());
						cart.setLastModifiedId(getCurrentUserId());
						cart.setFkProduct(product.getPkProduct());
						cart.setListPrice(product.getPrice());
						// 塞image
						// 先判斷 是哪種商品
						if (product.getProps() != null) {
							// 教具
							cart.setProductImage(product.getProps().getImageOneId());
							// 檢查數量是否足夠
							if (param.getQuantity() >= 1) {
								if (product.getProps().getQuantity() >= param.getQuantity()) {
									cart.setQuantity(param.getQuantity());
									// product.setQuantity(product.getQuantity()
									// - param.getQuantity());
								} else
									throw new RestException(resources.getMessage("product.stock.error", null, null));
							} else {
								throw new RestException(resources.getMessage("cart.quantity.error", null, null));
							}
							cart.setType(CategoryTypeEnum.PROPS.toString());
						} else {
							cart.setType(CategoryTypeEnum.COURSE.toString());
							// 課程
							cart.setProductImage(product.getCourse().getImageOne());
							cart.setQuantity(1);
						}

						cart.setProductName(product.getProductName());
						cart.setPurchaser(getCurrentUserId());
						cart.setUnitPrice(product.getDiscountPrice());
						cart.setStatus(Constant.CART_STATUS_AVAILABLE);
						cart = cartRepository.save(cart);
						return toCartResult(cart);

					} else {
						throw new RestException(resources.getMessage("product.query.error", null, null));
					}
				} else {
					if (!param.isCheckExist())
						return toCartResult(cartList.stream().findFirst().get());
					else
						throw new RestException(resources.getMessage("cart.exist.error", null, null));
				}

			} else {
				throw new RestException(resources.getMessage("product.not.exist.error", null, null));
			}

		} catch (RestException e) {
			log.error("addCart", e);
			throw e;
		} catch (Exception e) {
			log.error("addCart", e);
			throw new RestException(resources.getMessage("add.cart.error", null, null));
		}
	}

	@Transactional(rollbackFor = { RestException.class }, noRollbackFor = { MessageException.class })
	public List<CartResult> editCart(List<CartResult> param) throws RestException {
		final RestResult result = new RestResult(true);
		if (param != null) {
			param.stream().forEach(c -> {
				if (cartRepository.exists(c.getPkCart())) {
					Cart cart = cartRepository.findOne(c.getPkCart());
					if (Constant.CART_STATUS_AVAILABLE.equals(cart.getStatus())
							&& StringUtils.equals(getCurrentUserId(), cart.getPurchaser()) && c.getQuantity() > 0) {
						if (c.getQuantity().intValue() != cart.getQuantity().intValue())
							cart.setQuantity(c.getQuantity());
					} else {
						result.setSuccess(false);
					}

				} else {
					if (productRepository.exists(c.getFkProduct())) {
						CartAddParam cartAddParam = new CartAddParam();
						cartAddParam.setProductId(c.getFkProduct());
						cartAddParam.setQuantity(c.getQuantity());
						try {
							addCart(cartAddParam);
						} catch (RestException e) {
							result.setSuccess(false);
						}
					}

				}
			});
			if (!result.isSuccess())
				throw new RestException(resources.getMessage("edit.cart.error", null, null));
		}
		CartQueryParam queryParam = new CartQueryParam();
		queryParam.setQueryStock(true);
		return queryCart(queryParam);
	}

	@Transactional(rollbackFor = { RestException.class }, noRollbackFor = { MessageException.class })
	public CartResult editCart(CartResult param) throws RestException {
		if (cartRepository.exists(param.getPkCart())) {
			Cart cart = cartRepository.findOne(param.getPkCart());
			if (Constant.CART_STATUS_AVAILABLE.equals(cart.getStatus())
					&& StringUtils.equals(getCurrentUserId(), cart.getPurchaser()) && param.getQuantity() > 0) {
				if (param.getQuantity().intValue() != cart.getQuantity().intValue())
					cart.setQuantity(param.getQuantity());
				return param;
			}

		}
		throw new RestException(resources.getMessage("edit.cart.error", null, null));
	}

	public CartResult toCartResult(Cart cart) {
		CartResult cartResult = new CartResult();
		cartResult.setCreateDt(cart.getCreateDt());
		cartResult.setCreateId(cart.getCreateId());
		cartResult.setFkProduct(cart.getFkProduct());
		cartResult.setLastModifiedId(cart.getLastModifiedId());
		cartResult.setListPrice(cart.getListPrice());
		cartResult.setPkCart(cart.getPkCart());
		cartResult.setProductImage(configService.getHost() + configService.getImageApi() + cart.getProductImage());
		cartResult.setProductName(cart.getProductName());
		cartResult.setPurchaser(cart.getPurchaser());
		cartResult.setQuantity(cart.getQuantity());
		cartResult.setStatus(cart.getStatus());
		cartResult.setUnitPrice(cart.getUnitPrice());
		cartResult.setUpdateDt(cart.getUpdateDt());
		cartResult.setStock(cart.getStock());
		cartResult.setType(cart.getType());

		return cartResult;
	}

	private String getCurrentUserBySessionOrToken(String token) throws RestException {
		String loginId = null;
		try {
			if (StringUtils.isNotBlank(token)) {
				SsoUserInfoResponse ssoUserInfoResponse = ssoService.getUserInfo(token);
				if (!Constant.SSO_STATUS_ERROR.equals(ssoUserInfoResponse.getHeader().getStatus())) {
					loginId = ssoUserInfoResponse.getBody().getUserInfo().getLoginId();
				} else {
					throw new RestException(ssoUserInfoResponse.getBody().getCode(),
							ssoUserInfoResponse.getBody().getMessage());
				}
			} else if (!StringUtils.equals(getCurrentUserId(), "anonymous")) {
				loginId = getCurrentUserId();
			} else {
				throw new RestException(resources.getMessage("message.token.empty.err", null, null));
			}
		} catch (RestException e) {
			throw e;
		} catch (ServiceException e) {
			throw new RestException(e.getMessage());
		}
		return loginId;
	}
}
