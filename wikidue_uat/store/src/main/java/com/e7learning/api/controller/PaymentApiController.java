package com.e7learning.api.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.e7learning.api.annotation.E7ApiController;
import com.e7learning.api.domain.RestResult;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.payment.service.PaymentService;
import com.e7learning.payment.vo.PaymentResultVo;
import com.google.gson.Gson;

@E7ApiController
public class PaymentApiController {

	private static final Logger LOG = Logger.getLogger(PaymentApiController.class);

	@Autowired
	private PaymentService paymentService;

	@RequestMapping(value = "/payment", method = { RequestMethod.POST, RequestMethod.GET })
	public String payment(PaymentResultVo result) {
		LOG.error("payment result:" + new Gson().toJson(result));
		try {
			if (paymentService.handleResult(result)) {
				LOG.error(result.getMerchantTradeNo() + " payment return 1");
				return "1";
			}
		} catch (Exception e) {
		}
		LOG.error((result != null ? result.getMerchantTradeNo() : "") + " payment return 0");
		return "0";
	}
	
	@RequestMapping(value = "/checkPaymentResult", method = { RequestMethod.POST})
	public RestResult<PaymentResultVo> checkPaymentResult(@RequestBody PaymentResultVo result) throws ServiceException {
		return  new RestResult<PaymentResultVo>(true, paymentService.checkPaymentResult(result));
	}
}
