package com.e7learning.api.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.api.domain.param.MessageQueryParam;
import com.e7learning.api.domain.result.MessageCountResult;
import com.e7learning.api.domain.result.MessageQueryResult;
import com.e7learning.common.Constant;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.RestException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.service.SsoService;
import com.e7learning.common.vo.SsoUserInfoResponse;
import com.e7learning.repository.E7MessageRepository;
import com.e7learning.repository.model.E7Message;

@Service
public class MessageApiService extends BaseService {

	private static final Logger log = Logger.getLogger(MessageApiService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private E7MessageRepository messageRepository;

	@Autowired
	private SsoService ssoService;

	@Autowired
	private ConfigService configService;

	public List<MessageQueryResult> queryMessage(MessageQueryParam param) throws RestException {
		List<MessageQueryResult> result = new ArrayList<>();

		try {
			List<E7Message> msgs = messageRepository.findTop5ByOwnerIDAndActiveOrderByCreateDtDesc(
					getCurrentUserBySessionOrToken(param.getToken()), Constant.TRUE);

			msgs.stream().forEach(v -> result.add(new MessageQueryResult(v)));
		} catch (RestException e) {
			throw e;
		} catch (Exception e) {
			log.error("queryMessage", e);
			throw new RestException(resources.getMessage("op.query.error", null, null));
		}

		return result;
	}

	public MessageCountResult queryMessageCount(MessageQueryParam param) throws RestException {
		MessageCountResult result = new MessageCountResult();
		try {
			result.setUnreadCount(messageRepository.countByOwnerIDAndActiveAndIsReadOrderByCreateDtDesc(
					getCurrentUserBySessionOrToken(param.getToken()), Constant.TRUE, Constant.FLASE));
			return result;
		} catch (RestException e) {
			throw e;
		} catch (Exception e) {
			log.error("queryMessage", e);
			throw new RestException(resources.getMessage("op.query.error", null, null));
		}
	}

	public Page<MessageQueryResult> listMessage(MessageQueryParam param) throws RestException {

		Sort sort = new Sort(Direction.DESC, "createDt");
		Pageable pageable = new PageRequest(param.getPage() - 1, configService.getPageSize(), sort);
		try {
			Page<E7Message> messages = messageRepository
					.findAll((Root<E7Message> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
						List<Predicate> predicates = new ArrayList<>();

						predicates.add(cb.equal(root.get("ownerID"), getCurrentUserId()));
						predicates.add(cb.equal(root.get("active"), Constant.TRUE));

						if (!predicates.isEmpty()) {
							return cb.and(predicates.toArray(new Predicate[predicates.size()]));
						}
						return cb.conjunction();
					}, pageable);

			return messages.map(v -> new MessageQueryResult(v));
		} catch (Exception e) {
			throw new RestException(resources.getMessage("op.query.error", null, null));
		}
	}

	@Transactional(rollbackFor = { RestException.class }, noRollbackFor = { MessageException.class })
	public MessageQueryResult queryMsgDetail(MessageQueryParam param) throws RestException {

		try {
			if (StringUtils.isNotBlank(param.getMessageId()) && messageRepository.exists(param.getMessageId())) {
				E7Message msg = messageRepository.findOne(param.getMessageId());
				if (!StringUtils.equals(getCurrentUserId(), msg.getOwnerID())) {
					throw new RestException(resources.getMessage("message.not.found", null, null));
				}
				msg.setIsRead(Constant.TRUE);
				messageRepository.save(msg);
				return new MessageQueryResult(msg);
			} else {
				throw new RestException(resources.getMessage("message.not.found", null, null));
			}
		} catch (RestException e) {
			throw e;
		} catch (Exception e) {
			throw new RestException(e.getMessage());
		}

	}

	private String getCurrentUserBySessionOrToken(String token) throws RestException {
		String loginId = null;
		try {
			if (StringUtils.isNotBlank(token)) {
				SsoUserInfoResponse ssoUserInfoResponse = ssoService.getUserInfo(token);
				if (!Constant.SSO_STATUS_ERROR.equals(ssoUserInfoResponse.getHeader().getStatus())) {
					loginId = ssoUserInfoResponse.getBody().getUserInfo().getLoginId();
				} else {
					throw new RestException(ssoUserInfoResponse.getBody().getCode(),
							ssoUserInfoResponse.getBody().getMessage());
				}
			} else if (!StringUtils.equals(getCurrentUserId(), "anonymous")) {
				loginId = getCurrentUserId();
			} else {
				throw new RestException(resources.getMessage("message.token.empty.err", null, null));
			}
		} catch (RestException e) {
			throw e;
		} catch (ServiceException e) {
			throw new RestException(e.getMessage());
		}
		return loginId;
	}
}
