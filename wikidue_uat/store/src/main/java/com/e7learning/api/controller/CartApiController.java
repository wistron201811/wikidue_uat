package com.e7learning.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.e7learning.api.annotation.E7ApiController;
import com.e7learning.api.domain.RestResult;
import com.e7learning.api.domain.param.CartAddParam;
import com.e7learning.api.domain.param.CartQueryParam;
import com.e7learning.api.domain.param.CartRemoveParam;
import com.e7learning.api.domain.result.CartResult;
import com.e7learning.api.service.CartApiService;
import com.e7learning.cart.service.CartService;
import com.e7learning.common.exception.RestException;

/**
 * @author ChrisTsai
 *
 */
@E7ApiController
public class CartApiController {

	@Autowired
	private CartApiService cartApiService;

	@Autowired
	private CartService cartService;

	@RequestMapping(value = "/cart/query", method = { RequestMethod.POST })
	public RestResult<List<CartResult>> queryTag(@RequestBody CartQueryParam param) throws RestException {
		return new RestResult<>(true, cartApiService.queryCart(param));
	}

	@RequestMapping(value = "/cart/add", method = { RequestMethod.POST })
	public RestResult<CartResult> addCart(@RequestBody CartAddParam param) throws RestException {
		return new RestResult<>(true, cartApiService.addCart(param));
	}

	@RequestMapping(value = "/cart/remove", method = { RequestMethod.POST })
	public RestResult<?> removeCart(@RequestBody CartRemoveParam param) throws RestException {
		cartApiService.removeCart(param);
		return new RestResult<>(true);
	}

	@RequestMapping(value = "/cart/edit", method = { RequestMethod.POST })
	public RestResult<CartResult> editCart(@RequestBody CartResult param) throws RestException {
		return new RestResult<>(true, cartApiService.editCart(param));
	}

	@RequestMapping(value = "/cart/buyFreeCourse", method = { RequestMethod.POST })
	public RestResult<?> buyFreeCourse(@RequestBody CartAddParam param) throws RestException {
		cartService.buyFreeCourse(param.getProductId());
		return new RestResult<>(true);
	}
}
