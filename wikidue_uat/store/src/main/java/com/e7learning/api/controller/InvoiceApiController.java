package com.e7learning.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.e7learning.api.annotation.E7ApiController;
import com.e7learning.api.domain.RestResult;
import com.e7learning.api.domain.param.InvoiceRequestParam;
import com.e7learning.api.domain.result.InvoiceResult;
import com.e7learning.api.service.InvoiceApiService;
import com.e7learning.common.exception.RestException;

@E7ApiController
public class InvoiceApiController {

	@Autowired
	private InvoiceApiService invoiceApiService;

	@RequestMapping(value = "/invoice/query", method = { RequestMethod.POST })
	public RestResult<InvoiceResult> isFavourite(@RequestBody InvoiceRequestParam param)
			throws RestException {
		return new RestResult<>(true, invoiceApiService.queryInvoiceDetail(param.getPaymentId()));
	}
}
