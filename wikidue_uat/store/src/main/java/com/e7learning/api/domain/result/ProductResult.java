package com.e7learning.api.domain.result;

public class ProductResult {

	private String pkProduct;
	
	private Integer pkCamp;

	private Integer pkShare;
	
	private String title;

	private String summary;
	
	private String image;

	private Integer discountPrice;

	private Integer price;
	
	private Integer quantity = 0;
	
	private String externalLink;

	public String getPkProduct() {
		return pkProduct;
	}

	public void setPkProduct(String pkProduct) {
		this.pkProduct = pkProduct;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	
	public Integer getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Integer discountPrice) {
		this.discountPrice = discountPrice;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getPkCamp() {
		return pkCamp;
	}

	public void setPkCamp(Integer pkCamp) {
		this.pkCamp = pkCamp;
	}

	public Integer getPkShare() {
		return pkShare;
	}

	public void setPkShare(Integer pkShare) {
		this.pkShare = pkShare;
	}

	public String getExternalLink() {
		return externalLink;
	}

	public void setExternalLink(String externalLink) {
		this.externalLink = externalLink;
	}

	
	
}
