package com.e7learning.api.domain.param;

public class LearningCenterQueryParam {
		
	private String order = "createDt";
	
	private Integer page = 1;

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}
	
	
	
}
