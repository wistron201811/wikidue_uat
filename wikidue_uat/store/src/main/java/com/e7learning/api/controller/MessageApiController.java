package com.e7learning.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.e7learning.api.annotation.E7ApiController;
import com.e7learning.api.domain.RestResult;
import com.e7learning.api.domain.param.MessageQueryParam;
import com.e7learning.api.domain.result.MessageCountResult;
import com.e7learning.api.domain.result.MessageQueryResult;
import com.e7learning.api.service.MessageApiService;
import com.e7learning.common.exception.RestException;

/**
 * @author ChrisTsai
 *
 */
@E7ApiController
public class MessageApiController {

	@Autowired
	private MessageApiService messageService;

	@RequestMapping(value = "/message")
	public RestResult<List<MessageQueryResult>> queryMessage(@RequestBody MessageQueryParam paem) throws RestException {
		return new RestResult<>(true, messageService.queryMessage(paem));
	}

	@RequestMapping(value = "/message/count")
	public RestResult<MessageCountResult> queryMessageCount(@RequestBody MessageQueryParam paem) throws RestException {
		return new RestResult<>(true, messageService.queryMessageCount(paem));
	}

	@RequestMapping(value = "/message/list")
	public RestResult<Page<MessageQueryResult>> listMessage(@RequestBody MessageQueryParam paem) throws RestException {
		return new RestResult<>(true, messageService.listMessage(paem));
	}

	@RequestMapping(value = "/message/detail")
	public RestResult<MessageQueryResult> queryMsgDetail(@RequestBody MessageQueryParam paem) throws RestException {
		return new RestResult<>(true, messageService.queryMsgDetail(paem));
	}

}
