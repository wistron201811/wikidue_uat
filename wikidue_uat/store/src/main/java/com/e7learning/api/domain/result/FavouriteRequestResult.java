package com.e7learning.api.domain.result;

public class FavouriteRequestResult {

	private boolean result = false;

	public FavouriteRequestResult() {
	}

	public FavouriteRequestResult(boolean result) {
		this.result = result;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

}
