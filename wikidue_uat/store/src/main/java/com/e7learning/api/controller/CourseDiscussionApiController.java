package com.e7learning.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.e7learning.api.annotation.E7ApiController;
import com.e7learning.api.domain.RestResult;
import com.e7learning.api.domain.param.QuestionRequestParam;
import com.e7learning.api.domain.result.QuestionRequestResult;
import com.e7learning.api.service.CourseDiscussionApiService;
import com.e7learning.common.exception.RestException;

@E7ApiController
public class CourseDiscussionApiController {

	@Autowired
	private CourseDiscussionApiService courseDiscussionApiService;

	@RequestMapping(value = "/discussion/query", method = { RequestMethod.POST })
	public RestResult<List<QuestionRequestResult>> queryQuestions(@RequestBody QuestionRequestParam param)
			throws RestException {
		return new RestResult<List<QuestionRequestResult>>(true, courseDiscussionApiService.queryQuestions(param));
	}

	@RequestMapping(value = "/discussion/ask", method = { RequestMethod.POST })
	public RestResult<?> askQuestion(@RequestBody QuestionRequestParam param) throws RestException {
		courseDiscussionApiService.askQuestion(param);
		return new RestResult<>(true);
	}

	@RequestMapping(value = "/discussion/reply", method = { RequestMethod.POST })
	public RestResult<?> replyQuestion(@RequestBody QuestionRequestParam param) throws RestException {
		courseDiscussionApiService.replyQuestion(param);
		return new RestResult<>(true);
	}
}
