package com.e7learning.api.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.api.domain.param.PoQueryParam;
import com.e7learning.api.domain.result.PoDetailQueryResult;
import com.e7learning.api.domain.result.PoQueryResult;
import com.e7learning.common.enums.PayWayEnum;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.RestException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.repository.MainOrderRepository;
import com.e7learning.repository.model.MainOrder;
import com.e7learning.repository.model.Product;
import com.e7learning.repository.model.SubOrder;
import com.e7learning.repository.model.SubOrderDetail;

/**
 * @author J
 *
 */
@Service
public class PurchaseOrderService extends BaseService {

	private static final Logger log = Logger.getLogger(PurchaseOrderService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private MainOrderRepository mainOrderRepository;

	@Autowired
	private ConfigService configService;

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public Page<PoQueryResult> queryMainOrderList(PoQueryParam param) throws RestException {
		try {
			int page = param.getPageNo() == 0 ? 1 : param.getPageNo();
			Pageable pageable = new PageRequest(page - 1, configService.getPageSize(),
					new Sort(Direction.DESC, "orderDt"));
			Page<MainOrder> result = mainOrderRepository
					.findAll((Root<MainOrder> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
						return cb.and(cb.equal(root.get("purchaserId"), getCurrentUserId()));
					}, pageable);
			return result.map(PurchaseOrderService::toPoQueryResult);
		} catch (Exception e) {
			log.error("queryMainOrderList", e);
			throw new RestException(resources.getMessage("op.query.error", null, null));
		}
	}

	private static PoQueryResult toPoQueryResult(MainOrder mainOrder) {
		PoQueryResult result = null;
		if (mainOrder != null) {
			result = new PoQueryResult();
			result.setCreateDt(mainOrder.getCreateDt());
			result.setMainOrderNo(mainOrder.getPkMainOrder());
			result.setPaymentDueDate(mainOrder.getPaymentDueDate());
			result.setStatus(mainOrder.getOrderStatus());
			result.setTotalAmount(String.valueOf(mainOrder.getOrderAmount()));
			result.setRemark(mainOrder.getUserRemark());
			if (mainOrder.getPayment() != null) {
				result.setPaymentType(mainOrder.getPayment().getPaymentType());
				PayWayEnum payWay = PayWayEnum.getByCode(mainOrder.getPayment().getPaymentType());
				if (payWay != null) {
					result.setPaymentTypeName(payWay.getName());
				}
				result.setPaymentStatus(mainOrder.getPayment().getStatus());
				result.setTradeAmt(String.valueOf(mainOrder.getPayment().getTradeAmt()));
				result.setTradeNo(String.valueOf(mainOrder.getPayment().getTradeNo()));
				result.setPaymentId(mainOrder.getPayment().getPkPayment());
			}
			// 放收件人資料
			result.setRecipient(mainOrder.getRecipient());
			result.setAddress(mainOrder.getAddress());
			result.setTelphone(mainOrder.getTelphone());
		}
		return result;
	}

	@Transactional(readOnly = true)
	public PoQueryResult queryOrderDetail(PoQueryParam param) {
		PoQueryResult queryResult = null;
		try {
			if (param.getMainOrderNo() != null) {
				MainOrder mainOrder = mainOrderRepository.findOne(param.getMainOrderNo());
				if (mainOrder != null) {
					queryResult = toPoQueryResult(mainOrder);
					List<SubOrder> subList = mainOrder.getSubOrderList();
					if (subList != null && !subList.isEmpty()) {
						List<PoDetailQueryResult> poDetails = new ArrayList<>();
						for (SubOrder sub : subList) {
							List<SubOrderDetail> detailList = sub.getSubOrderDetailList();
							for (SubOrderDetail detail : detailList) {
								PoDetailQueryResult detailResult = toDetailResult(detail);
								if (detailResult != null) {
									poDetails.add(detailResult);
								}
							}
						}
						queryResult.setPoDetails(poDetails);
					}
				}
			}

		} catch (Exception e) {
			log.error("queryOrderDetail", e);
		}
		return queryResult;
	}

	private PoDetailQueryResult toDetailResult(SubOrderDetail detail) {
		PoDetailQueryResult result = null;
		if (detail != null) {
			result = new PoDetailQueryResult();
			result.setSubOrderNo(detail.getSubOrder().getPkSubOrder());
			result.setProductId(detail.getProduct().getPkProduct());
			result.setLogistics(detail.getSubOrder().getLogistics());
			result.setLogisticsNum(detail.getSubOrder().getLogisticsNum());
			result.setProductName(detail.getProductName());
			result.setQuantity(detail.getQuantity() == null ? "" : detail.getQuantity().toString());
			result.setTotalAmount(detail.getTotalAmount() == null ? "" : detail.getTotalAmount().toString());
			result.setUnitPrice(detail.getUnitPrice() == null ? "" : detail.getUnitPrice().toString());
			// 取原主檔的第一張圖
			Product product = detail.getProduct();
			String imageId = "";
			String desc = "";
			String productType = "";
			if (product.getCourse() != null) {
				imageId = product.getCourse().getImageOne();
				desc = product.getCourse().getCourseSummary();
				productType = "COURSE";
			} else if (product.getProps() != null) {
				imageId = product.getProps().getImageOneId();
				desc = product.getProps().getPropsSummary();
				productType = "PROPS";
			}
			if (StringUtils.isNoneBlank(imageId)) {
				result.setImage(configService.getHost() + configService.getImageApi() + imageId);
			}
			result.setProductDesc(desc);
			result.setProductType(productType);
			result.setStatus(detail.getStatus());
			result.setUpdateDt(detail.getUpdateDt());
			result.setReturnReason(detail.getReturnReason());
			result.setRemark(detail.getUserRemark());			
		}
		return result;
	}
	
}
