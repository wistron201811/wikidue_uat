package com.e7learning.api.domain.result;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

public class PoQueryResult implements Serializable {

	/*
	 * 訂單編號(金訂單編號)
	 */
	private String mainOrderNo;
	/*
	 * 訂單狀態
	 */
	private String status;
	/*
	 * 付款方式
	 */
	private String paymentType;
	/*
	 * 付款方式名稱
	 */
	private String paymentTypeName;
	/*
	 * 價格(總價)(訂單金額)
	 */
	private String totalAmount;
	/*
	 * 購買日期(訂單日期)
	 */
	@JsonFormat(pattern="yyyy-MM-dd HH:mm", timezone = "GMT+8")
	private Date createDt;
	
	/** 付款期限 */
	@JsonFormat(pattern="yyyy-MM-dd HH:mm", timezone = "GMT+8")
	private Date paymentDueDate;
	/*
	 * 綠界訂單號碼
	 */
	private String tradeNo;
	/*
	 * 結帳金額
	 */
	private String tradeAmt;
	
	/*
	 * 綠界結帳狀態
	 */
	private String paymentStatus;
	
	/** payment pk */
	private String paymentId;
	
	/** 主訂單備註 */
	private String remark;
	
	/** 收件人 */
	private String recipient;
	
	/** 收件地址 */
	private String address;
	
	/** 收件人電話 */
	private String telphone;

	private List<PoDetailQueryResult> poDetails;
	
	public PoQueryResult() {
	}

	public String getMainOrderNo() {
		return mainOrderNo;
	}

	public void setMainOrderNo(String mainOrderNo) {
		this.mainOrderNo = mainOrderNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getTradeNo() {
		return tradeNo;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}

	public String getTradeAmt() {
		return tradeAmt;
	}

	public void setTradeAmt(String tradeAmt) {
		this.tradeAmt = tradeAmt;
	}

	public List<PoDetailQueryResult> getPoDetails() {
		return poDetails;
	}

	public void setPoDetails(List<PoDetailQueryResult> poDetails) {
		this.poDetails = poDetails;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public Date getPaymentDueDate() {
		return paymentDueDate;
	}

	public void setPaymentDueDate(Date paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getPaymentTypeName() {
		return paymentTypeName;
	}

	public void setPaymentTypeName(String paymentTypeName) {
		this.paymentTypeName = paymentTypeName;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTelphone() {
		return telphone;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}
	
	
}
