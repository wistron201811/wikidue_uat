package com.e7learning.api.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.e7learning.common.service.CommonService;

@Controller
@RequestMapping("/api")
public class CommonApiController {

	@Autowired
	private CommonService commonService;

	@RequestMapping(value = "/query/image/{fileId}")
	public void queryImage(@PathVariable("fileId") String fileId, HttpServletResponse response) throws IOException {
		commonService.downloadImageFile(fileId, response);
		response.flushBuffer();
	}

	@RequestMapping(value = "/file/download/{fileId}")
	public void download(@PathVariable("fileId") String fileId, HttpServletResponse response) throws IOException {
		commonService.downloadFile(fileId, response);
		response.flushBuffer();
	}
}
