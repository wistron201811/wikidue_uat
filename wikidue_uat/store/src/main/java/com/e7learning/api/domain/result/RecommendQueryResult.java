package com.e7learning.api.domain.result;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class RecommendQueryResult {

	private String pkRecommend;
	/**
	 * 影像
	 */
	private String image;

	/**
	 * 作者
	 */
	private String author;
	/**
	 * 標題
	 */
	private String title;
	/**
	 * 內容
	 */
	private String summary;
	/**
	 * 上架日期
	 */
	@JsonFormat(pattern="yyyy/MM/dd", timezone = "GMT+8")
	private Date createDt;
	
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public Date getCreateDt() {
		return createDt;
	}
	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}
	public String getPkRecommend() {
		return pkRecommend;
	}
	public void setPkRecommend(String pkRecommend) {
		this.pkRecommend = pkRecommend;
	}

	
	
}
