package com.e7learning.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.e7learning.api.annotation.E7ApiController;
import com.e7learning.api.domain.RestResult;
import com.e7learning.api.domain.param.FavouriteQueryParam;
import com.e7learning.api.domain.param.FavouriteRequestParam;
import com.e7learning.api.domain.result.FavouriteRequestResult;
import com.e7learning.api.domain.result.FavouriteRequestResult2;
import com.e7learning.api.domain.result.ProductResult;
import com.e7learning.api.service.FavouriteApiService;
import com.e7learning.common.exception.RestException;

@E7ApiController
public class FavouriteAipController {

	@Autowired
	private FavouriteApiService favouriteApiService;

	@RequestMapping(value = "/fav/check", method = { RequestMethod.POST })
	public RestResult<FavouriteRequestResult2> isFavourite(@RequestBody FavouriteRequestParam param)
			throws RestException {
		return new RestResult<FavouriteRequestResult2>(true, favouriteApiService.isFavourite(param));
	}

	@RequestMapping(value = "/fav/change", method = { RequestMethod.POST })
	public RestResult<FavouriteRequestResult2> changeFavourite(@RequestBody FavouriteRequestParam param)
			throws RestException {
		return new RestResult<FavouriteRequestResult2>(true, favouriteApiService.changeFavourite(param));
	}

	@RequestMapping(value = "/fav/query", method = { RequestMethod.POST })
	public RestResult<List<ProductResult>> queryFavourite(@RequestBody FavouriteQueryParam param) throws RestException {
		return new RestResult<List<ProductResult>>(true, favouriteApiService.queryFavourite(param));
	}
}
