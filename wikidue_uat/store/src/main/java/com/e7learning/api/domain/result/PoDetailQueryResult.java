package com.e7learning.api.domain.result;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.gson.FieldAttributes;

public class PoDetailQueryResult implements Serializable {

	/** 子訂單編號 */
	private Integer subOrderNo;
	
	/** 產品ID */
	private String productId;
	
	/*
	 * 產品名稱
	 */
	private String productName;
	/*
	 * 數量
	 */
	private String quantity;
	/*
	 * 單價
	 */
	private String unitPrice;
	/*
	 * 總價
	 */
	private String totalAmount;
	/*
	 * 圖片
	 */
	private String image;
	/*
	 * 商品簡述
	 */
	private String productDesc;
	/*
	 * 商品類型("COURSE","PROPS","CAMP")
	 */
	private String productType;
	
	/** 狀態 */
	private String status;
	
	/** 退貨原因 */
	private String returnReason;
	
	/** 退款金額 */
	private Integer returnAmount;
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm", timezone = "GMT+8")
	private Date updateDt;

	/** 商品備註 */
	private String remark;
	
	/** 物流公司 */
	private String logistics;
	
	/** 出貨單號 */
	private String logisticsNum;
	
	public Integer getSubOrderNo() {
		return subOrderNo;
	}


	public void setSubOrderNo(Integer subOrderNo) {
		this.subOrderNo = subOrderNo;
	}


	public PoDetailQueryResult() {
	}


	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		this.productName = productName;
	}


	public String getQuantity() {
		return quantity;
	}


	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}


	public String getUnitPrice() {
		return unitPrice;
	}


	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}


	public String getTotalAmount() {
		return totalAmount;
	}


	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}


	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}


	public String getProductDesc() {
		return productDesc;
	}


	public void setProductDesc(String productDesc) {
		this.productDesc = productDesc;
	}


	public String getProductType() {
		return productType;
	}


	public void setProductType(String productType) {
		this.productType = productType;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getReturnReason() {
		return returnReason;
	}


	public void setReturnReason(String returnReason) {
		this.returnReason = returnReason;
	}


	public Integer getReturnAmount() {
		return returnAmount;
	}


	public void setReturnAmount(Integer returnAmount) {
		this.returnAmount = returnAmount;
	}


	public Date getUpdateDt() {
		return updateDt;
	}


	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}


	public String getLogistics() {
		return logistics;
	}


	public void setLogistics(String logistics) {
		this.logistics = logistics;
	}


	public String getLogisticsNum() {
		return logisticsNum;
	}


	public void setLogisticsNum(String logisticsNum) {
		this.logisticsNum = logisticsNum;
	}


	public String getProductId() {
		return productId;
	}


	public void setProductId(String productId) {
		this.productId = productId;
	}
	
	
	

}
