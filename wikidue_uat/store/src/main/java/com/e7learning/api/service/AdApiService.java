package com.e7learning.api.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.e7learning.api.domain.param.AdQueryParam;
import com.e7learning.api.domain.result.AdQueryResult;
import com.e7learning.common.Constant;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.repository.AdvertisementRepostiory;
import com.e7learning.repository.model.AdRoleGrade;
import com.e7learning.repository.model.Advertisement;

@Service
public class AdApiService extends BaseService {

	private static final Logger LOG = Logger.getLogger(AdApiService.class);

	@Autowired
	private AdvertisementRepostiory advertisementRepostiory;

	@Autowired
	private ConfigService configService;

	public List<AdQueryResult> queryAd(AdQueryParam param) {
		List<AdQueryResult> resultList = new ArrayList<>();
		try {
			List<Advertisement> adList = advertisementRepostiory
					.findAll((Root<Advertisement> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
						List<Predicate> predicates = new ArrayList<>();
						query.distinct(true);

						// 已上架
						predicates.add(cb.equal(root.get("active"), Constant.TRUE));
						predicates.add(cb.lessThanOrEqualTo(root.get("publishSDate"), new Date()));
						predicates.add(cb.greaterThanOrEqualTo(root.get("publishEDate"), new LocalDate().toDate()));

						if (!StringUtils.isEmpty(param.getCategoryCode())) {
							predicates.add(cb.equal(root.get("category").get("categoryCode"), param.getCategoryCode()));
						} else {
							predicates.add(cb.isNull(root.get("category").get("categoryCode")));
						}

						if (!StringUtils.isEmpty(param.getRole())) {
							Join<Advertisement, AdRoleGrade> adRoleGradeJoin = root.join("roleGrades");
							predicates.add(cb.equal(adRoleGradeJoin.get("roleGradeCode"), param.getRole()));
						}

						if (!predicates.isEmpty()) {
							return cb.and(predicates.toArray(new Predicate[predicates.size()]));
						}

						return cb.conjunction();
					});
			if (adList != null) {
				int listSize = adList.size() > 3 ? 3 : adList.size();
				List<Integer> stock = new ArrayList<>();
				while (stock.size() < listSize) {
					int random = new Random().nextInt(listSize);
					if (stock.contains(random)) {
						continue;
					}
					stock.add(random);
					Advertisement ad = adList.get(random);
					AdQueryResult result = new AdQueryResult();
					result.setAdsName(ad.getAdsName());
					result.setExternalLink(ad.getExternalLink());
					if (!StringUtils.isEmpty(ad.getImageOneId())) {
						result.setImage1(configService.getHost() + configService.getImageApi() + ad.getImageOneId());
					}
					if (!StringUtils.isEmpty(ad.getImageTwoId())) {
						result.setImage2(configService.getHost() + configService.getImageApi() + ad.getImageTwoId());
					}
					resultList.add(result);
				}
			}
		} catch (Exception e) {
			LOG.error("exception", e);
		}
		return resultList;
	}
}
