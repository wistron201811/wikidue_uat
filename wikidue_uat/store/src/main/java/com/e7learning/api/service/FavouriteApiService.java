package com.e7learning.api.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.api.domain.param.FavouriteQueryParam;
import com.e7learning.api.domain.param.FavouriteRequestParam;
import com.e7learning.api.domain.param.ProductQueryParam;
import com.e7learning.api.domain.result.FavouriteRequestResult;
import com.e7learning.api.domain.result.FavouriteRequestResult2;
import com.e7learning.api.domain.result.ProductResult;
import com.e7learning.common.Constant;
import com.e7learning.common.enums.CategoryTypeEnum;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.RestException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.repository.UserFavouriteRepository;
import com.e7learning.repository.model.Camp;
import com.e7learning.repository.model.Product;
import com.e7learning.repository.model.ShareCenter;
import com.e7learning.repository.model.UserFavourite;

@Service
public class FavouriteApiService extends BaseService {

	private static final Logger log = Logger.getLogger(CampApiService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private UserFavouriteRepository userFavouriteRepository;

	@Autowired
	private ConfigService configService;

	public FavouriteRequestResult2 isFavourite(FavouriteRequestParam param) {
		// return new FavouriteRequestResult(userFavouriteRepository
		FavouriteRequestResult res = new FavouriteRequestResult(userFavouriteRepository
				.count((Root<UserFavourite> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
					List<Predicate> predicates = new ArrayList<>();

					if (StringUtils.isNotBlank(param.getProductId())) {
						predicates.add(cb.equal(root.get("product").get("pkProduct"), param.getProductId()));

					} else if (param.getCampId() != null) {
						predicates.add(cb.equal(root.get("camp").get("pkCamp"), param.getCampId()));
					} else if (param.getShareId() != null) {
						predicates.add(cb.equal(root.get("shareCenter").get("pkShare"), param.getShareId()));
					}
					predicates.add(cb.equal(root.get("active"), Constant.TRUE));
					predicates.add(cb.equal(root.get("createId"), getCurrentUserId()));

					if (!predicates.isEmpty()) {
						return cb.and(predicates.toArray(new Predicate[predicates.size()]));
					}
					return cb.conjunction();
				}) > 0);
		FavouriteRequestResult2 res1 = new FavouriteRequestResult2(res.isResult());
		res1.setCount(userFavouriteRepository.countByshareCenters(param.getShareId(), "1"));
		return res1;

	}

	public List<ProductResult> queryFavourite(FavouriteQueryParam param) {
		return userFavouriteRepository
				.findAll((Root<UserFavourite> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
					List<Predicate> predicates = new ArrayList<>();
					predicates.add(cb.equal(root.get("active"), Constant.TRUE));
					predicates.add(cb.equal(root.get("createId"), getCurrentUserId()));
					switch (param.getType()) {
					case COURSE:
						predicates.add(cb.isNotNull(root.get("product").get("course")));
						// 關鍵字查詢
						if (StringUtils.isNotBlank(param.getKeyword())) {
							predicates.add(cb.or(
									cb.like(root.get("product").get("productName"), "%" + param.getKeyword() + "%"),
									cb.like(root.get("product").get("course").get("courseSummary"),
											"%" + param.getKeyword() + "%")));
						}
						// 已上架
						predicates.add(cb.equal(root.get("product").get("active"), Constant.TRUE));
						predicates.add(cb.lessThanOrEqualTo(root.get("product").get("publishSDate"), new Date()));
						predicates.add(cb.greaterThanOrEqualTo(root.get("product").get("publishEDate"), new Date()));
						break;
					case PROPS:
						predicates.add(cb.isNotNull(root.get("product").get("props")));
						// 關鍵字查詢
						if (StringUtils.isNotBlank(param.getKeyword())) {
							predicates.add(cb.or(
									cb.like(root.get("product").get("productName"), "%" + param.getKeyword() + "%"),
									cb.like(root.get("product").get("props").get("propsSummary"),
											"%" + param.getKeyword() + "%")));
						}
						// 已上架
						predicates.add(cb.equal(root.get("product").get("active"), Constant.TRUE));
						predicates.add(cb.lessThanOrEqualTo(root.get("product").get("publishSDate"), new Date()));
						predicates.add(cb.greaterThanOrEqualTo(root.get("product").get("publishEDate"), new Date()));
						break;
					case CAMP:
						predicates.add(cb.isNotNull(root.get("camp")));
						// 關鍵字查詢
						if (StringUtils.isNotBlank(param.getKeyword())) {
							predicates.add(cb.or(
									cb.like(root.get("camp").get("campName"), "%" + param.getKeyword() + "%"),
									cb.like(root.get("camp").get("campIntroduction"), "%" + param.getKeyword() + "%")));
						}
						// 已上架
						predicates.add(cb.equal(root.get("camp").get("active"), Constant.TRUE));
						// 報名截止日之前
						predicates.add(cb.greaterThanOrEqualTo(root.get("camp").get("signUpEndDate"), new Date()));
						break;
					case SHARE:
						predicates.add(cb.isNotNull(root.get("shareCenter")));
						// 關鍵字查詢
						if (StringUtils.isNotBlank(param.getKeyword())) {
							predicates.add(cb.or(
									cb.like(root.get("shareCenter").get("shareName"), "%" + param.getKeyword() + "%"),
									cb.like(root.get("shareCenter").get("shareIntroduction"),
											"%" + param.getKeyword() + "%")));
						}
						// 已上架的
						predicates
								.add(cb.equal(root.get("shareCenter").get("approveStatus"), Constant.APPROVE_STATUS_Y));
						predicates.add(cb.equal(root.get("shareCenter").get("active"), Constant.TRUE));
						break;
					default:
						break;
					}

					if (!predicates.isEmpty()) {
						return cb.and(predicates.toArray(new Predicate[predicates.size()]));
					}

					return cb.conjunction();
				}, genSort(param)).stream().map(v -> toProductResult(v, param.getType())).collect(Collectors.toList());
	}

	private ProductResult toProductResult(UserFavourite userFavourite, CategoryTypeEnum type) {
		ProductResult productResult = new ProductResult();
		switch (type) {
		case COURSE:
			productResult.setPkProduct(userFavourite.getProduct().getPkProduct());
			productResult.setDiscountPrice(userFavourite.getProduct().getDiscountPrice());
			productResult.setPrice(userFavourite.getProduct().getPrice());
			productResult.setTitle(userFavourite.getProduct().getProductName());
			productResult.setSummary(userFavourite.getProduct().getCourse().getCourseSummary());
			productResult.setImage(configService.getHost() + configService.getImageApi()
					+ userFavourite.getProduct().getCourse().getImageOne());
			productResult.setQuantity(-1);
			break;
		case PROPS:
			productResult.setPkProduct(userFavourite.getProduct().getPkProduct());
			productResult.setDiscountPrice(userFavourite.getProduct().getDiscountPrice());
			productResult.setPrice(userFavourite.getProduct().getPrice());
			productResult.setTitle(userFavourite.getProduct().getProductName());
			productResult.setSummary(userFavourite.getProduct().getProps().getPropsSummary());
			if (StringUtils.isNotBlank(userFavourite.getProduct().getProps().getImageOneId()))
				productResult.setImage(configService.getHost() + configService.getImageApi()
						+ userFavourite.getProduct().getProps().getImageOneId());
			productResult.setQuantity(userFavourite.getProduct().getProps().getQuantity());
			break;
		case CAMP:
			productResult.setPkCamp(userFavourite.getCamp().getPkCamp());
			productResult.setTitle(userFavourite.getCamp().getCampName());
			productResult.setSummary(userFavourite.getCamp().getCampIntroduction());
			if (StringUtils.isNotBlank(userFavourite.getCamp().getImageId()))
				productResult.setImage(
						configService.getHost() + configService.getImageApi() + userFavourite.getCamp().getImageId());
			productResult.setQuantity(-1);
			productResult.setExternalLink(userFavourite.getCamp().getExternalLink());
			break;
		case SHARE:
			productResult.setPkShare(userFavourite.getShareCenter().getPkShare());
			productResult.setTitle(userFavourite.getShareCenter().getShareName());
			productResult.setSummary(userFavourite.getShareCenter().getShareIntroduction());
			if (StringUtils.isNotBlank(userFavourite.getShareCenter().getShareImage()))
				productResult.setImage(configService.getHost() + configService.getImageApi()
						+ userFavourite.getShareCenter().getShareImage());
			productResult.setQuantity(-1);
			break;
		default:
			break;
		}

		return productResult;
	}

	@Transactional(rollbackFor = { RestException.class }, noRollbackFor = { MessageException.class })
	public FavouriteRequestResult2 changeFavourite(FavouriteRequestParam param) {
		UserFavourite userFavourite = userFavouriteRepository
				.findAll((Root<UserFavourite> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
					List<Predicate> predicates = new ArrayList<>();

					if (StringUtils.isNotBlank(param.getProductId())) {
						predicates.add(cb.equal(root.get("product").get("pkProduct"), param.getProductId()));
					} else if (param.getCampId() != null) {
						predicates.add(cb.equal(root.get("camp").get("pkCamp"), param.getCampId()));
					} else if (param.getShareId() != null) {
						predicates.add(cb.equal(root.get("shareCenter").get("pkShare"), param.getShareId()));
					}
					predicates.add(cb.equal(root.get("createId"), getCurrentUserId()));

					if (!predicates.isEmpty()) {
						return cb.and(predicates.toArray(new Predicate[predicates.size()]));
					}
					return cb.conjunction();
				}).stream().findFirst().orElse(null);

		if (userFavourite == null) {
			// 第一次則新增一筆
			userFavourite = new UserFavourite();
			if (StringUtils.isNotBlank(param.getProductId())) {
				Product product = new Product();
				product.setPkProduct(param.getProductId());
				userFavourite.setProduct(product);
			} else if (param.getCampId() != null) {
				Camp camp = new Camp();
				camp.setPkCamp(param.getCampId());
				userFavourite.setCamp(camp);
			} else if (param.getShareId() != null) {
				ShareCenter shareCenter = new ShareCenter();
				shareCenter.setPkShare(param.getShareId());
				userFavourite.setShareCenter(shareCenter);
			}
			userFavourite.setActive(Constant.TRUE);
			userFavourite.setCreateDt(new Date());
			userFavourite.setCreateId(getCurrentUserId());
			userFavourite.setUpdateDt(userFavourite.getCreateDt());
			userFavouriteRepository.save(userFavourite);
		} else {
			userFavourite.setActive(Constant.TRUE.equals(userFavourite.getActive()) ? Constant.FLASE : Constant.TRUE);
			userFavouriteRepository.save(userFavourite);
		}
		return isFavourite(param);
	}

	private Sort genSort(FavouriteQueryParam param) {

		Sort sort = null;

		switch (param.getType()) {
		case COURSE:
		case PROPS: {
			if (param.getOrder() != null) {
				switch (param.getOrder()) {
				case PUBLISH_START_DATE_DESC:
					sort = new Sort(Direction.DESC, "product.publishSDate");
					break;
				case PUBLISH_START_DATE_ASC:
					sort = new Sort(Direction.ASC, "product.publishSDate");
					break;
				}
			} else
				sort = new Sort(Direction.DESC, "product.publishSDate");
		}
			break;
		case CAMP: {
			if (param.getOrder() != null) {
				switch (param.getOrder()) {
				case PUBLISH_START_DATE_DESC:
					sort = new Sort(Direction.DESC, "camp.activitySDate");
					break;
				case PUBLISH_START_DATE_ASC:
					sort = new Sort(Direction.ASC, "camp.activitySDate");
					break;
				}
			} else
				sort = new Sort(Direction.DESC, "camp.activitySDate");
		}
			break;
		case SHARE: {
			if (param.getOrder() != null) {
				switch (param.getOrder()) {
				case PUBLISH_START_DATE_DESC:
					sort = new Sort(Direction.DESC, "shareCenter.createDt");
					break;
				case PUBLISH_START_DATE_ASC:
					sort = new Sort(Direction.ASC, "shareCenter.createDt");
					break;
				}
			} else
				sort = new Sort(Direction.DESC, "shareCenter.createDt");
		}

		}
		return sort;
	}

}
