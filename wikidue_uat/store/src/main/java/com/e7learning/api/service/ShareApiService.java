package com.e7learning.api.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.e7learning.api.domain.param.AccusationRequestParam;
import com.e7learning.api.domain.param.ShareQueryParam;
import com.e7learning.api.domain.result.ShareResult;
import com.e7learning.common.Constant;
import com.e7learning.common.exception.RestException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.service.MailService;
import com.e7learning.repository.AccusationRepository;
import com.e7learning.repository.ShareCenterRepository;
import com.e7learning.repository.model.Accusation;
import com.e7learning.repository.model.ShareCenter;
import com.google.gson.Gson;

@Service
public class ShareApiService extends BaseService {

	private static final Logger log = Logger.getLogger(CategoryApiService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private ShareCenterRepository shareCenterRepository;

	@Autowired
	private AccusationRepository accusationRepository;

	@Autowired
	private ConfigService configService;

	@Autowired
	private MailService mailService;

	public Page<ShareResult> queryShareCenter(ShareQueryParam param) throws RestException {
		try {
			log.info(new Gson().toJson(param));

			Sort sort = new Sort(Direction.DESC, param.getOrder());
			int pageSize;
			if (param.getPageSize() != null) {
				pageSize = param.getPageSize();
			} else {
				pageSize = configService.getPageSize();
			}
			Pageable pageable = new PageRequest(param.getPage() - 1, pageSize, sort);

			Page<ShareCenter> products = shareCenterRepository
					.findAll((Root<ShareCenter> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
						List<Predicate> predicates = new ArrayList<>();
						query.distinct(true);

						// tag條件
						if (param.getTags() != null && param.getTags().length > 0) {
							predicates.add(cb.and(root.join("shareTags").get("tag").in(param.getTags())));
						}
						// 關鍵字查詢
						if (StringUtils.isNotBlank(param.getKeyword())) {
							predicates.add(cb.or(cb.like(root.get("shareName"), "%" + param.getKeyword() + "%"),
									cb.like(root.get("shareIntroduction"), "%" + param.getKeyword() + "%")));
						}
						// 已上架的
						predicates.add(cb.equal(root.get("approveStatus"), Constant.APPROVE_STATUS_Y));

						predicates.add(cb.equal(root.get("active"), Constant.TRUE));
						if (!predicates.isEmpty()) {
							return cb.and(predicates.toArray(new Predicate[predicates.size()]));
						}

						return cb.conjunction();
					}, pageable);
			log.info("getTotalPages:" + products.getTotalPages());
			log.info("getNumber:" + products.getNumber());
			log.info("getNumberOfElements:" + products.getNumberOfElements());
			log.info("getSize:" + products.getSize());
			log.info("getTotalElements:" + products.getTotalElements());
			return products.map(this::toProductResult);
		} catch (Exception e) {
			log.error("queryTag", e);
			throw new RestException(resources.getMessage("product.query.err", null, null));
		}
	}

	private ShareResult toProductResult(ShareCenter shareCenter) {
		ShareResult shareResult = new ShareResult();
		if (shareCenter != null) {
			shareResult.setAuthor(shareCenter.getShareUid());
			shareResult.setContent(shareCenter.getShareIntroduction());
			shareResult.setShareId(shareCenter.getPkShare());
			shareResult.setTitle(shareCenter.getShareName());
			shareResult.setImage(configService.getHost() + configService.getImageApi() + shareCenter.getShareImage());
		}

		return shareResult;
	}

	public void checkAccusation(AccusationRequestParam param) throws RestException {
		if (accusationRepository.countByShareCenterPkShareAndApproveStatusIsNull(param.getShareId()) > 0) {
			throw new RestException(resources.getMessage("accusation.exist", null, null));
		}
	}

	public void submitAccusation(AccusationRequestParam param) throws RestException {
		boolean success = false;
		try {
			Accusation accusation = new Accusation();
			accusation.setCreateDt(new Date());
			accusation.setInformer(getCurrentUserId());
			accusation.setReportReason(param.getReportReason());
			if (!shareCenterRepository.exists(param.getShareId()))
				throw new RestException(resources.getMessage("accusation.error", null, null));
			ShareCenter shareCenter = new ShareCenter();
			shareCenter.setPkShare(param.getShareId());
			accusation.setShareCenter(shareCenter);
			accusation.setUpdateDt(accusation.getCreateDt());
			accusation.setCreateId(getCurrentUserId());
			accusationRepository.save(accusation);
			success = true;
		} catch (RestException e) {
			log.error("submitAccusation", e);
			throw e;
		} catch (Exception e) {
			log.error("submitAccusation", e);
			throw new RestException(resources.getMessage("accusation.error", null, null));
		} finally {
			if (success) {
				ShareCenter shareCenter = shareCenterRepository.findOne(param.getShareId());
				if (shareCenter != null) {
					Date now = new Date();
					mailService.sendAccusationNotificationEmail(new String[] { shareCenter.getCreateId() }, null, now,
							shareCenter.getShareUid(), now, getCurrentUserId(), shareCenter.getShareName(),
							param.getReportReason());
				}
			}
		}

	}

}
