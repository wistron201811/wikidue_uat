package com.e7learning.tag.vo;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

/**
 * The persistent class for the TAG database table.
 * 
 */
public class TagVo implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	
	private String tag;

	private String tagName;

	private String tagLevel;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public String getTagLevel() {
		return tagLevel;
	}

	public void setTagLevel(String tagLevel) {
		this.tagLevel = tagLevel;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof TagVo) {
			TagVo source = (TagVo) obj;
			return StringUtils.equals(source.getTag(), this.getTag())
					&& StringUtils.equals(source.getTagLevel(), this.getTagLevel())
					&& StringUtils.equals(source.getTagName(), this.getTagName());
		}
		return false;
	}

}