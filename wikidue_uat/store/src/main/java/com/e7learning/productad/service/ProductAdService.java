package com.e7learning.productad.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.e7learning.common.Constant;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.productad.vo.ProductAdVo;
import com.e7learning.repository.ProductAdRepository;
import com.e7learning.repository.model.Course;
import com.e7learning.repository.model.Product;
import com.e7learning.repository.model.ProductAd;
import com.e7learning.repository.model.Props;

@Service
public class ProductAdService extends BaseService {

	private static final Logger LOG = Logger.getLogger(ProductAdService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private ConfigService configService;

	@Autowired
	private ProductAdRepository productAdRepository;

	public List<ProductAdVo> queryIndexProductAdList() {
		List<ProductAdVo> list = new ArrayList<>();
		try {
			Date now = new Date();
			Date endDate = new LocalDate().toDate();
//			List<ProductAd> queryList = productAdRepository
//					.findTop6ByPublishStartDateLessThanEqualAndPublishEndDateGreaterThanEqualAndActiveOrderByPublishStartDateDesc(
//							now, now, Constant.TRUE);
			/** 20181207 改成商品如果已下架就不要顯示 by Chris */
			Page<ProductAd> result = productAdRepository.findAll(
					(Root<ProductAd> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
						List<Predicate> predicates = new ArrayList<>();
						query.distinct(true);
						// ad 已上架
						predicates.add(cb.equal(root.get("active"), Constant.TRUE));
						predicates.add(cb.lessThanOrEqualTo(root.get("publishStartDate"), now));
						predicates.add(cb.greaterThanOrEqualTo(root.get("publishEndDate"), endDate));
						
						// 產品也是上架中
						predicates.add(cb.equal(root.get("product").get("active"), Constant.TRUE));
						predicates.add(cb.lessThanOrEqualTo(root.get("product").get("publishSDate"), now));
						predicates.add(cb.greaterThanOrEqualTo(root.get("product").get("publishEDate"), now));
						
						if (!predicates.isEmpty()) {
							return cb.and(predicates.toArray(new Predicate[predicates.size()]));
						}
						return cb.conjunction();
					}, new PageRequest(0, 6, new Sort(Direction.DESC, "publishStartDate")));
			List<ProductAd> queryList = result.getContent();
			if (queryList != null && !queryList.isEmpty()) {
				for (ProductAd ad : queryList) {
					ProductAdVo vo = new ProductAdVo();
					vo.setDescription(ad.getDescription());
					vo.setPublishEndDate(vo.getPublishEndDate());
					vo.setPublishStartDate(vo.getPublishStartDate());
					vo.setType(vo.getType());
					Product product = ad.getProduct();
					if (product != null) {
						vo.setDiscountPrice(product.getDiscountPrice());
						vo.setProductName(product.getProductName());
						vo.setPrice(product.getPrice());
						if (product.getCourse() != null) {
							Course course = product.getCourse();
							vo.setProductUrl(configService.getHost() + "/course/detail/" + product.getPkProduct());
							vo.setImageUrl(configService.getHost()+configService.getImageApi()+course.getImageOne());
						} else if (product.getProps() != null) {
							Props props = product.getProps();
							vo.setProductUrl(configService.getHost() + "/props/detail/" + product.getPkProduct());
							vo.setImageUrl(configService.getHost()+configService.getImageApi()+props.getImageOneId());
						}

					}
					list.add(vo);
				}
			}
		} catch (Exception e) {
			LOG.error("exception:", e);
		}
		return list;
	}

}
