package com.e7learning.productad.vo;

import java.io.Serializable;
import java.util.Date;

public class ProductAdVo implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer pkProductAd;
	
	private String productName;
	
	private Integer price;
	
	private Integer discountPrice;
	
	private String imageUrl;
	
	private Date publishStartDate;

	private Date publishEndDate;

	private String description;

	private String type;
	
	private String productUrl;

	public Integer getPkProductAd() {
		return pkProductAd;
	}

	public void setPkProductAd(Integer pkProductAd) {
		this.pkProductAd = pkProductAd;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Integer discountPrice) {
		this.discountPrice = discountPrice;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Date getPublishStartDate() {
		return publishStartDate;
	}

	public void setPublishStartDate(Date publishStartDate) {
		this.publishStartDate = publishStartDate;
	}

	public Date getPublishEndDate() {
		return publishEndDate;
	}

	public void setPublishEndDate(Date publishEndDate) {
		this.publishEndDate = publishEndDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getProductUrl() {
		return productUrl;
	}

	public void setProductUrl(String productUrl) {
		this.productUrl = productUrl;
	}

}
