package com.e7learning.camp.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.common.Constant;
import com.e7learning.common.service.BaseService;
import com.e7learning.course.service.CourseService;
import com.e7learning.repository.CampRepository;
import com.e7learning.repository.model.Camp;

@Service
public class CampService extends BaseService {

	private static final Logger LOG = Logger.getLogger(CourseService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private CampRepository campRepository;

	@Transactional(readOnly = true)
	public List<Camp> queryIndexCampList() {
		List<Camp> result = new ArrayList<>();
		try {
			List<Camp> list = campRepository.findAll((Root<Camp> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
				List<Predicate> predicates = new ArrayList<>();
				query.distinct(true);
				// 已上架
				predicates.add(cb.equal(root.get("active"), Constant.TRUE));
				predicates.add(cb.greaterThanOrEqualTo(root.get("signUpEndDate"), new Date()));

				if (!predicates.isEmpty()) {
					return cb.and(predicates.toArray(new Predicate[predicates.size()]));
				}

				return cb.conjunction();
			});
			if (list != null) {
				if (list.size() <= 4) {
					return list;
				} else {
					int count = 0;
					while (count < 4) {
						Camp camp = list.get(new Random().nextInt(list.size()));
						boolean exist = false;
						for (Camp tmp : result) {
							if (tmp.getPkCamp() == camp.getPkCamp()) {
								exist = true;
								break;
							}
						}
						if (!exist) {
							result.add(camp);
							count++;
						}
					}
				}
			}
		} catch (Exception e) {
			LOG.error("e", e);
		}

		return result;
	}

}
