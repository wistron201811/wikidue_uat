package com.e7learning.camp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/camp")
public class CampController {
	private static String HOME_PAGE = "campHomePage";
	
	@RequestMapping(value = "/home", method = { RequestMethod.GET })
	public String goHome(Model model) {
		return HOME_PAGE;
	}
}
