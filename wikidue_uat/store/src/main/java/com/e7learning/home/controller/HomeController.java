package com.e7learning.home.controller;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.web.savedrequest.DefaultSavedRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.e7learning.api.domain.param.ProductQueryParam;
import com.e7learning.api.domain.param.ShareQueryParam;
import com.e7learning.api.service.ProductApiService;
import com.e7learning.api.service.ShareApiService;
import com.e7learning.banner.service.BannerService;
import com.e7learning.camp.service.CampService;
import com.e7learning.common.Constant;
import com.e7learning.common.enums.CategoryTypeEnum;
import com.e7learning.common.service.ConfigService;
import com.e7learning.common.service.SsoService;
import com.e7learning.common.vo.SsoToken;
import com.e7learning.common.vo.SsoUserInfoResponse;
import com.e7learning.enums.ProductSortEnum;
import com.e7learning.productad.service.ProductAdService;
import com.e7learning.recommend.service.RecommendService;
import com.e7learning.videoad.service.VideoAdService;
import com.google.gson.Gson;

@Controller
@RequestMapping
public class HomeController {

	private static Logger log = Logger.getLogger(HomeController.class);

	private static final String LOGIN_PAGE = "login";

	private static final String PAGE_NOT_FOUND = "notFoundPage";

	private static final String ERROR_PAGE = "errorPage";

	private static String INDEX_PAGE = "indexPage";

	@Autowired
	private ProductApiService productApiService;

	@Autowired
	private VideoAdService videoAdService;

	@Autowired
	private ProductAdService productAdService;

	@Autowired
	private BannerService bannerService;

	@Autowired
	private ShareApiService shareApiService;

	@Autowired
	private RecommendService recommendService;

	@Autowired
	private CampService campService;

	@Autowired
	private ConfigService configService;

	@Autowired
	private SsoService ssoService;

	@Autowired
	private MessageSource resources;

	@RequestMapping(value = { "", "/", "/index" })
	public String goQuery(Model model) {
		log.info("goindex");
		try {
			// 首頁Banner輪播速度
			model.addAttribute("bannerAutoPlaySpeed", bannerService.getBannerAutoPlaySpeed());
			// 首頁Banner清單
			model.addAttribute("bannerList", bannerService.queryIndexBannerList());
			// 首頁影音廣告清單
			model.addAttribute("videoAd", videoAdService.queryRandomActiveVideAd());
			// 首頁主打商品清單
			model.addAttribute("productAdList", productAdService.queryIndexProductAdList());
			// 首頁課程清單
			ProductQueryParam courseParam = new ProductQueryParam();
			courseParam.setPageSize(4);
			courseParam.setType(CategoryTypeEnum.COURSE);
			model.addAttribute("courseList", productApiService.queryProduct(courseParam).getContent());
			// 首頁教具清單
			ProductQueryParam propsParam = new ProductQueryParam();
			propsParam.setPageSize(4);
			propsParam.setType(CategoryTypeEnum.PROPS);
			model.addAttribute("propsList", productApiService.queryProduct(propsParam).getContent());
			// 首頁分享園地清單
			ShareQueryParam shareParam = new ShareQueryParam();
			shareParam.setPageSize(4);
			shareParam.setOrder("createDt");
			model.addAttribute("shareList", shareApiService.queryShareCenter(shareParam).getContent());
			// 首頁營隊清單
			model.addAttribute("campList", campService.queryIndexCampList());
			// 首頁名人推薦清單
			model.addAttribute("recommendList", recommendService.queryIndexRecommendList());
		} catch (Exception e) {

		}
		return INDEX_PAGE;
	}

	@RequestMapping(value = { "/login" })
	public String goLogin(HttpServletRequest request, HttpSession session, Model model, String redirectUri) {
		model.addAttribute("ssoLoginPage", configService.getSsoLoginPage());
		model.addAttribute("clientId", configService.getSsoClientId());
		model.addAttribute("responseType", configService.getSsoLoginResponseType());
		model.addAttribute("redirectUri", configService.getHost() + configService.getSsoLoginRedirectUri());

		// DefaultSavedRequest savedRequest = (DefaultSavedRequest)
		// session.getAttribute("SPRING_SECURITY_SAVED_REQUEST");

		if (StringUtils.isNotEmpty(redirectUri)) {
			session.setAttribute("LOGIN_REDIRCT_URI", redirectUri);
		}
		return LOGIN_PAGE;
	}

	@RequestMapping(value = { "/signUp" })
	public String goSignUp(HttpServletRequest request, HttpSession session, Model model, String redirectUri) {
		model.addAttribute("ssoLoginPage", configService.getSsoSignUpPage() + "&oauthRedirectUrl=" + redirectUri);

		// DefaultSavedRequest savedRequest = (DefaultSavedRequest)
		// session.getAttribute("SPRING_SECURITY_SAVED_REQUEST");

		if (StringUtils.isNotEmpty(redirectUri)) {
			session.setAttribute("LOGIN_REDIRCT_URI", redirectUri);
		}
		return LOGIN_PAGE;
	}

	@RequestMapping(value = { "/login/auth" }, method = { RequestMethod.GET })
	public String auth(String code, String state, String series, HttpSession session, HttpServletResponse response) {
		log.info("code:" + code);
		log.info("state:" + state);
		log.info("series:" + series);

		try {
			if (resources.getMessage("sso.get.token.error", null, null)
					.equals(session.getAttribute(Constant.ERROR_MESSAGE_KEY))) {
				session.removeAttribute(Constant.ERROR_MESSAGE_KEY);
			}
			SsoToken ssoToken = ssoService.getToken(code, state, series,
					configService.getHost() + configService.getSsoLoginRedirectUri());
			// 取得使用者資訊
			if (StringUtils.isNoneBlank(ssoToken.getAccessToken())) {
				SsoUserInfoResponse ssoUserInfoResponse = ssoService.getUserInfo(ssoToken.getAccessToken());
				ssoService.authUser(ssoUserInfoResponse.getBody().getUserInfo(), ssoToken.getRefreshToken(), series,
						false);
				Map<String, String> data = new HashMap<>();
				data.put("accessToken", ssoToken.getAccessToken());
				data.put("series", series);
				data.put("refreshToken", ssoToken.getRefreshToken());
				Cookie cookie = new Cookie(Constant.COOKIE_WIKIDUE_TOKEN, new Gson().toJson(data));
//				cookie.setDomain(configService.getDomain());
				cookie.setPath("/");
				cookie.setMaxAge(60 * 60 * 24);
				response.addCookie(cookie);

				DefaultSavedRequest savedRequest = (DefaultSavedRequest) session
						.getAttribute("SPRING_SECURITY_SAVED_REQUEST");
				String redirectUri = (String) session.getAttribute("LOGIN_REDIRCT_URI");
				if (redirectUri != null) {
					return "redirect:" + redirectUri + "?d=" + Calendar.getInstance().getTimeInMillis();
				} else if (savedRequest != null) {
					return "redirect:" + savedRequest.getRedirectUrl() + "?d="
							+ Calendar.getInstance().getTimeInMillis();
				}
				return "redirect:/index?d=" + Calendar.getInstance().getTimeInMillis();
			}
		} catch (Exception e) {
			session.setAttribute("host", configService.getHost());
			session.setAttribute("series", series);
		}
		return "redirect:/login?d=" + Calendar.getInstance().getTimeInMillis();
	}

	@RequestMapping(value = { "/logout" })
	public String logout(HttpSession session, HttpServletResponse response) {
		log.info("/logout removeAttribute");
		session.removeAttribute("series");
		Cookie cookie = new Cookie(Constant.COOKIE_WIKIDUE_TOKEN, StringUtils.EMPTY);
//		cookie.setDomain(configService.getDomain());
		cookie.setMaxAge(0);
		cookie.setPath("/");
		response.addCookie(cookie);
		return "redirect:/logout/success?d=" + Calendar.getInstance().getTimeInMillis();
	}

	@RequestMapping(value = "/404")
	public String go404() {
		return PAGE_NOT_FOUND;
	}

	@RequestMapping(value = "/error")
	public String goErrpage() {
		return ERROR_PAGE;
	}
}
