package com.e7learning.learningcenter.vo;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

public class LastWatchCourseVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 課程編號
	 */
	private Integer courseId;

	/**
	 * 課程名稱
	 */
	private String courseName;

	/**
	 * 圖片ID
	 */
	private String imageId;

	/**
	 * 進度 %數
	 */
	private int progress;

	/**
	 * 上次觀看日期
	 */
	private Date lastWatchDt;

	/**
	 * 上次觀看的課程影片Id
	 */
	private String lastWatchVideoId;

	/**
	 * 上次觀看的課程影片名稱
	 */
	private String lastWatchVideoName;

	/**
	 * 課程時數(hour)
	 */
	private int courseDurationHour;

	/**
	 * 課程時數(minute)
	 */
	private int courseDurationMinute;

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}

	public Date getLastWatchDt() {
		return lastWatchDt;
	}

	public void setLastWatchDt(Date lastWatchDt) {
		this.lastWatchDt = lastWatchDt;
	}

	public String getLastWatchVideoId() {
		return lastWatchVideoId;
	}

	public void setLastWatchVideoId(String lastWatchVideoId) {
		this.lastWatchVideoId = lastWatchVideoId;
	}

	public String getLastWatchVideoName() {
		return lastWatchVideoName;
	}

	public void setLastWatchVideoName(String lastWatchVideoName) {
		this.lastWatchVideoName = lastWatchVideoName;
	}

	public int getCourseDurationHour() {
		return courseDurationHour;
	}

	public void setCourseDurationHour(int courseDurationHour) {
		this.courseDurationHour = courseDurationHour;
	}

	public int getCourseDurationMinute() {
		return courseDurationMinute;
	}

	public void setCourseDurationMinute(int courseDurationMinute) {
		this.courseDurationMinute = courseDurationMinute;
	}

	public Integer getCourseId() {
		return courseId;
	}

	public void setCourseId(Integer courseId) {
		this.courseId = courseId;
	}

	public String getLastWatchVideoNameBrief() {
		if (!StringUtils.isEmpty(lastWatchVideoName) && lastWatchVideoName.length() > 9) {
			return lastWatchVideoName.substring(0, 9)+"...";
		}
		return lastWatchVideoName;
	}

}
