package com.e7learning.learningcenter.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.e7learning.common.exception.ServiceException;
import com.e7learning.learningcenter.form.LearningCenterForm;
import com.e7learning.learningcenter.service.LearningCenterService;

@Controller
@RequestMapping(value = "/learningCenter")
public class LearningCenterController {

	private static final String PAGE = "learningCenterPage";

	private static final Logger LOG = Logger.getLogger(LearningCenterController.class);

	@Autowired
	private LearningCenterService learningCenterService;

	@RequestMapping(method = { RequestMethod.GET,RequestMethod.POST })
	public String home(Model model, @ModelAttribute("learningCenterForm") LearningCenterForm form) {
		try {
			model.addAttribute("courseList", learningCenterService.queryLearningCenter(form));
			model.addAttribute("lastWatchCourse", learningCenterService.queryLastWatchCourse());
		} catch (ServiceException e) {

		}
		return PAGE;
	}

}
