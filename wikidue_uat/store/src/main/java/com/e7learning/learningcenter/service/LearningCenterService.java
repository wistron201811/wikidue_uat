package com.e7learning.learningcenter.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.ConfigService;
import com.e7learning.course.service.CourseService;
import com.e7learning.course.vo.CourseVideoVo;
import com.e7learning.course.vo.CourseVo;
import com.e7learning.learningcenter.form.LearningCenterForm;
import com.e7learning.learningcenter.vo.LastWatchCourseVo;
import com.e7learning.repository.CourseRepository;
import com.e7learning.repository.CourseVideoRepository;
import com.e7learning.repository.LearningProgressRepository;
import com.e7learning.repository.UserCourseRepository;
import com.e7learning.repository.model.Course;
import com.e7learning.repository.model.CourseVideo;
import com.e7learning.repository.model.LearningProgress;
import com.e7learning.repository.model.UserCourse;

@Service
public class LearningCenterService extends BaseService {

	private static final Logger log = Logger.getLogger(LearningCenterService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private UserCourseRepository userCourseRepository;

	@Autowired
	private ConfigService configService;

	@Autowired
	private LearningProgressRepository learningProgressRepository;

	@Autowired
	private CourseVideoRepository courseVideoRepository;

	@Autowired
	private CourseRepository courseRepository;

	@Autowired
	private CourseService courseService;

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public List<LastWatchCourseVo> queryLearningCenter(LearningCenterForm form) throws ServiceException {
		try {
			List<UserCourse> userCourses = userCourseRepository
					.findAll((Root<UserCourse> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
						List<Predicate> predicates = new ArrayList<>();
						query.distinct(true);

						// 已上架的
						predicates.add(cb.equal(root.get("userId"), getCurrentUserId()));
						if (!StringUtils.isEmpty(form.getKeyword())) {
							predicates
									.add(cb.like(root.get("course").get("courseName"), "%" + form.getKeyword() + "%"));
						}

						if (!predicates.isEmpty()) {
							return cb.and(predicates.toArray(new Predicate[predicates.size()]));
						}
						return cb.conjunction();
					});
			if (userCourses != null) {
				return userCourses.stream().map(v -> toLastWatchCourseVo(v.getCourse())).collect(Collectors.toList());
			} else {
				return new ArrayList<>();
			}
		} catch (Exception e) {
			log.error("queryLearningCenter", e);
			throw new ServiceException(resources.getMessage("product.query.err", null, null));
		}
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public LastWatchCourseVo queryLastWatchCourse() {
		try {
			List<LearningProgress> list = learningProgressRepository
					.findAll((Root<LearningProgress> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
						List<Predicate> predicates = new ArrayList<>();
						query.distinct(true);

						// 已上架的
						predicates.add(cb.equal(root.get("userId"), getCurrentUserId()));
						predicates.add(cb.isNotNull(root.get("lastWatchDt")));

						if (!predicates.isEmpty()) {
							return cb.and(predicates.toArray(new Predicate[predicates.size()]));
						}
						return cb.conjunction();
					}, new Sort(Direction.DESC, "lastWatchDt"));

			if (list != null && !list.isEmpty()) {
				LearningProgress lp = list.get(0);
				if (lp != null && lp.getFkCourseVideo() != null) {
					CourseVideo courseVideo = courseVideoRepository.findOne(lp.getFkCourseVideo());
					if (courseVideo != null) {
						Course tmp = courseVideo.getCourseChapter().getCourse();
						if (tmp != null) {
							return toLastWatchCourseVo(tmp);
							// CourseVo courseVo =
							// courseService.generateVo(tmp);
							// LastWatchCourseVo vo = new LastWatchCourseVo();
							// vo.setCourseId(tmp.getPkCourse());
							// vo.setCourseName(tmp.getCourseName());
							// vo.setImageId(tmp.getImageOne());
							// vo.setLastWatchDt(lp.getLastWatchDt());
							// vo.setLastWatchVideoId(
							// courseVideo.getVideo() != null ?
							// courseVideo.getVideo().getPkVideo() : null);
							// vo.setLastWatchVideoName(courseVideo.getCourseVideoName());
							// int totalDurationSecond = 0;
							// List<Video> videoList = new ArrayList<>();
							// tmp.getCourseChapterList().size();
							// for (CourseChapter chapter :
							// tmp.getCourseChapterList()) {
							// chapter.getCourseVideoList().size();
							// for (CourseVideo cv :
							// chapter.getCourseVideoList()) {
							// if (cv.getVideo() != null) {
							// videoList.add(cv.getVideo());
							// totalDurationSecond +=
							// (cv.getVideo().getDuration() != null
							// ? cv.getVideo().getDuration()
							// : 0);
							// }
							// }
							// }
							// if (!videoList.isEmpty()) {
							// vo.setProgress(courseVo.getProgress());
							// }
							// int totalMin = totalDurationSecond / 60;
							// vo.setCourseDurationHour(totalMin / 60 > 0 ?
							// ((totalMin / 60) - 1) : 0);
							// vo.setCourseDurationMinute(totalMin % 60);
							//
							// return vo;
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("queryLastWatchCourse", e);
		}
		return null;
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	private LastWatchCourseVo toLastWatchCourseVo(Course tmp) {
		LastWatchCourseVo vo = new LastWatchCourseVo();
		if (tmp != null) {
			CourseVo courseVo = courseService.generateVo(null, tmp);
			courseVo = courseService.addProgess(courseVo);
			vo.setCourseId(tmp.getPkCourse());
			vo.setCourseName(tmp.getCourseName());
			vo.setImageId(tmp.getImageOne());
			if (courseVo.getCourseChapterList().stream().findFirst().isPresent() && !CollectionUtils
					.isEmpty(courseVo.getCourseChapterList().stream().findFirst().get().getCourseVideoList())) {
				CourseVideoVo lastWatchVideo = courseVo.getCourseChapterList().stream().findFirst().get()
						.getCourseVideoList().stream().filter(video -> video.getLastWatchDt() != null)
						// .sorted(Comparator.comparing(CourseVideoVo::getLastWatchDt)).findFirst().orElse(null);
						.sorted((f1, f2) -> Long.compare(
								f2.getLastWatchDt() != null ? f2.getLastWatchDt().getTime() : 0,
								f1.getLastWatchDt() != null ? f1.getLastWatchDt().getTime() : 0))
						.findFirst().orElse(null);
				if (lastWatchVideo != null) {
					// 最後觀看影片
					vo.setLastWatchDt(lastWatchVideo.getLastWatchDt());
					vo.setLastWatchVideoId(lastWatchVideo.getVideo().getVideoId());
					vo.setLastWatchVideoName(lastWatchVideo.getCourseVideoName());
				}
			}

			int totalDurationSecond = courseVo.getCourseChapterList().stream()
					.mapToInt(v -> v.getCourseVideoList() != null ? v.getCourseVideoList().stream()
							.mapToInt(video -> video.getVideoDuration() != null ? video.getVideoDuration() : 0)
							.reduce((total, progress) -> total + progress).getAsInt() : 0)
					.reduce((total, progress) -> total + progress).getAsInt();
			vo.setProgress(courseVo.getProgress());
			int totalMin = totalDurationSecond / 60;
			vo.setCourseDurationHour(totalMin / 60 > 0 ? (totalMin / 60)  : 0);
			vo.setCourseDurationMinute(totalMin % 60);
		}
		return vo;
	}
	
}
