package com.e7learning.footer.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FooterController {

	private static final String ABOUT_PAGE = "aboutPage";

	private static final String FAQ_PAGE = "faqPage";

	@RequestMapping(value = "/about")
	public String goAbout() {
		return ABOUT_PAGE;
	}
	
	@RequestMapping(value = "/faq")
	public String goFaq() {
		return FAQ_PAGE;
	}
}
