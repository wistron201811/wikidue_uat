package com.e7learning.props.form;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import com.e7learning.repository.model.Vendor;

public class PropsForm {
	
	/**
	 * 產品PK
	 */
	private String productPk;
	/**
	 * 商品PK
	 */
	private int pkProps;
	/**
	 * 商品名稱
	 */
	private String propsName;
	/**
	 * 商品數量
	 */
	private String quantity;
	/**
	 * 上架日期
	 */
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date publishSDate;
	/**
	 * 下架日期
	 */
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date publishEDate;
	/**
	 * 圖片一
	 */
	private MultipartFile photoOneId;
	/**
	 * 圖片二
	 */
	private MultipartFile photoTwoId;
	/**
	 * 圖片三
	 */
	private MultipartFile photoThirdId;
	/**
	 * 圖片四
	 */
	private MultipartFile photoFourthId;
	/**
	 * 原價
	 */
	private String price;
	/**
	 * 優惠價
	 */
	private String discountPrice;
	/**
	 * 注意事項
	 */
	private String notice;
	/**
	 * 商品簡述
	 */
	private String propsSummary;
	/**
	 * 商品說明
	 */
	private String propsIntroduction;

	/**
	 * 啟用
	 */
	private String active;

	private String materialNum;

	private String costPrice;

	private String vendorUid;

	private List<Vendor> vendorList;

	public PropsForm() {
	}

	public int getPkProps() {
		return pkProps;
	}

	public void setPkProps(int pkProps) {
		this.pkProps = pkProps;
	}

	public String getPropsName() {
		return propsName;
	}

	public void setPropsName(String propsName) {
		this.propsName = propsName;
	}

	public Date getPublishSDate() {
		return publishSDate;
	}

	public void setPublishSDate(Date publishSDate) {
		this.publishSDate = publishSDate;
	}

	public Date getPublishEDate() {
		return publishEDate;
	}

	public void setPublishEDate(Date publishEDate) {
		this.publishEDate = publishEDate;
	}

	public MultipartFile getPhotoOneId() {
		return photoOneId;
	}

	public void setPhotoOneId(MultipartFile photoOneId) {
		this.photoOneId = photoOneId;
	}

	public MultipartFile getPhotoTwoId() {
		return photoTwoId;
	}

	public void setPhotoTwoId(MultipartFile photoTwoId) {
		this.photoTwoId = photoTwoId;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(String discountPrice) {
		this.discountPrice = discountPrice;
	}

	public String getNotice() {
		return notice;
	}

	public void setNotice(String notice) {
		this.notice = notice;
	}

	public String getPropsSummary() {
		return propsSummary;
	}

	public void setPropsSummary(String propsSummary) {
		this.propsSummary = propsSummary;
	}

	public String getPropsIntroduction() {
		return propsIntroduction;
	}

	public void setPropsIntroduction(String propsIntroduction) {
		this.propsIntroduction = propsIntroduction;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getMaterialNum() {
		return materialNum;
	}

	public void setMaterialNum(String materialNum) {
		this.materialNum = materialNum;
	}

	public String getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(String costPrice) {
		this.costPrice = costPrice;
	}

	public String getVendorUid() {
		return vendorUid;
	}

	public void setVendorUid(String vendorUid) {
		this.vendorUid = vendorUid;
	}

	public MultipartFile getPhotoThirdId() {
		return photoThirdId;
	}

	public void setPhotoThirdId(MultipartFile photoThirdId) {
		this.photoThirdId = photoThirdId;
	}

	public MultipartFile getPhotoFourthId() {
		return photoFourthId;
	}

	public void setPhotoFourthId(MultipartFile photoFourthId) {
		this.photoFourthId = photoFourthId;
	}

	public List<Vendor> getVendorList() {
		return vendorList;
	}

	public void setVendorList(List<Vendor> vendorList) {
		this.vendorList = vendorList;
	}

	public String getProductPk() {
		return productPk;
	}

	public void setProductPk(String productPk) {
		this.productPk = productPk;
	}

	
}
