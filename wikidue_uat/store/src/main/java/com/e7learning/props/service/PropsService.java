package com.e7learning.props.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.e7learning.common.Constant;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.service.BaseService;
import com.e7learning.common.service.FileStorageService;
import com.e7learning.course.vo.CourseVo;
import com.e7learning.props.form.PropsForm;
import com.e7learning.repository.CartRepository;
import com.e7learning.repository.ProductRepository;
import com.e7learning.repository.PropsRepository;
import com.e7learning.repository.model.Cart;
import com.e7learning.repository.model.Product;
import com.e7learning.repository.model.Props;

@Service
public class PropsService extends BaseService {

	private static final Logger LOG = Logger.getLogger(PropsService.class);

	@Autowired
	private MessageSource resources;

	@Autowired
	private PropsRepository propsRepository;

	@Autowired
	private CartRepository cartRepository;

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private FileStorageService fileStorageService;

	public List<Props> queryIndexPropsList() {
		try {
			Date now = new Date();
			return propsRepository
					.findTop4ByPublishSDateLessThanEqualAndPublishEDateGreaterThanEqualAndActiveOrderByPublishSDateDesc(
							now, new LocalDate().toDate(), Constant.TRUE);
		} catch (Exception e) {
			LOG.error("exception:", e);
		}
		return null;
	}

	public PropsForm findAvailableByMaterialNum(String materialNum) throws ServiceException {
		try {
			List<Product> products = productRepository
					.findAll((Root<Product> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
						List<Predicate> predicates = new ArrayList<>();
						query.distinct(true);
						predicates.add(cb.isNotNull(root.get("props")));
						predicates.add(cb.equal(root.get("props").get("materialNum"), materialNum));
						// 已上架
						predicates.add(cb.equal(root.get("active"), Constant.TRUE));
						predicates.add(cb.lessThanOrEqualTo(root.get("publishSDate"), new Date()));
						predicates.add(cb.greaterThanOrEqualTo(root.get("publishEDate"), new Date()));
						return cb.and(predicates.toArray(new Predicate[predicates.size()]));
					});
			if (!products.isEmpty()) {
				return getPropsByProductPk(products.get(0).getPkProduct());
			}
		} catch (Exception e) {
			LOG.error("exception:", e);
			throw new ServiceException(resources.getMessage("op.query.error", null, null));
		}
		return null;
	}

	public PropsForm getPropsByProductPk(String productPk) throws ServiceException {
		if (StringUtils.isNotBlank(productPk)) {
			Product product = productRepository.findOne(productPk);
			try {
				if (product != null && product.getProps() != null) {
					PropsForm form = new PropsForm();
					Props props = product.getProps();
					BeanUtils.copyProperties(form, props);
					// 使用Product的價格
					form.setPrice(product.getPrice() + "");
					form.setDiscountPrice(product.getDiscountPrice() + "");
					form.setProductPk(productPk);

					form.setPhotoOneId(fileStorageService.getFileStorage(props.getImageOneId()).orElse(null));
					form.setPhotoTwoId(fileStorageService.getFileStorage(props.getImageTwoId()).orElse(null));
					form.setPhotoThirdId(fileStorageService.getFileStorage(props.getImageThirdId()).orElse(null));
					form.setPhotoFourthId(fileStorageService.getFileStorage(props.getImageFourthId()).orElse(null));
					return form;
				}
			} catch (Exception e) {
				LOG.error("exception:", e);
				throw new ServiceException(resources.getMessage("op.query.error", null, null));
			}
		}
		return null;
	}

}
