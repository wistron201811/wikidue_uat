package com.e7learning.props.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.e7learning.cart.service.CartService;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.props.form.PropsForm;
import com.e7learning.props.service.PropsService;

@Controller
@RequestMapping(value = "/props")
public class PropsController {

	public static final String FORM_BEAN_NAME = "propsForm";
	private static String HOME_PAGE = "propsHomePage";
	private static final String DETAIL_PAGE = "propsDetailPage";

	@Autowired
	private PropsService propsService;

	@Autowired
	private CartService cartService;

	@RequestMapping(value = "/home", method = { RequestMethod.GET })
	public String goHome(Model model) {
		return HOME_PAGE;
	}

	@RequestMapping(value = "/mn/{materialNum}", method = { RequestMethod.GET })
	public String goDetailByMaterialNum(@PathVariable("materialNum") String materialNum, Model model) {
		try {

			PropsForm form = propsService.findAvailableByMaterialNum(materialNum);
			if (form == null)
				return "redirect:/props/home";
			model.addAttribute(FORM_BEAN_NAME, form);
			model.addAttribute("existCart",
					StringUtils.isNotBlank(form.getProductPk()) ? cartService.existCart(form.getProductPk()) : false);
		} catch (ServiceException e) {
			return "redirect:/props/home";
		}
		return DETAIL_PAGE;
	}

	@RequestMapping(value = "/detail/{productId}", method = { RequestMethod.GET })
	public String goDetail(Model model, @PathVariable("productId") String productId) {
		// PoQueryParam param = new PoQueryParam();
		// param.setPageNo(1);
		// param.setMainOrderNo("A1234567890");
		// purchaseOrderService.getPoList(param);
		// purchaseOrderService.getPoDetail(param);
		try {
			model.addAttribute("existCart", cartService.existCart(productId));
			model.addAttribute(FORM_BEAN_NAME, propsService.getPropsByProductPk(productId));
		} catch (ServiceException e) {
			return "redirect:/props/home";
		}
		return DETAIL_PAGE;
	}
}
