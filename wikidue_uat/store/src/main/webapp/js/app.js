var myApp = angular
    .module('myApp', ["bsLoadingOverlay"])
    .run(function (bsLoadingOverlayService) {
        bsLoadingOverlayService.setGlobalConfig({
            templateUrl: '/store/js/lib/angular-loading-overlay/loading-overlay-template.html'
        });
    }).factory(
        "restClientService",
        function ($http, bsLoadingOverlayService) {
            return {
                post: function (apiUrl, loadingId, param, succ, err) {
                    if (loadingId) {
                        bsLoadingOverlayService.start({
                            referenceId: loadingId
                        });
                    } else {
                        bsLoadingOverlayService.start();
                    }
                    // console.log("POST param:" + JSON.stringify(param));
                    $http({
                        method: 'POST',
                        url: apiUrl,
                        data: param
                    }).then(function (response) {
                        if (loadingId) {
                            bsLoadingOverlayService.stop({
                                referenceId: loadingId
                            });
                        } else {
                            bsLoadingOverlayService.stop();
                        }
                        // console.log("success response:" + JSON.stringify(response));
                        if (response.data.success) {
                            succ(response.data.result);
                        } else {
                            var errmsg = response.data.message;
                            if (!errmsg && response.data.indexOf('登入頁') > 0) {
                                errmsg = '請先登入會員';
                            }
                            if (typeof err != 'function') {
                                // alert(errmsg || '系統錯誤');
                                BootstrapDialog.show({
                                    title: '提示訊息',
                                    type: BootstrapDialog.TYPE_WARNING,
                                    message: errmsg || '系統錯誤'
                                });
                            } else {
                                err(errmsg || '系統錯誤');
                            }
                        }
                    }, function (response) {
                        if (loadingId) {
                            bsLoadingOverlayService.stop({
                                referenceId: loadingId
                            });
                        } else {
                            bsLoadingOverlayService.stop();
                        }
                        console.error('http status = [' + response.status + ']');
                        console.error("fail response:" + JSON.stringify(response));
                        if (typeof err != 'function') {
                            //alert("查詢失敗[" + response.status + ']');
                            BootstrapDialog.show({
                                title: '提示訊息',
                                type: BootstrapDialog.TYPE_WARNING,
                                message: '查詢失敗'
                            });
                        } else {
                            if (response.data)
                                err(response.data.message);
                            else
                                err(('連線失敗[' + response.status + ']'));
                        }
                    });
                }
            };
        }).factory(
            "cartService",
            function (restClientService, $rootScope) {
                return {
                    addCart: function (param, succ, err) {
                        restClientService.post(cartAddApi, 'cart-block',
                            param, function (data) {
                                $rootScope.$emit('refreshCart');
                                succ(data);
                            }, function (msg) {
                                err(msg);
                            }
                        )

                    },
                    queryCart: function (param, succ, err) {
                        restClientService.post(cartQueryApi, 'cart-block',
                            param, function (data) {
                                succ(data);
                            }, function (msg) {
                                err(msg);
                            }
                        )
                    }, removeCart: function (param, succ, err) {
                        restClientService.post(cartRemoveApi, 'cart-block',
                            param, function (data) {
                                $rootScope.$emit('refreshCart');
                                succ(data);
                            }, function (msg) {
                                err(msg);
                            }
                        )

                    }, editCart: function (param, block, succ, err) {
                        restClientService.post(cartEditApi, block,
                            param, function (data) {
                                succ(data);
                            }, function (msg) {
                                err(msg);
                            }
                        )

                    }, addCartNoRefresh: function (param, succ, err) {
                        param.checkExist = false
                        restClientService.post(cartAddApi, 'cart-block',
                            param, function (data) {
                                succ(data);
                            }, function (msg) {
                                err(msg);
                            }
                        )

                    }
                };
            }).factory(
                "favService", function (restClientService) {
                    return {
                        checkFav: function (param, succ, err) {
                            restClientService.post(favCheckApi, 'fav-block',
                                param, function (data) {
                                    succ(data);
                                }, function (msg) {
                                    if (err) {
                                        err(msg);
                                    }
                                }
                            )

                        },
                        updateFav: function (param, succ, err) {
                            restClientService.post(favChangeApi, 'fav-block',
                                param, function (data) {
                                    succ(data);
                                }, function (msg) {
                                    if (err) {
                                        err(msg);
                                    } else {
                                        BootstrapDialog.show({
                                            title: '提示訊息',
                                            type: BootstrapDialog.TYPE_WARNING,
                                            message: msg
                                        });
                                    }

                                }
                            )

                        }
                    };
                }).filter('range', function () {
                    return function (input, total, start) {
                        total = parseInt(total);

                        for (var i = start || 1; i <= total; i++) {
                            input.push(i);
                        }

                        return input;
                    };
                }).filter('addGrade', function () {
                    return function (data) {
                        var grade = "";
                        switch (data.tag.split("-")[0]) {
                            case "0001":
                                grade = "一年級-";
                                break;
                            case "0002":
                                grade = "二年級-";
                                break;
                            case "0003":
                                grade = "三年級-";
                                break;
                            case "0004":
                                grade = "四年級-";
                                break;
                            case "0005":
                                grade = "五年級-";
                                break;
                            case "0006":
                                grade = "六年級-";
                                break;
                            case "0007":
                                grade = "七年級-";
                                break;
                            case "0008":
                                grade = "八年級-";
                                break;
                            case "0009":
                                grade = "九年級-";
                                break;
                            case "0010":
                                grade = "高一-";
                                break;
                            case "0011":
                                grade = "高二-";
                                break;
                            case "0012":
                                grade = "高三-";
                                break;

                            default:
                                break;
                        }
                        return grade + data.name;
                    };
                }).filter('trustAsHtml', ['$sce', function ($sce) {
                    return function (text) {
                        return $sce.trustAsHtml(text);
                    };
                }]).filter('newline', [function ($sce) {
                    return function (text) {
                        return text.replace('\r\n', '<br>');
                    };
                }]).filter('trustAsResourceUrl', ['$sce', function ($sce) {
                    return function (val) {
                        return $sce.trustAsResourceUrl(val);
                    };
                }]).filter('secondsToDateTime', [function () {
                    return function (seconds) {
                        return new Date(1970, 0, 1).setSeconds(seconds);
                    };
                }]).filter('cut', [function () {
                    /** Usage: {{some_text | cut:true:100:' ...'}} */
                    return function (value, wordwise, max, tail) {
                        if (!value) return '';

                        max = parseInt(max, 10);
                        if (!max) return value;
                        if (value.length <= max) return value;

                        value = value.substr(0, max);
                        if (wordwise) {
                            var lastspace = value.lastIndexOf(' ');
                            if (lastspace !== -1) {
                                //Also remove . and , so its gives a cleaner result.
                                if (value.charAt(lastspace - 1) === '.' || value.charAt(lastspace - 1) === ',') {
                                    lastspace = lastspace - 1;
                                }
                                value = value.substr(0, lastspace);
                            }
                        }
                        return value + (tail || ' …');
                    };
                }]).directive("showMore", ['$compile', '$timeout', function ($compile, $timeout) {
                    return {
                        scope: { block: '=block' },
                        restrict: "AEC",
                        link: function (scope, element, attrs) {
                            var vm = scope.vm = {};
                            vm.element = element
                            vm.uuid = '_' + Math.random().toString(36).substring(3, 8)
                            element.attr('id', vm.uuid);
                            scope.$watch("block", function (newValue, oldValue) {
                                // console.log('offsetHeight:' + element[0].offsetHeight);
                                // console.log('scrollHeight:' + element[0].scrollHeight);
                                //console.log('height:' + document.querySelector('#' + vm.uuid).offsetHeight);
                                element.removeClass('one-line-collapse');
                                // console.log(element.parent());
                                if (element.parent().parent()[0].querySelector('.wid_60px'))
                                    element.parent().parent()[0].querySelector('.wid_60px').remove();
                                $timeout(function () {
                                    checkHeght();
                                }, 50)

                            });
                            function checkHeght() {
                                var height = document.querySelector('#' + vm.uuid).offsetHeight;
                                if (height > 40) {
                                    var el = $compile('<td ng-click="vm.showMore()" class="vtop rt wid_60px"><a href="javascript:;" class="btn btn-default btn-sm more-btn">{{vm.show?"更多":"更少"}}<i ng-class=\'{"icon-expand":vm.show,"icon-pack-up":!vm.show}\'></i></a></td>')(scope);
                                    element.parent().parent().append(el);
                                    element.addClass('one-line-collapse');
                                    vm.show = true;
                                } else {
                                    element.removeClass('one-line-collapse');
                                    vm.show = false;
                                    if (element.parent().parent()[0].querySelector('.wid_60px'))
                                        element.parent().parent()[0].querySelector('.wid_60px').remove();
                                }
                            }
                            vm.showMore = function () {
                                vm.show = !vm.show
                                if (vm.show) {
                                    vm.element.addClass('one-line-collapse');
                                } else {
                                    vm.element.removeClass('one-line-collapse');
                                }

                            }
                        }


                    }
                }]).directive("e7Pagination", ['$compile', '$timeout', function ($compile, $timeout) {
                    return {
                        scope: {
                            page: '=page',
                            queryPage: '=fun',
                            pageClass: '=pageclass'
                        },
                        templateUrl: '/store/js/pagination-footer.html',
                        restrict: "AEC",
                        link: function (scope, element, attrs) {
                            var vm = scope.vm = {};
                        }
                    }
                }]);


Array.prototype.pushUnique = function (val) {
    var tmp = this;
    var isDupilcate = false;
    for (i = 0; i < tmp.length; i++) {
        // var tmpJ = tmp[i];
        // delete tmpJ.$$hashKey;
        // console.log(JSON.stringify(tmpJ)+" =? "+JSON.stringify(val)+" = "+(JSON.stringify(tmpJ)==JSON.stringify(val))); 
        // if(JSON.stringify(tmpJ)==JSON.stringify(val)){
        //     isDupilcate = true;
        //     return;
        // }
        if (tmp[i].tag instanceof Array) {
            if (tmp[i].tag.equals(val.tag)) {
                isDupilcate = true;
                return;
            }
        } else {
            if (tmp[i].tag == val.tag) {
                isDupilcate = true;
                return;
            }
        }

    }
    if (!isDupilcate)
        this.push(val);
}

// Warn if overriding existing method
if (Array.prototype.equals)
    console.warn("Overriding existing Array.prototype.equals. Possible causes: New API defines the method, there's a framework conflict or you've got double inclusions in your code.");
// attach the .equals method to Array's prototype to call it on any array
Array.prototype.equals = function (array) {
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time 
    if (this.length != array.length)
        return false;

    for (var i = 0, l = this.length; i < l; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].equals(array[i]))
                return false;
        }
        else if (this[i] != array[i]) {
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;
        }
    }
    return true;
}
// Hide method from for-in loops
Object.defineProperty(Array.prototype, "equals", { enumerable: false });

function lengthInUtf8Bytes(str) {
    // Matches only the 10.. bytes that are non-initial characters in a multi-byte sequence.
    var m = encodeURIComponent(str).match(/%[89ABab]/g);
    return str.length + (m ? m.length : 0);
  }
  