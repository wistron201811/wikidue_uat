/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function order_getOrderListDetail(po_number, type) {
    // console.log("po_number: " + po_number + " type: " + type);
    window.location.href = "order_list_detail.php?po_number=" + po_number + "&type=" + type;

}

$(document).ready(function(e) {
	var w_width = $(window).width();
	if(w_width < 768){
		$('.sub_menu').width(w_width).addClass('collapse').removeClass('expand');
		var _sub_width = w_width/4;
		$('.left-1').css('left', (0-_sub_width)+'px');
		$('.left-2').css('left', (0-_sub_width*2)+'px');
		$('.left-3').css('left', (0-_sub_width*3)+'px');
	}
	$(window).resize(function(){
		w_width = $(window).width();
		if(w_width < 768){
			$('.sub_menu').width(w_width).addClass('collapse').removeClass('expand');
		}

	});
	$('.helpcenter-toggle-menu').on('click',function(){
		var $helpcenterToggleBtn = $(this);
		if($helpcenterToggleBtn.next().hasClass('collapse')){
			// 展開
			$helpcenterToggleBtn.addClass('active');
			$helpcenterToggleBtn.children().next().children().removeClass('icon-plus').addClass('icon-minus');
			$helpcenterToggleBtn.children().next().removeClass('collapse').addClass('expand');
			$helpcenterToggleBtn.next().removeClass('collapse').addClass('expand');
		}else{
			// 關閉
			$helpcenterToggleBtn.removeClass('active');
			$helpcenterToggleBtn.children().next().children().removeClass('icon-minus').addClass('icon-plus');
			$helpcenterToggleBtn.children().next().removeClass('expand').addClass('collapse');
			$helpcenterToggleBtn.next().removeClass('expand').addClass('collapse');
		}
	});
});