

myApp.controller('ShareController', function ($scope, bsLoadingOverlayService, restClientService) {
    $scope.grades = [
        { name: "一年級", tag: "0001", level: "1" }, { name: "二年級", tag: "0002", level: "1" }, { name: "三年級", tag: "0003", level: "1" }, { name: "四年級", tag: "0004", level: "1" },
        { name: "五年級", tag: "0005", level: "1" }, { name: "六年級", tag: "0006", level: "1" }, { name: "七年級", tag: "0007", level: "1" }, { name: "八年級", tag: "0008", level: "1" },
        { name: "九年級", tag: "0009", level: "1" }, { name: "高一", tag: "0010", level: "1" }, { name: "高二", tag: "0011", level: "1" }, { name: "高三", tag: "0012", level: "1" }
    ]

    $scope.stage = 0;
    $scope.grade = null;
    $scope.setGrade = function (grade) {
        if ($scope.grade != null && $scope.grade.tag == grade.tag) {
            $scope.grade = null;
            $scope.stage = 0;
            return;
        }
        $scope.grade = grade;
        $scope.subject = null;
        $scope.chapter = null;
        restClientService.post(tagQueryApi, 'tag-block',
            {
                tag: [grade.tag],
                level: 1
            }, function (data) {
                if (data && data.length > 0) {
                    $scope.stage = 1;
                    $scope.subjects = data;
                    setTimeout(function () {
                        select_toggle();
                    }, 300);
                } else {
                    $scope.stage = 0;
                    $scope.subjects = [];
                }

            }
        )
    }

    $scope.subject = null;
    $scope.subjects = null;
    $scope.setSubject = function (subject) {
        if ($scope.subject != null && $scope.subject.tag == subject.tag) {
            $scope.subject = null;
            $scope.stage = 1;
            return;
        }
        $scope.subject = subject;
        restClientService.post(tagQueryApi, 'tag-block',
            {
                tag: [subject.tag],
                level: 3
            }, function (data) {
                if (data && data.length > 0) {
                    $scope.stage = 2;
                    $scope.chapters = data;
                    setTimeout(function () {
                        select_toggle();
                    }, 300)
                } else {
                    $scope.stage = 1;
                    $scope.chapters = [];
                }

            }
        )
    }

    $scope.chapter = null;
    $scope.chapters = null;
    $scope.setChapter = function (chapter) {
        if ($scope.chapter != null && $scope.chapter.tag == chapter.tag) {
            $scope.chapter = null;
            $scope.stage = 2;
            return;
        }
        $scope.chapter = chapter;
    }

    $scope.addSelTag = function () {
        var categoryName = $scope.category ? "/" + $scope.category.categoryName : "";
        var categoryTag = $scope.category ? "-" + $scope.category.categoryId : "";
        var o = $scope.selTagSet.length;// 原本大小
        switch ($scope.stage) {
            case 0://只有年級
                if ($scope.grade) {
                    $scope.selTagSet.pushUnique({
                        name: $scope.grade.name + categoryName,
                        tag: $scope.grade.tag + categoryTag,
                        level: "0"
                    });
                } else {
                    if (categoryName) {
                        $scope.selTagSet.pushUnique({
                            name: $scope.category.categoryName,
                            tag: $scope.category.categoryId,
                            level: ""
                        });
                    }
                }
                break;
            case 1://只有年級
                if (!$scope.subject) {
                    $scope.selTagSet.pushUnique({
                        name: $scope.grade.name + categoryName,
                        tag: $scope.grade.tag + categoryTag,
                        level: "0"
                    });
                }
                else
                    $scope.selTagSet.pushUnique({
                        name: $scope.grade.name + '/' + $scope.subject.name + categoryName,
                        tag: $scope.subject.tag + categoryTag,
                        level: categoryName ? "" : "1"
                    });
                break;
            case 2://只有年級科目
                if (!$scope.chapter)
                    $scope.selTagSet.pushUnique({
                        name: $scope.grade.name + '/' + $scope.subject.name + categoryName,
                        tag: $scope.subject.tag + categoryTag,
                        level: categoryName ? "" : "1"
                    });
                else
                    $scope.selTagSet.pushUnique({
                        name: $scope.grade.name + '/' + $scope.subject.name + '/' + $scope.chapter.name + categoryName,
                        tag: $scope.chapter.tag + categoryTag,
                        level: categoryName ? "" : "3"
                    });
                break;

            default:
                $scope.selTagSet.pushUnique({
                    name: categoryName,
                    tag: categoryTag,
                    level: ""
                });
                break;
        }
        if (o == $scope.selTagSet.length)
            alert('請至少選擇一項tag或不要選擇重複的tag');

        syncTagInput();

    }

    // 類別
    $scope.categorys = [];
    $scope.category = null;
    restClientService.post(categoryQueryApi, 'tag-block',
        {
            categoryType: 'SHARE'
        }, function (data) {
            $scope.categorys = data;
            // 將tag 塞回
            if (document.shareForm.tag.value) {
                $scope.selTagSet = JSON.parse(document.shareForm.tag.value);
            }
            if (document.shareForm.category.value) {
                var tmp = JSON.parse(document.shareForm.category.value);
                tmp.forEach(function (sel) {
                    $scope.categorys.forEach(function (c) {
                        if (sel.categoryId == c.categoryId) {
                            c['selected'] = true;
                        }
                    });
                });
            }

        }
    )
    $scope.setCategory = function (category) {
        if ($scope.category != null && $scope.category.categoryCode == category.categoryCode) {
            $scope.category = null;
            return;
        }
        $scope.category = category;
    }



    $scope.selTagSet = [];// 已選擇的設定

    $scope.removeTag = function (tag) {
        $scope.selTagSet = $scope.selTagSet.filter(function (value) {
            return value.tag != tag;
        });
        syncTagInput();
    }

    $scope.clearAllTag = function (tag) {
        $scope.selTagSet = [];
        syncTagInput();
    }

    /**
     * 把tag & category塞回表單
     */
    function syncTagInput() {
        var tag = [];
        if ($scope.selTagSet && $scope.selTagSet.length > 0) {
            document.shareForm.tag.value = JSON.stringify($scope.selTagSet);
        } else {
            document.shareForm.tag.value = "";
        }

    }
    $scope.selCategory = [];

    $scope.syncCategory = function (cg) {
        if (cg.selected == true)
            cg.selected = false;
        else
            cg.selected = true;

        $scope.selCategory = $scope.categorys.filter(function (value) {
            return value.selected == true;
        });
        if ($scope.selCategory.length > 0) {
            document.shareForm.category.value = JSON.stringify($scope.selCategory);
        } else {
            document.shareForm.category.value = "";
        }
    }

});