myApp
		.controller(
				'DetailController',
				function($scope, bsLoadingOverlayService, restClientService,
						cartService, favService) {

					$scope.checkAccusation = function(shareId, title_) {
						restClientService
								.post(
										checkAccusationApi,
										'check-block',
										{
											shareId : shareId
										},
										function(data) {

											BootstrapDialog
													.show({
														title : '檢舉[' + title_
																+ ']',
														message : function(
																dialogItself) {
															var $textarea = $('<textarea class="form-control" placeholder="請輸入檢舉原因"></textarea>');
															dialogItself
																	.setData(
																			'field-title-drop',
																			$textarea); // Put
															// it
															// in
															// dialog
															// data's
															// container
															// then
															// you
															// can
															// get
															// it
															// easier
															// by
															// using
															// dialog.getData()
															// later.
															return $textarea;
														},
														buttons : [ {
															label : '送出',
															cssClass : 'btn-primary',
															action : function(
																	dialogItself) {
																if (!dialogItself
																		.getData(
																				'field-title-drop')
																		.val()) {
																	BootstrapDialog
																			.show({
																				title : '提示訊息',
																				type : BootstrapDialog.TYPE_WARNING,
																				message : '請輸入檢舉原因'
																			})
																} else {
																	$scope
																			.submitAccusation(
																					shareId,
																					dialogItself
																							.getData(
																									'field-title-drop')
																							.val(),
																					dialogItself);
																}
															}
														} ]
													});

										},
										function(msg) {
											BootstrapDialog
													.show({
														title : '提示訊息',
														type : BootstrapDialog.TYPE_WARNING,
														message : msg
													})
										})
					}

					$scope.submitAccusation = function(shareId, reportReason,
							dialogItself) {
						restClientService.post(submitAccusationApi,
								'check-block', {
									shareId : shareId,
									reportReason : reportReason
								}, function(data) {
									BootstrapDialog.show({
										title : '提示訊息',
										message : '檢舉成功'
									})
									dialogItself.close();
								}, function(msg) {
									BootstrapDialog.show({
										title : '提示訊息',
										type : BootstrapDialog.TYPE_WARNING,
										message : msg
									})
								})
					}

					$scope.isFav = false;
					$scope.checkFav = function(shareId) {
						favService.checkFav({
							shareId : shareId
						}, function(data) {
							$scope.isFav = data.result;
							$scope.count = data.count;
						})
					}
					$scope.setFav = function(shareId) {
						favService.updateFav({
							shareId : shareId
						}, function(data) {
							$scope.isFav = data.result;
							$scope.count = data.count;
						})
					}

				});