
myApp.controller('AdController', function ($scope, bsLoadingOverlayService, restClientService, cartService) {

    $scope.adList = [];
    var queryAd = $scope.queryAd = function () {
        var param = {
        	role: "",
        	categoryCode: ""
        }
        if(typeof roleCode !== 'undefined' && roleCode){
        	param.role = roleCode;
        }
        if(typeof categoryCode !== 'undefined' && categoryCode){
            param.categoryCode = categoryCode;
        }
        restClientService.post(adPath, 'ad-block',
            param, function (data) {
                $scope.adList = data;
            }
        )
    }
    queryAd();
    

	    $scope.getRandomImageUrl = function(imageOne, imageTwo) {
		if (!imageTwo) {
			return imageOne;
		} else {
			return (Math.ceil(Math.random() * 100) % 2 == 1 )? imageOne
					: imageTwo;
		}
	};

});