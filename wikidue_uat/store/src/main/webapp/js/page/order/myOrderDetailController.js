
myApp.controller('MyOrderDetailController', function ($scope, bsLoadingOverlayService, restClientService, cartService) {

    $scope.order;
    $scope.queryOrderDetail = function (mainOrderNo) {
        restClientService.post(orderDetailApi, 'order-block',
            {mainOrderNo:mainOrderNo}, function (data) {
                $scope.order = data;
            }, function (msg) {
                BootstrapDialog.show({
                    title: '提示訊息',
                    type: BootstrapDialog.TYPE_WARNING,
                    message: msg
                })
            }
        )
    }

    $scope.invoice = {};
    $scope.queryInvoice = function(paymentId){
        restClientService.post(invoiceQueryApi, null,
            {paymentId:paymentId}, function (data) {
                $scope.invoice = data;
            }, function (msg) {
                BootstrapDialog.show({
                    title: '提示訊息',
                    type: BootstrapDialog.TYPE_WARNING,
                    message: '電子發票查詢失敗：'+msg
                })
            }
        )
    }

    $scope.goPage = function (url) {
        location.href = url;
    }
});