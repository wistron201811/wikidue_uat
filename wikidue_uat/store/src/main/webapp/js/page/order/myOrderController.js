
myApp.controller('MyOrderController', function ($scope, bsLoadingOverlayService, restClientService, cartService) {

    $scope.orderList = [];
    $scope.page;
    $scope.currentDate = new Date();
    var queryMyOrder = $scope.queryMyOrder = function (page) {
        restClientService.post(orderListApi, 'order-block',
            { pageNo: page }, function (data) {
                $scope.page = data;
                $scope.page.current = page;
                $scope.orderList = data.content;
            }, function (msg) {
                BootstrapDialog.show({
                    title: '提示訊息',
                    type: BootstrapDialog.TYPE_WARNING,
                    message: msg
                })
            }
        )
    }
    queryMyOrder(1);

    $scope.checkPay = function (payDueDate, currentDate) {
        return currentDate <= payDueDate;
    }

    $scope.goPage = function (url) {
        location.href = url;
    }
});