
myApp.controller('CartController', function ($scope, bsLoadingOverlayService, restClientService, cartService) {



    $scope.cartList = [];
    $scope.hasProps = false;
    var queryCartList = $scope.queryCartList = function () {
        bsLoadingOverlayService.start({
            referenceId: 'order-list-block'
        });
        cartService.queryCart(
            { queryStock: true },
            function (data) {
                bsLoadingOverlayService.stop({
                    referenceId: 'order-list-block'
                });
                $scope.cartList = data;
                (data.filter(
                    function(v){
                        return v.stock > -1;
                    }
                ).length>0)?$scope.hasProps = true: $scope.hasProps=false;
            }, function (error) {
                bsLoadingOverlayService.stop({
                    referenceId: 'order-list-block'
                });
                if (error && error.indexOf('token') == -1) {
                	BootstrapDialog.show({
                        title: '提示訊息',
                        type: BootstrapDialog.TYPE_WARNING,
                        message: error
                    });
                }
            }
        )
    }
    queryCartList();

    $scope.setHasProps = function(){
        $scope.hasProps = true;
    }

    $scope.getTotalAmount = function () {
        var total = 0;
        angular.forEach($scope.cartList, function (cart, key) {
            total = total + (cart.unitPrice * cart.quantity)
        });
        return total;
    }

    $scope.edit = function (cart) {
        cartService.editCart(cart, 'order-list-block', function (data) {
            queryCartList();
        }, function (error) {
            BootstrapDialog.show({
                title: '提示訊息',
                type: BootstrapDialog.TYPE_WARNING,
                message: error
            });
        });
    }

    $scope.remove = function (cart) {

        BootstrapDialog.confirm({
            title: '提示訊息',
            message: '確認要刪除[' + cart.productName + ']?',
            type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
            closable: true, // <-- Default value is false
            draggable: true, // <-- Default value is false
            btnCancelLabel: '取消', // <-- Default value is 'Cancel',
            btnOKLabel: '確定', // <-- Default value is 'OK',
            btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
            callback: function (result) {
                if (result) {
                    cartService.removeCart(
                        { cartId: cart.pkCart }, function () {
                            BootstrapDialog.show({
                                title: '提示訊息',
                                message: '購物車移除[' + cart.productName + ']成功'
                            });
                            queryCartList();
                        }, function (msg) {
                            BootstrapDialog.show({
                                title: '提示訊息',
                                type: BootstrapDialog.TYPE_WARNING,
                                message: msg
                            });
                            queryCartList();
                        }
                    );
                } else {

                }
            }
        });
    }


    $scope.goPage = function (url) {
        location.href = url;
    }

    $scope.$on("$destroy", function(){
        // $q.defer();
        console.err('destroy');
    });
});