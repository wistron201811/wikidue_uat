
myApp.controller('PaymentController', function ($scope, bsLoadingOverlayService, restClientService, $interval) {


    var stop;
    $scope.count = 60;
    $scope.startPaymentProcessing = function (merchantTradeNo) {
        bsLoadingOverlayService.setGlobalConfig({
            templateUrl: "/store/js/page/cart/loading-overlay-template.html"
        });
        bsLoadingOverlayService.start({
            referenceId: 'payment-block'
        });

        // Don't start a new fight if we are already fighting
        if (angular.isDefined(stop)) return;
        stop = $interval(function () {
            if ($scope.count > 0) {
                $scope.count = $scope.count - 1;
                restClientService.post(paymentResult, '_none',
                    {
                        merchantTradeNo: merchantTradeNo
                    }, function (data) {
                        if (angular.isDefined(data) && data != null) {
                            $scope.paymentResult = data.rtnMsg;
                            $scope.stopPaymentProcessing();
                        }
                    }
                )
            } else {
                $scope.paymentResult = "交易結果不明";
                $scope.stopPaymentProcessing();
            }
        }, 1000);
    };

    $scope.stopPaymentProcessing = function () {
        if (angular.isDefined(stop)) {
            $interval.cancel(stop);
            stop = undefined;
            bsLoadingOverlayService.stop({
                referenceId: 'payment-block'
            });
        }
    };
    $scope.goPage = function (url) {
        location.href = url;
    }

    $scope.$on("$destroy", function () {
        // $q.defer();
        $scope.stopPaymentProcessing();
        console.err('destroy');
    });
});