
myApp.controller('MsgController', function ($scope, bsLoadingOverlayService, restClientService, cartService) {

    $scope.messageList = [];
    $scope.page;
    $scope.queryMsgList = function (page) {
        restClientService.post(msgListApi, 'msg-block',
            { page: page }, function (data) {
                $scope.page = data;
                $scope.page.current = page;
                $scope.messageList = data.content;
            }, function (msg) {
                BootstrapDialog.show({
                    title: '提示訊息',
                    type: BootstrapDialog.TYPE_WARNING,
                    message: msg
                })
            }
        )
    }

    $scope.queryMsgDetail = function (messageId) {
        restClientService.post(msgDetailApi, 'msg-block',
            { messageId: messageId }, function (data) {
                $scope.message = data;
            }, function (msg) {
                BootstrapDialog.show({
                    title: '提示訊息',
                    type: BootstrapDialog.TYPE_WARNING,
                    message: msg
                })
            }
        )
    }

    $scope.goPage = function (url) {
        location.href = url;
    }
});