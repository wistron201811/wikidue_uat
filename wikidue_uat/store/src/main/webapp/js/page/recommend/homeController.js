
myApp.controller('HomeController', function ($scope, bsLoadingOverlayService, restClientService, cartService) {

    $scope.list = [];
    $scope.page;
    $scope.queryRecommend = function (page) {
        restClientService.post(recommendQueryApi, 'query-block',
            { page: page }, function (data) {
                $scope.page = data;
                $scope.page.current = page;
                $scope.list = data.content;
            }, function (msg) {
                BootstrapDialog.show({
                    title: '提示訊息',
                    type: BootstrapDialog.TYPE_WARNING,
                    message: msg
                })
            }
        )
    }

    $scope.goPage = function (url) {
        location.href = url;
    }
});