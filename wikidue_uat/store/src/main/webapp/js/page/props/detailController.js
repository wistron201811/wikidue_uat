
myApp.controller('DetailController', function ($scope, bsLoadingOverlayService, restClientService, cartService, favService) {

    $scope.selQty = 1;
    $scope.addCart = function (productId) {
        bsLoadingOverlayService.start({
            referenceId: 'add-cart-block'
        });
        cartService.addCart({ productId: productId, quantity: $scope.selQty }, function (data) {
            bsLoadingOverlayService.stop({
                referenceId: 'add-cart-block'
            });
            BootstrapDialog.show({
                title: '提示訊息',
                message: '購物車已成功加入[' + data.productName + ']'
            });
        }, function (msg) {
            bsLoadingOverlayService.stop({
                referenceId: 'add-cart-block'
            });
            BootstrapDialog.show({
                title: '提示訊息',
                type: BootstrapDialog.TYPE_WARNING,
                message: msg
            });
        });
    }

    $scope.goCheckout = function (productId) {
        cartService.addCartNoRefresh({ productId: productId, quantity: $scope.selQty }, function (data) {
            bsLoadingOverlayService.stop({
                referenceId: 'add-cart-block'
            });
            location.href = checkOutPath;
        }, function (msg) {
            bsLoadingOverlayService.stop({
                referenceId: 'add-cart-block'

            });
            BootstrapDialog.show({
                title: '提示訊息',
                type: BootstrapDialog.TYPE_WARNING,
                message: msg
            });
        });
    }

    $scope.isFav = false;
    $scope.checkFav = function (productId) {
        favService.checkFav(
            { productId: productId }, function (data) {
                $scope.isFav = data.result;
            }
        )
    }
    $scope.setFav = function (productId) {
        favService.updateFav(
            { productId: productId }, function (data) {
                $scope.isFav = data.result;
            }
        )
    }

    $scope.goPage = function (url) {
        location.href = url;
    }
});