
myApp.controller('CourseInController', function ($scope, bsLoadingOverlayService, restClientService, cartService, favService) {
	
	$scope.buyItem = function (productId) {
		bsLoadingOverlayService.start({
			referenceId: 'add-cart-block'
		});
		cartService.addCartNoRefresh({ productId: productId }, function (data) {
			bsLoadingOverlayService.stop({
				referenceId: 'add-cart-block'
			});
			location.href = checkOutPath;
		}, function (msg) {
			bsLoadingOverlayService.stop({
				referenceId: 'add-cart-block'
					
			});
			BootstrapDialog.show({
				title: '提示訊息',
				type: BootstrapDialog.TYPE_WARNING,
				message: msg
			});
		});
	}

    $scope.buyFreeCourse = function (productId) {
        bsLoadingOverlayService.start({
            referenceId: 'add-cart-block'
        });
        restClientService.post(buyFreeCourseApi, '',
        		{ productId: productId }, function (data) {
            bsLoadingOverlayService.stop({
                referenceId: 'add-cart-block'
            });
            location.href = coursePageUrl+productId;
        }, function (msg) {
            bsLoadingOverlayService.stop({
                referenceId: 'add-cart-block'

            });
            BootstrapDialog.show({
                title: '提示訊息',
                type: BootstrapDialog.TYPE_WARNING,
                message: msg
            });
        });
    }

    $scope.isFav = false;
    $scope.checkFav = function (productId) {
        favService.checkFav(
            { productId: productId }, function (data) {
                $scope.isFav = data.result;
            }
        )
    }
    $scope.setFav = function (productId) {
        favService.updateFav(
            { productId: productId }, function (data) {
                $scope.isFav = data.result;
            }
        )
    }

    $scope.searchKey;
    $scope.init = false;
    $scope.queryQuestion = function (courseId, init) {
        var params = { courseId: courseId };
        if($scope.searchKey) params.searchKey = $scope.searchKey;
        $scope.init = init||false;
        init||$("#tab-05").LoadingOverlay("show");
        restClientService.post(discussionQueryApi, '',
            params, function (data) {
                init||$("#tab-05").LoadingOverlay("hide");
                $scope.questionList = data;
            }, function (msg) {
                init||$("#tab-05").LoadingOverlay("hide");
                BootstrapDialog.show({
                    title: '提示訊息',
                    type: BootstrapDialog.TYPE_WARNING,
                    message: msg
                });
            }
        )
    }

    $scope.question = {
        topic: "",
        content: ""
    };

    $scope.askQuestion = function (courseId) {
        var err, check = true;
        if (!$scope.question.topic) {
            check = false;
            err = "請輸入問題";
        }
        if (!$scope.question.content) {
            err += check ? "請輸入問題內容" : "、請輸入問題內容";
            check = false;
        }
        if (!check) {
            BootstrapDialog.show({
                title: '提示訊息',
                type: BootstrapDialog.TYPE_WARNING,
                message: err
            });
            return;
        }

        var param = {
            courseId: courseId,
            topic: $scope.question.topic,
            content: $scope.question.content
        }
        restClientService.post(discussionAskApi, 'discussion-block',
            param, function () {
                $scope.question = {
                    topic: "",
                    content: ""
                }
                BootstrapDialog.show({
                    title: '提示訊息',
                    message: '發問成功'
                });
                $scope.queryQuestion(courseId);
            }, function (msg) {
                BootstrapDialog.show({
                    title: '提示訊息',
                    type: BootstrapDialog.TYPE_WARNING,
                    message: msg
                });
            }
        )
    }

    $scope.replyQuestion = function (courseId, questionId, content) {
        restClientService.post(discussionReplyApi, 'discussion-block',
            {
                questionId: questionId,
                content: content
            }, function () {
                BootstrapDialog.show({
                    title: '提示訊息',
                    message: '回覆成功'
                });
                $scope.selectQuestion = questionId;
                $scope.queryQuestion(courseId);
            }, function (msg) {
                BootstrapDialog.show({
                    title: '提示訊息',
                    type: BootstrapDialog.TYPE_WARNING,
                    message: msg
                });
            }
        )
    }

    $scope.selectQuestion;
    $scope.checkActive = function (id) {
        return '' + id + (id == $scope.selectQuestion ? ' in' : '');
    }
});