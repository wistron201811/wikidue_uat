myApp.controller('VideoController', function ($scope, bsLoadingOverlayService, restClientService, cartService) {


    $scope.videoImgShow = true;
    $scope.iframePlayerShow = false;
    $scope.playIcon = true;
    $scope.videoStreamUrl = "#";
    $scope.playCount = 0;
    $scope.vodeoId, $scope.videoStreamUrl, $scope.videoImg
    var getToken = $scope.getToken = function (videoId, videoStreamUrl) {
        $scope.playIcon = false;
        restClientService.post(videoTokenApi, 'player-block',
            {
                videoId: videoId
            }, function (data) {
                $scope.videoStreamUrl = videoStreamUrl.replace(/http:\/\//g, 'https://') + encodeURIComponent("&token=" + data.token + "&uid=" + data.uid + "&hbID=" + videoId);
                $scope.videoImgShow = false;
                $scope.iframePlayerShow = true;
                set_iframe();
                // console.log($scope.videoStreamUrl);
            }
        )
    }
    var countVideoWatched;
    $scope.countVideoWatched = countVideoWatched = function (videoId) {
    	$scope.playCount = '-';
        restClientService.post(videoWatchedApi, null,
            {
                videoId: videoId
            }, function (data) {
                $scope.playCount = data;
            }
        )

    }
    var playVideo = $scope.playVideo = function () {
        getToken($scope.vodeoId, $scope.videoStreamUrl);
        countVideoWatched($scope.vodeoId);
    }

    var playFreeVideo = $scope.playFreeVideo = function () {
        //console.log($scope.videoStreamUrl);
        $scope.videoStreamUrl = $scope.videoStreamUrl.replace(/http:\/\//g, 'https://');
        $scope.videoImgShow = false;
        $scope.playIcon = false;
        $scope.iframePlayerShow = true;
        set_iframe();
    }

    $scope.setPlayVideo = function (vodeoId, videoStreamUrl, videoImg) {
        $scope.vodeoId = vodeoId;
        $scope.videoStreamUrl = videoStreamUrl.replace(/http:\/\//g, 'https://');
        $scope.videoImg = videoImg;
        $scope.videoImgShow = true;
        $scope.playIcon = true;
        $scope.iframePlayerShow = false;
        countVideoWatched($scope.vodeoId);
    }

});