
myApp.controller('CourseController', function ($scope, bsLoadingOverlayService, restClientService, cartService) {


    $scope.grades = [{
        name: "1~2",
        tag: ["0001", "0002"],
        active: true
    }, {
        name: "3~4",
        tag: ["0003", "0004"],
        active: false
    }, {
        name: "5~6",
        tag: ["0005", "0006"],
        active: false
    }, {
        name: "7~9",
        tag: ["0007", "0008", "0009"],
        active: false
    }, {
        name: "10~12",
        tag: ["0010", "0011", "0012"],
        active: false
    }]

    $scope.stage = 0;
    $scope.grade = null;
    $scope.setGrade = function (grade) {
        if ($scope.grade != null && $scope.grade.tag == grade.tag) {
            $scope.grade = null;
            $scope.stage = 0;
            return;
        }
        $scope.grade = grade;
        $scope.subject = null;
        $scope.chapter = null;
        restClientService.post(productTagQueryApi, 'tag-block',
            {
                type: "COURSE",
                tag: grade.tag,
                level: 1
            }, function (data) {
                if (data && data.length > 0) {
                    $scope.stage = 1;
                    $scope.subjects = data;
                    setTimeout(function () {
                        select_toggle();
                    }, 300);
                } else {
                    $scope.stage = 0;
                    $scope.subjects = [];
                }

            }
        )
    }

    $scope.subject = null;
    $scope.subjects = null;
    $scope.setSubject = function (subject) {
        if ($scope.subject != null && $scope.subject.tag == subject.tag) {
            $scope.subject = null;
            $scope.stage = 1;
            return;
        }
        $scope.subject = subject;
        restClientService.post(productTagQueryApi, 'tag-block',
            {
                type: "COURSE",
                tag: [subject.tag],
                level: 3
            }, function (data) {
                if (data && data.length > 0) {
                    $scope.stage = 2;
                    $scope.chapters = data;
                    setTimeout(function () {
                        select_toggle();
                    }, 300)
                } else {
                    $scope.stage = 1;
                    $scope.chapters = [];
                }

            }
        )
    }

    $scope.chapter = null;
    $scope.chapters = null;
    $scope.setChapter = function (chapter) {
        if ($scope.chapter != null && $scope.chapter.tag == chapter.tag) {
            $scope.chapter = null;
            $scope.stage = 2;
            return;
        }
        $scope.chapter = chapter;
    }

    $scope.addSelTag = function () {
        var orilength = $scope.selTagSet.length;
        var categoryName = $scope.category ? "/" + $scope.category.categoryName : "";
        var categoryTag = $scope.category ? "-" + $scope.category.categoryId : "";
        var tmpTag = [];
        switch ($scope.stage) {
            case 0://只有年級
                if ($scope.grade) {
                    angular.forEach($scope.grade.tag, function (value, key) {
                        // $scope.grade.tag[key] = value + categoryTag;
                        tmpTag[key] = value + categoryTag;
                    });
                    $scope.selTagSet.pushUnique({
                        name: $scope.grade.name + categoryName,
                        tag: tmpTag
                    });
                } else {
                    if (categoryName) {
                        $scope.selTagSet.pushUnique({
                            name: $scope.category.categoryName,
                            tag: [$scope.category.categoryId]
                        });
                    }

                }
                break;
            case 1://只有年級
                if (!$scope.subject) {
                    tmpTag = [];
                    angular.forEach($scope.grade.tag, function (value, key) {
                        // $scope.grade.tag[key] = value + categoryTag;
                        tmpTag[key] = value + categoryTag;
                    });
                    $scope.selTagSet.pushUnique({
                        name: $scope.grade.name + categoryName,
                        tag: $scope.grade.tag
                    });
                }
                else
                    $scope.selTagSet.pushUnique({
                        name: $scope.grade.name + '/' + $scope.subject.name + categoryName,
                        tag: [$scope.subject.tag + categoryTag]
                    });
                break;
            case 2://只有年級科目
                if (!$scope.chapter)
                    $scope.selTagSet.pushUnique({
                        name: $scope.grade.name + '/' + $scope.subject.name + categoryName,
                        tag: [$scope.subject.tag + categoryTag]
                    });
                else
                    $scope.selTagSet.pushUnique({
                        name: $scope.grade.name + '/' + $scope.subject.name + '/' + $scope.chapter.name + categoryName,
                        tag: [$scope.chapter.tag + categoryTag]
                    });
                break;

            default:
                $scope.selTagSet.pushUnique({
                    name: categoryName,
                    tag: [categoryTag]
                });
                break;
        }
        orilength == $scope.selTagSet.length || queryProduct(1);
    }

    // 類別
    $scope.categorys = [];
    $scope.category = null;
    restClientService.post(categoryQueryApi, 'tag-block',
        {
            categoryType: 'COURSE'
        }, function (data) {
            $scope.categorys = data;
        }
    )
    $scope.setCategory = function (category) {
        if ($scope.category != null && $scope.category.categoryCode == category.categoryCode) {
            $scope.category = null;
            return;
        }
        $scope.category = category;
    }



    $scope.selTagSet = [];// 已選擇的設定

    $scope.removeTag = function (tag) {
        $scope.selTagSet = $scope.selTagSet.filter(function (value) {
            return value.tag != tag;
        });
        queryProduct(1);
    }

    $scope.clearAllTag = function (tag) {
        $scope.selTagSet = [];
        queryProduct(1);
    }


    $scope.courseList = [];
    $scope.page = null;
    var queryProduct = $scope.queryProduct = function (page, tag) {
        var param = {
            type: "COURSE",
            page: page,
            order1: document.getElementById("order1").value || null,
            order2: document.getElementById("order2").value || null,
            tags: []
        }
        if ($scope.keyword) {
            param.keyword = $scope.keyword;
        }
        if (tag) {
            $scope.selTagSet.push(tag);
        }
        if ($scope.selTagSet && $scope.selTagSet.length > 0) {
            angular.forEach($scope.selTagSet, function (value, key) {
                param.tags = param.tags.concat(value.tag);
            });
        }
        restClientService.post(productQueryApi, 'course-block',
            param, function (data) {
                $scope.page = data;
                $scope.page.current = page;
                $scope.courseList = data.content;
            }
        )
    }
    //queryProduct(1);

    $scope.addCart = function (productId) {
        bsLoadingOverlayService.start({
            referenceId: 'add-cart-block'
        });
        cartService.addCart({ productId: productId }, function (data) {
            bsLoadingOverlayService.stop({
                referenceId: 'add-cart-block'
            });
            BootstrapDialog.show({
                title: '提示訊息',
                message: '購物車已成功加入[' + data.productName + ']'
            });
        }, function (msg) {
            bsLoadingOverlayService.stop({
                referenceId: 'add-cart-block'
            });
            BootstrapDialog.show({
                title: '提示訊息',
                type: BootstrapDialog.TYPE_WARNING,
                message: msg
            });
        });
    }

    $scope.goPage = function (url) {
        location.href = url;
    }
});