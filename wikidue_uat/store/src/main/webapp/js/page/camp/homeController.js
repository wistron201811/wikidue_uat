
myApp.controller('HomeController', function ($scope, bsLoadingOverlayService, restClientService, cartService, favService) {


    $scope.campAll = {};
    $scope.campQueryList = function () {
        restClientService.post(campListApi, 'list-block',
            {}, function (data) {
                $scope.campAll = data;
            }
        )
    }

    $scope.campList = [];
    $scope.page = null;
    $scope.queryCamp = function (page) {
        var param = {
            page: page,
            order1: document.getElementById("order1").value || null,
            order2: document.getElementById("order2").value || null,
            city: document.getElementById("city").value || null
        }
        if ($scope.keyword) {
            param.keyword = $scope.keyword;
        }
        restClientService.post(campQueryApi, 'camp-block',
            param, function (data) {
                $scope.page = data;
                $scope.page.current = page;
                $scope.campList = data.content;
            }
        )
    }

    $scope.isFav = true;
    $scope.checkFav = function (camp) {
        favService.checkFav(
            { campId: camp.pkCamp }, function (data) {
                camp.isFav = data.result;
            }
        )
    }
    $scope.setFav = function (camp) {
        favService.updateFav(
            { campId: camp.pkCamp }, function (data) {
                camp.isFav = data.result;
            }
        )
    }

    $scope.sendGaEvent = function (pkCamp, campName) {
        ga('send', {
            hitType: 'event',
            eventCategory: 'camp',
            eventAction: 'open',
            eventLabel: pkCamp+'-'+campName
        });
    }

    $scope.goPage = function (url) {
        location.href = url;
    }
});