myApp.controller('HeaderController', function ($scope, bsLoadingOverlayService, restClientService, cartService, $rootScope) {

    $scope.cartList = [];
    $scope.quantity = 0;
    $scope.getTotalAmount = function () {
        var total = 0;
        angular.forEach($scope.cartList, function (cart, key) {
            total = total + (cart.unitPrice * cart.quantity)
        });
        return total;
    }


    $scope.removeCart = function (cart) {

        BootstrapDialog.confirm({
            title: '提示訊息',
            message: '確認要刪除[' + cart.productName + ']?',
            type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
            closable: true, // <-- Default value is false
            draggable: true, // <-- Default value is false
            btnCancelLabel: '取消', // <-- Default value is 'Cancel',
            btnOKLabel: '確定', // <-- Default value is 'OK',
            btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
            callback: function (result) {
                if (result) {
                    cartService.removeCart(
                        { cartId: cart.pkCart }, function () {
                            BootstrapDialog.show({
                                title: '提示訊息',
                                message: '購物車移除[' + cart.productName + ']成功'
                            });
                        }, function (msg) {
                            BootstrapDialog.show({
                                title: '提示訊息',
                                type: BootstrapDialog.TYPE_WARNING,
                                message: msg
                            });
                        }
                    );
                } else {

                }
            }
        });
    }


    $rootScope.$on('refreshCart', function () {
        // console.log('refreshCart');
        cartService.queryCart({},
            function (data) {
                $scope.cartList = data;
                setTimeout(function(){  $('[data-toggle="tooltip"]').tooltip(); }, 500);
            }, function (error) {
            	if (error && error.indexOf('token') == -1) {
	                BootstrapDialog.show({
	                    title: '提示訊息',
	                    type: BootstrapDialog.TYPE_WARNING,
	                    message: error
	                });
            	}
            }
        )
    })



    // 訊息
    $scope.badge = 0;
    $scope.messageList = 0;


    /** 查購物車 */
    $scope.queryCart = function () {
        cartService.queryCart({},
            function (data) {
                $scope.cartList = data;
                setTimeout(function(){  $('[data-toggle="tooltip"]').tooltip(); }, 500);
            }, function (error) {
            	if (error && error.indexOf('token') == -1) {
	                BootstrapDialog.show({
	                    title: '提示訊息',
	                    type: BootstrapDialog.TYPE_WARNING,
	                    message: error
	                });
            	}
            }
        )
    }

    $scope.queryMessage = function () {
        /** 查個人訊息 */
        restClientService.post(msgCountApi, 'msg-block',
            {}, function (data) {
                $scope.badge = data.unreadCount;
            }, function (msg) {
                if (msg && msg.indexOf('token') == -1) {
                    BootstrapDialog.show({
                        title: '提示訊息',
                        type: BootstrapDialog.TYPE_WARNING,
                        message: msg
                    });
                }
            }
        )
        restClientService.post(msgQueryApi, 'msg-block',
            {}, function (data) {
                $scope.messageList = data;
            }, function (msg) {
                if (msg && msg.indexOf('token') == -1) {
                    BootstrapDialog.show({
                        title: '提示訊息',
                        type: BootstrapDialog.TYPE_WARNING,
                        message: msg
                    });
                }
            }
        )
    }

$scope.goPage = function (url) {
    location.href = url;
}


});