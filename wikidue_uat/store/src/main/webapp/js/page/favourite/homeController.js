
myApp.controller('FavouriteController', function ($scope, bsLoadingOverlayService, restClientService, cartService, favService) {

    $scope.queryAll = function () {
        $scope.queryFavByCourse();
        $scope.queryFavByProps();
        $scope.queryFavByCamp();
        $scope.queryFavByShare();
    }

    $scope.removeFav = function (type, id) {

        var param = {};
        switch (type) {
            case "COURSE":
            case "PROPS":
                param['productId'] = id;
                break;
            case "CAMP":
                param['campId'] = id;
                break;
            case "SHARE":
                param['shareId'] = id;
                break;
        }
        favService.updateFav(
            param, function (data) {
                switch (type) {
                    case "COURSE":
                        $scope.queryFavByCourse();
                        break;
                    case "PROPS":
                        $scope.queryFavByProps();
                        break;
                    case "CAMP":
                        $scope.queryFavByCamp();
                        break;
                    case "SHARE":
                        $scope.queryFavByShare();
                        break;
                }
            }
        )
    }

    $scope.queryFavByCourse = function () {
        var param = {
            type: "COURSE",
            order: document.getElementById("order").value || null
        }
        if ($scope.keyword) {
            param.keyword = $scope.keyword;
        }
        restClientService.post(favQueryApi, 'course-block',
            param, function (data) {
                $scope.courseList = data;
            }
        )
    }

    $scope.queryFavByProps = function () {
        var param = {
            type: "PROPS",
            order: document.getElementById("order").value || null
        }
        if ($scope.keyword) {
            param.keyword = $scope.keyword;
        }
        restClientService.post(favQueryApi, 'props-block',
            param, function (data) {
                $scope.propsList = data;
            }
        )
    }

    $scope.queryFavByCamp = function () {
        var param = {
            type: "CAMP",
            order: document.getElementById("order").value || null
        }
        if ($scope.keyword) {
            param.keyword = $scope.keyword;
        }
        restClientService.post(favQueryApi, 'camp-block',
            param, function (data) {
                $scope.campList = data;
            }
        )
    }

    $scope.queryFavByShare = function () {
        var param = {
            type: "SHARE",
            order: document.getElementById("order").value || null
        }
        if ($scope.keyword) {
            param.keyword = $scope.keyword;
        }
        restClientService.post(favQueryApi, 'share-block',
            param, function (data) {
                $scope.shareList = data;
            }
        )
    }

    $scope.goPage = function (url) {
        location.href = url;
    }

    $scope.openNewWindow = function (url) {
        window.open(url, "_blank");
    }
});