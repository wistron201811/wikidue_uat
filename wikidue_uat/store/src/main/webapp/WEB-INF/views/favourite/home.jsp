<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
	<!-- angularjs -->
	<script src="${pageContext.request.contextPath}/js/page/favourite/homeController.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
	<div ng-controller="FavouriteController">
		<div class="index-main-list for-camp">
			<div class="container">
				<!-- ///////////////////////////////////////////// -->
				<div class="row">
					<!-- /////////////////////LEFT//////////////////// -->
					<div class="col-md-4 left-side">
						<div class="left-avatar">
							<h2 class="dark-title ct mt0">學習中心</h2>
							<img src="${pageContext.request.contextPath}/img/icon/avatar-man.png" alt="" class="img-circle img-thumbnail">
							<p>${user_auth.name}</p>
						</div>
						<div class="left-side-in">
							<!-- MENU -->
							<div class="panel-group sort-panel" id="AA">
								<!-- QA-LIST -->
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="accordion-toggle one-level" href="${pageContext.request.contextPath}/learningCenter">學習中心</a>
										</h4>
									</div>
								</div>
								<!--/  QA-LIST -->
								<!-- QA-LIST -->
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="accordion-toggle active one-level" href="javascript:;">我的收藏</a>
										</h4>
									</div>
								</div>
								<!--/  QA-LIST -->
							</div>
							<!-- panel-group -->
							<!-- END MENU -->

							<div class="mb15 visible-xs visible-sm"></div>
						</div>
					</div>
					<!-- /////////////////////END LEFT//////////////////// -->
					<!-- /////////////////////RIGHT//////////////////// -->
					<div class="col-md-8 right-content line-left">
						<div class="clearfix mb20"></div>

						<div class="row gap20">
							<div class="col-sm-3 col-xs-6">
								<select class="selectpicker" id="order" data-width="100%" title="排序">
									<option data-hidden="true"></option>
									<option value="PUBLISH_START_DATE_DESC">最新在前</option>
									<option value="PUBLISH_START_DATE_ASC">最舊在前</option>
								</select>
							</div>
							<div class="clearfix visible-xs mb10"></div>
							<div class="col-sm-5">
								<div class="input-group search">
									<input type="text" class="form-control" ng-model="keyword" placeholder="輸入您要查詢的關鍵字">
									<span class="input-group-btn">
										<a href="javascript:;" ng-click="queryAll()" class="btn btn-warning">
											<i class="icon-main_search"></i>
										</a>
									</span>
								</div>
							</div>
						</div>
						<div class="clearfix mb20"></div>
						<h2 class="main-title mt10 mb20">線上課程</h2>
						<div class="row" data-ng-init="queryFavByCourse()" bs-loading-overlay="course-block">
							<!-- list -->
							<div class="col-sm-4" ng-repeat="course in courseList">
								<div class="learning-list">
									<span class="l-close" ng-click="removeFav('COURSE',course.pkProduct)"></span>
									<div class="img-hold" ng-click="goPage('${pageContext.request.contextPath}/course/detail/'+course.pkProduct)">
										<img ng-src="{{course.image}}" ng-click="goPage('${pageContext.request.contextPath}/course/detail/'+course.pkProduct)">
									</div>
									<h4>{{course.title}}</h4>
									<p class="l-price">
										<s>{{course.discountPrice | currency : '$' : 0}}</s> {{course.price | currency : '$' : 0}}</p>
								</div>
							</div>
							<div ng-if="!courseList || courseList.length<=0" class="alert alert-warning">
								搜尋結果：沒有資料
							</div>
							<!-- /list -->
						</div>
						<h2 class="main-title mt10 mb20">創客教具</h2>
						<div class="row" data-ng-init="queryFavByProps()" bs-loading-overlay="props-block">
							<!-- list -->
							<div class="col-sm-4" ng-repeat="props in propsList">
								<div class="learning-list">
									<span class="l-close" ng-click="removeFav('PROPS',props.pkProduct)"></span>
									<div class="img-hold" ng-click="goPage('${pageContext.request.contextPath}/props/detail/'+props.pkProduct)">
										<img ng-src="{{props.image}}" ng-click="goPage('${pageContext.request.contextPath}/props/detail/'+props.pkProduct)">
									</div>
									<h4>{{props.title}}</h4>
									<p class="l-price">
										<s>{{props.discountPrice | currency : '$' : 0}}</s> {{props.price | currency : '$' : 0}}</p>
								</div>
							</div>
							<div ng-if="!propsList || propsList.length<=0" class="alert alert-warning">
								搜尋結果：沒有資料
							</div>
							<!-- /list -->
						</div>
						<h2 class="main-title mt10 mb20">營隊活動</h2>
						<div class="row" data-ng-init="queryFavByCamp()" bs-loading-overlay="camp-block">
							<!-- list -->
							<div class="col-sm-4" ng-repeat="camp in campList">
								<div class="learning-list">
									<span class="l-close" ng-click="removeFav('CAMP',camp.pkCamp)"></span>
									<div class="img-hold" ng-click="openNewWindow(camp.externalLink)">
										<img ng-src="{{camp.image}}" ng-click="openNewWindow(camp.externalLink)">
									</div>
									<h4>{{camp.title}}</h4>
								</div>
							</div>
							<div ng-if="!campList || campList.length<=0" class="alert alert-warning">
								搜尋結果：沒有資料
							</div>
							<!-- /list -->
						</div>
						<h2 class="main-title mt10 mb20">分享園地</h2>
						<div class="row" data-ng-init="queryFavByShare()" bs-loading-overlay="share-block">
							<!-- list -->
							<div class="col-sm-4" ng-repeat="share in shareList">
								<div class="learning-list">
									<span class="l-close" ng-click="removeFav('SHARE',share.pkShare)"></span>
									<div class="img-hold" ng-click="goPage('${pageContext.request.contextPath}/share/detail/'+share.pkShare)">
										<img src="{{share.image}}" ng-click="goPage('${pageContext.request.contextPath}/share/detail/'+share.pkShare)">
									</div>
									<h4>{{share.title}}</h4>
								</div>
							</div>
							<div ng-if="!shareList || shareList.length<=0" class="alert alert-warning">
								搜尋結果：沒有資料
							</div>
							<!-- /list -->
						</div>
					</div>
					<!-- /////////////////////END RIGHT//////////////////// -->
				</div>
				<!-- row -->
			</div>
			<!-- container -->
		</div>
	</div>