<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/plugin/jquery-cookie/jquery.cookie.js"></script>
    <!-- <script src="js/jquery-migrate-1.2.1.min.js"></script>   -->
    <script src="${pageContext.request.contextPath}/plugin/bootstrap/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/base.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
    <script src="${pageContext.request.contextPath}/plugin/bootstrap-select/js/bootstrap-select.min.js"></script>

    <script>
	 	// 線上課程廣告群組
		categoryCode = "ADIN002";
    
        // very simple to use!
        $(document).ready(function () {
            select_toggle();
            tab_mobile();
            setTimeout(function () {
                $("ul.menu li").eq("1").addClass("active");
            }, 300);
        });

        $(".video-holder").hide();
        $('.play-video').on('click', function () {
            $(this).hide();
            $(".video-holder").show();
            $(".video")[0].src += "1";
        });

        // function setAutoresize(){
        //     $.each($('textarea[data-autoresize]'), function () {
        //         var offset = this.offsetHeight - this.clientHeight;

        //         var resizeTextarea = function (el) {
        //             $(el).css('height', 'auto').css('height', el.scrollHeight + offset);
        //         };
        //         $(this).on('keyup input', function () { resizeTextarea(this); }).removeAttr('data-autoresize');
        //     });
        // }


        function resizeTextarea(el){
            var offset = $(el)[0].offsetHeight - $(el)[0].clientHeight;
            $(el).css('height', 'auto').css('height', el.scrollHeight + offset);
        }

        
    </script>