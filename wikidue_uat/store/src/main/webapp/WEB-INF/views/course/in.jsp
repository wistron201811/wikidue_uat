<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!doctype html>
<!-- angularjs -->
<script
	src="${pageContext.request.contextPath}/js/page/course/inController.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
<div id="fb-root"></div>
<script>
(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.7";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>
<div ng-controller="CourseInController">
	<div>
		<div id="load-header">
			<!-- load-header -->
		</div>
		<div class="clearfix"></div>
		<!-- 更改將head mark 190522 by kuopu start
		<div class="container ">
			<div class="avatar-section">
				<div class="row">
					<div class="col-md-6">
						<div class="div-tb">
							<div class="td vmid img-avatar">
								<img
									src="${pageContext.request.contextPath}/api/query/image/${course.teacher.photoId}"
									alt="">
							</div>
							<div class="td vmid title" style="">
								<h3>${course.teacher.teacherName}</h3>
							</div>
							<div class="td vmid">
								<table class="table table-no-line avatar-form">
									<tr>
										<td class="td-nowarp">分類：</td>
										<td class="pl10"><c:forEach
												items="${course.categoryNameList}" var="categoryName"
												varStatus="loop">
												<c:if test="${loop.index >0}">、</c:if>${categoryName}
                                </c:forEach></td>
									</tr>
									<%-- <tr>
										<td class="td-nowarp">學生數：</td>
										<td class="pl10">${course.studentAmount}</td>
									</tr> --%>
								</table>
							</div>
						</div>
					</div>

					<div class="col-md-6">
						<ul class="tag-list">
							<c:forEach items="${course.tags}" var="tag">
								<c:if test="${not empty tag.tagName}">
									<li><a class="btn btn-select-g active" href="${pageContext.request.contextPath}/course/home?tagId=${tag.id}">${tag.tagName}</a></li>
								</c:if>
							</c:forEach>
						</ul>
					</div>
				</div>
				<!-- / row -->
			<!-- /div>    更改將head mark 190522 by kuopu end-->
			<!-- avatar-section -->
		<!-- /div-->
		<!-- /container -->
		<!-- video-block -->
		<div class="video-section reduse">
			<div class="container">
				<div class="video-block">
					<div class="clearfix"></div>
					<div class="row gap0 m0 plr0">
						<div class="col-md-12"><!-- 原col-md-8 -->
							<div class="video-img">
								<div class="video-holder ">
									<div class="embed-responsive embed-responsive-16by9">
										<iframe class="video"
											src="https://www.youtube.com/embed/${course.videoUrl}?autoplay="
											frameborder="0" allow="autoplay; encrypted-media"
											allowfullscreen></iframe>
									</div>
								</div>
								<span class="play play-video"> <i class="icon-play"></i>
								</span> <img
									src="${pageContext.request.contextPath}/api/query/image/${course.imageOneId}"
									alt="">
							</div>
						</div>
						<!-- <div class="col-md-4">
							<div class="video-right-content">
								<a href="javascript:;" ng-click="setFav('${course.productId}')"
									data-ng-init="checkFav('${course.productId}')" class="love"
									ng-class="{active:isFav}" bs-loading-overlay="fav-block"> <i></i>
								</a>
								<div class="like">
									<i></i>
								</div>
								<h3>${course.courseName}</h3>
								<small class="v-info"> PS://        <span class="v-badge">基礎</span> 
									PS://         <span class="time"><i class=" icon-top_clock"></i> 2.7小時</span>
									PS://        <span class="time"><i class=" icon-people"></i> 50人以下</span> 
								</small>
								<div class="v-des">${course.courseSummary}</div>
								<!-- <div class="price-info">
									<p class="price-w">
										<c:if test="${course.discountPrice > 0}">
											<fmt:formatNumber value="${course.discountPrice}"
												type="currency" maxFractionDigits="0" />
										</c:if>
										<c:if test="${course.discountPrice <= 0}">
											免費
										</c:if>
									</p>
									<s class="price-b"> <fmt:formatNumber
											value="${course.price}" type="currency" maxFractionDigits="0" />
									</s>
									PS://                  <s class="price-b">優惠截止日期：2018-01-31</s>
								</div>-->
								<!-- <div class="mb30 visible-lg visible-xs"></div>
								<div class="row gap20">
									<c:if test="${not course.ownCourse}">
										<c:if test="${not existCart}">
											<c:if test="${course.discountPrice > 0}">
												<div class="col-xs-12">
													<a href="javascript:;" class="btn btn-success btn-block"
														bs-loading-overlay="add-cart-block"
														ng-click="buyItem('${course.productId}')">直接購買</a>
												</div>
											</c:if>
											<c:if test="${course.discountPrice <= 0}">
												<div class="col-xs-12">
													<a href="javascript:;" class="btn btn-success btn-block"
														bs-loading-overlay="add-cart-block"
														ng-click="buyFreeCourse('${course.productId}')">立即上課</a>
												</div>
											</c:if>
										</c:if>
										<c:if test="${existCart}">
											<div class="col-xs-12">
												<a href="${pageContext.request.contextPath}/cart/checkout"
													class="btn btn-success btn-block">已放入購物車，前往結帳</a>
											</div>
										</c:if>
									</c:if>
									<c:if test="${course.ownCourse}">
										PS://				<div class="col-xs-6"><button class="btn btn-success btn-block disabled" >已購買課程</button></div>
										<div class="col-xs-12">
											<a
												href="${pageContext.request.contextPath}/course/play?id=${course.courseId}"
												class="btn btn-success btn-block">開始上課</a>
										</div>
									</c:if>
									PS:// <div class="col-xs-6"><a href="javascript:;" class="btn btn-success btn-block">加入購物車</a></div>
								</div>
							</div>
						</div>-->

					</div>
					<!-- row -->
				</div>
			</div>
			<!-- container -->
			<!-- END video-block -->
		</div>
		<!-- video-section -->
<!-- 更改 tail  mark 190522 by kuopu start
		<div class="tabs-v2">
			<ul class="nav nav-tabs hidden-xs" role="tablist">
				<li class="active"><a href="#tab-01" data-toggle="tab">課程簡介</a>
				</li>
				<li><a href="#tab-02" data-toggle="tab">課程內容</a></li>
				<li><a href="#tab-03" data-toggle="tab">講師資訊</a></li>
				<li><a href="#tab-04" data-toggle="tab">課程提問</a></li>
				<li><a href="#tab-05" data-toggle="tab">課程討論區</a></li>
			</ul>
			<div class="visible-xs dropdown tab-mobile">
				<a href=""
					class="btn btn-success btn-lg btn-block tab-mobile-select"
					data-toggle="dropdown"> <span>課程簡介</span> <i
					class="icon-expand pull-right"></i>
				</a>
				<div class="dropdown-menu ">
					<ul class="nav nav-tabs">
						<li><a href="#tab-01" data-toggle="tab">課程簡介</a></li>
						<li><a href="#tab-02" data-toggle="tab">課程內容</a></li>
						<li><a href="#tab-03" data-toggle="tab">講師資訊</a></li>
						<li><a href="#tab-04" data-toggle="tab">課程提問</a></li>
						<li><a href="#tab-05" data-toggle="tab">課程討論區</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="container">
		
			<div class="tab-content">
				PS:// tab-01
				
				<div class="tab-pane fade in  active" id="tab-01">
					<div class="row">

						<div class="col-md-7">
							<h4 class="special-title mb30">課程目標</h4>
							${e7:previewHtmlImg(pageContext.request.contextPath,course.courseExpectation)}
						</div>
						<div class="col-md-5">
							<h4 class="special-title mb30">適合對象</h4>
							${e7:previewHtmlImg(pageContext.request.contextPath,course.courseTarget)}
						</div>
						<div class="col-md-7">
							<h4 class="special-title mb30">課程介紹</h4>
							${e7:previewHtmlImg(pageContext.request.contextPath,course.courseIntroduction)}
						</div>
						<div class="col-md-5">
							<h4 class="special-title mb30">課前準備</h4>
							${e7:previewHtmlImg(pageContext.request.contextPath,course.coursePreparation)}
						</div>

					</div>
					PS: //row 
				</div> 
				PS:// END tab-01 
				PS:// END tab-02 
				<div class="tab-pane fade" id="tab-02">
					<div class="row">
						<div class="col-md-8">
							<h4 class="special-title mb30">課程單元</h4>

							PS:// MENU 
							<div class="panel-group sort-panel" id="AA">
								PS:// QA-LIST 
								<c:forEach items="${course.courseChapterList}"
									var="courseChapter">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a class="accordion-toggle collapsed" data-toggle="collapse"
													data-parent="#AA"
													href="#collapse${courseChapter.courseChapterId}"
													aria-expanded="false"> <span class="num">${courseChapter.courseChapterNo}</span>${courseChapter.courseChapterName}
												</a>
											</h4>
										</div>
										<div class="panel-collapse collapse"
											id="collapse${courseChapter.courseChapterId}"
											aria-expanded="false">
											<div class="panel-body">
												<ul class="video-sub-menu">
													<c:forEach items="${courseChapter.courseVideoList}"
														var="courseVideo">
														<li><c:if test="${not course.ownCourse}">
																<a href="javascript:;">
																	${courseVideo.courseVideoName}
																	<span class="time">
																		<c:if test="${courseVideo.free}">
																			<button onclick="location.href='${pageContext.request.contextPath}/course/play?id=${course.courseId}&productId=${course.productId}&videoId=${courseVideo.video.videoId}'" class="btn btn-success btn-xs">試閱</button>
																		</c:if>
																		<e7:secFmt sec="${courseVideo.video.duration}"></e7:secFmt>
																	</span>
																</a>
															</c:if> <c:if test="${course.ownCourse}">
																<a href="javascript:;">${courseVideo.courseVideoName}
																	<span class="time">
																		<button onclick="location.href='${pageContext.request.contextPath}/course/play?id=${course.courseId}&videoId=${courseVideo.video.videoId}'" class="btn btn-success btn-xs">播放</button>
																		<e7:secFmt sec="${courseVideo.video.duration}"></e7:secFmt>
																	</span>
																</a>
															</c:if></li>
													</c:forEach>
												</ul>
											</div>
										</div>
									</div>
								</c:forEach>
								PS:// QA-LIST
							</div>
							PS:// panel-group
							PS:// END MENU 

							<div class="mb15 visible-xs visible-sm"></div>

						</div>
						<div class="col-md-4">
							<h4 class="special-title mb30">檔案下載</h4>
							<ul class="list-group">
								<c:forEach items="${course.courseFileList}" var="courseFile">
									<li class="list-group-item">${courseFile.fileName}<a
										href="${pageContext.request.contextPath}/api/file/download/${courseFile.fileId}"
										class="download pull-right mt5"> <i class="icon-download"></i>
									</a>
									</li>
								</c:forEach>
							</ul>
						</div>
					</div>
					PS:// row 
				</div>
				PS:// END tab-02 
				PS:// END tab-03 
				<div class="tab-pane fade " id="tab-03">
					<h4 class="special-title mb30">教師簡介</h4>
					<div class="row">
						<div class="col-sm-3 avatar-section-v">
							<div class="img-avatar inline-block">
								<img
									src="${pageContext.request.contextPath}/api/query/image/${course.teacher.photoId}"
									alt="">
							</div>
							<h3>${course.teacher.teacherName}</h3>
						</div>
						<div class="col-sm-4">
							<h3 class="dark-title">【現職】</h3>
							${course.teacher.teacherIntroduction}
						</div>
						<div class="col-sm-5">
							<h3 class="dark-title">【經歷】</h3>
							<table class="table">
								<tbody>
									<tr>
										<td>${course.teacher.experience}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				PS: //END tab-03 
				<div class="tab-pane fade " id="tab-04">
					<div class="fb-comments" data-href="<spring:eval expression="@configService.host" />/course/${course.courseId}" data-width="100%" data-numposts="5" data-order-by="reverse_time"></div>
				</div>
				<sec:authorize access="authenticated" var="authenticated" />
				<c:if test="${!authenticated}">
					<div class="tab-pane fade" id="tab-05">請先登入</div>
				</c:if>
				<c:if test="${authenticated}">
					<c:if test="${course.ownCourse}">
						<div class="tab-pane fade" id="tab-05">
							<div class="row mb20">
								<div class="col-sm-6">
									<div class="input-group input-group-lg">
										<input type="text" class="form-control" ng-model="searchKey"
											placeholder="輸入您要查詢的關鍵字"> <span
											class="input-group-btn"> <a href="javascript:;" ng-click="queryQuestion('${course.courseId}')"
											class="btn btn-default"> <i class="icon-main_search"></i>
										</a>
										</span>
									</div>
								</div>
								<div class="col-sm-2 ct">
									<p class="mb10 mt10">或</p>
								</div>
								<div class="col-sm-4">
									<a href="#modal-ask" class="btn btn-lg btn-success btn-block"
										data-toggle="modal">我要提問</a>
								</div>
							</div>
							<ul class="list-group"
								data-ng-init="queryQuestion('${course.courseId}', true)">
								PS:// list 
								<li class="list-group-item talk-list"
									ng-repeat="q in questionList"><a
									ng-href="{{'.'+q.questionId}}" data-toggle="collapse">
										<div class="div-tb">
											<div class="td avatar-area">
												<img
													src="${pageContext.request.contextPath}/img/icon/avatar-man.png"
													alt="" class="img-thumbnail img-circle">
											</div>
											<div class="td">
												<h5 class="mb5">
													<strong>{{q.topic}}</strong>
												</h5>
												<small class="t-date">{{q.userName}}.{{q.createDt}}
													<span>發問</span>
												</small>
											</div>
											<div class="td respon">
												<span class="active">{{q.questionReplies.length}}</span>
												<p>回覆</p>
											</div>
										</div>
										<div class="collapse talk-contant"
											ng-class="checkActive(q.questionId)">
											<p>{{q.content}}</p>
										</div>
								</a>
									<div class="collapse talk-contant"
										ng-class="checkActive(q.questionId)">
										<div class="div-tb" ng-repeat="reply in q.questionReplies">
											<div class="td avatar-area">
												<img
													src="${pageContext.request.contextPath}/img/icon/avatar-man.png"
													alt="" class="img-thumbnail img-circle">
											</div>
											<div class="td">
												<h6 ng-if="reply.courseTeacher" class="badge">授課老師</h6>
												<small class="t-date">{{reply.userName}}
													.{{reply.createDt}} <span>回覆</span>
												</small>
												<p>{{reply.content}}</p>
											</div>
										</div>
										<div class="div-tb">
											<div class="td avatar-area small">
												<img
													src="${pageContext.request.contextPath}/img/icon/avatar-man.png"
													alt="" class="img-circle">
											</div>
											<div class="td">
												<textarea class="form-control" rows="1"
													ng-model="reply.replyContent"
													onkeyup="resizeTextarea(this)"></textarea>
												<p class="ct mt10">
													<a href="javascript:;"
														ng-click="replyQuestion('${course.courseId}',q.questionId, reply.replyContent)"
														class="btn btn-default">送出</a>
												</p>
											</div>
										</div>
									</div></li>
								<li ng-if="!questionList || questionList.length==0"
									class="list-group-item">{{!init?'查無資料':'暫無人發問'}}</li>
								PS: /// list 
							</ul>
						</div>
					</c:if>
					<c:if test="${!course.ownCourse}">
						<div class="tab-pane fade" id="tab-05">請先購買課程</div>
					</c:if>
				</c:if>
				<hr>
				<a href="${pageContext.request.contextPath}/course/home" class="btn btn-default pull-right">回上一頁 <i class="icon-back"></i></a>
			</div>
			PS:// containter  
		</div>
	</div>-->


	<div class="modal fade modal-ask" id="modal-ask">
		<div class="modal-vertical-middle">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-body">
						<input type="text" ng-model="question.topic"
							class="form-control input-lg mb10" placeholder="輸入您的問題">
						<textarea class="form-control  input-lg"
							ng-model="question.content" placeholder="請填寫要發送的內容" rows="5"></textarea>
					</div>
					<div class="modal-footer ct">
						<div class="btn-group">
							<a href="javascript:;" class="btn btn-default wid_100px"
								data-dismiss="modal">取消</a> <a href="javascript:;"
								class="btn btn-success no-round wid_100px"
								ng-click="askQuestion('${course.courseId}')"
								data-dismiss="modal">發佈問題</a>
						</div>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
	</div>

</div>