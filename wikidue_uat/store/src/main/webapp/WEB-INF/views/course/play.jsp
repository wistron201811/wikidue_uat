<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
  <%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
    <%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
      <%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
        <%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
        <%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
          <!doctype html>
          <html>
          <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
          <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

          <head>
            <meta charset="utf-8">
            <title>Wikidue Store 教育商城 -
              <tiles:getAsString name="pageTitle" />
            </title>
            <!--SEO Meta Tags-->
			<!--<meta name="description" content="" />
            <meta name="keywords" content="S-moda" />-->
            <meta name="description" content="全台首創最大STEAMxMaker教育商城。整合科普教育與程式設計理念，提供創客教具、線上課程、教育檢測、營隊活動等服務，提供親、師、生最專業、優質可信賴的一站到位服務。" />
            <meta name="keywords" content="程式, 程式教育, Maker, Coder, STEN, STEAM, 創客, 創客教育, 手作, 自走車, 無人機, 空拍機, 飛行器, 機器人, Arduino, APCS, Wikidue, 科普, K12, 科學, 科學教育, 教育" />
            <meta property="og:title" content="Wikidue Store 教育商城" />
            <meta property="og:description" content="全台首創最大STEAMxMaker教育商城。整合科普教育與程式設計理念，提供創客教具、線上課程、教育檢測、營隊活動等服務，提供親、師、生最專業、優質可信賴的一站到位服務。" />
            <meta property="og:image" content="https://store.wikidue.com/store/img/wikidue-circle_02.png" />
            <meta name="author" content="8Guild" />
            <!--Mobile Specific Meta Tag-->
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
            <!--Favicon-->
            <link rel="shortcut icon" href="${pageContext.request.contextPath}/favicon.ico" type="image/x-icon">
            <link rel="icon" href="${pageContext.request.contextPath}/S-moda.ico">
            <!--Master Slider Styles-->
            <!-- menu -->
            <link href="${pageContext.request.contextPath}/plugin/bootstrap/bootstrap.css" rel="stylesheet">
            <link href="${pageContext.request.contextPath}/css/icomoon/style.css" rel="stylesheet">
            <link href="${pageContext.request.contextPath}/plugin/owl-carousel/owl.carousel.css" rel="stylesheet">
            <link href="${pageContext.request.contextPath}/css/header.css" rel="stylesheet">
            <link href="${pageContext.request.contextPath}/css/owl-style.css" rel="stylesheet">
            <!-- <link href="plugin/owl-carousel/owl.theme.css" rel="stylesheet"> -->
            <link href="${pageContext.request.contextPath}/css/layout.css" rel="stylesheet">
            <link href="${pageContext.request.contextPath}/css/responsive.css" rel="stylesheet">
            <link href="${pageContext.request.contextPath}/css/e7learning.css" rel="stylesheet">

            <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
            <script src="${pageContext.request.contextPath}/plugin/jquery-cookie/jquery.cookie.js"></script>



            <!-- angularjs -->
            <script src="${pageContext.request.contextPath}/js/angular.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/app.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
            <script src="${pageContext.request.contextPath}/js/lib/angular-loading-overlay/angular-loading-overlay.js"></script>
            <script src="${pageContext.request.contextPath}/js/page/course/videoController.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
          </head>

          <body ng-app="myApp" class="no-header">
            <!-- 載入api config -->
            <jsp:include page="/WEB-INF/views/common/apiConfig.jsp" />
            <div ng-controller="VideoController">
              <div class="video-play">
                <div class="left-menu">
                  <div class="top-bar">
                    <a href="javascript:;" class="toggle-left-menu">
                      <i class="icon-list"></i>
                    </a>
                    <%-- <c:if test="${not empty productId}">
                    <a href="${pageContext.request.contextPath}/course/detail/${productId}" class="back">
                      <i class="icon-expand_left"></i> 返回</a>
                    </c:if>
                    <c:if test="${empty productId}">
                    <a href="${pageContext.request.contextPath}/course/${course.courseId}" class="back">
                      <i class="icon-expand_left"></i> 返回</a>
                    </c:if> --%>
                    <a href="javascript:;" onclick="window.history.back()" class="back">
                      <i class="icon-expand_left"></i> 返回</a>
                  </div>
                  <div class="list-hold" id="myGroup">
                    <div class="course-info">
                      <div class="div-tb">
                        <div class="td">${course.courseName}</div>
                        <div class="td td-nowarp"><i class="icon-progress-${e7:progress(course.progress)}-per"></i> ${course.progress}%</div>
                      </div>
                    </div>
                    <!-- ////////////////////////// -->
                    <c:forEach items="${course.courseChapterList}" var="chapter" varStatus="loop">
                      <a href="#${chapter.courseChapterId}" class="list-toggle  collapsed" data-toggle="collapse">
                        <div class="div-tb">
                          <div class="td">
                            <span class="num">${loop.index+1}</span>
                          </div>
                          <div class="td">
                            <p class="mb5">${chapter.courseChapterName}</p>
                            <div>
                              <div class="progress progress-sm">
                                <div class="progress-bar progress-bar-success "  style="width: ${chapter.progress}%;"> </div>
                              </div>
                              <div class="percent rt"> ${chapter.progress}%</div>
                            </div>
                          </div>
                          <div class="td"></div>
                        </div>
                      </a>
                      <c:set var="collapse"></c:set>
                      <c:forEach items="${chapter.courseVideoList}" var="video">
                        <c:if test="${video.video != null && video.video.videoId == videoId}">
                          <c:set var="collapse">in</c:set>
                        </c:if>
                      </c:forEach>
                      <div id="${chapter.courseChapterId}" class="collapse ${collapse}">
                        <ul class="teacher-list">
                          <c:forEach items="${chapter.courseVideoList}" var="video">
                            <li>
                              <c:if test="${video.video == null}">
                                <a href="javascript:;">
                                  ${video.courseVideoName}
                                  <span>轉黨中</span>
                                </a>
                              </c:if>
                              <c:if test="${video.video != null && course.ownCourse}">
                                <%-- 有購買 --%>
                                <c:if test="${video.video.videoId == videoId}">
                                  <a href="javascript:;" <c:if test="${video.complete}">class="active"</c:if> ng-class="{'play-active':vodeoId=='${video.video.hbID}'}" ng-init="vodeoId='${video.video.hbID}';videoStreamUrl='${video.video.videoStreamUrl}';videoImg='${video.video.img}';countVideoWatched('${video.video.hbID}')"
                                    ng-click="setPlayVideo('${video.video.hbID}','${video.video.videoStreamUrl}','${video.video.img}')">
                                </c:if>
                                <c:if test="${video.video.videoId != videoId}">
                                  <a href="javascript:;" <c:if test="${video.complete}">class="active"</c:if> ng-class="{'play-active':vodeoId=='${video.video.hbID}'}" ng-click="setPlayVideo('${video.video.hbID}','${video.video.videoStreamUrl}','${video.video.img}')">
                                </c:if>
                                ${video.courseVideoName}
                                <c:if test="${video.videoDuration!=null && video.videoDuration>0}">
                                <span class="complete"><i class="icon-progress-${e7:progress2(video.learningDuration,video.videoDuration)}-per"></i> 
                                  <c:if test="${video.learningDuration >= video.videoDuration}">
                                    100%
                                  </c:if>
                                  <c:if test="${video.learningDuration < video.videoDuration}">
                                    <fmt:formatNumber value="${video.learningDuration/video.videoDuration}" type="percent" />
                                  </c:if>
                                </span>
                                </c:if>
                                <span>
                                  <e7:secFmt sec="${video.video.duration}"></e7:secFmt>
                                </span>
                                <c:if test="${video.videoDuration!=null && video.videoDuration>0}">
                                  <div>
                                    <%-- <div class="progress progress-sm">
                                      <div class="progress-bar progress-bar-success " style="width: <fmt:formatNumber value="${video.learningDuration/video.videoDuration}" type="percent"/>;"> </div>
                                    </div>
                                    <div class="percent rt">
                                      <fmt:formatNumber value="${video.learningDuration/video.videoDuration}" type="percent" />
                                    </div> --%>
                                  </div>
                                </c:if>
                                </a>
                              </c:if>
                              <c:if test="${video.video != null && !course.ownCourse}">
                                <%-- 無購買 --%>
                                <c:if test="${video.free}">
                                    <c:if test="${video.video.videoId == videoId}">
                                      <a href="javascript:;" ng-class="{'play-active':vodeoId=='${video.video.hbID}'}" ng-init="vodeoId='${video.video.hbID}';videoStreamUrl='${video.video.videoStreamUrl}';videoImg='${video.video.img}';countVideoWatched('${video.video.hbID}')"
                                        ng-click="setPlayVideo('${video.video.hbID}','${video.video.videoStreamUrl}','${video.video.img}')">
                                    </c:if>
                                    <c:if test="${video.video.videoId != videoId}">
                                      <a href="javascript:;" ng-class="{'play-active':vodeoId=='${video.video.hbID}'}" ng-click="setPlayVideo('${video.video.hbID}','${video.video.videoStreamUrl}','${video.video.img}')">
                                    </c:if>
                                    ${video.courseVideoName}
                                    <span>
                                      <e7:secFmt sec="${video.video.duration}"></e7:secFmt>
                                    </span>
                                  </a>
                                </c:if>
                                <c:if test="${!video.free}">
                                  <a href="javascript:;" style="cursor: not-allowed!important">
                                    ${video.courseVideoName}
                                    <span>
                                      <e7:secFmt sec="${video.video.duration}"></e7:secFmt>
                                    </span>
                                  </a>
                                </c:if>
                              </c:if>
                            </li>
                          </c:forEach>
                        </ul>
                      </div>
                    </c:forEach>
                  </div>
                </div>
                <!-- END left-menu -->
                <div class="right-content">
                  <div id="right-top" class="top-bar">
                    <a href="javascript:;" class="toggle-left-menu">
                      <i class="icon-list"></i>
                    </a>
                    <sec:authorize access="authenticated" var="authenticated" />
                    <h3 class="title">講師：${course.teacher.teacherName}</h3>
                    <c:if test="${authenticated}">
                    <p class="info">
                      瀏覽次數：
                      <span>{{playCount}}</span>
                    </p>
                  </c:if>
                  </div>
                  <div class="content-hold ct" id="player_container" style="margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;">
                    <script>
                      function set_iframe() {
                        setTimeout(function () {
                          if ($("#iframe_player").is(':visible')) {
                            var w = $("#player_container").width();
                            var h = $("#myGroup").height() - 10;
                            // console.log("loaded", w, h);
                            $("#iframe_player").css({ "height": h + "px", "width": w + "px" });
                          }
                        }, 200);
                      }
                    </script>
                    <iframe id="iframe_player" ng-if="iframePlayerShow" onload="set_iframe()" style="margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;"
                      width="100%" height="100%" frameborder="0" ng-src="{{videoStreamUrl|trustAsResourceUrl}}" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    <div class="video-img" bs-loading-overlay="player-block" ng-if="videoImgShow">
                      <c:if test="${course.ownCourse}">
                          <span ng-if="playIcon" class="play" ng-click="playVideo()">
                      </c:if>
                      <c:if test="${!course.ownCourse}">
                          <span ng-if="playIcon" class="play" ng-click="playFreeVideo()">
                      </c:if>
                        <i class="icon-play"></i>
                      </span>
                      <img ng-src="{{videoImg|trustAsResourceUrl}}" alt="">
                    </div>
                  </div>
                </div>
                <!-- right-content -->
              </div>
              <script src="${pageContext.request.contextPath}/plugin/bootstrap/bootstrap.min.js"></script>
              <script src="${pageContext.request.contextPath}/plugin/bootstrap3-dialog/js/bootstrap-dialog.min.js"></script>
			  <link href="${pageContext.request.contextPath}/plugin/bootstrap3-dialog/css/bootstrap-dialog.min.css" rel="stylesheet">
              <script src="${pageContext.request.contextPath}/js/base.js"></script>

              <script>
                $(document).ready(function () {
                  $(".toggle-left-menu").click(function () {
                    $(".video-play").toggleClass("menu-close");
                  });
                  $('.toggle-left-menu').on("click", function () {
                    set_iframe();
                  });

                });

                $(window).resize(function () {
                  set_iframe();
                });

                select_toggle_video_play();
              </script>
            </div>
          </body>

          </html>