<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <div class="container">
        <div class="row">
            <div class="mb30 hidden-xs"></div>
            <div class="col-lg-3 col-sm-2 hidden-xs"></div>
            <div class="col-lg-3 col-sm-4">
                <div class="mb30 hidden-xs"></div>
                <h1>錯誤訊息</h1>
                <p class="mb20">系統發生錯誤，請聯絡網站管理員。</p>
                <a href="/" class="btn btn-success btn-block mb30">回到首頁</a>
            </div>
            <div class="col-lg-3 col-sm-4">
                <img src="${pageContext.request.contextPath}/img/inpage/Error.png" alt="" class="img-responsive">
            </div>
            <div class="col-lg-3 col-sm-2 hidden-xs"></div>
        </div>
    </div>