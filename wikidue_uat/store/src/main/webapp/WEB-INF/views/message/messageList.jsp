<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!doctype html>
<!-- angularjs -->
<script src="${pageContext.request.contextPath}/js/page/message/msgController.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>

<div ng-controller="MsgController" class="index-main-list">
	<div class="container" data-ng-init="queryMsgList(1)">
		<!-- ///////////////////////////////////////////// -->
		<div class="row">
			<!-- /////////////////////LEFT//////////////////// -->
			<div class="col-md-4 left-side">
				<div class="left-avatar">
					<h2 class="dark-title ct mt0">訊息中心</h2>
					<img src="${pageContext.request.contextPath}/img/icon/avatar-man.png" alt="" class="img-circle img-thumbnail">
					<p>${user_auth.name}</p>
				</div>
				<div class="left-side-in">
					<!-- MENU -->
					<div class="panel-group sort-panel" id="AA">
						<!-- QA-LIST -->
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a class="accordion-toggle active one-level" href="#">訊息中心</a>
								</h4>
							</div>
						</div>
						<!--/  QA-LIST -->
					</div>
					<!-- panel-group -->
					<!-- END MENU -->
					<div class="mb15 visible-xs visible-sm"></div>
				</div>
			</div>
			<!-- /////////////////////END LEFT//////////////////// -->
			<!-- /////////////////////RIGHT//////////////////// -->
			<div class="col-md-8 right-content line-left">
				<h3 class="main-title mt0 mb30">訊息中心</h3>
				<ul class="news-list" bs-loading-overlay="msg-block">
					<li ng-class="{active:msg.read}" ng-repeat="msg in messageList">
						<a ng-href="${pageContext.request.contextPath}/message/detail/{{msg.messageId}}">{{msg.title|cut:false:50:' ...'}}</a>
						<!-- <span class="pull-right">{{msg.createDt}}</span> -->
					</li>
					<li ng-if="!messageList || messageList.length==0">
						<a href="#">您目前沒有訊息</a>
					</li>
				</ul>
				<e7-pagination page="page" fun="queryMsgList" pageclass="'ct'"/>
			</div>
			<!-- /////////////////////END RIGHT//////////////////// -->
		</div>
		<!-- row -->
	</div>

</div>