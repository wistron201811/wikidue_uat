<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!doctype html>
<!-- angularjs -->
<script src="${pageContext.request.contextPath}/js/page/message/msgController.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>

<div ng-controller="MsgController" class="index-main-list for-camp" data-ng-init="queryMsgDetail('${messageId}')">
	<div class="container" bs-loading-overlay="msg-block">
		<h2>{{message.title}}</h2>
		<div class="rt">
			<a href="${pageContext.request.contextPath}/message/list" class="btn btn-default"><i class="icon-back"></i>  回到列表 </a>
		</div>
		<p ng-bind-html="message.content|trustAsHtml"></p>
		
		<!-- row -->
	</div>
	<!-- container -->
</div>