<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
		<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
			<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
			<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
				<!doctype html>
				<!-- angularjs -->
				<script src="${pageContext.request.contextPath}/js/page/order/myOrderDetailController.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
				<div ng-controller="MyOrderDetailController" class="index-main-list for-camp">
					<div class="container" bs-loading-overlay="order-block">
						<!-- ///////////////////////////////////////////// -->
						<div class="row">
							<!-- /////////////////////LEFT//////////////////// -->
							<div class="col-md-4 left-side">
								<div class="left-avatar">
									<h2 class="dark-title ct mt0">我的訂單</h2>
									<img src="${pageContext.request.contextPath}/img/icon/avatar-man.png" alt="" class="img-circle img-thumbnail">
									<p>${user_auth.name}</p>
								</div>
								<div class="left-side-in">
									<!-- MENU -->
									<div class="panel-group sort-panel" id="AA">
										<!-- QA-LIST -->
										<div class="panel panel-default">
											<div class="panel-heading">
												<h4 class="panel-title">
													<a class="accordion-toggle active one-level" href="${pageContext.request.contextPath}/order/myOrder">我的訂單</a>
												</h4>
											</div>
										</div>
										<!--/  QA-LIST -->

									</div>
									<!-- panel-group -->
									<!-- END MENU -->

									<div class="mb15 visible-xs visible-sm"></div>
								</div>
							</div>
							<!-- /////////////////////END LEFT//////////////////// -->
							<!-- /////////////////////RIGHT//////////////////// -->
							<div class="col-md-8 right-content line-left" data-ng-init="queryOrderDetail('${mainOrderNo}')">
								<h2 class="main-title mt0 mb20">訂單資訊</h2>
								<div class="row gap10 list-info">
									<div class="col-sm-6 col">
										<div class="div-tb">
											<div class="td hd">訂單編號：</div>
											<div class="td">{{order.mainOrderNo}}</div>
										</div>
									</div>
									<div class="col-sm-6 col">
										<div class="div-tb">
											<div class="td hd">購買日期：</div>
											<div class="td">{{order.createDt}}</div>
										</div>
									</div>
									<div class="col-sm-6 col">
										<div class="div-tb">
											<div class="td hd">金額：</div>
											<div class="td">NT{{order.totalAmount|currency : '$' : 0}}</div>
										</div>
									</div>
									<div class="col-sm-6 col">
										<div class="div-tb" ng-switch="order.status">
											<div class="td hd">訂單狀態：</div>
											<div class="td" ng-switch-when="E">訂單成立</div>
											<div class="td color-red" ng-switch-when="S">訂單取消</div>
											<div class="td color-red" ng-switch-when="C">訂單取消</div>
											<div class="td" ng-switch-default>{{order.status}}</div>
										</div>
									</div>
									<div class="col-sm-6 col">
										<div class="div-tb" ng-switch="order.paymentStatus">
											<div class="td hd">付款狀態：</div>
											<div class="td" ng-switch-when="P">已付款</div>
											<div class="td" ng-switch-when="W">付款處理中</div>
											<div class="td color-red" ng-switch-when="N">未付款</div>
											<div class="td" ng-switch-default>{{order.totalAmount==0?'免費':order.paymentStatus}}</div>
										</div>
									</div>
									<div class="col-sm-6 col" ng-if="order.paymentStatus=='P'">
										<div class="div-tb" data-ng-init="queryInvoice(order.paymentId)">
											<div class="td hd">發票資訊：</div>
											<div class="td">
												<a href="#modal-detail" data-toggle="modal">發票明細</a>
											</div>
										</div>
									</div>
									<div class="col-sm-6 col">
										<div class="div-tb">
											<div class="td hd">備註：</div>
											<div class="td">{{order.remark||'無'}}</div>
										</div>
									</div>
								</div>
								<hr>
								<!-- row -->
								<h2 class="main-title mt0 mb20" ng-if="order.address">收件資訊</h2>
								<table class="table table-bordered  table-striped text-center" ng-if="order.address">
									<thead>
										<tr>
											<td>收件人</td>
											<td>聯絡電話</td>
											<td>送貨地址</td>
										</tr>
									</thead>
									<tbody>
										<td>{{order.recipient}}</td>
										<td>{{order.telphone}}</td>
										<td>{{order.address}}</td>
									</tbody>
								</table>
								<h2 class="main-title mt0 mb20">付款資訊</h2>
								<table class="table table-bordered  table-striped text-center">
									<thead>
										<tr>
											<td>No.</td>
											<td>付款方式</td>
											<td>繳款金額</td>
											<td>付款狀態</td>
										</tr>
									</thead>
									<tbody>
										<tr ng-switch="order.paymentStatus" ng-if="order.totalAmount > 0">
											<td>1</td>
											<td>{{order.paymentTypeName}}</td>
											<td>NT{{order.tradeAmt|currency : '$' : 0}}</td>
											<td ng-switch-when="N" class="color-red">未付款</td>
											<td ng-switch-when="W" class="color-red">付款處理中</td>
											<td ng-switch-when="P" class="color-green-dark">已付款</td>
											<td ng-switch-default>{{order.paymentStatus}}</td>
										</tr>
										<tr ng-if="order.totalAmount == 0">
											<td>1</td>
											<td>免費</td>
											<td>NT$0</td>
											<td>免費</td>
										</tr>
									</tbody>
								</table>
								<h2 class="main-title mt0 mb20">訂單明細</h2>
								<ul class="order-list">
									<!-- list -->
									<li ng-repeat="poDetail in order.poDetails">
										<div class="div-tb" style="table-layout:fixed" ng-switch="poDetail.productType">
											<div class="td wid_30per">
												<a target="_blank" href="{{'${pageContext.request.contextPath}/'+(poDetail.productType=='PROPS'?'props':'course')+'/detail/'+poDetail.productId}}"><img ng-src="{{poDetail.image}}" class="img-responsive"></a>
											</div>
											<div class="td" ng-switch-when="PROPS">
												<span ng-switch="poDetail.status">
												<h3 class="title st mb10" ng-switch-when="" ><i class="icon-check-circle-o"></i>已付款<span >( {{poDetail.updateDt}} )</span></h3>
												<h3 class="title st mb10" ng-switch-when="N" ><i class="icon-error_outline"></i>未付款<span >( {{poDetail.updateDt}} )</span></h3>
												<h3 class="title st mb10" ng-switch-when="P" ><i class="icon-check-circle-o"></i>已付款<span >( {{poDetail.updateDt}} )</span></h3>
												<h3 class="title st mb10" ng-switch-when="D" ><i class="icon-check-circle-o"></i>已出貨<span >( {{poDetail.updateDt}} )</span></h3>
												<h3 class="title st mb10" ng-switch-when="R" ><i class="icon-error_outline"></i>已退款<span >( {{poDetail.updateDt}} )</span></h3>
												<h3 class="title st mb10" ng-switch-when="C" ><i class="icon-error_outline"></i>已取消<span >( {{poDetail.updateDt}} )</span></h3>
												<h3 class="title st mb10" ng-switch-when="S" ><i class="icon-error_outline"></i>已逾期<span >( {{poDetail.updateDt}} )</span></h3>
												<h3 class="title st mb10" ng-switch-when="H" ><i class="icon-error_outline"></i>退貨處理中<span >( {{poDetail.updateDt}} )</span></h3>
												<h3 class="title st mb10" ng-switch-when="G" ><i class="icon-error_outline"></i>退款處理中<span >( {{poDetail.updateDt}} )</span></h3>
												<h3 class="title st mb10" ng-switch-when="J" ><i class="icon-error_outline"></i>拒絕退件<span >( {{poDetail.updateDt}} )</span></h3>
												<h3 class="title st mb10" ng-switch-default ><i class="icon-check-circle-o"></i>已付款<span >( {{poDetail.updateDt}} )</span></h3>
												</span>
												<h3 class="title">{{poDetail.productName}}</h3>
												<p class="info">數量
													<span>{{poDetail.quantity}}</span>
												</p>
												<div class="price">
													<span class="total">NT{{poDetail.unitPrice|currency : '$' : 0}}</span>
													小計 NT{{poDetail.totalAmount|currency : '$' : 0}}
												</div>
												<ul class="list-inline">
													<li ng-if="poDetail.logistics">物流公司：{{poDetail.logistics}}</li>
													<li ng-if="poDetail.logisticsNum">出貨單號：{{poDetail.logisticsNum}}</li>
													<li>子訂單編號：{{poDetail.subOrderNo}}</li>
												</ul>
												<div ng-if="poDetail.remark" class="clearfix">
												<a href="javascript:;" class="note-expand" onclick="showNote(this)">備註： {{poDetail.remark|cut:false:10:' ...'}} <i class="icon-expand"></i></a>
													<div class="collapse">
															<p ng-bind-html="poDetail.remark|newline|trustAsHtml"></p>
													</div>
												</div>
											</div>
											<div class="td" ng-switch-default>
												<span ng-switch="poDetail.status">
												<h3 class="title st mb10" ng-switch-when="" ><i class="icon-check-circle-o"></i>已付款<span >( {{poDetail.updateDt}} )</span></h3>
												<h3 class="title st mb10" ng-switch-when="N" ><i class="icon-error_outline"></i>未付款<span >( {{poDetail.updateDt}} )</span></h3>
												<h3 class="title st mb10" ng-switch-when="P" ><i class="icon-check-circle-o"></i>已付款<span >( {{poDetail.updateDt}} )</span></h3>
												<h3 class="title st mb10" ng-switch-when="D" ><i class="icon-check-circle-o"></i>已出貨<span >( {{poDetail.updateDt}} )</span></h3>
												<h3 class="title st mb10" ng-switch-when="R" ><i class="icon-error_outline"></i>已退款<span >( {{poDetail.updateDt}} )</span></h3>
												<h3 class="title st mb10" ng-switch-when="C" ><i class="icon-error_outline"></i>已取消<span >( {{poDetail.updateDt}} )</span></h3>
												<h3 class="title st mb10" ng-switch-when="S" ><i class="icon-error_outline"></i>已逾期<span >( {{poDetail.updateDt}} )</span></h3>
												<h3 class="title st mb10" ng-switch-when="H" ><i class="icon-error_outline"></i>退貨處理中<span >( {{poDetail.updateDt}} )</span></h3>
												<h3 class="title st mb10" ng-switch-when="G" ><i class="icon-error_outline"></i>退款處理中<span >( {{poDetail.updateDt}} )</span></h3>
												<h3 class="title st mb10" ng-switch-when="J" ><i class="icon-error_outline"></i>拒絕退件<span >( {{poDetail.updateDt}} )</span></h3>
												<h3 class="title st mb10" ng-switch-default ><i class="icon-check-circle-o"></i>已付款<span >( {{poDetail.updateDt}} )</span></h3>
												</span>
												<h3 class="title">{{poDetail.productName}}</h3>
												<p class="info">數量
													<span>1</span>
												</p>
												<div class="price">
													<span class="total">NT{{poDetail.totalAmount|currency : '$' : 0}}</span>
													小計 NT{{poDetail.totalAmount|currency : '$' : 0}}
												</div>
												<ul class="list-inline">
													<li ng-if="poDetail.logistics">物流公司：{{poDetail.logistics}}</li>
													<li ng-if="poDetail.logisticsNum">出貨單號：{{poDetail.logisticsNum}}</li>
													<li>子訂單編號：{{poDetail.subOrderNo}}</li>
												</ul>
												<div ng-if="poDetail.remark" class="clearfix">
												<a href="javascript:;" class="note-expand" onclick="showNote(this)">備註： {{poDetail.remark|cut:false:10:' ...'}} <i class="icon-expand"></i></a>
													<div class="collapse">
														<p ng-bind-html="poDetail.remark|newline|trustAsHtml"></p>
													</div>
												</div>
											</div>
										</div>
									</li>
									<!-- /list -->
								</ul>


							</div>
							<!-- /////////////////////END RIGHT//////////////////// -->
						</div>
						<!-- row -->
					</div>
					<!-- container -->
					<div class="modal fade" id="modal-detail">
						<div class="modal-vertical-middle">
							<div class="modal-dialog modal-sm">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
										<h4 class="modal-title">發票明細</h4>
									</div>
									<div class="modal-body">
										<div class="div-tb">
											<div class="td td-nowarp wid_80px lt">發票號碼</div>
											<div class="td rt">{{invoice.invoiceNumber}}</div>
										</div>
										<div class="div-tb">
											<div class="td td-nowarp wid_80px lt">隨機碼</div>
											<div class="td rt">{{invoice.randomNumber}}</div>
										</div>
										<div class="div-tb" ng-if="invoice.customerName">
											<div class="td td-nowarp wid_80px lt">抬頭</div>
											<div class="td rt">{{invoice.customerName}}</div>
										</div>
										<div class="div-tb" ng-if="invoice.customerId">
											<div class="td td-nowarp wid_80px lt">統編</div>
											<div class="td rt">{{invoice.customerId}}</div>
										</div>
										<div class="div-tb">
											<div class="td td-nowarp wid_80px lt">金額</div>
											<div class="td rt">NT{{invoice.salesAmount|currency : '$' : 0}}</div>
										</div>
										<div class="div-tb">
											<div class="td td-nowarp wid_80px lt">載具類別</div>
											<div class="td rt">{{invoice.carruerTypeDesc}}</div>
										</div>
										<div class="div-tb">
											<div class="td td-nowarp wid_80px lt">載具編號</div>
											<div class="td rt">{{invoice.carruerNumber}}</div>
										</div>
										<div class="div-tb">
											<div class="td td-nowarp wid_80px lt">發票開立時間</div>
											<div class="td rt">{{invoice.createDtText}}</div>
										</div>
										<div class="div-tb">
											<div class="td td-nowarp wid_80px lt">發票查詢連結</div>
											<div class="td rt"><a href="https://www.einvoice.nat.gov.tw/APMEMBERVAN/PublicAudit/PublicAudit" target="_blank">按我查詢</a></div>
										</div>
									</div>
								</div>
								<!-- /.modal-content -->
							</div>
							<!-- /.modal-dialog -->
						</div>
					</div>
					<!-- /.modal -->
				</div>