<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
	<!doctype html>
	<!-- angularjs -->
	<script src="${pageContext.request.contextPath}/js/page/order/myOrderController.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
	<div ng-controller="MyOrderController" class="index-main-list for-camp">
		<div class="container">
			<!-- ///////////////////////////////////////////// -->
			<div class="row" bs-loading-overlay="order-block">
				<!-- /////////////////////LEFT//////////////////// -->
				<div class="col-md-4 left-side">
					<div class="left-avatar">
						<h2 class="dark-title ct mt0">我的訂單</h2>
						<img src="${pageContext.request.contextPath}/img/icon/avatar-man.png" alt="" class="img-circle img-thumbnail">
						<p>${user_auth.name}</p>
					</div>
					<div class="left-side-in">
						<!-- MENU -->
						<div class="panel-group sort-panel" id="AA">
							<!-- QA-LIST -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle active one-level" href="#">我的訂單</a>
									</h4>
								</div>
							</div>
							<!--/  QA-LIST -->

						</div>
						<!-- panel-group -->
						<!-- END MENU -->

						<div class="mb15 visible-xs visible-sm"></div>
					</div>
				</div>
				<!-- /////////////////////END LEFT//////////////////// -->
				<!-- /////////////////////RIGHT//////////////////// -->
				<div class="col-md-8 right-content line-left">
					<h2 class="main-title mt0 mb20">我的訂單</h2>
					<table class="table table-bordered  table-striped text-center">
						<thead>
							<tr>
								<td>No.</td>
								<td>訂單號碼</td>
								<td class="hidden-xs">購買日期</td>
								<td class="hidden-xs">訂單狀態</td>
								<td>付款方式</td>
								<td>價格</td>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="order in orderList" ng-switch="order.status">
								<td>{{$index+1}}</td>
								<td>
									<a ng-href="${pageContext.request.contextPath}/order/detail/{{order.mainOrderNo}}" class="btn btn-link">{{order.mainOrderNo}}</a>
								</td>
								<td class="hidden-xs">{{order.createDt}}</td>
								<td ng-switch-when="E" class="hidden-xs">訂單成立</td>
								<td ng-switch-when="S" class="hidden-xs color-red">訂單取消</td>
								<td ng-switch-when="C" class="hidden-xs color-red">訂單取消</td>
								<td ng-switch-default></td>
								<td>
									<div ng-if="order.totalAmount==0">免費</div>
									<div ng-if="order.paymentStatus=='P'">{{order.paymentTypeName}}</div>
									<span ng-if="order.status=='E' && order.paymentStatus=='N' && checkPay(order.paymentDueDate, currentDate|date:'yyyy-MM-dd hh:mm:ss')"><a ng-href="${pageContext.request.contextPath}/cart/checkout?method=cancel&&mainOrderNo={{order.mainOrderNo}}" class="btn btn-link">取消訂單</a></span>
									<span ng-if="order.status=='E' && order.paymentStatus=='N' && checkPay(order.paymentDueDate, currentDate|date:'yyyy-MM-dd hh:mm:ss')"><a ng-href="${pageContext.request.contextPath}/cart/checkout?method=re&&mainOrderNo={{order.mainOrderNo}}" class="btn btn-link">前往付款</a><br><span class="color-red">付款期限：{{order.paymentDueDate}}</span></span>
								</td>
								<td>{{order.totalAmount|currency : '$' : 0}}元</td>
							</tr>
							<tr ng-if="!orderList || orderList.length<=0">
								<td colspan="6">暫無訂單資料</td>
							</tr>
						</tbody>
					</table>
					<e7-pagination page="page" fun="queryMyOrder" pageclass="'ct'"/>


				</div>
				<!-- /////////////////////END RIGHT//////////////////// -->
			</div>
			<!-- row -->
		</div>
		<!-- container -->
	</div>