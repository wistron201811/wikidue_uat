<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<script type="text/javascript">
	var tagQueryApi = "${pageContext.request.contextPath}/api/tag/query";
	var productTagQueryApi = "${pageContext.request.contextPath}/api/product/tag/query";
	var categoryQueryApi = "${pageContext.request.contextPath}/api/category/query";
	var productQueryApi = "${pageContext.request.contextPath}/api/product/query";
	// share
	var shareTagQueryApi = "${pageContext.request.contextPath}/api/share/tag/query";
	var shareQueryApi = "${pageContext.request.contextPath}/api/share/query";
	var checkAccusationApi = "${pageContext.request.contextPath}/api/share/accusation/check";
	var submitAccusationApi = "${pageContext.request.contextPath}/api/share/accusation/submit";
	// cart
	var cartQueryApi = "${pageContext.request.contextPath}/api/cart/query";
	var cartAddApi = "${pageContext.request.contextPath}/api/cart/add";
	var cartRemoveApi = "${pageContext.request.contextPath}/api/cart/remove";
	var cartEditApi = "${pageContext.request.contextPath}/api/cart/edit";
	// 課程
	var coursePageUrl = "${pageContext.request.contextPath}/course/detail/";
	var buyFreeCourseApi = "${pageContext.request.contextPath}/api/cart/buyFreeCourse";
	// 訊息
	var msgQueryApi = "${pageContext.request.contextPath}/api/message";
	var msgCountApi = "${pageContext.request.contextPath}/api/message/count";
	var msgListApi = "${pageContext.request.contextPath}/api/message/list";
	var msgDetailApi = "${pageContext.request.contextPath}/api/message/detail";
	//結帳
	var checkOutPath = "${pageContext.request.contextPath}/cart/checkout";
	// 取得video token
	var videoTokenApi = "${pageContext.request.contextPath}/api/video/token";
	var videoWatchedApi = "${pageContext.request.contextPath}/api/video/count";
	//廣告
	var adPath = "${pageContext.request.contextPath}/api/ad/query";
	//訂單
	var orderListApi = "${pageContext.request.contextPath}/api/myorder/query";
	var orderDetailApi = "${pageContext.request.contextPath}/api/myorder/detail/query";
	// 名人推薦
	var recommendQueryApi = "${pageContext.request.contextPath}/api/recommend/query";
	// 營隊活動
	var campQueryApi = "${pageContext.request.contextPath}/api/camp/query";
	var campListApi = "${pageContext.request.contextPath}/api/camp/list";
	// 加入收藏
	var favCheckApi = "${pageContext.request.contextPath}/api/fav/check";
	var favChangeApi = "${pageContext.request.contextPath}/api/fav/change";
	var favQueryApi = "${pageContext.request.contextPath}/api/fav/query";
	// 課程討論
	var discussionQueryApi = "${pageContext.request.contextPath}/api/discussion/query";
	var discussionAskApi = "${pageContext.request.contextPath}/api/discussion/ask";
	var discussionReplyApi = "${pageContext.request.contextPath}/api/discussion/reply";
	// 發票資訊
	var invoiceQueryApi = "${pageContext.request.contextPath}/api/invoice/query";
	// 付款結果查詢
	var paymentResult = "${pageContext.request.contextPath}/api/checkPaymentResult";
</script>