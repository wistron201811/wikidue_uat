<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<div class="index-main-list for-camp">
	<div class="container">
		<div class="row">
			<!-- /////////////////////RIGHT//////////////////// -->
			<div class="col-md-12 recoment-left">
				<h2>${recommendForm.title }</h2>
				<small>${recommendForm.author } <span class="color-gray">
				<fmt:formatDate  pattern="yyyy-MM-dd" value="${recommendForm.createDt }"/>
				</span></small>
				<hr>
				<div class="rt">
					<a href="${pageContext.request.contextPath}/recommend/home" class="btn btn-default"><i class="icon-back"></i>  回到列表 </a>
				</div>
				<div class="clearfix mb10"></div>
				${e7:previewHtmlImg(pageContext.request.contextPath,recommendForm.content)}
				<hr>
				<div class="rt">
					<a href="${pageContext.request.contextPath}/recommend/home" class="btn btn-default"><i class="icon-back"></i>  回到列表 </a>
				</div>
			</div>
			<!-- /////////////////////END RIGHT//////////////////// -->
			<!-- /////////////////////LEFT//////////////////// -->
			<%-- <div class="col-md-4 recoment-right">
				${e7:previewHtmlImg(pageContext.request.contextPath,recommendForm.rightBlock)}
			</div> --%>
			<!-- /////////////////////END LEFT//////////////////// -->
		</div>
		<!-- row -->
	</div>
	<!-- container -->
</div>