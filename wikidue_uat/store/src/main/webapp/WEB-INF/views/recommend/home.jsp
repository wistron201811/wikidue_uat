<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
	<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
	<script src="${pageContext.request.contextPath}/js/page/recommend/homeController.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
	<div ng-controller="HomeController">
		<div class="index-main-list for-camp" bs-loading-overlay="query-block">
			<div class="container">
				<div class="mt30 visible-xs"></div>
				<h2 class="mix-title mb20">分享園地</h2>
				<!-- ///////////////////////////////////////////// -->

				<ul class="camp-list" data-ng-init="queryRecommend(1)">
					<li ng-repeat="recommend in list">
						<div class="row gap20">
							<div class="col-sm-4 camp-img" ng-click="goPage('${pageContext.request.contextPath}/recommend/detail/'+recommend.pkRecommend)">
								<img ng-src="{{recommend.image}}" alt="" class="img-responsive">
							</div>
							<div class="clearfix visible-xs mb20"></div>
							<div style="cursor: pointer;" class="col-sm-8 camp-des" ng-click="goPage('${pageContext.request.contextPath}/recommend/detail/'+recommend.pkRecommend)">
								<h3>{{recommend.title}}</h3>
								<div class="div-tb">
									<div class="td td-nowarp">作者 : </div>
									<div class="td">{{recommend.author}}</div>
								</div>
								<div class="div-tb">
									<div class="td td-nowarp">日期 : </div>
									<div class="td">{{recommend.createDt}}</div>
								</div>
								<div class="div-tb">
									<div class="td ">{{recommend.summary|cut:false:20:' ...'}}</div>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<!-- container -->
		</div>
		<e7-pagination page="page" fun="queryRecommend"/>
	</div>