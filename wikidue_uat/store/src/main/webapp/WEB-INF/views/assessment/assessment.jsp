<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <div class="col-sm-4">
        <h4 class="orange-title">
            <a href="/assess/paper/paperAction!wikidueExamIndex.so">檢測中心</a>
        </h4>
        <a href="/assess/paper/paperAction!wikidueExamIndex.so" class="main-img">
            <img src="${pageContext.request.contextPath}/img/default_pics/Default_test-promote.png" class="scale">
        </a>
        <ul class="pic-list">
            <li>
                <a href="/assess/paper/paperAction!wikidueExamIndex.so" class="only-pic">
                    <img src="${pageContext.request.contextPath}/img/default_pics/Default_test-science.png" class="scale-img">
                </a>
            </li>
            <li>
                <a href="/assess/paper/paperAction!wikidueExamList.so" class="only-pic">
                    <img src="${pageContext.request.contextPath}/img/default_pics/Default_test-APCS.png" class="scale-img">
                </a>
            </li>
            <li>
                <a href="/assess/paper/paperAction!wikidueExamList.so?subjectName=APCS" class="only-pic">
                    <img src="${pageContext.request.contextPath}/img/default_pics/Default_test-more.png" class="scale-img">
                </a>
            </li>
        </ul>
        <div class="rt">
            <hr class="mt0 mb10">
            <a href="javascript:;" class="btn btn-default btn-sm">More</a>
        </div>
    </div>