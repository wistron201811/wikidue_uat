<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Wikidue Store 教育商城 - 登入頁</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
</head>
<body>
	<form name='loginForm' action="${ssoLoginPage}" method="post">
		<input type="hidden" name="client_id" value="${clientId}"/> 
		<input type="hidden" name="response_type" value="${responseType}"/>
		<input type="hidden" name="redirect_uri" value="${redirectUri}"/>
	</form>
	<script type="text/javascript">document.loginForm.submit();</script>
</body>
</html>