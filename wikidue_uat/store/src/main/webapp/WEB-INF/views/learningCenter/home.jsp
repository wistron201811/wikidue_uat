<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
        <%@ taglib uri="/WEB-INF/tld/e7.tld" prefix="e7" %>
            <%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
                <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c" %>
                    <%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
                        <%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
                            <!-- angularjs -->
                            <script src="${pageContext.request.contextPath}/js/page/learningCenter/homeController.js?v=<spring:eval expression=" @configService.applicationVersion
                                " />"></script>

                            <form:form modelAttribute="learningCenterForm" method="POST" action="${pageContext.request.contextPath}/learningCenter">
                                <input type="hidden" name="method" value="query" />
                                <div class="index-main-list for-camp" ng-controller="LearningCenterController">
                                    <div class="container">
                                        <!-- ///////////////////////////////////////////// -->
                                        <div class="row">
                                            <!-- /////////////////////LEFT//////////////////// -->
                                            <div class="col-md-4 left-side">
                                                <div class="left-avatar">
                                                    <h2 class="dark-title ct mt0">學習中心</h2>
                                                    <img src="${pageContext.request.contextPath}/img/icon/avatar-man.png" alt="" class="img-circle img-thumbnail">
                                                    <p>${user_auth.name}</p>
                                                </div>
                                                <div class="left-side-in">
                                                    <!-- MENU -->
                                                    <div class="panel-group sort-panel" id="AA">
                                                        <!-- QA-LIST -->
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle active one-level" href="javascript:;">學習中心</a>
                                                                </h4>
                                                            </div>
                                                        </div>
                                                        <!--/  QA-LIST -->
                                                        <!-- QA-LIST -->
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle one-level" href="${pageContext.request.contextPath}/favourite/home">我的收藏</a>
                                                                </h4>
                                                            </div>
                                                        </div>
                                                        <!--/  QA-LIST -->
                                                    </div>
                                                    <!-- panel-group -->
                                                    <!-- END MENU -->

                                                    <div class="mb15 visible-xs visible-sm"></div>
                                                </div>
                                            </div>
                                            <!-- /////////////////////END LEFT//////////////////// -->
                                            <!-- /////////////////////RIGHT//////////////////// -->
                                            <div class="col-md-8 right-content line-left">
                                                <c:if test="${not empty lastWatchCourse}">
                                                    <!-- 上次學習 start -->
                                                    <h2 class="main-title mt0 mb20">上次學習</h2>
                                                    <ul class="camp-list">
                                                        <!-- list -->
                                                        <li>
                                                            <div class="row gap20">
                                                                <div class="col-sm-4 camp-img">
                                                                    <a href="${pageContext.request.contextPath}/course/${lastWatchCourse.courseId}"><img src="${pageContext.request.contextPath}/api/query/image/${lastWatchCourse.imageId}" alt="" class="img-responsive"></a>
                                                                </div>
                                                                <div class="clearfix visible-xs mb20"></div>
                                                                <div class="col-sm-6 camp-des">
                                                                    <h4>${lastWatchCourse.courseName}</h4>
                                                                    <small class="color-gray">
                                                                        <i class=" icon-top_clock"></i>
                                                                        <fmt:formatNumber type="number" minIntegerDigits="2" value="${lastWatchCourse.courseDurationHour}" />h
                                                                        <fmt:formatNumber type="number" minIntegerDigits="2" value="${lastWatchCourse.courseDurationMinute}" />m</small>
                                                                    <div class="mt15 keep-go">
                                                                        <div class="div-tb">
                                                                            <div class="td vmid" data-toggle="tooltip" title="${lastWatchCourse.lastWatchVideoName}">
                                                                                <fmt:formatDate pattern="yyyy/MM/dd" value="${lastWatchCourse.lastWatchDt}" /> 學習了${lastWatchCourse.lastWatchVideoNameBrief}</div>
                                                                            <div class="td vmid rt">
                                                                                <a href="${pageContext.request.contextPath}/course/play?id=${lastWatchCourse.courseId}&videoId=${lastWatchCourse.lastWatchVideoId}"
                                                                                    class="btn btn-success">
                                                                                    <i class="icon-video-camera"></i> 繼續學習</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div class="clearfix visible-xs">
                                                                    <hr>
                                                                </div>
                                                                <div class="col-sm-2 ct">
                                                                    <!-- /////////////////////////////////////// -->
                                                                    <div class="percent-hold">
                                                                        <div class="progress-t"> ${lastWatchCourse.progress}% </div>
                                                                        <div class="easy-pie-chart percentage" data-percent="${lastWatchCourse.progress}" data-size="110"> </div>
                                                                    </div>
                                                                    <!-- /////////////////////////////////////// -->
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <!-- END list -->
                                                    </ul>
                                                    <!-- 上次學習 end -->
                                                </c:if>
                                                <div class="row gap20 mt20">
                                                    <div class="col-sm-5">
                                                        <div class="input-group search">
                                                            <form:input path="keyword" cssClass="form-control" placeholder="輸入您要查詢的關鍵字" maxlength="15" />
                                                            <span class="input-group-btn">
                                                                <a href="#" onclick="$('#learningCenterForm').submit()" class="btn btn-warning">
                                                                    <i class="icon-main_search"></i>
                                                                </a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h2 class="main-title mb20">我的課程</h2>
                                                <ul class="camp-list">
                                                    <c:if test="${not empty courseList}">
                                                        <c:forEach items="${courseList}" var="course">
                                                            <!-- list -->
                                                            <li>
                                                                <div class="row gap20">
                                                                    <div class="col-sm-4 camp-img">
                                                                        <a href="${pageContext.request.contextPath}/course/${course.courseId}"><img src="${pageContext.request.contextPath}/api/query/image/${course.imageId}" alt="" class="img-responsive"></a>
                                                                    </div>
                                                                    <div class="clearfix visible-xs mb20"></div>
                                                                    <div class="col-sm-6 camp-des">
                                                                        <h4>${course.courseName}</h4>
                                                                        <small class="color-gray">
                                                                            <i class=" icon-top_clock"></i>
                                                                            <fmt:formatNumber type="number" minIntegerDigits="2" value="${course.courseDurationHour}" />h
                                                                            <fmt:formatNumber type="number" minIntegerDigits="2" value="${course.courseDurationMinute}" />m</small>
                                                                        <div class="mt15 keep-go">
                                                                            <div class="div-tb">
                                                                            	<c:if test="${not empty course.lastWatchVideoId}">
	                                                                            	<div class="td vmid" data-toggle="tooltip" title="${course.lastWatchVideoName}">
	                                                                                    <fmt:formatDate pattern="yyyy/MM/dd" value="${course.lastWatchDt}" /> 學習了${course.lastWatchVideoNameBrief}</div>
	                                                                                <div class="td vmid rt">
	                                                                                    <a href="${pageContext.request.contextPath}/course/play?id=${course.courseId}&videoId=${course.lastWatchVideoId}"
	                                                                                        class="btn btn-success">
	                                                                                        <i class="icon-video-camera"></i> 繼續學習</a>
	                                                                                </div>
                                                                            	</c:if>
                                                                            	<c:if test="${empty course.lastWatchVideoId}">
                                                                            		<div class="td vmid">尚未學習</div>
	                                                                                <div class="td vmid rt">
	                                                                                    <a href="${pageContext.request.contextPath}/course/play?id=${course.courseId}"
	                                                                                        class="btn btn-success">
	                                                                                        <i class="icon-video-camera"></i> 繼續學習</a>
	                                                                                </div>
                                                                            	</c:if>
                                                                                
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                    <div class="clearfix visible-xs">
                                                                        <hr>
                                                                    </div>
                                                                    <div class="col-sm-2 ct">
                                                                        <!-- /////////////////////////////////////// -->
                                                                        <div class="percent-hold">
                                                                            <div class="progress-t"> ${course.progress}% </div>
                                                                            <div class="easy-pie-chart percentage" data-percent="${course.progress}" data-size="110"> </div>
                                                                        </div>
                                                                        <!-- /////////////////////////////////////// -->
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <!-- END list -->
                                                        </c:forEach>
                                                    </c:if>
                                                    <c:if test="${empty courseList}">
                                                        <div class="alert alert-warning">
                                                            搜尋結果：沒有資料
                                                        </div>
                                                    </c:if>
                                                </ul>
                                            </div>
                                            <!-- /////////////////////END RIGHT//////////////////// -->
                                        </div>
                                        <!-- row -->
                                    </div>
                                    <!-- container -->
                                </div>
                            </form:form>