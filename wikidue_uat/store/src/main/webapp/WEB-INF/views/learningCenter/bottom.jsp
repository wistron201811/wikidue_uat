<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- /footer -->
<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/plugin/jquery-cookie/jquery.cookie.js"></script>
<!-- <script src="js/jquery-migrate-1.2.1.min.js"></script>   -->
<script src="${pageContext.request.contextPath}/plugin/bootstrap/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/base.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
<script src="${pageContext.request.contextPath}/plugin/bootstrap-select/js/bootstrap-select.min.js"></script>
<script src="${pageContext.request.contextPath}/plugin/easy-pie-chart.min.js"></script>

<script type="text/javascript">

	  //學習中心廣告群組
	  categoryCode = "ADIN007";

      jQuery(function($) {
        $('.easy-pie-chart.percentage').each(function(){
          var $box = $(this).closest('.infobox');
          var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : '#000');
          var trackColor = barColor == '#000' ? '#000' : '#ccc';
          var size = parseInt($(this).data('size')) || 50;
          $(this).easyPieChart({
            barColor: barColor,
            trackColor: trackColor,
            scaleColor: false,
            lineCap: 'butt',
            lineWidth: parseInt(size/10),
            animate: /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase()) ? false : 1000,
            size: size
          });
        });
        $('[data-toggle="tooltip"]').tooltip(); 
      });
</script>
