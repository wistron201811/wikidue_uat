<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<script
	src="${pageContext.request.contextPath}/js/page/props/detailController.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>

<div class="product-detail" ng-controller="DetailController">
	<div class="container">
		<!-- //////////////////////////////// -->
		<div class="row">
			<!-- PRODUCT PIC -->
			<div class="col-md-7 col-sm-6">
				<!-- carousel -->
				<div class="carousel slide article-slide carousel-fade" id="Photo">
					<!-- Wrapper for slides -->
					<div class="carousel-inner">
						<c:if test="${not empty  propsForm.photoOneId}">
							<div class="item active">
								<img
									src="${pageContext.request.contextPath}/api/query/image/${propsForm.photoOneId.pkFile}"
									alt="" class="img-responsive">

							</div>
						</c:if>
						<c:if test="${not empty  propsForm.photoTwoId}">
							<div class="item">
								<img
									src="${pageContext.request.contextPath}/api/query/image/${propsForm.photoTwoId.pkFile}"
									alt="" class="img-responsive">
							</div>
						</c:if>
						<c:if test="${not empty  propsForm.photoThirdId}">
							<div class="item">
								<img
									src="${pageContext.request.contextPath}/api/query/image/${propsForm.photoThirdId.pkFile}"
									alt="" class="img-responsive">
							</div>
						</c:if>
						<c:if test="${not empty  propsForm.photoFourthId}">
							<div class="item">
								<img
									src="${pageContext.request.contextPath}/api/query/image/${propsForm.photoFourthId.pkFile}"
									alt="" class="img-responsive">
							</div>
						</c:if>
					</div>
					<!-- Indicators -->
					<ol class="carousel-indicators hidden-xs">
						<c:set var="slideCounter" value="-1"></c:set>
						<c:if test="${not empty  propsForm.photoOneId}">
							<c:set var="slideCounter" value="${slideCounter + 1 }"></c:set>
							<li class="active" data-slide-to="${slideCounter}"
								data-target="#Photo"><img alt=""
								src="${pageContext.request.contextPath}/api/query/image/${propsForm.photoOneId.pkFile}">
							</li>
						</c:if>
						<c:if test="${not empty  propsForm.photoTwoId}">
							<c:set var="slideCounter" value="${slideCounter + 1 }"></c:set>
							<li class="" data-slide-to="${slideCounter}" data-target="#Photo"><img
								alt=""
								src="${pageContext.request.contextPath}/api/query/image/${propsForm.photoTwoId.pkFile}">
							</li>
						</c:if>
						<c:if test="${not empty  propsForm.photoThirdId}">
							<c:set var="slideCounter" value="${slideCounter + 1 }"></c:set>
							<li class="" data-slide-to="${slideCounter}" data-target="#Photo"><img
								alt=""
								src="${pageContext.request.contextPath}/api/query/image/${propsForm.photoThirdId.pkFile}">
							</li>
						</c:if>
						<c:if test="${not empty  propsForm.photoFourthId}">
							<c:set var="slideCounter" value="${slideCounter + 1 }"></c:set>
							<li class="" data-slide-to="${slideCounter}" data-target="#Photo"><img
								alt=""
								src="${pageContext.request.contextPath}/api/query/image/${propsForm.photoFourthId.pkFile}">
							</li>
						</c:if>
					</ol>
					<!-- / Indicators -->
					<!-- arrow -->
					<a class="left carousel-control" href="#Photo" role="button"
						data-slide="prev"> <i class="icon-expand_left"></i>
					</a> <a class="right carousel-control" href="#Photo" role="button"
						data-slide="next"> <i class="icon-expand_right"></i>
					</a>
					<!-- arrow -->
				</div>
				<!--End carousel -->

			</div>
			<!-- END PRODUCT PIC -->
			<!-- PRODUCT INFO -->
			<div class="col-md-5 col-sm-6 " bs-loading-overlay="add-cart-block">
				<div class="clearfix mb20 visible-xs"></div>
				<a href="javascript:;" ng-click="setFav('${propsForm.productPk}')"
					data-ng-init="checkFav('${propsForm.productPk}')" class="love"
					ng-class="{active:isFav}" bs-loading-overlay="fav-block"> <i></i>
				</a>
				<h2 class="product-title m-b-5">${propsForm.propsName }</h2>
				<p>${propsForm.propsSummary }</p>
				<!-- list -->
				<div class="div-tb mt20">
					<div class="td td-nowarp vmid">原價 :</div>
					<div class="td td-nowarp pl10 vmid">
						<s class="color-gray"> $ <fmt:formatNumber
								value="${propsForm.price }" type="number" maxFractionDigits="0" />
						</s>
					</div>
					<div class="td pl10 vmid">
						<span class="color-orange price-big pl10"> $ <fmt:formatNumber
								value="${propsForm.discountPrice }" type="number"
								maxFractionDigits="0" />
						</span>
					</div>
				</div>
				<!-- list -->
				<!-- list -->
				<c:if test="${existCart}">
					<div class="btn-group mt20" style="width: 300px">
						<a href="${pageContext.request.contextPath}/cart/checkout"
							class="btn btn-danger wid_300px">已放入購物車，前往結帳</a>
					</div>
				</c:if>
				<c:if test="${!existCart}">
					<c:if
						test="${not empty  propsForm.quantity and propsForm.quantity > 0}">
						<div class="div-tb mt20">
							<div class="td td-nowarp vmid">數量 :</div>
							<div class="td pl10 vmid wid_80px">
								<select id="selQty" ng-model="selQty"
									class="btn dropdown-toggle btn-default" data-width="100%"
									title="請選擇">
									<c:choose>
										<c:when test="${empty  propsForm.quantity}">
											<c:set var="totalQuantity" value="0"></c:set>
										</c:when>
										<%--最多一次買10個 --%>
										<c:when
											test="${not empty  propsForm.quantity and propsForm.quantity > 10}">
											<c:set var="totalQuantity" value="10"></c:set>
										</c:when>
										<c:otherwise>
											<c:set var="totalQuantity" value="${ propsForm.quantity}"></c:set>
										</c:otherwise>
									</c:choose>
									<c:forEach begin="1" end="${totalQuantity }" var="count">
										<option>${count }</option>
									</c:forEach>
								</select>
							</div>
							<div class="td"></div>
						</div>
					</c:if>
					<!-- list -->
					<!-- list -->
					<div class="btn-group mt20" style="width: 300px">
						<c:if
							test="${not empty  propsForm.quantity and propsForm.quantity > 0}">
							<a href="javascript:;"
								ng-click="goCheckout('${propsForm.productPk}')"
								class="btn btn-danger wid_150px">立即結帳</a>
							<a href="javascript:;" class="btn btn-default wid_150px"
								ng-click="addCart('${propsForm.productPk}')"> <i
								class="icon-shopping-cart2"></i> 加入購物車
							</a>
						</c:if>
						<c:if
							test="${empty  propsForm.quantity or propsForm.quantity == '0'}">
							<a href="javascript:;" class="btn btn-default wid_300px" disabled>
								<i class="icon-shopping-cart2"></i> 已售完
							</a>
						</c:if>
					</div>
					<!-- list -->
				</c:if>
			</div>
			<!-- END PRODUCT INFO -->

		</div>
		<!-- row -->
		<!-- //////////////////////////////// -->
		<div class="clearfix mb70 hidden-xs"></div>
		<div class="clearfix mb30 visible-xs"></div>
		<!-- //////////////////////////////// -->
		<div class="tabs-widget ">
			<ul class="nav nav-tabs" role="tablist">
				<li class="active"><a>商品說明</a></li>
			</ul>
			<div class="tab-content">
				<!-- tab-01 -->
				<div class="tab-pane active" id="tab-01">
					<div class="row">
						<div class="col-md-8 col-md-push-2 mt30">
							<dl class="dl-1">
								${e7:previewHtmlImg(pageContext.request.contextPath,propsForm.propsIntroduction)}
							</dl>
						</div>
					</div>
					<!-- row -->
				</div>
				<!-- END tab-01 -->
			</div>
		</div>
		<!-- //////////////////////////////// -->
		<hr>
		<a href="${pageContext.request.contextPath}/props/home" class="btn btn-default pull-right">回上一頁 <i class="icon-back"></i></a>	  
	</div>
	<!-- container -->
</div>
<!-- product-detail -->