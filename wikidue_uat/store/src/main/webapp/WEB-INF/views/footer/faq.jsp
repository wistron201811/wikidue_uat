<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <div class="tabs-v2">
        <%-- <ul class="nav nav-tabs" role="tablist">
            <li class="active">
                <a href="#tab-01" data-toggle="tab">
                    <p class="has-icon">
                        <i class="icon-student"></i>
                    </p>
                    我是學生
                </a>
            </li>
            <li>
                <a href="#tab-02" data-toggle="tab">
                    <p class="has-icon">
                        <i class="icon-teacher"></i>
                    </p>
                    我是老師
                </a>
            </li>
        </ul>--%>
    </div> 
    <div class="container">
        <div class="tab-content">
            <!-- tab-01 -->
            <div class="tab-pane fade in  active" id="tab-01">
                <div class="row">
                    <!-- list -->
                    <div class="col-xs-12">
                        <div class="panel panel-default qa-toggle">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#c1">
                                    一、 線上課程常見問題
                                </a>
                            </div>
                            <div id="c1" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <ul class="row qa-list">
                                        <li class="col-md-4 col-sm-6">
                                            <a href="#modal-qa0-1" data-toggle="modal">線上課程購買流程</a>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <a href="#modal-qa0-2" data-toggle="modal">退款方式說明</a>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <a href="#modal-qa1-1" data-toggle="modal"> 進入學習中心沒有看到自己的課程怎麼辦？</a>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <a href="#modal-qa1-2" data-toggle="modal">課程影片無法播放</a>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <a href="#modal-qa1-3" data-toggle="modal"> 課程內容品質與測驗問題</a>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <a href="#modal-qa1-4" data-toggle="modal"> 找不到我購買的課程</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- / list -->
                    <!-- list -->
                    <div class="col-xs-12">
                        <div class="panel panel-default qa-toggle">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#c2">
                                    二、 教具商品常見問題
                                </a>
                            </div>
                            <div id="c2" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <ul class="row qa-list">
                                        <li class="col-md-4 col-sm-6">
                                            <a href="#modal-qa20-1" data-toggle="modal">商品購買流程</a>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <a href="#modal-qa20-2" data-toggle="modal">退款方式說明</a>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <a href="#modal-qa2-1" data-toggle="modal">常見Q&A</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- / list -->
                    <!-- list -->
                    <div class="col-xs-12">
                        <div class="panel panel-default qa-toggle">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#c3">
                                    三、 付款常見問題
                                </a>
                            </div>
                            <div id="c3" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <ul class="row qa-list">
                                        <li class="col-md-4 col-sm-6">
                                            <a href="#modal-qa3-1" data-toggle="modal"> 如何知道我是否付款成功了呢？付款完成後多久可以查到？</a>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <a href="#modal-qa3-2" data-toggle="modal">有提供分期付款的服務嗎？</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- / list -->
                    <!-- list -->
                    <div class="col-xs-12">
                        <div class="panel panel-default qa-toggle">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#c4">
                                    四、 發票問題
                                </a>
                            </div>
                            <div id="c4" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <ul class="row qa-list">
                                        <li class="col-md-4 col-sm-6">
                                            <a href="#modal-qa4-1" data-toggle="modal">Wikidue Store 教育商城選擇發票類型說明：</a>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <a href="#modal-qa4-2" data-toggle="modal">什麼是電子發票？是否會寄給我？</a>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <a href="#modal-qa4-3" data-toggle="modal">我怎麼知道我的發票是否中獎？若中獎發票是否會寄給我？</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- / list -->
                    <!-- list -->
                    <div class="col-xs-12">
                        <div class="panel panel-default qa-toggle">
                            <div class="panel-heading">
                                <a data-toggle="collapse" href="#c5">
                                    五、 其他常見問題
                                </a>
                            </div>
                            <div id="c5" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <ul class="row qa-list">
                                        <li class="col-md-4 col-sm-6">
                                            <a href="#modal-qa5-1" data-toggle="modal">忘記帳號密碼怎麼辦？</a>
                                        </li>
                                        <!--<li class="col-md-4 col-sm-6">
                                            <a href="#modal-qa5-2" data-toggle="modal">沒有收到驗證信怎麼辦？</a>
                                        </li>
                                        <li class="col-md-4 col-sm-6">
                                            <a href="#modal-qa5-3" data-toggle="modal">查詢或更正個人資料</a>
                                        </li>-->
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- / list -->
                </div>
                <!-- row -->
            </div>
            <!-- END tab-01 -->
            <!-- END tab-02 -->
            <div class="tab-pane fade" id="tab-02">
                002
                <!-- row -->
            </div>
            <!-- END tab-02 -->

        </div>
    </div>
    <!-- containter   -->
    <jsp:include page="qa.jsp"></jsp:include>