<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <div class="load-qa">
        <div class="modal fade" id="modal-qa0-1">
            <div class="modal-vertical-middle">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">線上課程購買流程</h4>
                        </div>
                        <div class="modal-body lt">
                            <ul class="list-num">
                                <li>選擇課程：
                                    <br>先挑選您想要的線上課程，若您選定了想購買的課程，請按下該課程旁的「立即結帳」或「加入購物車」鍵。</li>
                                <li>確認訂單：
                                    <br>確認您所購買的線上課程是否正確。</li>
                                <li>填寫付款資訊：
                                    <br>請選擇發票類型，並請仔細填寫購買人等相關資料。</li>
                                <li>前往付款：
                                    <br>填寫信用卡相關資訊，確認後送出即完成訂購。</li>
                                <li>email確認訂購完成：
                                    <br>完成訂購程序後，系統會將訂單通知信email傳送至您的電子信箱。您也可以至顧客中心「查訂單」確認訂單。</li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog modal-lg -->
            </div>
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-qa0-2">
            <div class="modal-vertical-middle">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">退款方式說明</h4>
                        </div>
                        <div class="modal-body lt">
                            <ul class="list-num">
                                <li>
                                    您若同時符合以下兩個退款條件，則可申請退款。
                                    <ol class="list-dot">
                                        <li>在購買七日(鑑賞期)內申請退款</li>
                                        <li>課程影片觀看時數未超過課程總時數的20%</li>
                                    </ol>
                                </li>
                                <li>
                                    若符合退款條件，請來信至客服信箱<a href="mailto:store_service@wikidue.com" class="inline-block" target="_blank">store_service@wikidue.com</a>，並提供以下資訊：
                                    <ol class="list-dot">
                                        <li>Wikidue Store 教育商城帳號：您註冊Wikidue Store 教育商城使用的Email帳號</li>
                                        <li>訂單號碼：登入帳號後，點選右上角「會員」選擇「我的訂單」，即可查看訂單號碼。</li>
                                        <li>退款原因：請簡單地說明您申請退款的原因。</li>
                                    </ol>
                                </li>
                                <li>收到您的來信並確認您符合退款的資格後，客服中心將會協助您辦理退款。</li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog modal-lg -->
            </div>
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-qa1-1">
            <div class="modal-vertical-middle">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">進入學習中心沒有看到自己的課程怎麼辦？</h4>
                        </div>
                        <div class="modal-body lt">
                            <ul class="list-num">
                                <li> 請先清除瀏覽器的快取和Cookie，再重新登入確認課程有沒有出現。 </li>
                                <li> 建議您使用最佳瀏覽器Chrome（其他瀏覽器可能會有部分功能不支援）。 </li>
                                <li>
                                    若清除瀏覽器的快取和Cookie後，登入帳號還是沒有看到課程，請提供以下資料：
                                    <ol class="list-dot">
                                        <li>TibaMe帳號</li>
                                        <li>學習中心「我的課程」的截圖</li>
                                    </ol>

                                </li>
                            </ul>
                            並寄信至客服中心(
                            <a href="mailto:shop_service@wikidue.com" class="inline-block" target="_blank">shop_service@wikidue.com</a>)，收到後我們會盡快請系統人員協助您排除問題！
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog modal-lg -->
            </div>
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-qa1-2">
            <div class="modal-vertical-middle">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">課程影片無法播放</h4>
                        </div>
                        <div class="modal-body lt">
                            如果遇到課程影片無法順利播放的情形，請試試看以下的方法來排除狀況：
                            <ul class="list-num">
                                <li>先確認您的網路環境是否有問題後，重新整理課程影片頁面 </li>
                                <li> 將瀏覽器的 Cookie 清除 </li>
                                <li>
                                    更換其他瀏覽器（Google Chrome、Firefox、Internet Explorer…等）
                                </li>
                            </ul>
                            若試過以上方法仍舊無法順利播放影片，麻煩請來信至客服中心(
                            <a href="mailto:store_service@wikidue.com" class="inline-block" target="_blank">store_service@wikidue.com</a>)，我們會盡快請系統人員協助您排除問題！
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog modal-lg -->
            </div>
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-qa1-3">
            <div class="modal-vertical-middle">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">課程內容品質與測驗問題</h4>
                        </div>
                        <div class="modal-body lt">
                            若您在學習過程中，遇到任何關於課程內容品質或是測驗問題，請來信至客服中心(
                            <a href="mailto:shop_service@wikidue.com" class="inline-block" target="_blank">shop_service@wikidue.com</a>)，並提供以下資訊：
                            <ul class="list-num">
                                <li>課程名稱（包括在哪個單元、哪個小節）</li>
                                <li>遇到的問題（請詳細描述）</li>
                                <li> 問題截圖 </li>
                            </ul>
                            收到您的來信後，我們將盡快為您處理！
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog modal-lg -->
            </div>
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-qa1-4">
            <div class="modal-vertical-middle">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">找不到我購買的課程</h4>
                        </div>
                        <div class="modal-body lt">
                            請先確認您註冊帳號的方式。不同帳號的購買，會歸存在其購買之帳號喔！ 若確認登入帳號無誤，在學習中心仍無法找到購買的課程，請來信至客服中心(
                            <a href="mailto:shop_service@wikidue.com" class="inline-block" target="_blank">shop_service@wikidue.com</a>)，並提供以下資訊：

                            <ul class="list-num">
                                <li>Wikidue Store 教育商城帳號：您註冊Wikidue Store使用的Email帳號</li>
                                <li>訂單號碼：登入帳號後，點選右上角「會員」選擇「我的訂單」，即可查看訂單號碼。</li>
                                <li> 課程名稱：請告訴我們您找不到哪一堂課程。 </li>
                            </ul>
                            收到您的來信後，我們將盡快為您處理！
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog modal-lg -->
            </div>
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-qa20-1">
            <div class="modal-vertical-middle">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">商品購買流程</h4>
                        </div>
                        <div class="modal-body lt">
                            <ul class="list-num">
                                <li>選擇商品：
                                    <br>先挑選您想要的商品，若您選定了想購買的商品，請按下該商品旁的「立即結帳」或「加入購物車」鍵。</li>
                                <li>確認訂單：
                                    <br>確認您所購買的品項與數量是否正確。</li>
                                <li>填寫付款資訊：
                                    <br>請選擇發票類型，並請仔細填寫購買人等相關資料。</li>
                                <li>填寫收貨資訊：
                                    <br>請輸入收件人的相關資料。</li>
                                <li>前往付款：
                                    <br>填寫信用卡相關資訊，確認後送出即完成訂購。</li>
                                <li>email確認訂購完成：
                                    <br>完成訂購程序後，系統會將訂單通知信email傳送至您的電子信箱。您也可以至顧客中心「查訂單」確認訂單。</li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog modal-lg -->
            </div>
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-qa20-2">
            <div class="modal-vertical-middle">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">退款方式說明</h4>
                        </div>
                        <div class="modal-body lt">
                            <ul class="list-num">
                                <li>依照消費者保護法的規定，您享有商品貨到次日起七天鑑賞期的權益。但鑑賞期並非試用期。因此請您留意，您所退回的商品必須回復原狀（須回復至商品到貨時的原始狀態）並且保持完整包裝（包括商品本體、配件、贈品、保證書、原廠包裝及所有附隨文件或資料的完整性），切勿缺漏任何配件或損毀原廠外盒。
                                </li>
                                <li>
                                    寄信至客服中心(
                                    <a href="mailto:shop_service@wikidue.com" class="inline-block" target="_blank">shop_service@wikidue.com</a>)申請退貨，並請在信件內寫明以下資訊：
                                    <ol class="list-dot">
                                        <li>Wikidue Store 教育商城帳號：您註冊Wikidue Store使用的Email帳號</li>
                                        <li>訂單號碼：登入帳號後，點選右上角「會員」選擇「我的訂單」，即可查看訂單號碼。</li>
                                        <li>退貨原因：請簡單地說明您申請退貨的原因。</li>
                                    </ol>
                                </li>
                                <li>在收到您的來信並確認您符合退貨的資格後，客服中心將會與您聯繫並協助您辦理退貨。</li>
								<li>若您購買的商品在兩件以上，為了降低您退貨時的不便，建議您可以在所訂購的商品全數收到之後，再將您要退貨的商品一次辦理。</li>
                                <li>
                                    退款說明：
                                    <ol class="list-dot">
                                        <li>商品必須經物流(供應商)端確定回收、檢測，並完成「銷貨折讓單」簽回相關事宜。</li>
                                        <li> 商品退回確認無誤後，會在7～14個工作天內將您的款項直接刷退。</li>
                                    </ol>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog modal-lg -->
            </div>
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-qa2-1">
            <div class="modal-vertical-middle">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">商品常見問題</h4>
                        </div>
                        <div class="modal-body lt">
                            <ul class="list-num">
                                <li>商品有缺件、破損怎麼辦：
                                    <br>就貨品之包裝、運送及送達過程中所生之危險，及貨品有缺件、污損、錯誤等，請與客服中心聯繫，本平台將依消費者之要求，儘速通知提供貨品之廠商處理。</li>
                                <li>
                                    商品有瑕疵怎麼辦：
                                    <br> 若您所訂購的商品有瑕疵之情形，您可以要求全額退費，請與客服中心聯繫，本平台將依消費者之要求，儘速通知提供貨品之廠商處理。
                                </li>
                                <li>訂購後要多久才收得到貨?
                                    <br> 一般商品將於您付款成功後的二～十個工作天(不含例假日)，出貨至您指定的地點。
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog modal-lg -->
            </div>
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-qa3-1">
            <div class="modal-vertical-middle">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">如何知道我是否付款成功了呢？付款完成後多久可以查到？</h4>
                        </div>
                        <div class="modal-body lt">
                            請您至網頁上方的「會員」的「我的訂單」中查詢所訂購商品的狀況。若已付款成功在訂單狀態會出現「已付款」。
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog modal-lg -->
            </div>
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-qa3-2">
            <div class="modal-vertical-middle">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">有提供分期付款的服務嗎？</h4>
                        </div>
                        <div class="modal-body lt">
								目前暫時沒有提供這項服務喔。
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog modal-lg -->
            </div>
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-qa4-1">
            <div class="modal-vertical-middle">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">Wikidue Store 教育商城選擇發票類型說明</h4>
                        </div>
                        <div class="modal-body lt">
                            <ul class="list-num">
                                <li>個人：
                                    <br>若您選擇「個人」，系統將會開立「二聯式電子發票」，並優先以網站會員帳號做為儲存電子發票載具。電子發票載具是記錄電子發票資料工具，您亦可另選擇手機條碼載具或自然人憑證條碼載具等共通性載具(需先至財政部電子發票平台申請)。
                                    若您選擇「索取發票」，發票將於申請後七個工作天內以平信寄出。
                                </li>
                                <li>捐贈：
                                    <br>若您選擇「捐贈」，將會在單月25號開獎後，由「財政部電子發票整合服務平台」對獎後匯入指定捐贈單位帳戶。</li>
                                <li>企業：若您選擇「企業」，發票將於出貨日後七個工作天內寄出到您所指定的地址。</li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog modal-lg -->
            </div>
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-qa4-2">
            <div class="modal-vertical-middle">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">什麼是電子發票？是否會寄給我？</h4>
                        </div>
                        <div class="modal-body lt">根據財政部訂定的「電子發票實施作業要點」，於本平台消費開立之「二聯式電子發票」將不主動寄送，本平台亦會將發票上傳至「財政部電子發票整合服務平台」供會員查閱。相關資料請參考財政部電子發票整合服務平台。</div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog modal-lg -->
            </div>
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-qa4-3">
            <div class="modal-vertical-middle">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title"> 我怎麼知道我的發票是否中獎？若中獎發票是否會寄給我？</h4>
                        </div>
                        <div class="modal-body lt">本平台每逢單月26日會根據財政部公佈之統一發票中獎號碼進行電腦對獎，若有中獎將以簡訊、e-mail或由專人電話通知，並以掛號方式把中獎發票郵寄給您。若您的電子發票使用通用載具(手機憑證載具、自然人憑證載具)，則將由財政部統一通知中獎人。</div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog modal-lg -->
            </div>
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-qa5-1">
            <div class="modal-vertical-middle">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">忘記帳號密碼怎麼辦？</h4>
                        </div>
                        <div class="modal-body lt">
                            <ul class="list-num">
                                <li>請點選登入頁面中「忘記密碼了嗎？」，輸入您註冊使用的 Email 信箱後確認。</li>
                                <li>系統會將密碼重設信寄至您的信箱，按照信件操作就可以重設您的帳號密碼囉！</li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog modal-lg -->
            </div>
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-qa5-2">
            <div class="modal-vertical-middle">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">沒有收到驗證信怎麼辦？</h4>
                        </div>
                        <div class="modal-body lt">
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog modal-lg -->
            </div>
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="modal-qa5-3">
            <div class="modal-vertical-middle">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">查詢或更正個人資料</h4>
                        </div>
                        <div class="modal-body lt">
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog modal-lg -->
            </div>
        </div>
        <!-- /.modal -->
    </div>