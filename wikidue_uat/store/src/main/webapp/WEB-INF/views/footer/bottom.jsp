<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/plugin/jquery-cookie/jquery.cookie.js"></script>
    <!-- <script src="js/jquery-migrate-1.2.1.min.js"></script>   -->
    <script src="${pageContext.request.contextPath}/plugin/bootstrap/bootstrap.min.js"></script>
    <!--JS plugins-->
    <script src="${pageContext.request.contextPath}/js/base.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>