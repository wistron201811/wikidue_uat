<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <div class="index-main-list">
        <div class="container">
            <h1 class="about-title color-green">Wikidue Store 教育商城</h1>
            <h3 class="about-title">全新STEAM X Maker創客教育服務平台</h3>
            <div class="div-tb table-row">
                <div class="td about-img">
                    <img src="${pageContext.request.contextPath}/img/about/robot.png" alt="">
                </div>
                <div class="td vmid">
                    <p>Wikidue為緯育針對 K12 STEAM教育所創立的品牌，利用創新的程式教具、教案與活動設計，開啟孩子對程式的興趣。自2015年創立至今，營隊活動及示範校遍及全台22縣市。STEAM是現今教育最夯的議題，也是國際的趨勢，因此教育部在十二年國教108課綱中，也將STEAM科技與工程之內涵納入科技領域課程規劃裡。</p>
                    <p>有感於STEAM在臺灣逐漸成長茁壯，但相關資源如課程、教具等，曝光較為零散，使得會接觸STEAM的族群，如創客玩家、教師、家長或是學生等，較難以獲得集中的豐富資源。因此，我們想替使用者打造一個主題明確、資源豐富，還能整合線上（online）到線下（offline）最多元的STEAM
                        X Maker創客Wikidue Store教育服務平台，包含教材教具、數位教學、教育檢測與教育活動。在2018年希望能提供給親、師、生，最專業優質可信賴的內容、活動、產品與服務。</p>
                </div>
            </div>
            <!-- div-tb -->
            <!-- div-tb -->
            <h1 class="about-title color-green mt50">我們的團隊</h1>
            <div class="div-tb table-row">
                <div class="td about-img">
                    <img src="${pageContext.request.contextPath}/img/about/about-03.jpg" alt="">
                </div>
                <div class="td vmid">
                    <ul class="list-dot mb20">
                        <li> 值得信賴與卓越的雲平台及數位教育服務內容 </li>
                        <li>實現學用合一的亞洲雲教育服務領導品牌</li>
                    </ul>
                    <p>緯育股份有限公司(wiedu)，由全球最大的資訊及通訊產品企業集團之一的緯創資通股份有限公司(Wistron Crop.)，投資成立的雲端數位學習服務公司，因應未來社會的學習潮流，並積極開創新的服務領域。期盼利用最先進的科技幫助更多人從不同的管道得到學習機會。</p>
                </div>
            </div>
            <!-- div-tb -->

        </div>
        <!-- container -->
    </div>
    <!-- ndex-main-list -->
    <div class="mix-block ptop">
        <div class="container">
            <h1 class="about-title color-green mb30">產品與服務</h1>
            <!-- <h3 class="about-title mb20">wikidue STEAM教育平台服務項目</h3> -->
            <ul class="row gap10">
                <div class="clearfix mb20 visible-xs"></div>
                <li class="col-sm-4">
                    <a href="javascript:;">
                        <img src="${pageContext.request.contextPath}/img/about/about-p1.jpg">
                        <div class="hover-show for-about">
                            <h3>檢測中心</h3>
                            <p class="des">wikidue提供國小、國中、高中數理學科試題線上檢測，並依SP演算分析作答結果推薦最合適的影片資源與教具教材，讓AI智慧在個人學習過程中發揮最大價值。</p>
                        </div>
                    </a>
                </li>
                <li class="col-sm-4">
                    <a href="javascript:;">
                        <img src="${pageContext.request.contextPath}/img/about/about-p2.jpg">
                        <div class="hover-show for-about">
                            <h3>創客教具</h3>
                            <p class="des">wikidue是全台第一座STEAM maker綜合教育商城，擁有種類最豐富、品項最多元的科普相關教具、教材、玩具或各式3C、手作主題等，提供使用者優質的產品及服務。</p>
                        </div>
                    </a>
                </li>
                <li class="col-sm-4">
                    <a href="javascript:;">
                        <img src="${pageContext.request.contextPath}/img/about/about-p3.jpg">
                        <div class="hover-show for-about">
                            <h3>營隊活動</h3>
                            <p class="des">各式營隊、活動大小訊息一把抓，各主辦單位可自由張貼各營隊或活動訊息，所有訊息都免費張貼！PS. 本平台僅提供活動頁面連結，不負責接受報名及收費 </p>
                        </div>
                    </a>
                </li>
                <li class="col-sm-4">
                    <a href="javascript:;">
                        <img src="${pageContext.request.contextPath}/img/about/about-p4.jpg">
                        <div class="hover-show for-about">
                            <h3>線上課程</h3>
                            <p class="des">wikidue與全台最多的教育廠商合作數位學習線上課程。提供STEAM及科普教育相關的知識及影片</p>
                        </div>
                    </a>
                </li>
                <li class="col-sm-4">
                    <a href="javascript:;">
                        <img src="${pageContext.request.contextPath}/img/about/about-p5.jpg">
                        <div class="hover-show for-about">
                            <h3>分享園地</h3>
                            <p class="des">wikidue提供免費的分享空間，作品、教案、心得等等，創客玩家都可以自由上傳分享。讓大家藉由分享的作品，更容易進入STEAM的創意空間。</p>
                        </div>
                    </a>
                </li>
                <li class="col-sm-4">
                    <a href="javascript:;">
                        <img src="${pageContext.request.contextPath}/img/about/about-p6.jpg">
                        <div class="hover-show for-about">
                            <h3>APCS專區</h3>
                            <p class="des">參考美國大學先修課程，與各大學合作命題，讓高中職學生檢驗成果外，並作為大學選才的參考依據！</p>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- mix-block -->