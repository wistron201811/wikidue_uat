<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
	<!-- angularjs -->
	<script src="${pageContext.request.contextPath}/js/page/camp/homeController.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
	<div ng-controller="HomeController">
		<div class="online-course">
			<div class="container">

				<div class="row gap20">

					<!-- <div class="clearfix visible-xs mb10"></div> -->
					<div class="col-sm-3 col-xs-6">
						<select class="selectpicker" id="city" data-size="7" data-width="100%" title="所在地區">
							<option></option>
							<option value="基隆市">基隆市</option>
							<option value="臺北市">臺北市</option>
							<option value="新北市">新北市</option>
							<option value="宜蘭縣">宜蘭縣</option>
							<option value="新竹市">新竹市</option>
							<option value="新竹縣">新竹縣</option>
							<option value="桃園市">桃園市</option>
							<option value="苗栗縣">苗栗縣</option>
							<option value="臺中市">臺中市</option>
							<option value="彰化縣">彰化縣</option>
							<option value="南投縣">南投縣</option>
							<option value="嘉義市">嘉義市</option>
							<option value="嘉義縣">嘉義縣</option>
							<option value="雲林縣">雲林縣</option>
							<option value="臺南市">臺南市</option>
							<option value="高雄市">高雄市</option>
							<option value="屏東縣">屏東縣</option>
							<option value="臺東縣">臺東縣</option>
							<option value="花蓮縣">花蓮縣</option>
							<option value="金門縣">金門縣</option>
							<option value="連江縣">連江縣</option>
							<option value="澎湖縣">澎湖縣</option>
						</select>
					</div>
					<div class="col-sm-3 col-xs-6">
						<select class="selectpicker" id="order2" data-width="100%" title="活動時間">
							<option></option>
							<option value="PUBLISH_START_DATE_DESC">最新在前</option>
							<option value="PUBLISH_START_DATE_ASC">最舊在前</option>
						</select>
					</div>
					<div class="clearfix visible-xs mb10"></div>
					<div class="col-sm-3 col-xs-6">
						<select class="selectpicker" id="order1" data-width="100%" title="活動價錢">
							<option></option>
							<option value="PRICE_ASC">由低至高</option>
							<option value="PRICE_DESC">由高至低</option>
						</select>
					</div>
					<div class="col-sm-3 col-xs-6">
						<div class="input-group search">
							<input type="text" ng-model="keyword" class="form-control" placeholder="">
							<span class="input-group-btn">
								<a href="javascript:;" ng-click="queryCamp(1)" class="btn btn-warning">
									<i class="icon-main_search"></i>
								</a>
							</span>
						</div>
					</div>
				</div>
				<!-- row -->

			</div>
			<!-- container -->
		</div>
		<div class="index-main-list for-camp">
			<div class="container">
				<div class="row">
					<!-- /////////////////////LEFT//////////////////// -->
					<div class="col-md-4 left-side">
						<div class="left-side-in">
							<!-- MENU -->
							<div class="panel-group sort-panel" data-ng-init="campQueryList()" bs-loading-overlay="list-block">
								<!-- QA-LIST -->
								<div ng-repeat="(category, camps) in campAll" class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#AA" ng-href="{{'#_'+$index}}" aria-expanded="false">{{category}}</a>
										</h4>
									</div>
									<div class="panel-collapse collapse" id="{{'_'+$index}}" aria-expanded="false">
										<div class="panel-body">
											<ul class="side-sub-menu">
												<li ng-repeat="camp in camps">
													<a ng-href="{{camp.externalLink}}" target="_blank">{{camp.campName}}</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<!-- / QA-LIST -->
							</div>
							<!-- panel-group -->
							<!-- END MENU -->

							<div class="mb15 visible-xs visible-sm"></div>
						</div>
					</div>
					<!-- /////////////////////END LEFT//////////////////// -->
					<!-- /////////////////////RIGHT//////////////////// -->
					<div class="col-md-8 right-content line-left">
						<ul class="camp-list" data-ng-init="queryCamp(1)" bs-loading-overlay="camp-block">
							<!-- list -->
							<li ng-repeat="camp in campList">
								<div class="row gap20">
									<div class="col-sm-4 camp-img">
										<a ng-href="{{camp.externalLink}}" target="_blank" ng-click="sendGaEvent(camp.pkCamp, camp.campName)">
											<img ng-src="{{camp.image}}" alt="" class="img-responsive">
										</a>
									</div>
									<div class="clearfix visible-xs mb20"></div>
									<div class="col-sm-4 camp-des">
										<a href="javascript:;" ng-click="setFav(camp)" data-ng-init="checkFav(camp)" class="love tr0" ng-class="{active:camp.isFav}" bs-loading-overlay="fav-block"><i></i></a>
										<h3>{{camp.campName}}</h3>
										<div class="div-tb">
											<div class="td td-nowarp">活動時間 : </div>
											<div class="td">
												<span class="color-red">{{camp.activitySDate}}～
													<br>{{camp.activityEDate}}</span>
											</div>
										</div>
										<div class="div-tb">
											<div class="td td-nowarp">報名時間 : </div>
											<div class="td">
												<span class="color-red">{{camp.signUpStartDate?camp.signUpStartDate:'即日起'}}～
													<br>{{camp.signUpEndDate}}止
												</span>
											</div>
										</div>
										<div class="div-tb">
											<div class="td td-nowarp">招生對象 : </div>
											<div class="td">{{camp.attendantTarget}}</div>
										</div>
										<div class="div-tb">
											<div class="td td-nowarp">主辦單位 : </div>
											<div class="td">{{camp.organizer}}</div>
										</div>
									</div>
									<div class="clearfix visible-xs">
										<hr>
									</div>
									<div class="col-sm-4 camp-price">
										<div class="map">
											<i class="icon-location"></i>{{camp.activityCounty}}</div>
										<s>原價 : {{camp.price | currency : '$' : 0}}</s>
										<p class="price color-orange">優惠價 : {{camp.discountPrice | currency : '$' : 0}}</p>
										<div class="ct">
											<a ng-href="{{camp.externalLink}}" target="_blank" ng-click="sendGaEvent(camp.pkCamp, camp.campName)" class="btn btn-success btn-block">查看詳情</a>
										</div>
									</div>
								</div>
							</li>
							<li ng-if="!campList || campList.length<=0" class="alert alert-warning">
								搜尋結果：沒有資料
							</li>
							<!-- END list -->
						</ul>
					</div>
					<!-- /////////////////////END RIGHT//////////////////// -->
				</div>
				<!-- row -->
			</div>
			<!-- container -->
		</div>
		<e7-pagination page="page" fun="queryCamp"/>
	</div>