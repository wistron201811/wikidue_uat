<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/plugin/jquery-cookie/jquery.cookie.js"></script>
    <!-- <script src="js/jquery-migrate-1.2.1.min.js"></script>   -->
    <script src="${pageContext.request.contextPath}/plugin/bootstrap/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/base.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
    <script src="${pageContext.request.contextPath}/plugin/bootstrap-select/js/bootstrap-select.min.js"></script>
    <script src="${pageContext.request.contextPath}/plugin/uploadPreview/jquery.uploadPreview.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>


    <script>
	 	// 分享園地廣告群組
		categoryCode = "ADIN005";
	 	
        var maxSize = parseInt('<spring:eval expression="@configService.maxSingleUploadSize" />');
        var totalMaxSize = parseInt('<spring:eval expression="@configService.maxUploadSize" />');

        // very simple to use!
        $(document).ready(function () {

            $('.article-slide').carousel({
                interval: false
            });

            setTimeout(function () {
                $("ul.menu li").eq("4").addClass("active");
            }, 300);


            $('.file-size').each(function(idx,val) {
                $(val).attr('file_size',parseInt($(val).html()));
                $(val).html(bytesToSize(parseInt($(val).html())));
            });
        });




        function addFileChange() {
            $('.attachments').change(function (e) {
                $in = $(this);
                var fileSize = "";
                if ($in.val() && $in[0].files) {
                    // 檔案大小
                    fileSize = "(" + bytesToSize($in[0].files.item(0).size) + ")";
                    if ($in[0].files.item(0).size > maxSize) {
                        BootstrapDialog.show({
                            title: '提示訊息',
                            type: BootstrapDialog.TYPE_WARNING,
                            message: '檔案大小不得超過' + bytesToSize(maxSize)
                        });
                        $in.val('');
                        $in.parent().next().html("請選擇上傳檔案");
                        retrun;
                    }
                    $in.parent().next().html("<span style='color:black'>" + $in.val().split('\\').pop() + fileSize + "</span>");
                } else {
                    $in.parent().next().html("請選擇上傳檔案");
                }

                checkShowAddFile();
            });
        }
        addFileChange();

        function addFileSelect() {
            $('#filepool').append(
                '<li>' +
                '<span class="l-close" onclick="$(this).parent().remove();checkShowAddFile();"></span>' +
                '<div class="div-tb">' +
                '<label class="btn btn-default wid_100px">' +
                '    <input name="attachments" type="file" class="hidden attachments">' +
                '    <i class="icon-main_search"></i> 瀏覽' +
                '</label>' +
                '<div class="td" style="vertical-align:middle;width: 100%; padding-left:10px;color:#ccc">' +
                '    請選擇上傳檔案' +
                '</div>' +
                '</div>' +
                '</li>'
            );
            addFileChange();
            $('#addFileSelectBtn').hide();
        }

        function checkShowAddFile() {
            var chk = true;
            $('.attachments').each(function (index) {
                if (!$(this).val()) chk = false;
            });

            if (chk && ($('.attachments').length + $('.orig').length) <= 4)
                $('#addFileSelectBtn').show();
            else
                $('#addFileSelectBtn').hide();
        }

        function checkTotalSize(){
            var oldFileSize = $('.file-size').length>0?$.map($('.file-size'), function(idx, val) { return parseInt($(val).attr('file_size'))}).reduce(function(a,b){return a+b}):0;
            var newFileSize = $('#shareForm [name=attachments]').length>0?$.map($('#shareForm [name=attachments]'), function(val) { return val.files.item(0)?val.files.item(0).size:0;}).reduce(function(a,b){return a+b}):0;
            return (oldFileSize + newFileSize) > totalMaxSize;
        }

        function add() {
            var message = [];
            if (!document.shareForm.title.value) {
                message.push("請輸入標題");
            }
            if (!document.shareForm.author.value) {
                message.push("請輸入作者");
            }
            if (!document.shareForm.image.value) {
                message.push("請選擇一張封面圖");
            }
            if (!document.shareForm.content.value) {
                message.push("請輸入文字描述");
            }
            if (!document.shareForm.tag.value) {
                message.push("請至少選擇一種標籤");
            }
            if (!document.shareForm.category.value) {
                message.push("請至少選擇一種分類");
            }
			if(checkTotalSize()){
				message.push("檔案全部大小不得超過"+bytesToSize(maxSize));
			}
            
            if (message.length) {
                BootstrapDialog.show({
                    title: '提示訊息',
                    type: BootstrapDialog.TYPE_WARNING,
                    message: message.join("、")
                });
            } else {
                $.LoadingOverlay("show");
                setTimeout(function(){
                    document.shareForm.content.value = document.shareForm.content.value.substr(0 , 200);
                    document.shareForm.submit();
                }, 500);
            }
        }

        function save() {
            var message = [];
            if (!document.shareForm.title.value) {
                message.push("請輸入標題");
            }
            if (!document.shareForm.author.value) {
                message.push("請輸入作者");
            }
            if (!document.shareForm.content.value) {
                message.push("請輸入文字描述");
            }
            if (!document.shareForm.tag.value) {
                message.push("請至少選擇一種標籤");
            }
            if (!document.shareForm.category.value) {
                message.push("請至少選擇一種分類");
            }
            if(checkTotalSize()){
				message.push("檔案全部大小不得超過"+bytesToSize(maxSize));
			}
            if (message.length) {
                BootstrapDialog.show({
                    title: '提示訊息',
                    type: BootstrapDialog.TYPE_WARNING,
                    message: message.join("、")
                });
            } else {
                $.LoadingOverlay("show");
                setTimeout(function(){
                    document.shareForm.content.value = document.shareForm.content.value.substr(0 , 200);
                    document.shareForm.submit();
                }, 500);
                
            }
        }

        function remove(fileId, this_) {
            $('#shareForm').append(
                '<input name="delFile" type="hidden" value="' + fileId + '"/>'
            );
            $(this_).parent().remove();
            checkShowAddFile();
        }

        function deleteShare() {
            BootstrapDialog.confirm({
                title: '提示訊息',
                message: '確認要刪除此分享?',
                type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
                closable: true, // <-- Default value is false
                draggable: true, // <-- Default value is false
                btnCancelLabel: '取消', // <-- Default value is 'Cancel',
                btnOKLabel: '確定', // <-- Default value is 'OK',
                btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
                callback: function (result) {
                    if (result) {
                        document.shareForm.submit();
                    } else {

                    }
                }
            });
        }

        //function formatBytes(a,b){if(0==a)return"0 Bytes";var c=1024,d=b||2,e=["Bytes","KB","MB","GB","TB","PB","EB","ZB","YB"],f=Math.floor(Math.log(a)/Math.log(c));return parseFloat((a/Math.pow(c,f)).toFixed(d))+" "+e[f]}
        function bytesToSize(bytes) {
            var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
            if (bytes == 0) return 'n/a';
            var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
            if (i == 0) return bytes + ' ' + sizes[i];
            return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
        };

    </script>