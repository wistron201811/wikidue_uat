<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<script
	src="${pageContext.request.contextPath}/js/page/share/detailController.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
<style type="text/css">
.testcss {
	text-decoration: none;
	color: #000000;
}
</style>
<form action="${pageContext.request.contextPath}/share/delete"
	name="shareForm" method="post">
	<input type="hidden" name="shareId" value="${shareForm.shareId}" />
</form>
<div class="product-detail" ng-controller="DetailController">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<img
					src="${pageContext.request.contextPath}/api/query/image/${shareForm.image.pkFile}"
					alt="" class="img-responsive img-thumbnail">
			</div>
			<div class="col-sm-6" bs-loading-overlay="check-block">
				<div class="btn-group pull-right">
					<div class="btn">
						<a href="javascript:;" ng-click="setFav('${shareForm.shareId}')"
							data-ng-init="checkFav('${shareForm.shareId}')" class="love"
							ng-class="{active:isFav}" bs-loading-overlay="fav-block"
							ng-model="count"> <i></i> <span class="testcss"> <c:choose>
									<c:when test="${empty user_auth.loginId}">
							${shareForm.cnt}
                          </c:when>
									<c:otherwise>
                            {{ count }}
                          </c:otherwise>
								</c:choose>
						</span>
						</a>
					</div>
					<sec:authorize url="/api/share/accusation/check">
						<div class="btn">
							<a href="javascript:;"
								ng-click="checkAccusation(${shareForm.shareId},'${shareForm.title}')"
								class="loudspeaker"><i class="icon-loudspeaker"></i></a>
						</div>
					</sec:authorize>
				</div>
				<h2>${shareForm.title}</h2>
				<p>${shareForm.content}</p>
				<p class="rt color-orange">${shareForm.author}</p>
				<ul class="list-group">
					<c:forEach items="${shareForm.origAttachments}" var="item">
						<li class="list-group-item">${item.fileName}(<span
							class="file-size">${item.fileSize}</span>) <a
							href="${pageContext.request.contextPath}/api/file/download/${item.pkFile}"
							target="_blank" class="download pull-right mt5"> <i
								class="icon-download"></i>
						</a>
						</li>
					</c:forEach>
				</ul>
				<c:if test="${fn:length(shareForm.origAttachments) gt 0}">
					<div class="ct">
						<a
							href="${pageContext.request.contextPath}/share/detail/download/${shareForm.shareId}.zip"
							class="btn btn-default"> <i class="icon-download"></i> 全部下載
						</a>
					</div>
				</c:if>
			</div>
			<div class="col-sm-6">
				<sec:authorize url="/share/edit">
					<c:if test="${shareForm.ownerId eq user_auth.loginId}">
						<a
							href="${pageContext.request.contextPath}/share/edit?id=${shareForm.shareId}"
							class="btn btn-primary btn-sm">修改</a>
					</c:if>
				</sec:authorize>
				<sec:authorize url="/share/delete">
					<c:if test="${shareForm.ownerId eq user_auth.loginId}">
						<a href="javascript:;" onclick="deleteShare()"
							class="btn btn-danger btn-sm">刪除</a>
					</c:if>
				</sec:authorize>
			</div>
		</div>
		<!-- row -->
		<hr>
		<a href="${pageContext.request.contextPath}/share/home"
			class="btn btn-default pull-right">回上一頁 <i class="icon-back"></i></a>
	</div>
	<!-- container -->
</div>
<!-- product-detail -->