<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
    <!-- angularjs -->
    <script src="${pageContext.request.contextPath}/js/page/share/homeController.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
    <div ng-controller="ShareController">

        <div class="online-course">
            <div class="container">
                <div class="row gap20">
                    <div class="col-md-1 hidden-sm hidden-xs"></div>
                    <div class="col-sm-7">
                        <div class="input-group search">
                            <input type="text" ng-model="keyword" class="form-control" placeholder="輸入您要查詢的關鍵字">
                            <span class="input-group-btn">
                                <a href="javascript:;" ng-click="queryShareCenter(1)" class="btn btn-warning">
                                    <i class="icon-main_search"></i>
                                </a>
                            </span>
                        </div>
                    </div>
                    <div class="clearfix visible-xs mb10"></div>
                    <div class="col-sm-3">
                        <a href="${pageContext.request.contextPath}/share/add" class="btn btn-primary btn-block">
                            <i class=" icon-main_export"></i> 上傳內容</a>
                    </div>
                    <div class="col-md-1 hidden-sm hidden-xs"></div>
                    <div class="clearfix mb20"></div>
                    <div class="col-md-10 col-md-push-1 pos-re" bs-loading-overlay="tag-block">
                        <div class="div-tb filter-selected">
                            <div class="td vtop nowrap wid_100px">您已選擇：</div>
                            <div class="td">
                                <ul class="all-select">
                                    <li ng-repeat="sel in selTagSet">{{sel.name}}
                                        <i ng-click="removeTag(sel.tag)"></i>
                                    </li>
                                </ul>
                            </div>
                            <div class="td vtop ct ">
                                <a href="javascript:;" ng-click="clearAllTag()" class="btn btn-default">清除條件</a>
                            </div>
                        </div>
                        <div class="clearfix mb10"></div>
                        <!-- PC版選取 -->
                        <table class="table mb0 hidden-xs select-table">
                            <tr>
                                <td class="bg-warning ct round-top-left nowrap">年級</td>
                                <td class="wid_20px hidden-sm"></td>
                                <td>
                                    <a ng-repeat="g in grades" class="btn btn-select-o" ng-click="setGrade(g)" ng-class="{active: grade.name == g.name}">{{g.name}}</a>
                                </td>
                            </tr>
                            <tr ng-if="stage > 0">
                                <td class="bg-warning ct">科目</td>
                                <td class="wid_20px hidden-sm"></td>
                                <td>
                                    <div show-more block="subjects">
                                        <a ng-repeat="s in subjects" class="btn btn-select-o" ng-click="setSubject(s)" ng-class="{active: s.tag == subject.tag}">{{s|addGrade}}</a>
                                    </div>
                                    <!-- one-line-collapse" -->
                                </td>
                            </tr>
                            <tr ng-if="stage > 1">
                                <td class="bg-warning ct">章節</td>
                                <td class="wid_20px hidden-sm"></td>
                                <td>
                                    <div show-more block="chapters">
                                        <a ng-repeat="c in chapters" class="btn btn-select-o" ng-click="setChapter(c)" ng-class="{active: c.tag == chapter.tag}">{{c.name}}</a>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="bg-success ct">分類</td>
                                <td class="wid_20px hidden-sm"></td>
                                <td>
                                    <div show-more block="categorys">
                                        <a ng-repeat="cg in categorys" class="btn btn-select-g" ng-click="setCategory(cg)" ng-class="{active: cg.categoryId == category.categoryId}">{{cg.categoryName}}</a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <!-- / PC版選取 -->
                        <!-- 手機版選取 -->
                        <div class="btn-group btn-group-justified select-toggle visible-xs">
                            <a href="#select-1" class="btn btn-warning has-arrow collapsed" data-toggle="collapse">年級
                                <i class="icon-expand"></i>
                            </a>
                            <a href="#select-2" ng-if="stage > 0" class="btn btn-warning has-arrow collapsed" data-toggle="collapse">科目
                                <i class="icon-expand"></i>
                            </a>
                            <a href="#select-3" ng-if="stage > 1" class="btn btn-warning has-arrow collapsed" data-toggle="collapse">章節
                                <i class="icon-expand"></i>
                            </a>
                            <a href="#select-4" class="btn btn-success has-arrow collapsed" data-toggle="collapse">分類
                                <i class="icon-expand"></i>
                            </a>
                        </div>
                        <!-- / 手機版選取 -->
                        <div id="select-1" class="collapse">
                            <ul class="select-list">
                                <li ng-repeat="g in grades">
                                    <a class="btn btn-select-o" ng-click="setGrade(g)" ng-class="{active: grade.name == g.name}">{{g.name}}</a>
                                </li>
                            </ul>
                        </div>
                        <div id="select-2" class="collapse">
                            <ul class="select-list">
                                <li ng-repeat="s in subjects">
                                    <a class="btn btn-select-o" ng-click="setSubject(s)" ng-class="{active: s.tag == subject.tag}">{{s|addGrade}}</a>
                                </li>
                            </ul>
                        </div>
                        <div id="select-3" class="collapse">
                            <ul class="select-list">
                                <li ng-repeat="c in chapters">
                                    <a class="btn btn-select-o" ng-click="setChapter(c)" ng-class="{active: c.tag == chapter.tag}">{{c.name}}</a>
                                </li>
                            </ul>
                        </div>
                        <div id="select-4" class="collapse">
                            <ul class="select-list">
                                <li ng-repeat="cg in categorys">
                                    <a class="btn btn-select-g" ng-click="setCategory(cg)" ng-class="{active: cg.categoryId == category.categoryId}">{{cg.categoryName}}</a>
                                </li>
                            </ul>
                        </div>
                        <div class="ct mt20">
                            <a href="javascript:;" ng-click="addSelTag()" class="btn btn-warning">確認選取</a>
                        </div>
                    </div>
                </div>
                <!-- row -->

            </div>
            <!-- container -->
        </div>
        <!-- online-course -->
        <div class="index-main-list" bs-loading-overlay="share-block">
            <div class="container">
                <div ng-if="!shareList || shareList.length<=0" class="alert alert-warning">
                    搜尋結果：沒有資料
                </div>
                <div class="row">
                    <!-- list -->
                    <div class="col-sm-4" ng-repeat="share in shareList">
                        <img src="${pageContext.request.contextPath}/img/inpage/theme_empty2.png" class="img-hover user-upload" ng-click="goPage('${pageContext.request.contextPath}/share/detail/'+share.shareId)"
                            style="background: url({{share.image}}) center no-repeat;background-size: cover;">
                        <ul class="pic-list mt0">
                            <li>
                                <div href="javascript:;" class="row">
                                    <div class="col-xs-12">
                                        <p class="color-gray cur-p" ng-click="goPage('${pageContext.request.contextPath}/share/detail/'+share.shareId)">{{share.author}}</p>
                                        <h4 class="title" style="overflow:hidden;white-space: nowrap;text-overflow: ellipsis" ng-click="goPage('${pageContext.request.contextPath}/share/detail/'+share.shareId)">{{share.title}}</h4>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!-- /list -->
                </div>
                <!-- row -->
            </div>
        </div>
        <e7-pagination page="page" fun="queryShareCenter"/>
    </div>