<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
      <%@ taglib prefix="e7" uri="/WEB-INF/tld/e7.tld"%>
	  <%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
        <!-- angularjs -->
        <script src="${pageContext.request.contextPath}/js/page/share/addController.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>

        <div ng-controller="ShareController">
          <div class="product-detail">
            <div class="container">
              <form:form modelAttribute="shareForm" name="shareForm" action="${pageContext.request.contextPath}/share/add" enctype="multipart/form-data">
                <form:hidden path="tag" />
                <form:hidden path="category" />
                <div class="row">
                  <div class="col-sm-6">
                    <span class="color-red">*建議尺寸 700x430</span>
                    <!-- <div class="img-responsive img-thumbnail"> -->
                    <e7:previewImage attribute="image" height="430px" width="700px" previewHeight="341px" previewWidth="100%" fileStorage="${shareForm.image}"/>
                    <!-- </div> -->
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <form:input path="title" class="form-control" placeholder="請輸入標題" maxlength="50"/>
                      <form:errors path="title" style="color:red"></form:errors>
                    </div>
                    <div class="form-group">
                      <form:textarea path="content" class="form-control" rows="5" placeholder="請輸入文字描述" maxlength="1000" />
                      <form:errors path="content" style="color:red"></form:errors>
                    </div>
                    <div class="form-group">
                      <form:input path="author" class="form-control" placeholder="請輸入作者" />
                      <form:errors path="author" style="color:red"></form:errors>
                    </div>
                    <div class="form-group">
                      <!-- <label class="btn btn-default wid_100px">
                        <input type="file" class="hidden">
                        <i class="icon-main_search"></i> 瀏覽
                      </label>
                      <a href="javascript:;" class="btn btn-primary wid_100px">
                        <i class="icon-main_export"></i> 上傳</a> -->
                      <ul id="filepool" class="order-list">
                        <!-- list -->
                        <li>
                          <span class="l-close" onclick="$(this).parent().remove();checkShowAddFile()"></span>
                          <div class="div-tb">
                            <label class="btn btn-default wid_100px">
                              <input name="attachments" type="file" class="hidden attachments">
                              <i class="icon-main_search"></i> 瀏覽
                            </label>
                            <div class="td" style="vertical-align:middle;width: 100%; padding-left:10px;color:#ccc">
                              請選擇上傳檔案
                            </div>
                          </div>
                        </li>
                        <!-- /list -->
                      </ul>
                    </div>
                    <div id="addFileSelectBtn" class="btn-group pull-right" style="display:none;">
                      <a href="javascript:;" onclick="addFileSelect()" class="btn btn-primary wid_100px">
                        新增一筆</a>
                    </div>

                  </div>
                </div>
              </form:form>
              <!-- row -->
              <div class="clearfix mb20"></div>

              <!-- row -->
            </div>
            <!-- container -->
            <div class="online-course only-top">
              <div class="container">

                <div class="row gap20">
                  <div class="col-md-10 col-md-push-1 pos-re">
                    <h4>請選擇</h4>
                  </div>
                  <div class="clearfix mb10"></div>
                  <div class="col-md-10 col-md-push-1 pos-re" bs-loading-overlay="tag-block">
                    <div class="div-tb filter-selected">
                      <div class="td vtop nowrap wid_100px">您已選擇：</div>
                      <div class="td">
                        <ul class="all-select">
                          <li ng-repeat="sel in selTagSet">{{sel.name}}
                            <i ng-click="removeTag(sel.tag)"></i>
                          </li>
                        </ul>
                      </div>
                      <div class="td vtop ct">
                        <a href="javascript:;" ng-click="clearAllTag()" class="btn btn-default">清除條件</a>
                      </div>
                    </div>
                    <div class="clearfix mb10"></div>
                    <!-- PC版選取 -->
                    <table class="table mb0 hidden-xs select-table">
                      <tr>
                        <td class="bg-warning ct round-top-left nowrap">年級</td>
                        <td class="wid_20px hidden-sm"></td>
                        <td>
                          <div show-more block="grades">
                            <a ng-repeat="g in grades" class="btn btn-select-o" ng-click="setGrade(g)" ng-class="{active: grade.name == g.name}">{{g.name}}</a>
                          </div>
                        </td>
                      </tr>
                      <tr ng-if="stage > 0">
                        <td class="bg-warning ct">科目</td>
                        <td class="wid_20px hidden-sm"></td>
                        <td>
                          <div show-more block="subjects">
                            <a ng-repeat="s in subjects" class="btn btn-select-o" ng-click="setSubject(s)" ng-class="{active: s.tag == subject.tag}">{{s.name}}</a>
                          </div>
                          <!-- one-line-collapse" -->
                        </td>
                      </tr>
                      <tr ng-if="stage > 1">
                        <td class="bg-warning ct">章節</td>
                        <td class="wid_20px hidden-sm"></td>
                        <td>
                          <div show-more block="chapters">
                            <a ng-repeat="c in chapters" class="btn btn-select-o" ng-click="setChapter(c)" ng-class="{active: c.tag == chapter.tag}">{{c.name}}</a>
                          </div>
                        </td>
                        <!-- <td class="vtop rt wid_60px">
                          <a href="javascript:;" class="btn btn-default btn-sm more-btn">更多
                            <i class="icon-expand"></i>
                          </a>
                        </td> -->
                      </tr>
                    </table>
                    <!-- / PC版選取 -->
                    <!-- 手機版選取 -->
                    <div class="btn-group btn-group-justified select-toggle visible-xs">
                      <a href="#select-1" class="btn btn-warning has-arrow collapsed" data-toggle="collapse">年級
                        <i class="icon-expand"></i>
                      </a>
                      <a ng-if="stage > 0" href="#select-2" class="btn btn-warning has-arrow collapsed" data-toggle="collapse">科目
                        <i class="icon-expand"></i>
                      </a>
                      <a ng-if="stage > 1" href="#select-3" class="btn btn-warning has-arrow collapsed" data-toggle="collapse">章節
                        <i class="icon-expand"></i>
                      </a>
                    </div>
                    <!-- / 手機版選取 -->
                    <div id="select-1" class="collapse">
                      <ul class="select-list">
                        <li ng-repeat="g in grades">
                          <a class="btn btn-select-o" ng-click="setGrade(g)" ng-class="{active: grade.name == g.name}">{{g.name}}</a>
                        </li>
                      </ul>
                    </div>
                    <div id="select-2" class="collapse">
                      <ul class="select-list">
                        <li ng-repeat="s in subjects">
                          <a class="btn btn-select-o" ng-click="setSubject(s)" ng-class="{active: s.tag == subject.tag}">{{s.name}}</a>
                        </li>
                      </ul>
                    </div>
                    <div id="select-3" class="collapse">
                      <ul class="select-list">
                        <li ng-repeat="c in chapters">
                          <a class="btn btn-select-o" ng-click="setChapter(c)" ng-class="{active: c.tag == chapter.tag}">{{c.name}}</a>
                        </li>
                      </ul>
                    </div>
                    <div class="ct mt20">
                      <a href="javascript:;" ng-click="addSelTag()" class="btn btn-warning">確認選取</a>
                    </div>
                    <hr>
                    <!--PC版選取 -->
                    <table class="table mb0 hidden-xs select-table">
                      <tr>
                        <td class="wid_20px bg-success ct nowrap">分類
                          <small>(可複選)</small>
                        </td>
                        <td class="wid_20px hidden-sm"></td>
                        <td>
                          <div show-more block="categorys">
                            <a ng-repeat="cg in categorys" class="btn btn-select-g" ng-click="syncCategory(cg)" ng-class="{active: cg.selected == true}">{{cg.categoryName}}</a>
                          </div>
                        </td>
                      </tr>
                    </table>
                    <!-- / PC版選取 -->
                    <!-- 手機版選取 -->
                    <div class="btn-group btn-group-justified select-toggle visible-xs">
                      <a href="#select-4" class="btn btn-success has-arrow collapsed" data-toggle="collapse">分類
                        <small>(可複選)</small>
                        <i class="icon-expand"></i>
                      </a>
                    </div>
                    <div id="select-4" class="collapse">
                      <ul class="select-list">
                        <li ng-repeat="cg in categorys">
                          <a ng-click="syncCategory(cg)" ng-class="{active: cg.selected == true}" class="btn btn-select-g">{{cg.categoryName}}</a>
                        </li>
                      </ul>
                    </div>
                    <!-- / 手機版選取 -->
                    <hr>
                    <!-- <div class="div-tb filter-selected">
                      <div class="td vtop nowrap wid_100px">分類：</div>
                      <div class="td">
                        <label ng-repeat="cg in categorys">
                          <input type="checkbox" ng-click="syncCategory()" ng-model="cg.selected" ng-true-value="true" ng-false-value="false"> {{cg.categoryName}}&emsp;&emsp;
                        </label>
                      </div>
                    </div> -->
                    <div class="ct mt20">
                      <a href="javascript:;" onclick="add()" class="btn btn-warning">確認儲存變更</a>
                    </div>
                  </div>
                </div>
                <!-- row -->
              </div>

              <!-- container -->

            </div>
          </div>
          <!-- product-detail -->
        </div>