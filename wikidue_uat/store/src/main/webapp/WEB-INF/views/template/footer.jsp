<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- footer -->
<footer>
  <div class="footer">
    <div class="container">
      <div class="row gap20">
          <div class="col-lg-6 col-md-8 col-lg-push-3 col-md-push-2">
              <ul class="foot-icon">
              <!-- list -->
                  <li><a href="${pageContext.request.contextPath}/about">
                      <i class="about"></i>
                  </a>
                      <p>關於我們</p>
                  </li>
              <!-- END list -->
              <!-- list -->
                  <li><a href="https://www.facebook.com/WikidueStore/" target="_blank">
                      <i class="fb"></i>
                  </a>
                      <p>粉絲團</p>
                  </li>
              <!-- END list -->
              <!-- list -->
                  <li><a href="https://line.me/R/ti/p/ABqfWNzTo2" target="_blank">
                      <i class="line"></i>
                  </a>
                      <p>LINE</p>
                  </li>
              <!-- END list -->
              <!-- list -->
                  <li><a href="${pageContext.request.contextPath}/faq">
                      <i class="faq"></i>
                  </a>
                      <p>FAQ</p>
                  </li>
              <!-- END list -->
              <!-- list -->
                  <li><a href="mailto:store_service@wikidue.com">
                      <i class="contact"></i>
                  </a>
                      <p>聯絡我們</p>
                  </li>
              <!-- END list -->
              <!-- list -->
                <!--   <li><a href="javascript:;">
                      <i class="school"></i>
                  </a>
                      <p>學校方案</p>
                  </li> -->
              <!-- END list -->
              </ul>
          </div>
      </div>
    </div>
  </div>
</footer>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-120804013-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-120804013-1');
</script>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-5059599021937798",
    enable_page_level_ads: true
  });
</script>
<!-- /footer -->