<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <script src="${pageContext.request.contextPath}/plugin/jquery-cookie/jquery.cookie.js"></script>
    <!-- <script src="js/jquery-migrate-1.2.1.min.js"></script>   -->
    <script src="${pageContext.request.contextPath}/plugin/bootstrap/bootstrap.min.js"></script>
    <!--JS plugins-->
    <script src="${pageContext.request.contextPath}/js/base.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
    <!--Javascript (jQuery) Libraries and Plugins-->
    <script src="${pageContext.request.contextPath}/plugin/owl-carousel/owl.carousel.js"></script>
    <script>
    	// 首頁廣告群組
    	categoryCode = "ADIN001";
    	
        // very simple to use!
        $(document).ready(function () {
            $(".owl-index").owlCarousel({
            	    autoplay:true,
            	    autoplayTimeout:${bannerAutoPlaySpeed},
                //autoplayHoverPause:true,
                items: 1,
                margin: 0,
                center: false,
                startPosition: 0,
                loop: true,
                navSpeed: 1000,
                nav: true,
                navText: ["<i class='icon-expand_left'></i>", "<i class='icon-expand_right'></i>"],
                paginationSpeed: 400,
                dots: true,
                // responsive: {
                //       300: {
                //           items: 2
                //       },
                //       600: {
                //           items: 3
                //       }
                //   }
            });
        });

        $(".video-holder").hide();
        $('.play-video').on('click', function () {
          $(this).hide();
        $(".video-holder").show();
            $(".video")[0].src += "1";
        });
    </script>