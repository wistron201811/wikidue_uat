<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!doctype html>
<html>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<head>
    <meta charset="utf-8">
    <title>Wikidue Store 教育商城 - <tiles:getAsString name="pageTitle"/></title>
    <!--SEO Meta Tags-->
    <!--<meta name="description" content="" />
    <meta name="keywords" content="S-moda" />-->
    <meta name="description" content="全台首創最大STEAMxMaker教育商城。整合科普教育與程式設計理念，提供創客教具、線上課程、教育檢測、營隊活動等服務，提供親、師、生最專業、優質可信賴的一站到位服務。" />
    <meta name="keywords" content="程式, 程式教育, Maker, Coder, STEN, STEAM, 創客, 創客教育, 手作, 自走車, 無人機, 空拍機, 飛行器, 機器人, Arduino, APCS, Wikidue, 科普, K12, 科學, 科學教育, 教育" />
    <meta property="og:title" content="Wikidue Store 教育商城" />
    <meta property="og:description" content="全台首創最大STEAMxMaker教育商城。整合科普教育與程式設計理念，提供創客教具、線上課程、教育檢測、營隊活動等服務，提供親、師、生最專業、優質可信賴的一站到位服務。" />
    <meta property="og:image" content="https://store.wikidue.com/store/img/wikidue-circle_02.png" />
    <meta name="author" content="8Guild" />
    <meta name="wikidue version" content="<spring:eval expression="@configService.applicationVersion" />">
    <!--Mobile Specific Meta Tag-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!--Favicon-->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/favico.png" type="image/png">
    <link rel="icon" href="${pageContext.request.contextPath}/S-moda.ico">
    <!-- fb app id -->
    <meta property='fb:app_id' content='<spring:eval expression="@configService.fbAppId" />'/>
    <!--Master Slider Styles-->
    <!-- menu -->
    <link href="${pageContext.request.contextPath}/plugin/bootstrap/bootstrap.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/icomoon/style.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/plugin/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/header.css?v=<spring:eval expression="@configService.applicationVersion" />" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/owl-style.css" rel="stylesheet">
    <!-- <link href="${pageContext.request.contextPath}/plugin/owl-carousel/owl.theme.css" rel="stylesheet"> -->
    <link href="${pageContext.request.contextPath}/plugin/bootstrap-select/css/bootstrap-select.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/layout.css?v=<spring:eval expression="@configService.applicationVersion" />" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/responsive.css" rel="stylesheet">
    
    <link href="${pageContext.request.contextPath}/css/e7learning.css?v=<spring:eval expression="@configService.applicationVersion" />" rel="stylesheet">
    <!-- HTML5 shiv & respond.js for IE6-8 support of HTML5 elements & media queries -->
    <!--[if lt IE 9]>
      <script src="plugins/html5shiv.js"></script>
      <script src="plugins/respond.min.js"></script>
    <![endif]-->
    <!-- menu -->
    <!--Modernizr-->
    <!--Adding Media Queries Support for IE8-->

    <!-- angularjs -->
    <script src="${pageContext.request.contextPath}/js/angular.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/app.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
    <script src="${pageContext.request.contextPath}/js/lib/angular-loading-overlay/angular-loading-overlay.js"></script>
    <style type="text/css">
	    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
		  display: none !important;
		}
    </style>
</head>
<!--Body-->

<body ng-cloak class="ng-cloak" ng-app="myApp">
	<script type="text/javascript">
		// 初始化廣告參數（使用者角色）
		var roleCode = "${user_auth.apiGrade}";
		// 廣告群組
		var categoryCode = "";
	</script>
	<!-- 載入api config -->
	<jsp:include page="/WEB-INF/views/common/apiConfig.jsp"/>
    <div id="load-header">
        <tiles:insertAttribute name="header" /> 
    </div>
    <div class="clearfix"></div>
    <tiles:insertAttribute name="content" />
    <div id="footer">
        <tiles:insertAttribute name="footer" />
    </div>
    <tiles:insertAttribute name="bottom" />
    <tiles:insertAttribute name="ad" />
    
    <script type="text/javascript">

	    // very simple to use!
	    $(document).ready(function() {
			viewport();
		});
		$(window).resize(function(){
			viewport();
	    });
    </script>
    <script src="${pageContext.request.contextPath}/plugin/jquery-loading-overlay-2.1.3/loadingoverlay.min.js"></script>
	<script src="${pageContext.request.contextPath}/plugin/bootstrap3-dialog/js/bootstrap-dialog.min.js"></script>
	<link href="${pageContext.request.contextPath}/plugin/bootstrap3-dialog/css/bootstrap-dialog.min.css" rel="stylesheet">
    <c:if test="${not empty message}">
        <script type="text/javascript">
            setTimeout(function(){ 
                BootstrapDialog.show({
                    title: '提示訊息',
                    message: '${message}'
                });
            }, 50);
			<c:remove var="message" scope="session" />
        </script>
	</c:if>
	<c:if test="${not empty error_message}">
        <script type="text/javascript">
            setTimeout(function(){ 
                BootstrapDialog.show({
                    title: '提示訊息',
                    type: BootstrapDialog.TYPE_WARNING,
                    message: '${error_message}'
                });
            }, 50);
			<c:remove var="error_message" scope="session" />
		</script>
	</c:if>
</body>

</html>