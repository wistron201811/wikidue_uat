<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
  <%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
    <!-- angularjs -->
    <script src="${pageContext.request.contextPath}/js/page/ad/adController.js?v=<spring:eval expression=" @configService.applicationVersion
      " />"></script>
    <div class="side-ad" ng-controller="AdController" ng-show="adList.length">
      <a href="javascript:;" class="open-toggle">
        <i class="icon-expand_left"></i> 展開廣告</a>
      <a href="javascript:;" class="close-toggle">
        <i class="icon-close"></i> 關閉廣告</a>
      <ul class="ad-list" ng-repeat="ad in adList" bs-loading-overlay="ad-block">
        <li>
          <a target="_blank" ng-href="{{ad.externalLink}}">
            <img ng-src="{{getRandomImageUrl(ad.image1,ad.image2)}}">
          </a>
        </li>
      </ul>
    </div>

    <script type="text/javascript">
      // very simple to use!
      $(document).ready(function () {
        $(".close-toggle").click(function () {
          $(".side-ad").addClass("active");
          $.cookie('wikidue_ad_close', true, {path:'/'});
        })
        $(".open-toggle").click(function () {
          $(".side-ad").removeClass("active");
          $.cookie('wikidue_ad_close', false, {path:'/'});
        }) 
        if($.cookie('wikidue_ad_close')=="true"){
          $(".side-ad").addClass("active");
        }
      });
    </script>