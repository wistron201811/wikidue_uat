<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
            <%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
                <script>
                    (function () {
                        var ie = !!(window.attachEvent && !window.opera), wk = /webkit\/(\d+)/i.test(navigator.userAgent) && (RegExp.$1 < 525);
                        var fn = [], run = function () { for (var i = 0; i < fn.length; i++)fn[i](); }, d = document; d.ready = function (f) {
                            if (!ie && !wk && d.addEventListener) { return d.addEventListener('DOMContentLoaded', f, false); } if (fn.push(f) > 1) return;
                            if (ie) (function () { try { d.documentElement.doScroll('left'); run(); } catch (err) { setTimeout(arguments.callee, 0); } })();
                            else if (wk) var t = setInterval(function () { if (/^(loaded|complete)$/.test(d.readyState)) clearInterval(t), run(); }, 0);
                        };
                    })();
                </script>
                <!-- angularjs -->
                <script src="${pageContext.request.contextPath}/js/page/header/headerController.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
                <div class="header" ng-controller="HeaderController">
                    <div class="container">
                        <div class="row gap10">
                            <div class="col-md-4  header-left">
                                <a href="${pageContext.request.contextPath}/index" class="logo">
                                    <img src="${pageContext.request.contextPath}/img/logo.png" alt="" class="img-logo">
                                    <%-- <img src="${pageContext.request.contextPath}/img/logo-text.png" alt="" class="hidden-xs hidden-sm img-logo-text"> --%>
                                </a>
                                <a href=".menu" class="m-menu-toggle  collapsed" data-toggle="collapse">
                                    <i class="icon-reorder"></i>
                                </a>
                            </div>
                            <sec:authorize access="authenticated" var="authenticated" />
                            <!-- /col-md-4 -->
                            <div class="col-md-8 header-right">
                                <ul class="menu">
                                    <li>
                                        <a href="<spring:eval expression=" @configService.examCenterUrl " />">檢測中心</a>
                                    </li>
                                    <li>
                                        <a href="${pageContext.request.contextPath}/course/home">線上課程</a>
                                    </li>
                                    <li>
                                        <a href="${pageContext.request.contextPath}/props/home">創客教具</a>
                                    </li>
                                    <li>
                                        <a href="${pageContext.request.contextPath}/camp/home">營隊活動</a>
                                    </li>
                                    <li>
                                        <a href="${pageContext.request.contextPath}/recommend/home">分享園地</a><!-- share/home -->
                                    </li>
                                    <c:if test="${authenticated}">
                                    <li class="member visible-xs">
                                        <div class="title">會員:<span>${user_auth.name}</span></div>
                                        <ul>
                                            <li><a href="${pageContext.request.contextPath}/learningCenter">學習中心</a></li>
                                            <li><a href="${pageContext.request.contextPath}/order/myOrder">我的訂單</a></li>
                                            <li><a href="<spring:eval expression="@configService.examRecordUrl"/>">測驗紀錄</a></li>
                                            <li><a target="_blank" href="<spring:eval expression="@configService.memberCenterUrl"/>">帳號設定</a></li>
                                        </ul>
                                    </li>
                                    <li class="logout visible-xs"><a href="javascript:;" onclick="document.logoutForm.submit();" class="btn btn-warning">登出</a></li>
                                    </c:if>
                                </ul>
                                <ul class="cart">
                                    <c:if test="${!authenticated}">
                                        <li>
                                            <a href="javascript:;" onclick="document.signUpForm.submit();">
                                                <i class="icon-user"></i>
                                                <span class="hidden-xs">註冊</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" onclick="document.loginForm.submit();">
                                                <i class="icon-login"></i>
                                                <span class="hidden-xs">登入</span>
                                            </a>
                                        </li>
                                        <form name='loginForm' action="${pageContext.request.contextPath}/login" method="post">
                                            <input type="hidden" name="redirectUri" value="">
                                        </form>
                                        <form name='signUpForm' action="${pageContext.request.contextPath}/signUp" method="post">
                                            <input type="hidden" name="redirectUri" value="">
                                        </form>
                                        <script>var isLogin = false; document.loginForm.redirectUri.value = window.location.href; document.signUpForm.redirectUri.value = window.location.href</script>
                                    </c:if>
                                    <c:if test="${authenticated}">
                                        <script>
                                            var isLogin = true;
                                        </script>
                                        <!-- list -->
                                        <li>
                                            <div class="dropdown" data-ng-init="queryCart();queryMessage();">
                                                <a href="${pageContext.request.contextPath}/cart/checkout" class="mobile-link visible-xs"></a>
                                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-shopping-cart" style="margin-right: -6px;">
                                                        <span class="badge" style="right:5px">{{cartList.length}}</span>
                                                    </i>
                                                    <span class="caret hidden-xs"></span>
                                                    <span class="hidden-xs">購物車 </span>
                                                </a>
                                                <div class="dropdown-menu pull-right float-bar-drop hidden-xs" bs-loading-overlay="cart-block">
                                                    <!-- list -->
                                                    <div class="bar-drop-list" ng-repeat="cart in cartList">
                                                        <img style="cursor: pointer;" ng-click="goPage('${pageContext.request.contextPath}/'+(cart.type == 'PROPS' ? 'props':'course')+'/detail/'+cart.fkProduct)"
                                                            ng-src="{{cart.productImage}}" alt="">
                                                        <span style="cursor: pointer;overflow:hidden;white-space: nowrap;text-overflow: ellipsis;" ng-click="goPage('${pageContext.request.contextPath}/'+(cart.type == 'PROPS' ? 'props':'course')+'/detail/'+cart.fkProduct)"
                                                            class="title" data-toggle="tooltip" title="{{cart.productName}}" data-placement="bottom">{{cart.productName}}</span>
                                                        <a href="javascript:;" class="i-remove" ng-click="removeCart(cart)">
                                                            <i class="icon-main_delete"></i>
                                                        </a>
                                                        <span class="price">{{(cart.unitPrice*cart.quantity)| currency : '$' : 0}}元</span>
                                                    </div>
                                                    <!--/ list -->
                                                    <div class="settlement">
                                                        <p class="left-col">總計
                                                            <span class="color-orange">{{cartList.length}}</span> 件商品</p>
                                                        <p class="right-col">合計 :
                                                            <span class="price">{{getTotalAmount() | currency : '$' : 0}}元</span>
                                                        </p>
                                                        <div class="clearfix">
                                                            <hr class="hr-s">
                                                        </div>
                                                        <a href="${pageContext.request.contextPath}/cart/checkout" class="btn btn-warning btn-block">結帳</a>
                                                    </div>
                                                </div>
                                                <!-- dropdown-menu -->
                                            </div>
                                        </li>
                                        <!-- / list -->
                                        <!-- list -->
                                        <li>
                                            <div class="dropdown">
                                                <a href="${pageContext.request.contextPath}/message/list" class="mobile-link visible-xs"></a>
                                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-bell" style="margin-right: -6px;">
                                                        <span class="badge" style="right:5px">{{badge}}</span>
                                                    </i>
                                                    <span class="caret hidden-xs"></span> 
                                                    <span class="hidden-xs">訊息 </span>
                                                </a>
                                                <ul class="dropdown-menu pull-right member hidden-xs" bs-loading-overlay="msg-block">
                                                    <div class="title">訊息中心</div>
                                                    <div class="infomation">
                                                        <span class="color-gray" ng-if="!messageList || messageList.length==0">您目前沒有訊息</span>
                                                        <ul class="infomation-list">
                                                            <li ng-class="{active:msg.read}" ng-repeat="msg in messageList">
                                                                <a href="${pageContext.request.contextPath}/message/detail/{{msg.messageId}}">{{msg.title|cut:false:25:' ...'}}</a>
                                                                <!-- <span class="pull-right">{{msg.createDt}}</span> -->
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="login">
                                                        <a href="${pageContext.request.contextPath}/message/list" class="btn btn-success btn-sm">顯示全部</a>
                                                    </div>
                                                </ul>
                                            </div>
                                        </li>
                                        <!-- / list -->
                                        <!-- list -->
                                        <li class="hidden-xs">
                                            <div class="dropdown">
                                                <form name="logoutForm" action="${sso_logout_page}">
                                                    <input type="hidden" name="series" value="${user_auth.series}" />
                                                    <input type="hidden" name="redirect_uri" value="${logout_redirect_uri}" />
                                                </form>
                                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                                    <img src="${pageContext.request.contextPath}/img/icon/avatar-man.png" alt="" class="avatar">
                                                    <span class="caret"></span> 會員
                                                </a>
                                                <ul class="dropdown-menu pull-right member">
                                                    <div class="title">${user_auth.name}</div>
                                                    <li>
                                                        <a href="${pageContext.request.contextPath}/learningCenter">學習中心</a>
                                                    </li>
                                                    <li>
                                                        <a href="${pageContext.request.contextPath}/order/myOrder">我的訂單</a>
                                                    </li>
                                                    <li>
                                                        <a href="<spring:eval expression=" @configService.examRecordUrl " />">測驗紀錄</a>
                                                    </li>
                                                    <li>
                                                        <a target="_blank" href="<spring:eval expression="@configService.memberCenterUrl" />">帳號設定</a>
                                                    </li>
                                                    <div class="login">
                                                        <a href="javascript:;" onclick="document.logoutForm.submit();" class="btn btn-warning btn-sm">登出</a>
                                                    </div>
                                                </ul>


                                            </div>
                                            <!-- dropdown -->

                                        </li>
                                        <!-- / list -->
                                    </c:if>
                                </ul>
                            </div>
                            <!-- /col-md-8 -->
                        </div>
                        <!-- row -->


                    </div>
                    <!-- container -->
                </div>
                <!-- header -->