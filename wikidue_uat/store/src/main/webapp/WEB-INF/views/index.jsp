<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
  <%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
      <!-- 跑馬燈banner -->
      <div id="owl-index">
        <ul class="owl-carousel owl-theme owl-index">
          <c:if test="${not empty bannerList}">
            <c:forEach items="${bannerList}" var="banner">
              <!-- list -->
              <li>
              	<c:if test="${not empty banner.url}">
              		<a href="${banner.url}" class="owl-card-item">
	                  <img src="${pageContext.request.contextPath}/api/query/image/${banner.imageId}">
	                </a>
              	</c:if>
                <c:if test="${empty banner.url}">
                  <img src="${pageContext.request.contextPath}/api/query/image/${banner.imageId}" class="owl-card-item">
              	</c:if>
              </li>
            </c:forEach>
          </c:if>
        </ul>
      </div>
      <!-- 商品廣告productAd -->
      <div class="mix-block">
        <div class="container">
          <h2 class="mix-title">優質推薦</h2>
          <ul class="row gap10">
            <div class="clearfix mb20 visible-xs"></div>
            <c:if test="${not empty productAdList}">
              <c:forEach items="${productAdList}" var="productAd">
                <li class="col-sm-4">
                  <a href="${productAd.productUrl}">
                    <img src="${productAd.imageUrl}">
                    <div class="hover-show">
                      <h3>${productAd.productName}</h3>
                      <p class="des">${productAd.description}</p>
                      <p class="price">$<s>${productAd.price}</s>
                        <span>$${productAd.discountPrice}</span>
                      </p>
                    </div>
                  </a>
                </li>
              </c:forEach>
            </c:if>
          </ul>
        </div>
      </div>
      <!-- mix-block -->
      <!-- video-block -->
      <!-- 影片廣浩videoAd -->
      <div class="video-section">
        <div class="container">
          <div class="video-block">
            <div class="clearfix"></div>
            <div class="row gap0 m0 plr0">
              <div class="col-md-8">
                <div class="video-img">
                  <div class="video-holder ">
                    <div class="embed-responsive embed-responsive-16by9">
                      <iframe class="video" src="https://www.youtube.com/embed/${videoAd.videoUrl}?autoplay=" frameborder="0" allow="autoplay; encrypted-media"
                        allowfullscreen></iframe>
                    </div>
                  </div>
                  <span class="play play-video">
                    <i class="icon-play"></i>
                  </span>
                  <img src="${pageContext.request.contextPath}/api/query/image/${videoAd.imageId}" alt="">
                </div>
              </div>
              <div class="col-md-4">
                <div class="video-right-content">
                  <div class="like">
                    <i></i>
                  </div>
                  <h2>${videoAd.title}</h2>
                  <div class="v-des">
                    ${videoAd.brief}
                  </div>
                  <div class="mb30"></div>
                  <a href="${videoAd.contentUrl}" class="btn btn-success btn-block btn-lg" target="_blank">詳細內容</a>
                </div>
              </div>

            </div>
            <!-- row -->
          </div>
        </div>
        <!-- container -->
        <!-- END video-block -->
      </div>
      <!-- video-section -->

      <div class="index-main-list for-index">
        <div class="container">
          <div class="row">
            <!-- list -->
            <!-- 線上課程course -->
            <div class="col-sm-4">
              <h4 class="orange-title">
                <a href="${pageContext.request.contextPath}/course/home">線上課程</a>
              </h4>
              <c:if test="${not empty courseList}">
                <a href="${pageContext.request.contextPath}/course/detail/${courseList[0].pkProduct}" class="main-img">
                  <img src="${courseList[0].image}">
                  <div class="hover-show">
                    <h3>${courseList[0].title}</h3>
                    <p class="des">${courseList[0].summary}</p>
                    <p class="price">
                      <s class="c-text">$<fmt:formatNumber value="${courseList[0].price}" type="number" maxFractionDigits="0" /></s>
                      <span>$<fmt:formatNumber value="${courseList[0].discountPrice}" type="number" maxFractionDigits="0" />
                      </span>
                    </p>
                  </div>
                </a>
                <ul class="pic-list">
                  <c:forEach items="${courseList}" var="course" varStatus="loop">
                    <c:if test="${loop.index > 0}">
                      <li>
                        <a href="${pageContext.request.contextPath}/course/detail/${course.pkProduct}" class="row gap10">
                          <div class="col-xs-4">
                            <img src="${course.image}">
                          </div>
                          <div class="col-xs-8">
                            <h2>${course.title}</h2>
                            <p class="price-b">$<s><fmt:formatNumber value="${course.price}" type="number" maxFractionDigits="0" /></s> <span>$<fmt:formatNumber value="${course.discountPrice}" type="number" maxFractionDigits="0" /></span>  </p>
                          </div>
                        </a>
                      </li>
                    </c:if>
                  </c:forEach>
                </ul>
                <div class="rt">
                  <hr class="mt0 mb10">
                  <a href="${pageContext.request.contextPath}/course/home" class="btn btn-default btn-sm">More</a>
                </div>

              </c:if>
            </div>
            <!-- /list -->
            <!-- list -->
            <!-- 營隊活動(暫無) -->
            <div class="col-sm-4">
              <h4 class="orange-title">
                <a href="${pageContext.request.contextPath}/camp/home">營隊活動</a>
              </h4>
              <c:if test="${not empty campList}">
                <a href="${campList[0].externalLink}" class="main-img">
                  <img src="${pageContext.request.contextPath}/api/query/image/${campList[0].imageId}">
                  <div class="hover-show">
                    <h3>${campList[0].campName}</h3>
                    <p class="des">${campList[0].campIntroduction}</p>
                    <p class="price">
                      <s class="c-text">$<fmt:formatNumber value="${campList[0].price}" type="number" maxFractionDigits="0" /></s>
                      <span>$<fmt:formatNumber value="${campList[0].discountPrice}" type="number" maxFractionDigits="0" />
                      </span>
                    </p>
                  </div>
                </a>
                <ul class="pic-list">
                  <c:forEach items="${campList}" var="camp" varStatus="loop">
                    <c:if test="${loop.index>0}">
                      <li>
                        <a href="${camp.externalLink}" class="row gap10">
                          <div class="col-xs-4">
                            <img src="${pageContext.request.contextPath}/api/query/image/${camp.imageId}">
                          </div>
                          <div class="col-xs-8">
                            <h2>${camp.campName}</h2>
                            <p class="price-b">$<s><fmt:formatNumber value="${camp.price}" type="number" maxFractionDigits="0" /></s> <span>$<fmt:formatNumber value="${camp.discountPrice}" type="number" maxFractionDigits="0" /></span>  </p>
                          </div>
                        </a>
                      </li>
                    </c:if>
                  </c:forEach>
                </ul>
                <div class="rt">
                  <hr class="mt0 mb10">
                  <a href="${pageContext.request.contextPath}/camp/home" class="btn btn-default btn-sm">More</a>
                </div>
              </c:if>
            </div>
            <!-- /list -->
            <!-- list -->
            <!-- 創客教具 -->
            <div class="col-sm-4">
              <h4 class="orange-title">
                <a href="${pageContext.request.contextPath}/props/home">創客教具</a>
              </h4>
              <c:if test="${not empty propsList}">
                <a href="${pageContext.request.contextPath}/props/detail/${propsList[0].pkProduct}" class="main-img">
                  <img src="${propsList[0].image}">
                  <div class="hover-show">
                    <h3>${propsList[0].title}</h3>
                    <p class="des">${propsList[0].summary}</p>
                    <p class="price">
                      <s class="c-text">$<fmt:formatNumber value="${propsList[0].price}" type="number" maxFractionDigits="0" /></s>
                      <span>$<fmt:formatNumber value="${propsList[0].discountPrice}" type="number" maxFractionDigits="0" />
                      </span>
                    </p>
                  </div>
                </a>
                <ul class="pic-list">
                  <c:forEach items="${propsList}" var="props" varStatus="loop">
                    <c:if test="${loop.index>0}">
                      <li>
                        <a href="${pageContext.request.contextPath}/props/detail/${props.pkProduct}" class="row gap10">
                          <div class="col-xs-4">
                            <img src="${props.image}">
                          </div>
                          <div class="col-xs-8">
                            <h2>${props.title}</h2>
                            <p class="price-b">$<s><fmt:formatNumber value="${props.price}" type="number" maxFractionDigits="0" /></s> <span>$<fmt:formatNumber value="${props.discountPrice}" type="number" maxFractionDigits="0" /></span>  </p>
                          </div>
                        </a>
                      </li>
                    </c:if>
                  </c:forEach>
                </ul>
                <div class="rt">
                  <hr class="mt0 mb10">
                  <a href="${pageContext.request.contextPath}/props/home" class="btn btn-default btn-sm">More</a>
                </div>
              </c:if>
            </div>
            <!-- /list -->
            <div class="clearfix mb20"></div>
            <!-- list -->
            <!-- 創客市集 -->
            <div class="col-sm-4">
              <h4 class="orange-title">
                <a href="${pageContext.request.contextPath}/share/home">創客基地</a><!--分享園地-->
              </h4>
              <c:if test="${not empty shareList}">
                <a href="${pageContext.request.contextPath}/share/detail/${shareList[0].shareId}" class="main-img">
                  <img src="${shareList[0].image}">
                  <div class="hover-show author">
                    <h3>${shareList[0].title}</h3>
                    <p class="des">${shareList[0].author}</p>
                  </div>
                </a>
                <ul class="pic-list">
                  <c:forEach items="${shareList}" var="share" varStatus="loop">
                    <c:if test="${loop.index > 0}">
                      <li>
                        <a href="${pageContext.request.contextPath}/share/detail/${share.shareId}" class="row gap10">
                          <div class="col-xs-4">
                            <img src="${share.image}">
                          </div>
                          <div class="col-xs-8">
                            <p class="des">${share.title}</p>
                            <p class="rt mt5">${share.author}</p>
                          </div>
                        </a>
                      </li>
                    </c:if>
                  </c:forEach>
                </ul>
                <div class="rt">
                  <hr class="mt0 mb10">
                  <a href="${pageContext.request.contextPath}/share/home" class="btn btn-default btn-sm">More</a>
                </div>
              </c:if>
            </div>
            <!-- /list -->
            <!-- list -->
            <!-- 名人推薦 -->
            <div class="col-sm-4">
              <h4 class="orange-title">
                <a href="${pageContext.request.contextPath}/recommend/home">分享園地</a>
              </h4>
              <c:if test="${not empty recommendList}">
                <a href="${pageContext.request.contextPath}/recommend/home" class="main-img">
                  <img src="${pageContext.request.contextPath}/img/default_pics/Default_celebrity-promote.png" class="scale">
                </a>
                <ul class="pic-list">
                  <c:forEach items="${recommendList}" var="recommend" varStatus="loop">
                    <li>
                      <a href="${pageContext.request.contextPath}/recommend/detail/${recommend.pkRecommend}" class="row gap10">
                        <div class="col-xs-4">
                          <img src="${pageContext.request.contextPath}/api/query/image/${recommend.imageId}">
                        </div>
                        <div class="col-xs-8">
                          <p class="des">${recommend.title}</p>
                          <p class="rt mt5">${recommend.author}</p>
                        </div>
                      </a>
                    </li>
                  </c:forEach>
                </ul>
                <div class="rt">
                  <hr class="mt0 mb10">
                  <a href="${pageContext.request.contextPath}/recommend/home" class="btn btn-default btn-sm">More</a>
                </div>
              </c:if>
            </div>
            <!-- /list -->
            <!-- list -->
            <!-- 檢測中心 -->
	        <jsp:include page="assessment/assessment.jsp"></jsp:include>
            <!-- /list -->
          </div>
          <!-- row -->
        </div>
      </div>