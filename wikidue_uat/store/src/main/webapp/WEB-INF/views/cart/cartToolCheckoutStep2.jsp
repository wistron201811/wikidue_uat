<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- angularjs -->
<script src="${pageContext.request.contextPath}/js/page/cart/cartController.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
<div ng-controller="CartController">
<form:form modelAttribute="checkoutForm" 
	action="${pageContext.request.contextPath}/cart/checkout" method="post">
<input type="hidden" id="method" name="method" value="checkoutValidate" />
<form:hidden path="receiptName" />
<form:hidden path="receiptTelPhone" />
<form:hidden path="receiptEmail" />
<form:hidden path="receiptZipCode" />
<form:hidden path="receiptCounty" />
<form:hidden path="receiptDistrict" />
<form:hidden path="receiptAddress" />
<form:hidden path="mainOrderNo" />
<form:hidden path="tempOrderNo" />
<form:hidden path="orderAmount" />

<form:hidden path="companyNo" />
<form:hidden path="companyName" />
<form:hidden path="loveCode" />
<form:hidden path="mobileCode" />
<form:hidden path="moicaNo" />
<form:hidden path="invoiceType" />
<form:hidden path="memberDeviceType" />
<form:hidden path="companyDeviceType" />
<form:hidden path="loveCodeName" />
<form:hidden path="remember" />
<div class="step-hold">
  <div class="container">
    <ul class="wizard-steps">
	    <li class="active"> 
	    <span class="step"><i class="icon-checklist"></i></span>
	    <span class="title">確認訂單</span> 
	    </li>
	    <li class="active"> 
	    <span class="step"><i class="icon-overview-use"></i></span>
	    <span class="title">填寫付款資訊</span> 
	    </li>
	    <li class="active"> 
	    <span class="step"><i class="icon-truck"></i></span>
	    <span class="title">填寫收貨資訊</span> 
	    </li>
	    <li> 
	    <span class="step"><i class="icon-expend_financialcard"></i></span>
	    <span class="title">選擇付款方式</span> 
	    </li>
	    <li> 
	    <span class="step"><i class="icon-clipboard"></i></span>
	    <span class="title">完成</span> 
	    </li>
  </ul>
  </div>
</div>
<div class="index-main-list for-camp">
      <div class="container">
      <div class="visible-xs mb10"></div>
                 <h2 class="main-title mt0 mb20 inline-block">輸入收件者資訊</h2>
      <!-- ///////////////////////////////////////////// -->
      <div class="row">
        <div class="col-md-8">
<hr class="mt0">
<div class="row">
  <div class="col-md-6">
    <div class="div-tb block-xs mb10">
                       <div class="td rt vmid wid_150px"><span class="color-red">*</span> 收件者</div>
                       <div class="td vmid pl10">
                           <form:input path="recipient" cssClass="form-control" maxlength="15"/>
                       </div>
                     </div>
  </div>
  <div class="col-md-6">
  <div class="div-tb block-xs mb10">
                       <div class="td rt vmid wid_150px"><span class="color-red">*</span> 行動電話</div>
                       <div class="td vmid pl10">
                           <form:input path="telPhone" cssClass="form-control" maxlength="10"/>
                       </div>
                     </div>
  </div>
  <div class="col-xs-12">
                       <div class="div-tb block-xs mb10" id="twzipcode">
  		
							<div class="td rt vmid wid_150px"><span class="color-red">*</span> 地址</div>
								<div class="td vmid pl10" data-role="county"
						         	data-name="county" data-width="100%" data-size="7"
						         	data-style="selectpicker">
								</div>
							
							<div class="mb10 visible-xs"></div>
							
							<div class="td vmid pl10" data-role="district"
						         data-name="district" data-width="100%" data-size="7"
						         data-style="selectpicker">
							</div>
							
							<div class="mb10 visible-xs"></div>
							
							<div class="td vmid pl10" data-role="zipcode"
						         data-name="zipCode"
			         			 data-value="${checkoutForm.zipCode}"
						         data-style="form-control">
							</div>
			        </div>
                     <div class="div-tb block-xs mb10">
                     <div class="td rt vmid wid_150px hidden-xs"></div>
                       <div class="td vmid pl10">
                           <form:input path="address" cssClass="form-control" placeholder="地址" maxlength="20"/>
                       </div>
                     </div>
  </div>
  
</div>
<!-- row -->


                     
        </div> 
        <div class="col-md-4">
          <ul class="order-list green-rim">
        <!-- list -->
          <li>
<!--             <div class="div-tb"> -->
<!--             <div class="td vmid"><p class="info">總價</p> </div> -->
<!--             <div class="td vmid rt"><p class="info">NT$ 3,300</p> </div> -->
<!--           </div> -->
<!--           <hr class="mt10 mb20"> -->
          <div class="div-tb">
            <div class="td vmid"> <h3 class="title">結帳金額</h3> </div>
            <div class="td vmid rt"> <h3 class="price mt0 mb0"><fmt:formatNumber value = "${checkoutForm.orderAmount}" type = "currency" maxFractionDigits="0"/>元</h3> </div>
          </div>
          	<a href="javascript:;" onclick="nextStep();return false;" class="btn btn-success btn-block mt20">前往付款</a>
       		<br>
    		<a href="javacript:;" onclick="lastStep();return false;" style="text-align:center;">回上一步</a>
          </li>
          <!-- /list -->
         
        </ul>
        </div>
      </div>
        <!-- row -->
      </div>
      <!-- container -->
    </div>
</form:form>
</div>