<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- angularjs -->
<script src="${pageContext.request.contextPath}/js/page/cart/cartController.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
<div ng-controller="CartController">
<form:form modelAttribute="checkoutForm" 
	action="${pageContext.request.contextPath}/cart/checkout" method="post">
<input type="hidden" name="method" value="checkoutValidate" />
<form:hidden path="recipient" />
<form:hidden path="telPhone" />
<form:hidden path="zipCode" />
<form:hidden path="address" />
<form:hidden path="mainOrderNo" />
<form:hidden path="tempOrderNo" />
<form:hidden path="orderAmount" />
<form:hidden path="loveCodeName" />
<div class="step-hold">
  <div class="container">
    <ul class="wizard-steps">
     <li class="active"> 
     <span class="step"><i class="icon-checklist"></i></span>
     <span class="title">確認訂單</span> 
     </li>
     <li class="active"> 
     <span class="step"><i class="icon-overview-use"></i></span>
     <span class="title">填寫付款資訊</span> 
     </li>
     <li> 
     <span class="step"><i class="icon-expend_financialcard"></i></span>
     <span class="title">選擇付款方式</span> 
     </li>
     <li> 
     <span class="step"><i class="icon-clipboard"></i></span>
     <span class="title">完成</span> 
     </li>
   </ul>
  </div>
</div>
<div class="index-main-list for-camp">
      <div class="container">
      <div class="visible-xs mb10"></div>
                 <h2 class="main-title mt0 mb20 inline-block">選擇發票類型</h2>
  <p class="color-gray inline-block"><span class="color-red">*</span>為必填欄位</p>
      <!-- ///////////////////////////////////////////// -->
      <div class="row">
        <div class="col-md-8">
       <div class="btn-group" data-toggle="buttons">
       <c:choose>
       	<c:when test="${checkoutForm.invoiceType=='1'}">
		  <label class="btn btn-default active">
		  </c:when>
		  <c:otherwise>
		  <label class="btn btn-default">
		  </c:otherwise>
		</c:choose>
		    <form:radiobutton path="invoiceType" value="1"/> 個人
		  </label>
	
       <c:choose>
       	<c:when test="${checkoutForm.invoiceType=='2'}">
		  <label class="btn btn-default active">
		  </c:when>
		  <c:otherwise>
		  <label class="btn btn-default">
		  </c:otherwise>
		</c:choose>
		    <form:radiobutton path="invoiceType" value="2"/> 捐贈
		  </label>
       <c:choose>
       	<c:when test="${checkoutForm.invoiceType=='3'}">
		  <label class="btn btn-default active">
		  </c:when>
		  <c:otherwise>
		  <label class="btn btn-default">
		  </c:otherwise>
		</c:choose>
		    <form:radiobutton path="invoiceType" value="3"/> 企業
		  </label>
</div>
<hr>
<div class="row">
  <div class="col-md-6">
    <div class="div-tb block-xs mb10">
                       <div class="td rt vmid wid_150px"><span class="color-red">*</span> 購買人</div>
                       <div class="td vmid pl10">
                           <form:input path="receiptName" cssClass="form-control" maxlength="15"/>
                       </div>
                     </div>
  </div>
  <div class="col-md-6">
  <div class="div-tb block-xs mb10">
                       <div class="td rt vmid wid_150px"><span class="color-red">*</span> 行動電話</div>
                       <div class="td vmid pl10">
                           <form:input path="receiptTelPhone" cssClass="form-control" maxlength="10"/>
                       </div>
                     </div>
  </div>
  <div class="col-xs-12 ">
  <div class="div-tb block-xs mb10">
                       <div class="td rt vmid wid_150px">電子信箱</div>
                       <div class="td vmid pl10">
                       		${checkoutForm.receiptEmail} <%-- <a class="btn" target="_blank" href="<spring:eval expression="@configService.memberCenterUrl" />">修改</a> --%>
                           <form:hidden path="receiptEmail"/>
                       </div>
                     </div>
  </div>
  <div class="col-xs-12">

  		<div class="div-tb block-xs mb10" id="twzipcode">
  		
				<div class="td rt vmid wid_150px"><span class="color-red">*</span> 地址</div>
					<div class="td vmid pl10" data-role="county"
			         	data-name="receiptCounty" data-width="100%" data-size="7"
			         	data-style="selectpicker">
					</div>
				
				<div class="mb10 visible-xs"></div>
				
				<div class="td vmid pl10" data-role="district"
			         data-name="receiptDistrict" data-width="100%" data-size="7"
			         data-style="selectpicker">
				</div>
				
				<div class="mb10 visible-xs"></div>
				
				<div class="td vmid pl10" data-role="zipcode"
			         data-name="receiptZipCode"
         			 data-value="${checkoutForm.receiptZipCode}"
			         data-style="form-control">
				</div>
        </div>
        <div class="div-tb block-xs mb10">
        <div class="td rt vmid wid_150px hidden-xs"></div>
          <div class="td vmid pl10">
              <form:input path="receiptAddress" cssClass="form-control" placeholder="地址" maxlength="20"/>
              <small class="color-gray info">做為寄送企業電子發票及個人中獎發票使用，請確實填寫</small>
          </div>
        </div>
           
            <div class="div-tb block-xs mb10 invoice2">
              <div class="td rt vmid wid_150px"><span class="color-red">*</span> 捐贈對象</div>
              <div class="td vmid pl10">
                  <form:input path="loveCode" cssClass="form-control" maxlength="7" placeholder="愛心碼"/>
              </div>
           </div>           
            <div class="div-tb block-xs mb10 invoice3">
              <div class="td rt vmid wid_150px"><span class="color-red">*</span> 企業名稱</div>
              <div class="td vmid pl10">
                  <form:input path="companyName" cssClass="form-control" maxlength="20"/>
              </div>
           </div>           
            <div class="div-tb block-xs mb10 invoice3">
              <div class="td rt vmid wid_150px"><span class="color-red">*</span> 統一編號</div>
              <div class="td vmid pl10">
                  <form:input path="companyNo" cssClass="form-control" maxlength="8"/>
              </div>
           </div>  
        <div class="div-tb block-xs mb10 invoice1">
          <div class="td rt vmid wid_150px"><span class="color-red">*</span> 電子發票載具</div>
          <div class="td vmid pl10">
              <form:select path="memberDeviceType" cssClass="selectpicker" title="請選擇">
				  <form:option value="1">會員載具</form:option>
				  <form:option value="2">共通性載具-自然人憑證</form:option>
				  <form:option value="3">共通性載具-手機條碼</form:option>
				  <form:option value="4">不使用載具-索取發票</form:option>
			  </form:select>
                 </div>
         </div>     
        <div class="div-tb block-xs mb10 invoice3">
          <div class="td rt vmid wid_150px"><span class="color-red">*</span> 電子發票載具</div>
          <div class="td vmid pl10">
              <form:select path="companyDeviceType" cssClass="selectpicker" title="請選擇">
				  <form:option value="4">不使用載具-索取發票</form:option>
			  </form:select>
                 </div>
         </div>     
          <div class="div-tb block-xs mb10 invoice1 device-type2">
              <div class="td rt vmid wid_150px"><span class="color-red">*</span> 自然人憑證號碼</div>
              <div class="td vmid pl10">
                  <form:input path="moicaNo" cssClass="form-control" maxlength="16" placeholder="輸入2位英文字母加上14位數字，ex：TP03000001234567"/>
                  <small class="color-gray info">根據財政部「電子發票實施作業」要點，系統預設為不索取紙本發票，並於發票中獎時以email通知</small>
              </div>
           </div>           
            <div class="div-tb block-xs mb10 invoice1 device-type3">
              <div class="td rt vmid wid_150px"><span class="color-red">*</span> 手機條碼</div>
              <div class="td vmid pl10">
                  <form:input path="mobileCode" cssClass="form-control" maxlength="8" placeholder="輸入1位斜線『/』加上7位由數字及字母組成的手機條碼"/>
                  <small class="color-gray info">根據財政部「電子發票實施作業」要點，系統預設為不索取紙本發票，並於發票中獎時以email通知</small>
              </div>
           </div>               
          <div class="div-tb block-xs mb10">
            <div class="td rt vmid wid_150px"></div>
              <div class="td vmid pl10">
               <div class="checkbox">
                 <label >
                   <form:checkbox path="remember"/> 記住我的資訊
                 </label>
               </div>
                       </div>
                     </div>

  </div>
  <div class="col-xs-12">
  <div class="well">
     <div class="checkbox">
                 <label ><input type="checkbox" name="check"> 
                   我已仔細閱讀並了解「<a href="#modal-privacy" class="inline" data-toggle="modal">隱私權政策</a>」「<a href="#modal-terms" class="inline" data-toggle="modal">使用條款</a>」等所載內容其意義，茲同意該等條款規定，並願遵守網站各種規則。
                 </label>
               </div>
               </div>
  </div>
</div>
<!-- row -->


                     
        </div> 
        <div class="col-md-4">
          <ul class="order-list green-rim">
        <!-- list -->
          <li>
<!--             <div class="div-tb"> -->
<!--             <div class="td vmid"><p class="info">總價</p> </div> -->
<!--             <div class="td vmid rt"><p class="info">NT$ 3,300</p> </div> -->
<!--           </div> -->
<!--           <hr class="mt10 mb20"> -->
          <div class="div-tb">
            <div class="td vmid"> <h3 class="title">結帳金額</h3> </div>
            <div class="td vmid rt"> <h3 class="price mt0 mb0"><fmt:formatNumber value = "${checkoutForm.orderAmount}" type = "currency" maxFractionDigits="0"/>元</h3> </div>
          </div>
          <a href="javascript:;" onclick="nextStep();return false;" class="btn btn-success btn-block mt20">下一步</a>
        
          </li>
          <!-- /list -->
         
        </ul>
        </div>
      </div>
        <!-- row -->
      </div>
      <!-- container -->
    </div>
</form:form>
</div>
<div class="modal fade " id="modal-privacy">
<div class="modal-vertical-middle">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Wikidue Store 教育商城 隱私權政策</h4>
      </div>
      <div class="modal-body ">
      <div class="panel-group sort-panel" id="AA">
                            <!-- QA-LIST -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> <a class="accordion-toggle " data-toggle="collapse" data-parent="#AA" href="#collapse1" aria-expanded="false"> 適用範圍 </a> </h4>
                                </div>
                                <div class="panel-collapse collapse in" id="collapse1" aria-expanded="false">
                                    <div class="panel-body lt">
                                       <ul class="list-num">
          <li>Wikidue Store 教育商城(以下簡稱本平台)隱私權聲明及其所包含之告知事項，僅適用於本平台所擁有及經營的網站與行動應用程式，所涉及之個人資料蒐集、處理與利用行為。</li>
          <li>本平台網站內可能包含許多連結、或其他合作夥伴所提供的服務，其有關個人資料的保護，適用第三方或各該網站的隱私權政策，關於該等連結網站或合作夥伴網站的隱私權聲明及與個人資料保護有關之告知事項，請參閱各該連結網站或合作夥伴網站。本平台不負任何連帶責任。</li>
        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- / QA-LIST -->
                            <!-- QA-LIST -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#AA" href="#collapse2" aria-expanded="false"> 個人資料之蒐集、處理及利用
                                    </a> </h4>
                                </div>
                                <div class="panel-collapse collapse" id="collapse2" aria-expanded="false">
                                    <div class="panel-body lt">
                                      <ul class="list-num">
          <li>本平台為了行銷、客戶管理與服務、提供網路購物及其他電子商務服務、履行法定或合約義務、保護當事人及相關利害關係人之權益、財產保險、責任保險、售後服務、辦理贈獎活動（包含確認得獎者身份、提供贈品及依法開立扣繳憑單等）以及經營合於營業登記項目或組織章程所定之業務等目的，可能蒐集您的姓名、生日、身份證號、連絡方式(包括但不限於電話、E-MAIL及地址等)、服務單位、職稱、為完成收款或付款所需之資料、ＩＰ位址、及其他得以直接或間接識別使用者身分之個人資料。</li>
          <li>本平台所蒐集的足以識別使用者身分的個人資料，都僅供本平台於其內部、依照蒐集之目的進行處理和利用，非經您書面同意，本平台不會將個人資料用於其他用途。</li>
          <li>如果您使用電話、傳真或本平台意見信箱與本平台聯繫時，請您提供正確的電話、傳真號碼或電子信箱地址，作為回覆來詢事項之用。</li>
          <li>您的個人資料在處理過程中，本平台將遵守相關之流程及內部作業規範，並依據資訊安全之要求，進行必要之人員控管。</li>
          <li>單純瀏覽本平台及下載檔案之行為，本平台不會蒐集任何與個人身份有關之資訊。</li>
          <li>您同意本平台透過您所提供的郵件地址，與您取得聯絡並發送課程相關資訊提供您參考。</li>
        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- / QA-LIST -->
                            <!-- QA-LIST -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#AA" href="#collapse3" aria-expanded="false"> 與第三人共用個人資料
                                    </a> </h4>
                                </div>
                                <div class="panel-collapse collapse" id="collapse3" aria-expanded="false">
                                    <div class="panel-body lt">
                                      <ul class="list-num">
                                      <li>本平台絕不會提供、交換、出租或出售任何您的個人資料給其他個人、團體、私人企業或公務機關，但有法律依據或合約義務者，不在此限。</li>
                                      <li>
                                      前項但書之情形包括不限於：
                                      <ol class="list-dot">
                                        <li>配合司法單位合法的調查。</li>
                                        <li>配合主管機關依職權或職務需要之調查或使用（例如審計部或會計師查帳）。</li>
                                        <li>基於善意相信揭露您的個人資料為法律所必需。</li>
                                        <li>當您在本平台的行為，違反本平台的使用條款或可能損害或妨礙本平台權益或導致任何人遭受損害時，經本平台研析揭露您的個人資料是為了辨識、聯絡或採取法律行動所必要者。</li>
                                        <li>基於委外契約關係，本平台依約履行提供個人資料之義務。</li>
                                      </ol>
                                      </li>
                                      <li>本平台委託廠商協助蒐集、處理或利用您的個人資料時，將對委外廠商或個人善盡監督管理之責。</li>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- / QA-LIST -->
                            <!-- QA-LIST -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#AA" href="#collapse4" aria-expanded="false"> cookie之運用
                                    </a> </h4>
                                </div>
                                <div class="panel-collapse collapse" id="collapse4" aria-expanded="false">
                                    <div class="panel-body lt">
                                      <ul class="list-num">
                                      <li>基於網站內部管理之需要及提供最佳個人化服務，本平台將在您的瀏覽器中寫入cookies並讀取記錄瀏覽者的IP位址、上網時間以及在各項資訊查閱之次數，進行網站流量和網路行為調查之總量分析，並適時地對個別瀏覽者進行分析，以提供更符合的資訊服務。</li>
                                      <li>若您關閉cookies，將無法使用本平台與網站群部分服務。</li>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- / QA-LIST -->
                             <!-- QA-LIST -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#AA" href="#collapse5" aria-expanded="false"> 伺服器紀錄
                                    </a> </h4>
                                </div>
                                <div class="panel-collapse collapse" id="collapse5" aria-expanded="false">
                                    <div class="panel-body lt">當您透過瀏覽器、應用程式或其他用戶端使用本平台時，我們的伺服器會自動記錄特定的技術性資訊。這些伺服器紀錄可能包含您的網頁要求、網際網路通訊協定位址、瀏覽器類型、瀏覽器語言、送出要求的日期和時間等資訊。此伺服器紀錄僅作為伺服器管理的參考，本平台不會利用此伺服器紀錄對「個別」瀏覽者進行分析。</div>
                                </div>
                            </div>
                            <!-- / QA-LIST -->
                            <!-- QA-LIST -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#AA" href="#collapse6" aria-expanded="false"> 隱私權聲明之修改
                                    </a> </h4>
                                </div>
                                <div class="panel-collapse collapse" id="collapse6" aria-expanded="false">
                                    <div class="panel-body lt">本隱私權聲明將適時依據法律修正、相關技術之發展及內部管理制度之調整而配合修改，並得於修訂後公佈在本平台網站上或行動應用程式之適當位置，不另行個別通知，您可以隨時在本平台網站上或行動應用程式詳閱修訂後的隱私權聲明及相關告知事項。</div>
                                </div>
                            </div>
                            <!-- / QA-LIST -->
                        </div>
      </div>
   
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
  </div>
</div><!-- /.modal -->
<div class="modal fade " id="modal-terms" >
<div class="modal-vertical-middle">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">使用條約</h4>
      </div>
      <div class="modal-body ">
      <div class="panel-group sort-panel" id="BB">
                            <!-- QA-LIST -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> <a class="accordion-toggle " data-toggle="collapse" data-parent="#BB" href="#cc1" aria-expanded="false">遵守會員規範 </a> </h4>
                                </div>
                                <div class="panel-collapse collapse in" id="cc1" aria-expanded="false">
                                    <div class="panel-body lt">
                                      在您於Wikidue Store 教育商城(以下簡稱為本平台)註冊成為會員後，可以使用本平台所提供之各種服務。當您使用本平台時，即表示同意接受本平台之會員規範及所有注意事項之約束。
                                    </div>
                                </div>
                            </div>
                            <!-- / QA-LIST -->
                            <!-- QA-LIST -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#BB" href="#cc2" aria-expanded="false">使用服務 </a> </h4>
                                </div>
                                <div class="panel-collapse collapse" id="cc2" aria-expanded="false">
                                    <div class="panel-body lt">
                                    <p>請您於註冊時儘可能提供符合真實之個人資料，您所登錄之資料事後若有變更時，也一併隨時更新。 </p>
                                    <p>個人資料之蒐集與使用</p>
                                      <ul class="list-num">
          <li>會員於本平台提供之個人資料，由緯育香港股份有限公司及其關係企業（以下簡稱為本公司）及本公司委外服務廠商基於系統營運、客戶服務、帳務管理及其他合於本公司營業項目之特定目的，依個人資料保護法相關規定，對會員之個人資料進行蒐集、處理、利用等事宜，以供本平台營運期間與業務涉及區域範圍內進行聯絡、會員管理、統計調查與分析、系統營運或系統資料庫之執行、使用與處理等用途。</li>
          <li>會員得自由選擇是否提供相關個人資料，惟會員所拒絕提供之個人資料，若是本公司或本平台辦理業務或作業所需之必要資料，本公司與本平台可能無法進行必要的業務或作業而礙難為會員提供相關服務，並有權暫時停止提供對會員之服務。</li>
        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- / QA-LIST -->
                            <!-- QA-LIST -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#BB" href="#cc3" aria-expanded="false">個人資料之保護 </a> </h4>
                                </div>
                                <div class="panel-collapse collapse" id="cc3" aria-expanded="false">
                                    <div class="panel-body lt">
                                    <p>對於會員所登錄或留存之個人資料，本平台在未獲得會員同意以前，決不對外揭露會員之姓名、地址、電子郵件地址及其他依法受保護之個人資料。下述幾種特殊情況不受此限：</p>
                                      <ul class="list-num">
                                      <li>基於法律之規定。</li>
                                      <li>受司法機關或其他有權機關基於法定程序之要求。</li>
                                      <li>為保障本平台之財產及權益。</li>
                                      <li>在緊急情況下為維護其他會員或第三人之人身安全。</li>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- / QA-LIST -->
                            <!-- QA-LIST -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#BB" href="#cc4" aria-expanded="false"> 線上訂購
                                    </a> </h4>
                                </div>
                                <div class="panel-collapse collapse" id="cc4" aria-expanded="false">
                                    <div class="panel-body lt">
                                      <ul class="list-num">
                                      <li> 除本約定條款外，商品銷售網頁及訂購流程中相關網頁上所呈現之相關資訊，包括相關交易條件、限制及說明等，亦為契約之一部分。</li>
                                      <li>原則上，您在本平台所進行的消費，是由本平台提供訂購服務，但是，您在本平台所瀏覽的訊息、或所進行的消費，也可能是由第三人所提供的訂購服務，在此等情況下，您是向各該第三人進行訂購、並由各該第三人依照其所制定的交易條件負責履行，本平台僅提供行銷服務、連結、或電子商務平台服務。</li>
                                      <li>您一旦在本平台依照網頁所定方式、條件及流程完成訂購程序，就表示您提出要約、願意依照本約定條款及相關網頁上所載明的約定內容、交易條件或限制，訂購該商品或服務。您所留存的資料(如地址、電話)如有變更，應立即自行上線修改所留存的資料，而且不得以資料不符為理由，否認訂購行為或拒絕付款。</li>
                                      <li>在您完成線上訂購程序以後，本系統會自動經由電子郵件或其他方式寄給您一封通知，但是該項通知只是通知您本系統已經收到您的訂購訊息，不代表交易已經完成或契約已經成立，本平台保留是否接受您的訂單的權利。如果本平台確認交易條件無誤、您所訂購之商品仍有存貨或所訂購之服務仍可提供、且無其他本平台無法接受訂單之情形，本平台會直接通知配合廠商出貨，不另行通知，但是您可以在網站上查詢出貨狀況。若交易條件有誤、商品無存貨、服務無法提供、或有本平台無法接受訂單之情形，本平台得拒絕接受訂購。</li>
                                      <li>您瞭解並同意，雖然本平台會盡力維護相關資料的正確性，但本平台不以任何明示或默示的方式保證所有出現在網頁上、或相關訊息上的資料均為完整、正確、即時的資訊。關於相關商品或服務之訂購數量上限，依各該商品或服務銷售網頁及訂購流程中相關網頁之記載。如果相關商品或服務的規格、圖片或說明有誤，仍以原廠、代理商、進口商、經銷商或服務提供者的資料為準。如果網頁上、或相關訊息所標示的價格有誤：
                                      <ol class="list-dot">
                                        <li>※若標示價格比正確價格高，本平台只會向您收取較低的正確價格；</li>
                                        <li>※若標示價格比正確價格低，本平台保留拒絕接受訂單的權利。
如果相關商品或服務的規格、圖片、說明、價格、或相關交易條件有誤，本平台得在您完成訂購程序後二日內，拒絕接受您的訂單。
</li>
                                      </ol>
                                      </li>
                                      <li>您所訂購的所有商品或服務，關於其品質、保固及售後服務等，都是由各該商品或服務的原廠、代理商、進口商、經銷商或服務提供者，依照其所制定的條件，負責對您提供品質承諾、保固及售後服務等，但本平台承諾協助您解決關於因為線上消費所產生的疑問或爭議。</li>
                                      <li>您可以依照各該商品或服務銷售網頁及訂購流程中相關網頁所記載之方式、條件及限制，選擇您所訂購之商品或服務之交付地點及方式。您所訂購的商品或服務，若經配送兩次無法送達、且經無法聯繫超過三天者，本平台將取消該筆訂單、並全額退款。</li>
                                      <li>本平台網站內可能包含許多連結，這些被連結網站或網頁上的所有資訊，都是由被連結網站所提供，本平台不以任何明示或默示的方式擔保其內容的正確性、可信度或即時性。</li>
                                      <li>如果本平台內的商品說明、價格及相關交易條件等，是由配合廠商所自行製作及上載，則本平台不以任何明示或默示的方式擔保其內容的正確性、可信度或即時性。</li>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- / QA-LIST -->
                             <!-- QA-LIST -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#BB" href="#cc5" aria-expanded="false">關於退貨退款
                                    </a> </h4>
                                </div>
                                <div class="panel-collapse collapse" id="cc5" aria-expanded="false">
                                    <div class="panel-body lt">
                                      <ul class="list-num">
                                      <li>如果您所訂購的商品有瑕疵，您可以要求全額退費。</li>
                                      <li>您可以依消費者保護法第十九條第一項之規定，行使相關權利。</li>
                                      <li>關於退貨退款方式及條件，依各該商品銷售網頁及訂購流程中之相關網頁之記載。</li>
                                      <li>您所退回的商品，必須保持所有商品、贈品、附件、包裝、及所有附隨文件或資料在出貨當時狀態的完整性，如果有實體發票，並應連同發票一併退回及簽署折讓單等相關法令所要求之單據；否則本平台得拒絕接受您的退貨退款要求。</li>
                                      <li>您瞭解並同意，若因您要求退貨或換貨、或因本平台無法接受您全部或部分之訂單、或契約因故解除或失其效力，而需為您辦理退款事宜時，本平台得代您處理發票或折讓單等相關法令所要求之單據。</li>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- / QA-LIST -->
                            <!-- QA-LIST -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#BB" href="#cc6" aria-expanded="false"> 會員的義務與責任
                                    </a> </h4>
                                </div>
                                <div class="panel-collapse collapse" id="cc6" aria-expanded="false">
                                    <div class="panel-body lt">
                                    <p>會員對本身於本平台或透過本平台傳輸的一切內容自負全責:</p>
                                      <ul class="list-num">
                                      <li>會員承諾遵守中華民國相關法規及一切國際網際網路規定與慣例。</li>
                                      <li>會員同意並保證不公布或傳送任何不實、威脅、不雅、猥褻、不法、攻擊性、誹謗性或侵害他人智慧財產權的文字，圖片或任何形式的檔案於本平台上。</li>
                                      <li>非經本平台書面同意，會員不得於本平台上從事廣告或販賣商品行為。</li>
                                      <li>會員同意避免在公眾討論區討論私人事務，發表文章時，請尊重他人的權益及隱私權。</li>
                                      <li>會員同意必須充份尊重著作權，禁止發表侵害他人各項智慧財產權之文字、圖片或任何形式的檔案。</li>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- / QA-LIST -->
                            <!-- QA-LIST -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#BB" href="#cc7" aria-expanded="false">智慧財產權
                                    </a> </h4>
                                </div>
                                <div class="panel-collapse collapse" id="cc7" aria-expanded="false">
                                    <div class="panel-body lt">
                                      <ul class="list-num">
                                      <li>本平台所使用之軟體或程式(以下簡稱軟體)、網站上所有內容，包括但不限於著作、圖片、檔案、資訊、資料、網站架構、網站畫面的安排、網頁設計、會員內容等，均由本公司或其他權利人依法擁有其智慧財產權，包括但不限於商標權、專利權、著作權、營業秘密與專有技術等。任何人不得逕自使用、修改、重製、公開播送、公開傳輸、公開演出、改作、散布、發行、公開發表、進行還原工程、解編或反向組譯。若會員欲引用或轉載前述軟體、程式或網站內容，除明確為法律所許可者外，必須依法取得本公司或其他權利人的事前書面同意。</li>
                                      <li>本平台為本公司行銷宣傳之商標，依其註冊或使用之狀態，受商標法及公平交易法等之保護，未經本公司事前書面同意，任何人不得以任何方式使用本平台商標。</li>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- / QA-LIST -->
                            <!-- QA-LIST -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#BB" href="#cc8" aria-expanded="false">禁止從事違反法律規定之行為
                                    </a> </h4>
                                </div>
                                <div class="panel-collapse collapse" id="cc8" aria-expanded="false">
                                    <div class="panel-body lt">本平台就會員的行為是否符合使用條約有最終決定權。若本平台決定會員的行為違反本使用條約或任何法令，會員同意本平台得隨時停止帳號使用權或清除帳號，並停止使用本平台。會員就違反法律規定之情事，應自負法律責任。</div>
                                </div>
                            </div>
                            <!-- / QA-LIST -->
                             <!-- QA-LIST -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#BB" href="#cc9" aria-expanded="false">禁止從事違反法律規定之行為
                                    </a> </h4>
                                </div>
                                <div class="panel-collapse collapse" id="cc9" aria-expanded="false">
                                    <div class="panel-body lt">
                                      <ul class="list-num">
                                      <li>
                                        於發生下列情形之一時，本平台有權停止或中斷提供服務：
                                        <ol class="list-dot">
                                          <li>對本平台之設備進行必要之保養及施工時</li>
                                          <li>發生突發性之設備故障時</li>
                                          <li>由於本平台所申請之ISP業者無法提供服務時</li>
                                          <li>因天災等不可抗力之因素致使本平台無法提供服務時</li>
                                        </ol>
                                      </li>
                                      <li>本平台可能因公司或ISP業者網路系統軟硬體設備之故障、失靈或人為操作上之疏失而造成全部或部份中斷、暫時無法使用、延遲或造成資料傳輸或儲存上之錯誤、或遭第三人侵入系統篡改或偽造變造資料等，會員不得因此而要求任何補償。</li>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- / QA-LIST -->
                            <!-- QA-LIST -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#BB" href="#cc10" aria-expanded="false">保管及通知義務
                                    </a> </h4>
                                </div>
                                <div class="panel-collapse collapse" id="cc10" aria-expanded="false">
                                    <div class="panel-body lt">您有責任維持密碼及帳號的機密安全。您必須完全負起因利用該密碼及帳號所進行之一切行動之責任。當密碼或帳號遭到未經授權之使用，或發生其他任何安全問題時，您必須立即通知本平台，每次您連線完畢，均要結束您的帳號使用。因您未遵守本項約定所生之任何損失或損害，我們將無法亦不予負責。</div>
                                </div>
                            </div>
                            <!-- / QA-LIST -->
                            <!-- QA-LIST -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#BB" href="#cc11" aria-expanded="false">特別同意事項
                                    </a> </h4>
                                </div>
                                <div class="panel-collapse collapse" id="cc11" aria-expanded="false">
                                    <div class="panel-body lt">您同意於本平台所發表之一切內容僅代表您個人之立場與行為，並同意承擔所有相關衍生之責任，本平台不負任何責任。
您同意，本平台可以透過您所提供的E-mail和您取得聯絡，並提供相關產品或服務的資訊給您。
</div>
                                </div>
                            </div>
                            <!-- / QA-LIST -->
                            <!-- QA-LIST -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#BB" href="#cc12" aria-expanded="false">擔保責任免除
                                    </a> </h4>
                                </div>
                                <div class="panel-collapse collapse" id="cc12" aria-expanded="false">
                                    <div class="panel-body lt">
                                      <ul class="list-num">
                                      <li> 本平台保留隨時停止、更改各項服務內容或終止任一會員帳號使用之權利，無需事先通知會員本人。無論任何情形，就停止或更改服務或終止會員帳號使用所可能產生之困擾、不便或損害，本平台對任何會員或第三人均不負任何責任。 </li>
                                      <li>本平台保留將來新增、修改或刪除各項服務之全部或一部之權利，且不另行個別通知會員不得因此而要求任何補償或賠償。</li>
                                      <li>本平台提供之特定服務可能存在專屬之服務條款，在此情形下，雙方權利義務將依據該服務之專屬條款決定之。</li>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- / QA-LIST -->
                            <!-- QA-LIST -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#BB" href="#cc13" aria-expanded="false">損害賠償
                                    </a> </h4>
                                </div>
                                <div class="panel-collapse collapse" id="cc13" aria-expanded="false">
                                    <div class="panel-body lt">因會員違反相關法令或違背本使用條約之任一條款，致本平台或本公司及其關係企業、受僱人、受託人、代理人及其他相關履行輔助人因而受有損害或支出費用（包括且不限於因進行民事、刑事及行政程序所支出之律師費用）時，會員應負擔損害賠償責任或填補其費用。</div>
                                </div>
                            </div>
                            <!-- / QA-LIST -->
                            <!-- QA-LIST -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#BB" href="#cc14" aria-expanded="false">使用條約之修改
                                    </a> </h4>
                                </div>
                                <div class="panel-collapse collapse" id="cc14" aria-expanded="false">
                                    <div class="panel-body lt">本平台保留隨時修改本使用條約之權利，本平台將於修改使用條約時，於WEB首頁公告修改之內容，而不另對會員作個別通知。若會員不同意修改的內容，請勿繼續使用本平台。如果會員繼續使用本平台，將視為會員已同意並接受本使用條約增訂或修改內容之約束。</div>
                                </div>
                            </div>
                            <!-- / QA-LIST -->
                            <!-- QA-LIST -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#BB" href="#cc15" aria-expanded="false">個別條款之效力
                                    </a> </h4>
                                </div>
                                <div class="panel-collapse collapse" id="cc15" aria-expanded="false">
                                    <div class="panel-body lt">本使用條約所定之任何條款之全部或一部無效時，不影響其他條款之效力。</div>
                                </div>
                            </div>
                            <!-- / QA-LIST -->
                            <!-- QA-LIST -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"> <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#BB" href="#cc16" aria-expanded="false">準據法及管轄法院
                                    </a> </h4>
                                </div>
                                <div class="panel-collapse collapse" id="cc16" aria-expanded="false">
                                    <div class="panel-body lt">本使用條約之解釋及適用以及會員因使用本平台而與本平台間所生之權利義務關係，應依中華民國法令解釋適用之。其因此所生之爭議，以臺灣臺北地方法院為第一審管轄法院。</div>
                                </div>
                            </div>
                            <!-- / QA-LIST -->
                        </div>
      </div>
   
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
  </div>
</div><!-- /.modal -->