<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
        <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
        <%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
            <!-- angularjs -->
            <script src="${pageContext.request.contextPath}/js/page/cart/cartController.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
            <div ng-controller="CartController">
                <div class="step-hold">
                    <div class="container">
                        <ul class="wizard-steps">
                            <li class="active">
                                <span class="step">
                                    <i class="icon-checklist"></i>
                                </span>
                                <span class="title">確認訂單</span>
                            </li>
                            <li>
                                <span class="step">
                                    <i class="icon-overview-use"></i>
                                </span>
                                <span class="title">填寫付款資訊</span>
                            </li>
                            <li ng-if="hasProps">
                                <span class="step">
                                    <i class="icon-truck"></i>
                                </span>
                                <span class="title">填寫收貨資訊</span>
                            </li>
                            <li>
                                <span class="step">
                                    <i class="icon-expend_financialcard"></i>
                                </span>
                                <span class="title">選擇付款方式</span>
                            </li>
                            <li>
                                <span class="step">
                                    <i class="icon-clipboard"></i>
                                </span>
                                <span class="title">完成</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="index-main-list for-camp">
                    <div class="container">
                        <div class="visible-xs mb10"></div>
                        <h2 class="main-title mt0 mb20 inline-block">確認訂單</h2>
<!--                         <p class="color-gray inline-block"> -->
<!--                             購物車中總共1門課程 -->
<!--                             <span class="color-green">從收藏加入購物車</span> -->
<!--                         </p> -->
                        <!-- ///////////////////////////////////////////// -->
                        <div class="row">
                            <div class="col-md-8">
                                <ul class="order-list" bs-loading-overlay="order-list-block">
                                    <!-- list -->
                                    <li ng-repeat="cart in cartList">
                                        <span class="l-close" ng-click="remove(cart)"></span>
                                        <div class="div-tb">
                                            <div class="td wid_30per">
                                                <img style="cursor: pointer;" ng-click="goPage('${pageContext.request.contextPath}/'+(cart.stock > -1?'props':'course')+'/detail/'+cart.fkProduct)" ng-src="{{cart.productImage}}" class="img-responsive">
                                            </div>
                                            <div class="td">
                                                <h3 class="title" style="cursor: pointer;" ng-click="goPage('${pageContext.request.contextPath}/'+(cart.stock > -1?'props':'course')+'/detail/'+cart.fkProduct)">{{cart.productName}}</h3>
                                                <!-- <p class="info">數位行銷</p> -->
                                                <div class="price mt10 mb10">{{(cart.unitPrice*cart.quantity)| currency : '$' : 0}}元</div>
                                                <div class="div-tb pull-right" ng-if="cart.stock > -1">
                                                    <div data-ng-init="setHasProps()" class="td td-nowarp vmid">數量</div>
                                                    <div class="td">
                                                        <select ng-change="edit(cart)" ng-model="cart.quantity" data-size="6" data-width="70px"
                                                            ng-options="n as n for n in []|range: (cart.stock>10?10:cart.stock)">
                                                            <!-- select as label for value in array track by expr -->
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </li>

                                    <!-- /list -->
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <ul class="order-list green-rim">
                                    <!-- list -->
                                    <li>
                                        <!-- <p>請輸入折價券序號</p>
                                        <div class="input-group mb20">
                                            <input type="text" class="form-control">
                                            <span class="input-group-btn">
                                                <a href="javascript:;" class="btn btn-default">兌換</a>
                                            </span>
                                        </div>
                                        <div class="div-tb">
                                            <div class="td vmid">
                                                <p class="info">總價</p>
                                            </div>
                                            <div class="td vmid rt">
                                                <p class="info">NT$ 3,300</p>
                                            </div>
                                        </div> 
                                        <hr class="mt10 mb20"> -->
                                        <div class="div-tb">
                                            <div class="td vmid">
                                                <h3 class="title">結帳金額</h3>
                                            </div>
                                            <div class="td vmid rt">
                                                <h3 class="price mt0 mb0">{{getTotalAmount() | currency : '$' : 0}}元</h3>
                                            </div>
                                        </div>
                                        <div bs-loading-overlay="order-list-block">
                                        	<a ng-if="cartList.length > 0" href="javascript:;" onclick="$('#checkoutForm').submit();return false;" class="btn btn-success btn-block mt20">確認購買-填寫付款資訊</a>
                                        	<a ng-if="cartList.length < 1" href="javascript:;" class="btn btn-success btn-block mt20 disabled">購物車無商品</a>
										</div>
                                    </li>
                                    <!-- /list -->

                                </ul>
                            </div>
                        </div>
                        <!-- row -->
                    </div>
                    <!-- container -->
                </div>
            </div>
<form:form modelAttribute="checkoutForm" 
	action="${pageContext.request.contextPath}/cart/checkout" method="post">
<input type="hidden" name="method" value="step1" /></form:form>