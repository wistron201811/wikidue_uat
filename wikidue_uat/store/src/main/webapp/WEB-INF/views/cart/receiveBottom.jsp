<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
  <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
  <script src="${pageContext.request.contextPath}/plugin/jquery-cookie/jquery.cookie.js"></script>
  <!-- <script src="js/jquery-migrate-1.2.1.min.js"></script>   -->
  <script src="${pageContext.request.contextPath}/plugin/bootstrap/bootstrap.min.js"></script>
  <script src="${pageContext.request.contextPath}/js/base.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
  <script src="${pageContext.request.contextPath}/plugin/bootstrap-select/js/bootstrap-select.min.js"></script>
  <script src="${pageContext.request.contextPath}/plugin/easy-pie-chart.min.js"></script>
  <script src="${pageContext.request.contextPath}/plugin/twzipcode/jquery.twzipcode-1.7.14.js"></script>
  <script src="${pageContext.request.contextPath}/plugin/jquery-validation/jquery.validate.min.js"></script>

  <script type="text/javascript">
  var validator = null;
    jQuery(function ($) {
      $('.easy-pie-chart.percentage').each(function () {
        var $box = $(this).closest('.infobox');
        var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : '#000');
        var trackColor = barColor == '#000' ? '#000' : '#ccc';
        var size = parseInt($(this).data('size')) || 50;
        $(this).easyPieChart({
          barColor: barColor,
          trackColor: trackColor,
          scaleColor: false,
          lineCap: 'butt',
          lineWidth: parseInt(size / 10),
          animate: /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase()) ? false : 1000,
          size: size
        });
      });
      /*
		$('#twzipcode').twzipcode({
			'countyName' : 'county', // 預設值為 county
			'districtName' : 'district', // 預設值為 district
			'zipcodeName' : 'zipCode', // 預設值為 zipcode
			'zipcodeSel' : '${checkoutForm.zipCode}' // 預設值為 zipcode
			//'zipcodeIntoDistrict':true, // 隱藏郵遞區號輸入框，並顯示於鄉鎮市區清單內。
		});

		$('#twzipcode select').addClass("btn dropdown-toggle btn-default btn-group");
		$('#twzipcode input').addClass("form-control");
		*/
     	$('#twzipcode').twzipcode({
     		"readonly": true,
			"onCountySelect":function () {
				$('.selectpicker').selectpicker('refresh');
				$('[name=district]').trigger('change');
			}
		});
    });
    

    function validateForm() {
        /* 定義 valiadation start */
        return  $("#checkoutForm").validate({
            onBlur : true,
            onChange : true,
            rules : {
    			"recipient":{
    		        required: true
    		        },
    			"telPhone":{
    		        required: true,
    		        digits: true,
    		        rangelength: [10, 10]
    		        },
    			"address":{
    		        required: true
    		        },
    			"zipCode":{
    		        required: true
    		        },
    			"check":{
    		        required: true
    		        }
    			
            },
            messages : {
    			"recipient":{
    		        required: "<span style='color:red'>請輸入收件者</span>"
    		        },
    			"telPhone":{
    		        required: "<span style='color:red'>請輸入行動電話</span>",
    		        digits: "<span style='color:red'>行動電話只能輸入數字</span>",
    		        rangelength: "<span style='color:red'>行動電話需為10碼手機號碼</span>"
    		        },
    			"address":{
    		        required: "<span style='color:red'>請輸入地址</span>"
    		        },
    			"zipCode":{
    		        required: "<span style='color:red'>請輸入郵遞區號</span>"
    		        },
    			"check":{
    		        required: "<span style='color:red'>請確認條款規定</span>"
    		        }
    			
            }
        });
        /* 定義 valiadation end */
    }
    
    function nextStep(){
    	validator = validateForm();
    	if (validator.form()) {
    		$("#checkoutForm").submit();
    	}
    }
    
    function lastStep(){
		if(validator!=null){
			validator.destroy();
		}
    	$("#method").val("goBack");
    	$("#checkoutForm").submit();
    	
    }
  </script>