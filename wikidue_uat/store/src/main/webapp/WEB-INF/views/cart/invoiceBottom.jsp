<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
  <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
  <script src="${pageContext.request.contextPath}/plugin/jquery-cookie/jquery.cookie.js"></script>
  <!-- <script src="js/jquery-migrate-1.2.1.min.js"></script>   -->
  <script src="${pageContext.request.contextPath}/plugin/bootstrap/bootstrap.min.js"></script>
  <script src="${pageContext.request.contextPath}/js/base.js?v=<spring:eval expression="@configService.applicationVersion" />"></script>
  <script src="${pageContext.request.contextPath}/plugin/bootstrap-select/js/bootstrap-select.min.js"></script>
  <script src="${pageContext.request.contextPath}/plugin/easy-pie-chart.min.js"></script>
  <script src="${pageContext.request.contextPath}/plugin/twzipcode/jquery.twzipcode-1.7.14.js"></script>
  <script src="${pageContext.request.contextPath}/plugin/jquery-validation/jquery.validate.min.js"></script>

  <script type="text/javascript">
    jQuery(function ($) {
      $('.easy-pie-chart.percentage').each(function () {
        var $box = $(this).closest('.infobox');
        var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : '#000');
        var trackColor = barColor == '#000' ? '#000' : '#ccc';
        var size = parseInt($(this).data('size')) || 50;
        $(this).easyPieChart({
          barColor: barColor,
          trackColor: trackColor,
          scaleColor: false,
          lineCap: 'butt',
          lineWidth: parseInt(size / 10),
          animate: /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase()) ? false : 1000,
          size: size
        });
      });
      /*
		$('#twzipcode').twzipcode({
			'countyName' : 'receiptCounty', // 預設值為 county
			'districtName' : 'receiptDistrict', // 預設值為 district
			'zipcodeName' : 'receiptZipCode', // 預設值為 zipcode
			'zipcodeSel' : '${checkoutForm.receiptZipCode}' // 預設值為 zipcode
			//'zipcodeIntoDistrict':true, // 隱藏郵遞區號輸入框，並顯示於鄉鎮市區清單內。
		});

		$('#twzipcode select').addClass("btn dropdown-toggle btn-default btn-group");
		$('#twzipcode input').addClass("form-control");*/
		$('#twzipcode').twzipcode({
			"readonly": true,
 			"onCountySelect":function () {
				$('.selectpicker').selectpicker('refresh');
				$('[name=receiptDistrict]').trigger('change');
			}
		});
		changeInvoiceType();
		changeDeviceType();
		
		$("input[name='invoiceType']").change(function(){
			changeInvoiceType();
		});
		$("#memberDeviceType").change(function(){
			changeDeviceType();
		});
    });
    
    function changeInvoiceType(){
        invoiceType = $("input[name='invoiceType']:checked").val();
        if(invoiceType =='1'){
        	$(".invoice1").show();
        	$(".invoice2").hide();
        	$(".invoice3").hide();
        	changeDeviceType();
        }else if(invoiceType =='2'){
        	$(".invoice1").hide();
        	$(".invoice2").show();
        	$(".invoice3").hide();
        }else if(invoiceType =='3'){
        	$(".invoice1").hide();
        	$(".invoice2").hide();
        	$(".invoice3").show();
        }
    }
    
    function changeDeviceType(){
        deviceType = $("#memberDeviceType option:selected").val();
        if(deviceType =='1'){
        	$(".device-type2").hide();
        	$(".device-type3").hide();
        }else if(deviceType =='2'){
        	$(".device-type2").show();
        	$(".device-type3").hide();
        }else if(deviceType =='3'){
        	$(".device-type2").hide();
        	$(".device-type3").show();
        }else if(deviceType =='4'){
        	$(".device-type2").hide();
        	$(".device-type3").hide();
        }
    }
    

    function validateForm() {
        /* 定義 valiadation start */
        return $("#checkoutForm").validate({
            onBlur : true,
            onChange : true,
            rules : {
    			"receiptName":{
    		        required: true
    		        },
    			"receiptTelPhone":{
    		        required: true,
    		        digits: true,
    		        rangelength: [10, 10]
    		        },
    			"receiptEmail":{
    		        required: true
    		        },
    			"receiptAddress":{
    		        required: true
    		        },
    			"receiptZipCode":{
    		        required: true
    		        },
    			"memberDeviceType":{
    		        required: true
    		        },
    			"companyDeviceType":{
    		        required: true
    		        },
    			"moicaNo":{
    		        required: true,
    		        checkMoicaNo: true
    		        },
    			"mobileCode":{
    		        required: true,
    		        checkMobileCode: true
    		        },
    			"companyName":{
    		        required: true
    		        },
    			"companyNo":{
    		        required: true,
    		        digits: true,
    		        rangelength: [8, 8]
    		        },
    			"loveCode":{
    		        required: true,
    		        digits: true,
    		        rangelength: [3, 7]
    		        },
    			"check":{
    		        required: true
    		        }
    			
            },
            messages : {
    			"receiptName":{
    		        required: "<span style='color:red'>請輸入購買人</span>"
    		        },
    			"receiptTelPhone":{
    		        required: "<span style='color:red'>請輸入行動電話</span>",
    		        digits: "<span style='color:red'>行動電話只能輸入數字</span>",
    		        rangelength: "<span style='color:red'>行動電話需為10碼手機號碼</span>"
    		        },
    			"receiptEmail":{
    		        required: "<span style='color:red'>請輸入電子信箱</span>"
    		        },
    			"receiptAddress":{
    		        required: "<span style='color:red'>請輸入地址</span>"
    		        },
    			"receiptZipCode":{
    		        required: "<span style='color:red'>請輸入郵遞區號</span>"
    		        },
       			"memberDeviceType":{
    		        required: "<span style='color:red'>請選擇電子發票載具</span>"
       		        },
       			"companyDeviceType":{
    		        required: "<span style='color:red'>請選擇電子發票載具</span>"
       		        },
       			"moicaNo":{
    		        required: "<span style='color:red'>請輸入自然人憑證號碼</span>"
       		        },
       			"mobileCode":{
    		        required: "<span style='color:red'>請輸入手機條碼</span>"
       		        },
       			"companyName":{
    		        required: "<span style='color:red'>請輸入企業名稱</span>"
       		        },
       			"companyNo":{
    		        required: "<span style='color:red'>請輸入統一編號</span>",
    		        digits: "<span style='color:red'>統一編號只能輸入數字</span>",
    		        rangelength: "<span style='color:red'>統一編號長度為8碼</span>"
       		        },
       			"loveCode":{
    		        required: "<span style='color:red'>請輸入捐贈對象</span>",
    		        rangelength: "<span style='color:red'>愛心碼長度為3~7碼</span>",
    		        digits: "<span style='color:red'>愛心碼只能輸入數字</span>"
       		        },
    			"check":{
    		        required: "<span style='color:red'>請確認條款規定</span>"
    		        }
    			
            }
        });
        /* 定義 valiadation end */
    }
    
    $.validator.addMethod("checkMoicaNo", function(value, element, params){
        return (value.match(/[A-Za-z]{2}[0-9]{14}/) != null);
     }, "<span style='color:red'>格式錯誤，請輸入2位英文字母加上14位數字</span>");
    
    $.validator.addMethod("checkMobileCode", function(value, element, params){
        return (value.match(/^\/[A-Za-z0-9\s+-]{7}$/) != null);
     }, "<span style='color:red'>格式錯誤，請輸入1位斜線『/』加上7位由數字及字母組成的手機條碼");
    
    function nextStep(){
    	if (validateForm().form()) {
    		$("#checkoutForm").submit();
    	}else{
    		if(!$("input[name='check']").prop("checked")){
    			BootstrapDialog.show({
    				title: '提示訊息',
    				type: BootstrapDialog.TYPE_WARNING,
    				message: '請先同意本網站條款規定'
    			});
    		}
    	}
    }
    
  </script>