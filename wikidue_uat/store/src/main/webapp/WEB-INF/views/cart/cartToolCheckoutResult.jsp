<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
  <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
    <%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
      <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
        <%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
          <!-- angularjs -->
          <script src="${pageContext.request.contextPath}/js/page/cart/paymentController.js?v=<spring:eval expression=" @configService.applicationVersion
            " />"></script>
          <div ng-controller="PaymentController">
            <div class="step-hold" <c:if test="${RtnCode == '1' }">
              data-ng-init="startPaymentProcessing('${MerchantTradeNo}')" bs-loading-overlay="payment-block"
              </c:if>>
              <div class="container">
                <ul class="wizard-steps">
                  <li class="active">
                    <span class="step">
                      <i class="icon-checklist"></i>s
                    </span>
                    <span class="title">確認訂單</span>
                  </li>
                  <li class="active">
                    <span class="step">
                      <i class="icon-overview-use"></i>
                    </span>
                    <span class="title">填寫付款資訊</span>
                  </li>
                  <li class="active">
                    <span class="step">
                      <i class="icon-truck"></i>
                    </span>
                    <span class="title">填寫收貨資訊</span>
                  </li>
                  <li class="active">
                    <span class="step">
                      <i class="icon-expend_financialcard"></i>
                    </span>
                    <span class="title">選擇付款方式</span>
                  </li>
                  <li class="active">
                    <span class="step">
                      <i class="icon-clipboard"></i>
                    </span>
                    <span class="title">完成</span>
                  </li>
                </ul>
              </div>
            </div>
            <div class="index-main-list for-camp">
              <div class="container">
                <div class="visible-xs mb10"></div>
                <h2 class="main-title mt0 mb20 inline-block">付款結果</h2>
                <!-- ///////////////////////////////////////////// -->
                <div class="row">
                  {{paymentResult}}
                  <c:if test="${RtnCode != '1' }">
                    <span class="color-red">付款失敗!</span>
                  </c:if>
                  <br>
                  <a href="${pageContext.request.contextPath}/order/myOrder">前往「我的訂單」</a>
                </div>
                <!-- row -->
              </div>
              <!-- container -->
            </div>
          </div>