package com.e7learning.invoice.integration;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.e7learning.common.exception.EcpayException;
import com.e7learning.invoice.integration.domain.AllowanceInvalidObj;
import com.e7learning.invoice.integration.domain.AllowanceObj;
import com.e7learning.invoice.integration.domain.CheckLoveCodeObj;
import com.e7learning.invoice.integration.domain.CheckMobileBarCodeObj;
import com.e7learning.invoice.integration.domain.DelayIssueObj;
import com.e7learning.invoice.integration.domain.InvoiceNotifyObj;
import com.e7learning.invoice.integration.domain.IssueInvalidObj;
import com.e7learning.invoice.integration.domain.IssueObj;
import com.e7learning.invoice.integration.domain.QueryAllowanceInvalidObj;
import com.e7learning.invoice.integration.domain.QueryAllowanceObj;
import com.e7learning.invoice.integration.domain.QueryIssueInvalidObj;
import com.e7learning.invoice.integration.domain.QueryIssueObj;
import com.e7learning.invoice.integration.domain.TriggerIssueObj;
import com.e7learning.invoice.integration.ecpayOperator.EcpayFunction;
import com.e7learning.invoice.integration.verification.VerifyAllowance;
import com.e7learning.invoice.integration.verification.VerifyAllowanceInvalid;
import com.e7learning.invoice.integration.verification.VerifyCheckLoveCode;
import com.e7learning.invoice.integration.verification.VerifyCheckMobileBarCode;
import com.e7learning.invoice.integration.verification.VerifyDelayIssue;
import com.e7learning.invoice.integration.verification.VerifyInvoiceNotify;
import com.e7learning.invoice.integration.verification.VerifyIssue;
import com.e7learning.invoice.integration.verification.VerifyIssueInvalid;
import com.e7learning.invoice.integration.verification.VerifyQueryAllowance;
import com.e7learning.invoice.integration.verification.VerifyQueryAllowanceInvalid;
import com.e7learning.invoice.integration.verification.VerifyQueryIssue;
import com.e7learning.invoice.integration.verification.VerifyQueryIssueInvalid;
import com.e7learning.invoice.integration.verification.VerifyTriggerIssue;
import com.google.gson.Gson;

/**
 * �ڥI�_�q�l�o�����\�����O
 * @author mark.chiu
 *
 */
@Service
public class InvoiceAllInOne {

	@Value("${operating.mode}")
	private String operatingMode;

	@Value("${invoice.hash.key}")
	private String HashKey;
	
	@Value("${invoice.hash.iv}")
	private String HashIV;

	@Value("${merchant.id}")
	private String MerchantID;
	
	private static String issueUrl;
	private static String delayIssueUrl;
	private static String triggerIssueUrl;
	private static String allowanceUrl;
	private static String issueInvalidUrl;
	private static String allowanceInvalidUrl;
	private static String queryIssueUrl;
	private static String queryAllowanceUrl;
	private static String queryIssueInvalidUrl;
	private static String queryAllowanceInvalidUrl;
	private static String invoiceNotifyUrl;
	private static String checkMobileBarCodeUrl;
	private static String checkLoveCodeUrl;
	
	
	
	private final static Logger LOG = Logger.getLogger(InvoiceAllInOne.class);

	public InvoiceAllInOne(){
		super();
	}
	
	public String issue(IssueObj obj){
		obj.setMerchantID(MerchantID);
		obj.setTimeStamp(EcpayFunction.genUnixTimeStamp());
		LOG.info("issue params: " + obj.toString());
		String result = "";
		String CheckMacValue = "";
		try{
			VerifyIssue verify = new VerifyIssue();
			issueUrl = verify.getAPIUrl(operatingMode);
			verify.verifyParams(obj);
			obj.setCustomerName(EcpayFunction.urlEncode(obj.getCustomerName()));
			obj.setCustomerAddr(EcpayFunction.urlEncode(obj.getCustomerAddr()));
			obj.setCustomerEmail(EcpayFunction.urlEncode(obj.getCustomerEmail()));
			obj.setInvoiceRemark(EcpayFunction.urlEncode(obj.getInvoiceRemark()));
			obj.setItemName(EcpayFunction.urlEncode(obj.getItemName()));
			obj.setItemWord(EcpayFunction.urlEncode(obj.getItemWord()));
			obj.setItemRemark(EcpayFunction.urlEncode(obj.getItemRemark()));
			obj.setCarruerNum(obj.getCarruerNum().replaceAll("\\+", " "));
			CheckMacValue = EcpayFunction.genCheckMacValue(HashKey, HashIV, obj);
			LOG.info("issue generate CheckMacValue: " + CheckMacValue);
			String httpValue = EcpayFunction.genHttpValue(obj, CheckMacValue);
			result = EcpayFunction.httpPost(issueUrl, httpValue, "UTF-8");
		} catch(EcpayException e){
			e.ShowExceptionMessage();
			LOG.error("e",e);
			throw new EcpayException(e.getNewExceptionMessage());
		}
		return result;
	}
	
	public String delayIssue(DelayIssueObj obj){
		obj.setMerchantID(MerchantID);
		obj.setTimeStamp(EcpayFunction.genUnixTimeStamp());
		LOG.info("delayIssue params: " + obj.toString());
		String result = "";
		String CheckMacValue = "";
		try{
			VerifyDelayIssue verify = new VerifyDelayIssue();
			delayIssueUrl = verify.getAPIUrl(operatingMode);
			verify.verifyParams(obj);
			obj.setCustomerName(EcpayFunction.urlEncode(obj.getCustomerName()));
			obj.setCustomerAddr(EcpayFunction.urlEncode(obj.getCustomerAddr()));
			obj.setCustomerEmail(EcpayFunction.urlEncode(obj.getCustomerEmail()));
			obj.setInvoiceRemark(EcpayFunction.urlEncode(obj.getInvoiceRemark()));
			obj.setItemName(EcpayFunction.urlEncode(obj.getItemName()));
			obj.setItemWord(EcpayFunction.urlEncode(obj.getItemWord()));
			obj.setCarruerNum(obj.getCarruerNum().replaceAll("\\+", " "));
			CheckMacValue = EcpayFunction.genCheckMacValue(HashKey, HashIV, obj);
			LOG.info("delayIssue generate CheckMacValue: " + CheckMacValue);
			String httpValue = EcpayFunction.genHttpValue(obj, CheckMacValue);
			LOG.info("delayIssue post String: " + httpValue);
			result = EcpayFunction.httpPost(delayIssueUrl, httpValue, "UTF-8");
		} catch(EcpayException e){
			e.ShowExceptionMessage();
			LOG.error(e.getNewExceptionMessage());
			throw new EcpayException(e.getNewExceptionMessage());
		}
		return result;
	}
	
	public String triggerIssue(TriggerIssueObj obj){
		obj.setMerchantID(MerchantID);
		obj.setTimeStamp(EcpayFunction.genUnixTimeStamp());
		LOG.info("triggerIssue params: " + obj.toString());
		String result = "";
		String CheckMacValue = "";
		try{
			VerifyTriggerIssue verify = new VerifyTriggerIssue();
			triggerIssueUrl = verify.getAPIUrl(operatingMode);
			verify.verifyParams(obj);
			CheckMacValue = EcpayFunction.genCheckMacValue(HashKey, HashIV, obj);
			LOG.info("triggerIssue generate CheckMacValue: " + CheckMacValue);
			String httpValue = EcpayFunction.genHttpValue(obj, CheckMacValue);
			LOG.info("triggerIssue post String: " + httpValue);
			result = EcpayFunction.httpPost(triggerIssueUrl, httpValue, "UTF-8");
		} catch(EcpayException e){
			e.ShowExceptionMessage();
			LOG.error(e.getNewExceptionMessage());
			throw new EcpayException(e.getNewExceptionMessage());
		}
		return result;
	}
	
	public String allowance(AllowanceObj obj){
		obj.setMerchantID(MerchantID);
		obj.setTimeStamp(EcpayFunction.genUnixTimeStamp());
		LOG.info("allowance params: " + obj.toString());
		String result = "";
		String CheckMacValue = "";
		try{
			VerifyAllowance verify = new VerifyAllowance();
			allowanceUrl = verify.getAPIUrl(operatingMode);
			verify.verifyParams(obj);
			obj.setCustomerName(EcpayFunction.urlEncode(obj.getCustomerName()));
			obj.setNotifyMail(EcpayFunction.urlEncode(obj.getNotifyMail()));
			obj.setItemName(EcpayFunction.urlEncode(obj.getItemName()));
			obj.setItemWord(EcpayFunction.urlEncode(obj.getItemWord()));
			CheckMacValue = EcpayFunction.genCheckMacValue(HashKey, HashIV, obj);
			LOG.info("allowance generate CheckMacValue: " + CheckMacValue);
			String httpValue = EcpayFunction.genHttpValue(obj, CheckMacValue);
			LOG.info("allowance post String: " + httpValue);
			result = EcpayFunction.httpPost(allowanceUrl, httpValue, "UTF-8");
		} catch(EcpayException e){
			e.ShowExceptionMessage();
			LOG.error(e.getNewExceptionMessage());
			throw new EcpayException(e.getNewExceptionMessage());
		}
		return result;
	}
	
	public String issueInvalid(IssueInvalidObj obj){
		obj.setMerchantID(MerchantID);
		obj.setTimeStamp(EcpayFunction.genUnixTimeStamp());
		LOG.info("issueInvalid params: " + obj.toString());
		String result = "";
		String CheckMacValue = "";
		try{
			VerifyIssueInvalid verify = new VerifyIssueInvalid();
			issueInvalidUrl = verify.getAPIUrl(operatingMode);
			verify.verifyParams(obj);
			obj.setReason(EcpayFunction.urlEncode(obj.getReason()));
			CheckMacValue = EcpayFunction.genCheckMacValue(HashKey, HashIV, obj);
			LOG.info("issueInvalid generate CheckMacValue: " + CheckMacValue);
			String httpValue = EcpayFunction.genHttpValue(obj, CheckMacValue);
			LOG.info("issueInvalid post String: " + httpValue);
			result = EcpayFunction.httpPost(issueInvalidUrl, httpValue, "UTF-8");
		} catch(EcpayException e){
			e.ShowExceptionMessage();
			LOG.error(e.getNewExceptionMessage());
			throw new EcpayException(e.getNewExceptionMessage());
		}
		return result;
	}
	
	public String allowanceInvalid(AllowanceInvalidObj obj){
		obj.setMerchantID(MerchantID);
		obj.setTimeStamp(EcpayFunction.genUnixTimeStamp());
		LOG.info("allowanceInvalid params: " + obj.toString());
		String result = "";
		String CheckMacValue = "";
		try{
			VerifyAllowanceInvalid verify = new VerifyAllowanceInvalid();
			allowanceInvalidUrl = verify.getAPIUrl(operatingMode);
			verify.verifyParams(obj);
			obj.setReason(EcpayFunction.urlEncode(obj.getReason()));
			CheckMacValue = EcpayFunction.genCheckMacValue(HashKey, HashIV, obj);
			LOG.info("allowanceInvalid generate CheckMacValue: " + CheckMacValue);
			String httpValue = EcpayFunction.genHttpValue(obj, CheckMacValue);
			LOG.info("allowanceInvalid post String: " + httpValue);
			result = EcpayFunction.httpPost(allowanceInvalidUrl, httpValue, "UTF-8");
		} catch(EcpayException e){
			e.ShowExceptionMessage();
			LOG.error(e.getNewExceptionMessage());
			throw new EcpayException(e.getNewExceptionMessage());
		}
		return result;
	}
	
	public String queryIssue(QueryIssueObj obj){
		obj.setMerchantID(MerchantID);
		obj.setTimeStamp(EcpayFunction.genUnixTimeStamp());
		LOG.info("queryIssue params: " + obj.toString());
		String result = "";
		String CheckMacValue = "";
		try{
			VerifyQueryIssue verify = new VerifyQueryIssue();
			queryIssueUrl = verify.getAPIUrl(operatingMode);
			verify.verifyParams(obj);
			CheckMacValue = EcpayFunction.genCheckMacValue(HashKey, HashIV, obj);
			String httpValue = EcpayFunction.genHttpValue(obj, CheckMacValue);
			LOG.info("queryIssue post String: " + httpValue);
			result = EcpayFunction.httpPost(queryIssueUrl, httpValue, "UTF-8");
		} catch(EcpayException e){
			e.ShowExceptionMessage();
			LOG.error(e.getNewExceptionMessage());
			throw new EcpayException(e.getNewExceptionMessage());
		}
		return result;
	}
	
	public String queryAllowance(QueryAllowanceObj obj){
		obj.setMerchantID(MerchantID);
		obj.setTimeStamp(EcpayFunction.genUnixTimeStamp());
		LOG.info("queryAllowance params: " + obj.toString());
		String result = "";
		String CheckMacValue = "";
		try{
			VerifyQueryAllowance verify = new VerifyQueryAllowance();
			queryAllowanceUrl = verify.getAPIUrl(operatingMode);
			verify.verifyParams(obj);
			CheckMacValue = EcpayFunction.genCheckMacValue(HashKey, HashIV, obj);
			LOG.info("queryAllowance generate CheckMacValue: " + CheckMacValue);
			String httpValue = EcpayFunction.genHttpValue(obj, CheckMacValue);
			LOG.info("queryAllowance post String: " + httpValue);
			result = EcpayFunction.httpPost(queryAllowanceUrl, httpValue, "UTF-8");
		} catch(EcpayException e){
			e.ShowExceptionMessage();
			LOG.error(e.getNewExceptionMessage());
			throw new EcpayException(e.getNewExceptionMessage());
		}
		return result;
	}
	
	public String queryIssueInvalid(QueryIssueInvalidObj obj){
		obj.setMerchantID(MerchantID);
		obj.setTimeStamp(EcpayFunction.genUnixTimeStamp());
		LOG.info("queryIssueInvalid params: " + obj.toString());
		String result = "";
		String CheckMacValue = "";
		try{
			VerifyQueryIssueInvalid verify = new VerifyQueryIssueInvalid();
			queryIssueInvalidUrl = verify.getAPIUrl(operatingMode);
			verify.verifyParams(obj);
			CheckMacValue = EcpayFunction.genCheckMacValue(HashKey, HashIV, obj);
			LOG.info("queryIssueInvalid generate CheckMacValue: " + CheckMacValue);
			String httpValue = EcpayFunction.genHttpValue(obj, CheckMacValue);
			LOG.info("queryIssueInvalid post String: " + httpValue);
			result = EcpayFunction.httpPost(queryIssueInvalidUrl, httpValue, "UTF-8");
		} catch(EcpayException e){
			e.ShowExceptionMessage();
			LOG.error(e.getNewExceptionMessage());
			throw new EcpayException(e.getNewExceptionMessage());
		}
		return result;
	}
	
	public String queryAllowanceInvalid(QueryAllowanceInvalidObj obj){
		obj.setMerchantID(MerchantID);
		obj.setTimeStamp(EcpayFunction.genUnixTimeStamp());
		LOG.info("queryAllowanceInvalid params: " + obj.toString());
		String result = "";
		String CheckMacValue = "";
		try{
			VerifyQueryAllowanceInvalid verify = new VerifyQueryAllowanceInvalid();
			queryAllowanceInvalidUrl = verify.getAPIUrl(operatingMode);
			verify.verifyParams(obj);
			CheckMacValue = EcpayFunction.genCheckMacValue(HashKey, HashIV, obj);
			LOG.info("queryAllowanceInvalid generate CheckMacValue: " + CheckMacValue);
			String httpValue = EcpayFunction.genHttpValue(obj, CheckMacValue);
			LOG.info("queryAllowanceInvalid post String: " + httpValue);
			result = EcpayFunction.httpPost(queryAllowanceInvalidUrl, httpValue, "UTF-8");
		} catch(EcpayException e){
			e.ShowExceptionMessage();
			LOG.error(e.getNewExceptionMessage());
			throw new EcpayException(e.getNewExceptionMessage());
		}
		return result;
	}
	
	public String invoiceNotify(InvoiceNotifyObj obj){
		obj.setMerchantID(MerchantID);
		obj.setTimeStamp(EcpayFunction.genUnixTimeStamp());
		LOG.info("invoiceNotify params: " + obj.toString());
		String result = "";
		String CheckMacValue = "";
		try{
			VerifyInvoiceNotify verify = new VerifyInvoiceNotify();
			invoiceNotifyUrl = verify.getAPIUrl(operatingMode);
			verify.verifyParams(obj);
			obj.setNotifyMail(EcpayFunction.urlEncode(obj.getNotifyMail()));
			CheckMacValue = EcpayFunction.genCheckMacValue(HashKey, HashIV, obj);
			LOG.info("invoiceNotify generate CheckMacValue: " + CheckMacValue);
			String httpValue = EcpayFunction.genHttpValue(obj, CheckMacValue);
			LOG.info("invoiceNotify post String: " + httpValue);
			result = EcpayFunction.httpPost(invoiceNotifyUrl, httpValue, "UTF-8");
		} catch(EcpayException e){
			e.ShowExceptionMessage();
			LOG.error(e.getNewExceptionMessage());
			throw new EcpayException(e.getNewExceptionMessage());
		}
		return result;
	}
	
	public String checkMobileBarCode(CheckMobileBarCodeObj obj){
		obj.setMerchantID(MerchantID);
		obj.setTimeStamp(EcpayFunction.genUnixTimeStamp());
		if(obj.getBarCode().contains("+")){
			String tmp = obj.getBarCode().replaceAll("\\+", " ");
			obj.setBarCode(tmp);
		}
		LOG.info("checkMobileBarCode params: " + obj.toString());
		String result = "";
		String CheckMacValue = "";
		try{
			VerifyCheckMobileBarCode verify = new VerifyCheckMobileBarCode();
			checkMobileBarCodeUrl = verify.getAPIUrl(operatingMode);
			verify.verifyParams(obj);
			CheckMacValue = EcpayFunction.genCheckMacValue(HashKey, HashIV, obj);
			LOG.info("checkMobileBarCode generate CheckMacValue: " + CheckMacValue);
			String httpValue = EcpayFunction.genHttpValue(obj, CheckMacValue);
			LOG.info("checkMobileBarCode post String: " + httpValue);
			result = EcpayFunction.httpPost(checkMobileBarCodeUrl, httpValue, "UTF-8");
		} catch(EcpayException e){
			e.ShowExceptionMessage();
			LOG.error(e.getNewExceptionMessage());
			throw new EcpayException(e.getNewExceptionMessage());
		}
		return result;
	}
	
	public String checkLoveCode(CheckLoveCodeObj obj){
		obj.setMerchantID(MerchantID);
		obj.setTimeStamp(EcpayFunction.genUnixTimeStamp());
		LOG.info("checkLoveCode params: " + obj.toString());
		String result = "";
		String CheckMacValue = "";
		try{
			VerifyCheckLoveCode verify = new VerifyCheckLoveCode();
			checkLoveCodeUrl = verify.getAPIUrl(operatingMode);
			verify.verifyParams(obj);
			CheckMacValue = EcpayFunction.genCheckMacValue(HashKey, HashIV, obj);
			LOG.info("checkLoveCode generate CheckMacValue: " + CheckMacValue);
			String httpValue = EcpayFunction.genHttpValue(obj, CheckMacValue);
			LOG.info("checkLoveCode post String: " + httpValue);
			result = EcpayFunction.httpPost(checkLoveCodeUrl, httpValue, "UTF-8");
		} catch(EcpayException e){
			e.ShowExceptionMessage();
			LOG.error(e.getNewExceptionMessage());
			throw new EcpayException(e.getNewExceptionMessage());
		}
		return result;
	}
}
