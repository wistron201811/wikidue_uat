package com.e7learning.repository.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Clarence 
 * 影片檔案資料
 */
@Entity
@Table(name = "video_data")
@NamedQuery(name = "VideoData.findAll", query = "SELECT v FROM VideoData v")
public class VideoData implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PK_VIDEO_DATA")
	private Integer pkVideoData;

	/**
	 * 課程編號
	 */
	@ManyToOne
	@JoinColumn(name = "FK_VIDEO")
	private Video video;

	/**
	 * 下載連結
	 */
	@Column(name = "HTTP")
	private String http;

	/**
	 * 影音編碼率
	 */
	@Column(name = "BIT_RATE")
	private Integer bitRate;

	/**
	 * 影片解析度
	 */
	@Column(name = "RESOLUTION")
	private String resolution;

	public Integer getPkVideoData() {
		return pkVideoData;
	}

	public void setPkVideoData(Integer pkVideoData) {
		this.pkVideoData = pkVideoData;
	}

	public Video getVideo() {
		return video;
	}

	public void setVideo(Video video) {
		this.video = video;
	}

	public String getHttp() {
		return http;
	}

	public void setHttp(String http) {
		this.http = http;
	}

	public Integer getBitRate() {
		return bitRate;
	}

	public void setBitRate(Integer bitRate) {
		this.bitRate = bitRate;
	}

	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}
	
	
}
