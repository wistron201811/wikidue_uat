package com.e7learning.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.Props;

/**
 * @author J
 *
 */
@Repository("propsRepository")
@Transactional
public interface PropsRepository extends JpaRepository<Props, Integer> ,
PagingAndSortingRepository<Props, Integer>, JpaSpecificationExecutor<Props> {

	List<Props> findTop4ByPublishSDateLessThanEqualAndPublishEDateGreaterThanEqualAndActiveOrderByPublishSDateDesc(
			Date sDate, Date eDate, String active);
	
	List<Props> findByVendorUid(String vendorUid);
	
	List<Props> findByMaterialNum(String materialNum);
	
	@Query("SELECT p FROM Props p WHERE p.materialNum = :materialNum and p.pkProps != :pkProps")
	List<Props> findByPkPropsAndMaterialNum(@Param("pkProps")Integer pkProps,@Param("materialNum")String materialNum);
	
	
}
