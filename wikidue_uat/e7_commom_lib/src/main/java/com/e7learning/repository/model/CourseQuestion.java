package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the COURSE_QUESTION database table.
 * 
 */
@Entity
@Table(name="course_question")
@NamedQuery(name="CourseQuestion.findAll", query="SELECT c FROM CourseQuestion c")
public class CourseQuestion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="PK_COURSE_QUESTION")
	private Integer pkCourseQuestion;

	private String active;

	@Column(name="CREATE_DT")
	private Date createDt;

	@Column(name="CREATE_ID")
	private String createId;

	@Column(name="LAST_MODIFIED_ID")
	private String lastModifiedId;

	private String topic;
	
	private String content;

	@Column(name="USER_NAME")
	private String userName;
	
	@Column(name="UPDATE_DT")
	private Date updateDt;

	//bi-directional many-to-one association to Product
	@ManyToOne
	@JoinColumn(name="FK_COURSE")
	private Course course;

	//bi-directional many-to-one association to QuestionReply
	@OneToMany(mappedBy="courseQuestion")
	private List<QuestionReply> questionReplies;

	public CourseQuestion() {
	}

	public Integer getPkCourseQuestion() {
		return this.pkCourseQuestion;
	}

	public void setPkCourseQuestion(Integer pkCourseQuestion) {
		this.pkCourseQuestion = pkCourseQuestion;
	}

	public String getActive() {
		return this.active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Date getCreateDt() {
		return this.createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return this.createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public String getLastModifiedId() {
		return this.lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}

	public String getTopic() {
		return this.topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public Date getUpdateDt() {
		return this.updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public List<QuestionReply> getQuestionReplies() {
		return this.questionReplies;
	}

	public void setQuestionReplies(List<QuestionReply> questionReplies) {
		this.questionReplies = questionReplies;
	}

	public QuestionReply addQuestionReply(QuestionReply questionReply) {
		getQuestionReplies().add(questionReply);
		questionReply.setCourseQuestion(this);

		return questionReply;
	}

	public QuestionReply removeQuestionReply(QuestionReply questionReply) {
		getQuestionReplies().remove(questionReply);
		questionReply.setCourseQuestion(null);

		return questionReply;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	

	
}