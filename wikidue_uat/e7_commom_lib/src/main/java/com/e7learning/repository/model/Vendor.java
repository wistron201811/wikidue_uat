package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the Vendor database table.
 * 
 */
@Entity
@Table(name="vendor")
@NamedQuery(name="Vendor.findAll", query="SELECT v FROM Vendor v")
public class Vendor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="UID")
	private String uid;

	@Column(name="CREATE_DT")
	private Date createDt;

	@Column(name="CREATE_ID")
	private String createId;

	@Column(name="VENDOR_NAME")
	private String vendorName;

	@Column(name="VENDOR_CODE")
	private String vendorCode;
	
	@Column(name="VENDOR_TYPE")
	private String vendorType;

	@Column(name="AREA_CODE")
	private String areaCode;

	@Column(name="TELPHONE")
	private String telphone;
	
	@Column(name="EXT")
	private String ext;
	
	@Column(name="MOBILE")
	private String mobile;

	@Column(name="ZIPCODE")
	private String zipcode;
	
	@Column(name="COUNTY")
	private String county;
	
	@Column(name="DISTRICT")
	private String district;
	
	@Column(name="ADDRESS")
	private String address;
	
	@Column(name="TAX_ID")
	private String taxId;

	@Column(name="email")
	private String email;
	
	@Column(name="COMPANY_PHONE")
	private String companyPhone;
	
	@Column(name="UPDATE_DT")
	private Date updateDt;

	@Column(name = "VENDOR_INTRODUCTION")
	private String vendorIntroduction;
	
	@Column(name="PHOTO_ID")
	private String photoId;
	
	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;
	
	@Column(name="FAX_NO")
	private String faxNo;
	
	@Column(name="CONTACT")
	private String contact;
	
	@Column(name="HOMEPAGE")
	private String homepage;
	
	@Column(name="REMARK")
	private String remark;
	
	@Column(name = "CONTRACT_S_DATE")
	private Date contractSDate;

	@Column(name = "CONTRACT_E_DATE")
	private Date contractEDate;
	
	@Column(name = "ACCOUNT")
	private String account;
	
	@Column(name = "BANK_CODE")
	private String bankCode;
	
	@Column(name = "REPRESENTATIVE")
	private String representative;
	
	@Column(name="AREA_CODE1")
	private String areaCode1;

	@Column(name="EXT1")
	private String ext1;
	
	@Column(name="PROPS_SPLIT_RATIO")
	private Integer propsSplitRatio;
	
	@Column(name="COURSE_SPLIT_RATIO")
	private Integer courseSplitRatio;
	
	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}
	
	public Vendor() {
	}

	public String getUid() {
		return uid;
	}
	
	public void setUid(String uid) {
		this.uid = uid;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getVendorType() {
		return vendorType;
	}

	public void setVendorType(String vendorType) {
		this.vendorType = vendorType;
	}

	public String getTelphone() {
		return telphone;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public String getVendorIntroduction() {
		return vendorIntroduction;
	}

	public void setVendorIntroduction(String vendorIntroduction) {
		this.vendorIntroduction = vendorIntroduction;
	}

	public String getPhotoId() {
		return photoId;
	}

	public void setPhotoId(String photoId) {
		this.photoId = photoId;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getFaxNo() {
		return faxNo;
	}

	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getHomepage() {
		return homepage;
	}

	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getContractSDate() {
		return contractSDate;
	}

	public void setContractSDate(Date contractSDate) {
		this.contractSDate = contractSDate;
	}

	public Date getContractEDate() {
		return contractEDate;
	}

	public void setContractEDate(Date contractEDate) {
		this.contractEDate = contractEDate;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getRepresentative() {
		return representative;
	}

	public void setRepresentative(String representative) {
		this.representative = representative;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

	public String getAreaCode1() {
		return areaCode1;
	}

	public void setAreaCode1(String areaCode1) {
		this.areaCode1 = areaCode1;
	}

	public String getExt1() {
		return ext1;
	}

	public void setExt1(String ext1) {
		this.ext1 = ext1;
	}

	public Integer getPropsSplitRatio() {
		return propsSplitRatio;
	}

	public void setPropsSplitRatio(Integer propsSplitRatio) {
		this.propsSplitRatio = propsSplitRatio;
	}

	public Integer getCourseSplitRatio() {
		return courseSplitRatio;
	}

	public void setCourseSplitRatio(Integer courseSplitRatio) {
		this.courseSplitRatio = courseSplitRatio;
	}
	
}