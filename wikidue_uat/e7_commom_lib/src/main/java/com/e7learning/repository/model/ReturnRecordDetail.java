package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "return_record_detail")
@NamedQuery(name = "ReturnRecordDetail.findAll", query = "SELECT r FROM ReturnRecordDetail r")
public class ReturnRecordDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PK_RETURN_RECORD_DETAIL")
	private Integer pkReturnRecordDetail;
	
	@ManyToOne
	@JoinColumn(name = "FK_RETURN_RECORD")
	private ReturnRecord returnRecord;

	@OneToOne
	@JoinColumn(name = "FK_ORDER_DETAIL")
	private SubOrderDetail orderDetail;
	
	@Column(name = "RETURN_QUANTITY")
	private Integer returnQuantity;

	@Column(name = "CREATE_DT")
	private Date createDt;

	@Column(name = "CREATE_ID")
	private String createId;

//	@Column(name = "UPDATE_DT")
//	private Date updateDt;
	
	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;

	@Column(name = "ACTIVE")
	private String active;

	public Integer getPkReturnRecordDetail() {
		return pkReturnRecordDetail;
	}

	public void setPkReturnRecordDetail(Integer pkReturnRecordDetail) {
		this.pkReturnRecordDetail = pkReturnRecordDetail;
	}

	public ReturnRecord getReturnRecord() {
		return returnRecord;
	}

	public void setReturnRecord(ReturnRecord returnRecord) {
		this.returnRecord = returnRecord;
	}

	public SubOrderDetail getOrderDetail() {
		return orderDetail;
	}

	public void setOrderDetail(SubOrderDetail orderDetail) {
		this.orderDetail = orderDetail;
	}

	public Integer getReturnQuantity() {
		return returnQuantity;
	}

	public void setReturnQuantity(Integer returnQuantity) {
		this.returnQuantity = returnQuantity;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

//	public Date getUpdateDt() {
//		return updateDt;
//	}

//	public void setUpdateDt(Date updateDt) {
//		this.updateDt = updateDt;
//	}

	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

}
