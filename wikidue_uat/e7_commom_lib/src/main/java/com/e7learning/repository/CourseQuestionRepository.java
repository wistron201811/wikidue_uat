package com.e7learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.CourseQuestion;

@Repository
@Transactional
public interface CourseQuestionRepository extends JpaRepository<CourseQuestion, Integer>,
		PagingAndSortingRepository<CourseQuestion, Integer>, JpaSpecificationExecutor<CourseQuestion> {

}
