package com.e7learning.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.Camp;

/**
 * @author J
 *
 */
@Repository("campRepository")
@Transactional
public interface CampRepository extends JpaRepository<Camp, Integer> ,
PagingAndSortingRepository<Camp, Integer>, JpaSpecificationExecutor<Camp> {

	@Query("SELECT c FROM Camp c WHERE c.category.pkCategory = :fkCategory")
	List<Camp> findByFkCategory(@Param("fkCategory")Integer fkCategory);
}
