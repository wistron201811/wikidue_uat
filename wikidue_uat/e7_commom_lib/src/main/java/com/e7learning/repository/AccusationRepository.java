package com.e7learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.e7learning.repository.model.Accusation;

/**
 * @author J
 *
 */
public interface AccusationRepository extends JpaRepository<Accusation, Integer>,
		PagingAndSortingRepository<Accusation, Integer>, JpaSpecificationExecutor<Accusation> {

	long countByShareCenterPkShareAndApproveStatusIsNull(Integer pkShare);
}
