package com.e7learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.e7learning.repository.model.ShareTag;

/**
 * @author ChrisTsai
 *
 */
public interface ShareTagRepository extends JpaRepository<ShareTag, Integer>,
		PagingAndSortingRepository<ShareTag, Integer>, JpaSpecificationExecutor<ShareTag> {

	void deleteByShareCenterPkShare(Integer pkShare);
}
