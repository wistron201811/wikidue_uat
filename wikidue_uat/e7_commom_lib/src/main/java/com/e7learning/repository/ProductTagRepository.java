package com.e7learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.ProductTag;

/**
 * @author ChrisTsai
 *
 */
@Repository("productTagRepository")
@Transactional
public interface ProductTagRepository extends JpaRepository<ProductTag, Integer> ,
PagingAndSortingRepository<ProductTag, Integer>, JpaSpecificationExecutor<ProductTag> {
	
	@Modifying
    @Transactional
	@Query("DELETE FROM ProductTag p WHERE p.product.pkProduct = :pkProduct")
	void deleteByPkProduct(@Param("pkProduct")String pkProduct);
	
}
