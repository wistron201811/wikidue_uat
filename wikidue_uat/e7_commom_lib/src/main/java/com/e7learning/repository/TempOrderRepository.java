package com.e7learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.e7learning.repository.model.TempOrder;

/**
 * @author Clarence
 *
 */
@Repository
public interface TempOrderRepository extends JpaRepository<TempOrder, Integer> ,
PagingAndSortingRepository<TempOrder, Integer>, JpaSpecificationExecutor<TempOrder> {

}
