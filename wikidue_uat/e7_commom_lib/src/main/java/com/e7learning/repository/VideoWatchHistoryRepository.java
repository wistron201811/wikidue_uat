package com.e7learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.e7learning.repository.model.VideoWatchHistory;

/**
 * @author ChrisTsai
 *
 */
public interface VideoWatchHistoryRepository extends JpaRepository<VideoWatchHistory, Integer>,
		PagingAndSortingRepository<VideoWatchHistory, Integer>, JpaSpecificationExecutor<VideoWatchHistory> {

}
