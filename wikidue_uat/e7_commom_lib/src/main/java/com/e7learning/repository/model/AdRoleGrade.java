package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the AD_ROLE_GRADE database table.
 * 
 */
@Entity
@Table(name="ad_role_grade")
@NamedQuery(name="AdRoleGrade.findAll", query="SELECT a FROM AdRoleGrade a")
public class AdRoleGrade implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PK_AD_ROLE_GRADE")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int pkAdRoleGrade;

	private String active;

	@ManyToOne
	@JoinColumn(name = "AD_ID")
	private Advertisement ad;

	@Column(name="CREATE_DT")
	private Date createDt;

	@Column(name="CREATE_ID")
	private String createId;

	@Column(name="ROLE_GRADE_CODE")
	private String roleGradeCode;

	@Column(name="UPDATE_DT")
	private Date updateDt;

	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;
	
	
	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}
	
	public AdRoleGrade() {
	}


	public int getPkAdRoleGrade() {
		return this.pkAdRoleGrade;
	}

	public void setPkAdRoleGrade(int pkAdRoleGrade) {
		this.pkAdRoleGrade = pkAdRoleGrade;
	}

	public String getActive() {
		return this.active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Advertisement getAd() {
		return ad;
	}

	public void setAd(Advertisement ad) {
		this.ad = ad;
	}

	public Date getCreateDt() {
		return this.createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return this.createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public String getRoleGradeCode() {
		return this.roleGradeCode;
	}

	public void setRoleGradeCode(String roleGradeCode) {
		this.roleGradeCode = roleGradeCode;
	}

	public Date getUpdateDt() {
		return this.updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

}