package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Clarence
 * 影片廣告
 */
@Entity
@Table(name = "video_ad")
@NamedQuery(name = "VideoAd.findAll", query = "SELECT v FROM VideoAd v")
public class VideoAd implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 影片廣告編號
	 */
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="PK_VIDEO_AD")
	private Integer pkVideoAd;

	/**
	 * 隨機數字
	 */
	@Column(name = "RANDOM")
	private Integer random;
	
	/**
	 * 圖片編號
	 */
	@Column(name = "IMAGE_ID")
	private String imageId;
	
	/**
	 * 影片連結  https://www.youtube.com/watch?v=xxxxxxx
	 * xxxxxxx
	 */
	@Column(name = "VIDEO_URL")
	private String videoUrl;
	
	/**
	 * 影片廣告標題
	 */
	@Column(name = "TITLE")
	private String title;
	
	/**
	 * 影片廣告介紹
	 */
	@Column(name = "BRIEF")
	private String brief;

	/**
	 * 影片網址
	 */
	@Column(name = "CONTENT_URL")
	private String contentUrl;

	/**
	 * 新增時間
	 */
	@Column(name = "CREATE_DT")
	private Date createDt;

	/**
	 * 建立人員
	 */
	@Column(name = "CREATE_ID")
	private String createId;

	/**
	 * 修改時間
	 */
	@Column(name = "UPDATE_DT")
	private Date updateDt;

	/**
	 * 是否可用
	 */
	@Column(name = "ACTIVE", columnDefinition = "CHAR(1) default '1'")
	private String active;

	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;
	
	
	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}
	
	public Integer getPkVideoAd() {
		return pkVideoAd;
	}

	public void setPkVideoAd(Integer pkVideoAd) {
		this.pkVideoAd = pkVideoAd;
	}

	public Integer getRandom() {
		return random;
	}

	public void setRandom(Integer random) {
		this.random = random;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public String getVideoUrl() {
		return videoUrl;
	}

	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBrief() {
		return brief;
	}

	public void setBrief(String brief) {
		this.brief = brief;
	}

	public String getContentUrl() {
		return contentUrl;
	}

	public void setContentUrl(String contentUrl) {
		this.contentUrl = contentUrl;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public Date getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

}
