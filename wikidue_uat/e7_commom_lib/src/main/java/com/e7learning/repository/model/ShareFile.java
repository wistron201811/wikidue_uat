package com.e7learning.repository.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "share_file")
public class ShareFile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PK_SHARE_FILE")
	private String pkShareFile;

	@ManyToOne
	@JoinColumn(name = "FK_SHARE")
	private ShareCenter shareCenter;

	@Column(name = "FILE_ID")
	private String fileId;
	
	public String getPkShareFile() {
		return pkShareFile;
	}

	public void setPkShareFile(String pkShareFile) {
		this.pkShareFile = pkShareFile;
	}

	public ShareCenter getShareCenter() {
		return shareCenter;
	}

	public void setShareCenter(ShareCenter shareCenter) {
		this.shareCenter = shareCenter;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

}
