
package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ORDER_DETAIL database table.
 * 
 */
@Entity
@Table(name = "sub_order_detail")
@NamedQuery(name = "SubOrderDetail.findAll", query = "SELECT o FROM SubOrderDetail o")
public class SubOrderDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PK_SUB_ORDER_DETAIL")
	private Integer pkSubOrderDetail;

	@ManyToOne
	@JoinColumn(name = "FK_SUB_ORDER")
	private SubOrder subOrder;

	@ManyToOne
	@JoinColumn(name = "FK_PRODUCT")
	private Product product;

	@Column(name = "PRODUCT_NAME")
	private String productName;

	@Column(name = "QUANTITY")
	private Integer quantity;

	@Column(name = "UNIT_PRICE")
	private Integer unitPrice;

	@Column(name = "TOTAL_AMOUNT")
	private Integer totalAmount;

	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "RETURN_REASON")
	private String returnReason;
	
	@Column(name = "CREATE_DT")
	private Date createDt;

	@Column(name = "CREATE_ID")
	private String createId;

	@Column(name = "UPDATE_DT")
	private Date updateDt;

	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;
	
	@Column(name = "ADMIN_REMARK")
	private String adminRemark;

	@Column(name = "USER_REMARK")
	private String userRemark;
	
	@Column(name = "VENDOR_REMARK")
	private String vendorRemark;
	
	@Column(name="PROPS_SPLIT_RATIO")
	private Integer propsSplitRatio;
	
	@Column(name="COURSE_SPLIT_RATIO")
	private Integer courseSplitRatio;
	
	@Column(name = "DELIVERY_DT")
	private Date deliveryDt;
	
	@Column(name = "REFUND_DT")
	private Date refundDt;
	
	public SubOrderDetail() {
	}

	public Integer getPkSubOrderDetail() {
		return pkSubOrderDetail;
	}

	public void setPkSubOrderDetail(Integer pkSubOrderDetail) {
		this.pkSubOrderDetail = pkSubOrderDetail;
	}

	public SubOrder getSubOrder() {
		return subOrder;
	}

	public void setSubOrder(SubOrder subOrder) {
		this.subOrder = subOrder;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Integer unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Integer getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReturnReason() {
		return returnReason;
	}

	public void setReturnReason(String returnReason) {
		this.returnReason = returnReason;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public Date getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}

	public String getAdminRemark() {
		return adminRemark;
	}

	public void setAdminRemark(String adminRemark) {
		this.adminRemark = adminRemark;
	}

	public String getUserRemark() {
		return userRemark;
	}

	public void setUserRemark(String userRemark) {
		this.userRemark = userRemark;
	}

	public String getVendorRemark() {
		return vendorRemark;
	}

	public void setVendorRemark(String vendorRemark) {
		this.vendorRemark = vendorRemark;
	}
	
	public Integer getPropsSplitRatio() {
		return propsSplitRatio;
	}

	public void setPropsSplitRatio(Integer propsSplitRatio) {
		this.propsSplitRatio = propsSplitRatio;
	}

	public Integer getCourseSplitRatio() {
		return courseSplitRatio;
	}

	public void setCourseSplitRatio(Integer courseSplitRatio) {
		this.courseSplitRatio = courseSplitRatio;
	}

	public Date getDeliveryDt() {
		return deliveryDt;
	}

	public void setDeliveryDt(Date deliveryDt) {
		this.deliveryDt = deliveryDt;
	}

	public Date getRefundDt() {
		return refundDt;
	}

	public void setRefundDt(Date refundDt) {
		this.refundDt = refundDt;
	}
	
	
}