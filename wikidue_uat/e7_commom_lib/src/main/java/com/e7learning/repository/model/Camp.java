package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

/**
 * The persistent class for the CAMP database table.
 * 
 */
@Entity
@Table(name = "camp")
@NamedQuery(name = "Camp.findAll", query = "SELECT c FROM Camp c")
@DynamicInsert
@DynamicUpdate
public class Camp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PK_CAMP")
	private Integer pkCamp;

	@Column(name = "ACTIVE")
	private String active;

	@Column(name = "CREATE_DT")
	private Date createDt;

	@Column(name = "CREATE_ID")
	private String createId;

	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;

	@Column(name = "UPDATE_DT")
	private Date updateDt;

	@Column(name = "CAMP_NUM")
	private String campNum;

	@Column(name = "DISCOUNT_PRICE")
	private Integer discountPrice;

	@Column(name = "CAMP_NAME")
	private String campName;

	@Column(name = "PRICE")
	private Integer price;

	@Column(name = "COST_PRICE")
	private Integer costPrice;

	@Column(name = "ACTIVITY_S_DATE")
	private Date activitySDate;

	@Column(name = "ACTIVITY_E_DATE")
	private Date activityEDate;

	@Column(name = "SIGN_UP_START_DATE")
	private Date signUpStartDate;
	
	@Column(name = "SIGN_UP_END_DATE")
	private Date signUpEndDate;

	@Column(name = "ATTENDANT_TARGET")
	private String attendantTarget;

	@Column(name = "ORGANIZER")
	private String organizer;

	@Column(name = "IMAGE_ID")
	private String imageId;

	@Column(name = "ACTIVITY_COUNTY")
	private String activityCounty;

	@Column(name = "EXTERNAL_LINK")
	private String externalLink;

	// bi-directional many-to-one association to Category
	@OneToOne
	@JoinColumn(name = "FK_CATEGORY")
	private Category category;

	@Column(name = "CAMP_INTRODUCTION")
	private String campIntroduction;
	
	public Camp() {
	}

	public Integer getPkCamp() {
		return this.pkCamp;
	}

	public void setPkCamp(Integer pkCamp) {
		this.pkCamp = pkCamp;
	}

	public String getActive() {
		return this.active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Date getCreateDt() {
		return this.createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return this.createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public String getLastModifiedId() {
		return this.lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}

	public Date getUpdateDt() {
		return this.updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public String getCampNum() {
		return campNum;
	}

	public void setCampNum(String campNum) {
		this.campNum = campNum;
	}

	public Integer getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Integer discountPrice) {
		this.discountPrice = discountPrice;
	}

	public String getCampName() {
		return campName;
	}

	public void setCampName(String campName) {
		this.campName = campName;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(Integer costPrice) {
		this.costPrice = costPrice;
	}

	public Date getActivitySDate() {
		return activitySDate;
	}

	public void setActivitySDate(Date activitySDate) {
		this.activitySDate = activitySDate;
	}

	public Date getActivityEDate() {
		return activityEDate;
	}

	public void setActivityEDate(Date activityEDate) {
		this.activityEDate = activityEDate;
	}

	public Date getSignUpEndDate() {
		return signUpEndDate;
	}

	public void setSignUpEndDate(Date signUpEndDate) {
		this.signUpEndDate = signUpEndDate;
	}

	public String getAttendantTarget() {
		return attendantTarget;
	}

	public void setAttendantTarget(String attendantTarget) {
		this.attendantTarget = attendantTarget;
	}

	public String getOrganizer() {
		return organizer;
	}

	public void setOrganizer(String organizer) {
		this.organizer = organizer;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public String getActivityCounty() {
		return activityCounty;
	}

	public void setActivityCounty(String activityCounty) {
		this.activityCounty = activityCounty;
	}

	public String getExternalLink() {
		return externalLink;
	}

	public void setExternalLink(String externalLink) {
		this.externalLink = externalLink;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getCampIntroduction() {
		return campIntroduction;
	}

	public void setCampIntroduction(String campIntroduction) {
		this.campIntroduction = campIntroduction;
	}

	public Date getSignUpStartDate() {
		return signUpStartDate;
	}

	public void setSignUpStartDate(Date signUpStartDate) {
		this.signUpStartDate = signUpStartDate;
	}
	

}