package com.e7learning.repository.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Clarence 
 * 影片標籤
 */
@Entity
@Table(name = "video_tag")
@NamedQuery(name = "VideoTag.findAll", query = "SELECT v FROM VideoTag v")
public class VideoTag implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PK_VIDEO_TAG")
	private Integer pkVideoTag;

	/**
	 * 課程編號
	 */
	@ManyToOne
	@JoinColumn(name = "FK_VIDEO")
	private Video video;

	/**
	 * 標籤名稱下載連結
	 */
	@Column(name = "TAG_NAME")
	private String tagName;

	public Integer getPkVideoTag() {
		return pkVideoTag;
	}

	public void setPkVideoTag(Integer pkVideoTag) {
		this.pkVideoTag = pkVideoTag;
	}

	public Video getVideo() {
		return video;
	}

	public void setVideo(Video video) {
		this.video = video;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	
}
