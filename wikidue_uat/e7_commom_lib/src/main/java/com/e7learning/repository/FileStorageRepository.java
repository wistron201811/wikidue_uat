package com.e7learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.e7learning.repository.model.FileStorage;

public interface FileStorageRepository extends JpaRepository<FileStorage, String>,
		PagingAndSortingRepository<FileStorage, String>, JpaSpecificationExecutor<FileStorage> {
	
}