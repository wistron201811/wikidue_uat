package com.e7learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.e7learning.repository.model.ShareCenter;

/**
 * @author J
 *
 */
public interface ShareCenterRepository extends JpaRepository<ShareCenter, Integer>,
		PagingAndSortingRepository<ShareCenter, Integer>, JpaSpecificationExecutor<ShareCenter> {

}
