package com.e7learning.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.Vendor;

/**
 * @author J
 *
 */
@Repository("vendorRepository")
@Transactional
public interface VendorRepository extends JpaRepository<Vendor, String> ,
PagingAndSortingRepository<Vendor, String>, JpaSpecificationExecutor<Vendor> {

	@Query(value="SELECT v FROM Vendor v order by v.taxId desc")
	List<Vendor> findAllOrderByTaxId();
	
	@Query(value="SELECT count(v) FROM Vendor v where v.taxId = :taxId")
	Long countByTaxId(@Param("taxId") String taxId);
	
	@Query(value="SELECT count(v) FROM Vendor v where v.taxId = :taxId and v.uid != :uid")
	Long countByTaxIdWithPk(@Param("taxId") String taxId, @Param("uid") String uid);
	
	@Query(value="SELECT count(v) FROM Vendor v where v.vendorCode = :vendorCode")
	Long countByVendorCode(@Param("vendorCode") String vendorCode);
	
	@Query(value="SELECT count(v) FROM Vendor v where v.vendorCode = :vendorCode and v.uid != :uid")
	Long countByVendorCodeWithPk(@Param("vendorCode") String vendorCode, @Param("uid") String uid);
}
