package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "payment")
@NamedQuery(name = "Payment.findAll", query = "SELECT p FROM Payment p")
public class Payment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PK_PAYMENT")
	private String pkPayment;

	@Column(name = "PAYMENT_DT")
	private Date paymentDt;

	@Column(name = "CREATE_DT")
	private Date createDt;

	@Column(name = "ACTIVE")
	private String active;

	@Column(name = "UPDATE_DT")
	private Date updateDt;

	@Column(name = "CREATE_ID")
	private String createId;

	@Column(name = "TRADE_NO")
	private String tradeNo;

	@Column(name = "CHECK_MAC_VALUE")
	private String checkMacValue;

	@Column(name = "TRADE_AMT")
	private Integer tradeAmt;

	@Column(name = "PAYMENT_TYPE")
	private String paymentType;

	@Column(name = "CHARGE_FEE")
	private Integer chargeFee;

	@Column(name = "RETURN_MESSAGE")
	private String returnMessage;

	@Column(name = "RETURN_CODE")
	private Integer returnCode;
	
	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "MAIN_ORDER_NO")
	private String mainOrderNo;
	
	@Column(name = "CURRENT_AMT")
	private Integer currentAmt;
	
	@Column(name = "INVOICE_NO")
	private String invoiceNo;
	
	@Column(name = "INVOICE_RANDOM_NO")
	private String invoiceRandomNo;
	
	@OneToOne(mappedBy = "payment")
	private MainOrder mainOrder;

	/*
	 * 發票類型:
	 * 1:個人-會員
	 * 2:個人-自然人憑證
	 * 3:個人-手機條碼
	 * 4:個人-索取發票
	 * 5:捐贈
	 * 6:企業
	 * 
	 * */
	@Column(name = "INVOICE_TYPE")
	private String invoiceType;
	/*
	 * 自然人憑證號碼
	 * 
	 * */
	@Column(name = "MOICA_NO")
	private String moicaNo;
	
	/*
	 * 手機條碼
	 * 
	 * */
	@Column(name = "MOBILE_CODE")
	private String mobileCode;
	
	/*
	 * 愛心捐贈碼
	 * 
	 * */
	@Column(name = "LOVE_CODE")
	private String loveCode;
	
	/*
	 * 公司統一編號
	 * 
	 * */
	@Column(name = "COMPANY_NO")
	private String companyNo;
	
	/*
	 * 公司統一編號
	 * 
	 * */
	@Column(name = "COMPANY_NAME")
	private String companyName;
	
	/*
	 * 電子發票-自訂編碼
	 * 
	 * */
	@Column(name = "RELATE_NUMBER")
	private String relateNumber;
	

	public String getPkPayment() {
		return pkPayment;
	}

	public void setPkPayment(String pkPayment) {
		this.pkPayment = pkPayment;
	}

	public Date getPaymentDt() {
		return paymentDt;
	}

	public void setPaymentDt(Date paymentDt) {
		this.paymentDt = paymentDt;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Date getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public String getTradeNo() {
		return tradeNo;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}

	public String getCheckMacValue() {
		return checkMacValue;
	}

	public void setCheckMacValue(String checkMacValue) {
		this.checkMacValue = checkMacValue;
	}

	public Integer getTradeAmt() {
		return tradeAmt;
	}

	public void setTradeAmt(Integer tradeAmt) {
		this.tradeAmt = tradeAmt;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public Integer getChargeFee() {
		return chargeFee;
	}

	public void setChargeFee(Integer chargeFee) {
		this.chargeFee = chargeFee;
	}

	public String getReturnMessage() {
		return returnMessage;
	}

	public void setReturnMessage(String returnMessage) {
		this.returnMessage = returnMessage;
	}

	public Integer getReturnCode() {
		return returnCode;
	}

	public void setReturnCode(Integer returnCode) {
		this.returnCode = returnCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMainOrderNo() {
		return mainOrderNo;
	}

	public void setMainOrderNo(String mainOrderNo) {
		this.mainOrderNo = mainOrderNo;
	}

	public Integer getCurrentAmt() {
		return currentAmt;
	}

	public void setCurrentAmt(Integer currentAmt) {
		this.currentAmt = currentAmt;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getInvoiceRandomNo() {
		return invoiceRandomNo;
	}

	public void setInvoiceRandomNo(String invoiceRandomNo) {
		this.invoiceRandomNo = invoiceRandomNo;
	}

	public String getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}

	public String getMoicaNo() {
		return moicaNo;
	}

	public void setMoicaNo(String moicaNo) {
		this.moicaNo = moicaNo;
	}

	public String getMobileCode() {
		return mobileCode;
	}

	public void setMobileCode(String mobileCode) {
		this.mobileCode = mobileCode;
	}

	public String getLoveCode() {
		return loveCode;
	}

	public void setLoveCode(String loveCode) {
		this.loveCode = loveCode;
	}

	public String getCompanyNo() {
		return companyNo;
	}

	public void setCompanyNo(String companyNo) {
		this.companyNo = companyNo;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public MainOrder getMainOrder() {
		return mainOrder;
	}

	public void setMainOrder(MainOrder mainOrder) {
		this.mainOrder = mainOrder;
	}

	public String getRelateNumber() {
		return relateNumber;
	}

	public void setRelateNumber(String relateNumber) {
		this.relateNumber = relateNumber;
	}

}
