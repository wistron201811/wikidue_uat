package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the KNOWLEDGE_MAP database table.
 * 
 */
@Entity
@Table(name = "knowledge_map")
@NamedQuery(name="KnowledgeMap.findAll", query="SELECT k FROM KnowledgeMap k")
public class KnowledgeMap implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PK_KNOWLEDGE_MAP")
	private String pkKnowledgeMap;

	@Column(name="TAG_NAME")
	private String tagName;
	
	@Column(name="TAG_LEVEL")
	private String tagLevel;

	@Column(name="CREATE_DT")
	private Date createDt;

	@Column(name="CREATE_ID")
	private String createId;

	@Column(name="UPDATE_DT")
	private Date updateDt;

	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;

	
	public String getPkKnowledgeMap() {
		return pkKnowledgeMap;
	}

	public void setPkKnowledgeMap(String pkKnowledgeMap) {
		this.pkKnowledgeMap = pkKnowledgeMap;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public String getTagLevel() {
		return tagLevel;
	}

	public void setTagLevel(String tagLevel) {
		this.tagLevel = tagLevel;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public Date getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}

}