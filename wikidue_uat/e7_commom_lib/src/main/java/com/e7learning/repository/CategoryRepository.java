package com.e7learning.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.Category;

/**
 * @author J
 *
 */
@Repository("categoryRepository")
@Transactional
public interface CategoryRepository extends JpaRepository<Category, Integer>,
		PagingAndSortingRepository<Category, Integer>, JpaSpecificationExecutor<Category> {

	public Category findByPkCategory(Integer pkCategory);

	public List<Category> findByCategoryTypeAndActiveOrderByCreateDtDesc(String active, String categoryType);
	
	public List<Category> findByCategoryTypeOrderByCreateDtDesc(String categoryType);

	public List<Category> findByCategoryTypeOrderByCategoryCode(String categoryType);

}
