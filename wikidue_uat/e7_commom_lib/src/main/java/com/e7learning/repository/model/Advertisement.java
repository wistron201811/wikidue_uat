package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the ADVERTISEMENT database table.
 * 
 */
@Entity
@Table(name = "advertisement")
@NamedQuery(name = "Advertisement.findAll", query = "SELECT a FROM Advertisement a")
public class Advertisement implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PK_ADVERTISEMENT")
	private Integer pkAdvertisement;

	private String active;

	@Column(name = "ADS_NAME")
	private String adsName;

	@Column(name = "CREATE_DT")
	private Date createDt;

	@Column(name = "CREATE_ID")
	private String createId;

	@Column(name = "EXTERNAL_LINK")
	private String externalLink;
	
	@ManyToOne
	@JoinColumn(name = "AD_CATEGORY_ID")
	private AdCategory category;
	
	@OneToMany(mappedBy="ad")
	private List<AdRoleGrade> roleGrades;

	@Column(name = "IMAGE_ONE_ID")
	private String imageOneId;

	@Column(name = "IMAGE_TWO_ID")
	private String imageTwoId;

	private String notice;

	@Column(name = "PUBLISH_E_DATE")
	private Date publishEDate;

	@Column(name = "PUBLISH_S_DATE")
	private Date publishSDate;

	@Column(name = "UPDATE_DT")
	private Date updateDt;

	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;
	
	/** 廣告編號 */
	@Column(name = "BANNER_CODE")
	private String bannerCode;
	
	/** 供應廠商 */
	@Column(name = "SUPPLIER")
	private String supplier;
	
	/** 廠商編號 */
	@Column(name = "SUPPLIER_CODE")
	private String supplierCode;
	
	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}
	
	public Advertisement() {
	}

	public Integer getPkAdvertisement() {
		return this.pkAdvertisement;
	}

	public void setPkAdvertisement(Integer pkAdvertisement) {
		this.pkAdvertisement = pkAdvertisement;
	}

	public String getActive() {
		return this.active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getAdsName() {
		return this.adsName;
	}

	public void setAdsName(String adsName) {
		this.adsName = adsName;
	}

	public Date getCreateDt() {
		return this.createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return this.createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public String getExternalLink() {
		return this.externalLink;
	}

	public void setExternalLink(String externalLink) {
		this.externalLink = externalLink;
	}

	public String getImageOneId() {
		return this.imageOneId;
	}

	public void setImageOneId(String imageOneId) {
		this.imageOneId = imageOneId;
	}

	public String getImageTwoId() {
		return this.imageTwoId;
	}

	public void setImageTwoId(String imageTwoId) {
		this.imageTwoId = imageTwoId;
	}

	public String getNotice() {
		return this.notice;
	}

	public void setNotice(String notice) {
		this.notice = notice;
	}

	public Date getPublishEDate() {
		return this.publishEDate;
	}

	public void setPublishEDate(Date publishEDate) {
		this.publishEDate = publishEDate;
	}

	public Date getPublishSDate() {
		return this.publishSDate;
	}

	public void setPublishSDate(Date publishSDate) {
		this.publishSDate = publishSDate;
	}

	public Date getUpdateDt() {
		return this.updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public AdCategory getCategory() {
		return category;
	}

	public void setCategory(AdCategory category) {
		this.category = category;
	}

	public List<AdRoleGrade> getRoleGrades() {
		return roleGrades;
	}

	public void setRoleGrades(List<AdRoleGrade> roleGrades) {
		this.roleGrades = roleGrades;
	}

	public String getBannerCode() {
		return bannerCode;
	}

	public void setBannerCode(String bannerCode) {
		this.bannerCode = bannerCode;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	
	

}