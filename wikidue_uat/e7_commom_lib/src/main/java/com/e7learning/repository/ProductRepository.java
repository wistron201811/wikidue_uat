package com.e7learning.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.Product;

/**
 * @author Clarence
 *
 */
@Repository("productRepository")
@Transactional
public interface ProductRepository extends JpaRepository<Product, String> ,
PagingAndSortingRepository<Product, String>, JpaSpecificationExecutor<Product> {
	
	List<Product> findByVendorUid(String vendorUid);
	
	@Query("SELECT p FROM Product p WHERE p.course.pkCourse = :pkCourse and (p.publishSDate between :sDate and :eDate or p.publishEDate between :sDate and :eDate or :sDate between p.publishSDate and p.publishEDate or :eDate between p.publishSDate and p.publishEDate) and p.active='1'")
	List<Product> findByPkCourse(@Param("pkCourse")Integer pkCourse, @Param("sDate") Date sDate, @Param("eDate") Date eDate);
	
	@Query("SELECT p FROM Product p WHERE p.props.pkProps = :pkProps and (p.publishSDate between :sDate and :eDate or p.publishEDate between :sDate and :eDate or :sDate between p.publishSDate and p.publishEDate or :eDate between p.publishSDate and p.publishEDate) and p.active='1' ")
	List<Product> findByPkProps(@Param("pkProps")Integer pkProps, @Param("sDate") Date sDate, @Param("eDate") Date eDate);
	
	@Query("SELECT p FROM Product p WHERE p.pkProduct != :pkProduct and p.course.pkCourse = :pkCourse and (p.publishSDate between :sDate and :eDate or p.publishEDate between :sDate and :eDate or :sDate between p.publishSDate and p.publishEDate or :eDate between p.publishSDate and p.publishEDate) and p.active='1'")
	List<Product> findByPkCourseAndPkProduct(@Param("pkProduct") String pkProduct,@Param("pkCourse")Integer pkCourse, @Param("sDate") Date sDate, @Param("eDate") Date eDate);
	
	@Query("SELECT p FROM Product p WHERE p.pkProduct != :pkProduct and p.props.pkProps = :pkProps and (p.publishSDate between :sDate and :eDate or p.publishEDate between :sDate and :eDate or :sDate between p.publishSDate and p.publishEDate or :eDate between p.publishSDate and p.publishEDate) and p.active='1' ")
	List<Product> findByPkPropsAndPkProduct(@Param("pkProduct") String pkProduct,@Param("pkProps")Integer pkProps, @Param("sDate") Date sDate, @Param("eDate") Date eDate);
	
	@Query("SELECT p FROM Product p WHERE p.props.pkProps = :pkProps")
	List<Product> getByPkProps(@Param("pkProps")Integer pkProps);
	
	@Query("SELECT p FROM Product p WHERE p.course.pkCourse = :pkCourse and p.active='1' and :today between p.publishSDate and p.publishEDate")
	List<Product> findByPkCourseAfterSysdate(@Param("pkCourse")Integer pkCourse, @Param("today") Date today);
	
	@Query("SELECT p FROM Product p WHERE p.props.pkProps = :pkProps and p.active='1' and :today between p.publishSDate and p.publishEDate")
	List<Product> findByPkPropsAfterSysdate(@Param("pkProps")Integer pkProps, @Param("today") Date today);
	
}
