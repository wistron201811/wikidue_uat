package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "member_info")
@NamedQuery(name = "MemberInfo.findAll", query = "SELECT c FROM MemberInfo c")
public class MemberInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "MEMBER_UID")
	private String memberUid;

	@Column(name = "INVOICE_ADDRESS")
	private String invoiceAddress;

	@Column(name = "MAIL_ADDRESS")
	private String mailAddress;

	@Column(name = "INVOICE_TELPHONE")
	private String invoiceTelphone;

	@Column(name = "MAIL_TELPHONE")
	private String mailTelPhone;

	@Column(name = "INVOICE_EMAIL")
	private String invoiceEmail;

	@Column(name = "INVOICE_ZIPCODE")
	private String invoiceZipcode;

	@Column(name = "MAIL_ZIPCODE")
	private String mailZipcode;

	@Column(name = "LOVE_CODE")
	private String loveCode;

	@Column(name = "INVOICE_NAME")
	private String invoiceName;

	@Column(name = "MAIL_NAME")
	private String mailName;

	@Column(name = "COMPANY_NO")
	private String companyNo;

	@Column(name = "COMPANY_NAME")
	private String companyName;

	@Column(name = "MOICA_NO")
	private String moicaNo;

	@Column(name = "MOBILE_CODE")
	private String mobileCode;

	@Column(name = "INVOICE_TYPE")
	private String invoiceType;

	@Column(name = "DEVICE_TYPE")
	private String deviceType;

	@Column(name = "CREATE_DT")
	private Date createDt;

	@Column(name = "UPDATE_DT")
	private Date updateDt;

	@Column(name = "ACTIVE")
	private String active;

	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;

	public String getMemberUid() {
		return memberUid;
	}

	public void setMemberUid(String memberUid) {
		this.memberUid = memberUid;
	}

	public String getInvoiceAddress() {
		return invoiceAddress;
	}

	public void setInvoiceAddress(String invoiceAddress) {
		this.invoiceAddress = invoiceAddress;
	}

	public String getMailAddress() {
		return mailAddress;
	}

	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}

	public String getInvoiceTelphone() {
		return invoiceTelphone;
	}

	public void setInvoiceTelphone(String invoiceTelphone) {
		this.invoiceTelphone = invoiceTelphone;
	}

	public String getMailTelPhone() {
		return mailTelPhone;
	}

	public void setMailTelPhone(String mailTelPhone) {
		this.mailTelPhone = mailTelPhone;
	}

	public String getInvoiceEmail() {
		return invoiceEmail;
	}

	public void setInvoiceEmail(String invoiceEmail) {
		this.invoiceEmail = invoiceEmail;
	}

	public String getInvoiceZipcode() {
		return invoiceZipcode;
	}

	public void setInvoiceZipcode(String invoiceZipcode) {
		this.invoiceZipcode = invoiceZipcode;
	}

	public String getMailZipcode() {
		return mailZipcode;
	}

	public void setMailZipcode(String mailZipcode) {
		this.mailZipcode = mailZipcode;
	}

	public String getLoveCode() {
		return loveCode;
	}

	public void setLoveCode(String loveCode) {
		this.loveCode = loveCode;
	}

	public String getInvoiceName() {
		return invoiceName;
	}

	public void setInvoiceName(String invoiceName) {
		this.invoiceName = invoiceName;
	}

	public String getMailName() {
		return mailName;
	}

	public void setMailName(String mailName) {
		this.mailName = mailName;
	}

	public String getCompanyNo() {
		return companyNo;
	}

	public void setCompanyNo(String companyNo) {
		this.companyNo = companyNo;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getMoicaNo() {
		return moicaNo;
	}

	public void setMoicaNo(String moicaNo) {
		this.moicaNo = moicaNo;
	}

	public String getMobileCode() {
		return mobileCode;
	}

	public void setMobileCode(String mobileCode) {
		this.mobileCode = mobileCode;
	}

	public String getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public Date getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}

}
