package com.e7learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.e7learning.repository.model.AdRoleGrade;

/**
 * @author ChrisTsai
 *
 */
@Repository
public interface AdRoleGradeRepository extends JpaRepository<AdRoleGrade, Integer> ,
PagingAndSortingRepository<AdRoleGrade, Integer>, JpaSpecificationExecutor<AdRoleGrade> {

	
}
