
package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * The persistent class for the PROPS database table.
 * 
 */
@Entity
@Table(name = "props")
@NamedQuery(name = "Props.findAll", query = "SELECT p FROM Props p")
public class Props implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PK_PROPS")
	private Integer pkProps;

	@Column(name = "PROPS_NAME")
	private String propsName;

	@Column(name = "QUANTITY")
	private Integer quantity;

	@Column(name = "PUBLISH_S_DATE")
	private Date publishSDate;

	@Column(name = "PUBLISH_E_DATE")
	private Date publishEDate;

	@Column(name = "IMAGE_ONE_ID")
	private String imageOneId;

	@Column(name = "IMAGE_TWO_ID")
	private String imageTwoId;

	@Column(name = "IMAGE_THIRD_ID")
	private String imageThirdId;
	
	@Column(name = "IMAGE_FOURTH_ID")
	private String imageFourthId;
	
	@Column(name = "PRICE")
	private Integer price;

	@Column(name = "DISCOUNT_PRICE")
	private Integer discountPrice;

	@Column(name = "PROPS_SUMMARY")
	private String propsSummary;

	@Column(name = "PROPS_INTRODUCTION")
	private String propsIntroduction;

	@Column(name = "NOTICE")
	private String notice;

	// Many-to-one association to Vendor
	@ManyToOne(cascade = { CascadeType.MERGE })
	@JoinColumn(name = "VENDOR_UID")
	private Vendor vendor;

	@Column(name = "UPDATE_DT")
	private Date updateDt;

	@Column(name = "CREATE_DT")
	private Date createDt;

	@Column(name = "CREATE_ID")
	private String createId;

	@Column(name = "ACTIVE", columnDefinition = "CHAR(1) default 'Y'")
	private String active;

	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;
	
	@Column(name = "MATERIAL_NUM")
	private String materialNum;
	
	@Column(name = "COST_PRICE")
	private Integer costPrice;
	
	@OneToMany(mappedBy = "props")
	private List<Tag> tagList;
	
	@Transient
	private boolean onMarket;
	
	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}
	
	public Props() {
	}

	public Integer getPkProps() {
		return pkProps;
	}

	public void setPkProps(Integer pkProps) {
		this.pkProps = pkProps;
	}

	public String getPropsName() {
		return propsName;
	}

	public void setPropsName(String propsName) {
		this.propsName = propsName;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Date getPublishSDate() {
		return publishSDate;
	}

	public void setPublishSDate(Date publishSDate) {
		this.publishSDate = publishSDate;
	}

	public Date getPublishEDate() {
		return publishEDate;
	}

	public void setPublishEDate(Date publishEDate) {
		this.publishEDate = publishEDate;
	}

	public String getImageOneId() {
		return imageOneId;
	}

	public void setImageOneId(String imageOneId) {
		this.imageOneId = imageOneId;
	}

	public String getImageTwoId() {
		return imageTwoId;
	}

	public void setImageTwoId(String imageTwoId) {
		this.imageTwoId = imageTwoId;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Integer discountPrice) {
		this.discountPrice = discountPrice;
	}

	public String getNotice() {
		return notice;
	}

	public void setNotice(String notice) {
		this.notice = notice;
	}

	public String getPropsSummary() {
		return propsSummary;
	}

	public void setPropsSummary(String propsSummary) {
		this.propsSummary = propsSummary;
	}

	public String getPropsIntroduction() {
		return propsIntroduction;
	}

	public void setPropsIntroduction(String propsIntroduction) {
		this.propsIntroduction = propsIntroduction;
	}

	public Date getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public String getMaterialNum() {
		return materialNum;
	}

	public void setMaterialNum(String materialNum) {
		this.materialNum = materialNum;
	}

	public Integer getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(Integer costPrice) {
		this.costPrice = costPrice;
	}

	public String getImageThirdId() {
		return imageThirdId;
	}

	public void setImageThirdId(String imageThirdId) {
		this.imageThirdId = imageThirdId;
	}

	public String getImageFourthId() {
		return imageFourthId;
	}

	public void setImageFourthId(String imageFourthId) {
		this.imageFourthId = imageFourthId;
	}

	public List<Tag> getTagList() {
		return tagList;
	}

	public void setTagList(List<Tag> tagList) {
		this.tagList = tagList;
	}

	public boolean isOnMarket() {
		return onMarket;
	}

	public void setOnMarket(boolean onMarket) {
		this.onMarket = onMarket;
	}
	
	
}