
package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the RETURN_RECORD database table.
 * 
 */
@Entity
@Table(name = "return_record")
@NamedQuery(name = "ReturnRecord.findAll", query = "SELECT r FROM ReturnRecord r")
public class ReturnRecord implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PK_RETURN_RECORD")
	private Integer pkReturnRecord;

	@Column(name = "MAIN_ORDER_NO")
	private String mainOrderNo;

	@Column(name = "RETURN_REASON")
	private String returnReason;

	@Column(name = "RETURN_AMT")
	private Integer returnAmt;

	@Column(name = "CREATE_DT")
	private Date createDt;

	@Column(name = "CREATE_ID")
	private String createId;

	@Column(name = "UPDATE_DT")
	private Date updateDt;

	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;

	@Column(name = "ACTIVE")
	private String active;
	/*
	 * 發票折讓號碼
	 */
	@Column(name = "INVOICE_ALLOW_NO")
	private String invoiceAllowNo;
	/*
	 * 發票號碼
	 */
	@Column(name = "INVOICE_NO")
	private String invoiceNo;
	/*
	 * 發票折讓/作廢日期
	 */
	@Column(name = "INVOICE_ALLOW_DT")
	private Date invoiceAllowDt;
	/*
	 * A.作廢(全退) P.折讓(退部分)
	 */
	@Column(name = "TYPE")
	private String type;
	/*
	 * 發票剩餘金額
	 */
	@Column(name = "REMAIN_ALLOWANCE_AMT")
	private Integer remainAllowanceAmt;

	@OneToMany(mappedBy = "returnRecord")
	private List<ReturnRecordDetail> detailList;

	public ReturnRecord() {
	}

	public Integer getPkReturnRecord() {
		return pkReturnRecord;
	}

	public void setPkReturnRecord(Integer pkReturnRecord) {
		this.pkReturnRecord = pkReturnRecord;
	}

	public String getMainOrderNo() {
		return mainOrderNo;
	}

	public void setMainOrderNo(String mainOrderNo) {
		this.mainOrderNo = mainOrderNo;
	}

	public String getReturnReason() {
		return returnReason;
	}

	public void setReturnReason(String returnReason) {
		this.returnReason = returnReason;
	}

	public Integer getReturnAmt() {
		return returnAmt;
	}

	public void setReturnAmt(Integer returnAmt) {
		this.returnAmt = returnAmt;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public Date getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public List<ReturnRecordDetail> getDetailList() {
		return detailList;
	}

	public void setDetailList(List<ReturnRecordDetail> detailList) {
		this.detailList = detailList;
	}

	public String getInvoiceAllowNo() {
		return invoiceAllowNo;
	}

	public void setInvoiceAllowNo(String invoiceAllowNo) {
		this.invoiceAllowNo = invoiceAllowNo;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public Date getInvoiceAllowDt() {
		return invoiceAllowDt;
	}

	public void setInvoiceAllowDt(Date invoiceAllowDt) {
		this.invoiceAllowDt = invoiceAllowDt;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getRemainAllowanceAmt() {
		return remainAllowanceAmt;
	}

	public void setRemainAllowanceAmt(Integer remainAllowanceAmt) {
		this.remainAllowanceAmt = remainAllowanceAmt;
	}

}