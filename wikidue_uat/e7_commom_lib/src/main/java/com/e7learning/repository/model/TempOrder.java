package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;


/**
 * The persistent class for the CART database table.
 * 
 */
@Entity
@Table(name = "temp_order")
@DynamicInsert
@DynamicUpdate
@NamedQuery(name="TempOrder", query="SELECT c FROM TempOrder c")
public class TempOrder implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="PK_TEMP_ORDER")
	private Integer pkTempOrder;
	
	@Column(name="FK_CART")
	private Integer fkCart;

	@Column(name="CHECK_OUT_ID")
	private String checkOutId;

	@Column(name="CREATE_DT")
	private Date createDt;

	@Column(name="CREATE_ID")
	private String createId;

	@Column(name="LAST_MODIFIED_ID")
	private String lastModifiedId;

	@Column(name="LIST_PRICE")
	private Integer listPrice;

	@Column(name="FK_PRODUCT")
	private String fkProduct;

	@Column(name="PRODUCT_NAME")
	private String productName;

	private String purchaser;

	private Integer quantity;

	@Column(name="UNIT_PRICE")
	private Integer unitPrice;

	@Column(name="UPDATE_DT")
	private Date updateDt;
	/*
	 * N:已轉訂單
	 * */
	@Column(name="STATUS")
	private String status;

	public TempOrder() {
	}

	public Integer getPkTempOrder() {
		return pkTempOrder;
	}

	public void setPkTempOrder(Integer pkTempOrder) {
		this.pkTempOrder = pkTempOrder;
	}

	public Integer getFkCart() {
		return fkCart;
	}

	public void setFkCart(Integer fkCart) {
		this.fkCart = fkCart;
	}

	public String getCheckOutId() {
		return checkOutId;
	}

	public void setCheckOutId(String checkOutId) {
		this.checkOutId = checkOutId;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}

	public Integer getListPrice() {
		return listPrice;
	}

	public void setListPrice(Integer listPrice) {
		this.listPrice = listPrice;
	}

	public String getFkProduct() {
		return fkProduct;
	}

	public void setFkProduct(String fkProduct) {
		this.fkProduct = fkProduct;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPurchaser() {
		return purchaser;
	}

	public void setPurchaser(String purchaser) {
		this.purchaser = purchaser;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Integer unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Date getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}