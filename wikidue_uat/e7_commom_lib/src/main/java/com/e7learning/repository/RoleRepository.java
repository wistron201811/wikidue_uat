package com.e7learning.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.Role;
import com.e7learning.repository.model.RoleId;

/**
 * @author J
 *
 */
@Repository("roleRepository")
@Transactional
public interface RoleRepository extends JpaRepository<Role, RoleId> ,
PagingAndSortingRepository<Role, RoleId>, JpaSpecificationExecutor<Role> {

	@Query("SELECT r FROM Role r WHERE r.roleId.email=:email and r.roleId.roleName=:roleName")
	List<Role> findUserEmail(@Param("email") String email, @Param("roleName") String roleName);
	
	List<Role> findByRoleIdEmail(String email);
}
