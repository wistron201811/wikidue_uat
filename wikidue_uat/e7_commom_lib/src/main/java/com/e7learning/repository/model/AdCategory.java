package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the CATEGORY database table.
 * 
 */
@Entity
@Table(name = "ad_category")
@NamedQuery(name="AdCategory.findAll", query="SELECT c FROM AdCategory c")
public class AdCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PK_AD_CATEGORY")
	private Integer pkAdCategory;

	@Column(name = "ACTIVE", columnDefinition = "CHAR(1) default 'Y'")
	private String active;

	@Column(name="CATEGORY_CODE")
	private String categoryCode;

	@Column(name="CATEGORY_NAME")
	private String categoryName;

	@Column(name="CATEGORY_TYPE")
	private String categoryType;

	@Column(name="CREATE_DT")
	private Date createDt;

	@Column(name="CREATE_ID")
	private String createId;

	@Column(name="UPDATE_DT")
	private Date updateDt;

	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;
	
	
	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}
	
	public AdCategory() {
	}

	public Integer getPkAdCategory() {
		return pkAdCategory;
	}

	public void setPkAdCategory(Integer pkAdCategory) {
		this.pkAdCategory = pkAdCategory;
	}

	public String getActive() {
		return this.active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getCategoryCode() {
		return this.categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getCategoryName() {
		return this.categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryType() {
		return this.categoryType;
	}

	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}

	public Date getCreateDt() {
		return this.createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return this.createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public Date getUpdateDt() {
		return this.updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

}