package com.e7learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.QuestionReply;

@Repository
@Transactional
public interface QuestionReplyRepository extends JpaRepository<QuestionReply, Integer>,
		PagingAndSortingRepository<QuestionReply, Integer>, JpaSpecificationExecutor<QuestionReply> {

}
