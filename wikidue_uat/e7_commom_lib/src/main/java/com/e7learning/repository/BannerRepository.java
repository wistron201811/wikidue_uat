package com.e7learning.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.e7learning.repository.model.Banner;

@Repository
public interface BannerRepository extends JpaRepository<Banner, String>, PagingAndSortingRepository<Banner, String>,
		JpaSpecificationExecutor<Banner> {

	List<Banner> findByPublishSDateGreaterThanEqualAndPublishEDateLessThanEqualAndPublish(Date sDate, Date eDate,
			String publish);
	
	List<Banner> findByPublishSDateLessThanEqualAndPublishEDateGreaterThanEqualAndPublishOrderByOrder(Date sDate, Date eDate,
			String publish);
	
	@Query(value="select * from banner where banner.publish = :publish order by banner.order",nativeQuery=true)
	List<Banner> findByPublish(@Param("publish") String publish);
	
	List<Banner> findByPublishOrderByPublishSDate(@Param("publish") String publish);
	
//	@Query(value="select * from banner where banner.banner_id in :bannerid order by banner.order",nativeQuery=true)
	List<Banner> findByBannerIdIn(List<String> bannerId);
}
