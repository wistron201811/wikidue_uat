package com.e7learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.ReturnRecord;

/**
 * @author J
 *
 */
@Repository("returnRecordRepository")
@Transactional
public interface ReturnRecordRepository extends JpaRepository<ReturnRecord, Integer> ,
PagingAndSortingRepository<ReturnRecord, Integer>, JpaSpecificationExecutor<ReturnRecord> {


}
