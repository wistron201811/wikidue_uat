package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

/**
 * @author Clarence
 * 課程影片
 */
@Entity
@Table(name = "course_video")
@DynamicInsert
@DynamicUpdate
@NamedQuery(name = "CourseVideo.findAll", query = "SELECT c FROM CourseVideo c")
public class CourseVideo implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 課程影片編號
	 */
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="PK_COURSE_VIDEO")
	private Integer pkCourseVideo;

	/**
	 * 課程章節編號
	 */
	@ManyToOne
	@JoinColumn(name="FK_COURSE_CHAPTER")
	private CourseChapter courseChapter;

	/**
	 * 課程影片名稱
	 */
	@Column(name = "COURSE_VIDEO_NAME")
	private String courseVideoName;
	
	/**
	 * 課程影片順序
	 */
	@Column(name = "COURSE_VIDEO_NO")
	private Integer courseVideoNo;

	/**
	 * 平台影片外來鍵
	 */
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="FK_VIDEO")
	private Video video;

	/**
	 * 新增時間
	 */
	@Column(name = "CREATE_DT")
	private Date createDt;

	/**
	 * 建立人員
	 */
	@Column(name = "CREATE_ID")
	private String createId;

	/**
	 * 修改時間
	 */
	@Column(name = "UPDATE_DT")
	private Date updateDt;

	/**
	 * 是否可用
	 */
	@Column(name = "ACTIVE", columnDefinition = "CHAR(1) default '1'")
	private String active;

	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;
	
	/**
	 * 是否免費
	 */
	@Column(name = "IS_FREE")
	private String isFree;
	
	/** 影片長度(秒) */
	@Transient
	private Integer videoDuration;

	/** 已播進度(秒) */
	@Transient
	private Integer learningDuration;
	
	/** 最後播放時間 */
	@Transient
	private Date lastWatchDt;
	
	
	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}
	
	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public Date getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Integer getPkCourseVideo() {
		return pkCourseVideo;
	}

	public void setPkCourseVideo(Integer pkCourseVideo) {
		this.pkCourseVideo = pkCourseVideo;
	}

	public String getCourseVideoName() {
		return courseVideoName;
	}

	public void setCourseVideoName(String courseVideoName) {
		this.courseVideoName = courseVideoName;
	}

	public Integer getCourseVideoNo() {
		return courseVideoNo;
	}

	public void setCourseVideoNo(Integer courseVideoNo) {
		this.courseVideoNo = courseVideoNo;
	}

	public Video getVideo() {
		return video;
	}

	public void setVideo(Video video) {
		this.video = video;
	}

	public CourseChapter getCourseChapter() {
		return courseChapter;
	}

	public void setCourseChapter(CourseChapter courseChapter) {
		this.courseChapter = courseChapter;
	}

	public String getIsFree() {
		return isFree;
	}

	public void setIsFree(String isFree) {
		this.isFree = isFree;
	}

	public Integer getVideoDuration() {
		return videoDuration;
	}

	public void setVideoDuration(Integer videoDuration) {
		this.videoDuration = videoDuration;
	}

	public Integer getLearningDuration() {
		return learningDuration;
	}

	public void setLearningDuration(Integer learningDuration) {
		this.learningDuration = learningDuration;
	}
	
	public Date getLastWatchDt() {
		return lastWatchDt;
	}

	public void setLastWatchDt(Date lastWatchDt) {
		this.lastWatchDt = lastWatchDt;
	}

	public boolean isComplete() {
		// 播放長度超過總長度一半就算完成
		if (videoDuration != null && learningDuration != null && (Double.valueOf(learningDuration) / Double.valueOf(videoDuration) * 100 >= 50)) {
			return true;
		}

		return false;
	}

	
}
