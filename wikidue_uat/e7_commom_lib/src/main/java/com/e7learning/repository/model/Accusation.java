
package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ACCUSATION database table.
 * 
 */
@Entity
@Table(name = "accusation")
@NamedQuery(name = "Accusation.findAll", query = "SELECT a FROM Accusation a")
public class Accusation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PK_ACCUSATION")
	private Integer pkAccusation;

	// Many-to-one association to ShareCenter
	@ManyToOne
	@JoinColumn(name = "PK_SHARE")
	private ShareCenter shareCenter;

	@Column(name = "INFORMER")
	private String informer;

	@Column(name = "REPORT_REASON")
	private String reportReason;

	@Column(name = "APPROVE_STATUS")
	private String approveStatus;

	@Column(name = "CREATE_DT")
	private Date createDt;

	@Column(name = "CREATE_ID")
	private String createId;

	@Column(name = "UPDATE_DT")
	private Date updateDt;

	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;
	
	
	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}

	public Accusation() {
	}

	public Integer getPkAccusation() {
		return pkAccusation;
	}

	public void setPkAccusation(Integer pkAccusation) {
		this.pkAccusation = pkAccusation;
	}

	public ShareCenter getShareCenter() {
		return shareCenter;
	}

	public void setShareCenter(ShareCenter shareCenter) {
		this.shareCenter = shareCenter;
	}

	public String getInformer() {
		return informer;
	}

	public void setInformer(String informer) {
		this.informer = informer;
	}

	public String getReportReason() {
		return reportReason;
	}

	public void setReportReason(String reportReason) {
		this.reportReason = reportReason;
	}

	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public Date getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

}