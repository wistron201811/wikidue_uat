package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * @author Clarence 平台影片
 */
@Entity
@Table(name = "video")
@NamedQuery(name = "Video.findAll", query = "SELECT v FROM Video v")
public class Video implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 平台影片主鍵
	 */
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name = "PK_VIDEO")
	private String pkVideo;

	/**
	 * 影片資料清單
	 */
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "video")
	private List<VideoData> videoDataList;

	/**
	 * 影片資料清單
	 */
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "video")
	private List<VideoTag> videoTagList;

	/**
	 * 轉檔結果(成功或失敗)
	 */
	@Column(name = "RESULT")
	private String result;

	/**
	 * HLS 連結
	 */
	@Column(name = "STREAM")
	private String stream;

	/**
	 * 影片長度
	 */
	@Column(name = "DURATION")
	private Integer duration;

	/**
	 * 影片名稱
	 */
	@Column(name = "TITLE")
	private String title;

	/**
	 * 帳號名稱
	 */
	@Column(name = "ACCOUNT_NAME")
	private String accountName;

	/**
	 * 簡述
	 */
	@Column(name = "DESCRIPTION")
	private String description;

	/**
	 * 影片擷圖
	 */
	@Column(name = "IMG")
	private String img;

	/**
	 * 影音原始檔名
	 */
	@Column(name = "FILE_NAME")
	private String fileName;

	/**
	 * 影音內嵌碼
	 */
	@Column(name = "EMBED")
	private String embed;

	/**
	 * 影音平台ID
	 */
	@Column(name = "THIRD_ID")
	private String thirdId;

	/**
	 * 新增時間
	 */
	@Column(name = "CREATE_DT")
	private Date createDt;

	/**
	 * 建立人員
	 */
	@Column(name = "CREATE_ID")
	private String createId;

	/**
	 * 修改時間
	 */
	@Column(name = "UPDATE_DT")
	private Date updateDt;
	
	
	@Column(name = "PLAYER_STREAM_URL")
	private String playerStreamUrl;

	/**
	 * 是否可用
	 */
	@Column(name = "ACTIVE", columnDefinition = "CHAR(1) default '1'")
	private String active;

	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;
	
	
	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}
	
	public String getPkVideo() {
		return pkVideo;
	}

	public void setPkVideo(String pkVideo) {
		this.pkVideo = pkVideo;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getStream() {
		return stream;
	}

	public void setStream(String stream) {
		this.stream = stream;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getEmbed() {
		return embed;
	}

	public void setEmbed(String embed) {
		this.embed = embed;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public Date getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getThirdId() {
		return thirdId;
	}

	public void setThirdId(String thirdId) {
		this.thirdId = thirdId;
	}

	public List<VideoData> getVideoDataList() {
		return videoDataList;
	}

	public void setVideoDataList(List<VideoData> videoDataList) {
		this.videoDataList = videoDataList;
	}

	public List<VideoTag> getVideoTagList() {
		return videoTagList;
	}

	public void setVideoTagList(List<VideoTag> videoTagList) {
		this.videoTagList = videoTagList;
	}

	public String getPlayerStreamUrl() {
		return playerStreamUrl;
	}

	public void setPlayerStreamUrl(String playerStreamUrl) {
		this.playerStreamUrl = playerStreamUrl;
	}

	
}
