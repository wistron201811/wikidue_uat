package com.e7learning.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.e7learning.repository.model.Teacher;

/**
 * @author J
 *
 */
public interface TeacherRepository extends JpaRepository<Teacher, Integer>,
		PagingAndSortingRepository<Teacher, Integer>, JpaSpecificationExecutor<Teacher> {

	@Query("SELECT t FROM Teacher t WHERE t.teacherName like %:name%")
	List<Teacher> findByName(@Param("name") String name);

	@Query("SELECT t FROM Teacher t WHERE t.active = '1'")
	List<Teacher> findActive();
	
	List<Teacher> findByEmail(String email);
	
	@Query(value="SELECT count(t) FROM Teacher t WHERE t.nickname = :nickname")
	Long countByNickname(@Param("nickname") String nickname);
	
	@Query(value="SELECT count(t) FROM Teacher t WHERE t.nickname = :nickname and t.pkTeacher != :pkTeacher")
	Long countByNicknameWithPk(@Param("nickname") String nickname, @Param("pkTeacher") Integer pkTeacher);
}
