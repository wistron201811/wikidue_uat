
package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The persistent class for the MAIN_ORDER database table.
 * 
 */
@Entity
@Table(name = "main_order")
@NamedQuery(name = "MainOrder.findAll", query = "SELECT mo FROM MainOrder mo")
public class MainOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "PK_MAIN_ORDER")
	private String pkMainOrder;

	@Column(name = "ORDER_AMOUNT")
	private Integer orderAmount;
		
	@Column(name = "ORDER_STATUS")
	private String orderStatus;
	
	@Column(name = "ORDER_DT")
	private Date orderDt;
	
	@Column(name = "PAYMENT_DUE_DATE")
	private Date paymentDueDate;
	
	@Column(name = "PURCHASER")
	private String purchaser;

	@Column(name = "PURCHASER_ID")
	private String purchaserId;
	
	@Column(name = "RECIPIENT")
	private String recipient;

	@Column(name = "ADDRESS")
	private String address;

	@Column(name = "MOBILE")
	private String mobile;

	@Column(name = "TELPHONE")
	private String telphone;
	
	@OneToOne
	@JoinColumn(name = "FK_PAYMENT")
	private Payment payment;
	
	@Column(name = "RECEIPT_ADDRESS")
	private String receiptAddress;

	@Column(name = "RECEIPT_EMAIL")
	private String receiptEmail;

	@Column(name = "RECEIPT_TELPHONE")
	private String receiptTelphone;
	
	@Column(name = "CANCEL_REASON")
	private String cancelReason;
	
	@Column(name = "CANCEL_DT")
	private Date cancelDt;
	
	@Column(name = "CREATE_DT")
	private Date createDt;

	@Column(name = "CREATE_ID")
	private String createId;

	@Column(name = "UPDATE_DT")
	private Date updateDt;
	
	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;

	@OneToMany(mappedBy = "mainOrder")
	private List<SubOrder> subOrderList;
	
	@Column(name = "ADMIN_REMARK")
	private String adminRemark;

	@Column(name = "USER_REMARK")
	private String userRemark;
	
	public MainOrder() {
	}

	public String getPkMainOrder() {
		return pkMainOrder;
	}

	public void setPkMainOrder(String pkMainOrder) {
		this.pkMainOrder = pkMainOrder;
	}

	public Integer getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(Integer orderAmount) {
		this.orderAmount = orderAmount;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Date getOrderDt() {
		return orderDt;
	}

	public void setOrderDt(Date orderDt) {
		this.orderDt = orderDt;
	}

	public Date getPaymentDueDate() {
		return paymentDueDate;
	}

	public void setPaymentDueDate(Date paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}

	public String getPurchaser() {
		return purchaser;
	}

	public void setPurchaser(String purchaser) {
		this.purchaser = purchaser;
	}

	public String getPurchaserId() {
		return purchaserId;
	}

	public void setPurchaserId(String purchaserId) {
		this.purchaserId = purchaserId;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getTelphone() {
		return telphone;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public String getReceiptAddress() {
		return receiptAddress;
	}

	public void setReceiptAddress(String receiptAddress) {
		this.receiptAddress = receiptAddress;
	}

	public String getReceiptEmail() {
		return receiptEmail;
	}

	public void setReceiptEmail(String receiptEmail) {
		this.receiptEmail = receiptEmail;
	}

	public String getReceiptTelphone() {
		return receiptTelphone;
	}

	public void setReceiptTelphone(String receiptTelphone) {
		this.receiptTelphone = receiptTelphone;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public Date getCancelDt() {
		return cancelDt;
	}

	public void setCancelDt(Date cancelDt) {
		this.cancelDt = cancelDt;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public Date getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}

	public List<SubOrder> getSubOrderList() {
		return subOrderList;
	}

	public void setSubOrderList(List<SubOrder> subOrderList) {
		this.subOrderList = subOrderList;
	}

	public String getAdminRemark() {
		return adminRemark;
	}

	public void setAdminRemark(String adminRemark) {
		this.adminRemark = adminRemark;
	}

	public String getUserRemark() {
		return userRemark;
	}

	public void setUserRemark(String userRemark) {
		this.userRemark = userRemark;
	}
}