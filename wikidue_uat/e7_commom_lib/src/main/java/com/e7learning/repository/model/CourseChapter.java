package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

/**
 * @author Clarence
 * 課程章節
 */
@Entity
@Table(name = "course_chapter")
@NamedQuery(name = "CourseChapter.findAll", query = "SELECT c FROM CourseChapter c")
public class CourseChapter implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 課程章節編號
	 */
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="PK_COURSE_CHAPTER")
	private Integer pkCourseChapter;

	/**
	 * 課程編號
	 */
	@ManyToOne
	@JoinColumn(name="FK_COURSE")
	private Course course;

	/**
	 * 課程章節名稱
	 */
	@Column(name = "COURSE_CHAPTER_NAME")
	private String courseChapterName;

	/**
	 * 課程章節順序
	 */
	@Column(name = "COURSE_CHAPTER_NO")
	private Integer courseChapterNo;

	/**
	 * 新增時間
	 */
	@Column(name = "CREATE_DT")
	private Date createDt;

	/**
	 * 建立人員
	 */
	@Column(name = "CREATE_ID")
	private String createId;

	/**
	 * 修改時間
	 */
	@Column(name = "UPDATE_DT")
	private Date updateDt;

	/**
	 * 是否可用
	 */
	@Column(name = "ACTIVE", columnDefinition = "CHAR(1) default '1'")
	private String active;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "courseChapter")
	@OrderBy("courseVideoNo asc")
	private List<CourseVideo> courseVideoList;

	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;
	
	
	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}
	
	public Integer getPkCourseChapter() {
		return pkCourseChapter;
	}

	public void setPkCourseChapter(Integer pkCourseChapter) {
		this.pkCourseChapter = pkCourseChapter;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public String getCourseChapterName() {
		return courseChapterName;
	}

	public void setCourseChapterName(String courseChapterName) {
		this.courseChapterName = courseChapterName;
	}

	public Integer getCourseChapterNo() {
		return courseChapterNo;
	}

	public void setCourseChapterNo(Integer courseChapterNo) {
		this.courseChapterNo = courseChapterNo;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public Date getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public List<CourseVideo> getCourseVideoList() {
		return courseVideoList;
	}

	public void setCourseVideoList(List<CourseVideo> courseVideoList) {
		this.courseVideoList = courseVideoList;
	}
	
	public Integer getProgress() {
		if (courseVideoList != null) {
			return Math.round((Float.valueOf(courseVideoList.stream().filter(v -> v.isComplete()).count())
					/ Float.valueOf(courseVideoList.size()) * 100));
		}
		return 0;
	}

}
