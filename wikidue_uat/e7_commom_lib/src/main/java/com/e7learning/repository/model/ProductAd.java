package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "product_ad")
@NamedQuery(name = "ProductAd.findAll", query = "SELECT p FROM ProductAd p")
public class ProductAd implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PK_PRODUCT_AD")
	private Integer pkProductAd;

	@OneToOne
	@JoinColumn(name="FK_PRODUCT")
	private Product product;
	
	@Column(name = "PUBLISH_S_DATE")
	private Date publishStartDate;
	
	@Column(name = "PUBLISH_E_DATE")
	private Date publishEndDate;
	
	@Column(name = "DESCRIPTION")
	private String description;
	
	@Column(name = "TYPE")
	private String type;
	
	@Column(name = "CREATE_DT")
	private Date createDate;
	
	@Column(name = "CREATE_ID")
	private String createId;
	
	@Column(name = "UPDATE_DT")
	private Date updateDate;
	
	@Column(name = "ACTIVE")
	private String active;

	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;
	
	
	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}
	
	public Integer getPkProductAd() {
		return pkProductAd;
	}

	public void setPkProductAd(Integer pkProductAd) {
		this.pkProductAd = pkProductAd;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Date getPublishStartDate() {
		return publishStartDate;
	}

	public void setPublishStartDate(Date publishStartDate) {
		this.publishStartDate = publishStartDate;
	}

	public Date getPublishEndDate() {
		return publishEndDate;
	}

	public void setPublishEndDate(Date publishEndDate) {
		this.publishEndDate = publishEndDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
