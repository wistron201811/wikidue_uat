package com.e7learning.repository.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the MAIN_ORDER_NO_SEQ database table.
 * 
 */
@Entity
@Table(name="main_order_no_seq")
@NamedQuery(name="MainOrderNoSeq.findAll", query="SELECT m FROM MainOrderNoSeq m")
public class MainOrderNoSeq implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private MainOrderNoSeqPK id;

	public MainOrderNoSeq() {
	}

	public MainOrderNoSeqPK getId() {
		return this.id;
	}

	public void setId(MainOrderNoSeqPK id) {
		this.id = id;
	}

}