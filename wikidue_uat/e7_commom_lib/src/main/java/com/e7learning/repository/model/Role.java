package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the CATEGORY database table.
 * 
 */
@Entity
@Table(name = "role")
@NamedQuery(name="Role.findAll", query="SELECT r FROM Role r")
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private RoleId roleId;
	
	@Column(name = "VIDEO_WATCH_UPDATE_DT")
	private Date videoWatchUpateDt;

	public RoleId getRoleId() {
		return roleId;
	}

	public void setRoleId(RoleId roleId) {
		this.roleId = roleId;
	}

	public Date getVideoWatchUpateDt() {
		return videoWatchUpateDt;
	}

	public void setVideoWatchUpateDt(Date videoWatchUpateDt) {
		this.videoWatchUpateDt = videoWatchUpateDt;
	}
	
	
}