package com.e7learning.repository.model;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

/**
 * The persistent class for the FILE_STORAGE database table.
 * 
 */
@Entity
@Table(name = "file_storage")
@NamedQuery(name = "FileStorage.findAll", query = "SELECT f FROM FileStorage f")
public class FileStorage implements Serializable, MultipartFile {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name = "PK_FILE")
	private String pkFile;

	@Column(name = "ACTIVE", columnDefinition = "CHAR(1) default 'Y'")
	private String active;

	@Column(name = "CREATE_DT")
	private Date createDt;

	@Column(name = "CREATE_ID")
	private String createId;
	
	@Column(name = "FILE_SIZE")
	private Integer fileSize;

	@Transient
	private byte[] fileContent;

	@Column(name = "FILE_PATH")
	private String filePath;

	@Column(name = "FILE_NAME")
	private String fileName;

	@Column(name = "FILE_TYPE")
	private String fileType;

	@Column(name = "UPDATE_DT")
	private Date updateDt;

	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;

	@Transient
	private Resource resource;

	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}

	public FileStorage() {
	}

	public String getPkFile() {
		return this.pkFile;
	}

	public void setPkFile(String pkFile) {
		this.pkFile = pkFile;
	}

	public String getActive() {
		return this.active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Date getCreateDt() {
		return this.createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return this.createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	@Deprecated
	public byte[] getFileContent() {
		return this.fileContent;
	}

	public void setFileContent(byte[] fileContent) {
		this.fileContent = fileContent;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return this.fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public Date getUpdateDt() {
		return this.updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	@Override
	public String getName() {
		return this.getPkFile();
	}

	@Override
	public String getOriginalFilename() {
		return this.getFileName();
	}

	@Override
	public String getContentType() {
		return this.getFileType();
	}

	@Override
	public boolean isEmpty() {
		return this.getFileContent() == null || this.getFileContent().length == 0;
	}

	@Override
	public long getSize() {
		try {
			if (resource != null) {
				return resource.getFile().length();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this.getFileContent() == null ? 0 : this.getFileContent().length;
	}

	@Override
	public byte[] getBytes() throws IOException {
		return this.getFileContent();
	}

	@Override
	public InputStream getInputStream() throws IOException {
		return null;
	}

	@Override
	public void transferTo(File dest) throws IOException, IllegalStateException {

	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Resource getResource(String storagePath) {
		if (resource == null)
			resource = new FileSystemResource(storagePath + File.separator + filePath);
		return resource;
	}

	public void setResource(Resource resource) {
		this.resource = resource;
	}

	public Integer getFileSize() {
		return fileSize;
	}

	public void setFileSize(Integer fileSize) {
		this.fileSize = fileSize;
	}
	
	

}