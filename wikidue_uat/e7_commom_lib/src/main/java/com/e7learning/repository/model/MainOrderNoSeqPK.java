package com.e7learning.repository.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the MAIN_ORDER_NO_SEQ database table.
 * 
 */
@Embeddable
public class MainOrderNoSeqPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "CREATE_DT")
	private String createDt;

	private Integer seq;

	public MainOrderNoSeqPK(String createDt, Integer seq) {
		this.createDt = createDt;
		this.seq = seq;
	}

	public MainOrderNoSeqPK() {
	}

	public String getCreateDt() {
		return this.createDt;
	}

	public void setCreateDt(String createDt) {
		this.createDt = createDt;
	}

	public Integer getSeq() {
		return this.seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof MainOrderNoSeqPK)) {
			return false;
		}
		MainOrderNoSeqPK castOther = (MainOrderNoSeqPK) other;
		return this.createDt.equals(castOther.createDt) && (this.seq == castOther.seq);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.createDt.hashCode();
		hash = hash * prime + this.seq;

		return hash;
	}
}