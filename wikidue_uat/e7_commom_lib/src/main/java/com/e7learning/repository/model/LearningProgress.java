package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "learning_progress")
@NamedQuery(name = "LearningProgress.findAll", query = "SELECT k FROM LearningProgress k")
public class LearningProgress implements Serializable {
	private static final long serialVersionUID = 1L;
	

	/**
	 * pk
	 */
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="PK_LEARNING_PROGRESS")
	private Integer pkLearningProgress;

	/**
	 * 課程影片 fk
	 */
	@Column(name = "FK_COURSE_VIDEO")
	private Integer fkCourseVideo;

	/**
	 * 使用者帳號
	 */
	@Column(name = "USER_ID")
	private String userId;

	/**
	 * 影片長度(秒)
	 */
	@Column(name = "VIDEO_DURATION")
	private Integer videoDuration;

	/**
	 * 已播進度(秒)
	 */
	@Column(name = "LEARNING_DURATION")
	private Integer learningDuration;

	/**
	 * 
	 */
	@Column(name = "ACTIVE")
	private String active;

	/**
	 * 
	 */
	@Column(name = "CREATE_ID")
	private String createId;

	/**
	 * 
	 */
	@Column(name = "CREATE_DT")
	private Date createDt;

	/**
	 * 
	 */
	@Column(name = "UPDATE_DT")
	private Date updateDt;

	/**
	 * 
	 */
	@Column(name = "LAST_WATCH_DT")
	private Date lastWatchDt;
	
	/**
	 * 
	 */
	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;

	public Integer getPkLearningProgress() {
		return pkLearningProgress;
	}

	public void setPkLearningProgress(Integer pkLearningProgress) {
		this.pkLearningProgress = pkLearningProgress;
	}

	public Integer getFkCourseVideo() {
		return fkCourseVideo;
	}

	public void setFkCourseVideo(Integer fkCourseVideo) {
		this.fkCourseVideo = fkCourseVideo;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getVideoDuration() {
		return videoDuration;
	}

	public void setVideoDuration(Integer videoDuration) {
		this.videoDuration = videoDuration;
	}

	public Integer getLearningDuration() {
		return learningDuration;
	}

	public void setLearningDuration(Integer learningDuration) {
		this.learningDuration = learningDuration;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public Date getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}

	public Date getLastWatchDt() {
		return lastWatchDt;
	}

	public void setLastWatchDt(Date lastWatchDt) {
		this.lastWatchDt = lastWatchDt;
	}
	
}
