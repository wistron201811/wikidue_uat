package com.e7learning.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.ProductAd;

/**
 * @author Clarence
 *
 */
@Repository("productAdRepository")
@Transactional
public interface ProductAdRepository extends JpaRepository<ProductAd, Integer> ,
PagingAndSortingRepository<ProductAd, Integer>, JpaSpecificationExecutor<ProductAd> {
	List<ProductAd> findTop6ByPublishStartDateLessThanEqualAndPublishEndDateGreaterThanEqualAndActiveOrderByPublishStartDateDesc(Date sDate, Date eDate,
			String active);
}
