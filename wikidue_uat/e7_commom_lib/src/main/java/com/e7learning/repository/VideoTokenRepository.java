package com.e7learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.e7learning.repository.model.VideoToken;

/**
 * @author ChrisTsai
 *
 */
public interface VideoTokenRepository extends JpaRepository<VideoToken, String>,
		PagingAndSortingRepository<VideoToken, String>, JpaSpecificationExecutor<VideoToken> {

}
