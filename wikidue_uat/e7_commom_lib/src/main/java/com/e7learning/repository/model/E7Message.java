package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


/**
 * The persistent class for the E7_MESSAGE database table.
 * 
 */
@Entity
@Table(name="e7_message")
@NamedQuery(name="E7Message.findAll", query="SELECT e FROM E7Message e")
public class E7Message implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PK_MESSAGE")
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	private String pkMessage;

	private String active;

	private String content;

	@Column(name="CREATE_DT")
	private Date createDt;

	@Column(name="CREATE_ID")
	private String createId;

	@Column(name="IS_READ")
	private String isRead;

	private String title;
	
	@Column(name="OWNER_ID")
	private String ownerID;
	
	@Column(name="LAST_MODIFIED_ID")
	private String lastModifiedId;
	

	@Column(name="UPDATE_DT")
	private Date updateDt;

	//bi-directional many-to-one association to MsgCategory
	@ManyToOne
	@JoinColumn(name="FK_MSG_CATEGORY")
	private MsgCategory msgCategory;

	public E7Message() {
	}

	public String getPkMessage() {
		return this.pkMessage;
	}

	public void setPkMessage(String pkMessage) {
		this.pkMessage = pkMessage;
	}

	public String getActive() {
		return this.active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreateDt() {
		return this.createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return this.createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public String getIsRead() {
		return this.isRead;
	}

	public void setIsRead(String isRead) {
		this.isRead = isRead;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getUpdateDt() {
		return this.updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public MsgCategory getMsgCategory() {
		return this.msgCategory;
	}

	public void setMsgCategory(MsgCategory msgCategory) {
		this.msgCategory = msgCategory;
	}

	public String getOwnerID() {
		return ownerID;
	}

	public void setOwnerID(String ownerID) {
		this.ownerID = ownerID;
	}

	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}
	
	

}