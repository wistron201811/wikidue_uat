package com.e7learning.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.MainOrder;

/**
 * @author J
 *
 */
@Repository("mainOrderRepository")
@Transactional
public interface MainOrderRepository extends JpaRepository<MainOrder, String> ,
PagingAndSortingRepository<MainOrder, String>, JpaSpecificationExecutor<MainOrder> {

	@Query("SELECT m FROM MainOrder m WHERE m.orderStatus = 'E' and m.paymentDueDate < :dueDate and m.payment.status = 'N'")
	List<MainOrder> findOverDueMainOrder(@Param("dueDate") Date dueDate);
}
