package com.e7learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.MemberInfo;

/**
 * @author Clarence
 *
 */
@Repository("memberInfoRepository")
@Transactional
public interface MemberInfoRepository extends JpaRepository<MemberInfo, String> ,
PagingAndSortingRepository<MemberInfo, String>, JpaSpecificationExecutor<MemberInfo> {

}
