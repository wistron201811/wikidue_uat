package com.e7learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.ReturnRecordDetail;

/**
 * @author J
 *
 */
@Repository("returnRecordDetailRepository")
@Transactional
public interface ReturnRecordDetailRepository extends JpaRepository<ReturnRecordDetail, Integer> ,
PagingAndSortingRepository<ReturnRecordDetail, Integer>, JpaSpecificationExecutor<ReturnRecordDetail> {


}
