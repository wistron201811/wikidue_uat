package com.e7learning.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.LearningProgress;

@Repository("learningProgressRepository")
@Transactional
public interface LearningProgressRepository extends JpaRepository<LearningProgress, Integer>,
		PagingAndSortingRepository<LearningProgress, Integer>, JpaSpecificationExecutor<LearningProgress> {

	List<LearningProgress> findByFkCourseVideoAndUserId(Integer fkCourseVideo, String uderId);
}
