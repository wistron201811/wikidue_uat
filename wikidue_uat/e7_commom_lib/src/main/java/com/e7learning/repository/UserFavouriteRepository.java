package com.e7learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.UserFavourite;

@Repository
@Transactional
public interface UserFavouriteRepository extends JpaRepository<UserFavourite, Integer>,
		PagingAndSortingRepository<UserFavourite, Integer>, JpaSpecificationExecutor<UserFavourite> {
	@Query(value = "SELECT count(t) FROM UserFavourite t WHERE t.shareCenter.pkShare = :shareId and  t.active = :active")
	Integer countByshareCenters(@Param("shareId") Integer shareId, @Param("active") String active);

}
