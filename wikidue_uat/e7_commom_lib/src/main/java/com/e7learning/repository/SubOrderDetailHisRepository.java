package com.e7learning.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.SubOrderDetailHis;

/**
 * @author J
 *
 */
@Repository("subOrderDetailHisRepository")
@Transactional
public interface SubOrderDetailHisRepository extends JpaRepository<SubOrderDetailHis, Integer>,
		PagingAndSortingRepository<SubOrderDetailHis, Integer>, JpaSpecificationExecutor<SubOrderDetailHis> {

	@Query("SELECT d FROM SubOrderDetailHis d WHERE d.subOrder.mainOrder.pkMainOrder =:pkMainOrder order by subOrderDetail.pkSubOrderDetail,d.updateDt")
	List<SubOrderDetailHis> findByPkMainOrder(@Param("pkMainOrder") String pkMainOrder);

}
