package com.e7learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.CourseChapter;

@Repository("courseChapterRepository")
@Transactional
public interface CourseChapterRepository extends JpaRepository<CourseChapter, Integer>,
		PagingAndSortingRepository<CourseChapter, Integer>, JpaSpecificationExecutor<CourseChapter> {

}
