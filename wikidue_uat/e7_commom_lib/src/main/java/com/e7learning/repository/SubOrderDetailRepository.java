package com.e7learning.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.SubOrderDetail;

/**
 * @author J
 *
 */
@Repository("subOrderDetailRepository")
@Transactional
public interface SubOrderDetailRepository extends JpaRepository<SubOrderDetail, Integer>,
		PagingAndSortingRepository<SubOrderDetail, Integer>, JpaSpecificationExecutor<SubOrderDetail> {

	@Query("SELECT d FROM SubOrderDetail d WHERE d.product.pkProduct=:pkProduct")
	List<SubOrderDetail> findProductByPkProduct(@Param("pkProduct") String pkProduct);

	@Query("SELECT d FROM SubOrderDetail d WHERE d.subOrder.vendor.uid=:uid and d.subOrder.mainOrder.payment.status='P' and d.product.course != null and d.subOrder.mainOrder.payment.paymentDt between :start and :end order by d.subOrder.mainOrder.orderDt")
	List<SubOrderDetail> findMonthlyCourseReortByVendor(@Param("uid") String uid,@Param("start") Date start,@Param("end") Date end);
	
	@Query("SELECT d FROM SubOrderDetail d WHERE d.subOrder.vendor.uid=:uid and d.subOrder.mainOrder.payment.status='P' and d.product.props != null and d.deliveryDt between :start and :end order by d.subOrder.mainOrder.orderDt")
	List<SubOrderDetail> findMonthlyPropsReortByVendor(@Param("uid") String uid,@Param("start") Date start,@Param("end") Date end);
	
	@Query("SELECT d FROM SubOrderDetail d WHERE d.subOrder.vendor.uid=:uid and d.status='R' and d.refundDt between :start and :end order by d.subOrder.mainOrder.orderDt")
	List<SubOrderDetail> findMonthlyRefundReortByVendor(@Param("uid") String uid,@Param("start") Date start,@Param("end") Date end);
	
	@Query("SELECT d FROM SubOrderDetail d WHERE d.subOrder.vendor.uid=:uid and d.status='J' and d.updateDt between :start and :end and d.deliveryDt between :start and :end")
	List<SubOrderDetail> findMonthlyRefuseReturnReortByVendor(@Param("uid") String uid,@Param("start") Date start,@Param("end") Date end);
}
