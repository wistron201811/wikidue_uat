package com.e7learning.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.UserCourse;

/**
 * @author Clarence
 *
 */
@Repository("userCourseRepository")
@Transactional
public interface UserCourseRepository extends JpaRepository<UserCourse, Integer>,
		PagingAndSortingRepository<UserCourse, Integer>, JpaSpecificationExecutor<UserCourse> {

	List<UserCourse> findByCoursePkCourseAndUserId(Integer pkCourse, String uderId);
	
	@Modifying
	@Query("update UserCourse u set u.updateDt = ?1 where u.userId = ?2")
	int updateUpdateDtByUserId(Date updateDt, String userId);
}
