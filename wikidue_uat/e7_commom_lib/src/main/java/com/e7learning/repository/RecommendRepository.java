package com.e7learning.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.e7learning.repository.model.Recommend;

public interface RecommendRepository extends JpaRepository<Recommend, String>,
		PagingAndSortingRepository<Recommend, String>, JpaSpecificationExecutor<Recommend> {
	
	List<Recommend> findTop3ByPublishSDateLessThanEqualAndPublishEDateGreaterThanEqualAndActiveOrderByPublishSDateDesc(
			Date sDate, Date eDate, String active);

}
