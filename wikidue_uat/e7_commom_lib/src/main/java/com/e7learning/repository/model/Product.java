package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

/**
 * The persistent class for the PRODUCT database table.
 * 
 */
@Entity
@Table(name = "product")
@NamedQuery(name = "Product.findAll", query = "SELECT p FROM Product p")
@DynamicInsert
@DynamicUpdate
public class Product implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name = "PK_PRODUCT")
	private String pkProduct;

	private String active;

	@Column(name = "CREATE_DT")
	private Date createDt;

	@Column(name = "CREATE_ID")
	private String createId;

	@Column(name = "FK_COMBO_PRD")
	private Integer fkComboPrd;

	@Column(name = "IMAGE_ID")
	private String imageId;

	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;

	@Column(name = "DISCOUNT_PRICE")
	private Integer discountPrice;

	@Column(name = "PRODUCT_NAME")
	private String productName;

	@Column(name = "PRICE")
	private Integer price;

	@Column(name = "PUBLISH_E_DATE")
	private Date publishEDate;

	@Column(name = "PUBLISH_S_DATE")
	private Date publishSDate;

	@Column(name = "QUANTITY")
	private Integer quantity;

	@Column(name = "UPDATE_DT")
	private Date updateDt;

	// bi-directional many-to-one association to Camp
	@ManyToOne(cascade = {})
	@JoinColumn(name = "FK_CAMP")
	private Camp camp;

	// bi-directional many-to-one association to Course
	@ManyToOne(cascade = {})
	@JoinColumn(name = "FK_COURSE")
	private Course course;

	// bi-directional many-to-one association to Prop
	@ManyToOne(cascade = {})
	@JoinColumn(name = "FK_PROPS")
	private Props props;

	// bi-directional many-to-one association to ProductTag
	@OneToMany(mappedBy = "product")
	private List<ProductTag> productTags;

	// Many-to-one association to Vendor
	@ManyToOne(cascade = { CascadeType.MERGE })
	@JoinColumn(name = "VENDOR_UID")
	private Vendor vendor;
	
	@OneToMany(mappedBy="product")
	private List<UserFavourite> userFavourites;

	@Column(name = "WEIGHTS")
	private Integer weights;
	
	public Product() {
	}

	public String getPkProduct() {
		return this.pkProduct;
	}

	public void setPkProduct(String pkProduct) {
		this.pkProduct = pkProduct;
	}

	public String getActive() {
		return this.active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Date getCreateDt() {
		return this.createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return this.createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public Integer getFkComboPrd() {
		return this.fkComboPrd;
	}

	public void setFkComboPrd(Integer fkComboPrd) {
		this.fkComboPrd = fkComboPrd;
	}

	public String getImageId() {
		return this.imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public String getLastModifiedId() {
		return this.lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}

	public Integer getDiscountPrice() {
		return this.discountPrice;
	}

	public void setDiscountPrice(Integer discountPrice) {
		this.discountPrice = discountPrice;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public void setProps(Props props) {
		this.props = props;
	}

	public Props getProps() {
		return props;
	}

	public Integer getPrice() {
		return this.price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Date getPublishEDate() {
		return this.publishEDate;
	}

	public void setPublishEDate(Date publishEDate) {
		this.publishEDate = publishEDate;
	}

	public Date getPublishSDate() {
		return this.publishSDate;
	}

	public void setPublishSDate(Date publishSDate) {
		this.publishSDate = publishSDate;
	}

/** 庫存數量改用props的 
	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
*/
	public Date getUpdateDt() {
		return this.updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public Camp getCamp() {
		return this.camp;
	}

	public void setCamp(Camp camp) {
		this.camp = camp;
	}

	public Course getCourse() {
		return this.course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public List<ProductTag> getProductTags() {
		return this.productTags;
	}

	public void setProductTags(List<ProductTag> productTags) {
		this.productTags = productTags;
	}

	public ProductTag addProductTag(ProductTag productTag) {
		getProductTags().add(productTag);
		productTag.setProduct(this);

		return productTag;
	}

	public ProductTag removeProductTag(ProductTag productTag) {
		getProductTags().remove(productTag);
		productTag.setProduct(null);

		return productTag;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public List<UserFavourite> getUserFavourites() {
		return userFavourites;
	}

	public void setUserFavourites(List<UserFavourite> userFavourites) {
		this.userFavourites = userFavourites;
	}

	public Integer getWeights() {
		return weights;
	}

	public void setWeights(Integer weights) {
		this.weights = weights;
	}
	
}