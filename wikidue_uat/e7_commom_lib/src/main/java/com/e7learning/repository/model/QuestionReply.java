package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the QUESTION_REPLY database table.
 * 
 */
@Entity
@Table(name="question_reply")
@NamedQuery(name="QuestionReply.findAll", query="SELECT q FROM QuestionReply q")
public class QuestionReply implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="PK_QUESTION_REPLY")
	private Integer pkQuestionReply;

	private String active;

	private String content;
	
	@Column(name="USER_NAME")
	private String userName;

	@Column(name="CREATE_DT")
	private Date createDt;

	@Column(name="CREATE_ID")
	private String createId;

	@Column(name="LAST_MODIFIED_ID")
	private String lastModifiedId;

	@Column(name="UPDATE_DT")
	private Date updateDt;

	//bi-directional many-to-one association to CourseQuestion
	@ManyToOne
	@JoinColumn(name="FK_COURSE_QUESTION")
	private CourseQuestion courseQuestion;

	public QuestionReply() {
	}

	public Integer getPkQuestionReply() {
		return this.pkQuestionReply;
	}

	public void setPkQuestionReply(Integer pkQuestionReply) {
		this.pkQuestionReply = pkQuestionReply;
	}

	public String getActive() {
		return this.active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreateDt() {
		return this.createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return this.createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public String getLastModifiedId() {
		return this.lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}

	public Date getUpdateDt() {
		return this.updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public CourseQuestion getCourseQuestion() {
		return this.courseQuestion;
	}

	public void setCourseQuestion(CourseQuestion courseQuestion) {
		this.courseQuestion = courseQuestion;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	
}