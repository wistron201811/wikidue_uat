package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the USER_FAVOURITE database table.
 * 
 */
@Entity
@Table(name="user_favourite")
@NamedQuery(name="UserFavourite.findAll", query="SELECT u FROM UserFavourite u")
public class UserFavourite implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="PK_USER_FAVOURITE")
	private Integer pkUserFavourite;

	private String active;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATE_DT")
	private Date createDt;

	@Column(name="CREATE_ID")
	private String createId;

	@Column(name="LAST_MODIFIED_ID")
	private String lastModifiedId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATE_DT")
	private Date updateDt;

	//bi-directional many-to-one association to Camp
	@ManyToOne
	@JoinColumn(name="FK_CAMP")
	private Camp camp;

	//bi-directional many-to-one association to Product
	@ManyToOne
	@JoinColumn(name="FK_PRODUCT")
	private Product product;

	@ManyToOne
	@JoinColumn(name="FK_SHARE")
	private ShareCenter shareCenter;
	
	public UserFavourite() {
	}

	public Integer getPkUserFavourite() {
		return this.pkUserFavourite;
	}

	public void setPkUserFavourite(Integer pkUserFavourite) {
		this.pkUserFavourite = pkUserFavourite;
	}

	public String getActive() {
		return this.active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Date getCreateDt() {
		return this.createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return this.createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public String getLastModifiedId() {
		return this.lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}

	public Date getUpdateDt() {
		return this.updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public Camp getCamp() {
		return this.camp;
	}

	public void setCamp(Camp camp) {
		this.camp = camp;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public ShareCenter getShareCenter() {
		return shareCenter;
	}

	public void setShareCenter(ShareCenter shareCenter) {
		this.shareCenter = shareCenter;
	}
	
	

}