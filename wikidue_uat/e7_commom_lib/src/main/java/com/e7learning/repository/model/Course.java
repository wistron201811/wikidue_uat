package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * The persistent class for the COURSE database table.
 * 
 */
@Entity
@Table(name = "course")
@NamedQuery(name = "Course.findAll", query = "SELECT c FROM Course c")
public class Course implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PK_COURSE")
	private Integer pkCourse;

	@Column(name = "ACTIVE", columnDefinition = "CHAR(1) default '1'")
	private String active;

	@Column(name = "COURSE_EXPECTATION")
	private String courseExpectation;

	@Column(name = "COURSE_INTRODUCTION")
	private String courseIntroduction;

	@Column(name = "COURSE_NAME")
	private String courseName;

	@Column(name = "COURSE_PREPARATION")
	private String coursePreparation;

	@Column(name = "COURSE_SUMMARY")
	private String courseSummary;

	@Column(name = "COURSE_TARGET")
	private String courseTarget;

	@Column(name = "CREATE_DT")
	private Date createDt;

	@Column(name = "CREATE_ID")
	private String createId;

	@Column(name = "DISCOUNT_PRICE")
	private Integer discountPrice;

	@Column(name = "PRICE")
	private Integer price;

	@Column(name = "PUBLISH_E_DATE")
	private Date publishEDate;

	@Column(name = "PUBLISH_S_DATE")
	private Date publishSDate;

	@Column(name = "UPDATE_DT")
	private Date updateDt;

	@Column(name = "IMAGE_ONE_ID")
	private String imageOne;

	@Column(name = "IMAGE_TWO_ID")
	private String imageTwo;

	@ManyToOne
	@JoinColumn(name = "FK_TEACHER")
	private Teacher teacher;

	@OneToMany(mappedBy = "course")
	private List<CourseChapter> courseChapterList;

	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;

	@Column(name = "MATERIAL_NUM")
	private String materialNum;

	@Column(name = "COST_PRICE")
	private Integer costPrice;

	@Column(name = "VIDEO_URL")
	private String videoUrl;

	@OneToMany(mappedBy = "course")
	private List<CourseFile> courseFileList;

	@OneToMany(mappedBy = "course")
	private List<Tag> tags;
	
	@ManyToOne(cascade = { CascadeType.MERGE })
	@JoinColumn(name = "VENDOR_UID")
	private Vendor vendor;

	@Transient
	private boolean onMarket;
	
	@Transient
	private boolean hasVideo;
	
	public Integer getPkCourse() {
		return pkCourse;
	}

	public void setPkCourse(Integer pkCourse) {
		this.pkCourse = pkCourse;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getCourseExpectation() {
		return courseExpectation;
	}

	public void setCourseExpectation(String courseExpectation) {
		this.courseExpectation = courseExpectation;
	}

	public String getCourseIntroduction() {
		return courseIntroduction;
	}

	public void setCourseIntroduction(String courseIntroduction) {
		this.courseIntroduction = courseIntroduction;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCoursePreparation() {
		return coursePreparation;
	}

	public void setCoursePreparation(String coursePreparation) {
		this.coursePreparation = coursePreparation;
	}

	public String getCourseSummary() {
		return courseSummary;
	}

	public void setCourseSummary(String courseSummary) {
		this.courseSummary = courseSummary;
	}

	public String getCourseTarget() {
		return courseTarget;
	}

	public void setCourseTarget(String courseTarget) {
		this.courseTarget = courseTarget;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public Integer getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(Integer discountPrice) {
		this.discountPrice = discountPrice;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Date getPublishEDate() {
		return publishEDate;
	}

	public void setPublishEDate(Date publishEDate) {
		this.publishEDate = publishEDate;
	}

	public Date getPublishSDate() {
		return publishSDate;
	}

	public void setPublishSDate(Date publishSDate) {
		this.publishSDate = publishSDate;
	}

	public Date getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public String getImageOne() {
		return imageOne;
	}

	public void setImageOne(String imageOne) {
		this.imageOne = imageOne;
	}

	public String getImageTwo() {
		return imageTwo;
	}

	public void setImageTwo(String imageTwo) {
		this.imageTwo = imageTwo;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public List<CourseChapter> getCourseChapterList() {
		return courseChapterList;
	}

	public void setCourseChapterList(List<CourseChapter> courseChapterList) {
		this.courseChapterList = courseChapterList;
	}

	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}

	public String getMaterialNum() {
		return materialNum;
	}

	public void setMaterialNum(String materialNum) {
		this.materialNum = materialNum;
	}

	public Integer getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(Integer costPrice) {
		this.costPrice = costPrice;
	}

	public List<CourseFile> getCourseFileList() {
		return courseFileList;
	}

	public void setCourseFileList(List<CourseFile> courseFileList) {
		this.courseFileList = courseFileList;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public String getVideoUrl() {
		return videoUrl;
	}

	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public boolean isOnMarket() {
		return onMarket;
	}

	public void setOnMarket(boolean onMarket) {
		this.onMarket = onMarket;
	}

	public boolean isHasVideo() {
		return hasVideo;
	}

	public void setHasVideo(boolean hasVideo) {
		this.hasVideo = hasVideo;
	}
	
	public Integer getProgress() {
		if (courseChapterList != null) {
			return Math.round((Float
					.valueOf(courseChapterList.stream().mapToInt(v -> v.getProgress())
							.reduce((total, progress) -> total + progress).getAsInt())
					/ Float.valueOf(courseChapterList.size())));
		}
		return 0;
	}
}