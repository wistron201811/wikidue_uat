package com.e7learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.e7learning.repository.model.Advertisement;

public interface AdvertisementRepostiory extends JpaRepository<Advertisement, Integer>,
		PagingAndSortingRepository<Advertisement, Integer>, JpaSpecificationExecutor<Advertisement> {

	Advertisement findByCreateIdAndPkAdvertisement(String createId, Integer pkAdvertisement);

	Integer countByBannerCode(String bannerCode);

	Integer countByPkAdvertisementNotAndBannerCode(Integer pkAdvertisement, String bannerCode);
}
