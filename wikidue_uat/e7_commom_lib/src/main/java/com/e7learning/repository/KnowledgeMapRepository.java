package com.e7learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.KnowledgeMap;

/**
 * @author J
 *
 */
public interface KnowledgeMapRepository extends JpaRepository<KnowledgeMap, String>,
		PagingAndSortingRepository<KnowledgeMap, String>, JpaSpecificationExecutor<KnowledgeMap> {
	
	@Modifying
    @Transactional
	@Query(value="delete from KNOWLEDGE_MAP",nativeQuery = true)
	void truncateDB();
}
