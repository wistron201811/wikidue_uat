package com.e7learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.e7learning.repository.model.MainOrderNoSeq;
import com.e7learning.repository.model.MainOrderNoSeqPK;


@Repository
public interface MainOrderNoSeqRepository extends JpaRepository<MainOrderNoSeq, MainOrderNoSeqPK> ,
PagingAndSortingRepository<MainOrderNoSeq, MainOrderNoSeqPK>, JpaSpecificationExecutor<MainOrderNoSeq> {

//	@Modifying
//	@Query(value="INSERT INTO MAIN_ORDER_NO_SEQ (CREATE_DT, SEQ) VALUES (:CREATE_DT, (SELECT COALESCE(MAX(mons.SEQ)+1,1) FROM MAIN_ORDER_NO_SEQ mons WHERE mons.CREATE_DT=:CREATE_DT))", nativeQuery=true)
//	public MainOrderNoSeq insertSeq(@Param("CREATE_DT") String carateDt);
	
	@Query(value="select coalesce(max(mons.seq),1) from main_order_no_seq mons where mons.create_dt=:create_dt", nativeQuery=true)
	Integer getMaxSeq(@Param("create_dt") String carateDt);
}
