package com.e7learning.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.Course;

@Repository("courseRepository")
@Transactional
public interface CourseRepository extends JpaRepository<Course, Integer>, PagingAndSortingRepository<Course, Integer>,
		JpaSpecificationExecutor<Course> {

	@Query("SELECT c FROM Course c WHERE c.active = '1'")
	List<Course> findActive();

	@Query("SELECT c FROM Course c WHERE c.active = '1'")
	Page<Course> findActive(Pageable pageable);

	@Query("SELECT c FROM Course c WHERE c.teacher.pkTeacher = :pkTeacher")
	List<Course> findByPkTeacher(@Param("pkTeacher") Integer pkTeacher);

	List<Course> findTop4ByPublishSDateLessThanEqualAndPublishEDateGreaterThanEqualAndActiveOrderByPublishSDateDesc(
			Date sDate, Date eDate, String active);

	List<Course> findByVendorUid(String vendorUid);

	List<Course> findByMaterialNum(String materialNum);

	@Query("SELECT c FROM Course c WHERE c.materialNum = :materialNum and c.pkCourse != :pkCourse")
	List<Course> findByPkCourseAndMaterialNum(@Param("pkCourse")Integer pkCourse, @Param("materialNum")String materialNum);
}
