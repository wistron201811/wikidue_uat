package com.e7learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.e7learning.repository.model.ShareFile;

/**
 * @author ChrisTsai
 *
 */
public interface ShareFileRepository extends JpaRepository<ShareFile, Integer>,
		PagingAndSortingRepository<ShareFile, Integer>, JpaSpecificationExecutor<ShareFile> {

	void deleteByFileId(String fileId);
}
