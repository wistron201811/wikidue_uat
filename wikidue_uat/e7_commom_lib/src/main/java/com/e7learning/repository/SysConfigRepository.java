package com.e7learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.e7learning.repository.model.SysConfig;

@Repository
public interface SysConfigRepository extends JpaRepository<SysConfig, String>, PagingAndSortingRepository<SysConfig, String>,
		JpaSpecificationExecutor<SysConfig> {

}
