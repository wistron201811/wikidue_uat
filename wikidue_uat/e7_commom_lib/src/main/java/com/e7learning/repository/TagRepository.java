package com.e7learning.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.Tag;

/**
 * @author ChrisTsai
 *
 */
@Repository("tagRepository")
@Transactional
public interface TagRepository extends JpaRepository<Tag, Integer> ,
PagingAndSortingRepository<Tag, Integer>, JpaSpecificationExecutor<Tag> {

	@Query("SELECT t FROM Tag t WHERE t.course.pkCourse = :pkCourse order by t.tag")
	List<Tag> findByPkCourse(@Param("pkCourse")Integer pkCourse);
	
	@Query("SELECT t FROM Tag t WHERE t.props.pkProps = :pkProps order by t.tag")
	List<Tag> findByPkProps(@Param("pkProps")Integer pkProps);
	
	@Query("SELECT t FROM Tag t WHERE t.category.pkCategory = :fkCategory")
	List<Tag> findByFkCategory(@Param("fkCategory")Integer fkCategory);
}
