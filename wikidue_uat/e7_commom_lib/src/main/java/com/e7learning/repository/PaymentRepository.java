package com.e7learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.Payment;

/**
 * @author Clarence
 *
 */
@Repository("paymentRepository")
@Transactional
public interface PaymentRepository extends JpaRepository<Payment, String>, PagingAndSortingRepository<Payment, String>,
		JpaSpecificationExecutor<Payment> {

	@Modifying
	@Query(value = "UPDATE Payment p SET p.status = :status WHERE p.pkPayment = :pkPayment AND p.status = :oriStatus )")
	public int updatePaymentStatus(@Param("pkPayment") String pkPayment, @Param("oriStatus") String oriStatus,
			@Param("status") String status);

}
