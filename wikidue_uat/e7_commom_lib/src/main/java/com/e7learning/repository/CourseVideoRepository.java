package com.e7learning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.CourseVideo;

@Repository("courseVideoRepository")
@Transactional
public interface CourseVideoRepository extends JpaRepository<CourseVideo, Integer>,
PagingAndSortingRepository<CourseVideo, Integer>, JpaSpecificationExecutor<CourseVideo> {

}
