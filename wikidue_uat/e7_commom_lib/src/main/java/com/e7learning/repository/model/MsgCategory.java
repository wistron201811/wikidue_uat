package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the MSG_CATEGORY database table.
 * 
 */
@Entity
@Table(name="msg_category")
@NamedQuery(name="MsgCategory.findAll", query="SELECT m FROM MsgCategory m")
public class MsgCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PK_MSG_CATEGORY")
	private int pkMsgCategory;

	private String active;

	@Column(name="CATEGORY_CODE")
	private String categoryCode;

	@Column(name="CATEGORY_TYPE")
	private String categoryType;

	@Column(name="CREATE_DT")
	private Date createDt;

	@Column(name="CREATE_ID")
	private String createId;

	private String name;

	@Column(name="UPDATE_DT")
	private Date updateDt;

	//bi-directional many-to-one association to E7Message
	@OneToMany(mappedBy="msgCategory")
	private List<E7Message> e7Messages;

	public MsgCategory() {
	}

	public int getPkMsgCategory() {
		return this.pkMsgCategory;
	}

	public void setPkMsgCategory(int pkMsgCategory) {
		this.pkMsgCategory = pkMsgCategory;
	}

	public String getActive() {
		return this.active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getCategoryCode() {
		return this.categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getCategoryType() {
		return this.categoryType;
	}

	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}

	public Date getCreateDt() {
		return this.createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return this.createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getUpdateDt() {
		return this.updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public List<E7Message> getE7Messages() {
		return this.e7Messages;
	}

	public void setE7Messages(List<E7Message> e7Messages) {
		this.e7Messages = e7Messages;
	}

	public E7Message addE7Message(E7Message e7Message) {
		getE7Messages().add(e7Message);
		e7Message.setMsgCategory(this);

		return e7Message;
	}

	public E7Message removeE7Message(E7Message e7Message) {
		getE7Messages().remove(e7Message);
		e7Message.setMsgCategory(null);

		return e7Message;
	}

}