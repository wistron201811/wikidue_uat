
package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * The persistent class for the SUB_ORDER database table.
 * 
 */
@Entity
@Table(name = "sub_order")
@NamedQuery(name = "SubOrder.findAll", query = "SELECT so FROM SubOrder so")
public class SubOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PK_SUB_ORDER")
	private Integer pkSubOrder;

	@ManyToOne
	@JoinColumn(name = "FK_MAIN_ORDER")
	private MainOrder mainOrder;

	@ManyToOne
	@JoinColumn(name = "FK_VENDOR")
	private Vendor vendor;

	@Column(name = "ORDER_AMOUNT")
	private Integer orderAmount;
	
	@Column(name = "ORDER_STATUS")
	private String orderStatus;
	
	@Column(name = "DELIVERY_DT")
	private Date deliveryDt;
	
	@Column(name = "LOGISTICS")
	private String logistics;

	@Column(name = "LOGISTICS_NUM")
	private String logisticsNum;
	
	@Column(name = "CREATE_DT")
	private Date createDt;

	@Column(name = "CREATE_ID")
	private String createId;

	@Column(name = "UPDATE_DT")
	private Date updateDt;

	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;
	
	@OneToMany(mappedBy = "subOrder")
	private List<SubOrderDetail> subOrderDetailList;

	@Transient
	private boolean hasReturnRecord;
	
	public SubOrder() {
	}

	public Integer getPkSubOrder() {
		return pkSubOrder;
	}

	public void setPkSubOrder(Integer pkSubOrder) {
		this.pkSubOrder = pkSubOrder;
	}

	public MainOrder getMainOrder() {
		return mainOrder;
	}

	public void setMainOrder(MainOrder mainOrder) {
		this.mainOrder = mainOrder;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public Integer getOrderAmount() {
		return orderAmount;
	}

	public void setOrderAmount(Integer orderAmount) {
		this.orderAmount = orderAmount;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Date getDeliveryDt() {
		return deliveryDt;
	}

	public void setDeliveryDt(Date deliveryDt) {
		this.deliveryDt = deliveryDt;
	}

	public String getLogistics() {
		return logistics;
	}

	public void setLogistics(String logistics) {
		this.logistics = logistics;
	}

	public String getLogisticsNum() {
		return logisticsNum;
	}

	public void setLogisticsNum(String logisticsNum) {
		this.logisticsNum = logisticsNum;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public Date getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}

	public List<SubOrderDetail> getSubOrderDetailList() {
		return subOrderDetailList;
	}

	public void setSubOrderDetailList(List<SubOrderDetail> subOrderDetailList) {
		this.subOrderDetailList = subOrderDetailList;
	}

	public boolean isHasReturnRecord() {
		return hasReturnRecord;
	}

	public void setHasReturnRecord(boolean hasReturnRecord) {
		this.hasReturnRecord = hasReturnRecord;
	}

	
}