package com.e7learning.repository.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the SYS_CONFIG database table.
 * 
 */
@Entity
@Table(name="sys_config")
@NamedQuery(name="SysConfig.findAll", query="SELECT s FROM SysConfig s")
public class SysConfig implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String code;

	private String active;

	private String value;
	
	private String text;

	public SysConfig() {
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getActive() {
		return this.active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	

}