package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;


/**
 * The persistent class for the TAG database table.
 * 
 */
@Entity
@Table(name="tag")
@NamedQuery(name="Tag.findAll", query="SELECT t FROM Tag t")
@DynamicInsert
@DynamicUpdate
public class Tag implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="PK_TAG")
	private Integer pkTag;

	private String active;

	@Column(name="CREATE_DT")
	private Date createDt;

	@Column(name="CREATE_ID")
	private String createId;

	@Column(name="TAG")
	private String tag;
	
	@Column(name="TAG_NAME")
	private String tagName;

	@Column(name="LAST_MODIFIED_ID")
	private String lastModifiedId;

	@Column(name="TAG_LEVEL")
	private String tagLevel;

	@Column(name="UPDATE_DT")
	private Date updateDt;

	//bi-directional many-to-one association to Camp
	@ManyToOne
	@JoinColumn(name="FK_CAMP")
	private Camp camp;

	//bi-directional many-to-one association to Category
	@ManyToOne
	@JoinColumn(name="FK_CATEGORY")
	private Category category;

	//bi-directional many-to-one association to Course
	@ManyToOne
	@JoinColumn(name="FK_COURSE")
	private Course course;

	//bi-directional many-to-one association to Prop
	@ManyToOne
	@JoinColumn(name="FK_PROPS")
	private Props props;
	
	@ManyToOne
	@JoinColumn(name="FK_SHARE")
	private ShareCenter shareCenter;

	public Tag() {
	}

	public Integer getPkTag() {
		return this.pkTag;
	}

	public void setPkTag(Integer pkTag) {
		this.pkTag = pkTag;
	}

	public String getActive() {
		return this.active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Date getCreateDt() {
		return this.createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return this.createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public void setProps(Props props) {
		this.props = props;
	}

	public String getLastModifiedId() {
		return this.lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}

	public String getTagLevel() {
		return this.tagLevel;
	}

	public void setTagLevel(String tagLevel) {
		this.tagLevel = tagLevel;
	}

	public Date getUpdateDt() {
		return this.updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public Camp getCamp() {
		return this.camp;
	}

	public void setCamp(Camp camp) {
		this.camp = camp;
	}

	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Course getCourse() {
		return this.course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public ShareCenter getShareCenter() {
		return shareCenter;
	}

	public void setShareCenter(ShareCenter shareCenter) {
		this.shareCenter = shareCenter;
	}
	
	
}