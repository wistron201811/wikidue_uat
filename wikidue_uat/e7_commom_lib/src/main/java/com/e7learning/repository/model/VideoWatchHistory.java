package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the VIDEO_WATCH_HISTORY database table.
 * 
 */
@Entity
@Table(name="video_watch_history")
@NamedQuery(name="VideoWatchHistory.findAll", query="SELECT v FROM VideoWatchHistory v")
public class VideoWatchHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int seq;

	@Column(name="ACCOUNT_NAME")
	private String accountName;

	private Date begin;

	@Column(name="CREATE_DT")
	private Date createDt;

	@Column(name="HB_ID")
	private String hbId;

	private Double streaming;

	private String uid;

	private Integer views;

	public VideoWatchHistory() {
	}

	public int getSeq() {
		return this.seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}

	public String getAccountName() {
		return this.accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public Date getBegin() {
		return this.begin;
	}

	public void setBegin(Date begin) {
		this.begin = begin;
	}

	public Date getCreateDt() {
		return this.createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getHbId() {
		return this.hbId;
	}

	public void setHbId(String hbId) {
		this.hbId = hbId;
	}

	public Double getStreaming() {
		return this.streaming;
	}

	public void setStreaming(Double streaming) {
		this.streaming = streaming;
	}

	public String getUid() {
		return this.uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public int getViews() {
		return this.views;
	}

	public void setViews(int views) {
		this.views = views;
	}

}