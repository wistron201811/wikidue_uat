package com.e7learning.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.E7Message;

/**
 * @author ChrisTsai
 *
 */
@Transactional
public interface E7MessageRepository extends JpaRepository<E7Message, String> ,
PagingAndSortingRepository<E7Message, String>, JpaSpecificationExecutor<E7Message> {

	public List<E7Message> findTop5ByOwnerIDAndActiveOrderByCreateDtDesc(String ownerID, String active);
	
	public Integer countByOwnerIDAndActiveAndIsReadOrderByCreateDtDesc(String ownerID,String isRead, String active);
	
}
