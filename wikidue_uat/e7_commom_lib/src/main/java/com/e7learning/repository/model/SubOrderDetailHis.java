
package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the ORDER_DETAIL database table.
 * 
 */
@Entity
@Table(name = "sub_order_detail_his")
@NamedQuery(name = "SubOrderDetailHis.findAll", query = "SELECT s FROM SubOrderDetailHis s")
public class SubOrderDetailHis implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PK_SUB_ORDER_DETAIL_HIS")
	private Integer pkSubOrderDetailHis;
	
	@ManyToOne
	@JoinColumn(name = "FK_SUB_ORDER_DETAIL")
	private SubOrderDetail subOrderDetail;
	
	@ManyToOne
	@JoinColumn(name = "FK_SUB_ORDER")
	private SubOrder subOrder;

	@ManyToOne
	@JoinColumn(name = "FK_PRODUCT")
	private Product product;

	@Column(name = "PRODUCT_NAME")
	private String productName;

	@Column(name = "TYPE")
	private String type;
	
	@Column(name = "QUANTITY")
	private Integer quantity;

	@Column(name = "UNIT_PRICE")
	private Integer unitPrice;

	@Column(name = "TOTAL_AMOUNT")
	private Integer totalAmount;

	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "RETURN_REASON")
	private String returnReason;
	
	@Column(name = "CREATE_DT")
	private Date createDt;

	@Column(name = "CREATE_ID")
	private String createId;

	@Column(name = "UPDATE_DT")
	private Date updateDt;

	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;
	
	@Column(name = "DELIVERY_DT")
	private Date deliveryDt;
	
	@Column(name = "REFUND_DT")
	private Date refundDt;
	
	public SubOrderDetailHis() {
	}

	public SubOrder getSubOrder() {
		return subOrder;
	}

	public void setSubOrder(SubOrder subOrder) {
		this.subOrder = subOrder;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Integer getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Integer unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Integer getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Integer totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReturnReason() {
		return returnReason;
	}

	public void setReturnReason(String returnReason) {
		this.returnReason = returnReason;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public Date getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}

	public Date getDeliveryDt() {
		return deliveryDt;
	}

	public void setDeliveryDt(Date deliveryDt) {
		this.deliveryDt = deliveryDt;
	}

	public Date getRefundDt() {
		return refundDt;
	}

	public void setRefundDt(Date refundDt) {
		this.refundDt = refundDt;
	}

	public Integer getPkSubOrderDetailHis() {
		return pkSubOrderDetailHis;
	}

	public void setPkSubOrderDetailHis(Integer pkSubOrderDetailHis) {
		this.pkSubOrderDetailHis = pkSubOrderDetailHis;
	}

	public SubOrderDetail getSubOrderDetail() {
		return subOrderDetail;
	}

	public void setSubOrderDetail(SubOrderDetail subOrderDetail) {
		this.subOrderDetail = subOrderDetail;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}