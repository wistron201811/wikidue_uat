
package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

/**
 * The persistent class for the SHARE_CENTER database table.
 * 
 */
@Entity
@Table(name = "share_center")
@NamedQuery(name = "ShareCenter.findAll", query = "SELECT s FROM ShareCenter s")
@DynamicInsert
@DynamicUpdate
public class ShareCenter implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PK_SHARE")
	private Integer pkShare;

	@Column(name = "SHARE_NAME")
	private String shareName;

	@Column(name = "SHARE_INTRODUCTION")
	private String shareIntroduction;

	@Column(name = "SHARE_UID")
	private String shareUid;

	@Column(name = "SHARE_IAMGE")
	private String shareImage;
	
	@Column(name = "APPROVE_STATUS")
	private String approveStatus;

	@Column(name = "CREATE_DT")
	private Date createDt;

	@Column(name = "CREATE_ID")
	private String createId;

	@Column(name = "UPDATE_DT")
	private Date updateDt;

	@Column(name = "LAST_MODIFIED_ID")
	private String lastModifiedId;
	
	@OneToMany(mappedBy="shareCenter")
	private List<ShareTag> shareTags;
	
	@OneToMany(mappedBy="shareCenter")
	private List<Tag> tags;
	
	@OneToMany(mappedBy="shareCenter")
	private List<ShareFile> files;
	
	@Column(name = "ACTIVE")
	private String active;
	
	public String getLastModifiedId() {
		return lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}
	
	public ShareCenter() {
	}

	public Integer getPkShare() {
		return pkShare;
	}

	public void setPkShare(Integer pkShare) {
		this.pkShare = pkShare;
	}

	public String getShareName() {
		return shareName;
	}

	public void setShareName(String shareName) {
		this.shareName = shareName;
	}

	public String getShareIntroduction() {
		return shareIntroduction;
	}

	public void setShareIntroduction(String shareIntroduction) {
		this.shareIntroduction = shareIntroduction;
	}

	public String getShareUid() {
		return shareUid;
	}

	public void setShareUid(String shareUid) {
		this.shareUid = shareUid;
	}

	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	public Date getCreateDt() {
		return createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public Date getUpdateDt() {
		return updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public String getShareImage() {
		return shareImage;
	}

	public void setShareImage(String shareImage) {
		this.shareImage = shareImage;
	}

	public List<ShareTag> getShareTags() {
		return shareTags;
	}

	public void setShareTags(List<ShareTag> shareTags) {
		this.shareTags = shareTags;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public List<ShareFile> getFiles() {
		return files;
	}

	public void setFiles(List<ShareFile> files) {
		this.files = files;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}
	
	
	
	
}