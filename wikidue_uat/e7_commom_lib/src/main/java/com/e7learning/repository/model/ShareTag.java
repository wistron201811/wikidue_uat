package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the SHARE_TAG database table.
 * 
 */
@Entity
@Table(name="share_tag")
@NamedQuery(name="ShareTag.findAll", query="SELECT s FROM ShareTag s")
public class ShareTag implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="PK_SHARE_TAG")
	private int pkShareTag;

	private String active;

	@Column(name="CREATE_DT")
	private Date createDt;

	@Column(name="CREATE_ID")
	private String createId;

	@ManyToOne
	@JoinColumn(name = "FK_SHARE")
	private ShareCenter shareCenter;

	@Column(name="LAST_MODIFIED_ID")
	private String lastModifiedId;

	private String tag;

	@Column(name="TAG_LEVEL")
	private String tagLevel;

	@Column(name="TAG_NAME")
	private String tagName;

	@Column(name="UPDATE_DT")
	private Date updateDt;

	public ShareTag() {
	}

	public int getPkShareTag() {
		return this.pkShareTag;
	}

	public void setPkShareTag(int pkShareTag) {
		this.pkShareTag = pkShareTag;
	}

	public String getActive() {
		return this.active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Date getCreateDt() {
		return this.createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return this.createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public ShareCenter getShareCenter() {
		return shareCenter;
	}

	public void setShareCenter(ShareCenter shareCenter) {
		this.shareCenter = shareCenter;
	}

	public String getLastModifiedId() {
		return this.lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}

	public String getTag() {
		return this.tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getTagLevel() {
		return this.tagLevel;
	}

	public void setTagLevel(String tagLevel) {
		this.tagLevel = tagLevel;
	}

	public String getTagName() {
		return this.tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public Date getUpdateDt() {
		return this.updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

}