package com.e7learning.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.AdCategory;

/**
 * @author J
 *
 */
@Transactional
public interface AdCategoryRepository extends JpaRepository<AdCategory, Integer> ,
PagingAndSortingRepository<AdCategory, Integer>, JpaSpecificationExecutor<AdCategory> {

	List<AdCategory> findByCategoryTypeAndActive(String categoryType,String active);
	
	
	Integer countByCategoryCode(String categoryCode);
	
	Integer countByPkAdCategoryNotAndCategoryCode(Integer pkAdCategory, String categoryCode);
}
