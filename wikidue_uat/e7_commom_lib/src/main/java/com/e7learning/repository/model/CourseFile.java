package com.e7learning.repository.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * @author Clarence
 * 課程檔案
 *
 */
@Entity
@Table(name = "course_file")
@NamedQuery(name = "CourseFile.findAll", query = "SELECT c FROM CourseFile c")
public class CourseFile implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name = "PK_COURSE_FILE")
	private String pkCourseFile;

	@ManyToOne
	@JoinColumn(name = "FK_COURSE")
	private Course course;

	@Column(name = "FILE_ID")
	private String fileId;

	public String getPkCourseFile() {
		return pkCourseFile;
	}

	public void setPkCourseFile(String pkCourseFile) {
		this.pkCourseFile = pkCourseFile;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

}
