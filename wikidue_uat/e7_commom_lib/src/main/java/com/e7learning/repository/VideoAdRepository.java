package com.e7learning.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.repository.model.VideoAd;

@Repository("videoAdRepository")
@Transactional
public interface VideoAdRepository extends JpaRepository<VideoAd, Integer>,
PagingAndSortingRepository<VideoAd, Integer>, JpaSpecificationExecutor<VideoAd> {
	
	List<VideoAd> findByActive(String active);
}
