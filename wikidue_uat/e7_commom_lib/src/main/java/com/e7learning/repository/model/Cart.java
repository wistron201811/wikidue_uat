package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;


/**
 * The persistent class for the CART database table.
 * 
 */
@Entity
@Table(name = "cart")
@DynamicInsert
@DynamicUpdate
@NamedQuery(name="Cart.findAll", query="SELECT c FROM Cart c")
public class Cart implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="PK_CART")
	private Integer pkCart;

	@Column(name="CREATE_DT")
	private Date createDt;

	@Column(name="CREATE_ID")
	private String createId;

	@Column(name="LAST_MODIFIED_ID")
	private String lastModifiedId;

	@Column(name="LIST_PRICE")
	private Integer listPrice;

	@Column(name="FK_PRODUCT")
	private String fkProduct;

	@Column(name="PRODUCT_NAME")
	private String productName;
	
	@Column(name="PRODUCT_IMAGE")
	private String productImage;
	
	private String type;

	private String purchaser;

	private Integer quantity;

	private String status;

	@Column(name="UNIT_PRICE")
	private Integer unitPrice;

	@Column(name="UPDATE_DT")
	private Date updateDt;
	
	@Transient
	private Integer stock;

	public Cart() {
	}

	public Integer getPkCart() {
		return this.pkCart;
	}

	public void setPkCart(Integer pkCart) {
		this.pkCart = pkCart;
	}

	public Date getCreateDt() {
		return this.createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return this.createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public String getLastModifiedId() {
		return this.lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}

	public Integer getListPrice() {
		return this.listPrice;
	}

	public void setListPrice(Integer listPrice) {
		this.listPrice = listPrice;
	}

	public String getFkProduct() {
		return fkProduct;
	}

	public void setFkProduct(String fkProduct) {
		this.fkProduct = fkProduct;
	}

	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPurchaser() {
		return this.purchaser;
	}

	public void setPurchaser(String purchaser) {
		this.purchaser = purchaser;
	}

	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(Integer unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Date getUpdateDt() {
		return this.updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public String getProductImage() {
		return productImage;
	}

	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
	

}