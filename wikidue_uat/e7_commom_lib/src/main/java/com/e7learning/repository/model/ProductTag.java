package com.e7learning.repository.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;


/**
 * The persistent class for the PRODUCT_TAG database table.
 * 
 */
@Entity
@Table(name="product_tag")
@NamedQuery(name="ProductTag.findAll", query="SELECT p FROM ProductTag p")
@DynamicInsert
@DynamicUpdate
public class ProductTag implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PK_PRODUCT_TAG")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer pkProductTag;

	private String active;

	@Column(name="CREATE_DT")
	private Date createDt;

	@Column(name="CREATE_ID")
	private String createId;

	@Column(name="TAG")
	private String tag;
	
	@Column(name="TAG_NAME")
	private String tagName;

	@Column(name="LAST_MODIFIED_ID")
	private String lastModifiedId;

	@Column(name="TAG_LEVEL")
	private String tagLevel;

	@Column(name="UPDATE_DT")
	private Date updateDt;

	//bi-directional many-to-one association to Product
	@ManyToOne
	@JoinColumn(name="FK_PRODUCT")
	private Product product;

	//bi-directional many-to-one association to Category
	@ManyToOne
	@JoinColumn(name="FK_CATEGORY")
	private Category category;

	public ProductTag() {
	}

	public Integer getPkProductTag() {
		return this.pkProductTag;
	}

	public void setPkProductTag(Integer pkProductTag) {
		this.pkProductTag = pkProductTag;
	}

	public String getActive() {
		return this.active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Date getCreateDt() {
		return this.createDt;
	}

	public void setCreateDt(Date createDt) {
		this.createDt = createDt;
	}

	public String getCreateId() {
		return this.createId;
	}

	public void setCreateId(String createId) {
		this.createId = createId;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public String getLastModifiedId() {
		return this.lastModifiedId;
	}

	public void setLastModifiedId(String lastModifiedId) {
		this.lastModifiedId = lastModifiedId;
	}

	public String getTagLevel() {
		return this.tagLevel;
	}

	public void setTagLevel(String tagLevel) {
		this.tagLevel = tagLevel;
	}

	public Date getUpdateDt() {
		return this.updateDt;
	}

	public void setUpdateDt(Date updateDt) {
		this.updateDt = updateDt;
	}

	public Product getProduct() {
		return this.product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

}