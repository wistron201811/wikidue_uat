package com.e7learning.common.enums;

public enum RoleGradeCodeEnum {

	ROLE1("r1","一般會員(訪客)"),
	ROLE2("r2","學生"),
	ROLE3("r3","教師"),
//	ROLE4("r4","正規教師"),
	ROLE5("r5","家長"),
	GRADE1("g1","一年級"),
	GRADE2("g2","二年級"),
	GRADE3("g3","三年級"),
	GRADE4("g4","四年級"),
	GRADE5("g5","五年級"),
	GRADE6("g6","六年級"),
	GRADE7("g7","七年級"),
	GRADE8("g8","八年級"),
	GRADE9("g9","九年級"),
	GRADE10("g10","高一"),
	GRADE11("g11","高二"),
	GRADE12("g12","高三");
	
	private RoleGradeCodeEnum(String code, String name) {
		this.code = code;
		this.name = name;
	}
	
	private String code;
	
	private String name;

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
}
