package com.e7learning.common.service;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.core.GrantedAuthority;

import com.e7learning.common.aop.AuthThreadLocal;
import com.e7learning.common.vo.UserAuthVo;

/**
 * @author ChrisTsai
 *
 */
public class BaseService {

	/**
	 * Gets the current user.
	 *
	 * @return the current user
	 */
	public UserAuthVo getCurrentUser() {
		if (AuthThreadLocal.getLoginUser() != null) {
			return AuthThreadLocal.getLoginUser();
		}
		return null;
	}

	/**
	 * Gets the current user id.
	 *
	 * @return the current user id
	 */
	public String getCurrentUserId() {
		if (AuthThreadLocal.getLoginUser() != null) {
			return AuthThreadLocal.getLoginUser().getLoginId();
		}
		return "anonymous";
	}

	public boolean isAdmin() {
		if (AuthThreadLocal.getLoginUser() != null) {
			for (GrantedAuthority tmp : AuthThreadLocal.getLoginUser().getAuthorities()) {
				if (StringUtils.equals(tmp.getAuthority(), "ADMIN"))
					return true;
			}
		}
		return false;
	}

}
