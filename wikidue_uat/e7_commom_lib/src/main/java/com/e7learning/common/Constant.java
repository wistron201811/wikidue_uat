package com.e7learning.common;

public class Constant {
	
	public static final String TRUE = "1";
	
	public static final String FLASE = "0";
	
	public static final String APPROVE_STATUS_Y = "Y";
	
	public static final String APPROVE_STATUS_N = "N";
	
	public static final long UPLOAD_FILE_SIZE = 2*1024 * 1024; /* bytes，上傳檔案大小限制 */
	
	public static final int UPLOAD_FILE_SIZE_MB = 2; /* mb，上傳檔案大小限制 */
	/**
	 * 圖片格式
	 */
	public static final String[] IMAGE_TYPES = new String[] { "PNG", "JPG", "JPEG", "GIF", "BMP" };
	/**
	 * 從request取得登入會員vo的key
	 */
	public static final String USER_VO_KEY = "user_auth";
	/**
	 * 從request取得訊息的key
	 */
	public static final String MESSAGE_KEY = "message";
	/**
	 * 從request取得錯誤訊息的key
	 */
	public static final String ERROR_MESSAGE_KEY = "error_message";
	
	public static final String LOGOUT_REDIRECT_URI = "logout_redirect_uri";
		
	public static final String SSO_LOGOUT_PAGE = "sso_logout_page";
	
	public static final String ROLE_USER = "USER";
	
	
	public static final String CART_STATUS_AVAILABLE = "A";
	
	/**
	 * 首頁輪播速度參數
	 */
	public static final String BANNER_AUTO_PLAY_SPEED = "BANNER_AUTO_PLAY_SPEED";
	
	/**
	 * 名人推薦右側區塊
	 */
	public static final String RECOMMEND_RIGHT_BLOCK = "RECOMMEND_RIGHT_BLOCK";
	
	public static final String STYSTEM = "system";
	
	public static final String COOKIE_WIKIDUE_TOKEN = "wikidue_token";
	
	public static final String COOKIE_WIKIDUE_SERIES = "wikidue_series";
	
	public static final String SSO_STATUS_ERROR = "ERROR";

}
