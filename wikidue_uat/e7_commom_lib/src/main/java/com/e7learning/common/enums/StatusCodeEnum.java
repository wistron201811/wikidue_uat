package com.e7learning.common.enums;

public enum StatusCodeEnum {

	ALL("",""),
	PUBLISH("1","已上架"),
	UNPUBLISTED("0","未上架");
	
	private StatusCodeEnum(String code, String name) {
		this.code = code;
		this.name = name;
	}
	
	private String code;
	
	private String name;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
