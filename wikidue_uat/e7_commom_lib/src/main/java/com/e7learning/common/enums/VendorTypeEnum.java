package com.e7learning.common.enums;

public enum VendorTypeEnum {
	PRODUCT("商品"),
	ADVERTISEMENT("廣告"),
	RECOMMAND("推薦");
	
	private VendorTypeEnum(String displayName) {
		this.displayName = displayName;
	}
	
	private String displayName;
	
	public String getDisplayName() {
		return displayName;
	}

}
