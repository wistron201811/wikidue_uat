package com.e7learning.common.service;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.e7learning.common.exception.FileNotFoundException;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.repository.FileStorageRepository;
import com.e7learning.repository.RoleRepository;
import com.e7learning.repository.UserInfoRepository;
import com.e7learning.repository.model.FileStorage;
import com.e7learning.repository.model.Role;
import com.e7learning.repository.model.RoleId;
import com.e7learning.repository.model.UserInfo;
import com.google.gson.Gson;

@Service
public class CommonService {

	private static final Logger LOG = Logger.getLogger(CommonService.class);

	@Autowired
	private FileStorageService fileStorageService;

	@Autowired
	private FileStorageRepository fileStorageRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private UserInfoRepository userInfoRepository;

	@Autowired
	private CommonCfgService commonCfgService;
	
	@PersistenceContext
    private EntityManager em;
	

	public void downloadImageFile(String fileId, HttpServletResponse response) {
		try {

			FileStorage fileStorage = fileStorageService.getFileStorageById(fileId);
			if (fileStorage != null) {
				response.setContentType(fileStorage.getFileType());
				response.setHeader("Content-Disposition",
						String.format("inline; filename=\"" + fileStorage.getFileName() + "\""));
				response.setContentLength(Integer.parseInt(
						String.valueOf(fileStorage.getResource(commonCfgService.getStoragePath()).getFile().length())));
				// ByteArrayInputStream bis = new ByteArrayInputStream();
				IOUtils.copy(fileStorage.getResource(commonCfgService.getStoragePath()).getInputStream(),
						response.getOutputStream());
				if (LOG.isDebugEnabled())
					LOG.debug("Size:" + fileStorage.getResource(commonCfgService.getStoragePath()).getFile().length()
							+ ", resource:"
							+ new Gson().toJson(fileStorage.getResource(commonCfgService.getStoragePath())));

			}
		} catch (Exception e) {
			LOG.error("downloadImageFile:", e);
			throw new FileNotFoundException();
		}
	}

	public void downloadFile(String fileId, HttpServletResponse response) {
		try {
			FileStorage fileStorage = fileStorageService.getFileStorageById(fileId);
			if (fileStorage != null) {
				response.setContentType(fileStorage.getFileType());
				String fileName = URLEncoder.encode(fileStorage.getFileName(), "UTF-8");
				response.setHeader("Content-Disposition",
						String.format("attachment; filename=\"%s\"; filename*=utf-8''%S", fileName, fileName));
				response.setContentLength(Integer.parseInt(
						String.valueOf(fileStorage.getResource(commonCfgService.getStoragePath()).getFile().length())));
				IOUtils.copy(fileStorage.getResource(commonCfgService.getStoragePath()).getInputStream(),
						response.getOutputStream());
			}
		} catch (Exception e) {
			LOG.error("downloadImageFile:", e);
			throw new FileNotFoundException();
		}
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public List<String> getAdminEmailList() {
		List<String> adminEmailList = new ArrayList<>();
		try {
			List<Role> adminRoleList = roleRepository
					.findAll((Root<Role> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
						List<Predicate> predicates = new ArrayList<>();

						predicates.add(cb.equal(root.get("roleId").get("roleName"), "ADMIN"));

						if (!predicates.isEmpty()) {
							return cb.and(predicates.toArray(new Predicate[predicates.size()]));
						}

						return cb.conjunction();
					});
			if (adminRoleList != null && !adminRoleList.isEmpty()) {
				List<String> adminIdList = new ArrayList<>();
				for (Role role : adminRoleList) {
					if (role.getRoleId() != null) {
						RoleId roleId = role.getRoleId();
						adminIdList.add(roleId.getEmail());
					}
				}
				for (String adminId : adminIdList) {
					UserInfo info = userInfoRepository.findOne(adminId);
					adminEmailList
							.add((info != null && !StringUtils.isEmpty(info.getEmail())) ? info.getEmail() : adminId);
				}
			}
		} catch (Exception e) {
			LOG.error("getAdminList:", e);
		}
		return adminEmailList;
	}

	public void initFile() {
		try {
			List<FileStorage> list = fileStorageRepository.findAll();
			list.stream().forEach(tmp -> {
				if (tmp.getFileContent() != null) {
					try {
						tmp.setFilePath(fileStorageService.saveRealFile(tmp.getPkFile(), tmp.getFileContent(),
								tmp.getCreateDt()));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					fileStorageRepository.save(tmp);
				}
			});
		} catch (Exception e) {
			LOG.error("initFile:", e);
		}
	}

	public String getAllTableString() {
		List<String> tabels = em.createNativeQuery("SHOW TABLES").getResultList();
		return "['" + tabels.stream().collect(Collectors.joining(",")).replace(",", "','") + "']";
	}

	public List querySql(String sql) {
		Query query = em.createNativeQuery(sql);
		query.unwrap(org.hibernate.SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		List list = query.getResultList();
		return list;
	}

	public int exeSql(String sql) {
		return em.createNativeQuery(sql).executeUpdate();
	}
}
