package com.e7learning.common.aop;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import com.e7learning.common.vo.UserAuthVo;

public class AuthThreadLocal {

	/** The request local. */
	private static ThreadLocal<HttpServletRequest> requestLocal = new ThreadLocal<HttpServletRequest>();

	/** The fun name local. */
	private static ThreadLocal<UserAuthVo> loginUser = new ThreadLocal<UserAuthVo>();

	/** The token. */
	private static ThreadLocal<String> token = new ThreadLocal<String>();

	/**
	 * Gets the request.
	 * 
	 * @return the request
	 */
	public static HttpServletRequest getRequest() {
		return (HttpServletRequest) requestLocal.get();
	}

	/**
	 * Sets the request.
	 * 
	 * @param request
	 *            the new request
	 */
	public static void setRequest(HttpServletRequest request) {
		requestLocal.set(request);
	}

	/**
	 * Gets the session.
	 * 
	 * @return the session
	 */
	public static HttpSession getSession() {
		return (HttpSession) ((HttpServletRequest) requestLocal.get()).getSession();
	}

	public static UserAuthVo getLoginUser() {
		return AuthThreadLocal.loginUser.get();
	}

	public static void setLoginUser(UserAuthVo loginUser) {
		AuthThreadLocal.loginUser.set(loginUser);
	}

	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public static String getToken() {
		if (StringUtils.isEmpty(AuthThreadLocal.token.get())) {
			AuthThreadLocal.setToken(UUID.randomUUID().toString());
		}
		return AuthThreadLocal.token.get();
	}

	/**
	 * Sets the token.
	 *
	 * @param token
	 *            the new token
	 */
	public static void setToken(String token) {
		AuthThreadLocal.token.set(token);
	}

}
