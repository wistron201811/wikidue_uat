package com.e7learning.common.utils;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.e7learning.common.Constant;

public class E7ValidationUtils {

	/**
	 * @param file
	 * @param checkNull
	 * @return
	 */
	public static boolean isImageFile(MultipartFile file, boolean checkNull) {
		if (!checkNull)
			return file.isEmpty() || FilenameUtils.isExtension(StringUtils.upperCase(file.getOriginalFilename()),
					Constant.IMAGE_TYPES);
		return !file.isEmpty()
				&& FilenameUtils.isExtension(StringUtils.upperCase(file.getOriginalFilename()), Constant.IMAGE_TYPES);
	}

	/**
	 * 不檢查null或empty
	 * 
	 * @param file
	 * @return
	 */
	public static boolean isImageFile(MultipartFile file) {
		return isImageFile(file, false);
	}
}
