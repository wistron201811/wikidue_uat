package com.e7learning.common.vo;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.e7learning.common.adapter.DateTimeAdapter;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement
public class SsoUserInfoHeader {

	@XmlElement(name = "type")
	private String type;

	@XmlElement(name = "date")
	@XmlJavaTypeAdapter(DateTimeAdapter.class)
	private Date date;

	@XmlElement(name = "messageVersion")
	private String messageVersion;

	@XmlElement(name = "status")
	private String status;

	@XmlTransient
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@XmlTransient
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@XmlTransient
	public String getMessageVersion() {
		return messageVersion;
	}

	public void setMessageVersion(String messageVersion) {
		this.messageVersion = messageVersion;
	}

	@XmlTransient
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
}
