package com.e7learning.common.vo;

import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class UserAuthVo implements UserDetails {
	private static final long serialVersionUID = 1L;

	private String accessToken;
	
	private String refreshToken;

	private String userId;

	private String loginId;

	private String name;

	private String mobile;

	private String email;

	private String createTime;

	private String loginTime;

	private String series;// 登出用

	private String grade;// 年級

	private Role role;

	private Collection<? extends GrantedAuthority> authorities;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return null;
	}

	@Override
	public String getUsername() {
		return name;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(String loginTime) {
		this.loginTime = loginTime;
	}

	public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public boolean isAdmin() {
		for (GrantedAuthority tmp : authorities) {
			if (StringUtils.equals(tmp.getAuthority(), "ADMIN"))
				return true;
		}
		return false;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
	
	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getApiGrade() {
		/**
		 * 一般會員(訪客) 1 一般訪客或使用者 學生 2 教師 3 一般申請帳號時選擇教師之角色 正規教師 4 具備正式教師資格之教師 家長 5 學生家長
		 * 
		 * grade參數 ""r1"":""一般會員(訪客)"" ""r2"":""學生"" ""r3"":""教師"" ""r4"":""正規教師""
		 * ""r5"":""家長"" ""g1"":""一年級"" ""g2"":""二年級"" ""g3"":""三年級"" ""g4"":""四年級""
		 * ""g5"":""五年級"" ""g6"":""六年級"" ""g7"":""七年級"" ""g8"":""八年級"" ""g9"":""九年級""
		 * ""g10"":""高一"" ""g11"":""高二"" ""g12"":""高三""
		 */

		String result = StringUtils.EMPTY;
		if (role != null && role.getRoleId() != null) {
			// 先判斷角色
			switch (role.getRoleId()) {
			case "2":
				// 學生則回傳年級
				result = StringUtils.isBlank(grade) ? "g1" : ("g" + grade);
				break;
			default:
				result = "r" + role.getRoleId();
				break;
			}
		}
		return result;
	}

}
