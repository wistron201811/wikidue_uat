package com.e7learning.common.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SsoConfig {

	@Value("${sso.login.page}")
	private String ssoLoginPage;

	@Value("${sso.token.page}")
	private String ssoTokenPage;

	@Value("${sso.clientId}")
	private String ssoClientId;

	@Value("${sso.login.responseType}")
	private String ssoLoginResponseType;

	@Value("${sso.clientSecret}")
	private String clientSecret;

	@Value("${sso.token.grantType}")
	private String ssoTokenGrantType;

	@Value("${host}")
	private String host;

	@Value("${sso.userInfo.api}")
	private String ssoUserInfoApi;

	public String getSsoLoginPage() {
		return ssoLoginPage;
	}

	public String getSsoTokenPage() {
		return ssoTokenPage;
	}

	public String getSsoClientId() {
		return ssoClientId;
	}

	public String getSsoLoginResponseType() {
		return ssoLoginResponseType;
	}

	public String getClientSecret() {
		return clientSecret;
	}

	public String getSsoTokenGrantType() {
		return ssoTokenGrantType;
	}

	public String getHost() {
		return host;
	}

	public String getSsoUserInfoApi() {
		return ssoUserInfoApi;
	}
	
	


}
