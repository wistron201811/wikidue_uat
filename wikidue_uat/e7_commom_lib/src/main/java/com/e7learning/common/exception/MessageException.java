package com.e7learning.common.exception;
/**
 * 訊息 例外類別
 *
 */
public class MessageException extends RuntimeException {

	/** serialVersion. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new service exception.
	 */
	public MessageException() {
	}

	/**
	 * Instantiates a new service exception.
	 *
	 * @param message
	 *            the message
	 */
	public MessageException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new service exception.
	 *
	 * @param message
	 *            the message
	 * @param throwable
	 *            the throwable
	 */
	public MessageException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
