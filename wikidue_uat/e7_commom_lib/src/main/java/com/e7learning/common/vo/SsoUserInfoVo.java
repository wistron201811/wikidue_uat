package com.e7learning.common.vo;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.e7learning.common.adapter.DateTimeAdapter;


@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement
public class SsoUserInfoVo {

	
	@XmlAttribute(name="accessToken")
	private String accessToken;
	
	@XmlElement(name="userId")
	private String userId;
	
	@XmlElement(name="loginId")
	private String loginId;
	
	@XmlElement(name="name")
	private String name;
	
	@XmlElement(name="mobile")
	private String mobile;
	
	@XmlElement(name="email")
	private String email;
	
	@XmlElement(name="createTime")
	@XmlJavaTypeAdapter(DateTimeAdapter.class)
	private Date createTime;
	
	@XmlElement(name="loginTime")
	@XmlSchemaType(name="date")
	private String loginTime;
	
	@XmlElement(name="grade")
	private String grade;
	
	@XmlElement(name="role")
	@XmlElementWrapper(name="roleList")
	private List<Role> roleList;

	@XmlTransient
	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	@XmlTransient
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@XmlTransient
	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	@XmlTransient
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlTransient
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@XmlTransient
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@XmlTransient
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@XmlTransient
	public String getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(String loginTime) {
		this.loginTime = loginTime;
	}

	@XmlTransient
	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	@XmlTransient
	public List<Role> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<Role> roleList) {
		this.roleList = roleList;
	}
	
	
}
