package com.e7learning.common.exception;

/**
 * 邏輯處理 例外類別
 */
public class ServiceException extends Exception {

	/** serialVersion. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new service exception.
	 */
	public ServiceException() {
	}

	/**
	 * Instantiates a new service exception.
	 *
	 * @param message
	 *            the message
	 */
	public ServiceException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new service exception.
	 *
	 * @param message
	 *            the message
	 * @param throwable
	 *            the throwable
	 */
	public ServiceException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
