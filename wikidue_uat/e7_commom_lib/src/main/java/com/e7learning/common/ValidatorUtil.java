package com.e7learning.common;

import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

public class ValidatorUtil {

	/**
	 * 驗證mail格式.
	 * 
	 * @param mail
	 *            the mail
	 * @return true, if is mail
	 */
	public static boolean isMail(String mail) {
		if (mail != null && !"".equals(mail)) {
			return (mail.toLowerCase().matches("^[_a-z0-9-]+([.][_a-z0-9-]+)*@[a-z0-9-]+([.][a-z0-9-]+)*$"));
		}
		return false;

	}

	/**
	 * 驗證手機格式.
	 * 
	 * @param mobile
	 *            the mobile
	 * @return true, if is mobile
	 */
	public static boolean isMobile(String mobile) {
		return (mobile.matches("[0]{1}[9]{1}[0-9]{8}"));
	}

	/**
	 * 驗證是否為數字(0~9的數字).
	 * 
	 * @param str
	 *            the str
	 * @return true, if is number
	 */
	public static boolean isNumber(String str) {
		return (str.matches("^[0-9]*[0-9][0-9]*$"));
	}

	/**
	 * 驗證電話格式.
	 * 
	 * @param telephone
	 *            the telephone
	 * @return true, if is telephone
	 */
	public static boolean isTelephone(String telephone) {
		return (telephone.matches("^[0-9]*[1-9][0-9]*$"));
	}

	/**
	 * 驗證是否為正整數且在規定區間內.
	 *
	 * @param str
	 *            String
	 * @param gei
	 *            int
	 * @param lei
	 *            int
	 * @return boolean
	 */
	public static boolean isNumberRange(String str, int gei, int lei) {

		return ValidatorUtil.isNumber(str) && Integer.parseInt(str) >= gei && Integer.parseInt(str) <= lei;
	}

	/**
	 * 檢查密碼是否同時包含英文大小寫及數字.
	 *
	 * @param password
	 *            the password
	 * @return true, if successful
	 */
	public static boolean checkPassword(String password) {
		int num = 0;
		num = Pattern.compile("\\d").matcher(password).find() ? num + 1 : num;
		num = Pattern.compile("[a-z]").matcher(password).find() ? num + 1 : num;
		num = Pattern.compile("[A-Z]").matcher(password).find() ? num + 1 : num;
		return num == 3;
	}

	/**
	 * 檢查密碼是否同時包含英文大小寫及數字與長度.
	 *
	 * @param password
	 *            the password
	 * @param minLength
	 * @return true, if successful
	 */
	public static boolean checkPassword(String password, int minLength) {
		return !StringUtils.isEmpty(password) && password.length() >= minLength && checkPassword(password);
	}

	public static boolean checkEmpNo(String empNo, int length) {
		// int num = 0;
		// num = Pattern.compile("\\d").matcher(empNo).find() ? num + 1 : num;
		// num = Pattern.compile("[a-z]").matcher(empNo).find() ? num + 1 : num;
		return !StringUtils.isEmpty(empNo) && (empNo.matches("^[A-Za-z0-9]+$")) && (empNo.length() == length);
	}

	public static final Pattern TWPID_PATTERN = Pattern.compile("[ABCDEFGHJKLMNPQRSTUVXYWZIO][12]\\d{8}");

	/**
	 * 身分證字號檢查程式，身分證字號規則： 字母(ABCDEFGHJKLMNPQRSTUVXYWZIO)對應一組數(10~35)，
	 * 令其十位數為X1，個位數為X2；( 如Ａ：X1=1 , X2=0 )；D表示2~9數字 Y = X1 + 9*X2 + 8*D1 + 7*D2 +
	 * 6*D3 + 5*D4 + 4*D5 + 3*D6 + 2*D7+ 1*D8 + D9 如Y能被10整除，則表示該身分證號碼為正確，否則為錯誤。
	 * 臺北市(A)、臺中市(B)、基隆市(C)、臺南市(D)、高雄市(E)、臺北縣(F)、
	 * 宜蘭縣(G)、桃園縣(H)、嘉義市(I)、新竹縣(J)、苗栗縣(K)、臺中縣(L)、
	 * 南投縣(M)、彰化縣(N)、新竹市(O)、雲林縣(P)、嘉義縣(Q)、臺南縣(R)、
	 * 高雄縣(S)、屏東縣(T)、花蓮縣(U)、臺東縣(V)、金門縣(W)、澎湖縣(X)、 陽明山(Y)、連江縣(Z)
	 * 
	 * @since 2006/07/19
	 */
	public static boolean isValidTWPID(String twpid) {
		boolean result = false;
		String pattern = "ABCDEFGHJKLMNPQRSTUVXYWZIO";
		if (TWPID_PATTERN.matcher(twpid.toUpperCase()).matches()) {
			int code = pattern.indexOf(twpid.toUpperCase().charAt(0)) + 10;
			int sum = 0;
			sum = (int) (code / 10) + 9 * (code % 10) + 8 * (twpid.charAt(1) - '0') + 7 * (twpid.charAt(2) - '0')
					+ 6 * (twpid.charAt(3) - '0') + 5 * (twpid.charAt(4) - '0') + 4 * (twpid.charAt(5) - '0')
					+ 3 * (twpid.charAt(6) - '0') + 2 * (twpid.charAt(7) - '0') + 1 * (twpid.charAt(8) - '0')
					+ (twpid.charAt(9) - '0');
			if ((sum % 10) == 0) {
				result = true;
			}
		}
		return result;
	}

	public static final Pattern TWBID_PATTERN = Pattern.compile("^[0-9]{8}$");

	/**
	 * 營利事業統一編號檢查程式 可至 http://www.etax.nat.gov.tw/ 查詢營業登記資料
	 * 
	 **/
	public static boolean isValidTWBID(String twbid) {
		boolean result = false;
		String weight = "12121241";
		boolean type2 = false; // 第七個數是否為七
		if (TWBID_PATTERN.matcher(twbid).matches()) {
			int tmp = 0, sum = 0;
			for (int i = 0; i < 8; i++) {
				tmp = (twbid.charAt(i) - '0') * (weight.charAt(i) - '0');
				sum += (int) (tmp / 10) + (tmp % 10); // 取出十位數和個位數相加
				if (i == 6 && twbid.charAt(i) == '7') {
					type2 = true;
				}
			}
			if (type2) {
				if ((sum % 10) == 0 || ((sum + 1) % 10) == 0) { // 如果第七位數為7
					result = true;
				}
			} else {
				if ((sum % 10) == 0) {
					result = true;
				}
			}
		}
		return result;
	}
}
