package com.e7learning.common.utils;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Map;

import org.apache.commons.codec.digest.DigestUtils;


public class SsoUtils {

	public static String computeSign(String appKey, String secret, Map<String, String> params) {
		// 對參數名進行字母遞增排序
		String[] keyArray = params.keySet().toArray(new String[0]);
		Arrays.sort(keyArray);
		// 以appKey爲前綴，secret爲後綴，組合參數
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(appKey);
		for (String key : keyArray) {
		stringBuilder.append(key).append(params.get(key));
		}
		stringBuilder.append(secret);
		String codes = stringBuilder.toString();
		// 計算簽章（SHA-1計算，轉換爲大寫）
		String sign = null;
		try {
			sign = DigestUtils.sha1Hex(codes.getBytes("UTF-8")).toUpperCase();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return sign;
	}

}
