package com.e7learning.common.utils;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import com.google.inject.internal.Lists;

public class RestTemplateUtils {

	private static int timeout = 30 * 1000;
	private static int connectTimeout = 10 * 1000;

	public static RestTemplate getSSlRestTemplate() {
		// 新建RestTemplate物件
		RestTemplate restTemplate = new RestTemplate();
		// 判斷證書檔案地址是否存在
		// 在握手期間，如果URL的主機名和伺服器的標識主機名不匹配，則驗證機制可以回撥此介面的實現 程式來確定是否應該允許此連線
		HostnameVerifier hv = new HostnameVerifier() {
			@Override
			public boolean verify(String urlHostName, SSLSession session) {
				return true;
			}
		};
		HttpsURLConnection.setDefaultHostnameVerifier(hv);
		// 構建SSL-Socket連結工廠
		SSLConnectionSocketFactory ssLSocketFactory;
		try {
			ssLSocketFactory = buildSSLSocketFactory(Lists.newArrayList("TLSv1"));
			// Spring提供HttpComponentsClientHttpRequestFactory指定使用HttpClient作為底層實現建立 HTTP請求
			HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory(
					HttpClients.custom().setSSLSocketFactory(ssLSocketFactory).build());
			// 設定傳遞資料超時時長
			httpRequestFactory.setReadTimeout(timeout);
			// 設定建立連線超時時長
			httpRequestFactory.setConnectTimeout(connectTimeout);
			// 設定獲取連線超時時長
			httpRequestFactory.setConnectionRequestTimeout(connectTimeout);
			restTemplate.setRequestFactory(httpRequestFactory);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return restTemplate;
	}

	/**
	 * 構建SSLSocketFactory
	 *
	 * @param keyStoreType
	 * @param keyFilePath
	 * @param keyPassword
	 * @param sslProtocols
	 * @param auth
	 *            是否需要client預設相信不安全證書
	 * @return
	 * @throws Exception
	 */
	private static SSLConnectionSocketFactory buildSSLSocketFactory(List<String> sslProtocols) throws Exception {

		SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy() {
			public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
				return true;
			}
		}).build();
		SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(sslContext,
				sslProtocols.toArray(new String[sslProtocols.size()]), null, new HostnameVerifier() {
					// 這裡不校驗hostname
					@Override
					public boolean verify(String urlHostName, SSLSession session) {
						return true;
					}
				});
		return sslConnectionSocketFactory;
	}
}
