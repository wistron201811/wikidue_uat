package com.e7learning.common.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

public class Role implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@XmlElement(name="roleId")
	String roleId;
	@XmlElement(name="schoolCode")
	String schoolCode;
	@XmlElement(name="schoolUserId")
	String schoolUserId;
	@XmlElement(name="createTime")
	String createTime;
	@XmlTransient
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	@XmlTransient
	public String getSchoolCode() {
		return schoolCode;
	}
	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}
	@XmlTransient
	public String getSchoolUserId() {
		return schoolUserId;
	}
	public void setSchoolUserId(String schoolUserId) {
		this.schoolUserId = schoolUserId;
	}
	@XmlTransient
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	
	
}
