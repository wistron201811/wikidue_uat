package com.e7learning.common.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class ParamUtils {
	
	public static Map<String, String> solveParams(String param) {
		if (!StringUtils.isEmpty(param)) {
			Map<String, String> map = new HashMap<>();
			String[] data = StringUtils.splitPreserveAllTokens(param, "&");
			if (data != null && data.length > 0) {
				for (String str : data) {
					int idx = str.indexOf('=');
					String key = idx > 0 ? str.substring(0, idx) : str;
					String value = idx > 0 && str.length() > idx + 1 ? str.substring(idx + 1) : null;
					map.put(key, value);
				}
			}
			return map;
		}
		return null;
	}
}
