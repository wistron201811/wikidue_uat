package com.e7learning.common.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement
public class SsoUserInfoBody {
	
	@XmlElement(name="code")
	private String code;
	
	@XmlElement(name="message")
	private String message;
	
	@XmlElement(name="info")
	private SsoUserInfoVo userInfo;

	@XmlTransient
	public SsoUserInfoVo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(SsoUserInfoVo userInfo) {
		this.userInfo = userInfo;
	}

	@XmlTransient
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@XmlTransient
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
	
}
