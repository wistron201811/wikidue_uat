package com.e7learning.common.utils;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class HtmlPreview {
	
	public static String convertImgTag(String contextPath, String htmlBody) {
		if (StringUtils.isNotBlank(contextPath) &&  StringUtils.isNotBlank(htmlBody)) {
			Document data = Jsoup.parse(htmlBody);
			Elements images = data.select("img");
			for (Element img : images) {
				String imgSrc = img.attr("src");
				if (StringUtils.isNotBlank(imgSrc) && imgSrc.length() == 32) {
					img.attr("src", contextPath + "/common/query/image/" + imgSrc);
				}
			}
			return data.toString();
		}else
			return htmlBody;
		
	}

	public static String convertImgTagFront(String contextPath, String htmlBody) {
		if (StringUtils.isNotBlank(contextPath) &&  StringUtils.isNotBlank(htmlBody)) {
			Document data = Jsoup.parse(htmlBody);
			Elements images = data.select("img");
			for (Element img : images) {
				String imgSrc = img.attr("src");
				if (StringUtils.isNotBlank(imgSrc) && imgSrc.length() == 32) {
					img.attr("src", contextPath + "/api/query/image/" + imgSrc);
					img.addClass("autoHeight");
				}
			}
			return data.toString();
		}else
			return htmlBody;

	}
}
