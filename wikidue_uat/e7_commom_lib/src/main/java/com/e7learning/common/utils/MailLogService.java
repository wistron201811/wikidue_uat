package com.e7learning.common.utils;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

public class MailLogService {

	public static final Logger LOG = Logger.getLogger(MailLogService.class);

	public static void writeErrorLog(String title, String[] receivers, String[] copies, Throwable t) {
		StringBuilder text = new StringBuilder();
		Gson gson = new Gson();
		text.append(String.format("send mail error 信件標題:%s,收件者:%s,副本:%s,錯誤訊息:%s", title, gson.toJson(receivers),
				gson.toJson(copies), t != null ? t.getMessage() : null));

		LOG.error(text, t);
	}

}
