package com.e7learning.common.exception;

/**
 * 邏輯處理 例外類別
 */
public class RestException extends Exception {

	/** serialVersion. */
	private static final long serialVersionUID = 1L;

	private String code = "0";

	/**
	 * Instantiates a new service exception.
	 */
	public RestException() {
	}

	/**
	 * Instantiates a new service exception.
	 *
	 * @param message
	 *            the message
	 */
	public RestException(String message) {
		super(message);
	}

	/**
	 * Instantiates a new service exception.
	 *
	 * @param message
	 *            the message
	 * @param throwable
	 *            the throwable
	 */
	public RestException(String message, Throwable throwable) {
		super(message, throwable);
	}

	/**
	 * @param code
	 * @param message
	 */
	public RestException(String code, String message) {
		super(message);
		this.code = code;
	}

	/**
	 * @param code
	 * @param message
	 * @param throwable
	 */
	public RestException(String code, String message, Throwable throwable) {
		super(message, throwable);
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
