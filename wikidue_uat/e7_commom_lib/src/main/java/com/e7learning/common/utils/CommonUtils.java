package com.e7learning.common.utils;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.e7learning.common.enums.OrderStatusEnum;

public class CommonUtils {

	public static Date getTodayWithoutTimes() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String today = sdf.format(date);
		try {
			date = sdf.parse(today);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	public static Date getDateWithoutTimes(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String today = sdf.format(date);
		try {
			date = sdf.parse(today);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	public static Date getDateEndWithTimes(Date date) {
		date = CommonUtils.getDateWithoutTimes(date);
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, 1);
		c.add(Calendar.SECOND, -1);
		date = c.getTime();
		return date;
	}

	public static String getDateString(Date date, String format) {
		if (date == null) {
			return "";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String dateString = "";
		try {
			dateString = sdf.format(date);
		} catch (Exception e) {

		}
		return dateString;
	}

	public static String getStringWithThousandths(Integer number) {
		String result = "";
		if (number != null) {
			NumberFormat numberFormat = NumberFormat.getNumberInstance();
			result = numberFormat.format(number);
		}

		return result;
	}

	public static String getOrderStatusName(String code) {
		String name = "";
		List<OrderStatusEnum> orderStatusList = Arrays.asList(OrderStatusEnum.values());
		for (OrderStatusEnum status : orderStatusList) {
			if (StringUtils.equalsIgnoreCase(status.name(), code)) {
				name = status.getDisplayName();
				break;
			}
		}
		return name;
	}

	public static long convertProgress(Integer watch, Integer total) {
		if (watch == null || total == null || watch.intValue() == 0)
			return 0;
		else {
			long v = Math.round(Math.ceil(10L * watch / total));
			return v > 10 ? 100 : v * 10;
		}
	}

	public static long convertProgress(Integer percent) {
		if (percent == null || percent.intValue() == 0)
			return 0;
		else {
			long v = Math.round(Math.ceil(Float.valueOf(percent) / 10));
			return v > 10 ? 100 : v * 10;
		}
	}
}
