package com.e7learning.common.utils;

import java.util.Arrays;

import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

public class FileUtils {

	private static final String[] IMAGE_TYPES = new String[] { MediaType.IMAGE_GIF_VALUE, MediaType.IMAGE_JPEG_VALUE,
			MediaType.IMAGE_PNG_VALUE };

	public static boolean checkFileSize(MultipartFile file) {
		if (file != null) {

		}
		return false;
	}

	public static boolean checkImageType(MultipartFile file) {
		if (file != null) {
			return Arrays.asList(IMAGE_TYPES).contains(file.getContentType());
		}
		return false;
	}

}
