package com.e7learning.common.vo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement(name="info")
@XmlAccessorType(XmlAccessType.NONE)
public class SsoUserInfoResponse {
	
	
	private SsoUserInfoHeader header;
	
	@XmlElement(name="body")
	private SsoUserInfoBody body;
	
	
	@XmlElement(name="header")
	public SsoUserInfoHeader getHeader() {
		return header;
	}

	public void setHeader(SsoUserInfoHeader header) {
		this.header = header;
	}

	@XmlTransient
	public SsoUserInfoBody getBody() {
		return body;
	}

	public void setBody(SsoUserInfoBody body) {
		this.body = body;
	}

	

	
}
