package com.e7learning.common.enums;

public enum CategoryTypeEnum {
	COURSE("線上課程"),
	PROPS("教具"),
	SHARE("分享園地"),
	CAMP("營隊活動");
	
	private CategoryTypeEnum(String displayName) {
		this.displayName = displayName;
	}
	
	private String displayName;
	
	public String getDisplayName() {
		return displayName;
	}

}
