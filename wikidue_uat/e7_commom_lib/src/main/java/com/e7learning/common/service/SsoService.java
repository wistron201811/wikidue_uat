package com.e7learning.common.service;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.e7learning.common.Constant;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.utils.RestTemplateUtils;
import com.e7learning.common.utils.SsoUtils;
import com.e7learning.common.vo.SsoToken;
import com.e7learning.common.vo.SsoUserInfoResponse;
import com.e7learning.common.vo.SsoUserInfoVo;
import com.e7learning.common.vo.UserAuthVo;
import com.e7learning.repository.RoleRepository;
import com.e7learning.repository.UserInfoRepository;
import com.e7learning.repository.model.Role;
import com.e7learning.repository.model.RoleId;
import com.e7learning.repository.model.UserInfo;
import com.google.gson.Gson;

@Service
public class SsoService {

	private static Logger log = Logger.getLogger(SsoService.class);

	// @Autowired
	// private RestTemplate restTemplate;

	@Autowired
	private MessageSource resources;

	@Autowired
	private SsoConfig configService;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private UserInfoRepository userInfoRepository;

	public SsoToken getToken(String code, String state, String series, String redirectUri) throws ServiceException {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
		map.add("client_id", configService.getSsoClientId());
		map.add("client_secret", configService.getClientSecret());
		map.add("grant_type", configService.getSsoTokenGrantType());
		map.add("code", code);
		map.add("redirect_uri", redirectUri);

		log.info("getToken input:" + map);
		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
		try {
			ResponseEntity<String> response = RestTemplateUtils.getSSlRestTemplate()
					.postForEntity(configService.getSsoTokenPage(), request, String.class);
			log.info("getToken result:" + response);
			SsoToken ssoToken = new Gson().fromJson(response.getBody(), SsoToken.class);
			return ssoToken;
		} catch (Exception e) {
			log.error("getToken input:" + map);
			log.error("getToken error", e);
			throw new ServiceException(resources.getMessage("sso.get.token.error", null, null));
		}
	}

	public SsoUserInfoResponse getUserInfo(String accessToken) throws ServiceException {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		MultiValueMap<String, String> userInfoParam = new LinkedMultiValueMap<String, String>();
		userInfoParam.add("accessToken", accessToken);
		userInfoParam.add("appKey", configService.getSsoClientId());
		Map<String, String> params = new HashMap<String, String>();
		userInfoParam.add("sign",
				SsoUtils.computeSign(configService.getSsoClientId(), configService.getClientSecret(), params));
		log.info("getUserInfo input:" + userInfoParam);
		HttpEntity<MultiValueMap<String, String>> requestForUserInfo = new HttpEntity<MultiValueMap<String, String>>(
				userInfoParam, headers);
		ResponseEntity<String> responseForUserInfo = RestTemplateUtils.getSSlRestTemplate()
				.postForEntity(configService.getSsoUserInfoApi(), requestForUserInfo, String.class);
		log.info("getUserInfo result:" + responseForUserInfo.getBody());
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(SsoUserInfoResponse.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			SsoUserInfoResponse ssoUserInfoResponse = (SsoUserInfoResponse) unmarshaller
					.unmarshal(new StringReader(responseForUserInfo.getBody()));
			if (!Constant.SSO_STATUS_ERROR.equals(ssoUserInfoResponse.getHeader().getStatus())) {
				saveUserInfoData(ssoUserInfoResponse);
			}
			return ssoUserInfoResponse;
		} catch (Exception e) {
			log.error("getUserInfo error", e);
			throw new ServiceException(resources.getMessage("sso.user.info.error", null, null));
		}
	}

	private void saveUserInfoData(SsoUserInfoResponse ssoUserInfoResponse) {
		try {
			UserInfo userInfo;
			Date currentDate = new Date();
			if (userInfoRepository.exists(ssoUserInfoResponse.getBody().getUserInfo().getUserId())) {
				userInfo = userInfoRepository.findOne(ssoUserInfoResponse.getBody().getUserInfo().getUserId());
			} else {
				userInfo = new UserInfo();
				userInfo.setUserId(ssoUserInfoResponse.getBody().getUserInfo().getUserId());
				userInfo.setLoginId(ssoUserInfoResponse.getBody().getUserInfo().getLoginId());
				userInfo.setCreateDt(currentDate);
			}
			userInfo.setEmail(ssoUserInfoResponse.getBody().getUserInfo().getEmail());
			userInfo.setGrate(ssoUserInfoResponse.getBody().getUserInfo().getGrade());
			userInfo.setMobile(ssoUserInfoResponse.getBody().getUserInfo().getMobile());
			userInfo.setName(ssoUserInfoResponse.getBody().getUserInfo().getName());
			userInfo.setUpdateDt(currentDate);
			userInfoRepository.save(userInfo);
		} catch (Exception e) {
			log.error("saveUserInfoData:", e);
		}

	}

	public void authUser(SsoUserInfoVo userInfo, String refreshToken, String series) throws ServiceException {
		authUser(userInfo, refreshToken, series, true);

	}

	public void authUser(SsoUserInfoVo userInfo, String refreshToken, String series, boolean isAdmin)
			throws ServiceException {
		UserAuthVo userAuthVo = new UserAuthVo();
		try {
			BeanUtils.copyProperties(userAuthVo, userInfo);
			userAuthVo.setSeries(series);
			userAuthVo.setRefreshToken(refreshToken);
			List<Role> roles = roleRepository.findByRoleIdEmail(userInfo.getLoginId());
			if (roles == null || roles.isEmpty()) {
				if (isAdmin)
					throw new ServiceException(resources.getMessage("access.denied", null, null));
				else {
					// 前台使用者 如果沒有role 就幫他新增一筆
					roles.add(genUserRole(userInfo.getLoginId()));
				}
			} else {
				if (!isAdmin)// 前台使用者
					if (roles.stream().filter(v -> StringUtils.equals(v.getRoleId().getRoleName(), Constant.ROLE_USER))
							.count() == 0) {
						roles.add(genUserRole(userInfo.getLoginId()));
					}
			}

			if (!CollectionUtils.isEmpty(userInfo.getRoleList())) {
				long count = userInfo.getRoleList().stream().count();
				// 拿最後一筆role
				userAuthVo.setRole(userInfo.getRoleList().stream().skip(count - 1).findFirst().get());
			}

			List<GrantedAuthority> auths = new ArrayList<>();
			roles.stream().forEach(v -> {
				SimpleGrantedAuthority authority = new SimpleGrantedAuthority(v.getRoleId().getRoleName());
				auths.add(authority);
			});
			userAuthVo.setAuthorities(auths);
			Authentication authentication = new UsernamePasswordAuthenticationToken(userAuthVo,
					userAuthVo.getPassword(), userAuthVo.getAuthorities());
			SecurityContextHolder.getContext().setAuthentication(authentication);
		} catch (

		ServiceException e) {
			log.error("authUser error", e);
			throw e;
		} catch (Exception e) {
			log.error("authUser error", e);
			throw new ServiceException(resources.getMessage("sso.auth.user.error", null, null));
		}

	}

	private Role genUserRole(String loginId) {
		Role role = new Role();
		role.setRoleId(new RoleId(loginId, Constant.ROLE_USER));
		roleRepository.save(role);
		return role;
	}

}
