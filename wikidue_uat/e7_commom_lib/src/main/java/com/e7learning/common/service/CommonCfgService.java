package com.e7learning.common.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CommonCfgService {
	
	@Value("${storage.path}")
	private String storagePath;

	public String getStoragePath() {
		return storagePath;
	}
	
}
