package com.e7learning.common.vo;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class KnowledgeMapVo implements Serializable {
	
	@SerializedName("level")
	private String level;
	
	@SerializedName("subjectName")
	private String subjectName;
	
	@SerializedName("subjectCode")
	private String subjectCode;

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getSubjectCode() {
		return subjectCode;
	}

	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}
	
}
