package com.e7learning.common.enums;

import org.apache.commons.lang3.StringUtils;

public enum OrderStatusEnum {
	E("訂單成立"),
	N("未付款"),
	W("付款處理中"),
	P("已付款"),
	D("已出貨"),
	H("退貨處理中"),
	G("退款處理中"),
	J("拒絕退件"),
	R("已退款"),
	C("已取消"),
	S("已逾期");
	private OrderStatusEnum(String displayName) {
		this.displayName = displayName;
	}
	
	private String displayName;
	
	public String getDisplayName() {
		return displayName;
	}
	
	public static OrderStatusEnum getByName(String name) {
		for (OrderStatusEnum orderStatus : OrderStatusEnum.values()) {
			if (StringUtils.equals(name, orderStatus.name())) {
				return orderStatus;
			}
		}
		return null;
	}

}
