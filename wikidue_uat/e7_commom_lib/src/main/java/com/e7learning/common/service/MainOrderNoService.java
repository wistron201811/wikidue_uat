package com.e7learning.common.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.e7learning.repository.MainOrderNoSeqRepository;
import com.e7learning.repository.model.MainOrderNoSeq;
import com.e7learning.repository.model.MainOrderNoSeqPK;

@Service
public class MainOrderNoService {

	private static Logger log = Logger.getLogger(MainOrderNoService.class);

	@Autowired
	private MainOrderNoSeqRepository mainOrderNoSeqRepository;

	private static final String PRE = "3";

	public String genMainOrderNo() {
		String currentDate = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
		MainOrderNoSeq mainOrderNoSeq = null;
		Integer max = 20;
		do {
			try {
				Integer maxSeq = mainOrderNoSeqRepository.getMaxSeq(currentDate);
				mainOrderNoSeq = new MainOrderNoSeq();
				mainOrderNoSeq.setId(new MainOrderNoSeqPK(currentDate, maxSeq + 1));
				mainOrderNoSeqRepository.save(mainOrderNoSeq);
			} catch (Exception e) {
				mainOrderNoSeq = null;
				log.error("genMainOrderNo", e);
				max--;
			}
		} while (mainOrderNoSeq == null && max > 0);

		return mainOrderNoSeq == null ? null
				: (PRE + mainOrderNoSeq.getId().getCreateDt() + String.format("%04d", mainOrderNoSeq.getId().getSeq()));
	}
}
