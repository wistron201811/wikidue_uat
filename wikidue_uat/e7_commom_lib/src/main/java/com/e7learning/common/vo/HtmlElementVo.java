package com.e7learning.common.vo;

import java.util.ArrayList;
import java.util.List;

import com.e7learning.repository.model.FileStorage;

public class HtmlElementVo{
	
	String body;
	
	List<FileStorage> imgs = new ArrayList<>();
	
	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
	
	public List<FileStorage> getImgs() {
		return imgs;
	}

	public void setImgs(List<FileStorage> imgs) {
		this.imgs = imgs;
	}

	@Override
	public String toString() {
		return body;
	}
	

}
