package com.e7learning.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * 找不到檔案 例外類別
 *
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "找不到檔案")
public class FileNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 7406409997946062398L;

}
