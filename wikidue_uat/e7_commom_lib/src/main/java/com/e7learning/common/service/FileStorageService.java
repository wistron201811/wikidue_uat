package com.e7learning.common.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.e7learning.common.Constant;
import com.e7learning.common.exception.MessageException;
import com.e7learning.common.exception.ServiceException;
import com.e7learning.common.vo.HtmlElementVo;
import com.e7learning.repository.FileStorageRepository;
import com.e7learning.repository.model.FileStorage;

@Service
public class FileStorageService extends BaseService {

	private static final String PREFIX_BASE64IMAGE = "data:";

	private static final String SUFFIX_BASE64IMAGE = ";base64,";

	private static final String API_PATH = "/common/query/image/";

	private static DateFormat df = new SimpleDateFormat("yyyyMMdd");

	@Autowired
	private FileStorageRepository fileStorageRepository;

	@Autowired
	private CommonCfgService commonCfgService;

	private static final Logger LOG = Logger.getLogger(FileStorageService.class);

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public Optional<FileStorage> careateFile(MultipartFile file) throws IOException, ServiceException {
		return Optional.ofNullable(saveFile(file));

	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public FileStorage saveFile(MultipartFile file) throws IOException, ServiceException {
		if (file.isEmpty()) {
			return null;
		}
		byte[] bytes = file.getBytes();
		String fileName = file.getOriginalFilename();
		String fileType = FilenameUtils.getExtension(file.getOriginalFilename());
		FileStorage fs = new FileStorage();
		fs.setCreateDt(new Date());
		fs.setFileContent(bytes);
		fs.setFileName(fileName);
		fs.setFileType(file.getContentType());
		return saveFile(fs);

	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public List<FileStorage> saveFiles(MultipartFile[] files) throws IOException, ServiceException {
		if (files == null || files.length == 0) {
			return null;
		}
		List<FileStorage> result = new ArrayList<>();
		for (MultipartFile file : files) {
			result.add(saveFile(file));
		}
		return result;

	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public FileStorage saveFile(FileStorage file) throws ServiceException {
		if (file == null || file.isEmpty()) {
			return null;
		}
		file.setActive(Constant.TRUE);
		file.setCreateDt(new Date());
		file.setCreateId(getCurrentUserId());
		file.setPkFile(fileStorageRepository.save(file).getPkFile());
		try {
			file.setFilePath(saveRealFile(file.getPkFile(), file.getBytes()));
			file.setFileSize(file.getBytes().length);
			file.setFileContent(null);// 改成存file 不放db
			fileStorageRepository.save(file);
		} catch (IOException e) {
			throw new ServiceException("存擋失敗");
		}
		return file;
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public List<FileStorage> saveFile(List<FileStorage> files) throws ServiceException {
		if (files == null || files.isEmpty()) {
			return null;
		}
		List<FileStorage> result = new ArrayList<>();
		for (FileStorage file : files) {
			result.add(saveFile(file));
		}
		return result;
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public void delFile(String fileId) {
		if (StringUtils.isBlank(fileId)) {
			return;
		}
		if (fileStorageRepository.exists(fileId)) {
			FileStorage fileStorage = fileStorageRepository.findOne(fileId);
			try {
				fileStorage.getResource(commonCfgService.getStoragePath()).getFile().delete();
			} catch (Exception e) {
				LOG.error("delFile err", e);
			}
			fileStorageRepository.delete(fileStorage);
		}

	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public List<FileStorage> delFile(List<String> files) {
		if (files == null || files.isEmpty()) {
			return null;
		}
		List<FileStorage> result = new ArrayList<>();
		for (String fileId : files) {
			delFile(fileId);
		}
		return result;
	}

	public FileStorage getFileStorageById(String id) {
		return fileStorageRepository.findOne(id);
	}

	public Optional<FileStorage> getFileStorage(String id) {
		if (id != null && fileStorageRepository.exists(id))
			return Optional.ofNullable(fileStorageRepository.findOne(id));
		return Optional.empty();
	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public HtmlElementVo saveHtmlElementImage(String htmlBody) throws ServiceException {
		HtmlElementVo html = new HtmlElementVo();
		if (StringUtils.isNotBlank(htmlBody)) {
			Document data = Jsoup.parse(htmlBody);
			Elements images = data.select("img");
			for (Element img : images) {
				String imgSrc = img.attr("src");
				if (StringUtils.isNotBlank(imgSrc) && imgSrc.startsWith(PREFIX_BASE64IMAGE)) {
					FileStorage image = new FileStorage();
					image.setPkFile(UUID.randomUUID().toString().replaceAll("-", ""));
					image.setActive(Constant.TRUE);
					image.setCreateDt(new Date());
					image.setFileContent(Base64.getDecoder().decode(
							imgSrc.substring(imgSrc.indexOf(SUFFIX_BASE64IMAGE) + SUFFIX_BASE64IMAGE.length())));
					image.setFileName(img.attr("name"));
					image.setFileType(imgSrc.substring(5, imgSrc.indexOf(SUFFIX_BASE64IMAGE)));
					image.setCreateId(getCurrentUserId());
					html.getImgs().add(image);
					img.attr("src", saveFile(image).getPkFile());
				} else if (StringUtils.isNotBlank(imgSrc) && StringUtils.contains(imgSrc, API_PATH)) {
					String fileId = StringUtils.substringAfterLast(imgSrc, "/");
					img.attr("src", fileId);
				}
			}
			html.setBody(data.toString());
		}
		return html;

	}

	@Transactional(rollbackFor = { ServiceException.class }, noRollbackFor = { MessageException.class })
	public HtmlElementVo updateHtmlElementImage(String oldHtmlBody, String newHtmlBody) throws ServiceException {
		HtmlElementVo html = new HtmlElementVo();
		List<String> delLsit = new ArrayList<>();

		if (StringUtils.isNotBlank(oldHtmlBody)) {
			Document data = Jsoup.parse(oldHtmlBody);
			Elements images = data.select("img");
			for (Element img : images) {
				String imgSrc = img.attr("src");
				if (StringUtils.isNotBlank(imgSrc) && StringUtils.contains(imgSrc, API_PATH)) {
					delLsit.add(StringUtils.substringAfterLast(imgSrc, "/"));
				} else if (StringUtils.isNotBlank(imgSrc) && imgSrc.length() == 32) {
					delLsit.add(imgSrc);
				}
			}
		}
		if (StringUtils.isNotBlank(newHtmlBody)) {
			Document data = Jsoup.parse(newHtmlBody);
			Elements images = data.select("img");
			for (Element img : images) {
				String imgSrc = img.attr("src");
				if (StringUtils.isNotBlank(imgSrc) && imgSrc.startsWith(PREFIX_BASE64IMAGE)) {
					FileStorage image = new FileStorage();
					image.setPkFile(UUID.randomUUID().toString().replaceAll("-", ""));
					image.setActive(Constant.TRUE);
					image.setCreateDt(new Date());
					image.setFileContent(Base64.getDecoder().decode(
							imgSrc.substring(imgSrc.indexOf(SUFFIX_BASE64IMAGE) + SUFFIX_BASE64IMAGE.length())));
					image.setFileName(img.attr("name"));
					image.setFileType(imgSrc.substring(5, imgSrc.indexOf(SUFFIX_BASE64IMAGE)));
					image.setCreateId(getCurrentUserId());
					html.getImgs().add(image);
					img.attr("src", saveFile(image).getPkFile());
				} else if (StringUtils.isNotBlank(imgSrc) && StringUtils.contains(imgSrc, API_PATH)) {
					String fileId = StringUtils.substringAfterLast(imgSrc, "/");
					img.attr("src", fileId);
					if (delLsit.contains(fileId)) {
						delLsit.remove(fileId);
					}
				}
			}
			// 刪除沒用到的圖
			delFile(delLsit);

			html.setBody(data.toString());
		}
		return html;

	}

	/**
	 * 把檔案存在指定目錄, 回傳路徑
	 * 
	 * @param fileId
	 * @param oriFile
	 * @return
	 * @throws IOException
	 */
	public String saveRealFile(String fileId, byte[] content) throws IOException {
		return saveRealFile(fileId, content, null);
	}

	/**
	 * 把檔案存在指定目錄, 回傳路徑
	 * 
	 * @param fileId
	 * @param content
	 * @param createDt
	 * @return
	 * @throws IOException
	 */
	public String saveRealFile(String fileId, byte[] content, Date createDt) throws IOException {
		String filePath = File.separator + getCurrentDate(createDt);
		File dir = new File(commonCfgService.getStoragePath() + filePath);
		if (!dir.exists() || !dir.isDirectory()) {
			dir.mkdirs();
		}
		File file = new File(dir.getAbsolutePath() + File.separator + fileId);
		if (!file.exists()) {
			file.createNewFile();
		}
		FileOutputStream fout = null;
		try {
			fout = new FileOutputStream(file);
			fout.write(content);
		} finally {
			if (fout != null) {
				fout.close();
			}
		}

		return filePath + File.separator + fileId;
	}

	private String getCurrentDate(Date createDt) {
		if (createDt != null)
			return df.format(createDt);
		return LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
	}
}
