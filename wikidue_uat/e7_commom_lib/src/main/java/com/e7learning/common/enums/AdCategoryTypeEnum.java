package com.e7learning.common.enums;

public enum AdCategoryTypeEnum {
	E7("智慧平台"),
	EVALUATE("評測中心");
	
	private AdCategoryTypeEnum(String displayName) {
		this.displayName = displayName;
	}
	
	private String displayName;
	
	public String getDisplayName() {
		return displayName;
	}

}
