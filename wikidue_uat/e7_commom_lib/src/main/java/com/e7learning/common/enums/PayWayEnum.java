package com.e7learning.common.enums;

import org.apache.commons.lang3.StringUtils;

public enum PayWayEnum {

	CREDIT_CREDITCARD("Credit_CreditCard", "信用卡_一次付清");

	private PayWayEnum(String code, String name) {
		this.code = code;
		this.name = name;
	}

	private String code;

	private String name;

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public static PayWayEnum getByCode(String code) {
		if (!StringUtils.isEmpty(code)) {
			for (PayWayEnum payWay : values()) {
				if (StringUtils.equals(payWay.getCode(), code)) {
					return payWay;
				}
			}
		}
		return null;

	}
}
